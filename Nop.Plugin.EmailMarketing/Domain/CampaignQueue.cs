﻿using Nop.Core;
using System.Data;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.EmailMarketing;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.EmailMarketing.Domain
{
		public class CampaignQueue : BaseEntity
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the entity identifier
			/// </summary>
			/// <value></value>
      public virtual int Id { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the campaign id.
			/// </summary>
			/// <value>The campaign id.</value>
			public virtual int CampaignId { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the delivered.
			/// </summary>
			/// <value>The delivered.</value>
			public virtual int Delivered { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the opened.
			/// </summary>
			/// <value>The opened.</value>
			public virtual int Opened { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the clicked through.
			/// </summary>
			/// <value>The clicked through.</value>
			public virtual int ClickedThrough { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the email insert status.
			/// </summary>
			/// <value>The email insert status.</value>
			public virtual string EmailInsertStatus { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the broad cast time.
			/// </summary>
			/// <value>The broad cast time.</value>
			public virtual DateTime BroadCastTime {	get;	set; }

    }

}
