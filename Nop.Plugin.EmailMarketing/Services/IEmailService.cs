﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Plugin.EmailMarketing.Domain;

namespace Nop.Plugin.EmailMarketing.Services
{
    public interface IEmailService
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all smart group.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <returns></returns>
			IPagedList<Email> GetAllEmail(int pageIndex, int pageSize);

            IEnumerable<string> SearchCustomerByEmail(string email); 

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all email .
			/// </summary>
			/// <returns></returns>
			List<Email> GetAllEmail();

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Inserts the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      Email InsertEmail(Email email);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets the smart group by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
      Email GetEmailById(int id);

      Email GetEmailByEmailAddress(string EmailAddress);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gers the sent emails by campaign id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			int GerSentEmailsByCampaignId(int id);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all email by campaign id.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			IPagedList<Email> GetAllEmailByCampaignId(int pageIndex, int pageSize, int id, string status);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all email by campaign id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <param name="status">The status.</param>
			/// <returns></returns>
			List<Email> GetAllEmailByCampaignId(int id, string status);

			//int GetSentEmailByCampaignId(int id);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Updates the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void UpdateEmail(Email email);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Deletes the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void DeleteEmail(Email email);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Counts the emails by campaign id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			int CountEmailsByCampaignId(int id);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Counts the opened emails by campaign id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			int CountOpenedEmailsByCampaignId(int id);
       
    }
}
