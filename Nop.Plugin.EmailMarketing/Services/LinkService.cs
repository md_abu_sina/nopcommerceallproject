﻿using System;
using System.Data;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Events;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Plugin.EmailMarketing.Domain;
using System.Collections.Generic;
using Nop.Services.Events;

namespace Nop.Plugin.EmailMarketing.Services
{
    public class LinkService : ILinkService
    {
        #region fields

        private readonly IRepository<Link> _linkRepository;
				private readonly IRepository<LinkClick> _linkClickRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly EmailObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

				public LinkService(IRepository<Link> linkRepository, IRepository<LinkClick> linkClickRepository, IEventPublisher eventPublisher,
                           EmailObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
						this._linkRepository = linkRepository;
						this._linkClickRepository = linkClickRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of ILinkService

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all link.
				/// </summary>
				/// <param name="pageIndex">Index of the page.</param>
				/// <param name="pageSize">Size of the page.</param>
				/// <param name="campaignId">The campaign id.</param>
				/// <returns></returns>
        public virtual IPagedList<Link> GetAllLink(int pageIndex, int pageSize, int campaignId)
        {
            var query = (from l in _linkRepository.Table
												 where l.CampaignId == campaignId
                         orderby l.CampaignId
                         select l);
						var links = new PagedList<Link>(query, pageIndex, pageSize);
						return links;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all link.
				/// </summary>
				/// <param name="campaignId">The campaign id.</param>
				/// <returns></returns>
				public virtual List<Link> GetAllLink(int campaignId)
				{
					var query = (from l in _linkRepository.Table
											 where l.CampaignId == campaignId
											 orderby l.CampaignId
											 select l);
					var links = query.ToList();
					return links;
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the link.
				/// </summary>
				/// <param name="link">The link.</param>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
				public virtual Link InsertLink(Link link)
        {
            if (link == null)
                throw new ArgumentNullException("link");

            _linkRepository.Insert(link);
						//event notification
            _eventPublisher.EntityInserted(link);

						return link;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the link click.
				/// </summary>
				/// <param name="linkId">The link id.</param>
				/// <param name="emailId">The email id.</param>
				/// --------------------------------------------------------------------------------------------
				public virtual void InsertLinkClick(int linkId = 0, int emailId = 0)
				{
						if(linkId == 0)
							throw new ArgumentNullException("link");

						var ifExist = _linkClickRepository.Table.Any(lc => lc.LinkId == linkId);

						var emailIdParam = _dataProvider.GetParameter();
						emailIdParam.ParameterName = "EmailId";
						emailIdParam.Value = emailId;
						emailIdParam.DbType = DbType.AnsiStringFixedLength;

						var linkIdParam = _dataProvider.GetParameter();
						linkIdParam.ParameterName = "LinkId";
						linkIdParam.Value = linkId;
						linkIdParam.DbType = DbType.AnsiStringFixedLength;
						
						if(!ifExist)
							_dbContext.ExecuteSqlCommand("INSERT INTO Nop_LinkClick VALUES (@LinkId, @EmailId);", null, linkIdParam, emailIdParam);
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the email by link id.
				/// </summary>
				/// <param name="pageIndex">Index of the page.</param>
				/// <param name="pageSize">Size of the page.</param>
				/// <param name="linkId">The link id.</param>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
				public IPagedList<Email> GetEmailByLinkId(int pageIndex, int pageSize, int linkId)
				{
					var query = from l in _linkRepository.Table
											 from e in l.Emails
											 where l.Id == linkId
											 orderby l.CampaignId
											 select e;
											 /*group e by e.EmailAddress into grp
											 select new
											 {
												 email = grp.Key,
												 count = grp.Count()
											 };*/
					var emails = new PagedList<Email>(query, pageIndex, pageSize);
					return emails;
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the link by id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
				public Link GetLinkById(int id)
        {
						var db = _linkRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the link.
				/// </summary>
				/// <param name="link">The link.</param>
				/// --------------------------------------------------------------------------------------------
				public void UpdateLink(Link link)
        {
						if(link == null)
                throw new ArgumentNullException("link");

            _linkRepository.Update(link);
            //event notification
            _eventPublisher.EntityUpdated(link);

        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Deletes the link.
				/// </summary>
				/// <param name="link">The link.</param>
				/// --------------------------------------------------------------------------------------------
				public void DeleteLink(Link link)
        {
            _linkRepository.Delete(link);
            _eventPublisher.EntityDeleted(link);
        }

        

        #endregion
    }
}
