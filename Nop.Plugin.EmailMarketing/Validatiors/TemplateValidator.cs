﻿using FluentValidation;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Services.Localization;


namespace Nop.Plugin.EmailMarketing.Validators
{
	public class TemplateValidator : AbstractValidator<EmailTemplateModel>
	{
		public TemplateValidator(ILocalizationService localizationService, IEmailTemplateService emailTemplateService)
		{
			RuleFor(x => x.Name)
					.NotNull()
					.WithMessage(localizationService.GetResource("Admin.EmailMarketing.EmailTemplate.Fields.Name.Required"))
					.Must((x, name) => !emailTemplateService.TemplateNameIsExist(name, x.Id))
					.WithMessage("This Template Name allready exist");
		}
	}
}