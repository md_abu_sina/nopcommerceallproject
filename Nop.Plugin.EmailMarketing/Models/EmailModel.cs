﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class EmailModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the campaign id.
		/// </summary>
		/// <value>The campaign id.</value>
		public int CampaignId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the source id.
		/// </summary>
		/// <value>The source id.</value>
		public virtual int SourceId {	get;	set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the name of the table.
		/// </summary>
		/// <value>The name of the table.</value>
    public string TableName { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		/// <value>The email address.</value>
		public string EmailAddress { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the sent status.
		/// </summary>
		/// <value>The sent status.</value>
		public string SentStatus { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the open status.
		/// </summary>
		/// <value>The open status.</value>
		public string OpenStatus { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the updated time.
		/// </summary>
		/// <value>The updated time.</value>
		public virtual DateTime? UpdatedTime { get; set; }
		
  }

}
