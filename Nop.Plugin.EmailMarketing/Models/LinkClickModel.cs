﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class LinkClickModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the email id.
		/// </summary>
		/// <value>The email id.</value>
    public virtual int EmailId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the link id.
		/// </summary>
		/// <value>The link id.</value>
    public virtual int LinkId { get; set; }
		
  }

}
