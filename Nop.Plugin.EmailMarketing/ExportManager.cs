﻿using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Core;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;
using System;
using Nop.Core.Domain.Localization;
using System.IO;
using System.Web.Hosting;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using System.Net;
using System.Web;
using System.Collections.Generic;
using Nop.Core.Domain;
using System.Text;
using System.Xml;
using OfficeOpenXml;
using System.Drawing;
using Nop.Plugin.EmailMarketing.Models;
using OfficeOpenXml.Style;
using Nop.Plugin.EmailMarketing.Domain;

namespace Nop.Plugin.EmailMarketing
{
	public static class ExportManager
	{     
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Exports the campaigns to XLSX.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="campaignsReport">The campaigns report.</param>
		/// <param name="_storeContext">The _store information settings.</param>
        public static void ExportCampaignsToXlsx(string filePath, List<CampaignReportModel> campaignsReport, IStoreContext _storeContext)
		{
			var newFile = new FileInfo(filePath);
			// ok, we can run the real code of the sample now
			using(var xlPackage = new ExcelPackage(newFile))
			{
				// uncomment this line if you want the XML written out to the outputDir
				//xlPackage.DebugMode = true; 

				// get handle to the existing worksheet
				var worksheet = xlPackage.Workbook.Worksheets.Add("campaign-report");
				//Create Headers and format them
				var properties = new string[]
                    {
                        "Campaign Name",
                        "Subject",
                        "Broad Cast Time",
                        "Delivered ",
                        "Opened ",
                        "Clicked Through"
                        
                     };
				for(int i = 0; i < properties.Length; i++)
				{
					worksheet.Cells[1, i + 1].Value = properties[i];
					worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
					worksheet.Cells[1, i + 1].Style.Font.Bold = true;
				}


				int row = 2;
				foreach(var campaignReport in campaignsReport)
				{
					int col = 1;

					worksheet.Cells[row, col].Value = campaignReport.Name;
					col++;

					worksheet.Cells[row, col].Value = campaignReport.Subject;
					col++;

					worksheet.Cells[row, col].Value = campaignReport.BroadCastTime.ToString();
					col++;

					worksheet.Cells[row, col].Value = campaignReport.Delivered + "[" + campaignReport.DeliveredPercentage + "]";
					col++;

					worksheet.Cells[row, col].Value = campaignReport.Opened + "[" + campaignReport.OpenedPercentage + "]";
					col++;

					worksheet.Cells[row, col].Value = campaignReport.ClickedThrough;
					col++;



					row++;
				}



				// we had better add some document properties to the spreadsheet 

				// set some core property values
                xlPackage.Workbook.Properties.Title = string.Format("{0} campaignReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Author = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.Subject = string.Format("{0} campaignReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Keywords = string.Format("{0} campaignReports", _storeContext.CurrentStore.Name);
				xlPackage.Workbook.Properties.Category = "campaignReports";
                xlPackage.Workbook.Properties.Comments = string.Format("{0} campaignReports", _storeContext.CurrentStore.Name);

				// set some extended property values
                xlPackage.Workbook.Properties.Company = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.HyperlinkBase = new Uri(_storeContext.CurrentStore.Url);

				// save the new spreadsheet
				xlPackage.Save();
			}
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Exports the campaigns to XML.
		/// </summary>
		/// <param name="campaignReports">The campaign reports.</param>
		/// <returns></returns>
		public static string ExportCampaignsToXml(List<CampaignReportModel> campaignReports)
		{
			var sb = new StringBuilder();
			var stringWriter = new StringWriter(sb);
			var xmlWriter = new XmlTextWriter(stringWriter);
			xmlWriter.WriteStartDocument();
			xmlWriter.WriteStartElement("emailReport");
			xmlWriter.WriteAttributeString("Version", NopVersion.CurrentVersion);

			foreach(var campaignReport in campaignReports)
			{
				xmlWriter.WriteStartElement("campaignReport");

				xmlWriter.WriteElementString("Name", null, campaignReport.Name);
				xmlWriter.WriteElementString("Subject", null, campaignReport.Subject);
				xmlWriter.WriteElementString("BroadCastTime", null, campaignReport.BroadCastTime.ToString());
				xmlWriter.WriteElementString("Delivered", null, campaignReport.Delivered.ToString() + "[" + campaignReport.DeliveredPercentage + "]");
				xmlWriter.WriteElementString("Opened", null, campaignReport.Opened.ToString() + "[" + campaignReport.OpenedPercentage + "]");
				xmlWriter.WriteElementString("ClickedThrough", null, campaignReport.ClickedThrough.ToString());

				xmlWriter.WriteEndElement();
			}

			xmlWriter.WriteEndElement();
			xmlWriter.WriteEndDocument();
			xmlWriter.Close();
			return stringWriter.ToString();
		}


		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Exports the detail campaigns to XLSX.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="detailReport">The detail report.</param>
		/// <param name="_storeInformationSettings">The _store information settings.</param>
        internal static void ExportDetailCampaignsToXlsx(string filePath, List<Email> detailEmailReport, IStoreContext _storeContext)
		{
			var newFile = new FileInfo(filePath);
			// ok, we can run the real code of the sample now
			using(var xlPackage = new ExcelPackage(newFile))
			{
				// uncomment this line if you want the XML written out to the outputDir
				//xlPackage.DebugMode = true; 

				// get handle to the existing worksheet
				var worksheet = xlPackage.Workbook.Worksheets.Add("detail-campaign-report");
				//Create Headers and format them
				var properties = new string[]
                    {
                        "Email Address",
                        "Sent Status",
                        "Open Status"
                     };
				for(int i = 0; i < properties.Length; i++)
				{
					worksheet.Cells[1, i + 1].Value = properties[i];
					worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
					worksheet.Cells[1, i + 1].Style.Font.Bold = true;
				}


				int row = 2;
				foreach(var email in detailEmailReport)
				{
					int col = 1;

					worksheet.Cells[row, col].Value = email.EmailAddress;
					col++;

					worksheet.Cells[row, col].Value = email.SentStatus;
					col++;

					worksheet.Cells[row, col].Value = email.OpenStatus;
					col++;

					
					row++;
				}



				// we had better add some document properties to the spreadsheet 

				// set some core property values
                xlPackage.Workbook.Properties.Title = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Author = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.Subject = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Keywords = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
				xlPackage.Workbook.Properties.Category = "campaignDetailReports";
                xlPackage.Workbook.Properties.Comments = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);

				// set some extended property values
                xlPackage.Workbook.Properties.Company = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.HyperlinkBase = new Uri(_storeContext.CurrentStore.Url);

				// save the new spreadsheet
				xlPackage.Save();
			}
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Exports the detail campaigns to XLSX.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="detailReport">The detail report.</param>
		/// <param name="_storeInformationSettings">The _store information settings.</param>
        internal static void ExportDetailCampaignsToXlsx(string filePath, List<Link> detailLinkReport, IStoreContext _storeContext)
		{
			var newFile = new FileInfo(filePath);
			// ok, we can run the real code of the sample now
			using(var xlPackage = new ExcelPackage(newFile))
			{
				// uncomment this line if you want the XML written out to the outputDir
				//xlPackage.DebugMode = true; 

				// get handle to the existing worksheet
				var worksheet = xlPackage.Workbook.Worksheets.Add("detail-campaign-report");
				//Create Headers and format them
				var properties = new string[]
                    {
                        "Id",
                        "Campaign Id",
                        "Link",
												"Click"
                     };
				for(int i = 0; i < properties.Length; i++)
				{
					worksheet.Cells[1, i + 1].Value = properties[i];
					worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
					worksheet.Cells[1, i + 1].Style.Font.Bold = true;
				}


				int row = 2;
				foreach(var link in detailLinkReport)
				{
					int col = 1;

					worksheet.Cells[row, col].Value = link.Id;
					col++;

					worksheet.Cells[row, col].Value = link.CampaignId;
					col++;

					worksheet.Cells[row, col].Value = link.LinkHref;
					col++;

					worksheet.Cells[row, col].Value = link.Click;
					col++;


					row++;
				}



				// we had better add some document properties to the spreadsheet 

				// set some core property values
                xlPackage.Workbook.Properties.Title = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Author = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.Subject = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Keywords = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
				xlPackage.Workbook.Properties.Category = "campaignDetailReports";
                xlPackage.Workbook.Properties.Comments = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);

				// set some extended property values
                xlPackage.Workbook.Properties.Company = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.HyperlinkBase = new Uri(_storeContext.CurrentStore.Url);

				// save the new spreadsheet
				xlPackage.Save();
			}
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Exports the detail campaigns to XLSX.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="detailLinkClickReport">The detail link click report.</param>
		/// <param name="_storeInformationSettings">The _store information settings.</param>
        internal static void ExportDetailCampaignsToXlsx(string filePath, List<LinkClick> detailLinkClickReport, IStoreContext _storeContext)
		{
			var newFile = new FileInfo(filePath);
			// ok, we can run the real code of the sample now
			using(var xlPackage = new ExcelPackage(newFile))
			{
				// uncomment this line if you want the XML written out to the outputDir
				//xlPackage.DebugMode = true; 

				// get handle to the existing worksheet
				var worksheet = xlPackage.Workbook.Worksheets.Add("detail-campaign-report");
				//Create Headers and format them
				var properties = new string[]
                    {
                        "Email Address"
                     };
				for(int i = 0; i < properties.Length; i++)
				{
					worksheet.Cells[1, i + 1].Value = properties[i];
					worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
					worksheet.Cells[1, i + 1].Style.Font.Bold = true;
				}


				int row = 2;
				foreach(var linkClick in detailLinkClickReport)
				{
					int col = 1;

					worksheet.Cells[row, col].Value = linkClick.EmailId;
					col++;

					row++;
				}



				// we had better add some document properties to the spreadsheet 

				// set some core property values
                xlPackage.Workbook.Properties.Title = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Author = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.Subject = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Keywords = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);
				xlPackage.Workbook.Properties.Category = "campaignDetailReports";
                xlPackage.Workbook.Properties.Comments = string.Format("{0} campaignDetailReports", _storeContext.CurrentStore.Name);

				// set some extended property values
                xlPackage.Workbook.Properties.Company = _storeContext.CurrentStore.Name;
                xlPackage.Workbook.Properties.HyperlinkBase = new Uri(_storeContext.CurrentStore.Url);

				// save the new spreadsheet
				xlPackage.Save();
			}
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Exports the detail campaigns to XML.
		/// </summary>
		/// <param name="detailReport">The detail report.</param>
		/// <returns></returns>
		public static string ExportDetailCampaignsToXml(List<Email> detailReport)
		{
			var sb = new StringBuilder();
			var stringWriter = new StringWriter(sb);
			var xmlWriter = new XmlTextWriter(stringWriter);
			try
			{
				xmlWriter.WriteStartDocument();
				xmlWriter.WriteStartElement("emailDetailReport");
				xmlWriter.WriteAttributeString("Version", NopVersion.CurrentVersion);

				foreach(var email in detailReport)
				{
					xmlWriter.WriteStartElement("campaignReport");

					xmlWriter.WriteElementString("Email Address", null, email.EmailAddress.ToString());
					xmlWriter.WriteElementString("Sent Status", null, email.SentStatus.ToString());
					xmlWriter.WriteElementString("Open Status", null, (email.OpenStatus != null) ? email.OpenStatus.ToString() : "");

					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteEndElement();
				xmlWriter.WriteEndDocument();
				xmlWriter.Close();
			}
			catch(Exception ex)
			{
				
				throw ex;
			}
			return stringWriter.ToString();
		}

		
	}
}