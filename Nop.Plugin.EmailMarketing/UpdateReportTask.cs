﻿using Nop.Core.Plugins;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Services.Messages;
using Nop.Core.Domain;
using Nop.Plugin.EmailMarketing.Lib;
using Nop.Web.Framework.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using Nop.Plugin.EmailMarketing.Models;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Controllers;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace Nop.Plugin.EmailMarketing
{

    public class UpdateReportTask : ITask
    {
        private readonly ILogger _logger;

        private readonly ICampaignQueueService _campaignQueueService;

        private readonly IEmailService _emailService;

        public UpdateReportTask(ILogger logger, ICampaignQueueService campaignQueueService, IEmailService emailService)
        {
            this._logger = logger;
            this._campaignQueueService = campaignQueueService;
            this._emailService = emailService;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Execute task
        /// </summary>
        public void Execute()
        {
            try
            {
                //May need future.
                this.UpdateReport(@"\\\\mta1\MTA-Logs");
                _logger.Error("Update Report Success");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            /*var impersonateService = new ImpersonationService();
            try
            {
                if(impersonateService.Impersonate(@"3nerds.net\brainstation-23", "3nerds.net", "d3v3l0pm3nt~"))
                {
                    // Your business logic here
                    this.UpdateReport(@"\\mta1\MTA-Logs");

                    //_logger.Error(dirInfo.Exists.ToString());

                    impersonateService.UndoImpersonationContext();
                    _logger.Error("Report updated successfully from PowerMta Log file @ " + DateTime.Now.ToString());
                }
                else
                {
                    //logging errors
                    _logger.Error("Network shared folder of Bounce log file not Found");
                }
            }
            catch(Exception ex)
            {
                // logging errors
                _logger.Error("User Impersonation error ==>> " + ex.ToString());
            }*/
        }

        private void UpdateReport(string folderPath)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.sendgrid.com/api/bounces.get.json");
            string urlParameters = "?api_user=proco-test&api_key=pr0c0P@ss";
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                var bounceReport = response.Content.ReadAsStringAsync().Result;
                //List<UpdateReportModel> updatedReportList = test;
                var updatedReportList=JsonConvert.DeserializeObject<List<BounceReportModel>>(bounceReport);

                foreach (var reportToBeUpdate in updatedReportList)
                {
                    var targetEmail = _emailService.GetEmailByEmailAddress(reportToBeUpdate.email);
                    if (targetEmail != null)
                    {
                        targetEmail.SentStatus = reportToBeUpdate.reason;
                        targetEmail.UpdatedTime = DateTime.UtcNow.Date;
                        _emailService.UpdateEmail(targetEmail);
                    }
                }


            }
            //else
            //{

            //}

            //string directoryPath = folderPath;
            //var processLog = new ProcessLogDirectory(directoryPath);
            //List<UpdateReportModel> updatedReportList = processLog.GetUpdatedReport();
            //var individualCampaigns = updatedReportList.Select(o => o.EnvId).Distinct();

            //foreach (var reportToBeUpdate in updatedReportList)
            //{
            //    var targetEmail = _emailService.GetEmailById(reportToBeUpdate.JobId);
            //    var processedEmail = reportToBeUpdate.ToEmailEntity(targetEmail);
            //    _emailService.UpdateEmail(processedEmail);
            //}

            //foreach (var campaign in individualCampaigns)
            //{
            //    var campaignQueToBeUpdate = _campaignQueueService.GetQueuedCampaignById(campaign);
            //    var numberOfEmailSent = _emailService.GerSentEmailsByCampaignId(campaign);
            //    campaignQueToBeUpdate.Delivered = numberOfEmailSent;
            //    _campaignQueueService.UpdateCampaignQueue(campaignQueToBeUpdate);
            //}
        }
    }

}
