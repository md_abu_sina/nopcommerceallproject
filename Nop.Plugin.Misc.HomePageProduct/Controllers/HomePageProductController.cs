﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Seo;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Plugin.Misc.HomePageProduct.Models;
using System.Collections.Generic;
using Nop.Services.Media;
using Nop.Plugin.Misc.HomePageProduct.Services;
using Nop.Plugin.Misc.HomePageProduct.Domain;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;
using Nop.Web.Infrastructure.Cache;
using Nop.Core.Caching;
using System.Diagnostics;
using Nop.Services.Security;
using Nop.Services.Tax;
using Nop.Services.Directory;
using System.IO;

namespace Nop.Plugin.Misc.HomePageProduct.Controllers
{
    public class HomePageProductController : Controller
    {
        #region Field

        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IWorkContext _workContext;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductService _productService;
        private readonly ILocalizationService _localizationService;
        private readonly ICategoryService _categoryService;
        private readonly IPictureService _pictureService;
        private readonly IHomePageProductCategoryImageService _homePageProductCategoryImageService;
        private readonly IHomePageProductService _homePageProductService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IPermissionService _permissionService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IHomePageCategoryService _homePageCategoryService;
        private readonly IHomePageSubCategoryService _homePageSubCategoryService;
        
        #endregion

        #region Ctr

        public HomePageProductController(IDateTimeHelper dateTimeHelper,
            IWorkContext workContext,
            CatalogSettings catalogSettings,
            IProductService productService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            ICategoryService categoryService,
            IHomePageProductCategoryImageService homePageProductCategoryImageService,
            IHomePageProductService homePageProductService,
            IWebHelper webHelper,
            IStoreContext storeContext,
            ICacheManager cacheManager,
            IPermissionService permissionService,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IHomePageCategoryService homePageCategoryService,
            IHomePageSubCategoryService homePageSubCategoryService)
        {
            _dateTimeHelper = dateTimeHelper;
            _workContext = workContext;
            _catalogSettings = catalogSettings;
            _productService = productService;
            _localizationService = localizationService;
            _categoryService = categoryService;
            _productService = productService;
            _pictureService = pictureService;
            _homePageProductCategoryImageService = homePageProductCategoryImageService;
            _homePageProductService = homePageProductService;
            _webHelper = webHelper;
            _storeContext = storeContext;
            _cacheManager = cacheManager;
            _permissionService = permissionService;
            _priceCalculationService = priceCalculationService;
            _taxService = taxService;
            _currencyService = currencyService;
            _priceFormatter = priceFormatter;
            _homePageCategoryService = homePageCategoryService;
            _homePageSubCategoryService = homePageSubCategoryService;
        }

        #endregion

        #region Methods

        public ActionResult List(int CategoryId)
        {
            var model = new HomePageProductListModel();
            model.CategoryId = CategoryId;
            model.CategoryName = _categoryService.GetCategoryById(CategoryId).Name;
            return View("~/Plugins/Misc.HomePageProduct/Views/MiscHomePageProduct/List.cshtml", model);
        }

        public ActionResult SubCategoryList(int CategoryId)
        {
            var model = new HomePageProductSubCategoryListModel();
            model.CategoryId = CategoryId;
            model.CategoryName = _categoryService.GetCategoryById(CategoryId).Name;
            return View("~/Plugins/Misc.HomePageProduct/Views/MiscHomePageProduct/SubCategoryList.cshtml", model);
        }

        public ActionResult Configure()
        {
            return View("~/Plugins/Misc.HomePageProduct/Views/MiscHomePageProduct/Configure.cshtml");
        }
        
        public ActionResult AddCategory()
        {
            var model = new HomeCategoryModel();
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            model.CategoryPriority = 0;
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
            {
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });
            }
            return View("~/Plugins/Misc.HomePageProduct/Views/MiscHomePageProduct/CategoryList.cshtml", model);
        }

        [HttpPost]
        public ActionResult CategoryList(DataSourceRequest command, CategoryListModel model)
        {
            var homeapgecategories = _homePageCategoryService.GetHomePageCategory(pageIndex: command.Page - 1, pageSize: command.PageSize);
            var gridModel = new DataSourceResult();
            gridModel.Data = homeapgecategories.Select(x =>
            {
                return new CategoryListModel()
                {
                  CategoryId=x.CategoryId,
                  CategoryName=_categoryService.GetCategoryById(x.CategoryId).Name,
                  Publish=x.Publish,
                  CategoryPriority=x.CategoryPriority
                };
            });
            gridModel.Total = homeapgecategories.Count;
            PublicPage();
            return Json(gridModel);

            //return Json(gridModel);
        }

        [HttpPost]
        public ActionResult HomePageProductUpdate(IEnumerable<HomePageProductModel> products, int CategoryId)
        {
            if (CategoryId > 0)
            {

                HomePageProductCategory homePageProductCategory = new HomePageProductCategory();
                if (products != null)
                {
                    foreach (var pModel in products)
                    {
                        //update
                        if (pModel.ShowOnHomePage)
                        {
                            _homePageProductService.Delete(pModel.Id);

                            homePageProductCategory.ProductId = pModel.Id;
                            homePageProductCategory.CategoryId = CategoryId;
                            homePageProductCategory.Priority = pModel.Priority;
                            _homePageProductService.Insert(homePageProductCategory);
                        }
                        else
                        {
                            _homePageProductService.Delete(pModel.Id);
                        }
                    }
                }
                PublicPage();
                return new NullJsonResult();
            }
            else
            {
                PublicPage();
                return this.Json(new DataSourceResult
                {
                    Errors = "Please select a category"
                });
            }
            //return new NullJsonResult();
        }


         [HttpPost]
        public ActionResult HomePageSubCategoryUpdate(IEnumerable<HomePageSubCategoryModel> SubCategories, int CategoryId)
        {
            if (CategoryId > 0)
            {
                HomePageSubCategory homePageSubCategory = new HomePageSubCategory();

                if (SubCategories != null)
                {
                    foreach (var SubCategory in SubCategories)
                    {
                        //update
                        if (SubCategory.SubCategoryShowOnHomePage)
                        {
                            _homePageSubCategoryService.Delete(SubCategory.Id);

                            homePageSubCategory.SubCategoryId = SubCategory.Id;
                            homePageSubCategory.CategoryId = CategoryId;
                            homePageSubCategory.SubCategoryPriority = SubCategory.Priority;
                            _homePageSubCategoryService.Insert(homePageSubCategory);
                        }
                        else
                        {
                            _homePageSubCategoryService.Delete(SubCategory.Id);
                        }
                    }
                }
                PublicPage();
                return new NullJsonResult();
            }
            else
            {
                PublicPage();
                return this.Json(new DataSourceResult
                {
                    Errors = "Please select a category"
                });
            }
            //return new NullJsonResult();
        }


        

        [HttpPost]
        public ActionResult ListJson(DataSourceRequest command, HomePageProductListModel model)
        {
                //Session["categoryId"] = model.SearchCategoryId;
                var categoryIds = new List<int>() { model.CategoryId };
                model.CategoryId = model.CategoryId;
                model.SearchIncludeSubCategories = true;

                //include subcategories
                if (model.SearchIncludeSubCategories && model.CategoryId > 0)
                    categoryIds.AddRange(GetChildCategoryIds(model.CategoryId));

                var products = _productService.SearchProducts(
                    categoryIds: categoryIds,
                    pageIndex: command.Page - 1,
                    pageSize: command.PageSize,
                    showHidden: true
                );
                var gridModel = new DataSourceResult();
                gridModel.Data = products.Select(x =>
                {
                    var defaultProductPicture = _pictureService.GetPicturesByProductId(x.Id).FirstOrDefault();
                    return new HomePageProductModel()
                    {
                        Id = x.Id,
                        Add = 0,
                        DisableBuyButton = x.DisableBuyButton,
                        //ShowOnHomePage=x.ShowOnHomePage,
                        ShowOnHomePage = _homePageProductService.GetHomePageProductByProductId(x.Id).ProductId > 0 ? true : false,
                        Delete = 0,
                        ManageInventoryMethod = x.ManageInventoryMethod.GetLocalizedEnum(_localizationService, _workContext.WorkingLanguage.Id),
                        Name = x.Name,
                        OldPrice = x.OldPrice,
                        Price = x.Price,
                        Published = x.Published,
                        Sku = x.Sku,
                        StockQuantity = x.StockQuantity,
                        Priority = _homePageProductService.GetHomePageProductByProductId(x.Id).Priority,
                        PictureThumbnailUrl = _pictureService.GetPictureUrl(defaultProductPicture, 75, true)
                    };
                });
                gridModel.Total = products.TotalCount;
                PublicPage();
                return Json(gridModel);
        }



        [HttpPost]
        public ActionResult ListJsonSubCategory(DataSourceRequest command, HomePageProductSubCategoryListModel model)
        {
            //Session["categoryId"] = model.SearchCategoryId;
            var categoryIds = new List<int>() { model.CategoryId };
            model.CategoryId = model.CategoryId;
            model.SearchIncludeSubCategories = true;

            //include subcategories
            if (model.SearchIncludeSubCategories && model.CategoryId > 0)
                categoryIds.AddRange(GetChildCategoryIds(model.CategoryId));

            var subcategories = _categoryService.GetAllCategoriesByParentCategoryId(model.CategoryId, true, true);
            var gridModel = new DataSourceResult();
            gridModel.Data = subcategories.Select(x =>
            {
                return new HomePageSubCategoryModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    SubCategoryShowOnHomePage = _homePageSubCategoryService.GetHomePageSubCategoryBySubCategoryIdList(x.Id).Count() > 0 ? true : false,
                    Priority = _homePageSubCategoryService.GetHomePageSubCategoryBySubCategoryId(x.Id).SubCategoryPriority
                };
            });
            gridModel.Total = subcategories.Count();
            PublicPage();
            return Json(gridModel);
        }

        public ActionResult CategoryImage(int CategoryId)
        {
            HomePageCategoryImageModel model = new HomePageCategoryImageModel();
            model.CategoryName = _categoryService.GetCategoryById(CategoryId).Name;
            model.CategoryId = CategoryId;
            return View("~/Plugins/Misc.HomePageProduct/Views/MiscHomePageProduct/CategoryImage.cshtml", model);
        }

        [HttpPost]
        public ActionResult CategoryPictureAdd(int pictureId,
         string overrideAltAttribute, string overrideTitleAttribute,
         int categoryId, string categoryColor)
        {

            if (pictureId == 0)
                throw new ArgumentException();

            var picture = _pictureService.GetPictureById(pictureId);
            if (picture == null)
                throw new ArgumentException("No picture found with the specified id");

            categoryColor = "#" + categoryColor;
            _homePageProductCategoryImageService.Insert(new HomePageProductCategoryImage
            {
                ImageId = pictureId,
                CategoryId = categoryId,
                CategoryColor = categoryColor == "" ? "#ae2125" : categoryColor
            });

            _pictureService.UpdatePicture(picture.Id, picture.PictureBinary, picture.MimeType,
                picture.SeoFilename, overrideAltAttribute, overrideTitleAttribute);

            PublicPage();

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult UpdateCategoryColor(int categoryId, string categoryColor)
        {
            categoryColor = "#" + categoryColor;
            List<HomePageProductCategoryImage> homePageProductCategoryImages = _homePageProductCategoryImageService.GetHomePageProductCategoryImagesByCategoryID(categoryId);

            for (int i = 0; i < homePageProductCategoryImages.Count;i++ )
            {
                HomePageProductCategoryImage objOfHomePageProductCategoryImage = _homePageProductCategoryImageService.HomePageProductCategoryImage(homePageProductCategoryImages[i].Id);
                objOfHomePageProductCategoryImage.CategoryColor = categoryColor;
                objOfHomePageProductCategoryImage.CategoryId = categoryId;
                _homePageProductCategoryImageService.UpdateCategoryColor(objOfHomePageProductCategoryImage);
            }

            PublicPage();

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult CategoryAdd(int categoryId, int categoryPriority)
        {
            if (categoryId > 0)
            {
                if (!_homePageCategoryService.IsCategoryExist(categoryId))
                {
                    _homePageCategoryService.Insert(new HomePageCategory
                    {
                        Publish = true,
                        CategoryId = categoryId,
                        CategoryPriority = categoryPriority,

                    });
                    PublicPage();
                    return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    PublicPage();
                    return Json(new { Result = "Category already added" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                PublicPage();
                return Json(new { Result = "Please select a category first" }, JsonRequestBehavior.AllowGet);
            
            }

        }

        [HttpPost]
        public ActionResult CategoryUpdate(int categoryId, int categoryPriority)
        {
            if (categoryId > 0)
            {
                _homePageCategoryService.Delete(categoryId);
                _homePageCategoryService.Insert(new HomePageCategory
                {
                    Publish = true,
                    CategoryId = categoryId,
                    CategoryPriority = categoryPriority,

                });
                PublicPage();
                    return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                PublicPage();
                return Json(new { Result = "Please select a category first" }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public ActionResult ChangePriority(int categoryId)
        {
            var singleCategory = _homePageCategoryService.GetHomePageCategoryByCategoryId(categoryId);
            PublicPage();
            return Json(new { catId = categoryId, priority = singleCategory.CategoryPriority });

        }

        [HttpPost]
        public ActionResult DeleteCategory(int categoryId)
        {
           _homePageCategoryService.Delete(categoryId);
           PublicPage();
            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteWholeCategory(int categoryId)
        {

            var categoryHomePagePictureList = _homePageProductCategoryImageService.GetCategoryPicturesByCategoryId(categoryId);
            foreach (var categoryHomePagePicture in categoryHomePagePictureList)
            {
                _homePageProductCategoryImageService.DeleteCategoryPicture(categoryHomePagePicture);
            }

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult HomepageProductsBySpecialCategory()
        {
            var filePath = _webHelper.MapPath("~/Plugins/Misc.HomePageProduct/special-category.txt");
            var jsonSpecialCategory = System.IO.File.ReadAllText(filePath);

            return Content(jsonSpecialCategory);

            //return View("~/Plugins/Misc.BiponeeV340/Views/MiscBiponeeV340/HomepageProductsBySpecialCategory.cshtml", model);
        }

        public ActionResult PublicPage()
        {
            string cacheKey = "CategoryHomePagePublicPage";

            var cacheModel = _cacheManager.Get(cacheKey, () =>
            {
                var homePageCategories = new List<CategoryHomePageModel>();
                var model = _homePageCategoryService.GetHomePageCategory();
                foreach (var item in model)
                {
                    //var categoryIds = _categoryService.GetAllCategoriesByParentCategoryId(item.Id).Select(c => c.Id).ToList();
                    var homePageCategory = new CategoryHomePageModel();
                    homePageCategory.MainCategoryName = _categoryService.GetCategoryById(item.CategoryId).Name;
                    homePageCategory.MainCategorySeName = _categoryService.GetCategoryById(item.CategoryId).GetSeName();

                    var subCategoryIds = _homePageSubCategoryService.GetHomePageSubCategoryByCategoryIdList(item.CategoryId);

                    foreach (var subCategoryId in subCategoryIds)
                    {
                        var categoryModel = new CategorySimpleModel
                        {
                            Id = subCategoryId,
                            Name = _categoryService.GetCategoryById(subCategoryId).Name,
                            SeName = _categoryService.GetCategoryById(subCategoryId).GetSeName()
                        };
                        homePageCategory.Categories.Add(categoryModel);
                    }

                    //homePageCategory.Categories = this.PrepareCategorySimpleModels(item.CategoryId);

                    var categoryImageIdList = _homePageProductCategoryImageService.GetPageProductCategoryImageIdByCategoryId(item.CategoryId);
                    foreach (var categoryImageId in categoryImageIdList)
                    {
                        //var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_PICTURE_MODEL_KEY, item.Id, 20, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                        var picture = _pictureService.GetPictureById(categoryImageId);
                        var pictureModel = new PictureModel
                        {
                            FullSizeImageUrl = _pictureService.GetPictureUrl(picture, 500),
                            ImageUrl = _pictureService.GetPictureUrl(picture, 500),
                            //Title = string.Format(_localizationService.GetResource("Media.Category.ImageLinkTitleFormat"), item.Name),
                            //AlternateText = string.Format(_localizationService.GetResource("Media.Category.ImageAlternateTextFormat"), item.Name)
                            Title = "",
                            AlternateText = ""

                        };

                        homePageCategory.PictureModel.Add(pictureModel);
                    }

                    var productIdList = _homePageProductService.GetHomePageProductCategoryProductIdByCategoryId(item.CategoryId);
                    List<Product> products = new List<Product>();
                    foreach (var productId in productIdList)
                    {
                        var product = _productService.GetProductById(productId);
                        //homePageCategory.Products.Add(product);
                        products.Add(product);

                    }
                    homePageCategory.Products.AddRange(PrepareProductOverviewModels(products));
                    homePageCategory.CategoryColor = _homePageProductCategoryImageService.GetPageProductCategoryColor(item.CategoryId);
                    homePageCategories.Add(homePageCategory);
                }

                string specialCategoryHtml = this.RenderPartialViewToString("~/Plugins/Misc.HomePageProduct/Views/MiscHomePageProduct/PublicPage.cshtml", homePageCategories);

                
                var filePath = _webHelper.MapPath("~/Plugins/Misc.HomePageProduct/special-category.txt");
                System.IO.File.WriteAllText(filePath, String.Empty);
                System.IO.File.WriteAllText(filePath, specialCategoryHtml);

                return Content("");
                //if (model.Count == 0)
                //    return Content("");
            });

            //if(cacheModel.Count==0)
                return Content("");
            //return View("~/Plugins/Misc.HomePageProduct/Views/MiscHomePageProduct/PublicPage.cshtml", cacheModel);
        }

        public virtual string RenderPartialViewToString(string viewName, object model)
        {
            //Original source code: http://craftycodeblog.com/2010/05/15/asp-net-mvc-render-partial-view-to-string/
            if (string.IsNullOrEmpty(viewName))
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");

            this.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
                var viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        [HttpPost]
        public ActionResult CategoryPictureList(DataSourceRequest command, int categoryId)
        {
            //a vendor should have access only to his products

            var categoryPictures = _homePageProductCategoryImageService.GetCategoryPicturesByCategoryId(categoryId);
            var categoryPicturesModel = categoryPictures
                .Select(x =>
                {
                    var picture = _pictureService.GetPictureById(x.ImageId);
                    if (picture == null)
                        throw new Exception("Picture cannot be loaded");
                    var m = new HomePageCategoryImageModel
                    {
                        Id = x.Id,
                        PictureId = x.ImageId,
                        PictureUrl = _pictureService.GetPictureUrl(picture),
                        OverrideAltAttribute = picture.AltAttribute,
                        OverrideTitleAttribute = picture.TitleAttribute
                    };
                    return m;
                })
                .ToList();

            var gridModel = new DataSourceResult
            {
                Data = categoryPicturesModel,
                Total = categoryPicturesModel.Count
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult CategoryPictureDelete(int id)
        {
            var homePageProductCategoryImage = _homePageProductCategoryImageService.HomePageProductCategoryImage(id);
            if (homePageProductCategoryImage == null)
                throw new ArgumentException("No category picture found with the specified id");

            //a vendor should have access only to his products

            var pictureId = homePageProductCategoryImage.ImageId;

            var picture = _pictureService.GetPictureById(pictureId);
            if (picture == null)
                throw new ArgumentException("No picture found with the specified id");
            _pictureService.DeletePicture(picture);
            _homePageProductCategoryImageService.DeleteCategoryPicture(homePageProductCategoryImage);
            return new NullJsonResult();
        }
        public ActionResult Settings()
        {

            return View("~/Plugins/Misc.BiponeePreOrder/Views/MiscBiponeePreOrder/Settings.cshtml");
        }

        [HttpPost]
        public ActionResult Settingsssssss()
        {
            return View("~/Plugins/Misc.BiponeePreOrder/Views/MiscBiponeePreOrder/Settings.cshtml");
        }

        #endregion


        #region Utility

        [NonAction]
        protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
        {
            var categoriesIds = new List<int>();
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, true);
            foreach (var category in categories)
            {
                categoriesIds.Add(category.Id);
                categoriesIds.AddRange(GetChildCategoryIds(category.Id));
            }
            return categoriesIds;
        }


        protected virtual List<CategorySimpleModel> PrepareCategorySimpleModels(int rootCategoryId,
        bool loadSubCategories = true, IList<Category> allCategories = null)
        {
            var result = new List<CategorySimpleModel>();

            //little hack for performance optimization.
            //we know that this method is used to load top and left menu for categories.
            //it'll load all categories anyway.
            //so there's no need to invoke "GetAllCategoriesByParentCategoryId" multiple times (extra SQL commands) to load childs
            //so we load all categories at once
            //if you don't like this implementation if you can uncomment the line below (old behavior) and comment several next lines (before foreach)
            //var categories = _categoryService.GetAllCategoriesByParentCategoryId(rootCategoryId);
            //if (allCategories == null)
            //{
            //    //load categories if null passed
            //    //we implemeneted it this way for performance optimization - recursive iterations (below)
            //    //this way all categories are loaded only once
            //    allCategories = _categoryService.GetAllCategoriesByParentCategoryId(rootCategoryId);
            //}
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(rootCategoryId);
            foreach (var category in categories)
            {
                var categoryModel = new CategorySimpleModel
                {
                    Id = category.Id,
                    Name = category.GetLocalized(x => x.Name),
                    SeName = category.GetSeName(),
                    IncludeInTopMenu = category.IncludeInTopMenu
                };

                ////product number for each category
                //if (_catalogSettings.ShowCategoryProductNumber)
                //{
                //    string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_NUMBER_OF_PRODUCTS_MODEL_KEY,
                //        string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
                //        _storeContext.CurrentStore.Id,
                //        category.Id);
                //    categoryModel.NumberOfProducts = _cacheManager.Get(cacheKey, () =>
                //    {
                //        var categoryIds = new List<int>();
                //        categoryIds.Add(category.Id);
                //        //include subcategories
                //        if (_catalogSettings.ShowCategoryProductNumberIncludingSubcategories)
                //            categoryIds.AddRange(GetChildCategoryIds(category.Id));
                //        return _productService.GetCategoryProductNumber(categoryIds, _storeContext.CurrentStore.Id);
                //    });
                //}

                if (loadSubCategories)
                {
                    var subCategories = PrepareCategorySimpleModels(category.Id, loadSubCategories, allCategories);
                    categoryModel.SubCategories.AddRange(subCategories);
                }
                result.Add(categoryModel);
            }

            return result;
        }

        protected IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
         bool preparePriceModel = true, bool preparePictureModel = true,
         int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
         bool forceRedirectionAfterAddingToCart = false)
        {
            if (products == null)
                throw new ArgumentNullException("products");

            var models = new List<ProductOverviewModel>();
            foreach (var product in products)
            {
                var model = new ProductOverviewModel
                {
                    Id = product.Id,
                    Name = product.GetLocalized(x => x.Name),
                    ShortDescription = product.GetLocalized(x => x.ShortDescription),
                    FullDescription = product.GetLocalized(x => x.FullDescription),
                    SeName = product.GetSeName(),
                };
                //price
                if (preparePriceModel)
                {
                    #region Prepare product price

                    var priceModel = new ProductOverviewModel.ProductPriceModel();

                    switch (product.ProductType)
                    {
                        case ProductType.GroupedProduct:
                            {
                                #region Grouped product

                                var associatedProducts = _productService.GetAssociatedProducts(product.Id, _storeContext.CurrentStore.Id);

                                switch (associatedProducts.Count)
                                {
                                    case 0:
                                        {
                                            //no associated products
                                            priceModel.OldPrice = null;
                                            priceModel.Price = null;
                                            priceModel.DisableBuyButton = true;
                                            priceModel.DisableWishlistButton = true;
                                            priceModel.AvailableForPreOrder = false;
                                        }
                                        break;
                                    default:
                                        {
                                            //we have at least one associated product
                                            priceModel.DisableBuyButton = true;
                                            priceModel.DisableWishlistButton = true;
                                            priceModel.AvailableForPreOrder = false;

                                            if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                                            {
                                                //find a minimum possible price
                                                decimal? minPossiblePrice = null;
                                                Product minPriceProduct = null;
                                                foreach (var associatedProduct in associatedProducts)
                                                {
                                                    //calculate for the maximum quantity (in case if we have tier prices)
                                                    var tmpPrice = _priceCalculationService.GetFinalPrice(associatedProduct,
                                                        _workContext.CurrentCustomer, decimal.Zero, true, int.MaxValue);
                                                    if (!minPossiblePrice.HasValue || tmpPrice < minPossiblePrice.Value)
                                                    {
                                                        minPriceProduct = associatedProduct;
                                                        minPossiblePrice = tmpPrice;
                                                    }
                                                }
                                                if (minPriceProduct != null && !minPriceProduct.CustomerEntersPrice)
                                                {
                                                    if (minPriceProduct.CallForPrice)
                                                    {
                                                        priceModel.OldPrice = null;
                                                        priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
                                                    }
                                                    else if (minPossiblePrice.HasValue)
                                                    {
                                                        //calculate prices
                                                        decimal taxRate;
                                                        decimal finalPriceBase = _taxService.GetProductPrice(minPriceProduct, minPossiblePrice.Value, out taxRate);
                                                        decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

                                                        priceModel.OldPrice = null;
                                                        priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));

                                                    }
                                                    else
                                                    {
                                                        //Actually it's not possible (we presume that minimalPrice always has a value)
                                                        //We never should get here
                                                        //Debug.WriteLine("Cannot calculate minPrice for product #{0}", product.Id);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //hide prices
                                                priceModel.OldPrice = null;
                                                priceModel.Price = null;
                                            }
                                        }
                                        break;
                                }

                                #endregion
                            }
                            break;
                        case ProductType.SimpleProduct:
                        default:
                            {
                                #region Simple product

                                //add to cart button
                                priceModel.DisableBuyButton = product.DisableBuyButton ||
                                    !_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart) ||
                                    !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

                                //add to wishlist button
                                priceModel.DisableWishlistButton = product.DisableWishlistButton ||
                                    !_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) ||
                                    !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

                                //rental
                                priceModel.IsRental = product.IsRental;

                                //pre-order
                                if (product.AvailableForPreOrder)
                                {
                                    priceModel.AvailableForPreOrder = !product.PreOrderAvailabilityStartDateTimeUtc.HasValue ||
                                        product.PreOrderAvailabilityStartDateTimeUtc.Value >= DateTime.UtcNow;
                                    priceModel.PreOrderAvailabilityStartDateTimeUtc = product.PreOrderAvailabilityStartDateTimeUtc;
                                }

                                //prices
                                if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                                {
                                    if (!product.CustomerEntersPrice)
                                    {
                                        if (product.CallForPrice)
                                        {
                                            //call for price
                                            priceModel.OldPrice = null;
                                            priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
                                        }
                                        else
                                        {
                                            //prices

                                            //calculate for the maximum quantity (in case if we have tier prices)
                                            decimal minPossiblePrice = _priceCalculationService.GetFinalPrice(product,
                                                _workContext.CurrentCustomer, decimal.Zero, true, int.MaxValue);

                                            decimal taxRate;
                                            decimal oldPriceBase = _taxService.GetProductPrice(product, product.OldPrice, out taxRate);
                                            decimal finalPriceBase = _taxService.GetProductPrice(product, minPossiblePrice, out taxRate);

                                            decimal oldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(oldPriceBase, _workContext.WorkingCurrency);
                                            decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

                                            //do we have tier prices configured?
                                            var tierPrices = new List<TierPrice>();
                                            if (product.HasTierPrices)
                                            {
                                                tierPrices.AddRange(product.TierPrices
                                                    .OrderBy(tp => tp.Quantity)
                                                    .ToList()
                                                    .FilterByStore(_storeContext.CurrentStore.Id)
                                                    .FilterForCustomer(_workContext.CurrentCustomer)
                                                    .RemoveDuplicatedQuantities());
                                            }
                                            //When there is just one tier (with  qty 1), 
                                            //there are no actual savings in the list.
                                            bool displayFromMessage = tierPrices.Count > 0 &&
                                                !(tierPrices.Count == 1 && tierPrices[0].Quantity <= 1);
                                            if (displayFromMessage)
                                            {
                                                priceModel.OldPrice = null;
                                                priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));
                                            }
                                            else
                                            {
                                                if (finalPriceBase != oldPriceBase && oldPriceBase != decimal.Zero)
                                                {
                                                    priceModel.OldPrice = _priceFormatter.FormatPrice(oldPrice);
                                                    priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
                                                }
                                                else
                                                {
                                                    priceModel.OldPrice = null;
                                                    priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
                                                }
                                            }
                                            if (product.IsRental)
                                            {
                                                //rental product
                                                priceModel.OldPrice = _priceFormatter.FormatRentalProductPeriod(product, priceModel.OldPrice);
                                                priceModel.Price = _priceFormatter.FormatRentalProductPeriod(product, priceModel.Price);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //hide prices
                                    priceModel.OldPrice = null;
                                    priceModel.Price = null;
                                }

                                #endregion
                            }
                            break;
                    }

                    model.ProductPrice = priceModel;

                    #endregion
                }

                //picture
                if (preparePictureModel)
                {
                    #region Prepare product picture

                    //If a size has been set in the view, we use it in priority
                    int pictureSize = productThumbPictureSize.HasValue ? productThumbPictureSize.Value : 200;
                    //prepare picture model
                    var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                    model.DefaultPictureModel = new PictureModel
                    {
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture)
                    };
                    //"title" attribute
                    model.DefaultPictureModel.Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute)) ?
                        picture.TitleAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), model.Name);
                    //"alt" attribute
                    model.DefaultPictureModel.AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute)) ?
                        picture.AltAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), model.Name);

                    #endregion
                }

                models.Add(model);
            }
            return models;
        }
        #endregion
    }
}