﻿using Nop.Core.Plugins;
using Nop.Plugin.Misc.HomePageProduct.Data;
using Nop.Plugin.Misc.HomePageProduct.Services;
using Nop.Services.Cms;
using Nop.Services.Localization;
using Nop.Web.Framework.Menu;
using System.Collections.Generic;
using System.Web.Routing;
using System.Linq;

namespace Nop.Plugin.Misc.HomePageProduct
{
    public class MiscHomePageProductPlugin : BasePlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly HomePageProductObjectContext _context;
        private readonly IHomePageProductService _biponeePreOrderService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctr

        public MiscHomePageProductPlugin(HomePageProductObjectContext context, IHomePageProductService preOrderService, ILocalizationService localizationService)
        {
            _context = context;
            _biponeePreOrderService = preOrderService;
            _localizationService = localizationService;
        }

        #endregion

        #region Install / Uninstall

        public override void Install()
        {
            //resource
            //this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.LivePersonChat.ButtonCode", "Button code(max 2000)");

            //install db
            _context.InstallSchema();

            //base install
            base.Install();
        }
        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            //_settingService.DeleteSetting<FroogleSettings>();

            //data
           _context.Uninstall();

           

            base.Uninstall();
        }

        #endregion

        #region Menu Builder

        public bool Authenticate()
        {
            return true;
        }
        #endregion

        //public void ManageSiteMap(SiteMapNode rootNode)
        //{
        //    var mainnode = new SiteMapNode()
        //    {
        //        Title = _localizationService.GetResource("Plugin"),
        //        Visible = true,
        //        Url = "~/Plugin/Misc/HomePageProduct/CategoryAdd",
        //        RouteValues = new RouteValueDictionary() { { "Area", "Admin" } }
        //    };
        //    rootNode.ChildNodes.Add(mainnode);
        //    var subnode = new SiteMapNode()
        //    {
        //        Title = _localizationService.GetResource("Misc.HomePageProduct.Menu.Text"),
        //        Visible = true,
        //        Url = "~/Plugin/Misc/HomePageProduct/CategoryAdd"
        //    };
        //    mainnode.ChildNodes.Add(subnode);
        //}



        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                SystemName = "Misc.HomePageProduct",
                Title = _localizationService.GetResource("Misc.HomePageProduct.Menu.Text"),
                Visible = true,
                Url = "~/Plugin/Misc/HomePageProduct/CategoryAdd",
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };
            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Third party plugins");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(menuItem);
            else
                rootNode.ChildNodes.Add(menuItem);
        }


    }
}