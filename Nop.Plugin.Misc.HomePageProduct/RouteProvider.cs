﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Misc.HomePageProduct
{
    public class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            #region Manage

           routes.MapRoute("Plugin.Misc.HomePageProduct.CategoryAdd", "Plugin/Misc/HomePageProduct/CategoryAdd",
           new
           {
               controller = "HomePageProduct",
               action = "AddCategory"
           },
           new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

           routes.MapRoute("Plugin.Misc.HomePageProduct.List", "Plugin/Misc/HomePageProduct/List/{CategoryId}",
            new
            {
                controller = "HomePageProduct",
                action = "List"
            },
            new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.HomePageProduct.CategoryImage", "Plugin/Misc/HomePageProduct/CategoryImage/{CategoryId}",
            new
            {
                controller = "HomePageProduct",
                action = "CategoryImage"
            },
            new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.HomePageProduct.SubCategoryList", "Plugin/Misc/HomePageProduct/SubCategoryList/{CategoryId}",
            new
            {
                controller = "HomePageProduct",
                action = "SubCategoryList"
            },
            new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");


            routes.MapRoute("Plugin.Misc.HomePageProduct.CategoryImageAdd", "Plugin/Misc/HomePageProduct/CategoryPictureAdd",
            new
            {
                controller = "HomePageProduct",
                action = "CategoryPictureAdd"
            },
            new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.HomePageProduct.UpdateCategoryColor", "Plugin/Misc/HomePageProduct/UpdateCategoryColor",
            new
            {
                controller = "HomePageProduct",
                action = "UpdateCategoryColor"
            },
            new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

            #endregion

        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }

    }
}