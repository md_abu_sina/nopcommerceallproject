﻿$(function () {
    // Declare a proxy to reference the hub.
    var announcement = $.connection.liveannouncementHub;
    var orderIdForStart = "";
    //debugger;
    // Create a function that the hub can call to broadcast messages.
    announcement.client.liveAnnouncement = function (accouncemant) {
        showannouncement(accouncemant);
        latestannouncement(accouncemant);
    };
    // Start the connection.
    $.connection.hub.start().done(function () {
    }).fail(function (e) {
        alert(e);
    });

});

function showannouncement(accouncemant) {
    var accouncemantMessageSet = localStorage.setItem("accouncemantMessage", accouncemant);
    if (accouncemant) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": 300,
            "hideDuration": 10000,
            "timeOut": 100000,
            "extendedTimeOut": 20000,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        tostView = '<div class="item accouncemantToast" style="position: relative;min-height: 75px;">' + accouncemant + '</div>'
        toastr["info"](tostView);
        $('.toast-info').css("background-color", "#008080");

        
        toastr.options.onclick = function () {
            $("html, body").animate(
           { scrollTop: 0 },
           1000);
        }

        $(".toast").click(function () {
            $("html, body").animate(
             { scrollTop: 0 },
             1000);
        });

        $(".toast-info").click(function () {
            $("html, body").animate(
             { scrollTop: 0 },
             1000);
        });

        toastr.options = {
            onclick: function () {
                $("html, body").animate(
                       { scrollTop: 0 },
                       1000);
            }
        }

        $(".accouncemantToast").live("click", function () {
            $("html, body").animate(
            { scrollTop: 0 },
            1000);
        });
    }
}



function latestannouncement(accouncemant) {
    $(".announcementPage").empty();
    $(".announcementPage").append(accouncemant);
};

