﻿using Nop.Plugin.Misc.LiveAnnouncement.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Misc.LiveAnnouncement.Data
{
    public class AnnouncementMap : EntityTypeConfiguration<AnnouncementDomain>
    {
        public AnnouncementMap()
        {
            ToTable("Announcement");

            HasKey(x => x.Id);
            Property(x => x.Name);
            Property(x => x.Body);
            Property(x => x.ShowingToDate);
            Property(x => x.IsActive);
            Property(x => x.CreateDate);
        }
    }
}
