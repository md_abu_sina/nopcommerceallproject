using System;
using Nop.Core;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.LiveAnnouncement.Domain
{
    public class AnnouncementDomain : BaseEntity
    {
        public string Name { get; set; }
        public string Body { get; set; }

        public string ShowingToDate { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreateDate { get; set; }

    }
}