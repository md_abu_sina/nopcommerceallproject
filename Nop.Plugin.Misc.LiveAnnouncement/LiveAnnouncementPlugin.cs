﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Nop.Core.Plugins;
using Nop.Plugin.Misc.LiveAnnouncement.Data;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework.Menu;
using Owin;
using System;
using System.Collections.Generic;
using System.Web.Routing;
using System.Linq;

namespace Nop.Plugin.Misc.LiveAnnouncement
{
    public class LiveAnnouncementPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly LiveAnnouncementObjectContext _context;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingContext;


        private const string HeaderWidget = "header";

        #endregion


        #region Ctr

        public LiveAnnouncementPlugin(LiveAnnouncementObjectContext context, ILocalizationService localizationService, ISettingService settingContext)
        {
            _context = context;
            _localizationService = localizationService;
            _settingContext = settingContext;
        }

        #endregion

        #region Install / Uninstall


        public override void Install()
        {
            this.AddOrUpdatePluginLocaleResource("misc.announcement", "Announcement Create");


            //resource
            //this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.LivePersonChat.ButtonCode", "Button code(max 2000)");

            //install db
            _context.InstallSchema();

            //base install
            base.Install();
        }
        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings

            //data
           _context.Uninstall();
           this.DeletePluginLocaleResource("misc.announcement");
           

            base.Uninstall();
        }

        #endregion

        #region Menu Builder

        public bool Authenticate()
        {
            return true;
        }

        #endregion


        //public void ManageSiteMap(SiteMapNode rootNode)
        //{
        //    var mainnode = new SiteMapNode()
        //    {
        //        Title = _localizationService.GetResource("Plugin"),
        //        Visible = true,
        //        RouteValues = new RouteValueDictionary() { { "area", "Admin" } }
        //    };
        //    rootNode.ChildNodes.Add(mainnode);
        //    var subnode = new SiteMapNode()
        //    {
        //        Title = _localizationService.GetResource("Misc.ToastDesign"),
        //        Visible = true,
        //        Url = "~/LiveAnnouncement/ToastDesign",
        //        RouteValues = new RouteValueDictionary() { { "area", "Admin" } }
        //    };
        //    mainnode.ChildNodes.Add(subnode);
        //}


        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                SystemName = "LiveAnnouncement",
                Title = _localizationService.GetResource("Misc.Announcement"),
                Visible = true,
                Url = "~/LiveAnnouncement/Announcement",
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };
            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Third party plugins");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(menuItem);
            else
                rootNode.ChildNodes.Add(menuItem);
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>
            { 
               // "body_end_html_tag_before"
               HeaderWidget
            };
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "LiveAnnouncement";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Misc.LiveAnnouncement.Controllers" }, { "area", null } };
        }

        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "LiveAnnouncementView";
                controllerName = "LiveAnnouncement";
                routeValues = new RouteValueDictionary
                {
                    {"Namespaces", "Nop.Plugin.Misc.LiveAnnouncement.Controllers"},
                    {"area", null},
                    {"widgetZone", widgetZone}
                };

        }
    }
}