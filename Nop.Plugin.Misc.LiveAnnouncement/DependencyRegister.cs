﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Microsoft.Owin.Builder;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Misc.LiveAnnouncement.Data;
using Nop.Plugin.Misc.LiveAnnouncement.Domain;
using Nop.Plugin.Misc.LiveAnnouncement.Services;
using Owin;

namespace Nop.Plugin.Misc.LiveAnnouncement
{
    public partial class DependencyRegister : IDependencyRegistrar
    {
        #region Field

        private const string ContextName = "nop_object_context_live_announcement";

        #endregion

        #region Register

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            //Load custom data settings
            var dataSettingsManager = new DataSettingsManager();
            var dataSettings = dataSettingsManager.LoadSettings();

            //Register custom object context
            builder.Register<IDbContext>(c => RegisterIDbContext(c, dataSettings)).Named<IDbContext>(ContextName).InstancePerHttpRequest();
            builder.Register(c => RegisterIDbContext(c, dataSettings)).InstancePerHttpRequest();

            //Register services

            //builder.RegisterType<LiveAnnouncementService>().As<ILiveAnnouncementService>();


            builder.RegisterType<AnnouncementService>().As<IAnnouncementService>();
            //builder.RegisterType<AppBuilder>().As<IAppBuilder>();

            
            //Override the repository injection


            builder.RegisterType<EfRepository<AnnouncementDomain>>().As<IRepository<AnnouncementDomain>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(ContextName)).InstancePerHttpRequest();


        }

        #endregion

        #region DB

        public int Order
        {
            get { return 0; }
        }

        private LiveAnnouncementObjectContext RegisterIDbContext(IComponentContext componentContext,
                                                                DataSettings dataSettings)
        {
            string dataConnectionStrings;

            if (dataSettings != null && dataSettings.IsValid())
            {
                dataConnectionStrings = dataSettings.DataConnectionString;
            }
            else
            {
                dataConnectionStrings = componentContext.Resolve<DataSettings>().DataConnectionString;
            }

            return new LiveAnnouncementObjectContext(dataConnectionStrings);
        }

        #endregion

       
    }
}