﻿using System;
using Nop.Core;
using Nop.Plugin.Misc.LiveAnnouncement.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.LiveAnnouncement.Services
{
    public partial interface IAnnouncementService
    {
        void Delete(int Id);
        void Insert(AnnouncementDomain item);
        bool Update(AnnouncementDomain AnnouncementDomain);
        IPagedList<AnnouncementDomain> GetAnnouncementDomain(int pageIndex = 0, int pageSize = int.MaxValue);
        AnnouncementDomain GetAnnouncementDesignFirst();
        AnnouncementDomain GetAnnouncementById(int Id);
    }
}