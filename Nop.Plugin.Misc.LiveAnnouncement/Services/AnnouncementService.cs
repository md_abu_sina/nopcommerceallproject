﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.LiveAnnouncement.Domain;
using System.Collections.Generic;
using Nop.Services.Events;

namespace Nop.Plugin.Misc.LiveAnnouncement.Services
{
    public partial class AnnouncementService : IAnnouncementService
    {
        #region Field
        private readonly IRepository<AnnouncementDomain> _announcementDomainRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion

        #region Ctr

        public AnnouncementService(IRepository<AnnouncementDomain> announcementDomainRepository, IEventPublisher eventPublisher)
        {
            _announcementDomainRepository = announcementDomainRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        public void Delete(int Id)
        {
            //item.Deleted = true;

            var query = from c in _announcementDomainRepository.Table
                        where c.Id == Id
            select c;
            var announcementList = query.ToList();
            foreach (var announcement in announcementList)
            {
                _announcementDomainRepository.Delete(announcement);
            }
        }


        public bool Update(AnnouncementDomain AnnouncementDomain)
        {
            if (AnnouncementDomain == null)
                throw new ArgumentNullException("customer");

            _announcementDomainRepository.Update(AnnouncementDomain);
            return true;
        }


        public void Insert(AnnouncementDomain item)
        {
            //default value

            _announcementDomainRepository.Insert(item);
        }

        public IPagedList<AnnouncementDomain> GetAnnouncementDomain(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from c in _announcementDomainRepository.Table
                        select c;


            query = query.OrderBy(b => b.IsActive);

            var liveAnnouncementDomain = new PagedList<AnnouncementDomain>(query, pageIndex, pageSize);
            return liveAnnouncementDomain;
        }

        public AnnouncementDomain GetAnnouncementDesignFirst()
        {
            var query = from c in _announcementDomainRepository.Table
                        where c.IsActive==true
                        orderby c.CreateDate descending
                        select c;
            var LatestAnnouncement = query.ToList().FirstOrDefault();
            return LatestAnnouncement;
        }



        public AnnouncementDomain GetAnnouncementById(int Id)
        {
            var query = from c in _announcementDomainRepository.Table
                        where c.Id == Id
                        select c;
            return new AnnouncementDomain
                {
                    Id = query.ToList().FirstOrDefault().Id,
                    Name = query.ToList().FirstOrDefault().Name,
                    Body = query.ToList().FirstOrDefault().Body,
                    IsActive = query.ToList().FirstOrDefault().IsActive,
                    ShowingToDate = query.ToList().FirstOrDefault().ShowingToDate

                };
        }
        #endregion    
    

   

    }
}