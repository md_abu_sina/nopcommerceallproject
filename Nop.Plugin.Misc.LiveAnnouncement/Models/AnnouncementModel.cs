﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Localization;

namespace Nop.Plugin.Misc.LiveAnnouncement.Models
{
    public partial class AnnouncementModel : BaseNopEntityModel, ILocalizedModelLocal
    {
        [NopResourceDisplayName("Name")]
        public string Name { get; set; }


        [NopResourceDisplayName("Body")]
        [AllowHtml]
        public string Body { get; set; }

        public bool IsActive { get; set; }
        public string ShowingToDate { get; set; }


        public int LanguageId{ get; set; }
        public bool UpdateAnnouncement { get; set; }
    }
}