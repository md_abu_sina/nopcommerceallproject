﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.LiveAnnouncement
{
    [HubName("liveannouncementHub")]
    public class LiveAnnouncementHub : Hub
    {
        [HubMethodName("liveAnnouncement")]
        public static void liveAnnouncement(string accouncemant)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<LiveAnnouncementHub>();
            context.Clients.All.liveAnnouncement(accouncemant);
        }
    }
}
