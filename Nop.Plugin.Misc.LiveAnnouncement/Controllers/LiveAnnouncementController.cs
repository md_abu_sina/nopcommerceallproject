﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Seo;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using System.Collections.Generic;
using Nop.Services.Media;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;
using Nop.Web.Infrastructure.Cache;
using Nop.Core.Caching;
using System.Diagnostics;
using Nop.Services.Security;
using Nop.Services.Tax;
using Nop.Services.Directory;
using System.IO;
using Nop.Plugin.Misc.LiveAnnouncement.Services;
using Nop.Plugin.Misc.LiveAnnouncement.Models;
using Nop.Services.Orders;
using Nop.Plugin.Misc.LiveAnnouncement.Domain;
using Nop.Web.Models.Common;
using Nop.Admin.Models.Catalog;
using Nop.Services.Configuration;
using Nop.Plugin.LiveAnnouncement;

namespace Nop.Plugin.Misc.LiveAnnouncement.Controllers
{

    public class LiveAnnouncementController : Controller
    {
        #region Field

        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IWorkContext _workContext;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductService _productService;
        private readonly ILocalizationService _localizationService;
        private readonly ICategoryService _categoryService;
        private readonly IPictureService _pictureService;
        private readonly IAnnouncementService _announcementService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IPermissionService _permissionService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderService _orderService;
        private readonly ISettingService _settingContext;


        #endregion

        #region Ctr

        public LiveAnnouncementController(IDateTimeHelper dateTimeHelper,
            IWorkContext workContext,
            CatalogSettings catalogSettings,
            IProductService productService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            ICategoryService categoryService,
            IAnnouncementService announcementService,
            IWebHelper webHelper,
            IStoreContext storeContext,
            ICacheManager cacheManager,
            IPermissionService permissionService,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IOrderService orderService,
            ISettingService settingContext)
        {
            _dateTimeHelper = dateTimeHelper;
            _workContext = workContext;
            _catalogSettings = catalogSettings;
            _productService = productService;
            _localizationService = localizationService;
            _categoryService = categoryService;
            _productService = productService;
            _pictureService = pictureService;
            _announcementService = announcementService;
            _webHelper = webHelper;
            _storeContext = storeContext;
            _cacheManager = cacheManager;
            _permissionService = permissionService;
            _priceCalculationService = priceCalculationService;
            _taxService = taxService;
            _currencyService = currencyService;
            _priceFormatter = priceFormatter;
            _orderService = orderService;
            _settingContext = settingContext;
        }

        #endregion

        #region Methods

        public ActionResult LiveAnnouncementView(int page = 1)
        {
            var modelToday = new AnnouncementHomePageModel();
            return View("~/Plugins/LiveAnnouncement/Views/LiveAnnouncementView/LiveAnnouncement.cshtml", modelToday);
        }

        

        public ActionResult Announcement()
        {
            //ToastDesignInsert();
            var model = new AnnouncementModel();
            return View("~/Plugins/LiveAnnouncement/Views/LiveAnnouncementView/Announcement.cshtml", model);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Announcement(AnnouncementModel model)
        {
            AnnouncementDomain objOfAnnouncementDomain = new AnnouncementDomain();
            objOfAnnouncementDomain.Name=model.Name;
            objOfAnnouncementDomain.Body=model.Body;
            objOfAnnouncementDomain.IsActive=model.IsActive;
            objOfAnnouncementDomain.ShowingToDate=model.ShowingToDate;
            objOfAnnouncementDomain.CreateDate = DateTime.UtcNow;
            _announcementService.Insert(objOfAnnouncementDomain);

            if (model.IsActive==true)
            {
                LiveAnnouncementHub.liveAnnouncement(model.Body.ToString());
            
            }

            return RedirectToAction("AnnouncementList");
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(AnnouncementModel model)
        {
            AnnouncementDomain objOfAnnouncementDomain = new AnnouncementDomain();

            _announcementService.Delete(model.Id);

            objOfAnnouncementDomain.Id = model.Id;
            objOfAnnouncementDomain.Name = model.Name;
            objOfAnnouncementDomain.Body = model.Body;
            objOfAnnouncementDomain.IsActive = model.IsActive;
            objOfAnnouncementDomain.ShowingToDate = model.ShowingToDate;
            objOfAnnouncementDomain.CreateDate = DateTime.UtcNow;
            //objOfAnnouncementDomain.IsActive = model.ExtendedTimeOut;

            //objOfAnnouncementDomain.CreatedOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");
            //objOfAnnouncementDomain.UpdateOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");

            _announcementService.Insert(objOfAnnouncementDomain);

            if (model.IsActive == true)
            {
                LiveAnnouncementHub.liveAnnouncement(model.Body.ToString());

            }
                return RedirectToAction("AnnouncementList");
        }

        public ActionResult Edit(int Id)
        {
            var singleAnnouncement = _announcementService.GetAnnouncementById(Id);
            //return Json(new
            //{
            //    Id = singleAnnouncement.Id,
            //    Name = singleAnnouncement.Name,
            //    Body = singleAnnouncement.Body,
            //    IsActive = singleAnnouncement.IsActive,
            //    ShowingToDate = singleAnnouncement.ShowingToDate
            //});

            var model = new AnnouncementModel();
            model.Id = singleAnnouncement.Id;
            model.Name = singleAnnouncement.Name;
            model.Body = singleAnnouncement.Body;
            model.IsActive = singleAnnouncement.IsActive;
            model.ShowingToDate = singleAnnouncement.ShowingToDate;
            model.UpdateAnnouncement = true;
            return View("~/Plugins/LiveAnnouncement/Views/LiveAnnouncementView/Announcement.cshtml", model);

        }


        public ActionResult AnnouncementList()
        {
            //ToastDesignInsert();
            var model = new AnnouncementModel();
            return View("~/Plugins/LiveAnnouncement/Views/LiveAnnouncementView/AnnouncementList.cshtml", model);
        }

        [HttpPost]
        public ActionResult GetAnnouncement()
        {
            //ToastDesignInsert();
            var latestAnnouncement = _announcementService.GetAnnouncementDesignFirst();
            if (latestAnnouncement != null)
            {
                return Json(new
                {
                    success = true,
                    announcement = latestAnnouncement.Body
                });
            }
            else
            {
                return Json(new
                {
                    success = false
                    
                });
            }
        }

        [HttpPost]
        public ActionResult AnnouncementList(DataSourceRequest command, AnnouncementModel model)
        {

            var announcementPagedList = _announcementService.GetAnnouncementDomain(pageIndex: command.Page - 1, pageSize: command.PageSize);
            var gridModel = new DataSourceResult();
            gridModel.Data = announcementPagedList.Select(x =>
            {
                return new AnnouncementModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Body = x.Body,
                    IsActive = x.IsActive,
                    ShowingToDate = x.ShowingToDate
                };
            });
            gridModel.Total = announcementPagedList.TotalCount;
            return Json(gridModel);
        }
        #endregion

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            return View("~/Plugins/LiveAnnouncement/Views/LiveAnnouncementView/Configure.cshtml");
        }
    }
}