﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;
using Owin;
using Nop.Plugin.LiveAnnouncement;

namespace Nop.Plugin.Misc.LiveAnnouncement
{
    public class RouteProvider : IRouteProvider
    {
        public IAppBuilder _appBuilderService { get; set; }
        public void RegisterRoutes(RouteCollection routes)
        {
            RouteTable.Routes.MapOwinPath("/signalr", _appBuilderService => _appBuilderService.RunSignalR());

            //routes.MapRoute("Nop.Plugin.Misc.LiveAnnouncement.TotalOrderedProduct",
            //             "TotalOrderedProduct",
            //            new { controller = "LiveAnnouncement", action = "GetAllOrderedProduct" },
            //            new[] { "Nop.Plugin.Misc.LiveAnnouncement.Controllers" });
           // #region Manage

           //routes.MapRoute("Nop.Plugin.Misc.LiveAnnouncement.ToastDesing", "Plugin/Misc/LiveAnnouncement/ToastDesign",
           //new
           //{
           //    controller = "LiveAnnouncement",
           //    action = "AddCategory"
           //},
           //new[] { "Nop.Plugin.Misc.LiveAnnouncement.Controllers" }).DataTokens.Add("area", "admin");

           //routes.MapRoute("Plugin.Misc.HomePageProduct.List", "Plugin/Misc/HomePageProduct/List/{CategoryId}",
           // new
           // {
           //     controller = "HomePageProduct",
           //     action = "List"
           // },
           // new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

           // routes.MapRoute("Plugin.Misc.HomePageProduct.CategoryImage", "Plugin/Misc/HomePageProduct/CategoryImage/{CategoryId}",
           // new
           // {
           //     controller = "HomePageProduct",
           //     action = "CategoryImage"
           // },
           // new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

           // routes.MapRoute("Plugin.Misc.HomePageProduct.SubCategoryList", "Plugin/Misc/HomePageProduct/SubCategoryList/{CategoryId}",
           // new
           // {
           //     controller = "HomePageProduct",
           //     action = "SubCategoryList"
           // },
           // new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");


           // routes.MapRoute("Plugin.Misc.HomePageProduct.CategoryImageAdd", "Plugin/Misc/HomePageProduct/CategoryPictureAdd",
           // new
           // {
           //     controller = "HomePageProduct",
           //     action = "CategoryPictureAdd"
           // },
           // new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

           // routes.MapRoute("Plugin.Misc.HomePageProduct.UpdateCategoryColor", "Plugin/Misc/HomePageProduct/UpdateCategoryColor",
           // new
           // {
           //     controller = "HomePageProduct",
           //     action = "UpdateCategoryColor"
           // },
           // new[] { "Nop.Plugin.Misc.HomePageProduct.Controllers" }).DataTokens.Add("area", "admin");

           // #endregion

        }
        public int Priority
        {
            get
            {
                return 100;
            }
        }

    }
}