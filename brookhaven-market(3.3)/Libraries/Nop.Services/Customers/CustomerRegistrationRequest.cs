﻿using Nop.Core.Domain.Customers;

namespace Nop.Services.Customers
{
    public class CustomerRegistrationRequest
    {
        public Customer Customer { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public PasswordFormat PasswordFormat { get; set; }
        public bool IsApproved { get; set; }
				public string customerRoleId { get; set; }//by Iffat

        public CustomerRegistrationRequest(Customer customer, string email, string username,
            string password, 
            PasswordFormat passwordFormat,
            bool isApproved = true,
						string customerRoleId = null)//customer role id by Iffat
        {
            this.Customer = customer;
            this.Email = email;
            this.Username = username;
            this.Password = password;
            this.PasswordFormat = passwordFormat;
            this.IsApproved = isApproved;
						this.customerRoleId = customerRoleId; //by iffat
        }

        //public bool IsValid  
        //{
        //    get 
        //    {
        //        return (!CommonHelper.AreNullOrEmpty(
        //                    this.Email,
        //                    this.Password
        //                    ));
        //    }
        //}
    }
}
