﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Themes;
namespace Nop.Plugin.Other.CustomTopic.ViewEngines
{
    class CustomViewEngine : ThemeableRazorViewEngine 
    {
        public  CustomViewEngine()
        {
            var newLocationFormat = "~/Plugins/Other.CustomTopic/Views/{0}.cshtml";
            List<string> allLocationFormats = ViewLocationFormats.OfType<string>().ToList();
            allLocationFormats.Insert(0, newLocationFormat);
            ViewLocationFormats = allLocationFormats.ToArray();


            var newPartialLocationFormat = "~/Plugins/Other.CustomTopic/Views/{0}.cshtml";
            List<string> allPartialLocationFormats = PartialViewLocationFormats.OfType<string>().ToList();
            allPartialLocationFormats.Insert(0, newPartialLocationFormat);
            PartialViewLocationFormats = allPartialLocationFormats.ToArray();



            
        }
    }
}
