﻿using Nop.Core.Plugins;

using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using Nop.Core.Data;
using Nop.Web.Framework.Menu;
using Nop.Services.Events;
using Nop.Core.Events;


namespace Nop.Plugin.Other.CustomTopic
{
		public class CustomTopicPlugin : BasePlugin, IAdminMenuPlugin
    {
       
  
        public override void Install()
        {
            
          
            base.Install();
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Other.CustomTopics.Fields.LayoutPath", "Give Layout Path");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Other.CustomTopics.Fields.LayoutPath.Required", "Layout Path Required");


        }
        public SiteMapNode BuildMenuItem()
        {
            SiteMapNode node = new SiteMapNode
            {
                Visible = true,
                Title = "Toipcs",
                Url = "/Admin/Plugin/Other/CustomTopic/List"
            };

            


            return node;
        }
        public override void Uninstall()
        {
            this.DeletePluginLocaleResource("Nop.Plugin.Other.CustomTopics.Fields.LayoutPath");
            this.DeletePluginLocaleResource("Nop.Plugin.Other.CustomTopics.Fields.LayoutPath.Required");
            base.Uninstall();
        }


				public bool Authenticate()
				{
					return true;
				}

       
    }
}



/*public class GiftsPlugin : BasePlugin, IConsumer<EntityUpdated<ShoppingCartItem>>, IConsumer<EntityInserted<ShoppingCartItem>>, IConsumer<EntityDeleted<ShoppingCartItem>>
{

	public void HandleEvent(EntityUpdated<ShoppingCartItem> eventMessage)
	{
		//using eventMessage.Entity
	}

	public void HandleEvent(EntityInserted<ShoppingCartItem> eventMessage)
	{
		//using eventMessage.Entity
	}

	public void HandleEvent(EntityDeleted<ShoppingCartItem> eventMessage)
	{
		//using eventMessage.Entity
	}

}*/