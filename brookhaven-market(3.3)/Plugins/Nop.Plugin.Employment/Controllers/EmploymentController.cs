﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Employment.Models;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Employment.Controllers
{
		public class EmploymentController : BasePluginController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly PdfSettings _pdfSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
				private readonly IStoreContext _storeContext;
				private readonly ILogger _logger;

        #region email fields


        private readonly ITokenizer _tokenizer;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ILanguageService _languageService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IMessageTokenProvider _messageTokenProvider;
        // private readonly IMessageTemplateForUponService _messageTemplateForUponService;
        private readonly IRepository<MessageTemplate> _messageTemplateRepository;
        private readonly IRepository<EmailAccount> _emailAccountRepository;
        //private readonly IEventPublisher _eventPublisher;

        #endregion



        #endregion

        #region ctor

        public EmploymentController(IWorkContext workContext, ITokenizer tokenizer, IQueuedEmailService queuedEmailService, IMessageTemplateService messageTemplateService, StoreInformationSettings storeInformationSettings,
                                                                ILanguageService languageService, IEmailAccountService emailAccountService, EmailAccountSettings emailAccountSettings, IEmailSender emailSender,
                                                                IMessageTokenProvider messageTokenProvider, RewardPointsSettings rewardPointsSettings, CustomerSettings customerSettings, ILogger logger,
                                                                ForumSettings forumSettings, OrderSettings orderSettings, IAddressService addressService, IOrderService orderService, PdfSettings pdfSettings,
																																IRepository<MessageTemplate> messageTemplateRepository, IRepository<EmailAccount> emailAccountRepository, IStoreContext storeContext)
        {
            _workContext = workContext;
            _pdfSettings = pdfSettings;
            _storeInformationSettings = storeInformationSettings;
            _emailSender = emailSender;
            _tokenizer = tokenizer;
            _queuedEmailService = queuedEmailService;
            _messageTemplateService = messageTemplateService;
            _languageService = languageService;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
            _messageTokenProvider = messageTokenProvider;
            //  _messageTemplateForUponService = messageTemplateForUponService;
            _messageTemplateRepository = messageTemplateRepository;
            _emailAccountRepository = emailAccountRepository;
						_storeContext = storeContext;
						_logger = logger;
        }

        #endregion

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Shows the form.
				/// </summary>
				/// <returns></returns>
        public ActionResult ShowForm()
        {
            return View("EmploymentForm");
        }



        #region Email

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Submits the form.
				/// </summary>
				/// <param name="employmentModel">The employment model.</param>
				/// <returns></returns>
        [HttpPost]
        public ActionResult SubmitForm(EmploymentModel employmentModel)
        {
            employmentModel.StoreName = _storeContext.CurrentStore.Name;

            var languageId = EnsureLanguageIsActive(_workContext.WorkingLanguage.Id);

            var messageTemplate = GetLocalizedActiveMessageTemplate("Employment.EmailCv", languageId);

            var randomNumber = Guid.NewGuid();
            string fileName = string.Format("employmentform_{0}-{1}-{2}_" + randomNumber + ".html", employmentModel.FirstName, employmentModel.MiddleName, employmentModel.LastName);//employmentform_Andrea-Montoya_485112
            string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\employment", fileName);

            //var emailBody = GetEmailBody(messageTemplate);

            //var tokens = GenerateTokens(employmentModel);
            //var bodyReplaced = _tokenizer.Replace(emailBody, tokens, true);

            var bodyReplaced = RenderPartialViewToString(this, "Attached", employmentModel);


            EmploymentHtml.ConvertToHtml(bodyReplaced, filePath);

            //EmploymentPdf.PrintToPdf(employmentModel, _workContext.WorkingLanguage, bodyReplaced, filePath, _pdfSettings, this.Request.PhysicalApplicationPath);
						try
						{
							SendEmail(_workContext.WorkingLanguage.Id, employmentModel, filePath);
							return Redirect( _storeContext.CurrentStore.Url + "t/EmploymentApplicationCompletion");
							//return("Email send successfuly");
						}
						
						catch(Exception ex)
						{
							//string errorText = ex.ToString().Substring(0, Math.Min(ex.ToString().Length, 200));
							_logger.Error(ex.ToString());
							return Content(ex.ToString());
							//return Redirect(_storeContext.CurrentStore.Url + "t/EmploymentApplicationCompletion");
							//throw new Exception(ex); //wex.ToString();
							//return Redirect("http://www.brookhavenmarket.com/t/EmploymentApplicationCompletion");
						}
            //return View("EmploymentForm");
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Renders the partial view to string.
				/// </summary>
				/// <param name="controller">The controller.</param>
				/// <param name="viewName">Name of the view.</param>
				/// <param name="model">The model.</param>
				/// <returns></returns>
        public static string RenderPartialViewToString(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Installs the employment message templates.
				/// </summary>
        public virtual void installEmploymentMessageTemplates()
        {

						var eaGeneral = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Employment contact")).FirstOrDefault();
            //var eaSale = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Sales representative")).FirstOrDefault();
            //var eaCustomer = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Customer support")).FirstOrDefault();
            var messageTemplates = new List<MessageTemplate>
                               {
                                   new MessageTemplate
                                       {
                                           Name = "Employment.EmailCv",
                                           Subject = "%EmailCv.FirstName%. Job Application.",
                                           Body = "<p>An email sent \"%EmailCv.FirstName%\".</p>",
                                           IsActive = true,
                                           EmailAccountId = eaGeneral.Id,
                                       },
                                  
                                  
                               };
            messageTemplates.ForEach(mt => _messageTemplateRepository.Insert(mt));
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Adds the email cv tokens.
				/// </summary>
				/// <param name="tokens">The tokens.</param>
				/// <param name="employmentModel">The employment model.</param>
        public virtual void AddEmailCvTokens(IList<Token> tokens, EmploymentModel employmentModel)
        {
            //personal info
            tokens.Add(new Token("EmailCv.FirstName", employmentModel.FirstName));
            tokens.Add(new Token("EmailCv.LastName", employmentModel.LastName));
            tokens.Add(new Token("EmailCv.MiddleName", employmentModel.MiddleName));

            tokens.Add(new Token("EmailCv.Street", employmentModel.Street));
            tokens.Add(new Token("EmailCv.City", employmentModel.City));
            tokens.Add(new Token("EmailCv.StateZip", employmentModel.StateZip));
            tokens.Add(new Token("EmailCv.HomePhone", employmentModel.HomePhone));
            tokens.Add(new Token("EmailCv.Cell", employmentModel.Cell));
            tokens.Add(new Token("EmailCv.SS", employmentModel.SS));

            tokens.Add(new Token("EmailCv.Age16", employmentModel.Age16));
            tokens.Add(new Token("EmailCv.Age18", employmentModel.Age18));
            tokens.Add(new Token("EmailCv.UsaWorkPermit", employmentModel.UsaWorkPermit));
            tokens.Add(new Token("EmailCv.UsaWorkPermitProof", employmentModel.UsaWorkPermitProof));
            tokens.Add(new Token("EmailCv.HiringNewsSource", employmentModel.HiringNewsSource));

            //Employment Desired
            tokens.Add(new Token("EmailCv.Position", employmentModel.Position));
            tokens.Add(new Token("EmailCv.StartDate", employmentModel.StartDate));
            tokens.Add(new Token("EmailCv.Salary", employmentModel.Salary));
            tokens.Add(new Token("EmailCv.Store", employmentModel.Store));
            tokens.Add(new Token("EmailCv.Department", employmentModel.Department));
            tokens.Add(new Token("EmailCv.AvailableSunday", employmentModel.AvailableSunday));
            tokens.Add(new Token("EmailCv.AvailableMonday", employmentModel.AvailableMonday));
            tokens.Add(new Token("EmailCv.AvailableTuesday", employmentModel.AvailableTuesday));
            tokens.Add(new Token("EmailCv.AvailableWednesday", employmentModel.AvailableWednesday));
            tokens.Add(new Token("EmailCv.AvailableThursday", employmentModel.AvailableThursday));
            tokens.Add(new Token("EmailCv.AvailableFriday", employmentModel.AvailableFriday));
            tokens.Add(new Token("EmailCv.AvailableSaturday", employmentModel.AvailableSaturday));
            tokens.Add(new Token("EmailCv.MaxWorkingHour", employmentModel.MaxWorkingHour));
            tokens.Add(new Token("EmailCv.PreferWorkingHour", employmentModel.PreferWorkingHour));
            tokens.Add(new Token("EmailCv.OtherLanguage", employmentModel.OtherLanguage));
            tokens.Add(new Token("EmailCv.PreferNotAnswer", employmentModel.PreferNotAnswer));
            tokens.Add(new Token("EmailCv.AppliedBefore", employmentModel.AppliedBefore));
            tokens.Add(new Token("EmailCv.Where", employmentModel.Where));
            tokens.Add(new Token("EmailCv.When", employmentModel.When));

            //Education History
            tokens.Add(new Token("EmailCv.HighSchoolName", employmentModel.HighSchoolName));
            tokens.Add(new Token("EmailCv.CollegeName", employmentModel.CollegeName));
            tokens.Add(new Token("EmailCv.OtherName", employmentModel.OtherName));
            tokens.Add(new Token("EmailCv.HighSchoolComplete", employmentModel.HighSchoolComplete));
            tokens.Add(new Token("EmailCv.CollegeComplete", employmentModel.CollegeComplete));
            tokens.Add(new Token("EmailCv.OtherComplete", employmentModel.OtherComplete));
            tokens.Add(new Token("EmailCv.HighSchoolStatus", employmentModel.HighSchoolStatus));
            tokens.Add(new Token("EmailCv.CollegeStatus", employmentModel.CollegeStatus));
            tokens.Add(new Token("EmailCv.OtherStatus", employmentModel.OtherStatus));
            tokens.Add(new Token("EmailCv.HighSchoolSubject", employmentModel.HighSchoolSubject));
            tokens.Add(new Token("EmailCv.CollegeSubject", employmentModel.CollegeSubject));
            tokens.Add(new Token("EmailCv.OtherSubject", employmentModel.OtherSubject));

            //Employment Experience
            tokens.Add(new Token("EmailCv.CurrentlyEmployed", employmentModel.CurrentlyEmployed));
            tokens.Add(new Token("EmailCv.ContactCurrentEmployerPermission", employmentModel.ContactCurrentEmployerPermission));
            tokens.Add(new Token("EmailCv.FromMonthYear1", employmentModel.FromMonthYear1));
            tokens.Add(new Token("EmailCv.ToMonthYear1", employmentModel.ToMonthYear1));
            tokens.Add(new Token("EmailCv.Employer1", employmentModel.Employer1));
            tokens.Add(new Token("EmailCv.Salary1", employmentModel.Salary1));
            tokens.Add(new Token("EmailCv.Position1", employmentModel.Position1));
            tokens.Add(new Token("EmailCv.LeavingReason1", employmentModel.LeavingReason1));
            tokens.Add(new Token("EmailCv.FromMonthYear2", employmentModel.FromMonthYear2));
            tokens.Add(new Token("EmailCv.ToMonthYear2", employmentModel.ToMonthYear2));
            tokens.Add(new Token("EmailCv.Employer2", employmentModel.Employer2));
            tokens.Add(new Token("EmailCv.Salary2", employmentModel.Salary2));
            tokens.Add(new Token("EmailCv.Position2", employmentModel.Position2));
            tokens.Add(new Token("EmailCv.LeavingReason2", employmentModel.LeavingReason2));
            tokens.Add(new Token("EmailCv.FromMonthYear3", employmentModel.FromMonthYear3));
            tokens.Add(new Token("EmailCv.ToMonthYear3", employmentModel.ToMonthYear3));
            tokens.Add(new Token("EmailCv.Employer3", employmentModel.Employer3));
            tokens.Add(new Token("EmailCv.Salary3", employmentModel.Salary3));
            tokens.Add(new Token("EmailCv.Position3", employmentModel.Position3));
            tokens.Add(new Token("EmailCv.LeavingReason3", employmentModel.LeavingReason3));
            tokens.Add(new Token("EmailCv.FromMonthYear4", employmentModel.FromMonthYear4));
            tokens.Add(new Token("EmailCv.ToMonthYear4", employmentModel.ToMonthYear4));
            tokens.Add(new Token("EmailCv.Employer4", employmentModel.Employer4));
            tokens.Add(new Token("EmailCv.Salary4", employmentModel.Salary4));
            tokens.Add(new Token("EmailCv.Position4", employmentModel.Position4));
            tokens.Add(new Token("EmailCv.LeavingReason4", employmentModel.LeavingReason4));


            //GENERAL INFORMATION
            tokens.Add(new Token("EmailCv.SpecialStudy", employmentModel.SpecialStudy));
            tokens.Add(new Token("EmailCv.SpecialTraining", employmentModel.SpecialTraining));

            //REFERENCES
            tokens.Add(new Token("EmailCv.Name1", employmentModel.Name1));
            tokens.Add(new Token("EmailCv.Address1", employmentModel.Address1));
            tokens.Add(new Token("EmailCv.PhoneNumber1", employmentModel.PhoneNumber1));
            tokens.Add(new Token("EmailCv.Business1", employmentModel.Business1));
            tokens.Add(new Token("EmailCv.Relationship1", employmentModel.Relationship1));
            tokens.Add(new Token("EmailCv.Name2", employmentModel.Name2));
            tokens.Add(new Token("EmailCv.Address2", employmentModel.Address2));
            tokens.Add(new Token("EmailCv.PhoneNumber2", employmentModel.PhoneNumber2));
            tokens.Add(new Token("EmailCv.Business2", employmentModel.Business2));
            tokens.Add(new Token("EmailCv.Relationship2", employmentModel.Relationship2));
            tokens.Add(new Token("EmailCv.Name3", employmentModel.Name3));
            tokens.Add(new Token("EmailCv.Address3", employmentModel.Address3));
            tokens.Add(new Token("EmailCv.PhoneNumber3", employmentModel.PhoneNumber3));
            tokens.Add(new Token("EmailCv.Business3", employmentModel.Business3));
            tokens.Add(new Token("EmailCv.Relationship3", employmentModel.Relationship3));

            //PREVIOUS CONVICTION INFORMATION
            tokens.Add(new Token("EmailCv.ConvictionExplaination", employmentModel.ConvictionExplaination));
            tokens.Add(new Token("EmailCv.Explaination", employmentModel.Explaination));

            //AUTHORIZATION
            tokens.Add(new Token("EmailCv.DateToday", employmentModel.DateToday));
            tokens.Add(new Token("EmailCv.Signature", employmentModel.Signature));
            //event notification
            //_eventPublisher.TokensAdded(employmentModel, tokens);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Generates the tokens.
				/// </summary>
				/// <param name="employmentModel">The employment model.</param>
				/// <returns></returns>
        private IList<Token> GenerateTokens(EmploymentModel employmentModel)
        {
            var tokens = new List<Token>();
            //_messageTokenProvider.AddStoreTokens(tokens);
            AddEmailCvTokens(tokens, employmentModel);
            return tokens;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Sends the email.
				/// </summary>
				/// <param name="languageId">The language id.</param>
				/// <param name="employmentModel">The employment model.</param>
				/// <param name="attachedFilePath">The attached file path.</param>
        public virtual void SendEmail(int languageId, EmploymentModel employmentModel, string attachedFilePath)
        {



            languageId = EnsureLanguageIsActive(languageId);

            var messageTemplate = GetLocalizedActiveMessageTemplate("Employment.EmailCv", languageId);
            if (messageTemplate == null)
                return;

            // var tokens = new List<Token>();
            var couponTokens = GenerateTokens(employmentModel);

            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);
						var toEmail = emailAccount.DestinationEmail;//"razib@brainstation-23.com";
            var toName = emailAccount.DisplayName;

            /*var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

//Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, couponTokens, false);
            var bodyReplaced = _tokenizer.Replace(body, couponTokens, true);*/


            SendNotification(messageTemplate, emailAccount,
                languageId, couponTokens,
                toEmail, toName, attachedFilePath);
        }



				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the email account of message template.
				/// </summary>
				/// <param name="messageTemplate">The message template.</param>
				/// <param name="languageId">The language id.</param>
				/// <returns></returns>
        private EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
        {
            var emailAccounId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
            var emailAccount = _emailAccountService.GetEmailAccountById(emailAccounId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            return emailAccount;

        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the email body.
				/// </summary>
				/// <param name="messageTemplate">The message template.</param>
				/// <returns></returns>
        private String GetEmailBody(MessageTemplate messageTemplate)
        {
            return messageTemplate.Body;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Generates the tokens.
				/// </summary>
				/// <param name="customer">The customer.</param>
				/// <returns></returns>
        private IList<Token> GenerateTokens(Customer customer)
        {
            var tokens = new List<Token>();
            //  _messageTokenProvider.AddStoreTokens(tokens);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            return tokens;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Ensures the language is active.
				/// </summary>
				/// <param name="languageId">The language id.</param>
				/// <returns></returns>
        private int EnsureLanguageIsActive(int languageId)
        {
            var language = _languageService.GetLanguageById(languageId);
            if (language == null || !language.Published)
                language = _languageService.GetAllLanguages().FirstOrDefault();
            return language.Id;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the localized active message template.
				/// </summary>
				/// <param name="messageTemplateName">Name of the message template.</param>
				/// <param name="languageId">The language id.</param>
				/// <returns></returns>
        private MessageTemplate GetLocalizedActiveMessageTemplate(string messageTemplateName, int languageId)
        {
            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, _storeContext.CurrentStore.Id);
            if (messageTemplate == null)
                return null;

            //var isActive = messageTemplate.GetLocalized((mt) => mt.IsActive, languageId);
            //use
            var isActive = messageTemplate.IsActive;
            if (!isActive)
                return null;

            return messageTemplate;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Sends the notification.
				/// </summary>
				/// <param name="messageTemplate">The message template.</param>
				/// <param name="emailAccount">The email account.</param>
				/// <param name="languageId">The language id.</param>
				/// <param name="tokens">The tokens.</param>
				/// <param name="toEmailAddress">To email address.</param>
				/// <param name="toName">To name.</param>
				/// <param name="attachedFilePath">The attached file path.</param>
        private int SendNotification(MessageTemplate messageTemplate,
             EmailAccount emailAccount, int languageId, IEnumerable<Token> tokens,
             string toEmailAddress, string toName, string attachedFilePath)
        {
            //retrieve localized message template data
            var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
            var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
            var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            var bodyReplaced = _tokenizer.Replace(body, tokens, true);
						//_emailSender.SendAttachedEmail(emailAccount, subjectReplaced, bodyReplaced,
											// emailAccount.Email, emailAccount.Email, toEmailAddress, toName, attachedFilePath);

						var attachmentFilePath = attachedFilePath;
						var attachmentFileName = Path.GetFileName(attachedFilePath);

            
						var email = new QueuedEmail()
						{
							Priority = 5,
							From = emailAccount.Email,
							FromName = emailAccount.DisplayName,
							To = toEmailAddress,
							ToName = toName,
							CC = string.Empty,
							Bcc = bcc,
							Subject = subjectReplaced,
							Body = bodyReplaced,
							AttachmentFilePath = attachmentFilePath,
							AttachmentFileName = attachmentFileName,
							CreatedOnUtc = DateTime.UtcNow,
							EmailAccountId = emailAccount.Id
						};
						_queuedEmailService.InsertQueuedEmail(email);
						
						
            
            return email.Id;
        }
        #endregion



    }
}

