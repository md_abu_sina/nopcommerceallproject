﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Upon
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("EmploymentForm", "EmploymentForm",
                            new { controller = "Employment", action = "ShowForm" },
                            new[] { "Nop.Plugin.Employment.Controllers" });

            routes.MapRoute("SubmitForm", "Plugin/Employment/SubmitForm",
                            new { controller = "Employment", action = "SubmitForm" },
                            new[] { "Nop.Plugin.Employment.Controllers" });

            routes.MapRoute("installMessageTemplateForEmployment", "installMessageTemplateForEmployment",
                new { controller = "Employment", action = "installEmploymentMessageTemplates" },
                                            new[] { "Nop.Plugin.Employment.Controllers" });

					/*routes.MapRoute("EmailSent", "t/{SystemName}",
													new { controller = "Topic", action = "TopicDetails" },
													new[] { "Nop.Web.Controllers" });
					routes.MapLocalizedRoute("Topic","t/{SystemName}",
													new { controller = "Topic", action = "TopicDetails" },
													new[] { "Nop.Web.Controllers" });*/
        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}