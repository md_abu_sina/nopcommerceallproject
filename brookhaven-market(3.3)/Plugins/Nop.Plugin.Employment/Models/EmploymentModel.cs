﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Employment.Models
{
    public class EmploymentModel : BaseNopEntityModel
    {
        //public int Id { get; set; }
        public string StoreName { get; set; }

        #region Personal Information

        #region Name

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        #endregion

        #region Address

        public string Street { get; set; }

        public string City { get; set; }

        public string StateZip { get; set; }

        public string HomePhone { get; set; }

        public string Cell { get; set; }

        public string SS { get; set; }

        #endregion

        public string Age16 { get; set; }

        public string Age18 { get; set; }

        public string UsaWorkPermit { get; set; }

        public string UsaWorkPermitProof { get; set; }

        public string HiringNewsSource { get; set; }

        #endregion


        #region Employment Desired

        public string Position { get; set; }

        public string StartDate { get; set; }

        public string Salary { get; set; }

        public string Store { get; set; }

        public string Department { get; set; }

        public string AvailableSunday { get; set; }

        public string AvailableMonday { get; set; }

        public string AvailableTuesday { get; set; }

        public string AvailableWednesday { get; set; }

        public string AvailableThursday { get; set; }

        public string AvailableFriday { get; set; }

        public string AvailableSaturday { get; set; }

        public string MaxWorkingHour { get; set; }

        public string PreferWorkingHour { get; set; }

        public string OtherLanguage { get; set; }

        public string PreferNotAnswer { get; set; }

        public string AppliedBefore { get; set; }

        public string Where { get; set; }

        public string When { get; set; }

        #endregion

        #region Education History

        public string HighSchoolName { get; set; }

        public string CollegeName { get; set; }

        public string OtherName { get; set; }

        public string HighSchoolComplete { get; set; }

        public string CollegeComplete { get; set; }

        public string OtherComplete { get; set; }

        public string HighSchoolStatus { get; set; }

        public string CollegeStatus { get; set; }

        public string OtherStatus { get; set; }

        public string HighSchoolSubject { get; set; }

        public string CollegeSubject { get; set; }

        public string OtherSubject { get; set; }

        #endregion

        #region Employment Experience

        public string CurrentlyEmployed { get; set; }

        public string ContactCurrentEmployerPermission { get; set; }

        public string FromMonthYear1 { get; set; }

        public string ToMonthYear1 { get; set; }

        public string Employer1 { get; set; }

        public string Salary1 { get; set; }

        public string Position1 { get; set; }

        public string LeavingReason1 { get; set; }

        public string FromMonthYear2 { get; set; }

        public string ToMonthYear2 { get; set; }

        public string Employer2 { get; set; }

        public string Salary2 { get; set; }

        public string Position2 { get; set; }

        public string LeavingReason2 { get; set; }

        public string FromMonthYear3 { get; set; }

        public string ToMonthYear3 { get; set; }

        public string Employer3 { get; set; }

        public string Salary3 { get; set; }

        public string Position3 { get; set; }

        public string LeavingReason3 { get; set; }

        public string FromMonthYear4 { get; set; }

        public string ToMonthYear4 { get; set; }

        public string Employer4 { get; set; }

        public string Salary4 { get; set; }

        public string Position4 { get; set; }

        public string LeavingReason4 { get; set; }

        #endregion

        #region GENERAL INFORMATION

        public string SpecialStudy { get; set; }

        public string SpecialTraining { get; set; }

        #endregion

        #region REFERENCES

        public string Name1 { get; set; }

        public string Address1 { get; set; }

        public string PhoneNumber1 { get; set; }

        public string Business1 { get; set; }

        public string Relationship1 { get; set; }

        public string Name2 { get; set; }

        public string Address2 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string Business2 { get; set; }

        public string Relationship2 { get; set; }

        public string Name3 { get; set; }

        public string Address3 { get; set; }

        public string PhoneNumber3 { get; set; }

        public string Business3 { get; set; }

        public string Relationship3 { get; set; }

        #endregion

        #region PREVIOUS CONVICTION INFORMATION

        public string ConvictionExplaination { get; set; }

        public string Explaination { get; set; }

        #endregion

        #region AUTHORIZATION

        public string DateToday { get; set; }

        public string Signature { get; set; }

        #endregion

    }

}
