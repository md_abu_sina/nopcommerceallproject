﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Other.EmailMarketing.Domain;

namespace Nop.Plugin.Other.EmailMarketing.Data
{
    public partial class CampaignQueueMap : EntityTypeConfiguration<CampaignQueue>
    {
				public CampaignQueueMap()
        {
						ToTable("Nop_CampaignQueue");

            //Map the primary key
            HasKey(e => e.Id);
            Property(e => e.CampaignId);
						/*Property(e => e.Name);
            Property(e => e.Subject);
            Property(e => e.Body);
						Property(e => e.To);
            Property(e => e.Cc);
						Property(e => e.Bcc);
						Property(e => e.GroupName);*/
						Property(e => e.EmailInsertStatus);
						Property(e => e.BroadCastTime);


        }
    }
}
