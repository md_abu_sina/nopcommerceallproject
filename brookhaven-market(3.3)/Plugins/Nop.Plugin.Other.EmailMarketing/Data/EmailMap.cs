﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Other.EmailMarketing.Domain;

namespace Nop.Plugin.Other.EmailMarketing.Data
{
    public partial class EmailMap : EntityTypeConfiguration<Email>
    {
        public EmailMap()
        {
            ToTable("Nop_Email");

            //Map the primary key
            HasKey(e => e.Id);        
            Property(e => e.CampaignId);
						Property(e => e.SourceId);
            Property(e => e.TableName);
            Property(e => e.EmailAddress);
						Property(e => e.SentStatus);
            Property(e => e.OpenStatus);
						
						


        }
    }
}
