﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Other.EmailMarketing.Domain;

namespace Nop.Plugin.Other.EmailMarketing.Data
{
    public partial class LinkMap : EntityTypeConfiguration<Link>
    {
				public LinkMap()
        {
						ToTable("Nop_Link");

            //Map the primary key
            HasKey(e => e.Id);
            Property(e => e.CampaignId);
						Property(e => e.LinkHref);
            Property(e => e.Click);

						this.HasKey (e => e.Id)
								.HasMany(e => e.Emails)
								.WithMany(l => l.Links)
								.Map(x =>
								{
									x.ToTable("Nop_LinkClick"); 
									x.MapLeftKey("LinkId");
									x.MapRightKey("EmailId");
								});
					

        }
    }
}
