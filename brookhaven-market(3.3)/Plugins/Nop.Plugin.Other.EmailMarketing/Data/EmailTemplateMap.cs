﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Other.EmailMarketing.Domain;

namespace Nop.Plugin.Other.EmailMarketing.Data
{
    public partial class EmailTemplateMap : EntityTypeConfiguration<EmailTemplate>
    {
				public EmailTemplateMap()
        {
            ToTable("Nop_EmailTemplate");

            //Map the primary key
            HasKey(e => e.Id);
            Property(e => e.Name);
						Property(e => e.PictureId);
            Property(e => e.TemplatePath);
            Property(e => e.ThumbnailPath);
            Property(e => e.TemplateBody);


        }
    }
}
