﻿using System;
using System.IO;
using System.IO.Compression;
using Ionic.Zip;
using HtmlAgilityPack;

namespace Nop.Plugin.Other.EmailMarketing.Lib
{
	public static class Decompress
	{

		/*public static void ConvertToHtml(string html, string filePath )
		{
			TextWriter tw = new StreamWriter(filePath);

			// write a line of text to the file
			tw.WriteLine(html);

			// close the stream
			tw.Close();
		}*/

		public static string DecompressFile(FileInfo fileInfo, string baseUrl)
		{
			string zipToUnpack = fileInfo.FullName;
			string random = Guid.NewGuid().ToString();
			string unpackDirectory = zipToUnpack.Substring(0, zipToUnpack.Length - 4) + random;
			using(ZipFile zip1 = ZipFile.Read(zipToUnpack))
			{
				// here, we extract every entry, but we could extract conditionally
				// based on entry name, size, date, checkbox status, etc.  
				foreach(ZipEntry e in zip1)
				{
					e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
				}
			}

			//Delete the zip folder after unzip
			System.IO.FileInfo dir = new System.IO.FileInfo(zipToUnpack);
			if(dir.Exists)
				dir.Delete();

			string folderName = fileInfo.Name.Substring(0, fileInfo.Name.Length - 4) + random;
			return ReWriteHtml(unpackDirectory, baseUrl, folderName);

		}

		private static string ReWriteHtml(string filePath, string baseUrl, string folderName)
		{
			HtmlDocument doc = new HtmlDocument();
			doc.Load(filePath + "/index.html");

			foreach(HtmlNode image in doc.DocumentNode.SelectNodes("//img[@src]"))
			{
				HtmlAttribute att = image.Attributes["src"];
				att.Value = baseUrl + "Content/Template/" + folderName + "/" + att.Value;
			}

			/*var node = HtmlNode.CreateNode(string.Format("<img src='{0}Admin/Plugin/Other/EmailMarketing/EmailOpen' height='1px' width='1px'/>", baseUrl));
			HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");
			body.PrependChild(node);*/

			doc.Save(filePath + "/index.html");

			return System.IO.File.ReadAllText(filePath + "/index.html");

		}



	}
}