﻿using System;
using System.IO;
using System.IO.Compression;
using Ionic.Zip;
using HtmlAgilityPack;
using port25.pmta.api.submitter;
using Nop.Plugin.Other.EmailMarketing.Models;
using System.Collections.Generic;
using Nop.Admin.Models.Messages;

namespace Nop.Plugin.Other.EmailMarketing.Lib
{
	public static class Helper
	{

		//public static SmartContactModel GetContacts(string query)
		//{
			

		//}

		public static string GetConditions(string criteria = "")
		{
			string[] criterias = criteria.Split(',');
			List<string> tempCustomerWhere = new List<string>();
			List<string> tempNewsLetterWhere = new List<string>();
			List<string> tempCustomerRoleWhere = new List<string>();
			List<string> tempOthersWhere = new List<string>();
			//newsLetterWhere;
			for(var i = 0; i < criterias.Length; i++)
			{
				string tableName = criterias[i].Split('.')[0];
				if(tableName == "Customer")
				{
					tempCustomerWhere.Add(criterias[i]);
				}
				else if(tableName == "NewsLetterSubscription")
				{
					tempNewsLetterWhere.Add(criterias[i]);
				}
				else if(tableName == "CustomerRole")
				{
					tempCustomerRoleWhere.Add(criterias[i]);
				}
				else if(tableName == "GenericAttribute")
				{
					tempOthersWhere.Add(criterias[i]);
				}
			}

			string customerWhere = "";
			string newsLetterWhere = "";
			string customerRoleWhere = "";
			string othersWhere = "";

			if(tempCustomerWhere.Count > 0)
			{
				foreach(var item in tempCustomerWhere)
				{
					customerWhere += ReplaceSqlSign(item) + " ";//customerWhere += ReplaceSqlSign(item.Substring(item.IndexOf('.') + 1)) + " ";
				}
			}

			if(tempNewsLetterWhere.Count > 0)
			{
				foreach(var item in tempNewsLetterWhere)
				{
					newsLetterWhere += ReplaceSqlSign(item) + " ";//newsLetterWhere += ReplaceSqlSign(item.Substring(item.IndexOf('.') + 1)) + " ";
				}
			}

			if(tempCustomerRoleWhere.Count > 0)
			{
				foreach(var item in tempCustomerRoleWhere)
				{
					customerRoleWhere += ReplaceSqlSign(item) + " ";//customerRoleWhere += ReplaceSqlSign(item.Substring(item.IndexOf('.') + 1)) + " ";
				}
			}

			if(tempOthersWhere.Count > 0)
			{
				foreach(var item in tempOthersWhere)
				{
					othersWhere += ReplaceSqlSign(item.Substring(item.IndexOf('.') + 1), true) + " ";
				}
			}

			//string test = customerWhere + " " + newsLetterWhere;


			newsLetterWhere = (string.IsNullOrEmpty(newsLetterWhere)) ? newsLetterWhere : newsLetterWhere.Remove(newsLetterWhere.Length - 4);

			customerRoleWhere = (string.IsNullOrEmpty(customerRoleWhere)) ? customerRoleWhere : customerWhere + customerRoleWhere.Remove(customerRoleWhere.Length - 4);
			othersWhere = (string.IsNullOrEmpty(othersWhere)) ? othersWhere : customerWhere + othersWhere.Remove(othersWhere.Length - 4);

			customerWhere = (string.IsNullOrEmpty(customerWhere)) ? customerWhere : customerWhere.Remove(customerWhere.Length - 4);

			string conditionString = String.Format("{0}^{1}^{2}^{3}", customerWhere, newsLetterWhere, customerRoleWhere, othersWhere);

			return conditionString;
		}

		private static string ReplaceSqlSign(string item, bool genericAttribute = false)
		{
			string filter = item.Split('^')[0];
			string condition = item.Split('^')[1];
			string keyword = item.Split('^')[2];
			string andOr = item.Split('^')[3];

			if(condition == "Is Equal To")
			{
				condition = " = ";
				keyword = "'" + keyword + "'";
			}
			else if(condition == "Begin With")
			{
				condition = " Like ";
				keyword = "'" + keyword + "%'";
			}
			else if(condition == "Contains")
			{
				condition = " Like ";
				keyword = "'%" + keyword + "%'";
			}
			else if(condition == "Does Not Contain")
			{
				condition = " != ";
				keyword = "'" + keyword + "'";
			}
			else if(condition == "Greater Than")
			{
				condition = " > ";
				keyword = "'" + keyword + "'";
			}
			else if(condition == "Less Than")
			{
				condition = " < ";
				keyword = "'" + keyword + "'";
			}

			if(filter.Contains("Created on"))
			{
				filter = "CreatedOnUtc";
			}
			string finalString;
			if(!genericAttribute)
			{
				finalString = filter + " " + condition + " " + keyword + " " + andOr;
			}
			else
			{
				finalString = "[Key] = '" + filter + "' And Value " + condition + " " + keyword + " " + andOr;  //[Key] = 'JoinWineLoversClub' And Value = 'True' Or [Key] = 'City' And Value Like '%darien%'
			}

			return finalString;
		}


		public static void SendTestEmail(EmailModel email, CampaignModel campaign)
		{
			// See RFC 2557 for details; http://tools.ietf.org/html/rfc2557
			//var msg = GenerateMessage("razib@brainstation-23.com");
			Connection con = new Connection("69.65.19.13", 25);

			//foreach(SmartContactModel smartContact in smartContacts)
			//{
			con.Submit(GenerateMessage(email.EmailAddress, campaign));
			//}

			
			con.Close();

		}

		private static Message GenerateMessage(string destinationMail, CampaignModel campaign)
		{
			String mailfrom = "razib@brainstation-23.com";
			String recipient = destinationMail;

			Message msg = new Message(mailfrom);
			msg.AddDateHeader();
			String headers =
					"From: Email Campaign <" + mailfrom + ">\n" +
					"To: Our Customer <" + recipient + ">\n" +
					"Subject: " + campaign.Subject + "\n" +
					"Content-Type: text/html; charset=\"utf-8\"\n" +
					"\n"; // separate headers from body by an empty line
			msg.AddData(headers);

			String body = campaign.Body;
			msg.AddData(body);

			Recipient rcpt = new Recipient(recipient);
			msg.AddRecipient(rcpt);

			return msg;
		}

	}
}