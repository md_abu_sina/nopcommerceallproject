﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using Nop.Plugin.Other.EmailMarketing.Models;
using Nop.Admin.Models.Messages;
using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing;
using Nop.Plugin.Other.EmailMarketing.Domain;

namespace Nop.Plugin.Other.EmailMarketing.Lib
{
	public class ProcessCampaignFacade
	{

		private readonly ICampaignQueueService _campaignQueueService;
		private readonly ISmartGroupsService _smartGroupsService;
		private readonly IEmailService _emailService;
		private readonly ILinkService _linkService;

		public ProcessCampaignFacade(ICampaignQueueService campaignQueueService, ISmartGroupsService smartGroupsService, IEmailService emailService, ILinkService linkService)
		{
			this._campaignQueueService = campaignQueueService;
			this._emailService = emailService;
			this._smartGroupsService = smartGroupsService;
			this._linkService = linkService;
		}

		public CampaignQueue GetQueuedCampaign(CampaignModel campaign)
		{
			CampaignQueueModel campaignQueue = new CampaignQueueModel();

			campaignQueue.BroadCastTime = DateTime.Now;
			campaignQueue.CampaignId = campaign.Id;
			campaignQueue.Delivered = 0;

			return _campaignQueueService.InsertCampaignQueue(campaignQueue.ToCampaignQueueEntity());
		}

		public string InsertCampaignLink (string campaignBody, int campaignId)
		{
			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(campaignBody);

			foreach(HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
			{
				HtmlAttribute href = link.Attributes["href"];
				
				Link linkURL = new Link();
				linkURL.CampaignId = campaignId;
				linkURL.LinkHref = href.Value;
				linkURL.Click = 0;

				var insertedLink = _linkService.InsertLink(linkURL);

				href.Value = String.Format("?linkId={0}", insertedLink.Id);
			}

			return doc.DocumentNode.InnerHtml;

		}

		public IEnumerable<SmartContactModel> GetCampaignContacts(CampaignQueue campaign, string groupName)
		{
			var smartGroup = _smartGroupsService.GetSmartGroupByName(groupName);

			IEnumerable<SmartContactModel> smartContacts = _smartGroupsService.GetContacts(smartGroup.Id);

			return smartContacts;
		}

		public EmailModel TrackEmail(SmartContactModel contact, CampaignQueue queuedCampaign, CampaignModel campaign)
		{
			Email email = new Email();

			email.CampaignId = queuedCampaign.Id;
			email.EmailAddress = contact.Email;

			var insertedEmail = _emailService.InsertEmail(email);

			return insertedEmail.ToEmailModel();
		}

		public CampaignModel InsertOpenImage(CampaignModel campaign, EmailModel email, string baseUrl)
		{
			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(campaign.Body);

			var node = HtmlNode.CreateNode(string.Format("<img src='{0}Admin/Plugin/Other/EmailMarketing/EmailOpen?emailId={1}&campaignId={2}' height='1px' width='1px'/>", baseUrl, email.Id, email.CampaignId));
			//HtmlNode firstDiv = doc.DocumentNode.SelectSingleNode("//div");
			doc.DocumentNode.AppendChild(node);

			campaign.Body = doc.DocumentNode.InnerHtml;

			return campaign;
		}

		public CampaignModel ReplaceLink(CampaignModel campaign, EmailModel email, string baseUrl)
		{
			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(campaign.Body);

			foreach(HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
			{
				HtmlAttribute href = link.Attributes["href"];

				href.Value = String.Format("{0}Admin/Plugin/Other/EmailMarketing/LinkClick{1}&emailId={2}&campaignId={3}", baseUrl, href.Value, email.Id, email.CampaignId);
			}

			campaign.Body = doc.DocumentNode.InnerHtml;

			return campaign;
		}

		public void SendEmail(EmailModel email, CampaignQueue queuedCampaign, CampaignModel campaign)
		{
			var emailEntity = _emailService.GetEmailById(email.Id);
			try
			{
				Helper.SendTestEmail(email, campaign);
				emailEntity.SentStatus = "sent";
				_emailService.UpdateEmail(emailEntity);

				queuedCampaign.Delivered = queuedCampaign.Delivered + 1;
				_campaignQueueService.UpdateCampaignQueue(queuedCampaign);
			}
			catch(Exception ex)
			{
				emailEntity.SentStatus = ex.Message;
				_emailService.UpdateEmail(emailEntity);

			}
		}


		public void UpdateCampaignQue(CampaignQueue queuedCampaign)
		{
			queuedCampaign.EmailInsertStatus = "Complete";
			//queuedCampaign.Delivered = _emailService.GetSentEmailByCampaignId(queuedCampaign.CampaignId);
			_campaignQueueService.UpdateCampaignQueue(queuedCampaign);
		}


	}
}
