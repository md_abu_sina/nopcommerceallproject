﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.Other.EmailMarketing.Models
{
	public class CampaignReportModel : BaseNopEntityModel
  {
				public virtual int Id { get; set; }

				public virtual int CampaignId { get; set; }
		
				public virtual string Name { get; set; }
				
				public virtual string Subject { get; set; }
		
				public virtual DateTime BroadCastTime {	get;	set; }
		
				public virtual int Delivered { get; set; }
				
				public virtual int Opened { get; set; }

				public virtual int ClickedThrough { get; set; }
				
		
  }

}
