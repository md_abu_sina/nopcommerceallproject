﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Other.EmailMarketing.Models
{
	public class SmartContactModel : BaseNopEntityModel
  {
		public string UserName { get; set; }
		public string Email { get; set; }
		public DateTime CreatedOn { get; set; }
  }

}
