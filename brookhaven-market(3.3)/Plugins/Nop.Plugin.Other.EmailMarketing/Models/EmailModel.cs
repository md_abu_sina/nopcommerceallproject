﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.Other.EmailMarketing.Models
{
	public class EmailModel : BaseNopEntityModel
  {
		public int Id { get; set; }
		
		public int CampaignId { get; set; }
		
		//[UIHint("Picture")]
		//[NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
		public virtual int SourceId {	get;	set; }

		//[UIHint("ZipFile")]
		//[Display(Name = "Import Template")]
    public string TableName { get; set; }
    
		public string EmailAddress { get; set; }

		public string SentStatus { get; set; }

		public string OpenStatus { get; set; }
		
  }

}
