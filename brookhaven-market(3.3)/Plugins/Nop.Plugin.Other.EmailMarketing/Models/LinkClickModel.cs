﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.Other.EmailMarketing.Models
{
	public class LinkClickModel : BaseNopEntityModel
  {
		
    public virtual int EmailId { get; set; }

    public virtual int LinkId { get; set; }
		
  }

}
