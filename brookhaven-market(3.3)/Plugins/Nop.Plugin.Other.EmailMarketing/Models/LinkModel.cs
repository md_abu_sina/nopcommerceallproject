﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.Other.EmailMarketing.Models
{
	public class LinkModel : BaseNopEntityModel
  {
		public int Id { get; set; }
		
		public int CampaignId { get; set; }

		public virtual string LinkHref { get; set; }

    public virtual int Click { get; set; }
		
  }

}
