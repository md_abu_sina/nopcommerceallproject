﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Other.EmailMarketing.Models
{
	public class CriteriaModel : BaseNopEntityModel
  {
		public int Id { get; set; }

		public string Name { get; set; }

		public string KeyWord { get; set; }
		
		public string Columns { get; set; }

		public string Conditions { get; set; }

		public string AndOr { get; set; }

		public string Query { get; set; }

  }

}
