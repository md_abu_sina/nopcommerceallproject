﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.Other.EmailMarketing.Models
{
	public class EmailTemplateModel : BaseNopEntityModel
  {
		public int Id { get; set; }
		
		public string Name { get; set; }
		
		[UIHint("Picture")]
		[NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
		public virtual int PictureId {	get;	set; }

		[UIHint("ZipFile")]
		[Display(Name = "Import Template")]
    public string TemplatePath { get; set; }
    
		public string ThumbnailPath { get; set; }

		[Display(Name = "Message Tokens")]
		public IEnumerable<SelectListItem> Tokens { get; set; }

		[AllowHtml]
		public string TemplateBody { get; set; }
		
  }

}
