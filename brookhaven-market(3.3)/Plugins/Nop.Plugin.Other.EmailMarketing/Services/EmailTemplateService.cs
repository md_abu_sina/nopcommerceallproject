﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using System.Globalization;
//using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Data;
using Nop.Core.Domain.Common;
using System.Data;
using Nop.Plugin.Other.EmailMarketing.Data;
using Nop.Plugin.Other.EmailMarketing.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public class EmailTemplateService : IEmailTemplateService
    {
        #region fields

        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IEventPublisher _eventPublisher;
        //private readonly IDbContext _dbContext;
        private readonly EmailTemplateObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

        public EmailTemplateService(IRepository<EmailTemplate> emailTemplateRepository, IEventPublisher eventPublisher,
                                                            EmailTemplateObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
            this._emailTemplateRepository = emailTemplateRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of IEmailTemplateService

        public virtual IPagedList<EmailTemplate> GetAllEmailTemplate(int pageIndex, int pageSize)
        {
            //var query = _eventRepository.Table;
            var query = (from u in _emailTemplateRepository.Table
                         orderby u.Name
                         select u);
            var emailTemplates = new PagedList<EmailTemplate>(query, pageIndex, pageSize);
            return emailTemplates;
        }

				public virtual List<EmailTemplate> GetAllEmailTemplate()
				{
					//var query = _eventRepository.Table;
					var emailTemplates = (from u in _emailTemplateRepository.Table
											 orderby u.Name
											 select u);
					return emailTemplates.ToList();
				}

        public virtual void InsertEmailTemplate(EmailTemplate emailTemplate)
        {
            if (emailTemplate == null)
                throw new ArgumentNullException("emailTemplate");

            _emailTemplateRepository.Insert(emailTemplate);

            //event notification
            _eventPublisher.EntityInserted(emailTemplate);
        }

        public EmailTemplate GetEmailTemplateById(int id)
        {
            var db = _emailTemplateRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

        public void UpdateEmailTemplate(EmailTemplate emailTemplate)
        {
            if (emailTemplate == null)
                throw new ArgumentNullException("emailTemplate");

            _emailTemplateRepository.Update(emailTemplate);
            //event notification
            _eventPublisher.EntityUpdated(emailTemplate);

        }


        public void DeleteEmailTemplate(EmailTemplate emailTemplate)
        {
            _emailTemplateRepository.Delete(emailTemplate);
            _eventPublisher.EntityDeleted(emailTemplate);
        }

        

        #endregion
    }
}
