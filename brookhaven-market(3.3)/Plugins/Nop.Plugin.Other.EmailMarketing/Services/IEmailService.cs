﻿//using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Plugin.Other.EmailMarketing.Models;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public interface IEmailService
    {

			/// <summary>
			/// Gets all smart group.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <returns></returns>
			IPagedList<Email> GetAllEmail(int pageIndex, int pageSize);

			/// <summary>
			/// Gets all email .
			/// </summary>
			/// <returns></returns>
			List<Email> GetAllEmail();

			/// <summary>
			/// Inserts the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      Email InsertEmail(Email email);

			/// <summary>
			/// Gets the smart group by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
      Email GetEmailById(int id);

			/// <summary>
			/// Gets all email by campaign id.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			IPagedList<Email> GetAllEmailByCampaignId(int pageIndex, int pageSize, int id, string status);

			//int GetSentEmailByCampaignId(int id);

			/// <summary>
			/// Updates the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void UpdateEmail(Email email);

			/// <summary>
			/// Deletes the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void DeleteEmail(Email email);

       
    }
}
