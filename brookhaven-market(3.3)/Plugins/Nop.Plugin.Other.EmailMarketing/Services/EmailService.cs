﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using System.Globalization;
//using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Data;
using Nop.Core.Domain.Common;
using System.Data;
using Nop.Plugin.Other.EmailMarketing.Data;
using Nop.Plugin.Other.EmailMarketing.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public class EmailService : IEmailService
    {
        #region fields

        private readonly IRepository<Email> _emailRepository;
        private readonly IEventPublisher _eventPublisher;
        //private readonly IDbContext _dbContext;
        private readonly EmailObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

        public EmailService(IRepository<Email> emailRepository, IEventPublisher eventPublisher,
                                                            EmailObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
            this._emailRepository = emailRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of IEmailTemplateService

        public virtual IPagedList<Email> GetAllEmail(int pageIndex, int pageSize)
        {
            //var query = _eventRepository.Table;
            var query = (from u in _emailRepository.Table
                         orderby u.EmailAddress
                         select u);
            var emails = new PagedList<Email>(query, pageIndex, pageSize);
            return emails;
        }

				public virtual List<Email> GetAllEmail()
				{
					//var query = _eventRepository.Table;
					var emails = (from u in _emailRepository.Table
											 orderby u.EmailAddress
											 select u);
					return emails.ToList();
				}

        public virtual Email InsertEmail(Email email)
        {
            if (email == null)
                throw new ArgumentNullException("email");

            _emailRepository.Insert(email);
						//event notification
            _eventPublisher.EntityInserted(email);

						return email;
        }

        public Email GetEmailById(int id)
        {
            var db = _emailRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

				public IPagedList<Email> GetAllEmailByCampaignId(int pageIndex, int pageSize, int id, string status)
				{
					IQueryable<Email> query;

					switch(status)
					{
						case "all":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id)
											 orderby e.EmailAddress
											 select e);
							break;

						case "delivered":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.SentStatus == "sent"
											 orderby e.EmailAddress
											 select e);
							break;
						
						case "bounced":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.SentStatus != "sent"
											 orderby e.EmailAddress
											 select e);
							break;

						case "opened":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.OpenStatus == "opened"
											 orderby e.EmailAddress
											 select e);
							break;
						
						default:
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id)
											 orderby e.EmailAddress
											 select e);
							break;
					}

					var emails = new PagedList<Email>(query, pageIndex, pageSize);
					return emails;
				}

        public void UpdateEmail(Email email)
        {
            if (email == null)
                throw new ArgumentNullException("email");

            _emailRepository.Update(email);
            //event notification
            _eventPublisher.EntityUpdated(email);

        }


        public void DeleteEmail(Email email)
        {
            _emailRepository.Delete(email);
            _eventPublisher.EntityDeleted(email);
        }

        

        #endregion
    }
}
