﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using System.Globalization;
//using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Data;
using Nop.Core.Domain.Common;
using System.Data;
using Nop.Plugin.Other.EmailMarketing.Data;
using Nop.Plugin.Other.EmailMarketing.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public class LinkClickService : ILinkClickService
    {
        #region fields

        private readonly IRepository<LinkClick> _linkClickRepository;
        private readonly IEventPublisher _eventPublisher;
        //private readonly IDbContext _dbContext;
        private readonly EmailObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
				private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

				public LinkClickService(IRepository<LinkClick> linkClickRepository, IEventPublisher eventPublisher,
                                                            EmailObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
						this._linkClickRepository = linkClickRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of ILinkClickService

        public virtual IPagedList<LinkClick> GetAllLinkClick(int pageIndex, int pageSize, int campaignId)
        {
            //var query = _eventRepository.Table;
						var query = (from l in _linkClickRepository.Table
												// orderby l.LinkId
                         select l);
						var linkClicks = new PagedList<LinkClick>(query, pageIndex, pageSize);
						return linkClicks;
        }


				public virtual LinkClick InsertLinkClick(LinkClick linkClick)
        {
            if (linkClick == null)
                throw new ArgumentNullException("linkClick");
						
						_linkClickRepository.Insert(linkClick);
						//event notification
            _eventPublisher.EntityInserted(linkClick);

						return linkClick;
        }


				public LinkClick GetLinkClickById(int id)
        {
						var db = _linkClickRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }


				public void UpdateLinkClick(LinkClick linkClick)
        {
						if(linkClick == null)
                throw new ArgumentNullException("linkClick");

            _linkClickRepository.Update(linkClick);
            //event notification
            _eventPublisher.EntityUpdated(linkClick);

        }


				public void DeleteLinkClick(LinkClick linkClick)
        {
            _linkClickRepository.Delete(linkClick);
            _eventPublisher.EntityDeleted(linkClick);
        }

        

        #endregion
    }
}
