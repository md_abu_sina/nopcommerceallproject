﻿//using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Plugin.Other.EmailMarketing.Models;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
		public interface ICampaignQueueService
    {

			/// <summary>
			/// Gets all smart group.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <returns></returns>
			IPagedList<CampaignQueue> GetAllQueuedCampaigns(int pageIndex, int pageSize);

			/// <summary>
			/// Gets all email .
			/// </summary>
			/// <returns></returns>
			List<CampaignQueue> GetAllQueuedCampaignsList();

			/// <summary>
			/// Inserts the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
			CampaignQueue InsertCampaignQueue(CampaignQueue campaign);

			/// <summary>
			/// Gets the smart group by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
      CampaignQueue GetQueuedCampaignById(int id);

			/// <summary>
			/// Updates the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void UpdateCampaignQueue(CampaignQueue campaign);

			/// <summary>
			/// Deletes the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void DeleteEmail(CampaignQueue campaign);

       
    }
}
