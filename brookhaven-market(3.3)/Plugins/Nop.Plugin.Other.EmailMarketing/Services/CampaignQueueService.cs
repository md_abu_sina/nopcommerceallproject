﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using System.Globalization;
//using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Data;
using Nop.Core.Domain.Common;
using System.Data;
using Nop.Plugin.Other.EmailMarketing.Data;
using Nop.Plugin.Other.EmailMarketing.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public class CampaignQueueService : ICampaignQueueService
    {
        #region fields

        private readonly IRepository<CampaignQueue> _campaignQueueRepository;
        private readonly IEventPublisher _eventPublisher;
        //private readonly IDbContext _dbContext;
        private readonly CampaignQueueObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

				public CampaignQueueService(IRepository<CampaignQueue> campaignQueueRepository, IEventPublisher eventPublisher,
																															CampaignQueueObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
            this._campaignQueueRepository = campaignQueueRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of IEmailTemplateService

				public virtual IPagedList<CampaignQueue> GetAllQueuedCampaigns(int pageIndex, int pageSize)
        {
            //var query = _eventRepository.Table;
						var query = (from u in _campaignQueueRepository.Table
                         orderby u.BroadCastTime
                         select u);
						var campaigns = new PagedList<CampaignQueue>(query, pageIndex, pageSize);
            return campaigns;
        }

				public virtual List<CampaignQueue> GetAllQueuedCampaignsList()
				{
					//var query = _eventRepository.Table;
					var emails = (from u in _campaignQueueRepository.Table
											 orderby u.BroadCastTime
											 select u);
					return emails.ToList();
				}

				public virtual CampaignQueue InsertCampaignQueue(CampaignQueue campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

						_campaignQueueRepository.Insert(campaign);
						//event notification
            _eventPublisher.EntityInserted(campaign);

						return campaign;
        }

				public CampaignQueue GetQueuedCampaignById(int id)
        {
						var db = _campaignQueueRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

				public void UpdateCampaignQueue(CampaignQueue campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

						_campaignQueueRepository.Update(campaign);
            //event notification
            _eventPublisher.EntityUpdated(campaign);

        }


				public void DeleteEmail(CampaignQueue campaign)
        {
						_campaignQueueRepository.Delete(campaign);
            _eventPublisher.EntityDeleted(campaign);
        }

        

        #endregion
    }
}
