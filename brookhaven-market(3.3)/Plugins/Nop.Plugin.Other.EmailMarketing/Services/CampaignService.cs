﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using System.Globalization;
using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Core.Domain.Messages;

namespace Nop.Plugin.Other.EmailMarketing
{
    public class CampaignService : ICampaignService
    {
        #region fields

				private readonly IRepository<Campaign> _campaignRepository;
				private readonly IEventPublisher _eventPublisher;
				/*private readonly IEmailSender _emailSender;
				private readonly IMessageTokenProvider _messageTokenProvider;
				private readonly ITokenizer _tokenizer;
				private readonly IQueuedEmailService _queuedEmailService;
				private readonly ICustomerService _customerService;*/

        #endregion

        #region ctor

				public CampaignService(IRepository<Campaign> campaignRepository, IEventPublisher eventPublisher)
        {
            this._campaignRepository = campaignRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Implementation of ICampaignService

        /// <summary>
        /// get all unique coupons
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<UniqueCoupon></returns>
        public virtual IPagedList<Campaign> GetAllCampaigns(int pageIndex, int pageSize)
        {
            // var query = _uniqueCouponRepository.Table;
						var query = (from u in _campaignRepository.Table
                         orderby u.Id
                         select u);
						var campaigns = new PagedList<Campaign>(query, pageIndex, pageSize);
						return campaigns;
        }
			
        #endregion
    }
}
