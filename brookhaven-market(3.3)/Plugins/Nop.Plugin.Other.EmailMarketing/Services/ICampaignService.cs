﻿//using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Messages;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
	public interface ICampaignService
    {
				///--------------------------------------------------------------------------------------------
        /// <summary>
        /// get all unique coupons
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<UniqueCoupon></returns>
        IPagedList<Campaign> GetAllCampaigns(int pageIndex, int pageSize);

       
    }
}
