﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using System.Globalization;
//using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Data;
using Nop.Core.Domain.Common;
using System.Data;
using Nop.Plugin.Other.EmailMarketing.Data;
using Nop.Plugin.Other.EmailMarketing.Models;
using Nop.Plugin.Other.EmailMarketing.Lib;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public class SmartGroupsService : ISmartGroupsService
    {
        #region fields

				private readonly IRepository<SmartGroups> _smartGroupsRepository;
        private readonly IEventPublisher _eventPublisher;
				//private readonly IDbContext _dbContext;
				private readonly AdminAreaSettings _adminAreaSettings;
				private readonly SmartGroupsObjectContext _dbContext;
				private readonly CommonSettings _commonSettings;
				private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

				public SmartGroupsService(IRepository<SmartGroups> smartGroupsRepository, IEventPublisher eventPublisher, AdminAreaSettings adminAreaSettings,
																	SmartGroupsObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
					this._smartGroupsRepository = smartGroupsRepository;
					this._eventPublisher = eventPublisher;
					this._adminAreaSettings = adminAreaSettings;
					this._dbContext = dbContext;
					this._commonSettings = commonSettings;
					this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of ISmartGroupsService

				public virtual IPagedList<SmartGroups> GetAllSmartGroup(int pageIndex, int pageSize)
				{
					//var query = _eventRepository.Table;
					var query = (from u in _smartGroupsRepository.Table
											 orderby u.Name
											 select u);
					var smartGroups = new PagedList<SmartGroups>(query, pageIndex, pageSize);
					return smartGroups;
				}

				public virtual void InsertSmartGroup(SmartGroups smartGroup)
				{
					if(smartGroup == null)
						throw new ArgumentNullException("smartGroup");

					_smartGroupsRepository.Insert(smartGroup);

					//event notification
					_eventPublisher.EntityInserted(smartGroup);
				}

				public SmartGroups GetSmartGroupById(int id)
				{
					var db = _smartGroupsRepository;
					return db.Table.SingleOrDefault(x => x.Id == id);
				}

				public virtual SmartGroups GetSmartGroupByName(string name)
				{
					if(string.IsNullOrWhiteSpace(name))
						return null;

					/*var customers =  from sg in _smartGroupsRepository.Table
													 where sg.Name.Contains(name)
													 select sg.Name.SingleOrDefault();*/
					return _smartGroupsRepository.Table.SingleOrDefault(x => x.Name.Contains(name));
				}

				public virtual IEnumerable<string> SmartGroupAutoComplete(string name)
				{
					return
						from sg in _smartGroupsRepository.Table
						where sg.Name.Contains(name)
						select sg.Name;
				}

				public void UpdateSmartGroup(SmartGroups smartGroup)
				{
					if(smartGroup == null)
						throw new ArgumentNullException("smartGroup");

					_smartGroupsRepository.Update(smartGroup);
					//event notification
					_eventPublisher.EntityUpdated(smartGroup);
					
				}


				public void DeleteSmartGroup(SmartGroups smartGroup)
				{
					_smartGroupsRepository.Delete(smartGroup);
					_eventPublisher.EntityDeleted(smartGroup);
				}

				public IPagedList<SmartContactModel> GetContacts(int id)
				{
					var criteria = this.GetSmartGroupById(id).Query;

					string[] whereConditions = Helper.GetConditions(criteria).Split('^');
					string customerWhereCondition = whereConditions[0];
					string newsLetterWhereCondition = whereConditions[1];
					string roleWhereCondition = whereConditions[2];
					string othersWhereCondition = whereConditions[3];

					var contacts = this.SmartGroupSP(0, _adminAreaSettings.GridPageSize, customerWhereCondition, newsLetterWhereCondition, roleWhereCondition, othersWhereCondition);

					return contacts;
				}

				public IPagedList<SmartContactModel> SmartGroupSP(int pageIndex, int pageSize, string customerWhere, string newsLetterWhere, string customerRoleWhere, string othersWhere)
				{
					if(_commonSettings.UseStoredProceduresIfSupported && _dataProvider.StoredProceduredSupported)
					{
						//stored procedures are enabled and supported by the database.
						var pCustomerWhere = _dataProvider.GetParameter();
						pCustomerWhere.ParameterName = "CustomerWhere";
						pCustomerWhere.Value = customerWhere;
						pCustomerWhere.DbType = DbType.AnsiStringFixedLength;

						var pNewLetterWhere = _dataProvider.GetParameter();
						pNewLetterWhere.ParameterName = "NewsLetterWhere";
						pNewLetterWhere.Value = newsLetterWhere;
						pNewLetterWhere.DbType = DbType.AnsiStringFixedLength;

						var pCustomerRoleWhere = _dataProvider.GetParameter();
						pCustomerRoleWhere.ParameterName = "CustomerRoleWhere";
						pCustomerRoleWhere.Value = customerRoleWhere;
						pCustomerRoleWhere.DbType = DbType.AnsiStringFixedLength;
						
						var pOthersWhere = _dataProvider.GetParameter();
						pOthersWhere.ParameterName = "OthersWhere";
						pOthersWhere.Value = othersWhere;
						pOthersWhere.DbType = DbType.AnsiStringFixedLength;

						//long-running query. specify timeout (600 seconds)
						var smartContactModel = _dbContext.SqlQuery<SmartContactModel>("EXEC [SmartGroup_Email] @CustomerWhere, @NewsLetterWhere, @CustomerRoleWhere, @OthersWhere", pCustomerWhere, pNewLetterWhere, pCustomerRoleWhere, pOthersWhere).ToList();
						var smartGroups = new PagedList<SmartContactModel>(smartContactModel, pageIndex, pageSize);
						return smartGroups;
					}
					else
					{
						throw new ArgumentNullException("smartContactModel");
					}
				}
				
			
        #endregion
    }
}
