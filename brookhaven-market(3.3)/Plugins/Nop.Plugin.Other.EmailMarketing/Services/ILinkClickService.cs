﻿//using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Plugin.Other.EmailMarketing.Models;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public interface ILinkClickService
    {

			/// <summary>
			/// Gets all link click.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			IPagedList<LinkClick> GetAllLinkClick(int pageIndex, int pageSize, int id = 0);

			/// <summary>
			/// Inserts the link.
			/// </summary>
			/// <param name="linkClick">The link click.</param>
			/// <returns></returns>
			LinkClick InsertLinkClick(LinkClick linkClick);

			/// <summary>
			/// Gets the link click by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			LinkClick GetLinkClickById(int id);

			/// <summary>
			/// Updates the link click.
			/// </summary>
			/// <param name="linkClick">The link click.</param>
			void UpdateLinkClick(LinkClick linkClick);

			/// <summary>
			/// Deletes the link click.
			/// </summary>
			/// <param name="linkClick">The link click.</param>
			void DeleteLinkClick(LinkClick linkClick);

       
    }
}
