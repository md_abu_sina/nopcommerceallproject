﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using System.Globalization;
//using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Data;
using Nop.Core.Domain.Common;
using System.Data;
using Nop.Plugin.Other.EmailMarketing.Data;
using Nop.Plugin.Other.EmailMarketing.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public class LinkService : ILinkService
    {
        #region fields

        private readonly IRepository<Link> _linkRepository;
        private readonly IEventPublisher _eventPublisher;
        //private readonly IDbContext _dbContext;
        private readonly EmailObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

				public LinkService(IRepository<Link> linkRepository, IEventPublisher eventPublisher,
                                                            EmailObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
						this._linkRepository = linkRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of ILinkService

        public virtual IPagedList<Link> GetAllLink(int pageIndex, int pageSize, int campaignId)
        {
            //var query = _eventRepository.Table;
						var query = (from l in _linkRepository.Table
												 where l.CampaignId == campaignId
                         orderby l.CampaignId
                         select l);
						var links = new PagedList<Link>(query, pageIndex, pageSize);
						return links;
        }


				public virtual Link InsertLink(Link link)
        {
            if (link == null)
                throw new ArgumentNullException("link");

            _linkRepository.Insert(link);
						//event notification
            _eventPublisher.EntityInserted(link);

						return link;
        }

				public virtual void InsertLinkClick(int linkId = 0, int emailId = 0)
				{
						if(linkId == 0)
							throw new ArgumentNullException("link");

						var ifExist = _linkRepository.Table.Any(l => l.Id == linkId);

						var emailIdParam = _dataProvider.GetParameter();
						emailIdParam.ParameterName = "EmailId";
						emailIdParam.Value = emailId;
						emailIdParam.DbType = DbType.AnsiStringFixedLength;

						var linkIdParam = _dataProvider.GetParameter();
						linkIdParam.ParameterName = "LinkId";
						linkIdParam.Value = linkId;
						linkIdParam.DbType = DbType.AnsiStringFixedLength;
						
						if(!ifExist)
							_dbContext.ExecuteSqlCommand("INSERT INTO Nop_LinkClick VALUES (@LinkId, @EmailId);", null, linkIdParam, emailIdParam);
				}

				public IEnumerable<Email> GetEmailByLinkId(int linkId)
				{
					var emails = from l in _linkRepository.Table
											 from e in l.Emails
											 where l.Id == linkId
											 select e;
											 /*group e by e.EmailAddress into grp
											 select new
											 {
												 email = grp.Key,
												 count = grp.Count()
											 };*/
					return emails;
				}
				
				public Link GetLinkById(int id)
        {
						var db = _linkRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }


				public void UpdateLink(Link link)
        {
						if(link == null)
                throw new ArgumentNullException("link");

            _linkRepository.Update(link);
            //event notification
            _eventPublisher.EntityUpdated(link);

        }


				public void DeleteLink(Link link)
        {
            _linkRepository.Delete(link);
            _eventPublisher.EntityDeleted(link);
        }

        

        #endregion
    }
}
