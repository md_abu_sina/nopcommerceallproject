﻿//using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Other.EmailMarketing.Domain;
using Nop.Plugin.Other.EmailMarketing.Models;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Other.EmailMarketing.Services
{
    public interface ISmartGroupsService
    {

			/// <summary>
			/// Gets all smart group.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <returns></returns>
			IPagedList<SmartGroups> GetAllSmartGroup(int pageIndex, int pageSize);

			/// <summary>
			/// Inserts the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
			void InsertSmartGroup(SmartGroups smartGroup);

			/// <summary>
			/// Gets the contacts.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			IPagedList<SmartContactModel> GetContacts(int id);

			/// <summary>
			/// Gets the smart group by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			SmartGroups GetSmartGroupById(int id);

			/// <summary>
			/// Gets the name of the smart group by.
			/// </summary>
			/// <param name="name">The name.</param>
			/// <returns></returns>
			SmartGroups GetSmartGroupByName(string name);

			/// <summary>
			/// Smarts the group auto complete.
			/// </summary>
			/// <param name="name">The name.</param>
			/// <returns></returns>
			IEnumerable<string> SmartGroupAutoComplete(string name);

			/// <summary>
			/// Updates the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
			void UpdateSmartGroup(SmartGroups smartGroup);

			/// <summary>
			/// Deletes the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
			void DeleteSmartGroup(SmartGroups smartGroup);

			IPagedList<SmartContactModel> SmartGroupSP(int pageIndex, int pageSize, string customerWhere, string newsLetterWhere, string customerRoleWhere, string othersWhere);
       
    }
}
