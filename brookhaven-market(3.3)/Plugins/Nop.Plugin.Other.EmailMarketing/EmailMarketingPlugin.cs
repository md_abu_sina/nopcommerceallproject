﻿using Nop.Core.Plugins;
using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using Nop.Core.Data;
using Nop.Plugin.Other.EmailMarketing.Data;


namespace Nop.Plugin.Other.EmailMarketing
{
	public class EmailMarketingPlugin : BasePlugin, IAdminMenuPlugin
	{
    private readonly SmartGroupsObjectContext _context;

    private readonly EmailTemplateObjectContext _emailTemplateContext;

		private readonly EmailObjectContext _emailContext;

		private readonly CampaignQueueObjectContext _campaignQueueContext;

		private readonly LinkObjectContext _linkContext;

		private readonly LinkClickObjectContext _linkClickContext;  

		public EmailMarketingPlugin(SmartGroupsObjectContext context, EmailTemplateObjectContext emailTemplateContext, EmailObjectContext emailContext,
																CampaignQueueObjectContext campaignQueueContext, LinkObjectContext linkContext, LinkClickObjectContext linkClickContext)
		{

            _context = context;
            _emailTemplateContext = emailTemplateContext;
						_emailContext = emailContext;
						_campaignQueueContext = campaignQueueContext;
						_linkContext = linkContext;
						_linkClickContext = linkClickContext;
		}

		public void BuildMenuItem(Telerik.Web.Mvc.UI.MenuItemBuilder menuItemBuilder)
		{
			menuItemBuilder.Text("Email Marketing");
			menuItemBuilder.Url("/Admin/Plugin/Other/EmailMarketing/Main");
			menuItemBuilder.Route("Admin.Plugin.Other.EmailMarketing.Main");
		}

		public override void Install()
		{
      //_context.InstallSchema();
      //_emailTemplateContext.InstallSchema();
			//_emailContext.InstallSchema();
			//_campaignQueueContext.InstallSchema();
			
			//_linkClickContext.InstallSchema();
			_linkContext.InstallSchema();
			base.Install();


			//this.AddOrUpdatePluginLocaleResource("admin.contentmanagement.uniquecoupons", "Unique Coupons");



		}



		/// <summary>
		/// Uninstall plugin
		/// </summary>
		public override void Uninstall()
		{
			//database objects
			//_objectContext.Uninstall();

			//locales
			/*this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Country");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Country.Hint");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.StateProvince");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.StateProvince.Hint");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Zip");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Zip.Hint");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.TaxCategory");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.TaxCategory.Hint");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Percentage");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Percentage.Hint");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.AddRecord");
			this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.AddRecord.Hint");*/

			base.Uninstall();
		}

	}
}
