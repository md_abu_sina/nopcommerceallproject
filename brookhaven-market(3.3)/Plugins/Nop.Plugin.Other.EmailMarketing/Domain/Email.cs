﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.Other.EmailMarketing;
using Nop.Core.Domain.Customers;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Other.EmailMarketing.Domain
{
		[Table("Nop_Email")]
    public class Email : BaseEntity
    {
				private ICollection<Link> _links;

        public virtual int Id { get; set; }

        public virtual int CampaignId { get; set; }

				public virtual int SourceId {	get;	set; }

        public virtual string TableName { get; set; }

        public virtual string EmailAddress { get; set; }

        public virtual string SentStatus { get; set; }
			
				public virtual string OpenStatus { get; set; }

				//public ICollection<Link> Links { get; set; }
				//public ICollection<Link> Links;
				//public List<LinkClick> LinkClicked { get; set; }	

				public virtual ICollection<Link> Links
				{
					get
					{
						return _links ?? (_links = new List<Link>());
					}
					protected set
					{
						_links = value;
					}
				}


    }

}
