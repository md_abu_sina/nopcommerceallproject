﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.Other.EmailMarketing;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Other.EmailMarketing.Domain
{
		//[Table("Nop_Link")]
    public class Link : BaseEntity
    {
				private ICollection<Email> _emails;
				

        public virtual int Id { get; set; }

        public virtual int CampaignId { get; set; }

        public virtual string LinkHref { get; set; }

        public virtual int Click { get; set; }
				
				//public ICollection<Email> Emails { get; set; }
				//public ICollection<Email> Emails;
				//public virtual Email Email { get; set; } 
				
				public virtual ICollection<Email> Emails
				{
					get
					{
						return _emails ?? (_emails = new List<Email>());
					}
					protected set
					{
						_emails = value;
					}
				}

				

    }

}
