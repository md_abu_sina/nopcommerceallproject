﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.Other.EmailMarketing;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Other.EmailMarketing.Domain
{
    public class EmailTemplate : BaseEntity
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

				public virtual int PictureId {	get;	set; }

        public virtual string TemplatePath { get; set; }

        public virtual string ThumbnailPath { get; set; }

        public virtual string TemplateBody { get; set; }

    }

}
