﻿using Nop.Core;
using System.Data.Objects;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.Other.EmailMarketing;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Other.EmailMarketing.Domain
{
		public class CampaignQueue : BaseEntity
    {
        public virtual int Id { get; set; }
				
				public virtual int CampaignId { get; set; }

				public virtual int Delivered { get; set; }
				
				public virtual int Opened { get; set; }

				public virtual int ClickedThrough { get; set; }

				/*public virtual string Name { get; set; }

				public virtual string Subject { get; set; }

				public virtual string Body { get; set; }

				public virtual string To { get; set; }

				public virtual string Cc { get; set; }

				public virtual string Bcc { get; set; }

				public virtual string GroupName { get; set; }*/

				public virtual string EmailInsertStatus { get; set; }

				public virtual DateTime BroadCastTime {	get;	set; }

    }

}
