﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Other.EmailMarketing
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.Main", "Admin/Plugin/Other/EmailMarketing/Main",
                            new { controller = "EmailMarketing", action = "Main" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.Campaign", "Admin/Plugin/Other/EmailMarketing/Campaign",
                            new { controller = "EmailMarketing", action = "Campaign" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
         	routes.MapRoute("Admin.Plugin.Other.EmailMarketing.Create", "Admin/Plugin/Other/EmailMarketing/Create",
                            new { controller = "EmailMarketing", action = "Create" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.Edit", "Admin/Plugin/Other/EmailMarketing/Edit/{id}",
                            new { controller = "EmailMarketing", action = "Edit", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.To", "Admin/Plugin/Other/EmailMarketing/To/{term}",
                            new { controller = "EmailMarketing", action = "To", term = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.GroupName", "Admin/Plugin/Other/EmailMarketing/GroupName/{term}",
                            new { controller = "EmailMarketing", action = "GroupName", term = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");



					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.CreateGroup", "Admin/Plugin/Other/EmailMarketing/CreateGroup",
                            new { controller = "EmailMarketing", action = "CreateGroup" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.EditGroup", "Admin/Plugin/Other/EmailMarketing/EditGroup/{id}",
                            new { controller = "EmailMarketing", action = "EditGroup", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.Group", "Admin/Plugin/Other/EmailMarketing/Group",
                            new { controller = "EmailMarketing", action = "Group" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.SmartGroup", "Admin/Plugin/Other/EmailMarketing/SmartGroup/{id}",
                            new { controller = "EmailMarketing", action = "SmartGroup", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.SmartGroupAjax", "Admin/Plugin/Other/EmailMarketing/SmartGroupAjax",
                            new { controller = "EmailMarketing", action = "SmartGroupAjax" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");


          routes.MapRoute("Admin.Plugin.Other.EmailMarketing.TemplateList", "Admin/Plugin/Other/EmailMarketing/TemplateList",
                          new { controller = "EmailMarketing", action = "TemplateList" },
                          new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.CreateTemplate", "Admin/Plugin/Other/EmailMarketing/CreateTemplate",
                            new { controller = "EmailMarketing", action = "CreateTemplate" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.EditTemplate", "Admin/Plugin/Other/EmailMarketing/EditTemplate/{id}",
                            new { controller = "EmailMarketing", action = "EditTemplate", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.TemplateThumbnailAdd", "Admin/Plugin/Other/EmailMarketing/TemplateThumbnailAdd",
                            new { controller = "EmailMarketing", action = "TemplateThumbnailAdd" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.ZipFileUpload", "Admin/Plugin/Other/EmailMarketing/ZipFileUpload",
                            new { controller = "EmailMarketing", action = "ZipFileUpload" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.ViewTemplates", "Admin/Plugin/Other/EmailMarketing/ViewTemplates",
                            new { controller = "EmailMarketing", action = "ViewTemplates" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.GetTemplate", "Admin/Plugin/Other/EmailMarketing/GetTemplate",
                            new { controller = "EmailMarketing", action = "GetTemplate" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");


					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.Report", "Admin/Plugin/Other/EmailMarketing/Report",
                            new { controller = "EmailMarketing", action = "Report" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.EmailOpen", "Admin/Plugin/Other/EmailMarketing/EmailOpen",
                            new { controller = "EmailMarketing", action = "EmailOpen" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.LinkClick", "Admin/Plugin/Other/EmailMarketing/LinkClick",
                            new { controller = "EmailMarketing", action = "LinkClick" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.TestEmail", "Admin/Plugin/Other/EmailMarketing/TestEmail",
                            new { controller = "EmailMarketing", action = "TestEmail" },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.ShowTemplate", "Admin/Plugin/Other/EmailMarketing/ShowTemplate/{id}",
                            new { controller = "EmailMarketing", action = "ShowTemplate", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.DetailReport", "Admin/Plugin/Other/EmailMarketing/DetailReport/{id}",
                            new { controller = "EmailMarketing", action = "DetailReport", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.Link", "Admin/Plugin/Other/EmailMarketing/Link/{id}",
                            new { controller = "EmailMarketing", action = "Link", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					routes.MapRoute("Admin.Plugin.Other.EmailMarketing.LinkClickDetail", "Admin/Plugin/Other/EmailMarketing/LinkClickDetail/{id}",
                            new { controller = "EmailMarketing", action = "LinkClickDetail", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
					
        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}