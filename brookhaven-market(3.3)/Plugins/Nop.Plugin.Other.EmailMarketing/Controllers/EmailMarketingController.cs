﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Models.News;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.News;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.News;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Core.Domain.Customers;
using Telerik.Web.Mvc;
using Nop.Admin.Controllers;
using Nop.Core;
using Nop.Admin.Models.Customers;
using Nop.Services.Media;
using Nop.Core.Domain.Media;
using Nop.Services.Common;
using System.Web;
using Nop.Web.Framework.Mvc;
using System.IO;
using System.IO.Compression;
using System.Net;
using Nop.Core.Domain.Messages;
using Nop.Services.Messages;
using Nop.Core.Data;
using Nop.Core.Events;
using Nop.Web.Models.Customer;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Orders;
using Nop.Services.Orders;
using Nop.Core.Domain;
using Nop.Admin.Models.Messages;
using Nop.Admin;
using System.Text;
using Nop.Plugin.Other.EmailMarketing.Services;
using Nop.Plugin.Other.EmailMarketing.Models;
using Nop.Plugin.Other.EmailMarketing.Lib;
//using Nop.Plugin.Other.EmailMarketing.Models;

namespace Nop.Plugin.Other.EmailMarketing.Controllers
{
	public class EmailMarketingController : BaseNopController
	{
		#region Fields

		private readonly ICampaignService _campaignService;
		private readonly IEmailTemplateService _emailTemplateService;
		private readonly IPictureService _pictureService;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly IPermissionService _permissionService;
		private readonly IMessageTokenProvider _messageTokenProvider;
		private readonly ICustomerService _customerService;
		private readonly AdminAreaSettings _adminAreaSettings;
		private readonly ISmartGroupsService _smartGroupsService;
		private readonly StoreInformationSettings _storeInformationSettings;
		private readonly IEmailService _emailService;
		private readonly ICampaignQueueService _campaignQueueService;
		private readonly ILinkService _linkService;
		private readonly ILinkClickService _linkClickService;
		

		#region email fields




		#endregion



		#endregion

		#region ctor

		public EmailMarketingController(ICampaignService campaignService, IEmailTemplateService emailTemplateService, IEmailService emailService, AdminAreaSettings adminAreaSettings, ILinkService linkService, ILinkClickService linkClickService,
																		ISmartGroupsService smartGroupsService,IPictureService pictureService, IDateTimeHelper dateTimeHelper, StoreInformationSettings storeInformationSettings,
																		ICampaignQueueService campaignQueueService, IPermissionService permissionService, IMessageTokenProvider messageTokenProvider, ICustomerService customerService)
		{
			this._campaignService = campaignService;
			this._emailTemplateService = emailTemplateService;
			this._pictureService = pictureService;
			this._adminAreaSettings = adminAreaSettings;
			this._smartGroupsService = smartGroupsService;
			this._dateTimeHelper = dateTimeHelper;
			this._permissionService = permissionService;
			this._messageTokenProvider = messageTokenProvider;
			this._customerService = customerService;
			this._storeInformationSettings = storeInformationSettings;
			this._emailService = emailService;
			this._campaignQueueService = campaignQueueService;
			this._linkService = linkService;
			this._linkClickService = linkClickService;
		}

		#endregion


		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
		[AdminAuthorize]
		public ActionResult Main()
		{
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.Main");
		}


		#region Campaign

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Campaigns this instance.
		/// </summary>
		/// <returns></returns>
		[AdminAuthorize]
		public ActionResult Campaign()
		{
			/*if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return AccessDeniedView();*/

			var campaigns = _campaignService.GetAllCampaigns();
			var gridModel = new GridModel<CampaignModel>
			{
				Data = campaigns.Select(x =>
				{
					var model = x.ToModel();
					model.CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc);
					return model;
				}),
				Total = campaigns.Count
			};
			//return View(gridModel);
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.Campaign",gridModel);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Campaigns this instance.
		/// </summary>
		/// <returns></returns>
		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult Campaign(GridCommand command)
		{
			/*if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return AccessDeniedView();*/

			var campaigns = _campaignService.GetAllCampaigns();
			var gridModel = new GridModel<CampaignModel>
			{
				Data = campaigns.Select(x =>
				{
					var model = x.ToModel();
					model.CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc);
					return model;
				}),
				Total = campaigns.Count
			};
			return new JsonResult
			{
				Data = gridModel
			};
		}

		public ActionResult Create()
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return AccessDeniedView();

			var model = new CampaignModel();
			model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.CreateCampaign", model);
			//return View(model);
		}

		[HttpPost]
		public ActionResult Create(CampaignModel model)
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return AccessDeniedView();

			if(ModelState.IsValid)
			{
				var campaign = model.ToEntity();
				campaign.CreatedOnUtc = DateTime.UtcNow;
				_campaignService.InsertCampaign(campaign);

				//SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Added"));
				/*return continueEditing ? RedirectToAction("Edit", new
				{
					id = campaign.Id
				}) : RedirectToAction("Campaign");*/
				return RedirectToAction("Campaign", "/Plugin/Other/EmailMarketing");
			}

			//If we got this far, something failed, redisplay form
			//model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.CreateCampaign", model);
		}

		public ActionResult Edit(int id)
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return AccessDeniedView();

			var campaign = _campaignService.GetCampaignById(id);
			if(campaign == null)
				//No campaign found with the specified id
				return RedirectToAction("Campaign");

			var model = campaign.ToModel();
			model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.EditCampaign", model);
		}

		[HttpPost]
		//[FormValueExists("save", "save-continue", "continueEditing")]
		//[FormValueRequired("save", "save-continue")]
		public ActionResult Edit(CampaignModel model)
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return AccessDeniedView();

			var campaign = _campaignService.GetCampaignById(model.Id);
			if(campaign == null)
				//No campaign found with the specified id
				return RedirectToAction("List");

			if(ModelState.IsValid)
			{
				campaign = model.ToEntity(campaign);
				_campaignService.UpdateCampaign(campaign);

				//SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Updated"));
				//return continueEditing ? RedirectToAction("Edit", new
				//{
				//  id = campaign.Id
				//}) : RedirectToAction("List");

				return RedirectToAction("Campaign", "/Plugin/Other/EmailMarketing");
			}

			//If we got this far, something failed, redisplay form
			model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View(model);
		}
		

		#endregion


		#region Template

		[AdminAuthorize]
		public ActionResult TemplateList()
		{
			var templates = _emailTemplateService.GetAllEmailTemplate(0, _adminAreaSettings.GridPageSize);

			foreach(var template in templates)
			{
				template.ThumbnailPath = _pictureService.GetPictureUrl(template.PictureId);
			}

			var gridModel = new GridModel<EmailTemplateModel>
			{
				Data = templates.Select(x =>
				{
					var model = x.ToEmailTemplateModel();
					return model;
				}),
				Total = templates.Count
			};
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.TemplateList", gridModel);
		}


		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult TemplateList(GridCommand command)
		{
			var templates = _emailTemplateService.GetAllEmailTemplate(command.Page - 1, command.PageSize);
			var gridModel = new GridModel<EmailTemplateModel>
			{
				Data = templates.Select(x =>
				{
					var model = x.ToEmailTemplateModel();
					return model;
				}),
				Total = templates.Count
			};

			return new JsonResult
			{
				Data = gridModel
			};
		}

		
		[AdminAuthorize]
		public ActionResult ViewTemplates()
		{
			var templates = _emailTemplateService.GetAllEmailTemplate();
			
			foreach(var template in templates)
			{
				template.ThumbnailPath = _pictureService.GetPictureUrl(template.PictureId);
			}
			
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.ViewTemplates", templates);
		}

		[AdminAuthorize]
		public string GetTemplate(int id = 0)
		{
			var template = _emailTemplateService.GetEmailTemplateById(id);
			return template.TemplateBody;
		}

		public ActionResult CreateTemplate()
		{
			var model = new EmailTemplateModel();
			//model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());

			var allowedTokens = _messageTokenProvider.GetListOfAllowedTokens();

			model.Tokens = allowedTokens.ToTokenSelectList();

			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.CreateTemplate", model);
			//return View(model);
		}

		[HttpPost]
		public ActionResult CreateTemplate(EmailTemplateModel model)
		{
			if(ModelState.IsValid)
			{
				var emailTemplate = model.ToEmailTemplateEntity();
				emailTemplate.ThumbnailPath = _pictureService.GetPictureUrl(model.PictureId);
				_emailTemplateService.InsertEmailTemplate(emailTemplate);

				return RedirectToAction("TemplateList", "/Plugin/Other/EmailMarketing");
			}

			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.CreateTemplate", model);
		}

		public ActionResult EditTemplate(int id)
		{
			var emailTemplate = _emailTemplateService.GetEmailTemplateById(id);
			if(emailTemplate == null)
				//No emailTemplate found with the specified id
				return RedirectToAction("TemplateList");

			var model = emailTemplate.ToEmailTemplateModel();
			var allowedTokens = _messageTokenProvider.GetListOfAllowedTokens();

			model.Tokens = allowedTokens.ToTokenSelectList();
			//model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.EditTemplate", model);
		}

		[HttpPost]
		//[FormValueExists("save", "save-continue", "continueEditing")]
		//[FormValueRequired("save", "save-continue")]
		public ActionResult EditTemplate(EmailTemplateModel model)
		{
			var emailTemplate = _emailTemplateService.GetEmailTemplateById(model.Id);
			if(emailTemplate == null)
				//No emailTemplate found with the specified id
				return RedirectToAction("TemplateList");

			if(ModelState.IsValid)
			{
				emailTemplate = model.ToEmailTemplateEntity(emailTemplate);
				_emailTemplateService.UpdateEmailTemplate(emailTemplate);

				//SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Updated"));
				//return continueEditing ? RedirectToAction("Edit", new
				//{
				//  id = campaign.Id
				//}) : RedirectToAction("List");

				return RedirectToAction("TemplateList", "/Plugin/Other/EmailMarketing");
			}

			//If we got this far, something failed, redisplay form
			//model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.EditTemplate", model);
		}

		public RedirectResult LinkClick(int linkId = 0, int emailId = 0, int campaignId = 0)
		{
			var clickedLink = _linkService.GetLinkById(linkId);
			clickedLink.Click = clickedLink.Click + 1;
			_linkService.UpdateLink(clickedLink);
			_linkService.InsertLinkClick(linkId, emailId);

			var clickedCampaign = _campaignQueueService.GetQueuedCampaignById(campaignId);
			clickedCampaign.ClickedThrough = clickedCampaign.ClickedThrough + 1;
			_campaignQueueService.UpdateCampaignQueue(clickedCampaign);

			//_linkService.GetEmailByLinkId(linkId);
			/*var linkClickModel = new LinkClickModel();
			linkClickModel.LinkId = linkId;
			linkClickModel.EmailId = emailId;
			_linkClickService.InsertLinkClick(linkClickModel.ToLinkClickEntity());*/

			return Redirect(clickedLink.LinkHref);
		}

		public ActionResult LinkClickDetail(int id = 0)
		{
			var linkClickDetail = _linkService.GetEmailByLinkId(id);

			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.LinkClickDetail", linkClickDetail);
		}

		public void EmailOpen(int emailId = 0, int campaignId = 0)
		{
			var openedEmail = _emailService.GetEmailById(emailId);
			openedEmail.OpenStatus = "opened";
			_emailService.UpdateEmail(openedEmail);

			var openedCampaign = _campaignQueueService.GetQueuedCampaignById(campaignId);
			openedCampaign.Opened = openedCampaign.Opened + 1;
			_campaignQueueService.UpdateCampaignQueue(openedCampaign);
		}

		[HttpPost]
		public void TestEmail(CampaignModel campaign)
		{
			ProcessCampaignFacade facade = new ProcessCampaignFacade(_campaignQueueService, _smartGroupsService, _emailService, _linkService);

			var queuedCampaign = facade.GetQueuedCampaign(campaign);

			campaign.Body = facade.InsertCampaignLink(campaign.Body, queuedCampaign.Id);

			var campaignContacts = facade.GetCampaignContacts(queuedCampaign, campaign.GroupName);

			foreach(SmartContactModel contact in campaignContacts)
			{
				var trackedEmail = facade.TrackEmail(contact, queuedCampaign, campaign);
				campaign = facade.InsertOpenImage(campaign, trackedEmail, _storeInformationSettings.StoreUrl);
				campaign = facade.ReplaceLink(campaign, trackedEmail, _storeInformationSettings.StoreUrl);
				facade.SendEmail(trackedEmail, queuedCampaign, campaign);
			}

			facade.UpdateCampaignQue(queuedCampaign);
			/*queuedCampaign.EmailInsertStatus = "Complete";
			_campaignQueueService.UpdateCampaignQueue(queuedCampaign);*/

		}

		//private CampaignModel PrepareCampaignModel(CampaignModel campaign, ProcessCampaignFacade facade)
		//{
		//  campaign = facade.InsertOpenImage(campaign, _storeInformationSettings.StoreUrl);
		//  return campaign;
		//}

		[HttpPost]
		public ActionResult ZipFileUpload()
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.UploadPictures))
				return Json(new
				{
					success = false,
					error = "You do not have required permissions"
				}, "text/plain");

			//we process it distinct ways based on a browser
			//find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
			//Stream stream = null;
			var fileName = "";
			var stream = Request.InputStream;
			//var contentType = "";
			if(String.IsNullOrEmpty(Request["qqfile"]))
			{
				// IE
				HttpPostedFileBase httpPostedFile = Request.Files[0];
				if(httpPostedFile == null)
					throw new ArgumentException("No file uploaded");
				stream = httpPostedFile.InputStream;
				fileName = Path.GetFileName(httpPostedFile.FileName);
				//contentType = httpPostedFile.ContentType;
			}
			else
			{
				//Webkit, Mozilla
				stream = Request.InputStream;
				fileName = Request["qqfile"];
			}

			var fileExtension = Path.GetExtension(fileName);
			
			if(!String.IsNullOrEmpty(fileExtension))
				fileExtension = fileExtension.ToLowerInvariant();
			
			if(!fileExtension.Equals(".zip") )
			{
				return Json(new
				{
					success = false,
					//pictureId = picture.Id,
					errorMessage = "Please upload a zip file"
				}, "text/plain");
			}

			var path = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\Template", fileName);
			var buffer = new byte[stream.Length];
			stream.Read(buffer, 0, buffer.Length);
			System.IO.File.WriteAllBytes(path, buffer);
			
			var baseUrl = _storeInformationSettings.StoreUrl;

			var fileInfo = new FileInfo(path);

			string uploadedTemplateHtml = Decompress.DecompressFile(fileInfo, baseUrl);



			return Json(new
			{
				success = true,
				uploadedFileName = fileName,
				templateHtml = uploadedTemplateHtml
				//pictureId = picture.Id,
				//imageUrl = _pictureService.GetPictureUrl(picture, 100)
			},"text/plain");
		}


		#endregion


		#region Group

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
		[AdminAuthorize]
		public ActionResult Group()
		{
			var groups = _smartGroupsService.GetAllSmartGroup(0, _adminAreaSettings.GridPageSize);
			var GridModel = new GridModel<CriteriaModel>
			{
				Data = groups.Select(x =>
				{

					CriteriaModel m = x.ToCriteriaModel();

					return m;
				}),

				Total = groups.TotalCount

			};


			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.Group", GridModel);
		}


		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult Group(GridCommand command)
		{
			var groups = _smartGroupsService.GetAllSmartGroup(0, _adminAreaSettings.GridPageSize);
			var GridModel = new GridModel<CriteriaModel>
			{
				Data = groups.Select(x =>
				{

					CriteriaModel m = x.ToCriteriaModel();

					return m;
				}),

				Total = groups.TotalCount

			};
			return new JsonResult
			{
				Data = GridModel
			};
		}


		[AdminAuthorize]
		public ActionResult SmartGroup(int id)
		{

			var contacts = _smartGroupsService.GetContacts(id);

			var GridModel = new GridModel<SmartContactModel>
			{
				Data = contacts.Select(x =>
				{

					SmartContactModel m = x;

					return m;
				}),

				Total = contacts.TotalCount

			};
			ViewBag.SmartGroupId = id;
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.SmartContactList", GridModel);
		}

		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult SmartGroupAjax(int smartGroupId, GridCommand command)
		{
			
			var contacts = _smartGroupsService.GetContacts(smartGroupId);
			IEnumerable<SmartContactModel> test = contacts;
			var GridModel = new GridModel<SmartContactModel>
			{
				Data = contacts.Select(x =>
				{

					SmartContactModel m = x;

					return m;
				}),

				Total = contacts.TotalCount

			};

			JsonResult result = new JsonResult();

			result.Data = GridModel;
			result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

			return result;

		}


		public ActionResult CreateGroup()
		{
			var model = new CriteriaModel();
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.CreateGroup", model);
			//return View(model);
		}

		[HttpPost]
		public ActionResult CreateGroup(CriteriaModel model)
		{

			if(ModelState.IsValid)
			{
				var criteria = model.ToSmartGroupEntity();
				_smartGroupsService.InsertSmartGroup(criteria);

				//SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Added"));
				/*return continueEditing ? RedirectToAction("Edit", new
				{
					id = campaign.Id
				}) : RedirectToAction("Campaign");*/
				return RedirectToAction("Group", "/Plugin/Other/EmailMarketing");
			}

			//If we got this far, something failed, redisplay form
			//model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.CreateGroup", model);
		}

		public ActionResult EditGroup(int id)
		{
			var smartGroup = _smartGroupsService.GetSmartGroupById(id);
			if(smartGroup == null)
				//No campaign found with the specified id
				return RedirectToAction("Group");

			var model = smartGroup.ToCriteriaModel();
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.EditGroup", model);
		}

		[HttpPost]
		//[FormValueExists("save", "save-continue", "continueEditing")]
		//[FormValueRequired("save", "save-continue")]
		public ActionResult EditGroup(CriteriaModel model)
		{


			var smartGroup = _smartGroupsService.GetSmartGroupById(model.Id);
			if(smartGroup == null)
				//No campaign found with the specified id
				return RedirectToAction("Group");

			if(ModelState.IsValid)
			{
				model.ToSmartGroupEntity(smartGroup);
				//smartGroup.Name = model.Name;
				_smartGroupsService.UpdateSmartGroup(smartGroup);


				return RedirectToAction("Group", "/Plugin/Other/EmailMarketing");
			}

			//If we got this far, something failed, redisplay form
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.EditGroup", model);
		}

		#endregion


		#region Report
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
		public ActionResult Report()
		{
			var campaigns = _campaignQueueService.GetAllQueuedCampaigns(0, _adminAreaSettings.GridPageSize);
			var gridModel = new GridModel<CampaignReportModel>
			{
				Data = campaigns.Select(x =>
				{
					var model = x.ToCampaignReportModel();
					var campaign = _campaignService.GetCampaignById(x.CampaignId);
					model.Name = campaign.Name;
					model.Subject = campaign.Name;

					return model;
			
				}),
				
				Total = campaigns.Count
			};
			//return View(gridModel);
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.Report", gridModel);
		}

		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult Report(GridCommand command)
		{
			var campaigns = _campaignQueueService.GetAllQueuedCampaigns(command.Page - 1, command.PageSize);
			var gridModel = new GridModel<CampaignReportModel>
			{
				Data = campaigns.Select(x =>
				{
					var model = x.ToCampaignReportModel();
					var campaign = _campaignService.GetCampaignById(x.CampaignId);
					model.Name = campaign.Name;
					model.Subject = campaign.Name;

					return model;
			
				}),
				
				Total = campaigns.Count
			};
			return new JsonResult
			{
				Data = gridModel
			};
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
		public ActionResult Link(int id = 0)
		{
			var links = _linkService.GetAllLink(0, _adminAreaSettings.GridPageSize, id);
			var gridModel = new GridModel<LinkModel>
			{
				Data = links.Select(x =>
				{
					var model = x.ToLinkModel();
					
					return model;

				}),

				Total = links.Count
			};
			ViewBag.CampaignId = id;
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.Link", gridModel);
		}

		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult Link(GridCommand command, int id = 0)
		{
			var links = _linkService.GetAllLink(0, _adminAreaSettings.GridPageSize, id);
			var gridModel = new GridModel<LinkModel>
			{
				Data = links.Select(x =>
				{
					var model = x.ToLinkModel();

					return model;

				}),

				Total = links.Count
			};
			return new JsonResult
			{
				Data = gridModel
			};
		}

		public ActionResult DetailReport(int id, string status = "all")
		{
			var emails = _emailService.GetAllEmailByCampaignId(0, _adminAreaSettings.GridPageSize, id, status);
			var gridModel = new GridModel<EmailModel>
			{
				Data = emails.Select(x =>
				{
					var model = x.ToEmailModel();
					
					return model;
				}),

				Total = emails.Count
			};
			//return View(gridModel);
			ViewBag.CampaignId = id;
			ViewBag.Status = status;
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.DetailReport", gridModel);
		}

		[HttpPost, GridAction(EnableCustomBinding = true)]
		public ActionResult DetailReport(GridCommand command, int id, string status = "all")
		{
			var emails = _emailService.GetAllEmailByCampaignId(0, _adminAreaSettings.GridPageSize, id, status);
			var gridModel = new GridModel<EmailModel>
			{
				Data = emails.Select(x =>
				{
					var model = x.ToEmailModel();

					return model;
				}),

				Total = emails.Count
			};
			return new JsonResult
			{
				Data = gridModel
			};
		}

		[AdminAuthorize]
		public string ShowTemplate(int id = 0)
		{
			var campaign = _campaignService.GetCampaignById(id);
			return campaign.Body;
		}

		#endregion


		#region OtherHelperMethod
		
		private string FormatTokens(string[] tokens)
		{
			var sb = new StringBuilder();
			for(int i = 0; i < tokens.Length; i++)
			{
				string token = tokens[i];
				sb.Append(token);
				if(i != tokens.Length - 1)
					sb.Append(", ");
			}

			return sb.ToString();
		}

		public JsonResult To(string term = "")
		{
			var customersEmail = _customerService.SearchCustomerByEmail(term);
			return Json(customersEmail, JsonRequestBehavior.AllowGet);
			//return View(model);
		}

		public JsonResult GroupName(string term = "")
		{
			var groups = _smartGroupsService.SmartGroupAutoComplete(term);
			return Json(groups, JsonRequestBehavior.AllowGet);
			//return View(model);
		}

		

		

		#endregion


	}
}

