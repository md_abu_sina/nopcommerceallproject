﻿using Nop.Plugin.Upon.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Plugin.Upon.Services;
using Nop.Core;
using Nop.Core.Domain.Localization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using Nop.Core.Domain.Common;
using Nop.Services.Media;
using Nop.Core.Domain.Media;

namespace Nop.Plugin.Upon.Services
{
    public class CouponPdfService : ICouponPdfService
    {
        #region field

        private readonly IWebHelper _webHelper;
        private readonly PdfSettings _pdfSettings;
        private readonly IPictureService _pictureService;

        #endregion

        #region ctor

        public CouponPdfService(IWebHelper webHelper, PdfSettings pdfSettings, IPictureService pictureService)
        {
            _webHelper = webHelper;
            _pdfSettings = pdfSettings;
            _pictureService = pictureService;
        }

        #endregion

        #region Implementation of ICouponPdfService



        public void PrintProductsToPdf(UniqueCoupon uniquecoupon, ImprintedCoupon imprintedcoupon, string qrCode, Language lang, string filePath)
        {
            if (uniquecoupon == null)
                throw new ArgumentNullException("uniquecoupon");

            if (imprintedcoupon == null)
                throw new ArgumentNullException("imprintedcoupon");

            if (lang == null)
                throw new ArgumentNullException("lang");

            if (String.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("filePath");


            var pageSize = PageSize.A4;

            //if (_pdfSettings.LetterPageSizeEnabled)
            //{
            //    pageSize = PageSize.LETTER;
            //}

            var doc = new Document(pageSize);
            PdfWriter.GetInstance(doc, new FileStream(filePath, FileMode.Create));
            doc.Open();

            //fonts
            var titleFont = GetFont();
            titleFont.SetStyle(Font.BOLD);
            titleFont.Color = BaseColor.BLACK;
            var font = GetFont();

            //int productNumber = 1;
            //int prodCount = products.Count;

            //foreach (var product in products)
            //{
            //    string productName = product.GetLocalized(x => x.Name, lang.Id);
            //    string productFullDescription = product.GetLocalized(x => x.FullDescription, lang.Id);

            doc.Add(new Paragraph(String.Format("{0}. {1}", uniquecoupon.Title, uniquecoupon.Id), titleFont));
            doc.Add(new Paragraph(" "));
            doc.Add(new Paragraph(String.Format("{0}. {1}", imprintedcoupon.First_Name, imprintedcoupon.Last_Name), titleFont));
            doc.Add(new Paragraph(" "));
            doc.Add(new Paragraph(String.Format("{0}. {1}", imprintedcoupon.Email, imprintedcoupon.ExpirationDate), titleFont));
            doc.Add(new Paragraph(" "));


            //var pic = qrCode;

            //Picture QRCode = new Picture();
            //QRCode.
            //if (pic != null)
            //{
            //    var picBinary = _pictureService.LoadPictureBinary(pic);
            //    if (picBinary != null && picBinary.Length > 0)
            //    {
            //        var pictureLocalPath = _pictureService.GetPictureLocalPath(pic, 200, false);
            //        var cell = new PdfPCell(Image.GetInstance(pictureLocalPath));
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        cell.Border = Rectangle.NO_BORDER;
            //        doc.Add(cell);
            //    }
            //}




            doc.Add(new Paragraph(" "));
           
            doc.Close();
        }

        #region Utilities

        protected virtual Font GetFont()
        {
            //nopCommerce supports unicode characters
            //nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
            //It was downloaded from http://savannah.gnu.org/projects/freefont
            string fontPath = Path.Combine(_webHelper.MapPath("~/App_Data/Pdf/"), _pdfSettings.FontFileName);
            var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var font = new Font(baseFont, 10, Font.NORMAL);
            return font;
        }

        #endregion

        #endregion
    }
}
