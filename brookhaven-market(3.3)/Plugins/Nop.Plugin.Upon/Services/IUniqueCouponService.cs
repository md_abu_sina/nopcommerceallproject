﻿using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Upon.Services
{
    public interface IUniqueCouponService
    {
        /// <summary>
        /// get all unique coupons
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<UniqueCoupon></returns>
        IPagedList<UniqueCoupon> GetAllUniqueCoupons(int pageIndex, int pageSize);

        /// <summary>
        /// get all unique coupons
        /// </summary>
        /// <returns>List<UniqueCoupon></returns>
        List<UniqueCoupon> GetAllUniqueCouponsForUser();
				List<UniqueCoupon> GetUniqueCouponsForDeactivate();

        IPagedList<UniqueCoupon> GetAllAvailableUniqueCoupons(int pageIndex, int pageSize);

        /// <summary>
        /// insert Unique Coupon
        /// </summary>
        /// <param name="uniquecouponItem"></param>
        void InsertUniqueCoupon(UniqueCoupon uniquecouponItem);

        /// <summary>
        /// get  Unique Coupon  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        UniqueCoupon GetuniquecouponById(int id);

        /// <summary>
        /// Update Unique Coupon
        /// </summary>
        /// <param name="uniquecouponItem"></param>
        void UpdateUniqueCoupon(UniqueCoupon uniquecouponItem);

        /// <summary>
        /// delete Unique Coupon
        /// </summary>
        /// <param name="uniquecouponItem"></param>
        void DeleteUniqueCoupon(UniqueCoupon uniquecouponItem);
    }
}
