﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Nop.Plugin.Upon.Domain;

namespace Nop.Plugin.Upon.Domain
{
    public class UniqueCoupon : BaseEntity
    {
        /// <summary>
        /// Gets or sets the UniqueCoupon id.
        /// </summary>
        /// <value>
        /// The UniqueCoupon id.
        /// </value>
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets the StartDate.
        /// </summary>
        /// <value>
        /// The StartDate. when the uPon will be published
        /// </value>
        public virtual DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate.
        /// </summary>
        /// <value>
        /// The EndDate.  when the uPon will be taken down
        /// </value>
        public virtual DateTime EndDate { get; set; }



        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title.
        /// </value>
        public virtual string Title { get; set; }


        /// <summary>
        /// Gets or sets the Offer.
        /// </summary>
        /// <value>
        /// The Offer.
        /// </value>
        public virtual string Offer { get; set; }

        /// <summary>
        /// Gets or sets the Restrictions.
        /// </summary>
        /// <value>
        /// The Restrictions.
        /// </value>
        public virtual string Restrictions { get; set; }


        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        /// <value>
        /// The Description.
        /// </value>
        public virtual string Description { get; set; }

        /// <summary>
        /// Gets or sets the Quantity.
        /// </summary>
        /// <value>
        /// The Quantity.
        /// </value>
        public virtual int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the DataRange.
        /// </summary>
        /// <value>
        /// The DataRange.
        /// </value>
        public virtual DateTime DateRange { get; set; }

        /// <summary>
        /// Gets or sets the Coupon Image For Website.
        /// </summary>
        /// <value>
        /// The Coupon Image For Website.
        /// </value>
        public virtual string CouponImageForWebsite { get; set; }

        /// <summary>
        /// Gets or sets the Banner Coupon Image.
        /// </summary>
        /// <value>
        /// The Banner Coupon Image.
        /// </value>
        public virtual string BannerCouponImage { get; set; }

        /// <summary>
        /// Gets or sets the standard Coupon Image.
        /// </summary>
        /// <value>
        /// The standard Coupon Image.
        /// </value>
        public virtual string StandardCouponImage { get; set; }

        /// <summary>
        /// Gets or sets the Disclaimer.
        /// </summary>
        /// <value>
        /// The Disclaimer.
        /// </value>
        public virtual string Disclaimer { get; set; }

        /// <summary>
        /// Gets or sets the UPC.
        /// </summary>
        /// <value>
        /// The UPC.
        /// </value>
        public virtual string UPC { get; set; }

        /// <summary>
        /// Gets or sets the barcodeImagePath.
        /// </summary>
        /// <value>
        /// The barcodeImagePath.
        /// </value>
        public virtual string barcodeImagePath { get; set; }

        /// <summary>
        /// Gets or sets the Upon is active or not.
        /// </summary>
        /// <value>
        /// true or false.
        /// </value>
        public virtual bool Active { get; set; }
             

    }

}
