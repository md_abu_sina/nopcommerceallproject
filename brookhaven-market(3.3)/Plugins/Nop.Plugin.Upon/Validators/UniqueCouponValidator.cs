﻿using FluentValidation;
using Nop.Plugin.Upon.Models;
using Nop.Services.Localization;


namespace Nop.Plugin.Upon.Validators
{
    public class UniqueCouponValidator : AbstractValidator<UniqueCouponModel>
    {
        public UniqueCouponValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.StartDate)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.StartDate.Required"));

            RuleFor(x => x.EndDate)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.EndDate.Required"));

            RuleFor(x => x.Title)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.Title.Required"));

            RuleFor(x => x.Offer)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.Offer.Required"));


            RuleFor(x => x.Restrictions)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.Restrictions.Required"));

            RuleFor(x => x.Description)
               .NotNull()
               .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.Description.Required"));

            RuleFor(x => x.Quantity)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.Quantity.Required"));

            RuleFor(x => x.DateRange)
               .NotNull()
               .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.DateRange.Required"));
           
            RuleFor(x => x.CouponImageForWebsite)
             .NotNull()
             .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.CouponImageForWebsite.Required"));

            RuleFor(x => x.BannerCouponImage)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.BannerCouponImage.Required"));

            RuleFor(x => x.StandardCouponImage)
              .NotNull()
              .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.PrintableCouponImage.Required"));

            RuleFor(x => x.Disclaimer)
                   .NotNull()
                   .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.Disclaimer.Required"));

            RuleFor(x => x.UPC)
                 .NotNull()
                  .Length(11)
                  .WithMessage(localizationService.GetResource("Admin.ContentManagement.Upon.Fields.UPC.MustBeOff11Characters"));
                  


        }
    }
}