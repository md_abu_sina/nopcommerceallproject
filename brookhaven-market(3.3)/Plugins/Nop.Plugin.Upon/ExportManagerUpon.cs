﻿using AutoMapper;


using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
//using Nop.Services.PromotionFeed;
using Nop.Services.Shipping;
using Nop.Services.Tax;

using Nop.Plugin.Upon.Domain;
using Nop.Plugin.Upon.Models;
using Nop.Core;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;
using System;
using Nop.Core.Domain.Localization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Web.Hosting;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using System.Net;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections.Generic;
using System.Drawing;
using Nop.Core.Domain;
using System.Text;
using System.Xml;


namespace Nop.Plugin.Upon
{
    public static class ExportManagerUpon
    {


        /// <summary>
        /// Export Imprinted Upon lists to XLSX
        /// </summary>
        /// <param name="filePath">File path to use</param>
        /// <param name="imprintedUpons">imprintedUpons</param>
        public static void ExportImprintedUponsToXlsx(string filePath, List<ImprintedCoupon> imprintedUpons, StoreInformationSettings _storeInformationSettings, IStoreContext storeContext)
        {
            var newFile = new FileInfo(filePath);
            // ok, we can run the real code of the sample now
            using (var xlPackage = new ExcelPackage(newFile))
            {
                // uncomment this line if you want the XML written out to the outputDir
                //xlPackage.DebugMode = true; 

                // get handle to the existing worksheet
                var worksheet = xlPackage.Workbook.Worksheets.Add("imprintedupons");
                //Create Headers and format them
                var properties = new string[]
                    {
                        "Id",
                        "Title",
                        "Offer",
                        "Restrictions",
                        "First_Name ",
                        "Last_Name ",
                        "Email",
                        "CouponId",
                        "UniqueId_imprintedCoupon",
                        "CustomerID",
                        "ExpirationDate",
                        "Status",
                        "RedemptionDate",
                        "Amount",
                        "StartDate",
                        "EndDate",
                        "PdfPath",
                        "QrcodeImgPath",
                        "UPC",
                        "ImprintingDate"
                        
                     };
                for (int i = 0; i < properties.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = properties[i];
                    worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                    worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                }


                int row = 2;
                foreach (var imprintedupon in imprintedUpons)
                {
                    int col = 1;

                    worksheet.Cells[row, col].Value = imprintedupon.Id;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.Title;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.Offer;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.Restrictions;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.First_Name;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.Last_Name;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.Email;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.CouponId;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.UniqueId_imprintedCoupon;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.CustomerID;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.ExpirationDate;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.Status;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.RedemptionDate;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.Amount;
                    col++;

                    //roles
                    worksheet.Cells[row, col].Value = imprintedupon.StartDate;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.EndDate;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.PdfPath;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.QrcodeImgPath;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.UPC;
                    col++;

                    worksheet.Cells[row, col].Value = imprintedupon.ImprintingDate;
                    col++;

                   row++;
                }



                // we had better add some document properties to the spreadsheet 
							
                // set some core property values
								xlPackage.Workbook.Properties.Title = string.Format("{0} imprintedupons", storeContext.CurrentStore.Name);
								xlPackage.Workbook.Properties.Author = storeContext.CurrentStore.Name;
								xlPackage.Workbook.Properties.Subject = string.Format("{0} imprintedupons", storeContext.CurrentStore.Name);
								xlPackage.Workbook.Properties.Keywords = string.Format("{0} imprintedupons", storeContext.CurrentStore.Name);
                xlPackage.Workbook.Properties.Category = "imprintedupons";
								xlPackage.Workbook.Properties.Comments = string.Format("{0} imprintedupons", storeContext.CurrentStore.Name);

                // set some extended property values
								xlPackage.Workbook.Properties.Company = storeContext.CurrentStore.Name;
								xlPackage.Workbook.Properties.HyperlinkBase = new Uri(storeContext.CurrentStore.Url);

                // save the new spreadsheet
                xlPackage.Save();
            }
        }

        /// <summary>
        /// Export Imprinted Upon list to xml
        /// </summary>
        /// <param name="imprintedUpons">imprintedUpons</param>
        /// <returns>Result in XML format</returns>
        public static string ExportImprintedUponsToXml(List<ImprintedCoupon> imprintedUpons)
        {
            var sb = new StringBuilder();
            var stringWriter = new StringWriter(sb);
            var xmlWriter = new XmlTextWriter(stringWriter);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("imprintedupons");
            xmlWriter.WriteAttributeString("Version", NopVersion.CurrentVersion);

            foreach (var imprintedupon in imprintedUpons)
            {
                xmlWriter.WriteStartElement("imprintedupon");
               
                xmlWriter.WriteElementString("Id", null, imprintedupon.Id.ToString());
                xmlWriter.WriteElementString("Title", null, imprintedupon.Title);
                xmlWriter.WriteElementString("Offer", null, imprintedupon.Offer);
                xmlWriter.WriteElementString("Restrictions", null, imprintedupon.Restrictions);
                xmlWriter.WriteElementString("First_Name", null, imprintedupon.First_Name);
                xmlWriter.WriteElementString("Last_Name", null, imprintedupon.Last_Name);
                xmlWriter.WriteElementString("Email", null, imprintedupon.Email);
                xmlWriter.WriteElementString("CouponId", null, imprintedupon.CouponId.ToString());
                xmlWriter.WriteElementString("ExpirationDate", null, imprintedupon.ExpirationDate.ToString());
                xmlWriter.WriteElementString("Status", null, imprintedupon.Status);
                xmlWriter.WriteElementString("RedemptionDate", null, imprintedupon.RedemptionDate.ToString());
                xmlWriter.WriteElementString("Amount", null, imprintedupon.Amount);
                xmlWriter.WriteElementString("StartDate", null, imprintedupon.StartDate.ToString());
                xmlWriter.WriteElementString("EndDate", null, imprintedupon.EndDate.ToString());
                xmlWriter.WriteElementString("PdfPath", null, imprintedupon.PdfPath);
                xmlWriter.WriteElementString("QrcodeImgPath", null, imprintedupon.QrcodeImgPath);


                xmlWriter.WriteElementString("UPC", null, imprintedupon.UPC);
                xmlWriter.WriteElementString("ImprintingDate", null, imprintedupon.ImprintingDate.ToString());
               
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            return stringWriter.ToString();
        }

    }
}