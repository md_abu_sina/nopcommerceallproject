﻿using AutoMapper;


using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
//using Nop.Services.PromotionFeed;
using Nop.Services.Shipping;
using Nop.Services.Tax;

using Nop.Plugin.Upon.Domain;
using Nop.Plugin.Upon.Models;
using Nop.Core;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;
using System;
using Nop.Core.Domain.Localization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Web.Hosting;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using System.Net;
using System.Web;
//using System.Drawing;
//using System.Drawing;

namespace Nop.Plugin.Upon
{
    public static class CouponPdfmodule
    {

        public static void PrintToPdf(UniqueCoupon uniquecoupon, ImprintedCoupon imprintedcoupon, string qrCode, Language lang, string filePath, PdfSettings pdfsettings, string path)
        {
            if (uniquecoupon == null)
                throw new ArgumentNullException("uniquecoupon");

            if (imprintedcoupon == null)
                throw new ArgumentNullException("imprintedcoupon");

            if (lang == null)
                throw new ArgumentNullException("lang");

            if (String.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("filePath");


            var pageSize = PageSize.A4;

            //if (_pdfSettings.LetterPageSizeEnabled)
            //{
            //    pageSize = PageSize.LETTER;
            //}
           
            var doc = new Document(pageSize,0,0,0,0);
            PdfWriter.GetInstance(doc, new FileStream(filePath, FileMode.Create));
            doc.Open();
            
            
            //fonts
            var titleFont = GetFont(pdfsettings);
            titleFont.SetStyle(iTextSharp.text.Font.BOLD);
            titleFont.Color = BaseColor.BLACK;
            var font = GetFont(pdfsettings);

            Image uponimage = Image.GetInstance(path +  uniquecoupon.BannerCouponImage);//"Plugins/Upon/Content/images/imprint image.png");
           // uponimage.ScaleToFit(600f, 410f);
            var width = uponimage.PlainWidth;
            var height = uponimage.PlainHeight;
            var ratio = width / height;
         
            var newHeight1 = 600 / ratio;
            uponimage.ScaleAbsolute(600, newHeight1);
          
            uponimage.Alignment = Image.UNDERLYING;
            
            doc.Add(uponimage);
            doc.Add(new Paragraph(" ")); 
            doc.Add(new Paragraph(" "));
            doc.Add(new Paragraph(" "));
            doc.Add(new Paragraph(" "));


            Image img = Image.GetInstance(path + qrCode);
            img.ScaleToFit(75f,75f);
            img.Alignment = Image.ALIGN_RIGHT;
            //img.Top = 200;
            img.IndentationRight = 10f;
            //jpg.IndentationLeft = 9f;
           
            //img.SpacingAfter = 9f;
            //img.BorderWidthTop = 36f;
            doc.Add(img);
            doc.Add(new Paragraph(" "));
           
         
           
            Paragraph paragraph = new Paragraph(@"" + imprintedcoupon.First_Name + " " + imprintedcoupon.Last_Name);
            paragraph.Alignment = Element.ALIGN_RIGHT;
            paragraph.IndentationRight = 9f;
            paragraph.Font = titleFont;
            
            doc.Add(paragraph);
          
            Paragraph paragraph1 = new Paragraph(@"Email: " + imprintedcoupon.Email);
            paragraph1.Alignment = Element.ALIGN_RIGHT;
            paragraph1.IndentationRight = 9f;
            doc.Add(paragraph1);

            doc.Add(new Paragraph(" "));
            Image Standard_coupon_image = Image.GetInstance(path + uniquecoupon.StandardCouponImage);
         
            //Standard_coupon_image.BorderColor = iTextSharp.text.BaseColor.WHITE;
            //Standard_coupon_image.BorderWidthTop = 2f;
            ////Standard_coupon_image.ScaleAbsolute(600, 450);
            // Standard_coupon_image.PlainHeight = 700;
            // Standard_coupon_image.Alignment = Image.ALIGN_JUSTIFIED_ALL;
           


            var width1 = Standard_coupon_image.PlainWidth;
            var height1 = Standard_coupon_image.PlainHeight;
            var ratio1 = width1 / height1;
          
            var newHeight = 600 / ratio1;
            Standard_coupon_image.ScaleAbsolute(600, newHeight);
           
            doc.Add(Standard_coupon_image);

//            Image bottom_add = Image.GetInstance(path + "Plugins/Upon/Content/images/bottom ad image.png");
            //bottom_add.ScaleAbsolute(600, 200);
          //  bottom_add.ScaleAbsoluteWidth(600f);

          //  var width = bottom_add.PlainWidth;
          //  var height = bottom_add.PlainHeight;
          //  var ratio = width / height;
          //  //bottom_add.ScalePercent(ratio);
          //  bottom_add.ScaleAbsolute(600, 600 / ratio);
          //  Paragraph paragraph2 = new Paragraph(@"ratio: " + ratio + ",width:" + width + ", height:" + height + ", ratio of bear:" + ratio1 + ",width1" + width1 + ", height1: " + height1);
            
          //  doc.Add(paragraph2);
          ////  bottom_add.ScalePercent(100, 20);
          // // bottom_add.BorderWidthTop = 36f;
          // // bottom_add.Alignment = Image.UNDERLYING;
          //  doc.Add(bottom_add);
            doc.Close();
        }

        #region Utilities

        public static Font GetFont(PdfSettings pdfsettings)
        {
            //nopCommerce supports unicode characters
            //nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
            //It was downloaded from http://savannah.gnu.org/projects/freefont
            string fontPath = Path.Combine(MapPath("~/App_Data/Pdf/"), pdfsettings.FontFileName);
            var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            var font = new Font(baseFont, 10, Font.NORMAL);
            return font;
        }

        /// <summary>
        /// Maps a virtual path to a physical disk path.
        /// </summary>
        /// <param name="path">The path to map. E.g. "~/bin"</param>
        /// <returns>The physical path. E.g. "c:\inetpub\wwwroot\bin"</returns>
        public static  string MapPath(string path)
        {
            if (HostingEnvironment.IsHosted)
            {
                //hosted
                return HostingEnvironment.MapPath(path);
            }
            else
            {
                //not hosted. For example, run in unit tests
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                path = path.Replace("~/", "").TrimStart('/').Replace('/', '\\');
                return Path.Combine(baseDirectory, path);
            }
        }
        #endregion

   
    }
}