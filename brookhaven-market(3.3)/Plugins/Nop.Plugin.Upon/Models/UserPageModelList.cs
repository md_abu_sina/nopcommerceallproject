﻿
using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;


//using Nop.Web.Framework.Mvc;


namespace Nop.Plugin.Upon.Models
{

    public class UserPageModelList : BaseNopModel
    {

        public DataSourceResult imprintedcoupons { get; set; }

        public int Id { get; set; }



        public string Title { get; set; }



        public string Offer { get; set; }


        public string Restrictions { get; set; }


        public string First_Name { get; set; }


        public string Last_Name { get; set; }


        public string Email { get; set; }



        public int CouponId { get; set; }

        public Guid UniqueId_imprintedCoupon { get; set; }



        public int CustomerID { get; set; }



        public DateTime ExpirationDate { get; set; }


        public String Status { get; set; }

        [DataType(DataType.Currency)]
        public string AmountRangeFrom { get; set; }

        [DataType(DataType.Currency)]
        public string AmountRangeTo { get; set; }

        public DateTime StartDate { get; set; }


        public DateTime EndDate { get; set; }

				public bool ExportImport {	get;	set; }
    
        public string Description { get; set; }


        [NopResourceDisplayName("Admin.Upon.AllUpon.ImprintingDateStart")]
        [UIHint("DateNullable")]
        public DateTime? ImprintingDateStart { get; set; }

        [UIHint("DateNullable")]
        public DateTime? ImprintingDateEnd { get; set; }


        [UIHint("DateNullable")]
        public DateTime? ExpirationDateStart { get; set; }


        [UIHint("DateNullable")]
        public DateTime? ExpirationDateEnd { get; set; }


        [UIHint("DateNullable")]
        public DateTime? RedemptionDateStart { get; set; }

        [UIHint("DateNullable")]
        public DateTime? RedemptionDateEnd { get; set; }




    }

}
