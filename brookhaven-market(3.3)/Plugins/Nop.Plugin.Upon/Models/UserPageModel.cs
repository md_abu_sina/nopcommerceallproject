﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.Upon.Domain;
using Nop.Web.Framework.Mvc;
using FluentValidation.Attributes;
using Nop.Plugin.Upon.Validators;
using System.Web;
using Nop.Web.Models.Customer;

namespace Nop.Plugin.Upon.Models
{

    public class UserPageModel : BaseNopEntityModel
    {
        /// <summary>
        /// Gets or sets the ImprintedCoupon id.
        /// </summary>
        /// <value>
        /// The ImprintedCoupon id.
        /// </value>
        public int Id { get; set; }


        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title.
        /// </value>
        public string Title { get; set; }


        /// <summary>
        /// Gets or sets the Offer.
        /// </summary>
        /// <value>
        /// The Offer.
        /// </value>
        public string Offer { get; set; }

        /// <summary>
        /// Gets or sets the Restrictions.
        /// </summary>
        /// <value>
        /// The Restrictions.
        /// </value>
        public string Restrictions { get; set; }

        /// <summary>
        /// Gets or sets the first name of customer.
        /// </summary>
        /// <value>
        /// Customer's first name.
        /// </value>
        public string First_Name { get; set; }

        /// <summary>
        /// Gets or sets the last name of customer.
        /// </summary>
        /// <value>
        /// Customer's last name.
        /// </value>
        public string Last_Name { get; set; }


        /// <summary>
        /// Gets or sets the Email of customer.
        /// </summary>
        /// <value>
        /// Customer's Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Coupon id.
        /// </summary>
        /// <value>
        /// The Coupon id.
        /// </value>
        public int CouponId { get; set; }

        /// <summary>
        /// Gets or sets the Unique Id imprinted Coupon.
        /// </summary>
        /// <value>
        /// The Unique Id imprinted Coupon.
        /// </value>
        public Guid UniqueId_imprintedCoupon { get; set; }


        /// <summary>
        /// Gets or sets the CustomerID.
        /// </summary>
        /// <value>
        /// The CustomerID.
        /// </value>
        public int CustomerID { get; set; }


        /// <summary>
        /// Gets or sets the Expiration Date.
        /// </summary>
        /// <value>
        /// The Expiration Date.
        /// </value>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets Status.
        /// </summary>
        /// <value>
        /// Status.
        /// </value>
        public String Status { get; set; }


        /// <summary>
        /// Gets or sets Redemption Date.
        /// </summary>
        /// <value>
        /// Redemption Date.
        /// </value>
        public DateTime? RedemptionDate { get; set; }

        /// <summary>
        /// Gets or sets Amount.
        /// </summary>
        /// <value>
        /// Amount.
        /// </value>
        public string Amount { get; set; }

        /// <summary>
        /// Gets or sets Amount In Decimal.
        /// </summary>
        /// <value>
        /// Amount in decimal.
        /// </value>
        public double? AmountInDecimal { get; set; }

        /// <summary>
        /// Gets or sets the StartDate.
        /// </summary>
        /// <value>
        /// The StartDate. when the uPon will be published
        /// </value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate.
        /// </summary>
        /// <value>
        /// The EndDate.  when the uPon will be taken down
        /// </value>
        public DateTime EndDate { get; set; }


        public string PdfPath { get; set; }

        public string QrcodeImgPath { get; set; }

        /// <summary>
        /// Gets or sets the Coupon Image For Website.
        /// </summary>
        /// <value>
        /// The Coupon Image For Website.
        /// </value>
        public string CouponImageForWebsite { get; set; }

        /// <summary>
        /// Gets or sets the Banner Coupon Image.
        /// </summary>
        /// <value>
        /// The Banner Coupon Image.
        /// </value>
        public string BannerCouponImage { get; set; }

        /// <summary>
        /// Gets or sets the standard Coupon Image.
        /// </summary>
        /// <value>
        /// The standard Coupon Image.
        /// </value>
        public string StandardCouponImage { get; set; }

        public CustomerNavigationModel NavigationModel { get; set; }
      
        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        /// <value>
        /// The Description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets Imprinting Date.
        /// </summary>
        /// <value>
        /// Imprinting Date
        public DateTime? ImprintingDate { get; set; }

    }

}
