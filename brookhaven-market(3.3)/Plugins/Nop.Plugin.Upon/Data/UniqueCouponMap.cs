﻿using System.Data.Entity.ModelConfiguration;
using System;
using Nop.Plugin.Upon.Domain;

namespace Nop.Plugin.Upon.Data
{
    public class UniqueCouponMap: EntityTypeConfiguration<UniqueCoupon> {

        public UniqueCouponMap()
        {
            ToTable("Nop_UniqueCoupon");

            //Map the primary key
            HasKey(m => m.Id);

            //Map the additional properties
           
            Property(m => m.Title);
            Property(m => m.Offer);
            Property(m => m.Restrictions);
            Property(m => m.Description);
            Property(m => m.Quantity);
            Property(m => m.DateRange);
            Property(m => m.CouponImageForWebsite);
            Property(m => m.BannerCouponImage);
            Property(m => m.StandardCouponImage);
            Property(m => m.Disclaimer);
            Property(m => m.UPC);
            Property(m => m.barcodeImagePath);
           
            Property(m => m.StartDate);
            Property(m => m.EndDate);
            Property(m => m.Active);
        }
    }
}
