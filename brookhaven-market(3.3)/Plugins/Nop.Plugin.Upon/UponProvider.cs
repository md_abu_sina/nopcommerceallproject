﻿using Nop.Core.Plugins;
using Nop.Plugin.Upon.Data;
using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using Nop.Core.Data;
using Nop.Web.Framework.Menu;
//using TBarCode10Lib;

namespace Nop.Plugin.Upon
{
    public class UponProvider : BasePlugin, IAdminMenuPlugin
    {
        private readonly UniqueCouponObjectContext _uniquecouponcontext;
        private readonly ImprintedCouponObjectContext _imprintedcouponContext;
      

        public UponProvider(UniqueCouponObjectContext uniquecouponcontext, ImprintedCouponObjectContext imprintedcouponContext)
        {
            _uniquecouponcontext = uniquecouponcontext;
            // _standardcouponContext = standardcouponContext;
            _imprintedcouponContext = imprintedcouponContext;
            //TBarCode10 barcode = new TBarCode10();
            //barcode.BarcodeTypes = barcode.DataMatrix;
          
        }

        /*public void BuildMenuItem(Telerik.Web.Mvc.UI.MenuItemBuilder menuItemBuilder)
        {
            menuItemBuilder.Text("Coupons").Items(y =>
            {
                y.Add()
                    .Text("Unique Coupons")
                    .Url("/Admin/Plugin/Upon/UniqueCoupon/List");
                y.Add()
                    .Text("uPons Report")
                    .Url("/Admin/Plugin/Upon/uPonDashboard/Index");
            });
        }*/

				public SiteMapNode BuildMenuItem()
				{
					SiteMapNode node = new SiteMapNode
					{
						Visible = true,
						Title = "Coupons"
					};

					node.ChildNodes.Add(new SiteMapNode
					{
						Visible = true,
						Title = "Unique Coupons",
						Url = "/Admin/Plugin/Upon/UniqueCoupon/List"
					});
					node.ChildNodes.Add(new SiteMapNode
					{
						Visible = true,
						Title = "uPons Report",
						Url = "/Admin/Plugin/Upon/uPonDashboard/Index"
					});


					return node;
				}

        public override void Install()
        {

            //_uniquecouponcontext.InstallSchema();

            //_imprintedcouponContext.InstallSchema();
            base.Install();


            this.AddOrUpdatePluginLocaleResource("admin.contentmanagement.uniquecoupons", "Unique Coupons");
            this.AddOrUpdatePluginLocaleResource("admin.contentmanagement.events.edituniquecouponitemdetails", "Edit Unique Coupon Details");
            this.AddOrUpdatePluginLocaleResource("admin.contentmanagement.uniquecoupon.backtolist", "back to list");
            this.AddOrUpdatePluginLocaleResource("admin.contentmanagement.uniquecoupon.addnew", "Add New Unique Coupon");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Upon.Fields.UPC.MustBeOff11Characters", "UPC number Must Be Of 11 digits");
            this.AddOrUpdatePluginLocaleResource("admin.upondashboard ", "uPon Dashboard");
            


        }


				public bool Authenticate()
				{
					return true;
				}

       
    }
}
