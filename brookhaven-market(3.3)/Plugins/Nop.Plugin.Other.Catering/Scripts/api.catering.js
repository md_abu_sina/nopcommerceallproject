﻿
var Shipping = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    newAddress: function (isNew) {
        if (isNew) {
            this.resetSelectedAddress();
            $('#shipping-new-address-form').show();
        } else {
            $('#shipping-new-address-form').hide();
        }
    },

    resetSelectedAddress: function () {
        var selectElement = $('#shipping-address-select');
        if (selectElement) {
            selectElement.val('');
        }
    },

    save: function () {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('shipping');

        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        Checkout.setStepResponse(response);
    }
};

var cateringApi = function () { }

cateringApi.prototype.displayPopupContent = function (message, modal) {
    //types: success, error
    var container = $('#dialog-content');

    //we do not encode displayed message
    var htmlcode = '';
    if ((typeof message) == 'string') {
        htmlcode = '<p>' + message + '</p>';
    } else {
        for (var i = 0; i < message.length; i++) {
            htmlcode = htmlcode + '<p>' + message[i] + '</p>';
        }
    }

    container.html(htmlcode);

    var isModal = (modal ? true : false);
    container.dialog({
        dialogClass: "catering-popup",
        modal: isModal,
        minHeight:'none',
        height: 'auto',
        width: 'auto',
        maxWidth: '490px',
        hide: { effect: "fade", duration: 1000 }
    });
}

cateringApi.prototype.loadFoodMenu = function () {
    //var config = $.extend({
    //    success: function () { },
    //    error: function () { }
    //}, options);

    $.apiCall({
        url: '/catering/foodmenu',
        success: function (html) {
            $("#catering-main").html(html);
            displayAjaxLoading(false);
        }
    });

}

cateringApi.prototype.cancelEvent = function (eventId) {

    var cancelEvent;

    if (confirm("Want to remove this event?") == true) {
        displayAjaxLoading(true);
        $.apiCall({
            url: '/catering/cancel-event/' + eventId,
            success: function (html) {
                $(".event-brief").html(html);
                displayAjaxLoading(false);
                location.reload();
            }
        });
    } 

    displayAjaxLoading(false);
}

cateringApi.prototype.updateAttributeQuantity = function (attributeId) {
    displayAjaxLoading(true);
    var quantity = $(".quantity." + attributeId).val();
    $.apiCall({
        type: 'GET',
        url: '/catering/attribute-quantity',
        data: { productAttributeId: attributeId, paQuantity:quantity, updateQuantity:true },
        success: function (response) {
            displayBarNotification("This Product attribute has been updated successfuly", "success", 3000);
            
            $(".product-price span").text(response.product_price);
            $("#paQuantity").closest('li').find('label.price-adjust').html(response.price_string);
            $("#paQuantity").html(response.html);
            displayAjaxLoading(false);
        }
    });
}

cateringApi.prototype.showHideQuantity = function (sel) {

    $('.attribute-quantity').hide();
    var value = sel.value;
    $(".attribute-quantity." + value).show();
}

cateringApi.prototype.validateCart = function () {
    displayAjaxLoading(true);
    $.apiCall({
        url: '/catering/validate-cart',
        success: function (response) {
            //api.displayPopupContent(html, true);
            if (typeof response !== "object") {
                api.displayPopupContent(response, true);
            }
            else {
                window.location = "/cart";
            }
            displayAjaxLoading(false);
        }
    });
}

cateringApi.prototype.removeRestrictedItem = function () {
    displayAjaxLoading(true);
    $.apiCall({
        url: '/catering/validate-cart?removeRestrictedItems=true',
        success: function (response) {
            window.location = "/cart";
            
            displayAjaxLoading(false);
        }
    });
}

cateringApi.prototype.placeOrder = function (cafePickUp) {

    cafePickUp = typeof cafePickUp !== 'undefined' ? cafePickUp : false;

    displayAjaxLoading(true);
    $.apiCall({
        url: '/catering/place-order',
        data: { cafePickUp: cafePickUp },
        success: function (response) {
            if (typeof response !== "object") {
                api.displayPopupContent(response, true);
            }
            else {
                window.location = "/catering/foodmenu";
            }
            displayAjaxLoading(false);
        }
    });
}

cateringApi.prototype.menuRecommendation = function () {
    window.location = "/catering/foodmenu";
}

cateringApi.prototype.loadShippingForm = function () {
    displayAjaxLoading(true);
    $.apiCall({
        url: '/catering/shipping',
        success: function (html) {
            api.displayPopupContent(html, true);
            displayAjaxLoading(false);
        }
    });
}

cateringApi.prototype.loadBillingForm = function () {
    displayAjaxLoading(true);
    $.apiCall({
        url: '/catering/billing',
        success: function (html) {
            api.displayPopupContent(html, true);
            displayAjaxLoading(false);
        }
    });
}

cateringApi.prototype.storeSelect = function () {
    //var config = $.extend({
    //    success: function () { },
    //    error: function () { }
    //}, options);
    displayAjaxLoading(true);
    $.apiCall({
        url: '/catering/selectstore',
        success: function (html) {
            //$(".ajax-content").html(html);
            api.displayPopupContent(html, true);
            //window.history.back();
            //window.location.hash = '#/';
            displayAjaxLoading(false);
        }
    });

}

cateringApi.prototype.editShippingForm = function () {
    displayAjaxLoading(true);
    $.apiCall({
        type: 'GET',
        url: '/catering/shipping',
        data: { fromEdit: true },
        success: function (html) {
            api.displayPopupContent(html, true);
            displayAjaxLoading(false);
        }
    });
}

cateringApi.prototype.editStoreSelect = function () {
    //var config = $.extend({
    //    success: function () { },
    //    error: function () { }
    //}, options);
    displayAjaxLoading(true);
    $.apiCall({
        type: 'GET',
        url: '/catering/selectstore',
        data: { fromEdit: true },
        success: function (html) {
            api.displayPopupContent(html, true);
            displayAjaxLoading(false);
        }
    });

}

cateringApi.prototype.viewMap = function (location) {
    //var config = $.extend({
    //    data: {},
    //    success: function () { },
    //    error: function () { }
    //}, options);
    displayAjaxLoading(true);
    $.apiCall({
        type: 'GET',
        data: { systemName: location },
        url: '/catering/viewmap',
        success: function (html) {
            $(".store-map").html(html);
            //window.history.back();
            //window.location.hash = '#/';
            displayAjaxLoading(false);
        }
    });

}

cateringApi.prototype.previousPage = function () {
    window.history.back();
}

cateringApi.prototype.deliveryNextClick = function () {
    displayAjaxLoading(true);
    var formdata = $("#shipping-form").serialize();
    $.apiCall({
        type: 'POST',
        data: formdata,
        url: '/catering/saveShipping',
        success: function (data) {
            displayAjaxLoading(false);
            if (data.update_section.name === "shipping") {
                //$(".ajax-content").html(data.update_section.html.filter() );
                api.displayPopupContent(data.update_section.html, true);
            }
            else {
                $(".ajax-content").html(data.update_section.html);
                if ($('li.inactive').length > 0) {
                    $('li.inactive').addClass("active").removeClass("inactive");
                }
            }



        }
    });
    console.log("deliveryNextClick");
}

cateringApi.prototype.pickUpNextClick = function () {

    var selectedVal;
    var selected = $(".store-list input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
    }
    displayAjaxLoading(true);
    $.apiCall({
        type: 'GET',
        data: { storeId: selectedVal },
        url: '/catering/eventdetail',
        success: function (html) {
            $(".ajax-content").html(html);
            displayAjaxLoading(false);
            if ($('li.inactive').length > 0) {
                $('li.inactive').addClass("active").removeClass("inactive");
            }
        }
    });


    console.log("pickUpNextClick");
}

cateringApi.prototype.eventDetailNextClick = function () {

    displayAjaxLoading(true);
    var formdata = $("#eventdetail-form").serialize();
    $.apiCall({
        type: 'POST',
        data: formdata,
        url: '/catering/eventdetail',
        success: function (data) {
            displayAjaxLoading(false);
            if (data.success) {
                window.location = "/catering/foodmenu";
                $('#dialog-content').dialog("close");
            }
            else {
                $(".ajax-content").html(data);
            }

        }
    });
}

cateringApi.prototype.loadEventDetail = function () {
    //var config = $.extend({
    //    success: function () { },
    //    error: function () { }
    //}, options);
    displayAjaxLoading(true);
    $.apiCall({
        type: 'GET',
        url: '/catering/eventdetail',
        success: function (html) {
            $(".ajax-content").html(html);
            displayAjaxLoading(false);
        }
    });

}


cateringApi.prototype.viewSubCategory = function (options) {
    var config = $.extend({
        data: {},
        success: function () { },
        error: function () { }
    }, options);

    $.apiCall({
        type: 'GET',
        data: config.data,
        url: 'viewCategory',
        success: function (html) {
            $(".foodmenu-middle").html(html);
            $("div.middle-top").show();
            displayAjaxLoading(false);
        }
    });

}


cateringApi.prototype.viewCategoryProduct = function (options) {
    var config = $.extend({
        data: {},
        success: function () { },
        error: function () { }
    }, options);

    $.apiCall({
        type: 'GET',
        data: config.data,
        url: 'product',
        success: function (html) {
            $(".foodmenu-middle").html(html);
            $("div.middle-top").hide();
            api.viewCategoryProductInit();
            displayAjaxLoading(false);
        }
    });

}

cateringApi.prototype.viewCategoryProductInit = function () {
    $(".plus").click(function () {
        var currentValue = parseInt($(this).parent().find('span').text());
        $(this).parent().find('span').html(currentValue + 1);

        var cartLink = api.getChangedLink($(this).parent().prev().find('a.red-button').attr('href'), $(this).parent().find('span').html());
        var detailLink = api.getChangedLink($(this).parent().parent().find('.product-title>a').attr('href'), $(this).parent().find('span').html());

        $(this).parent().prev().find('a.red-button').attr('href', cartLink);
        $(this).parent().parent().find('.product-title>a').attr('href', detailLink);
    });

    $(".minus").click(function () {
        var currentValue = parseInt($(this).parent().find('span').text());
        if (currentValue > 1) {
            $(this).parent().find('span').html(currentValue - 1);

            var cartLink = api.getChangedLink($(this).parent().prev().find('a.red-button').attr('href'), $(this).parent().find('span').html());
            var detailLink = api.getChangedLink($(this).parent().parent().find('.product-title>a').attr('href'), $(this).parent().find('span').html());

            $(this).parent().prev().find('a.red-button').attr('href', cartLink);
            $(this).parent().parent().find('.product-title>a').attr('href', detailLink);
        }
    });
    $('.catering.pager a').click(function (event) {
        event.preventDefault();
        var pathArray = window.location.hash.split('/');

        var initialHash = pathArray[0] + "/" + pathArray[1] + "/" + pathArray[2] + "/";
        var currentPage = $('.catering.pager span').text();
        var clicked = $(this).text();
        if (clicked === 'Next') {
            window.location.hash = initialHash + (parseInt(currentPage) + 1);
            console.log(currentPage + 1);
            //console.log(clicked+'next');
        }
        else if (clicked === 'Previous') {
            window.location.hash = initialHash + (parseInt(currentPage) - 1);
            console.log(clicked);
        }
        else {
            window.location.hash = initialHash + clicked;
            var currentPage = $('.catering.pager span').text();
            console.log(clicked + 'number');
        }
    });

}

cateringApi.prototype.viewSearchProduct = function (options) {
    var config = $.extend({
        data: {},
        success: function () { },
        error: function () { }
    }, options);

    $.apiCall({
        type: 'GET',
        data: config.data,
        url: '/catering/search-food',
        success: function (html) {
            $(".foodmenu-middle").html(html);
            $("div.middle-top").hide();
            api.viewSearchProductInit();
            displayAjaxLoading(false);
        }
    });

}

cateringApi.prototype.viewSearchProductInit = function () {
    $(".plus").click(function () {
        var currentValue = parseInt($(this).parent().find('span').text());
        $(this).parent().find('span').html(currentValue + 1);

        var cartLink = api.getChangedLink($(this).parent().prev().find('a.red-button').attr('href'), $(this).parent().find('span').html());
        var detailLink = api.getChangedLink($(this).parent().parent().find('.product-title>a').attr('href'), $(this).parent().find('span').html());

        $(this).parent().prev().find('a.red-button').attr('href', cartLink);
        $(this).parent().parent().find('.product-title>a').attr('href', detailLink);
    });

    $(".minus").click(function () {
        var currentValue = parseInt($(this).parent().find('span').text());
        if (currentValue > 1) {
            $(this).parent().find('span').html(currentValue - 1);

            var cartLink = api.getChangedLink($(this).parent().prev().find('a.red-button').attr('href'), $(this).parent().find('span').html());
            var detailLink = api.getChangedLink($(this).parent().parent().find('.product-title>a').attr('href'), $(this).parent().find('span').html());

            $(this).parent().prev().find('a.red-button').attr('href', cartLink);
            $(this).parent().parent().find('.product-title>a').attr('href', detailLink);
        }
    });
    $('.catering.pager a').click(function (event) {
        event.preventDefault();
        var pathArray = window.location.hash.split('/');

        var initialHash = "#/search-food/";
        var currentPage = $('.catering.pager span').text();
        var clicked = $(this).text();
        if (clicked === 'Next') {
            window.location.hash = initialHash + (parseInt(currentPage) + 1);
            console.log(currentPage + 1);
            //console.log(clicked+'next');
        }
        else if (clicked === 'Previous') {
            window.location.hash = initialHash + (parseInt(currentPage) - 1);
            console.log(clicked);
        }
        else {
            window.location.hash = initialHash + clicked;
            var currentPage = $('.catering.pager span').text();
            console.log(clicked + 'number');
        }
    });

}

cateringApi.prototype.getChangedLink = function (link, changedPart) {
    //var cartLink = $(this).parent().prev().find('a.red-button').attr('href');
    //var detailLink = $(this).parent().parent().find('.product-title>a').attr('href');
    if (typeof link !== 'undefined') {
        var i = link.lastIndexOf('/');
        if (i != -1) {
            link = link.substr(0, i) + "/" + changedPart;
        }

        return link;
    }
    else {
        return 'javascript:void(0)';
    }
}

cateringApi.prototype.viewProductDetail = function (options) {

    //displayAjaxLoading(true);

    $.apiCall({
        type: 'GET',
        url: options,
        success: function (html) {
            $(".foodmenu-middle").html(html);
            $("div.middle-top").hide();
            api.viewProductDetailInit();
            //api.displayPopupContent(html, true);
            api.viewCategoryProductInit();
            displayAjaxLoading(false);
        }
    });

}

cateringApi.prototype.viewProductDetailInit = function () {

    //displayAjaxLoading(true);
    var url = location.hash;
    var value = url.substring(url.lastIndexOf('/') + 1);

    $(".add-to-cart-panel").find('.qty-input').val(value);

    $("li.check-select").click(function () {
        $(this).toggleClass('active');
        var checkBox = $(this).find("input[type=checkbox]");
        checkBox.prop("checked", !checkBox.prop("checked"));
    });
    //$("li.check-select [type = checkbox]").click(function () {
    $('li.check-select input[type="checkbox"]').click(function () {

        if ($(this).is(':checked'))
            $(this).closest('li.check-select').addClass('active');
        else
            $(this).closest('li.check-select').removeClass('active');
    });

}

/*cateringApi.prototype.loadShippingForm = function (options) {
    debugger;
    $.ajax({
        url: "catering/shipping",
        type: "POST",
        dataType: "html",
        success: options.success,
        error: function (result) {
            alert(result.responseText);
        }
    });
}*/


/*api.loadShippingForm({
    success: function (html) {
        this.displayPopupContent(html, true)
    }
});*/

function loadShippingForm() {
    $.ajax({
        url: "catering/shipping",
        type: "POST",
        dataType: "html"
    }).done(function (html) {
        displayPopupNotification(html, 'success', true)
    });
}


var api = new cateringApi();