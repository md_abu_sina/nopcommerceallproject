﻿using Nop.Core;
using Nop.Plugin.Other.Catering.Models;
using Nop.Plugin.Other.Catering.Services;
using Nop.Core.Domain.Messages;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Tasks;
using Nop.Services.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Services.Events;
using Nop.Core.Infrastructure;
using Nop.Services.Customers;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Common;
using Nop.Core.Data;

namespace Nop.Plugin.Other.Catering
{
    public class EndImpersonationSessionTask : ITask
    {
        private readonly IWorkContext _workContext;
        private readonly IRepository<GenericAttribute> _genericAttributeRepository;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerService _customerService;
        

        public EndImpersonationSessionTask(IWorkContext workContext, IGenericAttributeService genericAttributeService, 
                                            IRepository<GenericAttribute> genericAttributeRepository, ICustomerService customerService)
        {
            this._workContext = workContext;
            this._genericAttributeService = genericAttributeService;
            this._genericAttributeRepository = genericAttributeRepository;
            this._customerService = customerService;
        }

        ///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Execute task
		/// </summary>
        public void Execute()
        {
            //if (_workContext.OriginalCustomerIfImpersonated != null)
            //{
            var impersonatedUsers = (from ga in _genericAttributeRepository.Table
                                    where ga.KeyGroup.Equals("Customer") && ga.Key.Equals(SystemCustomerAttributeNames.ImpersonatedCustomerId) && !string.IsNullOrEmpty(ga.Value)
                                    select ga.EntityId).ToList();

            if (impersonatedUsers.Any())
            {
                //logout impersonated customer
                foreach (var impersonatedUser in impersonatedUsers)
                {
                    var customer = _customerService.GetCustomerById(impersonatedUser);
                    _genericAttributeService.SaveAttribute<int?>(customer,
                    SystemCustomerAttributeNames.ImpersonatedCustomerId, null);
                }    
            }
            

                

            //}
            
        }


        public int Order
        {
            //ensure that this task is run first 
            get { return 0; }
        }
    }
}
