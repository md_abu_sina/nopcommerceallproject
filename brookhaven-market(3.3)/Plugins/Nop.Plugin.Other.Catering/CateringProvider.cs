﻿using Nop.Core.Plugins;
using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using Nop.Core.Data;
using Nop.Plugin.Other.Catering.Data;
using Nop.Web.Framework.Menu;


namespace Nop.Plugin.Other.Catering
{
		public class CateringProvider : BasePlugin, IAdminMenuPlugin
    {

				private readonly CateringEventObjectContext _context;
				private readonly CateringEmailObjectContext _emailContext;

				public CateringProvider(CateringEventObjectContext context, CateringEmailObjectContext emailContext)
        {
					_context = context;
					_emailContext = emailContext;
        }

       
				public SiteMapNode BuildMenuItem()
				{
					SiteMapNode node = new SiteMapNode
					{
						Visible = true,
						Title = "Catering"
					};

					node.ChildNodes.Add(new SiteMapNode
					{
						Visible = true,
						Title = "Store Address",
						Url = "/Admin/Plugin/Catering/StoreAddress/List"
					});

					node.ChildNodes.Add(new SiteMapNode
					{
						Visible = true,
						Title = "Catering Event",
						Url = "/Admin/Plugin/Other/Catering/cateringList"
					});
					
					node.ChildNodes.Add(new SiteMapNode
					{
						Visible = true,
						Title = "Catering Email",
						Url = "/Admin/Plugin/Other/Catering/emailList"
					});

                    node.ChildNodes.Add(new SiteMapNode
                    {
                        Visible = true,
                        Title = "Catering Order",
                        Url = "/Admin/Plugin/Other/Catering/OrderList"
                    });

                    node.ChildNodes.Add(new SiteMapNode
                    {
                        Visible = true,
                        Title = "Catering Report Summary",
                        Url = "/Admin/Plugin/Other/Catering/ReportSummary"
                    });

                    node.ChildNodes.Add(new SiteMapNode
                    {
                        Visible = true,
                        Title = "Catering Report",
                        Url = "/Admin/Plugin/Other/Catering/Report"
                    });


					return node;
				}

        public override void Install()
        {
					_context.Install();
					_emailContext.Install();
          base.Install();
			  }

				public bool Authenticate()
				{
					return true;
				}

				/// <summary>
				/// Uninstall plugin
				/// </summary>
				public override void Uninstall()
				{
					_context.Uninstall();
					_emailContext.Uninstall();
					base.Uninstall();
				}
       
    }
}
