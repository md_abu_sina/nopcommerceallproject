using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Plugin.Other.Catering.Domain;

namespace Nop.Plugin.Other.Catering.Services
{
		public class CateringEmailService : ICateringEmailService
    {
        private readonly IRepository<CateringEmail> _cateringEmailRepository;


				public CateringEmailService(IRepository<CateringEmail> CateringEmailRepository)
        {
            _cateringEmailRepository = CateringEmailRepository;
        }

        #region Implementation of CateringEmailService

				/// <summary>
				/// insert Email
				/// </summary>
				/// <param name="EmailItem"></param>
				public virtual void InsertCateringEmail(CateringEmail cateringEmail)
				{
					if(cateringEmail == null)
						throw new ArgumentNullException("cateringEmail");


					_cateringEmailRepository.Insert(cateringEmail);

					
				}

				/// <summary>
				/// get Email  by id
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public CateringEmail GetCateringEmailById(int id)
				{
					var db = _cateringEmailRepository.Table;
					return db.SingleOrDefault(c => c.Id.Equals(id));
				}

				/// <summary>
				/// get Email  by id
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public string GetCateringEmailByStoreName(string storeName)
				{
					var db = _cateringEmailRepository.Table;
					return db.SingleOrDefault(c => c.StoreName.Equals(storeName)).EmailAddress;
				}	

				/// <summary>
				/// get Email  by id
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public List<CateringEmail> GetCateringEmailByStoreId(int storeId)
				{
					return 
							(from e in _cateringEmailRepository.Table
							where e.StoreId.Equals(storeId)
							select e).ToList();
				}

				/// <summary>
				/// get all cateringEmails
				/// </summary>
				/// <param name="pageIndex"></param>
				/// <param name="pageSize"></param>
				/// <returns>IPagedList<cateringEmails></returns>
				public virtual IPagedList<CateringEmail> GetAllCateringEmail(int pageIndex, int pageSize)
				{
					//var query = _eventRepository.Table;
					var query = (from u in _cateringEmailRepository.Table
											 orderby u.Id
											 select u);
					var cateringEmails = new PagedList<CateringEmail>(query, pageIndex, pageSize);
					return cateringEmails;
				}
				

				/// <summary>
				/// Update Email
				/// </summary>
				/// <param name="EmailItem"></param>
				public void UpdateCateringEmail(CateringEmail cateringEmail)
				{
					if(cateringEmail == null)
						throw new ArgumentNullException("cateringEmail");

					_cateringEmailRepository.Update(cateringEmail);

					//Email notification
					//_EmailPublisher.EntityUpdated(EmailItem);
				}

				/// <summary>
				/// delete Email
				/// </summary>
				/// <param name="EmailItem"></param>
				public void DeleteCateringEmail(CateringEmail cateringEmail)
				{
					_cateringEmailRepository.Delete(cateringEmail);
					//_EmailPublisher.EntityDeleted(EmailItem);
				}

        #endregion
    }
}