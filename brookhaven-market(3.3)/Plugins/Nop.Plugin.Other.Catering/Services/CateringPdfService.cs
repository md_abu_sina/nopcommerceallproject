﻿using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core;
using Nop.Core.Domain.Localization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using Nop.Core.Domain.Common;
using Nop.Services.Media;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Helpers;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Stores;
using Nop.Services.Configuration;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Tax;
using System.Globalization;
using Nop.Core.Domain.Shipping;
using Nop.Core.Html;
using iTextSharp.text.pdf.draw;
using Nop.Plugin.Other.Catering.Domain;
using Nop.Services.Customers;
using Nop.Services.Common;
using Nop.Plugin.Other.Catering.Models;

namespace Nop.Plugin.Other.Catering.Services
{
    public class CateringPdfService : ICateringPdfService
    {
        #region field

        private readonly ILocalizationService _localizationService;
				private readonly ILanguageService _languageService;
				private readonly IWorkContext _workContext;
				private readonly IOrderService _orderService;
				private readonly IPaymentService _paymentService;
				private readonly IDateTimeHelper _dateTimeHelper;
				private readonly IPriceFormatter _priceFormatter;
				private readonly ICurrencyService _currencyService;
				private readonly IMeasureService _measureService;
				private readonly IPictureService _pictureService;
				private readonly IProductService _productService;
				private readonly IProductAttributeParser _productAttributeParser;
				private readonly IStoreService _storeService;
				private readonly IStoreContext _storeContext;
				private readonly ICustomerService _customerService;
				private readonly ISettingService _settingContext;
				private readonly IWebHelper _webHelper;

				private readonly CatalogSettings _catalogSettings;
				private readonly CurrencySettings _currencySettings;
				private readonly MeasureSettings _measureSettings;
				private readonly PdfSettings _pdfSettings;
				private readonly TaxSettings _taxSettings;
				private readonly AddressSettings _addressSettings;
				private readonly IGenericAttributeService _genericAttributeService;

        #endregion

        #region ctor

				public CateringPdfService(ILocalizationService localizationService,
						ILanguageService languageService,
						IWorkContext workContext,
						IOrderService orderService,
						IPaymentService paymentService,
						IDateTimeHelper dateTimeHelper,
						IPriceFormatter priceFormatter,
						ICurrencyService currencyService,
						IMeasureService measureService,
						IPictureService pictureService,
						IProductService productService,
						IProductAttributeParser productAttributeParser,
						IStoreService storeService,
						IStoreContext storeContext,
						ICustomerService customerService,
						ISettingService settingContext,
						IWebHelper webHelper,
						CatalogSettings catalogSettings,
						CurrencySettings currencySettings,
						MeasureSettings measureSettings,
						PdfSettings pdfSettings,
						TaxSettings taxSettings,
						AddressSettings addressSettings,
					  IGenericAttributeService genericAttributeService)
				{
					this._localizationService = localizationService;
					this._languageService = languageService;
					this._workContext = workContext;
					this._orderService = orderService;
					this._paymentService = paymentService;
					this._dateTimeHelper = dateTimeHelper;
					this._priceFormatter = priceFormatter;
					this._currencyService = currencyService;
					this._measureService = measureService;
					this._pictureService = pictureService;
					this._productService = productService;
					this._productAttributeParser = productAttributeParser;
					this._storeService = storeService;
					this._storeContext = storeContext;
					this._customerService = customerService;
					this._settingContext = settingContext;
					this._webHelper = webHelper;
					this._currencySettings = currencySettings;
					this._catalogSettings = catalogSettings;
					this._measureSettings = measureSettings;
					this._pdfSettings = pdfSettings;
					this._taxSettings = taxSettings;
					this._addressSettings = addressSettings;
					this._genericAttributeService = genericAttributeService;
				}

        #endregion

        #region Implementation of ICateringPdfService

				/// <summary>
				/// Print an order to PDF
				/// </summary>
				/// <param name="order">Order</param>
				/// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
				/// <returns>A path of generates file</returns>
				public virtual string PrintOrderToPdf(Order order, int languageId, CateringEvent cateringEvent, Dictionary<int, string> secondaryStore = null)
				{
					if(order == null)
						throw new ArgumentNullException("order");

					string fileName = string.Format("order_{0}_{1}.pdf", order.OrderGuid, CommonHelper.GenerateRandomDigitCode(4));
					string filePath = Path.Combine(_webHelper.MapPath("~/content/files/ExportImport"), fileName);
					using(var fileStream = new FileStream(filePath, FileMode.Create))
					{
						var orders = new List<Order>();
						orders.Add(order);
						PrintOrdersToPdf(fileStream, orders, cateringEvent, languageId, secondaryStore);
					}
					return filePath;
				}

				/// <summary>
				/// Print orders to PDF
				/// </summary>
				/// <param name="stream">Stream</param>
				/// <param name="orders">Orders</param>
				/// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
				public virtual void PrintOrdersToPdf(Stream stream, IList<Order> orders, CateringEvent cateringEvent, int languageId = 0, Dictionary<int, string> secondaryStore = null)
				{
					if(stream == null)
						throw new ArgumentNullException("stream");

					if(orders == null)
						throw new ArgumentNullException("orders");

					var pageSize = PageSize.A4;

					if(_pdfSettings.LetterPageSizeEnabled)
					{
						pageSize = PageSize.LETTER;
					}


					var doc = new Document(pageSize);
					var pdfWriter = PdfWriter.GetInstance(doc, stream);
					doc.Open();

					//fonts
					var orangeTextFont = GetFontBold();
					orangeTextFont.Color = new BaseColor(129, 82, 45);
					
					var whiteTextFont = GetFontMinion();

					var titleFont = GetFontBold();
					
					
					var font = GetFont();


					var attributesFont = GetFont();
					attributesFont.SetStyle(Font.ITALIC);

					int ordCount = orders.Count;
					int ordNum = 0;

					foreach(var order in orders)
					{
						//by default _pdfSettings contains settings for the current active store
						//and we need PdfSettings for the store which was used to place an order
						//so let's load it based on a store of the current order
						var pdfSettingsByStore = _settingContext.LoadSetting<PdfSettings>(order.StoreId);


						var lang = _languageService.GetLanguageById(languageId == 0 ? order.CustomerLanguageId : languageId);
						if(lang == null || !lang.Published)
							lang = _workContext.WorkingLanguage;

						#region Header

						//logo
						var logoPicture = _pictureService.GetPictureById(pdfSettingsByStore.LogoPictureId);
						var paidPicture = _pictureService.GetPictureById(428);//1090
						var logoExists = logoPicture != null;

						//header
						var headerTable = new PdfPTable(logoExists ? 3 : 1);
						headerTable.WidthPercentage = 100f;
						if(logoExists)
							headerTable.SetWidths(new[] { 40, 30, 30 });

						//logo
						if(logoExists)
						{
							var logoFilePath = _pictureService.GetThumbLocalPath(logoPicture, 300, false);
							var logoFile = Image.GetInstance(logoFilePath);
							logoFile.ScalePercent(60);
							var cellLogo = new PdfPCell(logoFile);
							cellLogo.Border = Rectangle.NO_BORDER;


							var paidFilePath = _pictureService.GetThumbLocalPath(paidPicture, 160, false);
							var paidFile = Image.GetInstance(paidFilePath);
							paidFile.Alignment = Image.ALIGN_RIGHT;
							paidFile.ScalePercent(60);
							var cellPaid = new PdfPCell(paidFile);
							cellPaid.Border = Rectangle.NO_BORDER;
							
							headerTable.AddCell(cellLogo);
							headerTable.AddCell(cellPaid);
						}
						//store info
						var cell = new PdfPCell();
						cell.Border = Rectangle.NO_BORDER;
						cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("cateringinvoice.date", lang.Id), _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc).ToString("D", new CultureInfo(lang.LanguageCulture))), titleFont));
						//cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Order#", lang.Id), order.Id), titleFont));
						//var store = _storeService.GetStoreById(order.StoreId) ?? _storeContext.CurrentStore;
						//var anchor = new Anchor(store.Url.Trim(new char[] { '/' }), font);
						//anchor.Reference = store.Url;
						//cell.AddElement(new Paragraph(anchor));
						cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("cateringinvoice.order", lang.Id), order.Id), titleFont));
						
						//payment method
						var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
						string paymentMethodStr = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, lang.Id) : order.PaymentMethodSystemName;
						if(!String.IsNullOrEmpty(paymentMethodStr))
						{
							cell.AddElement(new Paragraph(" "));
							cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.paymentmethod", lang.Id), titleFont));
							cell.AddElement(new Paragraph(paymentMethodStr, font));
							cell.AddElement(new Paragraph(" "));
						}

						headerTable.AddCell(cell);
						doc.Add(headerTable);

						#endregion
						
						DottedLineSeparator separator = new DottedLineSeparator();
						Chunk linebreak = new Chunk(separator);
						separator.LineColor = BaseColor.LIGHT_GRAY;
						doc.Add(linebreak);
						
						#region Addresses

						var addressTable = new PdfPTable(2);
						addressTable.WidthPercentage = 100f;
						addressTable.SetWidths(new[] { 50, 50 });

						//billing info
						cell = new PdfPCell();
						cell.Border = Rectangle.NO_BORDER;
						cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.customerinfo", lang.Id), orangeTextFont));

						if(_addressSettings.CompanyEnabled && !String.IsNullOrEmpty(order.BillingAddress.Company))
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Company", lang.Id), order.BillingAddress.Company), font));

						cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Name", lang.Id), order.BillingAddress.FirstName + " " + order.BillingAddress.LastName), font));
						if(_addressSettings.PhoneEnabled)
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Phone", lang.Id), order.BillingAddress.PhoneNumber), font));
						if(_addressSettings.FaxEnabled && !String.IsNullOrEmpty(order.BillingAddress.FaxNumber))
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Fax", lang.Id), order.BillingAddress.FaxNumber), font));
						if(_addressSettings.StreetAddressEnabled)
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Address", lang.Id), order.BillingAddress.Address1), font));
						if(_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(order.BillingAddress.Address2))
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Address2", lang.Id), order.BillingAddress.Address2), font));
						if(_addressSettings.CityEnabled || _addressSettings.StateProvinceEnabled || _addressSettings.ZipPostalCodeEnabled)
							cell.AddElement(new Paragraph(String.Format("{0}, {1} {2}", order.BillingAddress.City, order.BillingAddress.StateProvince != null ? order.BillingAddress.StateProvince.GetLocalized(x => x.Name, lang.Id) : "", order.BillingAddress.ZipPostalCode), font));
						if(_addressSettings.CountryEnabled && order.BillingAddress.Country != null)
							cell.AddElement(new Paragraph(String.Format("{0}", order.BillingAddress.Country != null ? order.BillingAddress.Country.GetLocalized(x => x.Name, lang.Id) : ""), font));

						//VAT number
						if(!String.IsNullOrEmpty(order.VatNumber))
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.VATNumber", lang.Id), order.VatNumber), font));

						//payment method
						/*var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
						string paymentMethodStr = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, lang.Id) : order.PaymentMethodSystemName;
						if(!String.IsNullOrEmpty(paymentMethodStr))
						{
							cell.AddElement(new Paragraph(" "));
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.PaymentMethod", lang.Id), paymentMethodStr), font));
							cell.AddElement(new Paragraph());
						}*/

						//purchase order number (we have to find a better to inject this information because it's related to a certain plugin)
						if(paymentMethod != null && paymentMethod.PluginDescriptor.SystemName.Equals("Payments.PurchaseOrder", StringComparison.InvariantCultureIgnoreCase))
						{
							cell.AddElement(new Paragraph(" "));
							cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.PurchaseOrderNumber", lang.Id), order.PurchaseOrderNumber), font));
							cell.AddElement(new Paragraph());
						}

						addressTable.AddCell(cell);
						var impersonatedBy = _genericAttributeService.GetAttributesForEntity(order.Id, "Order");

						if(cateringEvent.CafePickup)
						{
							cell = new PdfPCell();
							cell.Border = Rectangle.NO_BORDER;

							cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.storepickup", lang.Id), orangeTextFont));
							cell.AddElement(new Paragraph(_customerService.GetCustomerRoleById(cateringEvent.SupplierStoreId).Name, orangeTextFont));

							
							if(impersonatedBy.Count > 0)
							{
								cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.impersonatedBy", lang.Id), orangeTextFont));
								cell.AddElement(new Paragraph(impersonatedBy.FirstOrDefault().Value, orangeTextFont));
							}

							addressTable.AddCell(cell);
						}
						else
						{
							//shipping info
							if(order.ShippingStatus != ShippingStatus.ShippingNotRequired)
							{
								if(order.ShippingAddress == null)
									throw new NopException(string.Format("Shipping is required, but address is not available. Order ID = {0}", order.Id));
								cell = new PdfPCell();
								cell.Border = Rectangle.NO_BORDER;

								cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.deliveryinfo", lang.Id), orangeTextFont));
								if(!String.IsNullOrEmpty(order.ShippingAddress.Company))
									cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Company", lang.Id), order.ShippingAddress.Company), font));
								cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Name", lang.Id), order.ShippingAddress.FirstName + " " + order.ShippingAddress.LastName), font));
								if(_addressSettings.PhoneEnabled)
									cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Phone", lang.Id), order.ShippingAddress.PhoneNumber), font));
								if(_addressSettings.FaxEnabled && !String.IsNullOrEmpty(order.ShippingAddress.FaxNumber))
									cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Fax", lang.Id), order.ShippingAddress.FaxNumber), font));
								if(_addressSettings.StreetAddressEnabled)
									cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Address", lang.Id), order.ShippingAddress.Address1), font));
								if(_addressSettings.StreetAddress2Enabled && !String.IsNullOrEmpty(order.ShippingAddress.Address2))
									cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.Address2", lang.Id), order.ShippingAddress.Address2), font));
								if(_addressSettings.CityEnabled || _addressSettings.StateProvinceEnabled || _addressSettings.ZipPostalCodeEnabled)
									cell.AddElement(new Paragraph(String.Format("{0}, {1} {2}", order.ShippingAddress.City, order.ShippingAddress.StateProvince != null ? order.ShippingAddress.StateProvince.GetLocalized(x => x.Name, lang.Id) : "", order.ShippingAddress.ZipPostalCode), font));
								if(_addressSettings.CountryEnabled && order.ShippingAddress.Country != null)
									cell.AddElement(new Paragraph(String.Format("{0}", order.ShippingAddress.Country != null ? order.ShippingAddress.Country.GetLocalized(x => x.Name, lang.Id) : ""), font));
								cell.AddElement(new Paragraph(" "));
								/*cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("PDFInvoice.ShippingMethod", lang.Id), order.ShippingMethod), font));
								cell.AddElement(new Paragraph());*/

								if(impersonatedBy.Count > 0)
								{
									cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.impersonatedBy", lang.Id), orangeTextFont));
									cell.AddElement(new Paragraph(impersonatedBy.FirstOrDefault().Value, orangeTextFont));
								}

								addressTable.AddCell(cell);
							}
							else
							{
								cell = new PdfPCell(new Phrase(" "));
								cell.Border = Rectangle.NO_BORDER;

								if(impersonatedBy.Count > 0)
								{
									cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.impersonatedBy", lang.Id), orangeTextFont));
									cell.AddElement(new Paragraph(impersonatedBy.FirstOrDefault().Value, orangeTextFont));
								}

								addressTable.AddCell(cell);
							}
						}
						
						doc.Add(addressTable);
						#endregion
						doc.Add(linebreak);
						#region Products
						//products
						//header
						var middelHeaderTable = new PdfPTable(2);
						middelHeaderTable.WidthPercentage = 100f;
						middelHeaderTable.SetWidths(new[] { 50, 50 });

						cell = new PdfPCell();
						cell.Border = Rectangle.NO_BORDER;
						cell.AddElement(new Paragraph(_localizationService.GetResource("PDFInvoice.Product(s)", lang.Id), orangeTextFont));

						middelHeaderTable.AddCell(cell);

						cell = new PdfPCell();
						cell.Border = Rectangle.NO_BORDER;
						//cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.eventtime", lang.Id), titleFont));
						cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("cateringinvoice.eventtime", lang.Id), cateringEvent.DeliveryTime.ToString("dddd dd MMMM h:mm tt yyyy", new CultureInfo(lang.LanguageCulture))), titleFont));

						middelHeaderTable.AddCell(cell);

						//doc.Add(new Paragraph(_localizationService.GetResource("PDFInvoice.Product(s)", lang.Id), orangeTextFont));
						doc.Add(middelHeaderTable);
						doc.Add(new Paragraph(" "));


						var orderItems = _orderService.GetAllOrderItems(order.Id, null, null, null, null, null, null);

						var productsTable = new PdfPTable(secondaryStore != null ? 6 : 5);
						productsTable.WidthPercentage = 100f;
						productsTable.SetWidths(secondaryStore != null ? new[] { 40, 15, 10, 10, 15, 10 } : new[] { 40, 15, 15, 15, 15 });

						//product name
						cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductName", lang.Id), whiteTextFont));
						cell.BackgroundColor = new BaseColor(129, 82, 45);
						cell.HorizontalAlignment = Element.ALIGN_CENTER;
						cell.VerticalAlignment = Element.ALIGN_MIDDLE;
						productsTable.AddCell(cell);

						//SKU
						//if(_catalogSettings.ShowProductSku)
						//{
						cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.SKU", lang.Id), whiteTextFont));
						cell.BackgroundColor = new BaseColor(129, 82, 45);
						cell.HorizontalAlignment = Element.ALIGN_CENTER;
						cell.VerticalAlignment = Element.ALIGN_MIDDLE;
						productsTable.AddCell(cell);
						//}

						//price
						cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductPrice", lang.Id), whiteTextFont));
						cell.BackgroundColor = new BaseColor(129, 82, 45);
						cell.HorizontalAlignment = Element.ALIGN_CENTER;
						cell.VerticalAlignment = Element.ALIGN_MIDDLE;
						productsTable.AddCell(cell);

						//qty
						cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductQuantity", lang.Id), whiteTextFont));
						cell.BackgroundColor = new BaseColor(129, 82, 45);
						cell.HorizontalAlignment = Element.ALIGN_CENTER;
						cell.VerticalAlignment = Element.ALIGN_MIDDLE;
						productsTable.AddCell(cell);

						if(secondaryStore != null)
						{
							cell = new PdfPCell(new Phrase(_localizationService.GetResource("Messages.Order.Product(s).SecondaryStore", lang.Id), whiteTextFont));
							cell.BackgroundColor = new BaseColor(129, 82, 45);
							cell.HorizontalAlignment = Element.ALIGN_CENTER;
							cell.VerticalAlignment = Element.ALIGN_MIDDLE;
							productsTable.AddCell(cell);
						}

						//total
						cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.ProductTotal", lang.Id), whiteTextFont));
						cell.BackgroundColor = new BaseColor(129, 82, 45);
						cell.HorizontalAlignment = Element.ALIGN_CENTER;
						cell.VerticalAlignment = Element.ALIGN_MIDDLE;
						productsTable.AddCell(cell);

						for(int i = 0; i < orderItems.Count; i++)
						{
							var orderItem = orderItems[i];
							var p = orderItem.Product;

							//product name
							string name = p.GetLocalized(x => x.Name, lang.Id);
							cell = new PdfPCell();
							cell.AddElement(new Paragraph(name, font));
							cell.HorizontalAlignment = Element.ALIGN_LEFT;
							var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(orderItem.AttributeDescription, true, true), attributesFont);
							cell.AddElement(attributesParagraph);
							productsTable.AddCell(cell);

							//SKU
							//if(_catalogSettings.ShowProductSku)
							//{
							var sku = p.FormatSku(orderItem.AttributesXml, _productAttributeParser);
							cell = new PdfPCell(new Phrase(sku ?? String.Empty, font));
							cell.HorizontalAlignment = Element.ALIGN_CENTER;
							productsTable.AddCell(cell);
							//}

							//price
							string unitPrice = string.Empty;
							//excluding tax
							var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
							unitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);
							/*if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
							{
								//including tax
								var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
								unitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);
							}
							else
							{
								//excluding tax
								var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
								unitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);
							}*/
							cell = new PdfPCell(new Phrase(unitPrice, font));
							cell.HorizontalAlignment = Element.ALIGN_LEFT;
							productsTable.AddCell(cell);

							//qty
							cell = new PdfPCell(new Phrase(orderItem.Quantity.ToString(), font));
							cell.HorizontalAlignment = Element.ALIGN_LEFT;
							productsTable.AddCell(cell);

							if(secondaryStore != null)
							{
								//supplier store
								string secondarySupplier = secondaryStore.ContainsKey(orderItem.Id) ? secondaryStore[orderItem.Id] : string.Empty;
								//sb.AppendLine(string.Format("<td style=\"padding: 0.6em 0.4em;text-align: right;font-weight: bold;\">{0}</td>", secondarySupplier));
								cell = new PdfPCell(new Phrase(secondarySupplier, font));
								cell.HorizontalAlignment = Element.ALIGN_LEFT;
								productsTable.AddCell(cell);
							}
							//total
							string subTotal = string.Empty;
							//excluding tax
							var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
							subTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);
							/*if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
							{
								//including tax
								var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
								subTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);
							}
							else
							{
								//excluding tax
								var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
								subTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);
							}*/
							cell = new PdfPCell(new Phrase(subTotal, font));
							cell.HorizontalAlignment = Element.ALIGN_LEFT;
							productsTable.AddCell(cell);
						}
						doc.Add(productsTable);

						#endregion

						#region Checkout attributes

						/*if(!String.IsNullOrEmpty(order.CheckoutAttributeDescription))
						{
							doc.Add(new Paragraph(" "));
							string attributes = HtmlHelper.ConvertHtmlToPlainText(order.CheckoutAttributeDescription, true, true);
							var pCheckoutAttributes = new Paragraph(attributes, font);
							pCheckoutAttributes.Alignment = Element.ALIGN_RIGHT;
							doc.Add(pCheckoutAttributes);
							doc.Add(new Paragraph(" "));
						}*/

						#endregion

						var footerTotalTable = new PdfPTable(2);
						footerTotalTable.WidthPercentage = 100f;
						footerTotalTable.SetWidths(new[] { 50, 50 });

						cell = new PdfPCell();
						cell.Border = Rectangle.NO_BORDER;
						cell.AddElement(new Paragraph(""));
						cell.AddElement(new Paragraph(""));
						cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.quality", lang.Id), font));
						cell.AddElement(new Paragraph(_localizationService.GetResource("cateringinvoice.packagenumber", lang.Id), font));

						footerTotalTable.AddCell(cell);

						cell = new PdfPCell();
						cell.Border = Rectangle.NO_BORDER;
						//cell.AddElement(new Paragraph(_localizationService.GetResource("PDFInvoice.Product(s)", lang.Id), titleFont));

						

						#region Totals

						//subtotal
						cell.AddElement(new Paragraph(" "));



						//order subtotal
						if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
						{
							//including tax

							var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
							string orderSubtotalInclTaxStr = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

							var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Sub-Total", lang.Id), orderSubtotalInclTaxStr), font);
							p.Alignment = Element.ALIGN_RIGHT;
							cell.AddElement(p);
						}
						else
						{
							//excluding tax

							var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
							string orderSubtotalExclTaxStr = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

							var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Sub-Total", lang.Id), orderSubtotalExclTaxStr), font);
							p.Alignment = Element.ALIGN_RIGHT;
							cell.AddElement(p);
						}

						//discount (applied to order subtotal)
						if(order.OrderSubTotalDiscountExclTax > decimal.Zero)
						{
							//order subtotal
							if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
							{
								//including tax

								var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
								string orderSubTotalDiscountInCustomerCurrencyStr = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

								var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Discount", lang.Id), orderSubTotalDiscountInCustomerCurrencyStr), font);
								p.Alignment = Element.ALIGN_RIGHT;
								cell.AddElement(p);
							}
							else
							{
								//excluding tax

								var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
								string orderSubTotalDiscountInCustomerCurrencyStr = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

								var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Discount", lang.Id), orderSubTotalDiscountInCustomerCurrencyStr), font);
								p.Alignment = Element.ALIGN_RIGHT;
								cell.AddElement(p);
							}
						}

						//shipping
						if(order.ShippingStatus != ShippingStatus.ShippingNotRequired)
						{
							if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
							{
								//including tax
								var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
								string orderShippingInclTaxStr = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

								var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Shipping", lang.Id), orderShippingInclTaxStr), font);
								p.Alignment = Element.ALIGN_RIGHT;
								cell.AddElement(p);
							}
							else
							{
								//excluding tax
								var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
								string orderShippingExclTaxStr = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

								var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Shipping", lang.Id), orderShippingExclTaxStr), font);
								p.Alignment = Element.ALIGN_RIGHT;
								cell.AddElement(p);
							}
						}

						//payment fee
						if(order.PaymentMethodAdditionalFeeExclTax > decimal.Zero)
						{
							if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
							{
								//including tax
								var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
								string paymentMethodAdditionalFeeInclTaxStr = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, true);

								var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.PaymentMethodAdditionalFee", lang.Id), paymentMethodAdditionalFeeInclTaxStr), font);
								p.Alignment = Element.ALIGN_RIGHT;
								cell.AddElement(p);
							}
							else
							{
								//excluding tax
								var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
								string paymentMethodAdditionalFeeExclTaxStr = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, lang, false);

								var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.PaymentMethodAdditionalFee", lang.Id), paymentMethodAdditionalFeeExclTaxStr), font);
								p.Alignment = Element.ALIGN_RIGHT;
								cell.AddElement(p);
							}
						}

						//tax
						string taxStr = string.Empty;
						var taxRates = new SortedDictionary<decimal, decimal>();
						bool displayTax = true;
						bool displayTaxRates = true;
						if(_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
						{
							displayTax = false;
						}
						else
						{
							if(order.OrderTax == 0 && _taxSettings.HideZeroTax)
							{
								displayTax = false;
								displayTaxRates = false;
							}
							else
							{
								taxRates = order.TaxRatesDictionary;

								displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
								displayTax = !displayTaxRates;

								var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
								taxStr = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, lang);
							}
						}
						if(displayTax)
						{
							var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Tax", lang.Id), taxStr), font);
							p.Alignment = Element.ALIGN_RIGHT;
							cell.AddElement(p);
						}
						if(displayTaxRates)
						{
							foreach(var item in taxRates)
							{
								string taxRate = String.Format(_localizationService.GetResource("PDFInvoice.TaxRate", lang.Id), _priceFormatter.FormatTaxRate(item.Key));
								string taxValue = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(item.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, lang);

								var p = new Paragraph(String.Format("{0} {1}", taxRate, taxValue), font);
								p.Alignment = Element.ALIGN_RIGHT;
								cell.AddElement(p);
							}
						}

						//discount (applied to order total)
						if(order.OrderDiscount > decimal.Zero)
						{
							var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
							string orderDiscountInCustomerCurrencyStr = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, lang);

							var p = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.Discount", lang.Id), orderDiscountInCustomerCurrencyStr), font);
							p.Alignment = Element.ALIGN_RIGHT;
							cell.AddElement(p);
						}

						//gift cards
						foreach(var gcuh in order.GiftCardUsageHistory)
						{
							string gcTitle = string.Format(_localizationService.GetResource("PDFInvoice.GiftCardInfo", lang.Id), gcuh.GiftCard.GiftCardCouponCode);
							string gcAmountStr = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, lang);

							var p = new Paragraph(String.Format("{0} {1}", gcTitle, gcAmountStr), font);
							p.Alignment = Element.ALIGN_RIGHT;
							cell.AddElement(p);
						}

						//reward points
						if(order.RedeemedRewardPointsEntry != null)
						{
							string rpTitle = string.Format(_localizationService.GetResource("PDFInvoice.RewardPoints", lang.Id), -order.RedeemedRewardPointsEntry.Points);
							string rpAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, lang);

							var p = new Paragraph(String.Format("{0} {1}", rpTitle, rpAmount), font);
							p.Alignment = Element.ALIGN_RIGHT;
							cell.AddElement(p);
						}

						//order total
						var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
						string orderTotalStr = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, lang);


						var pTotal = new Paragraph(String.Format("{0} {1}", _localizationService.GetResource("PDFInvoice.OrderTotal", lang.Id), orderTotalStr), titleFont);
						pTotal.Alignment = Element.ALIGN_RIGHT;
						doc.Add(pTotal);

						#endregion

						footerTotalTable.AddCell(cell);

						//doc.Add(new Paragraph(_localizationService.GetResource("PDFInvoice.Product(s)", lang.Id), orangeTextFont));
						doc.Add(footerTotalTable);

						#region Order notes

						if(pdfSettingsByStore.RenderOrderNotes)
						{
							var orderNotes = order.OrderNotes
									.Where(on => on.DisplayToCustomer)
									.OrderByDescending(on => on.CreatedOnUtc)
									.ToList();
							if(orderNotes.Count > 0)
							{
								doc.Add(new Paragraph(_localizationService.GetResource("PDFInvoice.OrderNotes", lang.Id), titleFont));

								doc.Add(new Paragraph(" "));

								var notesTable = new PdfPTable(2);
								notesTable.WidthPercentage = 100f;
								notesTable.SetWidths(new[] { 30, 70 });

								//created on
								cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.OrderNotes.CreatedOn", lang.Id), font));
								cell.BackgroundColor = BaseColor.LIGHT_GRAY;
								cell.HorizontalAlignment = Element.ALIGN_CENTER;
								notesTable.AddCell(cell);

								//note
								cell = new PdfPCell(new Phrase(_localizationService.GetResource("PDFInvoice.OrderNotes.Note", lang.Id), font));
								cell.BackgroundColor = BaseColor.LIGHT_GRAY;
								cell.HorizontalAlignment = Element.ALIGN_CENTER;
								notesTable.AddCell(cell);

								foreach(var orderNote in orderNotes)
								{
									cell = new PdfPCell();
									cell.AddElement(new Paragraph(_dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc).ToString(), font));
									cell.HorizontalAlignment = Element.ALIGN_LEFT;
									notesTable.AddCell(cell);

									cell = new PdfPCell();
									cell.AddElement(new Paragraph(HtmlHelper.ConvertHtmlToPlainText(orderNote.FormatOrderNoteText(), true, true), font));
									cell.HorizontalAlignment = Element.ALIGN_LEFT;
									notesTable.AddCell(cell);

									//should we display a link to downloadable files here?
									//I think, no. Onyway, PDFs are printable documents and links (files) are useful here
								}
								doc.Add(notesTable);
							}
						}

						#endregion

						#region Footer

						if(!String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) || !String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2))
						{
							var column1Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) ?
									new List<string>() :
									pdfSettingsByStore.InvoiceFooterTextColumn1
									.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
									.ToList();
							var column2Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2) ?
									new List<string>() :
									pdfSettingsByStore.InvoiceFooterTextColumn2
									.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
									.ToList();
							if(column1Lines.Count > 0 || column2Lines.Count > 0)
							{
								var totalLines = Math.Max(column1Lines.Count, column2Lines.Count);
								const float margin = 43;

								//if you have really a lot of lines in the footer, then replace 9 with 10 or 11
								int footerHeight = totalLines * 9;
								var directContent = pdfWriter.DirectContent;
								directContent.MoveTo(pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight);
								directContent.LineTo(pageSize.GetRight(margin), pageSize.GetBottom(margin) + footerHeight);
								directContent.Stroke();


								var footerTable = new PdfPTable(2);
								footerTable.WidthPercentage = 100f;
								footerTable.SetTotalWidth(new float[] { 250, 250 });

								//column 1
								if(column1Lines.Count > 0)
								{
									var column1 = new PdfPCell();
									column1.Border = Rectangle.NO_BORDER;
									column1.HorizontalAlignment = Element.ALIGN_LEFT;
									foreach(var footerLine in column1Lines)
									{
										column1.AddElement(new Phrase(footerLine, font));
									}
									footerTable.AddCell(column1);
								}
								else
								{
									var column = new PdfPCell(new Phrase(" "));
									column.Border = Rectangle.NO_BORDER;
									footerTable.AddCell(column);
								}

								//column 2
								if(column2Lines.Count > 0)
								{
									var column2 = new PdfPCell();
									column2.Border = Rectangle.NO_BORDER;
									column2.HorizontalAlignment = Element.ALIGN_LEFT;
									foreach(var footerLine in column2Lines)
									{
										column2.AddElement(new Phrase(footerLine, font));
									}
									footerTable.AddCell(column2);
								}
								else
								{
									var column = new PdfPCell(new Phrase(" "));
									column.Border = Rectangle.NO_BORDER;
									footerTable.AddCell(column);
								}

								footerTable.WriteSelectedRows(0, totalLines, pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight, directContent);
							}
						}

						#endregion

						ordNum++;
						if(ordNum < ordCount)
						{
							doc.NewPage();
						}
					}
					doc.Close();
				}

                /// <summary>
                /// Print orders to PDF
                /// </summary>
                /// <param name="stream">Stream</param>
                /// <param name="orders">Orders</param>
                /// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
                public virtual void PrintSummaryReportToPdf(Stream stream, ReportSummaryModel model, int languageId = 0)
                {
                    var pageSize = PageSize.A4;

                    if (_pdfSettings.LetterPageSizeEnabled)
                    {
                        pageSize = PageSize.LETTER;
                    }


                    var doc = new Document(pageSize);
                    var pdfWriter = PdfWriter.GetInstance(doc, stream);
                    doc.Open();

                    //fonts
                    var orangeTextFont = GetFontBold();
                    orangeTextFont.Color = new BaseColor(129, 82, 45);

                    var whiteTextFont = GetFontMinion();

                    var titleFont = GetFontBold();


                    var font = GetFont();


                    var attributesFont = GetFont();
                    attributesFont.SetStyle(Font.ITALIC);

                    //int ordCount = model.CateringEvents.Count();
                    //int ordNum = 0;
                    
                    #region Header
                    var pdfSettingsByStore = _settingContext.LoadSetting<PdfSettings>(0);


                    var lang = _languageService.GetLanguageById(languageId);
                    if (lang == null || !lang.Published)
                        lang = _workContext.WorkingLanguage;

                    //logo
                    var logoPicture = _pictureService.GetPictureById(pdfSettingsByStore.LogoPictureId);
                    var paidPicture = _pictureService.GetPictureById(428);//1090
                    var logoExists = logoPicture != null;

                    //header
                    var headerTable = new PdfPTable(logoExists ? 3 : 1);
                    headerTable.WidthPercentage = 100f;
                    if (logoExists)
                        headerTable.SetWidths(new[] { 40, 30, 30 });

                    //logo
                    if (logoExists)
                    {
                        var logoFilePath = _pictureService.GetThumbLocalPath(logoPicture, 300, false);
                        var logoFile = Image.GetInstance(logoFilePath);
                        logoFile.ScalePercent(60);
                        var cellLogo = new PdfPCell(logoFile);
                        cellLogo.Border = Rectangle.NO_BORDER;




                        headerTable.AddCell(cellLogo);
                        //headerTable.AddCell(cellPaid);
                    }
                    //store info
                    var cell = new PdfPCell();
                    cell.Border = Rectangle.NO_BORDER;

                    //cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("cateringinvoice.order", lang.Id), cateringEvent.Id), titleFont));



                    headerTable.AddCell(cell);
                    doc.Add(headerTable);

                    #endregion

                    DottedLineSeparator separator = new DottedLineSeparator();
                    Chunk linebreak = new Chunk(separator);
                    separator.LineColor = BaseColor.LIGHT_GRAY;
                    doc.Add(linebreak);
                    foreach (var cateringEvent in model.GroupedOrderItems)
                    {
                        
                        if(cateringEvent.DarienEventOrder.Count() > 0)
                        { 
                            #region Products
                            //products
                            //header
                            var middelHeaderTable = new PdfPTable(2);
                            middelHeaderTable.WidthPercentage = 100f;
                            middelHeaderTable.SetWidths(new[] { 60, 40 });

                            cell = new PdfPCell();
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                            cell.AddElement(new Paragraph("SUMMARY - CATERING PRODUCTION REPORT", whiteTextFont));

                            middelHeaderTable.AddCell(cell);

                            cell = new PdfPCell();
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                            cell.AddElement(new Paragraph(cateringEvent.OrderDay.Value.ToString("MM/dd/yyyy"), whiteTextFont));

                            middelHeaderTable.AddCell(cell);

                            doc.Add(middelHeaderTable);
                            doc.Add(new Paragraph(" "));



                            var productsTable = new PdfPTable(4);
                            productsTable.WidthPercentage = 100f;
                            productsTable.SetWidths( new[] { 20, 10, 30, 40 });
                        
                        


                            //Production Store
                            cell = new PdfPCell(new Phrase("Store Name", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Quantity
                            cell = new PdfPCell(new Phrase("Quantity", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Size
                            cell = new PdfPCell(new Phrase("Size", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Item
                            cell = new PdfPCell(new Phrase("Item", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            foreach (var item in cateringEvent.DarienEventOrder)
                            {
                                //Production Store
                                cell = new PdfPCell(new Phrase(item.StoreName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Quantity
                                cell = new PdfPCell(new Phrase(item.Quantity.ToString(), titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Size
                                cell = new PdfPCell(new Phrase(item.Size, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Item
                                cell = new PdfPCell();
                                cell.AddElement(new Paragraph(item.ProductName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(item.Attribute, true, true), attributesFont);
                                cell.AddElement(attributesParagraph);
                                productsTable.AddCell(cell);

                            
                            }


                       
                            doc.Add(productsTable);

                            #endregion
                        }

                        if (cateringEvent.MokenaEventOrder.Count() > 0)
                        {
                            #region Products
                            //products
                            //header
                            var middelHeaderTable = new PdfPTable(2);
                            middelHeaderTable.WidthPercentage = 100f;
                            middelHeaderTable.SetWidths(new[] { 60, 40 });

                            cell = new PdfPCell();
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                            cell.AddElement(new Paragraph("SUMMARY - CATERING PRODUCTION REPORT", whiteTextFont));

                            middelHeaderTable.AddCell(cell);

                            
                            cell = new PdfPCell();
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                            cell.AddElement(new Paragraph(cateringEvent.OrderDay.Value.ToString("MM/dd/yyyy"), whiteTextFont));

                            middelHeaderTable.AddCell(cell);

                            doc.Add(middelHeaderTable);
                            doc.Add(new Paragraph(" "));



                            var productsTable = new PdfPTable(4);
                            productsTable.WidthPercentage = 100f;
                            productsTable.SetWidths(new[] { 20, 10, 30, 40 });




                            //Production Store
                            cell = new PdfPCell(new Phrase("Store Name", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Quantity
                            cell = new PdfPCell(new Phrase("Quantity", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Size
                            cell = new PdfPCell(new Phrase("Size", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Item
                            cell = new PdfPCell(new Phrase("Item", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            foreach (var item in cateringEvent.MokenaEventOrder)
                            {
                                //Production Store
                                cell = new PdfPCell(new Phrase(item.StoreName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Quantity
                                cell = new PdfPCell(new Phrase(item.Quantity.ToString(), titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Size
                                cell = new PdfPCell(new Phrase(item.Size, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Item
                                cell = new PdfPCell();
                                cell.AddElement(new Paragraph(item.ProductName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(item.Attribute, true, true), attributesFont);
                                cell.AddElement(attributesParagraph);
                                productsTable.AddCell(cell);


                            }



                            doc.Add(productsTable);

                            #endregion
                        }

                        if (cateringEvent.BurridgeEventOrder.Count() > 0)
                        {
                            #region Products
                            //products
                            //header
                            var middelHeaderTable = new PdfPTable(2);
                            middelHeaderTable.WidthPercentage = 100f;
                            middelHeaderTable.SetWidths(new[] { 60, 40 });

                            cell = new PdfPCell();
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                            cell.AddElement(new Paragraph("SUMMARY - CATERING PRODUCTION REPORT", whiteTextFont));

                            middelHeaderTable.AddCell(cell);

                            cell = new PdfPCell();
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                            cell.AddElement(new Paragraph(cateringEvent.OrderDay.Value.ToString("MM/dd/yyyy"), whiteTextFont));

                            middelHeaderTable.AddCell(cell);

                            doc.Add(middelHeaderTable);
                            doc.Add(new Paragraph(" "));



                            var productsTable = new PdfPTable(4);
                            productsTable.WidthPercentage = 100f;
                            productsTable.SetWidths(new[] { 20, 10, 30, 40 });




                            //Production Store
                            cell = new PdfPCell(new Phrase("Store Name", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Quantity
                            cell = new PdfPCell(new Phrase("Quantity", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Size
                            cell = new PdfPCell(new Phrase("Size", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Item
                            cell = new PdfPCell(new Phrase("Item", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            foreach (var item in cateringEvent.BurridgeEventOrder)
                            {
                                //Production Store
                                cell = new PdfPCell(new Phrase(item.StoreName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Quantity
                                cell = new PdfPCell(new Phrase(item.Quantity.ToString(), titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Size
                                cell = new PdfPCell(new Phrase(item.Size, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Item
                                cell = new PdfPCell();
                                cell.AddElement(new Paragraph(item.ProductName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(item.Attribute, true, true), attributesFont);
                                cell.AddElement(attributesParagraph);
                                productsTable.AddCell(cell);


                            }



                            doc.Add(productsTable);

                            #endregion
                        }
                        #region Footer

                        if (!String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) || !String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2))
                        {
                            var column1Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) ?
                                    new List<string>() :
                                    pdfSettingsByStore.InvoiceFooterTextColumn1
                                    .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                                    .ToList();
                            var column2Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2) ?
                                    new List<string>() :
                                    pdfSettingsByStore.InvoiceFooterTextColumn2
                                    .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                                    .ToList();
                            if (column1Lines.Count > 0 || column2Lines.Count > 0)
                            {
                                var totalLines = Math.Max(column1Lines.Count, column2Lines.Count);
                                const float margin = 43;

                                //if you have really a lot of lines in the footer, then replace 9 with 10 or 11
                                int footerHeight = totalLines * 9;
                                var directContent = pdfWriter.DirectContent;
                                directContent.MoveTo(pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight);
                                directContent.LineTo(pageSize.GetRight(margin), pageSize.GetBottom(margin) + footerHeight);
                                directContent.Stroke();


                                var footerTable = new PdfPTable(2);
                                footerTable.WidthPercentage = 100f;
                                footerTable.SetTotalWidth(new float[] { 250, 250 });

                                //column 1
                                if (column1Lines.Count > 0)
                                {
                                    var column1 = new PdfPCell();
                                    column1.Border = Rectangle.NO_BORDER;
                                    column1.HorizontalAlignment = Element.ALIGN_LEFT;
                                    foreach (var footerLine in column1Lines)
                                    {
                                        column1.AddElement(new Phrase(footerLine, font));
                                    }
                                    footerTable.AddCell(column1);
                                }
                                else
                                {
                                    var column = new PdfPCell(new Phrase(" "));
                                    column.Border = Rectangle.NO_BORDER;
                                    footerTable.AddCell(column);
                                }

                                //column 2
                                if (column2Lines.Count > 0)
                                {
                                    var column2 = new PdfPCell();
                                    column2.Border = Rectangle.NO_BORDER;
                                    column2.HorizontalAlignment = Element.ALIGN_LEFT;
                                    foreach (var footerLine in column2Lines)
                                    {
                                        column2.AddElement(new Phrase(footerLine, font));
                                    }
                                    footerTable.AddCell(column2);
                                }
                                else
                                {
                                    var column = new PdfPCell(new Phrase(" "));
                                    column.Border = Rectangle.NO_BORDER;
                                    footerTable.AddCell(column);
                                }

                                footerTable.WriteSelectedRows(0, totalLines, pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight, directContent);
                            }
                        }

                        #endregion

                        
                    }
                    doc.Close();
                }

                /// <summary>
                /// Print orders to PDF
                /// </summary>
                /// <param name="stream">Stream</param>
                /// <param name="orders">Orders</param>
                /// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
                public virtual void PrintReportToPdf(Stream stream, ReportSummaryModel model, int languageId = 0)
                {
                    var pageSize = PageSize.A4;

                    if (_pdfSettings.LetterPageSizeEnabled)
                    {
                        pageSize = PageSize.LETTER;
                    }


                    var doc = new Document(pageSize);
                    var pdfWriter = PdfWriter.GetInstance(doc, stream);
                    doc.Open();

                    //fonts
                    var orangeTextFont = GetFontBold();
                    orangeTextFont.Color = new BaseColor(129, 82, 45);

                    var whiteTextFont = GetFontMinion();

                    var titleFont = GetFontBold();


                    var font = GetFont();


                    var attributesFont = GetFont();
                    attributesFont.SetStyle(Font.ITALIC);

                    int ordCount = model.CateringEvents.Count();
                    int ordNum = 0;
                    var pdfSettingsByStore = _settingContext.LoadSetting<PdfSettings>(0);


                    var lang = _languageService.GetLanguageById(languageId);
                    if (lang == null || !lang.Published)
                        lang = _workContext.WorkingLanguage;

                    #region Header

                    //logo
                    var logoPicture = _pictureService.GetPictureById(pdfSettingsByStore.LogoPictureId);
                    var paidPicture = _pictureService.GetPictureById(428);//1090
                    var logoExists = logoPicture != null;

                    //header
                    var headerTable = new PdfPTable(logoExists ? 3 : 1);
                    headerTable.WidthPercentage = 100f;
                    if (logoExists)
                        headerTable.SetWidths(new[] { 40, 30, 30 });

                    //logo
                    if (logoExists)
                    {
                        var logoFilePath = _pictureService.GetThumbLocalPath(logoPicture, 300, false);
                        var logoFile = Image.GetInstance(logoFilePath);
                        logoFile.ScalePercent(60);
                        var cellLogo = new PdfPCell(logoFile);
                        cellLogo.Border = Rectangle.NO_BORDER;




                        headerTable.AddCell(cellLogo);
                        //headerTable.AddCell(cellPaid);
                    }
                    //store info
                    var cell = new PdfPCell();
                    cell.Border = Rectangle.NO_BORDER;

                    //cell.AddElement(new Paragraph(String.Format(_localizationService.GetResource("cateringinvoice.order", lang.Id), cateringEvent.Id), titleFont));



                    headerTable.AddCell(cell);
                    doc.Add(headerTable);

                    #endregion

                    DottedLineSeparator separator = new DottedLineSeparator();
                    Chunk linebreak = new Chunk(separator);
                    separator.LineColor = BaseColor.LIGHT_GRAY;
                    doc.Add(linebreak);

                    foreach (var reportItem in model.GroupedReportItems)
                    {

                        #region Grey Background Header
                        var middelHeaderTable = new PdfPTable(2);
                        middelHeaderTable.WidthPercentage = 100f;
                        middelHeaderTable.SetWidths(new[] { 60, 40 });

                        cell = new PdfPCell();
                        cell.Border = Rectangle.NO_BORDER;
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        cell.AddElement(new Paragraph("Orders - Catering Production Order Report", whiteTextFont));

                        middelHeaderTable.AddCell(cell);

                        var orderCreatedTime = reportItem.OrderDay != null ? reportItem.OrderDay.Value.ToString("MM/dd/yyyy - dddd") : "";
                        cell = new PdfPCell();
                        cell.Border = Rectangle.NO_BORDER;
                        cell.BackgroundColor = BaseColor.LIGHT_GRAY;
                        cell.AddElement(new Paragraph(orderCreatedTime, whiteTextFont));

                        middelHeaderTable.AddCell(cell);

                        doc.Add(middelHeaderTable);
                        doc.Add(new Paragraph(" "));
                        #endregion

                        bool isFirstRow = true;

                        foreach (var cateringEvent in reportItem.CateringEvents)
                        {



                            #region Products
                            //products
                            //header

                            var productsTable = new PdfPTable(4);
                            productsTable.WidthPercentage = 100f;
                            productsTable.SetWidths(new[] { 20, 10, 30, 40 });

                            //Production Store
                            cell = new PdfPCell(new Phrase(cateringEvent.SupplierStoreName, titleFont));
                            cell.Border = (!isFirstRow) ? Rectangle.TOP_BORDER : Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Quantity
                            cell = new PdfPCell(new Phrase("Order", titleFont));
                            cell.Border = (!isFirstRow) ? Rectangle.TOP_BORDER : Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);
                            //}

                            //Size
                            cell = new PdfPCell(new Phrase(String.Format("#{0}", cateringEvent.OrderId.ToString(), titleFont)));
                            cell.Border = (!isFirstRow) ? Rectangle.TOP_BORDER : Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Item
                            var pickUp = cateringEvent.CafePickup.Equals(false) ? "Delivery" : "PickUp";
                            cell = new PdfPCell(new Phrase(String.Format(" {0} - {1}", cateringEvent.OrderedBy.ToString(), cateringEvent.DeliveryTime.ToString("MM/dd/yyyy"), titleFont)));
                            cell.Border = (!isFirstRow) ? Rectangle.TOP_BORDER : Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);


                            //Production Store
                            cell = new PdfPCell(new Phrase("Production Store", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Quantity
                            cell = new PdfPCell(new Phrase("Quantity", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Size
                            cell = new PdfPCell(new Phrase("Size", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            //Item
                            cell = new PdfPCell(new Phrase("Item", titleFont));
                            cell.Border = Rectangle.NO_BORDER;
                            cell.BorderColor = BaseColor.GRAY;
                            cell.HorizontalAlignment = Element.ALIGN_LEFT;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            productsTable.AddCell(cell);

                            

                            foreach (var item in cateringEvent.EventOrderItems)
                            {
                                //Production Store
                                cell = new PdfPCell(new Phrase(item.StoreName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Quantity
                                cell = new PdfPCell(new Phrase(item.Quantity.ToString(), titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Size
                                cell = new PdfPCell(new Phrase(item.Size, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Item
                                cell = new PdfPCell();
                                cell.AddElement(new Paragraph(item.ProductName, titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                var attributesParagraph = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(item.Attribute, true, true), attributesFont);
                                cell.AddElement(attributesParagraph);
                                productsTable.AddCell(cell);


                            }
                            if (cateringEvent.OrderNotes.Count() > 0)
                            {
                                //Production Store
                                cell = new PdfPCell(new Phrase("Order Notes", titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Quantity
                                cell = new PdfPCell(new Phrase("", titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Size
                                cell = new PdfPCell(new Phrase("", titleFont));
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                productsTable.AddCell(cell);

                                //Item
                                cell.Border = Rectangle.NO_BORDER;
                                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                var orderNotes = new Paragraph(HtmlHelper.ConvertHtmlToPlainText(String.Join("<br>", cateringEvent.OrderNotes.Select(on => "*" + on.Note).ToList()), true, true), attributesFont);
                                cell.AddElement(orderNotes);
                                productsTable.AddCell(cell);
                            }
                           


                            doc.Add(productsTable);

                            #endregion

                            isFirstRow = false;

                            ordNum++;
                            if (ordNum < ordCount)
                            {
                                // doc.NewPage();
                            }

                        }

                        
                    }
                    #region Footer

                    if (!String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) || !String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2))
                    {
                        var column1Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn1) ?
                                new List<string>() :
                                pdfSettingsByStore.InvoiceFooterTextColumn1
                                .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                                .ToList();
                        var column2Lines = String.IsNullOrEmpty(pdfSettingsByStore.InvoiceFooterTextColumn2) ?
                                new List<string>() :
                                pdfSettingsByStore.InvoiceFooterTextColumn2
                                .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                                .ToList();
                        if (column1Lines.Count > 0 || column2Lines.Count > 0)
                        {
                            var totalLines = Math.Max(column1Lines.Count, column2Lines.Count);
                            const float margin = 43;

                            //if you have really a lot of lines in the footer, then replace 9 with 10 or 11
                            int footerHeight = totalLines * 9;
                            var directContent = pdfWriter.DirectContent;
                            directContent.MoveTo(pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight);
                            directContent.LineTo(pageSize.GetRight(margin), pageSize.GetBottom(margin) + footerHeight);
                            directContent.Stroke();


                            var footerTable = new PdfPTable(2);
                            footerTable.WidthPercentage = 100f;
                            footerTable.SetTotalWidth(new float[] { 250, 250 });

                            //column 1
                            if (column1Lines.Count > 0)
                            {
                                var column1 = new PdfPCell();
                                column1.Border = Rectangle.NO_BORDER;
                                column1.HorizontalAlignment = Element.ALIGN_LEFT;
                                foreach (var footerLine in column1Lines)
                                {
                                    column1.AddElement(new Phrase(footerLine, font));
                                }
                                footerTable.AddCell(column1);
                            }
                            else
                            {
                                var column = new PdfPCell(new Phrase(" "));
                                column.Border = Rectangle.NO_BORDER;
                                footerTable.AddCell(column);
                            }

                            //column 2
                            if (column2Lines.Count > 0)
                            {
                                var column2 = new PdfPCell();
                                column2.Border = Rectangle.NO_BORDER;
                                column2.HorizontalAlignment = Element.ALIGN_LEFT;
                                foreach (var footerLine in column2Lines)
                                {
                                    column2.AddElement(new Phrase(footerLine, font));
                                }
                                footerTable.AddCell(column2);
                            }
                            else
                            {
                                var column = new PdfPCell(new Phrase(" "));
                                column.Border = Rectangle.NO_BORDER;
                                footerTable.AddCell(column);
                            }

                            footerTable.WriteSelectedRows(0, totalLines, pageSize.GetLeft(margin), pageSize.GetBottom(margin) + footerHeight, directContent);
                        }
                    }

                    #endregion
                    pdfWriter.CompressionLevel = PdfStream.BEST_COMPRESSION;
                    pdfWriter.SetFullCompression();
                    doc.Close();
                    
                }

                public virtual void PrintHtmlToPdf(Stream stream, String htmlText)
                {
//                    String strSelectUserListBuilder = @"<html><body>
//                                <h1>My First Heading</h1>
//                                <p>My first paragraph.</p>
//                            </body>
//                        </html>";

//                    String htmlText = strSelectUserListBuilder.ToString();
                    Document document = new Document();
                    PdfWriter.GetInstance(document, stream);
                    document.Open();
                    iTextSharp.text.html.simpleparser.HTMLWorker hw =
                                             new iTextSharp.text.html.simpleparser.HTMLWorker(document);
                    hw.Parse(new StringReader(htmlText));
                    document.Close();
                }
				#endregion

		#region Utilities

		protected virtual Font GetFont()
		{
			//nopCommerce supports unicode characters
			//nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
			//It was downloaded from http://savannah.gnu.org/projects/freefont
			string fontPath = Path.Combine(_webHelper.MapPath("~/App_Data/Pdf/"), _pdfSettings.FontFileName);
			var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			var font = new Font(baseFont, 12, Font.NORMAL);
			return font;
		}

		protected virtual Font GetFontBold()
		{
			//nopCommerce supports unicode characters
			//nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
			//It was downloaded from http://savannah.gnu.org/projects/freefont
			string fontPath = Path.Combine(_webHelper.MapPath("~/App_Data/Pdf/"), "AGaramondPro-Bold.otf");
			var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			var font = new Font(baseFont, 12, Font.NORMAL);
			font.Color = BaseColor.BLACK;
			return font;
		}

		protected virtual Font GetFontMinion()
		{
			//nopCommerce supports unicode characters
			//nopCommerce uses Free Serif font by default (~/App_Data/Pdf/FreeSerif.ttf file)
			//It was downloaded from http://savannah.gnu.org/projects/freefont
			string fontPath = Path.Combine(_webHelper.MapPath("~/App_Data/Pdf/"), "MinionPro-Regular.otf");
			var baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			var font = new Font(baseFont, 12, Font.BOLD);
			font.Color = BaseColor.WHITE;
			return font;
		}

		#endregion
    }
}
