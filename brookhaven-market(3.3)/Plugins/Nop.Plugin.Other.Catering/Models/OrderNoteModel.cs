using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Core;
using Nop.Plugin.Other.Catering.Validators;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework;
using Nop.Core.Domain.Orders;

namespace Nop.Plugin.Other.Catering.Models
{
    public class OrderNoteModel : BaseNopEntityModel
    {
        public OrderNoteModel()
        {
            OrderNotes = new List<OrderNote>();
            EventOrderNotes = new List<string>();
        }
        public int Id { get; set; }
        [NopResourceDisplayName("Admin.Orders.OrderNotes.Fields.Note")]
        [AllowHtml]
        public string AddOrderNoteMessage { get; set; }

        public IList<OrderNote> OrderNotes { get; set; }

        public virtual int EventId { get; set; }
        public virtual List<string> EventOrderNotes { get; set; }
        
    }
}
