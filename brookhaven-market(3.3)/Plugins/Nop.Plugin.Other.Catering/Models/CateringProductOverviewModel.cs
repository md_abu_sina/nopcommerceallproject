﻿using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Other.Catering.Models
{
		//[Validator(typeof(EventItemValidator))]
		public class CateringProductOverviewModel : ProductOverviewModel
    {
			public bool IsTimeRestricted { get; set; }
    }

}
