﻿using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Other.Catering.Models
{
		//[Validator(typeof(EventItemValidator))]
		public class CateringSearchModel 
    {
			public CateringSearchModel()
			{
				PagingFilteringContext = new SearchPagingFilteringModel();
				Products = new List<CateringProductOverviewModel>();
			}
			public SearchPagingFilteringModel PagingFilteringContext { get; set; }
      public IList<CateringProductOverviewModel> Products { get; set; }
			public string CategoryName { get; set; }
			public string CategoryTag { get; set; }
    }

}
