﻿using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Other.Catering.Models
{
    //[Validator(typeof(EventItemValidator))]
		public class CategoryViewModel : BaseNopModel
    {
			public int Id { get; set; }
			public string Name { get; set; }
			public string SEName { get; set; }
			public IList<Category> SubCategories { get; set; }
			public string PictureLink { get; set; }      			
    }

}
