﻿using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Catalog;
using Nop.Admin.Models.Orders;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Other.Catering.Models
{
    //[Validator(typeof(EventItemValidator))]
    public class CateringOrderListModel : OrderListModel
    {
            

            [NopResourceDisplayName("Admin.Plugin.Other.Catering.DeliveryStartDate")]
            [UIHint("DateNullable")]
            public DateTime? DeliveryStartDate { get; set; }

            [NopResourceDisplayName("Admin.Plugin.Other.Catering.DeliveryEndDate")]
            [UIHint("DateNullable")]
            public DateTime? DeliveryEndDate { get; set; }
    	
		
    }

}
