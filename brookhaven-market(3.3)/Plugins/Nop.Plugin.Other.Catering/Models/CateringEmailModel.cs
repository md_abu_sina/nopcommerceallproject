using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Core;
using Nop.Plugin.Other.Catering.Validators;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Other.Catering.Models
{
		[Validator(typeof(CateringEmailValidator))]
		public class CateringEmailModel : BaseNopEntityModel
    {
				//public virtual int StoreId { get; set; }
				//public virtual IEnumerable<SelectListItem> StoreId { get; set; }
				public virtual IEnumerable<SelectListItem> StoreList { get; set; }
				public virtual int StoreId { get; set; }
				public virtual string EmailAddress { get; set; }
				public virtual SelectList EmailAddressTypes { get; set; }
				[DisplayName("Addressed To")]
				public virtual int EmailAddressType { get; set; }
				public virtual string EmailType { get; set; }
				public virtual string StoreName { get; set; }
    }
}
