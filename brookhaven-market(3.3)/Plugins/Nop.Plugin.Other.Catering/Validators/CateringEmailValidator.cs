﻿using FluentValidation;
using Nop.Plugin.Other.Catering.Models;
using Nop.Services.Localization;


namespace Nop.Plugin.Other.Catering.Validators
{
    public class CateringEmailValidator : AbstractValidator<CateringEmailModel>
    {
				public CateringEmailValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.EmailAddress)
                .NotNull()
								.EmailAddress()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Catering.Fields.Email.Required"));
        }
    }
}