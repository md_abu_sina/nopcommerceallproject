using System;
using Nop.Core;

namespace Nop.Plugin.Other.Catering.Domain
{
		public class CateringEmail : BaseEntity
    {
				public virtual int StoreId { get; set; }
				public virtual string EmailAddress { get; set; }
				public virtual int EmailAddressType { get; set; }
				public virtual string StoreName { get; set; }
    }
}
