using System;
using Nop.Core;

namespace Nop.Plugin.Other.Catering.Domain
{
		public class CateringEvent : BaseEntity
    {
				public virtual int NumberOfPeople { get; set; }
				public virtual bool CafePickup { get; set; }
				public virtual DateTime DeliveryTime { get; set; }
				public virtual DateTime? OrderCreatedTime { get; set; }
				public virtual DateTime? OrderUpdatedTime { get; set; }
				public virtual int SupplierStoreId { get; set; }
				public virtual double SupplierStoreDistance { get; set; }
				public virtual int AddressId { get; set; }
				public virtual int CurrentCustomerId { get; set; }
				public virtual int OrderId { get; set; }
				public virtual int CateringOrderStatus { get; set; }
				public virtual string SecondaryStores { get; set; }
				public virtual string PdfInvoicePath { get; set; }
    }
}
