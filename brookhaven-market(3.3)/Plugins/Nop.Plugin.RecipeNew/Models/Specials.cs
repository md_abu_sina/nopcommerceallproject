﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.RecipeNew.Models
{

    public class SpecialsModel
    {
        public List<Department> Departments { set; get; }
        public List<Special> Specials { set; get; }
        public SpecialPagination Pagination { set; get; }
    }

    public class Department
    {
        public string name { set; get; }
        public string id { set; get; }
        public string image { set; get; }
    }

    public class Department_With_Special
    {
        public Department department { set; get; }
        public List<Special> Specials { set; get; }
    }

    public class Special
    {
        public string featured { set; get; }
        public string id { set; get; }
        public string brand { set; get; }
        public string product { set; get; }
        public string description { set; get; }
        public string offer { set; get; }
        public string image { set; get; }
        public string recipe_title { set; get; }
        public string recipe_id { set; get; }
        public string isAlreadyOnDatabase { set; get; }
    }

    public class SpecialPagination
    {
        public string current_page { set; get; }
        public string next_page { set; get; }
        public string total_pages { set; get; }
        public string per_page { set; get; }
        public string start_record { set; get; }
        public string end_record { set; get; }
        public string total_records { set; get; }
    }

}
