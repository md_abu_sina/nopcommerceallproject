﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.RecipeNew.Models
{
    public class TipsModel
    {
        public List<Tip> Tips { set; get; }
        public Pagination Pagination { set; get; }
    }

    public class Tip
    {
        public string id { set; get; }
        public string title { set; get; }
    }
    public class Pagination
    {
        public string current_page { set; get; }
        public string next_page { set; get; }
        public string total_pages { set; get; }
        public string per_page { set; get; }
        public string start_record { set; get; }
        public string end_record { set; get; }
        public string total_records { set; get; }
    }
    public class TipDetail
    {
        public string id { set; get; }
        public string title { set; get; }
        public string body { set; get; }
    }
}
