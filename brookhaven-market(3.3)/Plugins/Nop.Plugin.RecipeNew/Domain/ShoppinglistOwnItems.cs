﻿using Nop.Core;

namespace Nop.Plugin.RecipeNew.Domain
{
    public class ShoppinglistOwnItems : BaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int CustomerID { get; set; }
        public virtual string ItemTitle { get; set; }
    }
}
