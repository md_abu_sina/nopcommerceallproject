﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.RecipeNew.Data;
using Nop.Plugin.RecipeNew.Domain;
using Nop.Plugin.RecipeNew.Services;

namespace Nop.Plugin.RecipeNew
{
    public class ShoppinglistOwnItemsDependencyRegister: IDependencyRegistrar 
    {
        private const string CONTEXT_NAME = "nop_object_context_ShoppinglistOwnItemsnew";

        #region Implementation of IDependencyRegistrar

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //Load custom data settings
            var dataSettingsManager = new DataSettingsManager();
            DataSettings dataSettings = dataSettingsManager.LoadSettings();

            //Register custom object context
            builder.Register<IDbContext>(c => RegisterIDbContext(c, dataSettings)).Named<IDbContext>(CONTEXT_NAME).InstancePerHttpRequest();
            builder.Register(c => RegisterIDbContext(c, dataSettings)).InstancePerHttpRequest();

            //Register services
            builder.RegisterType<ShoppinglistOwnItemsService>().As<IShoppinglistOwnItemsService>();

            //Override the repository injection
            builder.RegisterType<EfRepository<ShoppinglistOwnItems>>().As<IRepository<ShoppinglistOwnItems>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME)).InstancePerHttpRequest();
        }

        #endregion

        #region Implementation of IDependencyRegistrar

        public int Order
        {
            get { return 1; }
        }

        #endregion

        /// <summary>
        /// Registers the I db context.
        /// </summary>
        /// <param name="componentContext">The component context.</param>
        /// <param name="dataSettings">The data settings.</param>
        /// <returns></returns>
        private ShoppinglistOwnitemsObjectContext RegisterIDbContext(IComponentContext componentContext, DataSettings dataSettings)
        {
            string dataConnectionStrings;

            if (dataSettings != null && dataSettings.IsValid())
            {
                dataConnectionStrings = dataSettings.DataConnectionString;
            }
            else
            {
                dataConnectionStrings = componentContext.Resolve<DataSettings>().DataConnectionString;
            }

            return new ShoppinglistOwnitemsObjectContext(dataConnectionStrings);
        }
    }
}
