$(document).ready(function () {
    var page = 1;
    var totalpage = $("#totalpage").val();
    var showing = false;
    $(function () {
        $(window).endlessScroll({
            bottomPixels: 500,
            fireDelay: 250,
            callback: function (i) {
                if (showing == false) {
                    showing = true;
                    var last_li = $("ul#UlList_mobile li:last");

                    page++;
                    if (page <= totalpage) {
                        if ($("#pagename").val() == 'recipelist') {

                            var tags = $("#tag").val();
                            var title = $("#title").val();
                            var ingredient = $("#ingredient").val();

                            var qstr = "page=" + page;
                            if (tags != '') {
                                qstr += "&tags=" + tags;
                            }
                            else if (title != '') {
                                qstr += "&title=" + title;
                            }
                            else if (ingredient != '') {
                                qstr += "&ingredient=" + ingredient;
                            }
                            qstr += "&mobileScroll='true'";
                            console.log(qstr);
                            $('div#nextPageLoader').removeClass("displayNone");
                            $.ajax({
                                cache: false,
                                type: "POST",
                                url: "/Recipenew/showRecipeList/",
                                data: qstr,
                                success: function (data) {
                                    $("ul#UlList_mobile").append(data);
                                    showing = false;
                                    $('div#nextPageLoader').addClass("displayNone");

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert('Failed to load page' + i)
                                }
                            });
                        }

                        if ($("#pagename").val() == 'specials') {
                            $('div#nextPageLoader').removeClass("displayNone");
                            var qstr = "page=" + page;
                            var departmentId = $("#departmentId").val();
                            var search = $("#search").val();
                            var unregistered = $("#unregistered").val();
                            var dontshowaddshoppinglist = $("#dontshowaddshoppinglist").val();

                            if (departmentId != '') {
                                qstr += "&department_id=" + departmentId;
                            }
                            else if (search != '') {
                                qstr += "&search=" + search;
                            }

                            qstr += "&mobileScroll='true'&unregistered=" + unregistered + "&dontshowaddshoppinglist=" + dontshowaddshoppinglist;
                            $.ajax({
                                cache: false,
                                type: "POST",
                                url: "/specials/specials_mobile/",
                                data: qstr,
                                success: function (data) {
                                    $("ul#UlList_mobile").append(data);
                                    showing = false;
                                    $('div#nextPageLoader').addClass("displayNone");

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert('Failed to load page' + i)
                                }
                            });
                        }
                        if ($("#pagename").val() == 'TipsList') {

                            var search = $("#search").val();

                            var qstr = "page=" + page;
                            if (search != '') {
                                qstr += "&search=" + search;
                            }

                            qstr += "&mobileScroll='true'";
                            console.log(qstr);
                            $('div#nextPageLoader').removeClass("displayNone");
                            $.ajax({
                                cache: false,
                                type: "POST",
                                url: "/nextpage/TipsdisplayableItems_mobilenew",
                                data: qstr,
                                success: function (data) {
                                    $("ul#UlList_mobile").append(data);
                                    showing = false;
                                    $('div#nextPageLoader').addClass("displayNone");

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert('Failed to load page' + i)
                                }
                            });
                        }
                    }
                }

            }
        });
    });
});