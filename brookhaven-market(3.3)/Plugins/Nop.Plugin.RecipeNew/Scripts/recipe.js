jQuery.curCSS = jQuery.css;
$(window).bind('load', function () {
    // shoppinglist_add_anon       

    $('#shoppinglist_add_anon').bubbletip($('#tip1_left'), {
        deltaDirection: 'left',
        animationDuration: 100,
        offsetLeft: -20
    });

    $('#AddIconCookBook').bubbletip($('#tip1_left1'), {
        deltaDirection: 'left',
        animationDuration: 100,
        offsetLeft: -20
    });

    $('#a_unbind').bubbletip($('#tip1_trigger2_unbind'));
    $('#a_unbind').bind('click', function (event) {
        $('#a1_trigger').removeBubbletip($('#tip1_trigger2'));
        event.preventDefault();
    });
    $('#a_bind').bubbletip($('#tip1_trigger2_bind'));
    $('#a_bind').bind('click', function (event) {
        $('#a1_trigger').bubbletip($('#tip1_trigger2'), {
            positionAtElement: $('#a1_target'),
            deltaDirection: 'down',
            delayShow: 500,
            delayHide: 1000
        });
        event.preventDefault();
    });

    $('#inpText').bubbletip($('#tip1_focusblur'), {
        positionAtElement: $('#inpTarget'),
        deltaDirection: 'right',
        bindShow: 'xShow', // set a custom show event
        bindHide: 'xHide' // set a custom hide event
    }).bind('focus', function () {
        $(this).data('focus', true).trigger('xShow');  // store the focus state and trigger the show event
    }).bind('blur', function () {
        $(this).data('focus', false).trigger('xHide'); // store the focus state and trigger the hide event
    });
    $('#inpTarget').bind('mouseover', function () {
        if (!$('#inpText').data('focus')) { // check to see if the input has focus
            $('#inpText').trigger('xShow'); // it does not, so trigger the show event
        }
    }).bind('mouseout', function () {
        if (!$('#inpText').data('focus')) { // check to see if the input has focus
            $('#inpText').trigger('xHide'); // it does not, so trigger the hide event
        }
    });


    $('#a2_trigger').bubbletip($('#tip2_multi'), { deltaDirection: 'left', calculateOnShow: true, positionAtElement: $('#a2_target') });
    $('.a2').bind('mouseover', function () { $('#a2_trigger').trigger('mouseover'); });
    $('.a2').bind('mouseout', function () { $('#a2_trigger').trigger('mouseout'); });

});
var autocompleteFlag = 0;
function setrecipeDraggableforCalender() {
    $('#external-events div.external-event').each(function () {


        var eventObject = {
            title: $.trim($(this).text()),
            id: $.trim($(this).attr('id')),
            url: "/recipes/" + $(this).attr('id')
        };

        $(this).data('eventObject', eventObject);

        $(this).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0
        });

    });
    $('#radio_title').attr('checked', 'checked')
}

$(document).ready(function () {
    setrecipeDraggableforCalender();



    $("#quicksearch").autocomplete({

        source: function (request, response) {
            var searchText = $('#quicksearch').val();
            
            if (searchText.toString().length > 2) {
                $(".loadrecipesearch").css("display", "block");
                var seachType;
                if ($('#radio_title').attr('checked')) {
                    seachType = "title";
                }
                else {
                    seachType = "ingredient";
                }

                $.ajax({
                    cache: false,
                    type: "POST",
                    url: "/getautocompleteSearchresults",
                    data: seachType + '=' + searchText + '&autocomplete=true',
                    success: function (data) {
                        // console.debug(data);
                        setTimeout(function () {
                            response($.map(data, function (item) {
                                $(".loadrecipesearch").css("display", "none");

                                return { label: item.title, id: item.id };
                            }))
                        }, 1000);
                        $(".loadrecipesearch").css("display", "none");
                    }
                })
            }
        },
        select: function (event, ui) {

            var url = '/recipes/' + ui.item.id;
            $(location).attr('href', encodeURI(url));
        },
        focus: function (event, ui) {
            autocompleteFlag = 1;
        }


    });

    $("#quicksearchMobile").autocomplete({

        source: function (request, response) {
            var searchText = $('#quicksearchMobile').val();

            if (searchText.toString().length > 2) {
                var seachType;
                if ($('#radio-mini-1').attr('checked')) {
                    seachType = "title";
                }
                else {
                    seachType = "ingredient";
                }

                $.ajax({
                    cache: false,
                    type: "POST",
                    url: "/getautocompleteSearchresults",
                    data: seachType + '=' + searchText + '&autocomplete=true',
                    success: function (data) {
                        // console.debug(data);
                        response($.map(data, function (item) {
                            return { label: item.title, id: item.id };
                        }))
                    }
                })
            }
        },
        select: function (event, ui) {

            var url = '/recipes/' + ui.item.id;
            $(location).attr('href', encodeURI(url));
        },
        focus: function (event, ui) {
            autocompleteFlag = 1;
        }


    });

    $('#txtSearch').autocomplete({

        source: function (request, response) {
            var searchText = $('#txtSearch').val();
            if (searchText.toString().length > 2) {
                $.ajax({
                    cache: false,
                    type: "POST",
                    url: "/getautocompleteTipsSearchresults",
                    data: 'search=' + searchText,
                    success: function (data) {
                        // console.debug(data);
                        response($.map(data, function (item) {
                            return { label: item.title, id: item.id };
                        }))
                    }
                })
            }
        },
        select: function (event, ui) {

            var url = '/tipsandguides/' + ui.item.id;
            $(location).attr('href', encodeURI(url));
        },
        focus: function (event, ui) {
            autocompleteFlag = 1;
        }


    });


    $("#divUserItems div").hover(function () {
        $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "inherit");
    },
            function () {
                $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "hidden");
            });

    $("#shoppinglist_head tr").hover(function () {
        $(this).find(".deleteIconCookBook").css("visibility", "inherit");
    },
            function () {
                $(this).find(".deleteIconCookBook").css("visibility", "hidden");
            });

    $("#cookbook_recipes li").hover(function () {
        $(this).find(".deleteIconCookBook1").css("visibility", "inherit");
    },
            function () {
                $(this).find(".deleteIconCookBook1").css("visibility", "hidden");
            });

    //    $('#ul_specials_normal_view li').hover(function () {
    //        $(this).find(".deleteIconSpecial").css("visibility", "inherit");
    //        alert("show");
    //    },
    //            function () {
    //                $(this).find(".deleteIconSpecial").css("visibility", "hidden");
    //            });


    //$('#addNewMyOwnItem').click(function () {
    //    var useritemText = $('#ownItemtxt').val();

    //    var url = '/recipesnew/addtoShoppingListUserItemsnew/' + useritemText;
    //    $.ajax({
    //        type: "POST",
    //        url: "/RecipeNew/addtoShoppingListUserItems",
    //        data: "itemTitle=" + useritemText,
    //        success: function (data) {
    //            $('#divUserItems').empty();
    //            $('#divUserItems').append(data);
    //            $('#ownItemtxt').val('');
    //            $("#divUserItems div").hover(function () {
    //                $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "inherit");
    //            },
    //              function () {
    //                  $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "hidden");
    //              });

    //        },
    //        error: function (xhr, ajaxOptions, thrownError) {
    //            alert('Failed to delete.')
    //        }
    //    });

    //    //$(location).attr('href', encodeURI(url));
    //});


    $('#imgSearchBtn').click(function () {
        var searchText = $('#quicksearch').val();
        var seachType;
        if ($('#radio_title').attr('checked')) {
            seachType = "title";
        }
        else {
            seachType = "ingredient";
        }
        var url = '/recipes/search/' + seachType + '/' + searchText;
        $(location).attr('href', encodeURI(url));
    });

    $('#quicksearch').bind("keydown", function (event) {
        // track enter key
        if (autocompleteFlag == 1) { return; }
        autocompleteFlag = 0;
        var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keycode == 13) { // keycode for enter key
            // force the 'Enter Key' to implicitly click the Update button
            //$('#imgSearchBtn').click();
            var searchText = $('#quicksearch').val();
            var seachType;
            if ($('#radio_title').attr('checked')) {
                seachType = "title";
            }
            else {
                seachType = "ingredient";
            }
            var tempArray = location.search.split("/");
            var baseURL = tempArray[0];
            //alert(baseURL);
            var url = '/recipes/search/' + seachType + '/' + searchText;
            $(location).attr('href', encodeURI(url));
            return false;
        } else {
            return true;
        }
    });
    $('#imgSearchBtnRecipeCalender').click(function () {
        var searchText = $('#quicksearchRecipeCalender').val();
        var seachType;
        if ($('#radio_title').attr('checked')) {
            seachType = "title";
        }
        else {
            seachType = "ingredient";
        }
        $.ajax({
            cache: false,
            type: "POST",
            url: "/searchinrecipecalender",
            data: seachType + '=' + searchText,
            success: function (data) {
                //console.debug(data);
                $('#external-events').empty();
                var h4 = "<h4>Recipe search Result for </h4>" + searchText;
                $('#external-events').add(h4);
                var itemList = data.arr;
                var pager = data.pagerStr;
                $('#pagination').empty();
                $('#pagination').append(pager.toString());

                $.each(itemList, function (index) {
                    //console.debug(data[index].Title);
                    var recipe = "<div class='external-event' id='" + itemList[index].id + "'><img src='" + itemList[index].Photo + "'/>" + itemList[index].title + "</div>"
                    $('#external-events').append(recipe);

                });
                setrecipeDraggableforCalender();

            }
        })
    });
    $('#quicksearchRecipeCalender').bind("keydown", function (event) {
        // track enter key
        if (autocompleteFlag == 1) { return; }
        autocompleteFlag = 0;
        var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keycode == 13) {
            var searchText = $('#quicksearchRecipeCalender').val();
            var seachType;
            if ($('#radio_title').attr('checked')) {
                seachType = "title";
            }
            else {
                seachType = "ingredient";
            }


            $.ajax({
                cache: false,
                type: "POST",
                url: "/searchinrecipecalender",
                data: seachType + '=' + searchText,
                success: function (data) {
                    //console.debug(data);
                    $('#external-events').empty();
                    var h4 = "<h4>Recipe search Result for </h4>" + searchText;
                    $('#external-events').add(h4);
                    var itemList = data.arr;
                    var pager = data.pagerStr;
                    $('#pagination').empty();
                    $('#pagination').append(pager.toString());

                    $.each(itemList, function (index) {
                        //console.debug(data[index].Title);
                        var recipe = "<div class='external-event' id='" + itemList[index].id + "'><img src='" + itemList[index].Photo + "'/>" + itemList[index].title + "</div>"
                        $('#external-events').append(recipe);

                    });
                    setrecipeDraggableforCalender();

                }
            })

        } else {

        }
    });
    $('#quicksearchMobile').bind("keydown", function (event) {
        if (autocompleteFlag == 1) { return; }
        autocompleteFlag = 0;
        // track enter key
        var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keycode == 13) { // keycode for enter key
            // force the 'Enter Key' to implicitly click the Update button
            //$('#imgSearchBtn').click();
            var searchText = $('#quicksearchMobile').val();
            var seachType;
            if ($('#radio-mini-1').attr('checked')) {
                seachType = "title";
            }
            else {
                seachType = "ingredient";
            }
            var tempArray = location.search.split("/");
            var baseURL = tempArray[0];
            //alert(baseURL);
            var url = '/recipes/search/' + seachType + '/' + searchText;
            $(location).attr('href', encodeURI(url));
            return false;
        } else {
            return true;
        }
    });

    $('#ul_specials_normal_view li').hover(function () {
        /* if (this.id != "liNormal") {
        $(this).css("color", "Black");
        $(this).css("font-weight", "bold");
        $(this).css("font-size", "20");
          
        }*/

        $(this).find(".deleteIconSpecial").css("visibility", "inherit");
    }, function () {
        /* if (this.id != "liNormal") {
        $(this).css("color", "Green");
        $(this).css("font-weight", "normal");
        $(this).css("font-size", "12");
        }*/
        $(this).find(".deleteIconSpecial").css("visibility", "hidden");
    });

    $('#specials_search').bind("keydown", function (event) {
        // track enter key
        var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keycode == 13) { // keycode for enter key
            // force the 'Enter Key' to implicitly click the Update button
            ondropdownchange('All');
            return false;
        } else {
            return true;
        }
    });

    $('#specials_search_mobile').bind("keydown", function (event) {
        // track enter key
        var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keycode == 13) { // keycode for enter key
            // force the 'Enter Key' to implicitly click the Update button
            ondropdownchange_mobile('All');
            return false;
        } else {
            return true;
        }
    });


    $('#txtSearch').bind("keydown", function (event) {
        if (autocompleteFlag == 1) { return; }
        autocompleteFlag = 0;
        // track enter key
        var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keycode == 13) { // keycode for enter key
            // force the 'Enter Key' to implicitly click the Update button
            var tempArray = location.search.split("/");
            var baseURL = tempArray[0];
            newquerystring = "search/" + $('#txtSearch').val();
            window.location = "/tipsandguides/" + newquerystring;
            return false;
        } else {
            return true;
        }
    });

    $('#calendar').fullCalendar({

        editable: true,
        droppable: true,

        events: "/Admin/Plugin/Recipe/Celender/getAllCalenderRecepies",
        disableDragging: true,
        dayClick: function (date, allDay, jsEvent, view) {
            var d = $.fullCalendar.formatDate(date, "yyyy-MM-dd");
            $.ajax({
                cache: false,
                type: "POST",
                url: "/Admin/Plugin/Recipe/Celender/checkIfDayisAlreadyOccupied",
                data: 'date=' + d,
                success: function (data) {

                    if (data == 'false') {

                        $("#date1").val(d);
                        $("#recipeIdText").val('');
                        $("#addRecipeId").dialog("open");

                    }
                    else {
                        //alert("day is occupied");
                        var id = parseInt(data);
                        var url = "/recipes/" + id;
                        //var  title = event.title;
                        $("#url").attr("href", url);

                        $("#url").text(url);

                        $("#date").val(d);
                        $("#Edit").dialog("open");   //open the dialog
                    }



                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert('')
                }
            });

            // change the day's background color just for fun
            // $(this).css('background-color', 'red');

        },
        eventClick: function (calEvent, jsEvent, view) {
            console.debug(calEvent.id);
            var recipeid = parseInt(calEvent.id);
            if (calEvent.url) {
                // window.open(event.url);
                $.ajax({
                    cache: false,
                    type: "POST",
                    url: "/Admin/Plugin/Recipe/Celender/ShowRecipeInCalenderPage",
                    data: 'recipeid=' + recipeid,
                    success: function (data) {
                        console.debug(data);
                        /*
                          <a href="/recipes/@ViewBag.recipeId" target="_blank"><span style="font-size: 18px;
                font-weight: bold; color: #30428d; font-family: Verdana,Arial,Helvetica,sans-serif;
                text-decoration: none">@ViewBag.recipe_title</span></a>
            <table>
                <tr>
                    <td valign="top">
                        <a href="/recipes/@ViewBag.recipeId" target="_blank">
                            <img src="@ViewBag.img_recipe_photo" border="0"><br>
                        </a><span style="font-size: 12px; font-weight: bold; color: #000000; font-family: Verdana,Arial,Helvetica,sans-serif">
                            Preparation time: @Html.Raw(Model.Preparation)<br>
                            Cooking time: @Html.Raw(Model.Cooking)
                        </span>
                        <br>
                    </td>
                    <td valign="top" style="padding-left: 30px">
                        <table>
                            <tr>
                                <td colspan="2" style="font-size: 14px; font-family: Verdana,Arial,Helvetica,sans-serif;
                                    font-weight: bold">
                                    Nutritional Info.
                                </td>
                            </tr>
                            @foreach (System.Data.DataRow row in Model.briefNutrientList.Rows)
                            {
                                <tr>
                                    <td width="150" style="font-size: 12px; font-weight: bold; font-family: Verdana,Arial,Helvetica,sans-serif">
                                        @row.ItemArray[2]
                                    </td>
                                    <td style="font-size: 12px; font-family: Verdana,Arial,Helvetica,sans-serif">
                                        @row.ItemArray[4] @row.ItemArray[1]
                                    </td>
                                </tr>
                            }
                            <tr>
                                <td colspan="2" align="right">
                                    <br>
                                    <a href="/recipes/@ViewBag.recipeId" target="_blank"><span style="font-size: 12px;
                                        font-family: Verdana,Arial,Helvetica,sans-serif; text-decoration: none">click for
                                        full details &gt; &gt;</span> </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
                        */

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    }
                });
                $('#recipeContent').empty();
                $('#recipeContent').append(calEvent.url);
                return false;
            }

        },
        drop: function (date, allDay) {
            var newDate = new Date(date)
            var month = newDate.getMonth() + 1
            var day = newDate.getDate()
            var year = newDate.getFullYear()


            var d = year + "/" + month + "/" + day;
            var originalEventObject = $(this).data('eventObject');

            var copiedEventObject = $.extend({}, originalEventObject);

            // alert(allDay)
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            console.debug(copiedEventObject);
            $.ajax({
                cache: false,
                type: "POST",
                url: "/Admin/Plugin/Recipe/Celender/checkIfDayisAlreadyOccupied",
                data: 'date=' + d,
                success: function (data) {

                    if (data == 'false') {


                        // $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                        // $('.fc-day'+ (day-1)).append("<input type='button' value='X' id='btnDel_" + originalEventObject.id +"' class='floatRight'/>");
                        var url = '/Admin/Plugin/Recipe/Celender/addrecipestothisday/';
                        $.ajax({
                            cache: false,
                            type: "POST",
                            url: url,
                            data: 'date=' + d + '&RecipeTitle=' + originalEventObject.title + '&RecipeID=' + originalEventObject.id,
                            success: function (data) {
                                $('#calendar').fullCalendar('refetchEvents');
                                alert('Recipe' + originalEventObject.id + " is added to date: " + d)

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert('Failed to add.')
                            }
                        });
                    }
                    else {
                        alert("day is occupied");
                    }



                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert('')
                }
            });



            //$(this).remove();


        }
        //aspectRatio: 2

        //   events: "/Event/getAllFutureEvents" + str


    });

    $("#addRecipeId").dialog({
        autoOpen: false,
        height: 170,
        width: 350,
        modal: true,
        buttons: {
            "Add Recipe Id to This Day": function () {
                var d = $("#date1").val();
                var recipeIdText = $("#recipeIdText").val();
                if (recipeIdText != '') {
                    var url = '/Admin/Plugin/Recipe/Celender/addrecipestothisday/';
                    $.ajax({

                        type: "POST",
                        url: url,
                        data: 'date=' + d + '&RecipeTitle=' + 'false' + '&RecipeID=' + parseInt(recipeIdText),
                        success: function (data) {
                            $('#calendar').fullCalendar('refetchEvents');

                        }
                    });
                    $('#calendar').fullCalendar('refetchEvents'); //the event has been removed from the database at this point so I just refetch the events
                    $(this).dialog("close");
                }

            },

        },

    });
    $("#Edit").dialog({
        autoOpen: false,
        height: 150,
        width: 350,
        modal: true,
        buttons: {
            "Delete Recipe From This Day": function () {
                var date = $("#date").val();
                $.ajax({
                    type: "POST",
                    url: "/Admin/Plugin/Recipe/Celender/deleteRecipeforThisDay/",
                    data: "date=" + date,
                    success: function (data) {
                        $('#calendar').fullCalendar('refetchEvents');

                    }
                });
                $('#calendar').fullCalendar('refetchEvents'); //the event has been removed from the database at this point so I just refetch the events
                $(this).dialog("close");
            },

        },

    });

});


function recipeListInCalender(searchType, searchTitle, pg) {

    $.ajax({
        cache: false,
        type: "POST",
        url: "/searchinrecipecalender",

        data: searchType + '=' + searchTitle + '&page=' + pg,
        success: function (data) {
            //console.debug(data);
            $('#external-events').empty();

            var itemList = data.arr;
            var pager = data.pagerStr;
            $('#pagination').empty();
            $('#pagination').append(pager.toString());

            $.each(itemList, function (index) {

                var recipe = "<div class='external-event' id='" + itemList[index].id + "'><img src='" + itemList[index].Photo + "'/>" + itemList[index].title + "</div>"
                $('#external-events').append(recipe);

            });
            setrecipeDraggableforCalender();

        }
    })
}

function deleteshoppinglistuseritem(userItemId) {
    // var useritemText = $('#ownItemtxt').val();
    var nuserItemId = parseInt(userItemId);
    // var url = '/recipes/deleteShoppingListUserItems/' + useritemText;
    $.ajax({
        cache: false,
        type: "POST",
        url: "/RecipeNew/deleteShoppingListUserItems",
        data: "userItemId=" + nuserItemId,
        success: function (data) {

            $('#divUserItems').empty();
            $('#divUserItems').append(data);
            $("#divUserItems div").hover(function () {
                $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "inherit");
            },
                  function () {
                      $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "hidden");
                  });

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to delete.')
        }
    });

    //$(location).attr('href', encodeURI(url));
}

function AddtoShoppingListUserItem() {
    var useritemText = $('#ownItemtxt').val();

    var url = '/recipesnew/addtoShoppingListUserItemsnew/' + useritemText;
    $.ajax({
        type: "POST",
        url: "/RecipeNew/addtoShoppingListUserItems",
        data: "itemTitle=" + useritemText,
        success: function (data) {
            $('#divUserItems').empty();
            $('#divUserItems').append(data);
            $('#ownItemtxt').val('');
            $("#divUserItems div").hover(function () {
                $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "inherit");
            },
              function () {
                  $(this).find(".deleteIconshoppinglistuseritem").css("visibility", "hidden");
              });

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to delete.')
        }
    });
}

function specialpageSizeChange() {
    var pageSize = $('#pageSize').val();
    var current_category_id = $('#ddlSpecials_select_category').val();
    var specials_search = $('#specials_search').val();

    var tempArray = location.search.split("?");
    var baseURL = tempArray[0];
    if (current_category_id == "All") {
        newquerystring = "department_id/" + current_category_id + "/pagesize/" + pageSize + "/search/" + specials_search;
        window.location = '/specials/' + newquerystring;
    }
    else { newquerystring = "department_id/" + current_category_id + "/pagesize/" + pageSize + "/search/" + specials_search; }   //+ $('#<%=specials_search.ClientID%>').val()
    window.location = '/specials/' + newquerystring;

}
function radio_button_clicked_for_search(seachType) {
    var searchText = $('#quicksearchMobile').val();

    var tempArray = location.search.split("/");
    var baseURL = tempArray[0];
    //alert(baseURL);
    var url = '/recipes/search/' + seachType + '/' + searchText;
    $(location).attr('href', encodeURI(url));
    return false;
}

var deletingFromCookBook = false;
function deleteshoppinglistRecipe(recipeId, mobile) {
    var nRecipeId = parseInt(recipeId);

    $.ajax({
        cache: false,
        type: "POST",
        url: "/RecipeNew/DeleteRecipeFromshoppinglist",
        data: "recipeid=" + nRecipeId + "&mobileview=" + mobile,
        success: function (data) {

            $('#row_' + recipeId).remove();
            $('#divAisleSet').empty();
            $('#divAisleSet').append(data);

            if ($('#shoppinglist_head tr').length == 1) {

                //$("#row_header").remove();
                $("#shoppinglist_head").remove();
            }

             $('#ul_specials_normal_view li').hover(function () {
                /*if (this.id != "liNormal") {
                $(this).css("color", "Black");
                $(this).css("font-weight", "bold");
                $(this).css("font-size", "20");
                        

                }*/

                $(this).find(".deleteIconSpecial").css("visibility", "inherit");
            }, function () {
                /* if (this.id != "liNormal") {
                $(this).css("color", "Green");
                $(this).css("font-weight", "normal");
                $(this).css("font-size", "12");
                }*/
                $(this).find(".deleteIconSpecial").css("visibility", "hidden");
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to delete.')
        }
    });


}

var deletingFromspecial = false;
function deletespecial(specialid, mobile) {
    var nspecialid = parseInt(specialid);

    $.ajax({
        cache: false,
        type: "POST",
        url: "/RecipeNew/DeleteSpecialsFromshoppinglist",
        data: "specialid=" + nspecialid + "&mobileview=" + mobile,
        success: function (data) {

            $('#li' + specialid).remove();
            $('#divAisleSet').empty();
            $('#divAisleSet').append(data);

            $('#ul_specials_normal_view li').hover(function () {
                /*if (this.id != "liNormal") {
                $(this).css("color", "Black");
                $(this).css("font-weight", "bold");
                $(this).css("font-size", "20");
                        

                }*/

                $(this).find(".deleteIconSpecial").css("visibility", "inherit");
            }, function () {
                /* if (this.id != "liNormal") {
                $(this).css("color", "Green");
                $(this).css("font-weight", "normal");
                $(this).css("font-size", "12");
                }*/
                $(this).find(".deleteIconSpecial").css("visibility", "hidden");
            });


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to delete.')
        }
    });


}


function deleteCookbookRecipe(recipeId) {
    var nRecipeId = parseInt(recipeId);

    $.ajax({
        cache: false,
        type: "POST",
        url: "/RecipeNew/DeleteRecipeFromCookBook", /*"@(Url.Action("DeleteRecipeFromCookBook", "Recipe"))",*/
        data: "recipeid=" + nRecipeId,
        success: function (data) {
            $('#li_' + recipeId).remove();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to delete.')
        }
    });



}

function deleteCookbookRecipe_Mobie(recipeId) {
    var nRecipeId = parseInt(recipeId);
    var recipeid_showing = $('#recipeid').val()
    var recipe_title = $('#recipeTitle').val();
    if (deletingFromCookBook == false) {
        deletingFromCookBook = true;
        $.ajax({
            cache: false,
            type: "POST",
            url: "/RecipeNew/DeleteRecipeFromCookBook", /*"@(Url.Action("DeleteRecipeFromCookBook", "Recipe"))",*/
            data: "recipeid=" + nRecipeId,

            success: function (data) {
                $('#li_' + recipeId).remove();
                deletingFromCookBook = false;
                if (recipeid_showing == recipeId) {
                    $('#removeCookBook').remove();
                    var addBlock = "<a  id='addToCookbook' data-role='button' href='javascript:addCookbookRecipe_Mobie()' class='sidebar-action ui-btn ui-btn-up-b' data-theme='b'><span class='ui-btn-inner'><span class='ui-btn-text'><div class='AddIconCookBook newLine'></div>Add to my Cookbook</span></span></a>";
                    $('#divCookBook').append(addBlock);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Failed to delete.')
                deletingFromCookBook = false;
            }
        });
    }
}
var addingCookBook = false;
function addCookbookRecipe_Mobie() {
    var recipeId = $('#recipeid').val()
    var recipeTitle = $('#recipeTitle').val();
    var nRecipeId = parseInt(recipeId);

    if (addingCookBook == false) {
        addingCookBook = true;
        $.ajax({
            cache: false,
            type: "POST",
            url: "/RecipeNew/AddTocookBook_mobile",
            data: "recipeid=" + nRecipeId + "&recipetitle=" + recipeTitle,

            success: function (data) {
                $('#addToCookbook').remove();
                addingCookBook = false;
                $('#divCookBook').append("<a id='removeCookBook' data-role='button' data-icon='delete' href='javascript:deleteCookbookRecipe_Mobie(" + nRecipeId + ")' class='bg_Red ui-btn ui-btn-icon-left ui-btn-up-b' data-theme='b'><span class='ui-btn-inner'><span class='ui-btn-text'><span>Remove this From your Cookbook</span></span><span class='ui-icon ui-icon-delete ui-icon-shadow'></span></span></a>");
                if ($("#cookbook_recipes").children().length >= 1) {
                    $('#cookbook_recipes').append("<li id='li_" + recipeId + "' class='ui-btn ui-btn-icon-right ui-li ui-li-has-alt ui-corner-bottom ui-btn-up-b' data-theme='b'><div class='ui-btn-inner ui-li ui-li-has-alt'><div class='ui-btn-text'><a id='cookbookItem' class='ui-link-inherit href='/recipes/" + recipeId + "'>" + recipeTitle + "</a></div> </div><a class='ui-li-link-alt ui-btn ui-btn-up-b ui-corner-br' data-icon='delete' data-role='button' href='javascript:deleteCookbookRecipe_Mobie(" + nRecipeId + ")' title='Delete' data-theme='b'><span class='ui-btn-inner'><span class='ui-btn-text'></span><span  data-theme='b' class='ui-btn ui-btn-up-b ui-btn-icon-notext ui-btn-corner-all ui-shadow'><span class='ui-btn-inner ui-btn-corner-all'><span class='ui-btn-text'></span><span class='ui-icon ui-icon-delete ui-icon-shadow'></span></span></span></span></a></li>");
                }
                else {
                    $('#cookbook_recipes').append("<li id='li_" + recipeId + "' class='ui-btn ui-btn-icon-right ui-li ui-li-has-alt ui-corner-top ui-btn-up-b' data-theme='b'><div class='ui-btn-inner ui-li ui-li-has-alt ui-corner-top'><div class='ui-btn-text'><a id='cookbookItem' class='ui-link-inherit href='/recipes/" + recipeId + "'>" + recipeTitle + "</a></div> </div><a class='ui-li-link-alt ui-btn ui-corner-tr ui-btn-up-b' data-icon='delete' data-role='button' href='javascript:deleteCookbookRecipe_Mobie(" + nRecipeId + ")' title='Delete' data-theme='b'><span class='ui-btn-inner ui-corner-tr'><span class='ui-btn-text ui-corner-tr'></span><span data-theme='b' class='ui-btn ui-btn-up-b ui-btn-icon-notext ui-btn-corner-all ui-shadow'><span class='ui-btn-inner ui-btn-corner-all ui-corner-tr'><span class='ui-btn-text ui-corner-tr'></span><span class='ui-icon ui-icon-delete ui-icon-shadow'></span></span></span></span></a></li>");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Failed to add.')
                addingCookBook = false;
            }
        });
    }



}

var addingshoppinglist = false;
function addshoppinglist_Mobie(recipeId, servings) {

    var nRecipeId = parseInt(recipeId);
    var totalshoppinglistitems = parseInt($('#totalshoppinglistitems').val());

    if (addingshoppinglist == false) {
        addingshoppinglist = true;
        $.ajax({
            cache: false,
            type: "POST",
            url: "/RecipeNew/AddToshoppingList_mobile",
            data: "recipeid=" + nRecipeId + "&servings=" + servings,

            success: function (data) {
                $('#addToshoppingList').remove();
                addingshoppinglist = false;
                if (totalshoppinglistitems == 0) {
                    $('#divviewshoppingList').append("<a data-role='button' href='/ShoppingList' class='sidebar-action ui-btn ui-corner-bottom ui-controlgroup-last ui-btn-up-b' id='viewshoppinglist' data-theme='b'><span class='ui-btn-inner ui-corner-bottom ui-controlgroup-last'><span class='ui-btn-text'><div class='showshoppinglistIconCookBook'></div><span>View my Shopping List</span></span></span></a>");
                }
                $('#totalshoppinglistitems').val(totalshoppinglistitems + 1);
                $('#divshoppingList').append("<a id='removefromshoppinglist' class='bg_Red ui-btn ui-btn-icon-left ui-corner-top ui-btn-up-b' data-icon='delete' data-role='button' href='javascript:deleteshoppinglistRecipe_mobile(" + nRecipeId + ")' data-theme='b'><span class='ui-btn-inner ui-corner-top'><span class='ui-btn-text'>Remove this from your Shopping List</span><span class='ui-icon ui-icon-delete ui-icon-shadow'></span></span></a>");

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Failed to add.')
                addingshoppinglist = false;
            }
        });
    }



}

function deleteshoppinglistRecipe_mobile(recipeId) {
    var nRecipeId = parseInt(recipeId);
    var servings = $('#sevings').val();
    var totalshoppinglistitems = parseInt($('#totalshoppinglistitems').val());

    $.ajax({
        cache: false,
        type: "POST",
        url: "/RecipeNew/DeleteRecipeFromshoppinglist",
        data: "recipeid=" + nRecipeId,
        success: function (data) {

            deletingFromCookBook = false;
            $('#removefromshoppinglist').remove();
            if (totalshoppinglistitems == 1) {
                $('#viewshoppinglist').remove();
            }
            $('#totalshoppinglistitems').val(totalshoppinglistitems - 1);
            var addBlock = "<a id='addToshoppingList' data-role='button' href='javascript:addshoppinglist_Mobie(" + nRecipeId + ",\"" + servings + "\")' class='sidebar-action ui-btn ui-corner-top ui-btn-up-b' data-theme='b'><span class='ui-btn-inner ui-corner-top'><span class='ui-btn-text'><div class='AddIconCookBook newLine'></div>Add to my Shopping List</span></span></a>";
            $('#divshoppingList').append(addBlock);


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to delete.')
        }
    });


}

function HideDetail() {
    $('.nu_detail').hide();
    $('.nu_brief').show();
    $('#ancMore').text('Click for more detail');
}
function DisplayDetail() {
    if ($('#ancMore').attr('title') == 'show') {
        $('.nu_detail').show();
        $('.nu_brief').hide();
        $('#ancMore').text('Click for less detail');
        $('#ancMore').attr('title', 'hide');
    }
    else {
        $('.nu_detail').hide();
        $('.nu_brief').show();
        $('#ancMore').text('Click for more detail');
        $('#ancMore').attr('title', 'show');
    }
}
function btnScale_Click(recipeId) {
    var servings = $('select#servings').val();

    var unit = "US";

    var tempArray = location.search.split("/");
    var baseURL = tempArray[0];
    var url = baseURL + '/recipes/' + recipeId + "/" + servings + "/" + unit + "/Scale";
    $(location).attr('href', encodeURI(url));

    return false;
}

function ondropdownchange(dept) {
    var pageSize = $('#pageSize').val();
    var current_category_id = $('#ddlSpecials_select_category').val();
    if (dept == "All") current_category_id = "All";
    var specials_search = $('#specials_search').val();

    var tempArray = location.search.split("?");
    var baseURL = tempArray[0];
    if (current_category_id == "All") {
        newquerystring = "department_id/" + current_category_id + "/pagesize/" + pageSize + "/search/" + specials_search;
        window.location = '/specials/' + newquerystring;
    }
    else { newquerystring = "department_id/" + current_category_id + "/pagesize/" + pageSize + "/search/" + specials_search; }   //+ $('#<%=specials_search.ClientID%>').val()
    window.location = '/specials/' + newquerystring;
}
function ondropdownchange_mobile(dept) {

    var current_category_id = $('#ddlSpecials_select_category').val();
    if (dept == "All") current_category_id = "All";
    var specials_search = $('#specials_search_mobile').val();

    var tempArray = location.search.split("?");
    var baseURL = tempArray[0];
    if (current_category_id == "All") {
        newquerystring = "department_id/" + current_category_id + "/search/" + specials_search;
        window.location = '/specials/' + newquerystring;
    }
    else { newquerystring = "department_id/" + current_category_id + "/search/" + specials_search; }   //+ $('#<%=specials_search.ClientID%>').val()
    window.location = '/specials/' + newquerystring;
}

function saveSpecialToShoppingList(id, brand, product, description, offer, image, recipe_title, recipe_id, featured) {
    // alert(id);
    $.ajax({
        cache: false,
        type: "POST",
        url: "/RecipeNew/AddTospecialshoppingList", /*"@(Url.Action("AddTospecialshoppingList", "Recipe"))",*/
        data: "id=" + id + "&brand=" + brand + "&product=" + product + "&description=" + description + "&offer=" + offer + "&image=" + image + "&recipe_title=" + recipe_title + "&recipe_id=" + recipe_id + "&featured=" + featured,
        success: function (data) {
            $("#special_adding_" + id).hide();
            $("#special_added_" + id).show();
            $("#special_added_" + id + " .sidebar-action").show();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to add.')
        }
    });

}

