﻿using System.Data.Entity.ModelConfiguration;
using System;
using Nop.Plugin.RecipeNew.Domain;

namespace Nop.Plugin.RecipeNew.Data
{
    public class CookBookMap: EntityTypeConfiguration<CookBook> {

        public CookBookMap()
        {
            ToTable("Nop_RecipeCookbook");

            //Map the primary key
            HasKey(m => m.Id);

            //Map the additional properties
            Property(m => m.CustomerID);
            Property(m => m.RecipeID);
           
            //Avoiding truncation/failure so we set the same max length used in the product tame
            Property(m => m.RecipeTitle).HasMaxLength(400);
           
        }
    }
}
