﻿using Nop.Plugin.RecipeNew.Domain;
using System.Collections.Generic;
using System;

namespace Nop.Plugin.RecipeNew.Services
{
    public interface IRecipeCalenderService
    {

        /// <summary>
        /// Get All Recipe Calender
        /// </summary>
        /// <returns></returns>
        List<RecipeCalender> GetAllRecipeCalender();

        /// <summary>
        /// Inserts  recipe to a Day in calender
        /// </summary>
        /// <param name="recipecalenderItem">recipecalenderItem</param>
        void InsertRecipetoADay(RecipeCalender recipecalenderItem);

        /// <summary>
        /// get Recipe by date
        /// </summary>
        /// <param name="date">date</param>
        RecipeCalender GetRecipeBydate(DateTime date);

        /// <summary>
        /// get Recipe by date
        /// </summary>
        /// <param name="date">date</param>
        RecipeCalender GetLastAddedRecipe();

         /// <summary>
        /// delete recipe calender item from recipe calender
        /// </summary>
        /// <param name="date">date</param>
        void DeleteRecipeCalenderItem(DateTime date);
    }
}
