﻿using Nop.Plugin.RecipeNew.Domain;
using System.Collections.Generic;
using System;

namespace Nop.Plugin.RecipeNew.Services
{
    public interface IShoppinglistOwnItemsService
    {
        
        /// <summary>
         /// get all shoppinglistuserownitems by customer id
         /// </summary>
         /// <param name="customerID">customerID</param>
        IList<ShoppinglistOwnItems> GetAllshoppinglistuserownitemsByCustomerID(int customerID);

        /// <summary>
         /// Inserts a users Own item
         /// </summary>
         /// <param name="userownitem">userownitem</param>
         void InsertShoppinglistOwnItems(ShoppinglistOwnItems userownitem);

       

        /// <summary>
         /// delete item from 
         /// </summary>
         /// <param name="ShoppinglistOwnItems">userownitem</param>
         void DeleteshoppinglistOwnItem(ShoppinglistOwnItems userownitem);


         /// <summary>
         /// get shoppinglistuseritem by id
         /// </summary>
         /// <param name="userItemId">userItemId</param>

         ShoppinglistOwnItems GetshoppinglistuseritemById(int userItemId);
        

       
    }
}
