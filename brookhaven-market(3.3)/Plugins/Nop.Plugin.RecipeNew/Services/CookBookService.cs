﻿using Nop.Plugin.RecipeNew.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Services.Events;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using Newtonsoft.Json;
using Nop.Services.Configuration;

namespace Nop.Plugin.RecipeNew.Services
{
    public class CookBookService : ICookBookService
    {
         private readonly IRepository<CookBook> _cookbookRecordRepository;
         private readonly IEventPublisher _eventPublisher;
         private readonly ISettingService _settingContext;

         public CookBookService(IRepository<CookBook> cookbookRecordRepository, IEventPublisher eventPublisher, ISettingService settingContext)
         {
             _cookbookRecordRepository = cookbookRecordRepository;
             _eventPublisher = eventPublisher;
             _settingContext = settingContext;
        }


         #region Implementation of ICookBookService

         /// <summary>
         /// get all cookbook by customer id
         /// </summary>
         /// <param name="customerID">customerID</param>
         public IList<CookBook> GetAllCookbooksByCustomerID(int customerID)
         {
             var context = _cookbookRecordRepository;
             //return db.RecipeCookbook.Where( SingleOrDefault(x => x.CustomerID == customerID);
             List<CookBook> cookbooks = (from u in _cookbookRecordRepository.Table
                                         orderby u.RecipeTitle
                                         where u.CustomerID == customerID
                                         select u).ToList();
             return cookbooks;
         }

         /// <summary>
         /// Inserts a cookbook item
         /// </summary>
         /// <param name="cookbookItem">cookbookItem</param>
         public virtual void InsertCookBook(CookBook cookbookItem)
         {
             if (cookbookItem == null)
                 throw new ArgumentNullException("cookbookItem");

             _cookbookRecordRepository.Insert(cookbookItem);

             //event notification
             _eventPublisher.EntityInserted(cookbookItem);
         }

         /// <summary>
         /// get cookbook by customer id and recipe id
         /// </summary>
         /// <param name="customerId">customerId</param>
         ///<param name="recipeId">recipeId</param>
         public int GetCookbookId(int customerId, int recipeId)
         {
             var context = _cookbookRecordRepository;
            
             List<CookBook> cookbooks = (from u in context.Table
                                               where u.CustomerID == customerId && u.RecipeID == recipeId
                                               select u).ToList();
             if (cookbooks.Count > 0) return cookbooks[0].Id;
             else return -1;
         }

         /// <summary>
         /// delete item from cookbook
         /// </summary>
         /// <param name="cookbookId">cookbookId</param>
         public void DeleteCookBookItem(int cookbookId)
         {
             if (cookbookId == 0)
                 return;
             CookBook cookbook = GetCookbookById(cookbookId);

             _cookbookRecordRepository.Delete(cookbook);
            
            //event notification
            _eventPublisher.EntityDeleted(cookbook);
             
         }

         /// <summary>
         /// get cookbook by id
         /// </summary>
         /// <param name="cookbookId">cookbookId</param>
         public CookBook GetCookbookById(int cookbookID)
         {
             var db = _cookbookRecordRepository;
             return db.Table.SingleOrDefault(x => x.Id == cookbookID);
         }

         /// <summary>
        /// Logs the specified record.
        /// </summary>
        /// <param name="record">The record.</param>
        public void Log(CookBook record) {
            _cookbookRecordRepository.Insert(record);
        }

        #endregion

        #region utility
        public byte[] GetResponseFromUrl(string AccessToken, string Url)
        {
            byte[] totalResponse;
            using (var client = new WebClient())
            {
                client.Headers.Add("Authorization", "Bearer " + AccessToken);
                var responseRecipeJson = client.DownloadData(Url);
                totalResponse = responseRecipeJson;
            }
            return totalResponse;
        }
        #endregion
    }
}
