﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Plugin.Events.Domain;
using Nop.Plugin.Events.Models;
using Nop.Plugin.Events.Services;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.UI.Captcha;
using Nop.Data;
using Nop.Admin.Models.Customers;
using Nop.Web.Framework.Kendoui;

namespace Nop.Plugin.Events.Controllers
{
    public class EventController : Controller
    {
        #region Fields

        private readonly IEventService _eventservice;
        private readonly IEventCommentService _eventCommentservice;
        private readonly IEvent_CustomerRole_customMappingService _event_customerRole_customMappingservice;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPictureService _pictureService;
        private readonly CustomerSettings _customerSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly IWebHelper _webHelper;
				private readonly CaptchaSettings _captchaSettings;

        #endregion

        #region ctor

        public EventController(IEventService eventservice, AdminAreaSettings adminAreaSettings, ILanguageService languageService, CaptchaSettings captchaSettings,
              ILocalizationService localizationService, IWorkContext workContext, IEventCommentService eventCommentservice,
            IEvent_CustomerRole_customMappingService event_customerRole_customMappingservice, ICustomerService customerService, IDateTimeHelper dateTimeHelper,
            CustomerSettings customerSettings, IPictureService pictureService, MediaSettings mediaSettings, IWebHelper webHelper)
        {
            _eventservice = eventservice;
            _adminAreaSettings = adminAreaSettings;
            _languageService = languageService;
            _localizationService = localizationService;
            _workContext = workContext;
            _eventCommentservice = eventCommentservice;
            _event_customerRole_customMappingservice = event_customerRole_customMappingservice;
            _customerService = customerService;
            _dateTimeHelper = dateTimeHelper;
            _customerSettings = customerSettings;
            _pictureService = pictureService;
            _mediaSettings = mediaSettings;
            _webHelper = webHelper;
						_captchaSettings = captchaSettings;
        }

        #endregion

        #region Event methodes

        public ActionResult Index()
        {
            List<EventModel> eventModelList = getAlleventsForthisStoreName();
            IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();
            ViewBag.storeRoleSelectorString = getAllStoreRole();
            ViewBag.totalStore = storeRoles.Count;
            return View("Index", eventModelList);
        }

        public ActionResult Eventlanding()
        {
            List<EventModel> eventModelList = getAlleventsForthisStoreName();
            IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();
            ViewBag.storeRoleSelectorString = getAllStoreRole();
            ViewBag.totalStore = storeRoles.Count;
            return View("EventLanding", eventModelList);
        }


        public ActionResult Details(int eventId)
        {
            Event eventItem = _eventservice.GetEventsById(eventId);

            // EventModel eventmodel = eventItem.ToModel();

            if (eventItem == null || !eventItem.Published)
                return RedirectToAction("Index", "Home");

            var model = new EventItemModel();
            PrepareNewsItemModel(model, eventItem, true);
            IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();
            ViewBag.storeRoleSelectorString = getAllStoreRole();
            ViewBag.totalStore = storeRoles.Count;
            //return View(model);
            return View("Details", model);
        }

        [HttpPost, ActionName("Details")]
        [FormValueRequired("add-comment")]
				[CaptchaValidator]
				public ActionResult EventCommentAdd(int eventId, EventItemModel model, bool captchaValid)
        {
            IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();
            ViewBag.storeRoleSelectorString = getAllStoreRole();
            ViewBag.totalStore = storeRoles.Count;
            Event eventItem = _eventservice.GetEventsById(eventId);
            if (eventItem == null || !eventItem.Published || !eventItem.AllowComments)
                return RedirectToAction("Index", "Home");

            //if (_workContext.CurrentCustomer.IsGuest())// && !_newsSettings.AllowNotRegisteredUsersToLeaveComments)
            //{
            //    ModelState.AddModelError("", _localizationService.GetResource("Event.Comments.OnlyRegisteredUsersLeaveComments"));
            //}
						//validate CAPTCHA
						if(_captchaSettings.Enabled && _captchaSettings.ShowOnEventCommentPage && !captchaValid)
						{
							ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptcha"));
						}

            if (ModelState.IsValid)
            {
                var comment = new EventComment()
                {
                    EventId = eventItem.Id,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    IpAddress = _webHelper.GetCurrentIpAddress(),
                    CommentTitle = model.AddNewComment.CommentTitle,
                    CommentText = model.AddNewComment.CommentText,
                    //IsApproved = true,
                    createdOn = DateTime.UtcNow,
                    // UpdatedOnUtc = DateTime.UtcNow,
                };
                _eventCommentservice.InsertEventComment(comment);

                //The text boxes should be cleared after a comment has been posted
                //That' why we reload the page
                TempData["nop.events.addcomment.result"] = _localizationService.GetResource("events.Comments.SuccessfullyAdded");
                return RedirectToRoute("eventDetails", new { eventId = eventId });
            }


            //If we got this far, something failed, redisplay form
            PrepareNewsItemModel(model, eventItem, true);
            //return RedirectToRoute("eventDetails", new { eventId = eventId });
            return View("Details", model);
        }


        [HttpPost]
        public string ChangeStoreSelector(int[] customerrole_ids)
        {

            List<int> EventIdsadded = new List<int>();
            string indexString = "<div></div>";
            Session["All"] = "0";
            if (customerrole_ids != null && !customerrole_ids.Contains(-1))
            {
                foreach (int i in customerrole_ids)
                {
                    if (i != -1)
                    {
                        IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();
                       
                        int customerRoleId = i;
                        List<int> eventIds = _event_customerRole_customMappingservice.getAllEventIdByCustomerRoleId(customerRoleId);

                        foreach (int eventId in eventIds)
                        { 
                            if (!EventIdsadded.Contains(eventId))
                            {
                                EventIdsadded.Add(eventId);
                                 
                                Event eventItem = _eventservice.GetEventsById(eventId);

                                EventModel eventmodel = eventItem.ToModel();

                                if (eventItem.Published == true)
                                {
                                    //eventModelList.A dd(eventmodel);
                                    indexString += "<div class='eventDiv'><div class='eventTitle'><a href='/Events/" + eventItem.Id + "'>" + eventItem.Title + "</a>- " + eventItem.DayTime.ToLongDateString() + "</div><div class='eventShortDescription'> " + (eventItem.ShortDescription) + "</div> <div><a href='/Events/" + eventItem.Id + "'>Details</a></div><br /></div>";

                                }
                            }
                        }
                    }

                }
            } 
            else if (customerrole_ids == null)
            {
                indexString = "<div></div>";
            }
            if (customerrole_ids.Contains(-1))
            {
                List<Event> events = _eventservice.GetAllEventsForGuest();
                Session["All"] = "1";
                foreach (Event eventItem in events)
                {
                    EventModel eventmodel = eventItem.ToModel();

                    if (eventItem.Published == true)
                    {
                        indexString += "<div class='eventDiv'><div class='eventTitle'><a href='/Events/" + eventItem.Id + "'>" + eventItem.Title + "</a>- " + eventItem.DayTime.ToLongDateString() + "</div><div class='eventShortDescription'> " + (eventItem.ShortDescription) + "</div> <div><a href='/Events/" + eventItem.Id + "'>Details</a></div><br /></div>";
                    }
                }
            }

            return indexString;
        }


        [HttpGet]
        public ActionResult getAllFutureEvents(int[] customerrole_ids = null)
        {

            List<int> EventIdsadded = new List<int>();
            //Session["All"] = "0";
            IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();
            foreach (int i in customerrole_ids)
            {
                if (i != -1)
                {
                    for (int j = 0; j < storeRoles.Count; j++)
                    {
                        if (storeRoles[j].Id == i)
                        {

                            Session[j.ToString()] = "1";
                        }
                    }
                }
                else
                {
                    Session["All"] = "0";
                }
            }
            List<EventObjectModel> eventModelList = new List<EventObjectModel>();
            if (customerrole_ids != null && !customerrole_ids.Contains(-1))
            {
                ////for (int i = 0; i < storeRoles.Count; i++)
                ////{


                ////    if (Session[i.ToString()] == null || Session[i.ToString()].ToString() == "0")
                ////    {

                ////        Session[i.ToString()] = "0";
                ////        // string a = Session[i.ToString()].ToString();
                ////    }
                ////    else
                ////    {
                ////        //flag = 1;
                ////    }
                ////}
                foreach (int i in customerrole_ids)
                {
                    if (i != -1)
                    {
                        int customerRoleId = i;
                        List<int> eventIds = _event_customerRole_customMappingservice.getAllEventIdByCustomerRoleId(customerRoleId);
                       
                        //for (int j = 0; j < storeRoles.Count; j++)
                        //{
                        //    if (storeRoles[j].Id == i)
                        //    {

                        //        Session[j.ToString()] = "1";
                        //    }
                        //}

                        foreach (int eventId in eventIds)
                        {
                            if (!EventIdsadded.Contains(eventId))
                            {
                                EventIdsadded.Add(eventId);

                                Event eventItem = _eventservice.GetEventsById(eventId);

                                if (eventItem.Published == true)
                                {
                                    EventObjectModel obj = new EventObjectModel();
                                    obj.url = "/Events/" + eventItem.Id;
                                    obj.title = eventItem.Title;
                                    obj.start = eventItem.DayTime.ToShortDateString();

                                    eventModelList.Add(obj);
                                }
                            }
                        }
                    }

                }
            }

            else if (customerrole_ids == null || customerrole_ids.Contains(-1))
            {
                //if (customerrole_ids.Contains(-1))
               //{
                    //Session["All"] = "1";
               // }
                List<Event> events = _eventservice.GetAllEventsForGuest();
                foreach (Event eventItem in events)
                {
                    EventModel eventmodel = eventItem.ToModel();

                    if (eventItem.Published == true)
                    {

                        EventObjectModel obj = new EventObjectModel();
                        obj.url = "/Events/" + eventItem.Id;
                        obj.title = eventItem.Title;
                        obj.start = eventItem.DayTime.ToShortDateString();

                        eventModelList.Add(obj);
                    }
                }
            }

            return Json(eventModelList, JsonRequestBehavior.AllowGet);
        }

        #region Utilities

        [HttpPost]
        private string getAllStoreRole()
        {
            IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();
            string storeRoleSelectorString = "";//"<select id='StoreRoleSelector' onchange='storeroleselectorchange()'>";
            var flag = 0;
            for (int i = 0; i < storeRoles.Count; i++)
            {

                
                if (Session[i.ToString()] == null || Session[i.ToString()].ToString() == "0")
                {
                   
                    Session[i.ToString()] = "0";
                     string a = Session[i.ToString()].ToString();
                }
                else
                {
                    flag = 1;
                }
            }
            for (int i = 0; i < storeRoles.Count; i++)
            {
                storeRoleSelectorString += "<input type='checkbox' id='SelectedCustomerRoleIds" + i + "' value='" + storeRoles[i].Id + "'";
                var customerRoles = _workContext.CurrentCustomer.CustomerRoles;

                if (flag == 0)
                {
                    if (customerRoles.Contains(storeRoles[i]))
                    {
                        storeRoleSelectorString += " checked='checked'";
                        Session[i.ToString()] = "1";
                    }
                }
                else
                {
                    if (Session[i.ToString()].ToString() == "1")
                    {
                        storeRoleSelectorString += " checked='checked'";
                        Session[i.ToString()] = "1";
                    }
                }
                storeRoleSelectorString += " onchange='showCalender(" + storeRoles.Count + ")' />" + storeRoles[i].Name + ", ";
                // storeRoleSelectorString += "<option value='" + storeRoles[i].Id + "'>" + storeRoles[i].Name + "</option>";
            }
            
            storeRoleSelectorString += "<input type='checkbox' id='SelectedCustomerRoleIds" + storeRoles.Count + "' value='-1' onchange='showCalender(" + storeRoles.Count + ")'";//"<option value='-1'>Show All Events</option></select>";
            if (Session["All"] != null)
            {
                storeRoleSelectorString += " checked='checked'";
                Session["All"] = "1";
            }
            storeRoleSelectorString += " />Show All Events";
            return storeRoleSelectorString;
        }

        private List<EventModel> getAlleventsForthisStoreName()
        {
            List<EventModel> eventModelList = new List<EventModel>();
            IList<CustomerRole> storeRoles = _customerService.GetCustomerRoleByStoreRole();

            var flag = 0;
            for (int i = 0; i < storeRoles.Count; i++)
            {
                
                if (Session[i.ToString()] == null)
                {
                    Session[i.ToString()] = "0";
                }
                else
                {
                    flag = 1;
                }
            }
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                if (flag == 0)
                {
                    ICollection<CustomerRole> customerRolesCollection = _workContext.CurrentCustomer.CustomerRoles;

                    List<int> EventIdsForThisCustomer = new List<int>();

                    foreach (CustomerRole i in customerRolesCollection)
                    {
                        int customerRoleId = i.Id;
                        List<int> eventIds = _event_customerRole_customMappingservice.getAllEventIdByCustomerRoleId(customerRoleId);

                        foreach (int eventId in eventIds)
                        {
                            if (!EventIdsForThisCustomer.Contains(eventId))
                            {
                                EventIdsForThisCustomer.Add(eventId);

                                Event eventItem = _eventservice.GetEventsById(eventId);

                                EventModel eventmodel = eventItem.ToModel();

                                if (eventItem.Published == true && eventItem.DayTime.Date >= DateTime.Today.Date)
                                {
                                    eventModelList.Add(eventmodel);
                                }
                            }
                        }

                    }
                }
                else
                {
                    List<int> EventIdsForThisCustomer = new List<int>();
                    for (int i = 0; i < storeRoles.Count; i++)
                    {
                        // var customerRoles = _workContext.CurrentCustomer.CustomerRoles;
                        
                        if (Session[i.ToString()].ToString() == "1")
                        {

                            int customerRoleId = storeRoles[i].Id;
                            List<int> eventIds = _event_customerRole_customMappingservice.getAllEventIdByCustomerRoleId(customerRoleId);

                            foreach (int eventId in eventIds)
                            {
                                if (!EventIdsForThisCustomer.Contains(eventId))
                                {
                                    EventIdsForThisCustomer.Add(eventId);

                                    Event eventItem = _eventservice.GetEventsById(eventId);

                                    EventModel eventmodel = eventItem.ToModel();

                                    if (eventItem.Published == true && eventItem.DayTime.Date >= DateTime.Today.Date)
                                    {
                                        eventModelList.Add(eventmodel);
                                    }
                                }
                            }


                        }

                    }
                }
                eventModelList.OrderBy(x => x.DayTime).ToList();

                return eventModelList;
            }
            else
            {
                List<Event> eventList = _eventservice.GetAllEventsForGuest();

                for (int i = 0; i < eventList.Count; i++)
                {
                    EventModel model = new EventModel();
                    model = eventList[i].ToModel();
                    if (model.Published == true && model.DayTime.Date >= DateTime.Today.Date)
                    {
                        eventModelList.Add(model);
                    }
                }
                return eventModelList;
            }
        }


        [NonAction]
        protected void PrepareNewsItemModel(EventItemModel model, Event eventItem, bool prepareComments)
        {
            if (eventItem == null)
                throw new ArgumentNullException("eventItem");

            if (model == null)
                throw new ArgumentNullException("model");

            model.Id = eventItem.Id;
            // model.SeName = eventItem.GetSeName();
            model.Title = eventItem.Title;
            model.ShortDescription = eventItem.ShortDescription;
            model.FullDescription = eventItem.FullDescription;
            model.AllowComments = eventItem.AllowComments;
            model.DayTime = _dateTimeHelper.ConvertToUserTime(eventItem.DayTime, DateTimeKind.Utc);
            model.totalComments = eventItem.EventComments.Count;
						model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnEventCommentPage ? true : false;

            string customerRolesForThisEvent = null;
            var customerRoleIds = _event_customerRole_customMappingservice.getAllCustomerRoleIdByEventId(eventItem.Id);
            for (int i = 0; i < customerRoleIds.Count; i++)
            {
                CustomerRole role = _customerService.GetCustomerRoleById(customerRoleIds[i]);

                if (i >= 1) customerRolesForThisEvent += ", ";
                customerRolesForThisEvent += role.Name.ToString();
            }
            model.CustomerRoles = customerRolesForThisEvent;
            if (prepareComments)
            {
                var eventComments = eventItem.EventComments.OrderBy(pr => pr.createdOn);
                foreach (var ec in eventComments)
                {
                    var customer = _customerService.GetCustomerById(ec.CustomerId);
                    EventCommentModel commentModel = new EventCommentModel()
                    {
                        Id = ec.Id,
                        CustomerId = ec.CustomerId,
                        CustomerName = customer.FormatUserName(),
                        CommentTitle = ec.CommentTitle,
                        CommentText = ec.CommentText,
                        createdOn = _dateTimeHelper.ConvertToUserTime(ec.createdOn, DateTimeKind.Utc),
                        AllowViewingProfiles = _customerSettings.AllowViewingProfiles && customer != null && !customer.IsGuest(),
                    };
                    if (_customerSettings.AllowCustomersToUploadAvatars)
                    {
                        // var customer = ec.Customer;
                        string avatarUrl = _pictureService.GetPictureUrl(customer.GetAttribute<int>(SystemCustomerAttributeNames.AvatarPictureId), _mediaSettings.AvatarPictureSize, false);
                        if (String.IsNullOrEmpty(avatarUrl) && _customerSettings.DefaultAvatarEnabled)
                            avatarUrl = _pictureService.GetDefaultPictureUrl(_mediaSettings.AvatarPictureSize, PictureType.Avatar);
                        commentModel.CustomerAvatarUrl = avatarUrl;
                    }
                    model.Comments.Add(commentModel);
                }
            }
        }

        #endregion

        #endregion

        #region admin methodes for events

        [AdminAuthorize]
        public ActionResult List()
        {
            return View("List");
        }

        [AdminAuthorize]
				[HttpPost]
				public ActionResult List(DataSourceRequest command)
        {
            var events = _eventservice.GetAllEvent(command.Page - 1, command.PageSize);
						var gridModel = new DataSourceResult
            {
                Data = events.Select(x =>
                {
                    var customerRoleIds = _event_customerRole_customMappingservice.getAllCustomerRoleIdByEventId(x.Id);

                    string customerRolesForThisEvent = null;

                    for (int i = 0; i < customerRoleIds.Count; i++)
                    {
                        CustomerRole role = _customerService.GetCustomerRoleById(customerRoleIds[i]);

                        if (i >= 1) customerRolesForThisEvent += ", ";
                        customerRolesForThisEvent += role.Name.ToString();
                    }

                    EventModel m = x.ToModel();
                    m.CustomerRoles = customerRolesForThisEvent;
										m.totalComments = _eventCommentservice.NumberOfCommentsByEventId(x.Id);//x.EventComments.Count;
                    return m;
                }),
                Total = events.TotalCount
            };
            return new JsonResult
						{
							Data = gridModel
						};
        }

        [AdminAuthorize]
        public ActionResult Create()
        {

            var model = new EventModel();
            IList<CustomerRole> allcustomerRoles = _customerService.GetCustomerRoleByStoreRole();
            List<CustomerRoleModel> roleModelList = new List<CustomerRoleModel>();
            for (int i = 0; i < allcustomerRoles.Count; i++)
            {
                CustomerRoleModel rolemodel = Nop.Admin.MappingExtensions.ToModel(allcustomerRoles[i]);
                roleModelList.Add(rolemodel);
            }
            model.AllCustomerRoles = roleModelList;
            return View("Create", model);
        }

        [AdminAuthorize]
				[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(EventModel model, bool continueEditing)
        {

            if (ModelState.IsValid)
            {
                Event eventItem = model.ToEntity();

                _eventservice.InsertEvent(eventItem);

                if (model.SelectedCustomerRoleIds != null)
                {
                    //Insert new entries in event_customerRole_customMapping Table
                    for (int i = 0; i < model.SelectedCustomerRoleIds.Length; i++)
                    {
                        Event_CustomerRole_CustomMapping item = new Event_CustomerRole_CustomMapping();
                        item.EventId = eventItem.Id;
                        item.CustomerRoleId = model.SelectedCustomerRoleIds[i];
                        _event_customerRole_customMappingservice.InsertEvent_CustomerRole_CustomMapping(item);
                    }
                }


                // SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Events.Added"));
                return continueEditing ? RedirectToAction("Edit", "/Plugin/Events/Event", new { id = eventItem.Id }) : RedirectToAction("List", "/Plugin/Events/Event");
            }

            IList<CustomerRole> allcustomerRoles = _customerService.GetCustomerRoleByStoreRole();
            List<CustomerRoleModel> roleModelList = new List<CustomerRoleModel>();
            for (int i = 0; i < allcustomerRoles.Count; i++)
            {
                CustomerRoleModel rolemodel = Nop.Admin.MappingExtensions.ToModel(allcustomerRoles[i]);
                roleModelList.Add(rolemodel);
            }
            model.AllCustomerRoles = roleModelList;

            return View("Create", model);

        }

        [AdminAuthorize]
        public ActionResult Edit(int id)
        {

            var eventItem = _eventservice.GetEventsById(id);
            if (eventItem == null)

                return RedirectToAction("List");


            IList<CustomerRole> allcustomerRoles = _customerService.GetCustomerRoleByStoreRole();
            List<CustomerRoleModel> roleModelList = new List<CustomerRoleModel>();
            for (int i = 0; i < allcustomerRoles.Count; i++)
            {
                CustomerRoleModel rolemodel = Nop.Admin.MappingExtensions.ToModel(allcustomerRoles[i]);
                roleModelList.Add(rolemodel);
            }


            var customerRoleIds = _event_customerRole_customMappingservice.getAllCustomerRoleIdByEventId(id);

            int[] customerRolesForThisEvent = new int[customerRoleIds.Count + 1];
            for (int j = 0; j < customerRoleIds.Count; j++)
            {
                customerRolesForThisEvent[j] = customerRoleIds[j];
            }

            EventModel model = eventItem.ToModel();
            model.AllCustomerRoles = roleModelList;
            model.SelectedCustomerRoleIds = customerRolesForThisEvent;

            return View("Edit", model);
        }

        [AdminAuthorize]
				[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(EventModel model, bool continueEditing)
        {

            var eventItem = _eventservice.GetEventsById(model.Id);

            if (eventItem == null)

                return RedirectToAction("List", "/Plugin/Events/Event");

            if (ModelState.IsValid)
            {

                eventItem.Title = model.Title;
                eventItem.AllowComments = model.AllowComments;
                eventItem.Published = model.Published;
                eventItem.ShortDescription = model.ShortDescription;
                eventItem.FullDescription = model.FullDescription;
                eventItem.DayTime = model.DayTime;

                _eventservice.UpdateEvent(eventItem);

                //delete previous entries in event_customerRole_customMapping Table
                _event_customerRole_customMappingservice.deleteAllEntryByEventId(model.Id);

                if (model.SelectedCustomerRoleIds != null)
                {
                    //Insert new entries in event_customerRole_customMapping Table
                    for (int i = 0; i < model.SelectedCustomerRoleIds.Length; i++)
                    {
                        Event_CustomerRole_CustomMapping item = new Event_CustomerRole_CustomMapping();
                        item.EventId = model.Id;
                        item.CustomerRoleId = model.SelectedCustomerRoleIds[i];
                        _event_customerRole_customMappingservice.InsertEvent_CustomerRole_CustomMapping(item);
                    }
                }
                // SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsItems.Updated"));
                return continueEditing ? RedirectToAction("Edit", "/Plugin/Events/Event", new { id = eventItem.Id }) : RedirectToAction("List", "/Plugin/Events/Event");
            }

            IList<CustomerRole> allcustomerRoles = _customerService.GetCustomerRoleByStoreRole();
            List<CustomerRoleModel> roleModelList = new List<CustomerRoleModel>();
            for (int i = 0; i < allcustomerRoles.Count; i++)
            {
                CustomerRoleModel rolemodel = Nop.Admin.MappingExtensions.ToModel(allcustomerRoles[i]);
                roleModelList.Add(rolemodel);
            }

            model.AllCustomerRoles = roleModelList;

            return View("Edit", model);

        }

        [AdminAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            var eventItem = _eventservice.GetEventsById(id);
            if (eventItem == null)
                //No news item found with the specified id
                return RedirectToAction("List", "/Plugin/Events/Event");

            //delete all entries in event_customerRole_customMapping Table for that event 
            _eventservice.DeleteEvents(eventItem);

            _event_customerRole_customMappingservice.deleteAllEntryByEventId(id);
            //  SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsItems.Deleted"));
            return RedirectToAction("List", "/Plugin/Events/Event");
        }

        #endregion

        #region admin methodes for event comments


        public ActionResult Comment(int? filterByEventItemId)
        {
            return View("CommentList");
        }
				
				[AdminAuthorize]
        [HttpPost]
				public ActionResult Comment(int? filterByEventItemId, DataSourceRequest command)
        {

            IPagedList<EventComment> comments;
            if (filterByEventItemId.HasValue)
            {
                //filter comments by news item 

                //var eventItem = _eventservice.GetEventsById(filterByEventItemId.Value);
                comments = _eventCommentservice.GetAllEventCommentByEventId(command.Page - 1, command.PageSize, filterByEventItemId.Value);
            }
            else
            {
                //load all news comments
                comments = _eventCommentservice.GetAllEventComment(command.Page - 1, command.PageSize);
            }

						var gridModel = new DataSourceResult
            {
                Data = comments.Select(eventComment =>
                {
                    var commentModel = new EventCommentModel();
                    commentModel.Id = eventComment.Id;
                    commentModel.EventId = eventComment.EventId;
                    commentModel.EventTitle = eventComment.eventItem.Title;
                    commentModel.CustomerId = eventComment.CustomerId;
                    commentModel.IpAddress = eventComment.IpAddress;
                    commentModel.createdOn = _dateTimeHelper.ConvertToUserTime(eventComment.createdOn, DateTimeKind.Utc);
                    commentModel.CommentTitle = eventComment.CommentTitle;
                    commentModel.CommentText = Core.Html.HtmlHelper.FormatText(eventComment.CommentText, false, true, false, false, false, false);
                    return commentModel;
                }),
                Total = comments.TotalCount,
            };

						return new JsonResult
						{
							Data = gridModel
						};

        }

        //[GridAction(EnableCustomBinding = true)]
				public ActionResult CommentDelete(int? filterByEventItemId, int id, DataSourceRequest command)
        {
            var comment = _eventCommentservice.GetEventCommentByEventCommentId(id);
            if (comment == null)
                throw new ArgumentException("No comment found with the specified id");
            _eventCommentservice.DeleteEventComment(comment);

            return Comment(filterByEventItemId, command);
        }

        #endregion
    }
}

