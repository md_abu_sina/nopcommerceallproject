﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Events
{
    public class RouteProvider : IRouteProvider
    {
      
        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
           
            routes.MapRoute("eventIndex", "Events",
                            new { controller = "Event", action = "Index" },
                            new[] { "Nop.Plugin.Events.Controllers" });
            routes.MapRoute("ChangeStoreSelector", " Event/ChangeStoreSelector",
                            new { controller = "Event", action = "ChangeStoreSelector" },
                            new[] { "Nop.Plugin.Events.Controllers" });
            routes.MapRoute("getAllFutureEvents", " Event/getAllFutureEvents",
                            new { controller = "Event", action = "getAllFutureEvents" },
                            new[] { "Nop.Plugin.Events.Controllers" });
            routes.MapRoute("eventLanding", "Eventslanding",
                           new { controller = "Event", action = "Eventlanding" },
                           new[] { "Nop.Plugin.Events.Controllers" });
            routes.MapRoute("eventDetails", "Events/{eventId}",
                          new { controller = "Event", action = "Details", eventId = "" },
                           new { eventId = @"\d+" },
                           new[] { "Nop.Plugin.Events.Controllers" });
           
            routes.MapRoute("Admin.Plugin.Events.Event.List", "admin/Plugin/Events/Event/List",
                            new { controller = "Event", action = "List" },
                            new[] { "Nop.Plugin.Events.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.Events.Event.Create", "admin/Plugin/Events/Event/Create",
                           new { controller = "Event", action = "Create" },
                           new[] { "Nop.Plugin.Events.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.Events.Event.Edit", "admin/Plugin/Events/Event/Edit/{id}",
                          new { controller = "Event", action = "Edit", id=UrlParameter.Optional },
                          new[] { "Nop.Plugin.Events.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.Events.Event.Delete", "Admin/Plugin/Events/Event/Delete/{id}",
                           new { controller = "Event", action = "DeleteConfirmed", id = UrlParameter.Optional },
                          new[] { "Nop.Plugin.Events.Controllers" }).DataTokens.Add("area", "admin");
            //Admin/Event/Comments
            routes.MapRoute("Admin.Plugin.Events.Comments.filterByEventItemId", "Admin/Plugin/Events/Event/Comment/filterByEventItemId/{filterByEventItemId}",
                            new { controller = "Event", action = "Comment", filterByEventItemId = UrlParameter.Optional },
                             new[] { "Nop.Plugin.Events.Controllers" }).DataTokens.Add("area", "admin");

             routes.MapRoute("Admin.Plugin.Events.Comments.List", "Admin/Plugin/Events/Event/Comment",
                           new { controller = "Event", action = "Comment" },
                            new[] { "Nop.Plugin.Events.Controllers" }).DataTokens.Add("area", "admin");

             routes.MapRoute("Admin.Plugin.Events.Event.Comment.Delete", "Admin/Plugin/Events/Event/CommentDelete/{id}",
                            new { controller = "Event", action = "CommentDelete", id = UrlParameter.Optional },
                           new[] { "Nop.Plugin.Events.Controllers" }).DataTokens.Add("area", "admin");
            
        }

              public int Priority
                {
                    get { return 0; }
                }

           #endregion
    }
}