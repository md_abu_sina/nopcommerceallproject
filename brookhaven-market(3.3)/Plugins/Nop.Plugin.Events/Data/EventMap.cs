﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Events.Domain;

namespace Nop.Plugin.Events.Data
{
    public partial class EventMap : EntityTypeConfiguration<Event>
    {
        public EventMap()
        {
           // this.ToTable("Nop_Event");

            HasKey(m => m.Id);
                       

            Property(e => e.Title);
            Property(e => e.DayTime);
            Property(e => e.ShortDescription);
            Property(e => e.FullDescription);
            Property(e => e.Published).IsOptional();
            Property(e => e.AllowComments).IsOptional();
            // Property(e => e.CustomerRoles);
           // this.HasRequired(ec => ec.eventItem)
            // .WithMany(m => m.EventComments)
           //  .HasForeignKey(ec => ec.EventId);

            //this.HasMany(e => e.EventComments)
            //    .WithOptional();
               

          //  this.HasMany(e => e.CustomerRoles);// Map(m => m.ToTable("Event_CustomerRole_Mapping"));
            // this.HasRequired(e => e.CustomerRoles);
            //.Map(m => m.ToTable("Event_CustomerRole_Mapping"));

            //this.HasOptional(e => e.EventComments).WithMany();


            //this.HasRequired(e => e.CustomerRoles)

            //this.HasMany(m => m.CustomerRoles)
            //  .WithMany()
            //  .Map(m => m.ToTable("Event_CustomerRole_Mapping"));
            //this.HasRequired(m => m.CustomerRoles)
            //    .WithMany()
            //    this.HasRequired(ec => ec.eventItem)
            //         .WithMany(m => m.EventComments)
            //       .HasForeignKey(ec => ec.EventId);

        }
    }
}
