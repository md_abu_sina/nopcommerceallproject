﻿using FluentValidation;
using Nop.Plugin.Events.Models;
using Nop.Services.Localization;


namespace Nop.Plugin.Events.Validators
{
    public class EventItemValidator : AbstractValidator<EventModel>
    {
        public EventItemValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Title)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Events.Fields.Title.Required"));

            RuleFor(x => x.SelectedCustomerRoleIds)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Events.Fields.StoreName.Required"));

            RuleFor(x => x.ShortDescription)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Events.Fields.ShortDescription.Required"));

            RuleFor(x => x.FullDescription)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Events.Fields.FullDescription.Required"));

            RuleFor(x => x.DayTime)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Events.Fields.DayTime.Required"));




        }
    }
}