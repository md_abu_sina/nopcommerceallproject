﻿using Nop.Plugin.Events.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Plugin.Events.Models;

namespace Nop.Plugin.Events.Services
{
    public interface IEventService
    {
        /// <summary>
        /// get all events
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IPagedList<Event> GetAllEvent(int pageIndex, int pageSize);

        /// <summary>
        /// get all events 
        /// </summary>
        /// <returns></returns>
        List<Event> GetAllEventsForGuest();

        
        /// <summary>
        /// get All Future Events
        /// </summary>
        /// <returns>List<Event></returns>
        List<Event> getAllFutureEvents();

        /// <summary>
        /// insert event
        /// </summary>
        /// <param name="eventItem"></param>
        void InsertEvent(Event eventItem);

        /// <summary>
        ///  Get Events By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Event GetEventsById(int id);

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="eventItem"></param>
        void UpdateEvent(Event eventItem);

        /// <summary>
        /// delete event
        /// </summary>
        /// <param name="eventItem"></param>
        void DeleteEvents(Event eventItem);
    }
}
