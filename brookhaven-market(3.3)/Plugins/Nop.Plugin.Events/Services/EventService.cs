﻿using Nop.Plugin.Events.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Plugin.Events.Services;
using Nop.Core;
using Nop.Services.Events;

namespace Nop.Plugin.Events.Services
{
    public class EventService : IEventService
    {
        #region fields

        private readonly IRepository<Event> _eventRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region ctor

        public EventService(IRepository<Event> eventRepository, IEventPublisher eventPublisher)
        {
            _eventRepository = eventRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region Implementation of IEventService

        /// <summary>
        /// get all events
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<Event></returns>
        public virtual IPagedList<Event> GetAllEvent(int pageIndex, int pageSize)
        {
            //var query = _eventRepository.Table;
					var query = (from u in _eventRepository.Table
											 orderby u.DayTime
											 select u);
            var events = new PagedList<Event>(query, pageIndex, pageSize);
            return events;
        }

        /// <summary>
        /// get all Events by Store id
        /// </summary>
        /// <param name="StoreId">StoreId</param>
        public virtual List<Event> GetAllEventsForGuest()
        {
            var context = _eventRepository;

            List<Event> events = (from u in _eventRepository.Table
                                  where u.Published == true
                                  orderby u.DayTime
                                  select u).ToList();
            return events;
        }

        /// <summary>
        /// get All Future Events
        /// </summary>
        /// <returns>List<Event></returns>
        public virtual List<Event> getAllFutureEvents()
        {
            var context = _eventRepository;

            List<Event> events = (from u in _eventRepository.Table
                                  where u.Published == true && u.DayTime >= DateTime.Today 
                                  orderby u.DayTime
                                  select u).ToList();
            return events;
        }

        /// <summary>
        /// insert event
        /// </summary>
        /// <param name="eventItem"></param>
        public virtual void InsertEvent(Event eventItem)
        {
            if (eventItem == null)
                throw new ArgumentNullException("eventItem");

            _eventRepository.Insert(eventItem);

            //event notification
            _eventPublisher.EntityInserted(eventItem);
        }

        /// <summary>
        /// get event  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Event GetEventsById(int id)
        {
            var db = _eventRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="eventItem"></param>
        public void UpdateEvent(Event eventItem)
        {
            if (eventItem == null)
                throw new ArgumentNullException("eventItem");

            _eventRepository.Update(eventItem);

            //event notification
            _eventPublisher.EntityUpdated(eventItem);
        }

        /// <summary>
        /// delete event
        /// </summary>
        /// <param name="eventItem"></param>
        public void DeleteEvents(Event eventItem)
        {
            _eventRepository.Delete(eventItem);
            _eventPublisher.EntityDeleted(eventItem);
        }
        #endregion
    }

		
}

