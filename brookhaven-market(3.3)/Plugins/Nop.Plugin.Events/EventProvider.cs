﻿using Nop.Core.Plugins;
using Nop.Plugin.Events.Data;
using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Web.Framework.Menu;
namespace Nop.Plugin.Events
{
    public class EventProvider : BasePlugin, IAdminMenuPlugin
    {
        private readonly EventObjectContext _context;
        private readonly EventCommentObjectContext _eventCommentContext;
        private readonly Event_customerRole_customMappingObjectContext _event_customerRole_customMapping;

        public EventProvider(EventObjectContext context, EventCommentObjectContext eventCommentContext, Event_customerRole_customMappingObjectContext event_customerRole_customMapping)
        {
            _context = context;
            _eventCommentContext = eventCommentContext;
            _event_customerRole_customMapping = event_customerRole_customMapping;
        }

        

				public SiteMapNode BuildMenuItem()
				{
					SiteMapNode node = new SiteMapNode
					{
						Visible = true,
						Title = "Events"
					};

					node.ChildNodes.Add(new SiteMapNode
					{
						Visible = true,
						Title = "Events Items",
						Url = "/Admin/Plugin/Events/Event/List"
					});
					node.ChildNodes.Add(new SiteMapNode
					{
						Visible = true,
						Title = "Events Comments",
						Url = "/Admin/Plugin/Events/Event/Comment"
					});


					return node;
				}


        public override void Install()
        {

						//_event_customerRole_customMapping.InstallSchema();
						//_context.InstallSchema();
						//_eventCommentContext.InstallSchema();
            base.Install();

            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.AddNew", "Add New");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.BackToList", "Back To List");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.LeaveYourComment", "Leave Your Comment");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.SubmitButton", "Submit");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.CreatedOn", "Created On");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.EditEventItemDetails", "Edit Event Item Details");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events", "Events");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.Comments", "Comments");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.CommentTitle", "Comment Title");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.CommentText", "Comment Text");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.Title", "Title");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.Comment", "Comment");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.StoreName", "Store Name");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.DayTime", "Date");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.ShortDescription", "Short Description");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.FullDescription", "Full Description");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.Published", "Published");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.AllowComments", "Allow Comments");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.Title.Required", "Title is Required");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.Comment.Required", "Comment is Required");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.StoreName.Required", "Store Name is Required");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.ShortDescription.Required", "Short Description is Required");
            this.AddOrUpdatePluginLocaleResource(" Admin.ContentManagement.Events.Fields.FullDescription.Required", "Full Description is Required");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Fields.DayTime.Required", "date is Required");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.CommentTitle.Required", "Comment Title is Required");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.CommentTitle.MaxLengthValidation", "Maximum Length of Comment Title is 200");
            this.AddOrUpdatePluginLocaleResource("Events.Comments.CommentText.Required", "Comment Text is Required");
            this.AddOrUpdatePluginLocaleResource("Admin.ContentManagement.Events.Comments", "Comments");
            this.AddOrUpdatePluginLocaleResource("Event.Comments.OnlyRegisteredUsersLeaveComments", "Only Registered Users can Leave Comments");
            this.AddOrUpdatePluginLocaleResource("events.Comments.SuccessfullyAdded", "Comment added Successfully!");


        }

				public bool Authenticate()
				{
					return true;
				}

        /*
         
         /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //database objects
            _objectContext.Uninstall();

            //locales
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Country");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Country.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.StateProvince");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.StateProvince.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Zip");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Zip.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.TaxCategory");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.TaxCategory.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Percentage");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Percentage.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.AddRecord");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.AddRecord.Hint");
            
            base.Uninstall();
        }
         */


    }
}
