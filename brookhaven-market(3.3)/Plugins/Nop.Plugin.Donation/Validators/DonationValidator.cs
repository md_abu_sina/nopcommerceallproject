﻿using FluentValidation;
using Nop.Plugin.Donation.Models;
using Nop.Services.Localization;


namespace Nop.Plugin.Donation.Validators
{
    public class DonationValidator : AbstractValidator<DonationModel>
    {
        public DonationValidator(ILocalizationService localizationService)
        {
                    RuleFor(x => x.StoreName)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.Organization.Required"));
                    RuleFor(x => x.Organization)
						.NotEmpty()
						.WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.Organization.Required"));
                    RuleFor(x => x.Contact)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.Contact.Required"));
                    RuleFor(x => x.CurrentDate)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.CurrentDate.Required"));
                    RuleFor(x => x.EventDate)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.EventDate.Required"));
                    RuleFor(x => x.Title)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.Title.Required"));
                    RuleFor(x => x.Address)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.Address.Required"));
                    RuleFor(x => x.Zip)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.Zip.Required"));
                    RuleFor(x => x.State)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.State.Required"));
                    RuleFor(x => x.Phone)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.Phone.Required"));
                    RuleFor(x => x.TaxId)
                        .NotEmpty()
                        .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.TaxId.Required"));
                    RuleFor(x => x.EventDetail)
                                .NotEmpty()
                                .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.TaxId.Required"));
                    RuleFor(x => x.RequestedItem)
                                .NotEmpty()
                                .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.TaxId.Required"));
                    RuleFor(x => x.Benifit)
                                .NotEmpty()
                                .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.TaxId.Required"));
                    RuleFor(x => x.OrganizationDetail)
                                .NotEmpty()
                                .WithMessage(localizationService.GetResource("Nop.Plugin.Donation.Validation.TaxId.Required"));
                    RuleFor(x => x.Email)
                        .NotEmpty()
                        .EmailAddress()
                        .WithMessage(localizationService.GetResource("Common.WrongEmail"));
        }
    }
}