﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Donation.Models;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;
using System.Web;
using System.Web.Configuration;
using Nop.Web.Framework.UI.Captcha;

namespace Nop.Plugin.Donation.Controllers
{
    public class DonationController : BasePluginController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly PdfSettings _pdfSettings;
        private readonly ILocalizationService _localizationService;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;

        #region email fields


        private readonly ITokenizer _tokenizer;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ILanguageService _languageService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IMessageTokenProvider _messageTokenProvider;
        // private readonly IMessageTemplateForUponService _messageTemplateForUponService;
        private readonly IRepository<MessageTemplate> _messageTemplateRepository;
        private readonly IRepository<EmailAccount> _emailAccountRepository;
        //private readonly IEventPublisher _eventPublisher;

        #endregion



        #endregion

        #region ctor

        public DonationController(IWorkContext workContext, ITokenizer tokenizer, IQueuedEmailService queuedEmailService, IMessageTemplateService messageTemplateService, StoreInformationSettings storeInformationSettings,
                                               ILanguageService languageService, IEmailAccountService emailAccountService, EmailAccountSettings emailAccountSettings, IEmailSender emailSender,
                                               IMessageTokenProvider messageTokenProvider, RewardPointsSettings rewardPointsSettings, CustomerSettings customerSettings, ILogger logger, ILocalizationService localizationService,
                                               ForumSettings forumSettings, OrderSettings orderSettings, IAddressService addressService, IOrderService orderService, PdfSettings pdfSettings,
                                               IRepository<MessageTemplate> messageTemplateRepository, IRepository<EmailAccount> emailAccountRepository, IStoreContext storeContext)
        {
            _workContext = workContext;
            _pdfSettings = pdfSettings;
            _storeInformationSettings = storeInformationSettings;
            _emailSender = emailSender;
            _tokenizer = tokenizer;
            _queuedEmailService = queuedEmailService;
            _messageTemplateService = messageTemplateService;
            _languageService = languageService;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
            _messageTokenProvider = messageTokenProvider;
            _localizationService = localizationService;
            //  _messageTemplateForUponService = messageTemplateForUponService;
            _messageTemplateRepository = messageTemplateRepository;
            _emailAccountRepository = emailAccountRepository;
            _storeContext = storeContext;
            _logger = logger;
        }

        #endregion

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Shows the form.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowForm()
        {
            return View("DonationForm");
        }



        #region Email

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Submits the form.
        /// </summary>
        /// <param name="donationModel">The donation model.</param>
        /// <returns></returns>
        [HttpPost]
        [CaptchaValidator]
        public ActionResult SubmitForm(DonationModel donationModel, bool captchaValid)
        {
            //validate CAPTCHA
            if (!captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptcha"));
            }
            donationModel.StoreName = _storeContext.CurrentStore.Name;

            var languageId = EnsureLanguageIsActive(_workContext.WorkingLanguage.Id);

            var messageTemplate = GetLocalizedActiveMessageTemplate("Donation.Email", languageId);

            var randomNumber = Guid.NewGuid();
            string fileName = string.Format("donationform_{0}-{1}.html", randomNumber, donationModel.Contact);//donationform_Andrea-Montoya_485112
            string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\donation", fileName);

            //var emailBody = GetEmailBody(messageTemplate);

            //var tokens = GenerateTokens(donationModel);
            //var bodyReplaced = _tokenizer.Replace(emailBody, tokens, true);

            var bodyReplaced = RenderPartialViewToString(this, "Attached", donationModel);


            DonationHtml.ConvertToHtml(bodyReplaced, filePath);

            if (!String.IsNullOrEmpty(donationModel.TaxLetter) && !String.IsNullOrWhiteSpace(donationModel.TaxLetter))
            {
                //Combine attached file with the html file generated from the filled up Donation form
                filePath = String.Format("{0},{1}", filePath, System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\donation", donationModel.TaxLetter));    
            }
            else
            {
                ModelState.AddModelError("TaxLetter", _localizationService.GetResource("nop.plugin.donation.validation.taxletter.required"));
            }
            
            

            //DonationPdf.PrintToPdf(donationModel, _workContext.WorkingLanguage, bodyReplaced, filePath, _pdfSettings, this.Request.PhysicalApplicationPath);
            if (ModelState.IsValid)
            {
                try
                {
                    SendEmail(_workContext.WorkingLanguage.Id, donationModel, filePath);
                    //return Redirect(_storeContext.CurrentStore.Url + "donation-application-completion");
                    return RedirectToAction("SubmitSuccess");
                    //return("Email send successfuly");
                }

                catch (Exception ex)
                {
                    //string errorText = ex.ToString().Substring(0, Math.Min(ex.ToString().Length, 200));
                    _logger.Error(ex.ToString());
                    return Content(ex.ToString());
                    //return Redirect(_storeContext.CurrentStore.Url + "t/DonationApplicationCompletion");
                    //throw new Exception(ex); //wex.ToString();
                    //return Redirect("http://www.brookhavenmarket.com/t/DonationApplicationCompletion");
                }
                //return View("DonationForm");
            }
            else
            {
                donationModel.CurrentDate = donationModel.CurrentDate.Date;
                donationModel.EventDate = donationModel.EventDate.Date;
                return View("DonationForm", donationModel);
            }
            
        }

        public ActionResult SubmitSuccess()
        {
            return View("SubmitSuccess");
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Renders the partial view to string.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static string RenderPartialViewToString(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Zips the file upload.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FileUpload()
        {
            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            //Stream stream = null;
            var fileName = "";
            
            //int fileSize = 0;
            try
            {
                var fileSize = Request.InputStream.Length;
            }
            catch (Exception)
            {
                return Json(new
                {
                    success = false,
                    errorMessage = "Please upload a file less than 4 MB size"
                }, "text/plain");
            }
            

            var stream = Request.InputStream;
            //var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                //fileSize = httpPostedFile.ContentLength / 1024;
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);

                //contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileExtension = Path.GetExtension(fileName);

            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();



            fileName = Guid.NewGuid() + "-" + fileName;
            var path = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\Donation", fileName);
            var buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            System.IO.File.WriteAllBytes(path, buffer);

            var baseUrl = _storeContext.CurrentStore.Url;

            var fileInfo = new FileInfo(path);

           
            return Json(new
            {
                success = true,
                uploadedFileName = fileName
                //templateHtml = uploadedTemplateHtml
                //pictureId = picture.Id,
                //imageUrl = _pictureService.GetPictureUrl(picture, 100)
            }, "text/plain");
        }


        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Installs the donation message templates.
        /// </summary>
        public virtual void installDonationMessageTemplates()
        {

            var eaGeneral = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Donation contact")).FirstOrDefault();
            //var eaSale = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Sales representative")).FirstOrDefault();
            //var eaCustomer = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Customer support")).FirstOrDefault();
            var messageTemplates = new List<MessageTemplate>
                               {
                                   new MessageTemplate
                                       {
                                           Name = "Donation.Email",
                                           Subject = "Donation Application.",
                                           Body = "<p>An email sent \"%Donation.Contact%\".</p>",
                                           IsActive = true,
                                           EmailAccountId = eaGeneral.Id,
                                       },
                                  
                                  
                               };
            messageTemplates.ForEach(mt => _messageTemplateRepository.Insert(mt));
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Adds the email cv tokens.
        /// </summary>
        /// <param name="tokens">The tokens.</param>
        /// <param name="donationModel">The donation model.</param>
        public virtual void AddEmailCvTokens(IList<Token> tokens, DonationModel donationModel)
        {
            //personal info
            tokens.Add(new Token("Donation.Contact", donationModel.Contact));
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Generates the tokens.
        /// </summary>
        /// <param name="donationModel">The donation model.</param>
        /// <returns></returns>
        private IList<Token> GenerateTokens(DonationModel donationModel)
        {
            var tokens = new List<Token>();
            //_messageTokenProvider.AddStoreTokens(tokens);
            AddEmailCvTokens(tokens, donationModel);
            return tokens;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="languageId">The language id.</param>
        /// <param name="donationModel">The donation model.</param>
        /// <param name="attachedFilePath">The attached file path.</param>
        public virtual void SendEmail(int languageId, DonationModel donationModel, string attachedFilePath)
        {



            languageId = EnsureLanguageIsActive(languageId);

            var messageTemplate = GetLocalizedActiveMessageTemplate("Donation.Email", languageId);
            if (messageTemplate == null)
                return;

            // var tokens = new List<Token>();
            var couponTokens = GenerateTokens(donationModel);

            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);
            var toEmail = emailAccount.DestinationEmail;//"razib@brainstation-23.com";
            var toName = emailAccount.DisplayName;

            /*var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

//Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, couponTokens, false);
            var bodyReplaced = _tokenizer.Replace(body, couponTokens, true);*/


            SendNotification(messageTemplate, emailAccount,
                languageId, couponTokens,
                toEmail, toName, attachedFilePath);
        }



        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the email account of message template.
        /// </summary>
        /// <param name="messageTemplate">The message template.</param>
        /// <param name="languageId">The language id.</param>
        /// <returns></returns>
        private EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
        {
            var emailAccounId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
            var emailAccount = _emailAccountService.GetEmailAccountById(emailAccounId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            return emailAccount;

        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the email body.
        /// </summary>
        /// <param name="messageTemplate">The message template.</param>
        /// <returns></returns>
        private String GetEmailBody(MessageTemplate messageTemplate)
        {
            return messageTemplate.Body;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Generates the tokens.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns></returns>
        private IList<Token> GenerateTokens(Customer customer)
        {
            var tokens = new List<Token>();
            //  _messageTokenProvider.AddStoreTokens(tokens);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            return tokens;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Ensures the language is active.
        /// </summary>
        /// <param name="languageId">The language id.</param>
        /// <returns></returns>
        private int EnsureLanguageIsActive(int languageId)
        {
            var language = _languageService.GetLanguageById(languageId);
            if (language == null || !language.Published)
                language = _languageService.GetAllLanguages().FirstOrDefault();
            return language.Id;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the localized active message template.
        /// </summary>
        /// <param name="messageTemplateName">Name of the message template.</param>
        /// <param name="languageId">The language id.</param>
        /// <returns></returns>
        private MessageTemplate GetLocalizedActiveMessageTemplate(string messageTemplateName, int languageId)
        {
            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, _storeContext.CurrentStore.Id);
            if (messageTemplate == null)
                return null;

            //var isActive = messageTemplate.GetLocalized((mt) => mt.IsActive, languageId);
            //use
            var isActive = messageTemplate.IsActive;
            if (!isActive)
                return null;

            return messageTemplate;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends the notification.
        /// </summary>
        /// <param name="messageTemplate">The message template.</param>
        /// <param name="emailAccount">The email account.</param>
        /// <param name="languageId">The language id.</param>
        /// <param name="tokens">The tokens.</param>
        /// <param name="toEmailAddress">To email address.</param>
        /// <param name="toName">To name.</param>
        /// <param name="attachedFilePath">The attached file path.</param>
        private int SendNotification(MessageTemplate messageTemplate,
             EmailAccount emailAccount, int languageId, IEnumerable<Token> tokens,
             string toEmailAddress, string toName, string attachedFilePath)
        {
            //retrieve localized message template data
            var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
            var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
            var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            var bodyReplaced = _tokenizer.Replace(body, tokens, true);
            //_emailSender.SendAttachedEmail(emailAccount, subjectReplaced, bodyReplaced,
            // emailAccount.Email, emailAccount.Email, toEmailAddress, toName, attachedFilePath);

            var attachmentFilePath = attachedFilePath;
            var attachmentFileName = Path.GetFileName(attachedFilePath);


            var email = new QueuedEmail()
            {
                Priority = 5,
                From = emailAccount.Email,
                FromName = emailAccount.DisplayName,
                To = toEmailAddress,
                ToName = toName,
                CC = string.Empty,
                Bcc = bcc,
                Subject = subjectReplaced,
                Body = bodyReplaced,
                AttachmentFilePath = attachmentFilePath,
                AttachmentFileName = attachmentFileName,
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id
            };

            
            _queuedEmailService.InsertQueuedEmail(email);



            return email.Id;
        }
        #endregion



    }
}

