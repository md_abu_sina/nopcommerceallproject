﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Other.AnotherBlog.Models
{
    public partial class AnotherBlogPostTagModel : BaseNopModel
    {
        public string Name { get; set; }

        public int BlogPostCount { get; set; }
    }
}