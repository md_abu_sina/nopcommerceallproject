﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Blogs;
using Nop.Web.Validators.Blogs;

namespace Nop.Plugin.Other.AnotherBlog.Models
{
		[Validator(typeof(AnotherBlogPostValidator))]
		public partial class AnotherBlogPostModel : BlogPostModel
    {
        public String ShortDescription { get; set; }
    }
}