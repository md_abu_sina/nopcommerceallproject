﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Blogs;

namespace Nop.Plugin.Other.AnotherBlog.Models
{
		public partial class AnotherBlogPostListModel : BlogPostListModel
    {
       public IList<AnotherBlogPostModel> BlogPosts { get; set; }
    }
}