﻿using FluentValidation;
using Nop.Services.Localization;
using Nop.Plugin.Other.AnotherBlog.Models;

namespace Nop.Web.Validators.Blogs
{
    public class AnotherBlogPostValidator : AbstractValidator<AnotherBlogPostModel>
    {
			public AnotherBlogPostValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.AddNewComment.CommentText).NotEmpty().WithMessage(localizationService.GetResource("Blog.Comments.CommentText.Required")).When(x => x.AddNewComment != null);
        }}
}