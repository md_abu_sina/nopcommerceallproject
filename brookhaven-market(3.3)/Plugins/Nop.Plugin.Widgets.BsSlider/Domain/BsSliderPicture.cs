using System;
using Nop.Core;
using Nop.Web.Framework;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.BsSlider.Domain
{
    public class BsSliderPicture : BaseEntity
    {
        public virtual int PictureId { get; set; }
        public virtual string PictureLink { get; set; }
        public virtual int DisplayOrder { get; set; }

        public virtual string HtmlCode { get; set; }
        public virtual bool Transition { get; set; }
        public virtual bool IsProductPicture { get; set; }
        public virtual Slider Slider { get; set; }

    }
}
