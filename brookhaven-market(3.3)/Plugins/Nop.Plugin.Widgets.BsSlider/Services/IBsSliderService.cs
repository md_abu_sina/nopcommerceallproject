using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.BsSlider.Domain;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Payments;

namespace Nop.Plugin.Widgets.BsSlider.Services
{
    public interface IBsSliderService
    {

        Slider GetSliderById(int sliderId);
        List<Slider> GetSlidersByWidget(string widgetZone);
        IPagedList<Slider> GetAllSliders(int pageIndex, int pageSize);
        void InsertSlider(Slider slider);
        void UpdateSlider(Slider slider);
        void DeleteSlider(Slider slider);
    }
}