using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Plugin.Widgets.BsSlider.Domain;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Services.Orders;

namespace Nop.Plugin.Widgets.BsSlider.Services
{
    public class BsSliderService : IBsSliderService
    {
        private readonly IRepository<Slider> _sliderRepository;

        public BsSliderService(IRepository<Slider> sliderRepository)
        {
            _sliderRepository = sliderRepository;
        }

        #region Implementation of BsSliderService


        /// <summary>
        /// get event  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual Slider GetSliderById(int sliderId)
        {
            var db = _sliderRepository.Table;
            return db.SingleOrDefault(s => s.Id.Equals(sliderId));
        }


        /// <summary>
        /// get event  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual List<Slider> GetSlidersByWidget(string widgetZone)
        {
            var db = _sliderRepository.Table;
            return db.Where(s => s.WidgetZone.Equals(widgetZone)).ToList();
        }
        /// <summary>
        /// get all cateringevents
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<cateringevents></returns>
        /*public virtual IList<Slider> GetAllSliders(int pageIndex, int pageSize)
        {

            var query = (from s in _sliderRepository.Table
                         orderby s.Id
                         select s).ToList();
            var sliders = new PagedList<Slider>(query, pageIndex, pageSize);
            return query;
        }*/
        public IPagedList<Slider> GetAllSliders(int pageIndex, int pageSize)
        {
            //var query = _eventRepository.Table;
            var query = (from u in _sliderRepository.Table
                         orderby u.Id
                         select u);
            var sliders = new PagedList<Slider>(query, pageIndex, pageSize);
            //sliders.Select
            return sliders;
        }

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="eventItem"></param>
        public void InsertSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException("slider");


            _sliderRepository.Insert(slider);


        }

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="eventItem"></param>
        public void UpdateSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException("slider");


            _sliderRepository.Update(slider);


        }

        /// <summary>
        /// delete event
        /// </summary>
        /// <param name="eventItem"></param>
        public void DeleteSlider(Slider slider)
        {
            _sliderRepository.Delete(slider);
            //_eventPublisher.EntityDeleted(eventItem);
        }

        #endregion

    }
}