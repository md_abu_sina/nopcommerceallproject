﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Widgets.BsSlider
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {

            //routes.MapRoute("Admin.Plugin.BsSlider.List", "Admin/Plugin/BsSlider/Slider/List",
            //    new { controller = "BsSlider", action = "List" },
            //    new[] { "Nop.Plugin.Widgets.BsSlider.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.BsSlider.Slider.List", "Admin/Plugin/BsSlider/Slider/List",
                new { controller = "WidgetsBsSlider", action = "List" },
                new[] { "Nop.Plugin.Widgets.BsSlider.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.BsSlider.Slider.Create", "Admin/Plugin/BsSlider/Slider/Create",
                new { controller = "WidgetsBsSlider", action = "Create" },
                new[] { "Nop.Plugin.Widgets.BsSlider.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.BsSlider.Slider.Edit", "Admin/Plugin/BsSlider/Slider/Edit/{sliderId}",
                new { controller = "WidgetsBsSlider", action = "Edit", sliderId = UrlParameter.Optional },
                new[] { "Nop.Plugin.Widgets.BsSlider.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.BsSlider.Slider.PictureList", "Admin/Plugin/BsSlider/Slider/PictureList/{sliderId}",
                new { controller = "WidgetsBsSlider", action = "PictureList", sliderId = UrlParameter.Optional },
                new[] { "Nop.Plugin.Widgets.BsSlider.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.BsSlider.Slider.PictureCreate", "Admin/Plugin/BsSlider/Slider/PictureCreate/{sliderId}",
                new { controller = "WidgetsBsSlider", action = "PictureCreate", sliderId = UrlParameter.Optional },
                new[] { "Nop.Plugin.Widgets.BsSlider.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.BsSlider.Slider.PictureEdit", "Admin/Plugin/BsSlider/Slider/PictureEdit/{sliderId}/{pictureId}",
                new { controller = "WidgetsBsSlider", action = "PictureEdit", sliderId = UrlParameter.Optional, pictureId = UrlParameter.Optional },
                new[] { "Nop.Plugin.Widgets.BsSlider.Controllers" }).DataTokens.Add("area", "admin");
                      
            
            //  routes.MapRoute("CateringInvoice", "catering/catering-invoice/{orderId}",
                      //      new { controller = "Catering", action = "CateringInvoice", orderId = UrlParameter.Optional  },
                      //      new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("SetStoreID", "catering/setStoreID/{orderId}",
                        //    new { controller = "Catering", action = "SetStoreID", orderId = UrlParameter.Optional  },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.FoodMenu", "catering/foodmenu",
                        //    new { controller = "Catering", action = "FoodMenu" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.CancelEvent", "catering/cancel-event/{eventId}",
                        //    new { controller = "Catering", action = "CancelEvent", eventId = UrlParameter.Optional },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.MigrateCustomer", "catering/migrate-customer",
                        //    new { controller = "Catering", action = "MigrateCustomer"},
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.CategoryTag", "catering/categorytag/{productId}",
                        //    new { controller = "Catering", action = "CategoryTag", eventId = UrlParameter.Optional },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.ProductAttributeQuantity", "catering/attribute-quantity",
                        //    new { controller = "Catering", action = "ProductAttributeQuantity" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.CateringDeliveryTime", "catering/delivery-time/{orderId}",
                        //    new { controller = "Catering", action = "CateringDeliveryTime", orderId = UrlParameter.Optional },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });


                        //routes.MapRoute("Catering.ValidateCart", "catering/validate-cart",
                        //    new { controller = "Catering", action = "ValidateCart" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.SearchFood", "catering/search-food",
                        //    new { controller = "Catering", action = "SearchFood" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.PlaceOrder", "catering/place-order",
                        //    new { controller = "Catering", action = "PlaceOrder" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Shipping", "catering/shipping",
                        //    new { controller = "Catering", action = "Shipping" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.SaveShipping", "catering/saveShipping",
                        //    new { controller = "Catering", action = "SaveShipping" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Category", "catering/category",
                        //    new { controller = "Catering", action = "Category" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.SelectStore", "catering/selectstore",
                        //    new { controller = "Catering", action = "SelectStore" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.EventDetail", "catering/eventdetail",
                        //    new { controller = "Catering", action = "EventDetail" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.EventBrief", "catering/eventbrief",
                        //    new { controller = "Catering", action = "EventBrief" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Checkout", "onepagecheckout",
                        //    new { controller = "Catering", action = "CateringPageCheckout" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Checkout.SaveBilling", "checkout/CateringSaveBilling",
                        //    new { controller = "Catering", action = "CateringSaveBilling" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Checkout.SaveShipping", "checkout/CateringSaveShipping",
                        //    new { controller = "Catering", action = "CateringSaveShipping" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Checkout.SaveShippingMethod", "checkout/CateringSaveShippingMethod",
                        //    new { controller = "Catering", action = "CateringSaveShippingMethod" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Checkout.SavePaymentMethod", "checkout/CateringSavePaymentMethod",
                        //    new { controller = "Catering", action = "CateringSavePaymentMethod" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Checkout.SavePaymentInfo", "checkout/CateringSavePaymentInfo",
                        //    new { controller = "Catering", action = "CateringSavePaymentInfo" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Checkout.ConfirmOrder", "checkout/CateringConfirmOrder",
                        //    new { controller = "Catering", action = "CateringConfirmOrder" },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.SubCategory", "catering/viewCategory/{categoryId}",
                        //    new { controller = "Catering", action = "ViewCategory", categoryId = UrlParameter.Optional },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.Product", "catering/product/{categoryId}",
                        //    new { controller = "Catering", action = "Product", categoryId = UrlParameter.Optional },
                        //    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Catering.ViewMap", "catering/viewmap/{systemName}",
                        //                                    new { controller = "Catering", action = "ViewMap", systemName = UrlParameter.Optional },
                        //                                    new[] { "Nop.Plugin.Other.Catering.Controllers" });

                        //routes.MapRoute("Admin.Plugin.Other.Catering.StoreAddressList", "Admin/Plugin/Catering/StoreAddress/List",
                        //    new { controller = "Catering", action = "List" },
                        //                                new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.CateringList", "Admin/Plugin/Other/Catering/CateringList",
                        //    new { controller = "Catering", action = "CateringList" },
                        //                                new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.StoreAddressCreate", "Admin/Plugin/Catering/StoreAddress/Create/{id}",
                        //    new { controller = "Catering", action = "Create", id = UrlParameter.Optional },
                        //                                new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.UpdateStatus", "Admin/Plugin/Other/Catering/UpdateStatus/{id}",
                        //    new { controller = "Catering", action = "UpdateStatus", id = UrlParameter.Optional },
                        //                                new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");


                        //routes.MapRoute("Admin.Plugin.Other.Catering.EmailList", "Admin/Plugin/Other/Catering/emailList",
                        //    new { controller = "Catering", action = "EmailList" },
                        //                                new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.CreateEmail", "Admin/Plugin/Other/Catering/CreateEmail",
                        //    new { controller = "Catering", action = "CreateEmail" },
                        //                                new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.Edit", "admin/Plugin/Other/Catering/Edit/{id}",
                        //  new { controller = "Catering", action = "EditEmail", id=UrlParameter.Optional },
                        //                            new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");
                        //routes.MapRoute("Admin.Plugin.Other.Catering.Delete", "Admin/Plugin/Other/Catering/Delete/{id}",
                        //   new { controller = "Catering", action = "DeleteConfirmed", id = UrlParameter.Optional },
                        //                            new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.Order", "Admin/Plugin/Other/Catering/OrderList",
                        //                new { controller = "Catering", action = "OrderList" },
                        //                                            new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.Invoice", "Admin/Plugin/Other/Catering/Invoice/{orderId}",
                        //                            new { controller = "Catering", action = "CateringInvoiceAdmin", orderId = UrlParameter.Optional },
                        //                            new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.ReportSummary", "Admin/Plugin/Other/Catering/ReportSummary",
                        //                            new { controller = "Catering", action = "ReportSummary" },
                        //                            new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");

                        //routes.MapRoute("Admin.Plugin.Other.Catering.Report", "Admin/Plugin/Other/Catering/Report",
                        //                                        new { controller = "Catering", action = "Report" },
                        //                                        new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");
                        //routes.MapRoute("Admin.Plugin.Other.Catering.PdfInvoiceSelected", "Admin/Plugin/Other/Catering/PdfInvoiceSelected",
                        //                                                    new { controller = "Catering", action = "PdfInvoiceSelected" },
                        //                                                    new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");
                        ///*routes.MapRoute("Admin.Plugin.Other.Catering.ReportPdf", "Admin/Plugin/Other/Catering/ReportPdf",
                        //                                                    new { controller = "Catering", action = "ReportPdf" },
                        //                                                    new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");
                        //routes.MapRoute("Admin.Plugin.Other.Catering.ReportSummaryPdf", "Admin/Plugin/Other/Catering/ReportSummaryPdf",
                        //                                                    new { controller = "Catering", action = "ReportSummaryPdf" },
                        //                                                    new[] { "Nop.Plugin.Other.Catering.Controllers" }).DataTokens.Add("area", "admin");*/

                       

        }

        public int Priority
        {
            get { return 1; }
        }

        #endregion
    }
}