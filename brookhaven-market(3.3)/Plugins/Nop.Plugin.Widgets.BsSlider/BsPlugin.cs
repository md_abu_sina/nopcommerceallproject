using System.Collections.Generic;
using System.IO;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Plugin.Other.Catering.Data;
using Nop.Web.Framework.Web;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Widgets.BsSlider
{
    /// <summary>
    /// PLugin
    /// </summary>
    public class BsSliderPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        private readonly BsSliderPictureObjectContext _context;

        public BsSliderPlugin(BsSliderPictureObjectContext context)
        {
            //this._pictureService = pictureService;
            //this._settingService = settingService;
            this._context = context;
        }

        public SiteMapNode BuildMenuItem()
        {
            SiteMapNode node = new SiteMapNode
            {
                Visible = true,
                Title = "BsSlider",
                Url = "/Admin/Plugin/BsSlider/Slider/List"
            };

            


            return node;
        }

        /// <summary>
        /// Gets widget zones where this widget should be rendered
        /// </summary>
        /// <returns>Widget zones</returns>
        public IList<string> GetWidgetZones()
        {
            return new List<string>() { "catering-banner", "home_page_top" };//home_page_top
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "WidgetsBsSlider";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Widgets.BsSlider.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for displaying widget
        /// </summary>
        /// <param name="widgetZone">Widget zone where it's displayed</param>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PublicInfo";
            controllerName = "WidgetsBsSlider";
            routeValues = new RouteValueDictionary()
            {
                {"Namespaces", "Nop.Plugin.Widgets.BsSlider.Controllers"},
                {"area", null},
                {"widgetZone", widgetZone}
            };
        }
        
        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            //pictures
            //var sampleImagesPath = _webHelper.MapPath("~/Plugins/Widgets.BsSlider/Content/nivoslider/sample-images/");


            


            /*this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Picture1", "Picture 1");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Picture2", "Picture 2");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Picture3", "Picture 3");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Picture4", "Picture 4");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Picture5", "Picture 5");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Picture", "Picture");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Picture.Hint", "Upload picture.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Text", "Comment");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Text.Hint", "Enter comment for picture. Leave empty if you don't want to display any text.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Link", "URL");
            this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.BsSlider.Link.Hint", "Enter URL. Leave empty if you don't want this picture to be clickable.");*/
            _context.Install();
            base.Install();
        }
        public bool Authenticate()
        {
            return true;
        }
        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            

            //locales
            /*this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Picture1");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Picture2");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Picture3");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Picture4");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Picture5");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Picture");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Picture.Hint");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Text");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Text.Hint");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Link");
            this.DeletePluginLocaleResource("Plugins.Widgets.BsSlider.Link.Hint");*/
            //_context.Uninstall();
            base.Uninstall();
        }
    }
}
