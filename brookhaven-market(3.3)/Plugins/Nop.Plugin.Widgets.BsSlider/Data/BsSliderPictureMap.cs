using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Widgets.BsSlider.Domain;

namespace Nop.Plugin.Widgets.BsSlider.Data
{
    public class BsSliderPictureMap : EntityTypeConfiguration<BsSliderPicture>
    {
        public BsSliderPictureMap()
        {
            //ToTable("BsSlider_Picture_Mapping");

            //Map the primary key
            HasKey(m => m.Id);
            //Map the additional properties
            Property(m => m.PictureId);
            Property(m => m.PictureLink);
            Property(m => m.DisplayOrder);
            Property(m => m.HtmlCode);
            Property(m => m.Transition);
            Property(m => m.IsProductPicture);
            //this.HasRequired(bc => bc.BsSlider)
            //                    .WithMany(bp => bp.BsSliderPictures)
            //                    .HasForeignKey(bc => bc.Id);
        }
    }

}
