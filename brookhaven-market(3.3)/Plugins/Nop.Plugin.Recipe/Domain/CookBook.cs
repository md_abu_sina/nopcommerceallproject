﻿using Nop.Core;

namespace Nop.Plugin.Recipe.Domain
{
    public class CookBook : BaseEntity
    {
        /// <summary>
        /// Gets or sets the cookbook id.
        /// </summary>
        /// <value>
        /// The cookbook id.
        /// </value>
        public virtual int Id { get; set; }
        /// <summary>
        /// Gets or sets the customer id.
        /// </summary>
        /// <value>
        /// The customer id.
        /// </value>
        public virtual int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets the Recipe id.
        /// </summary>
        /// <value>
        /// The Recipe id.
        /// </value>
        public virtual int RecipeID { get; set; }

        /// <summary>
        /// Gets or sets the Recipe Title.
        /// </summary>
        /// <value>
        /// The Recipe Title.
        /// </value>
        public virtual string RecipeTitle { get; set; }


    }
   
}
