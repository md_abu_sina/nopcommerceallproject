﻿using Nop.Plugin.Recipe.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Services.Events;

namespace Nop.Plugin.Recipe.Services 
{
    public class ShoppingListService : IShoppingListService
    {
        
        private readonly IRepository<RecipeShoppingList> _RecipeShoppingListRepository;
        private readonly IEventPublisher _eventPublisher;

        public ShoppingListService(IRepository<RecipeShoppingList> RecipeShoppingListRepository, IEventPublisher eventPublisher)
         {
             _RecipeShoppingListRepository = RecipeShoppingListRepository;
             _eventPublisher = eventPublisher;
        }

        #region Implementation of IShoppingListService

       #region Get Methods
        public RecipeShoppingList GetShoppingListById(int shoppingListID)
        {
            var db = _RecipeShoppingListRepository;
            return db.Table.SingleOrDefault(x => x.Id == shoppingListID);
        }

        public IList<RecipeShoppingList> GetAllShoppingListsByCustomerID(int customerID)
        {
            var context = _RecipeShoppingListRepository;
            //return db.RecipeShoppingList.Where( SingleOrDefault(x => x.CustomerID == customerID);
            List<RecipeShoppingList> shoppingLists = (from u in context.Table
                                                      where u.CustomerID == customerID
                                                      select u).ToList();
            return shoppingLists;
        }

        /// <summary>
        /// Inserts a shoppinglist item
        /// </summary>
        /// <param name="shoppinglistItem">shoppinglistItem</param>
        public virtual void InserttoShoppingList(RecipeShoppingList shoppinglistItem)
        {
            if (shoppinglistItem == null)
                throw new ArgumentNullException("shoppinglistItem");

            _RecipeShoppingListRepository.Insert(shoppinglistItem);

            //event notification
            _eventPublisher.EntityInserted(shoppinglistItem);
        }

        /// <summary>
        /// delete item from shoppinglist
        /// </summary>
        /// <param name="shoppinglistid">shoppinglistid</param>
        public void DeleteshoppinglistItem(int shoppinglistid)
        {
            if (shoppinglistid == 0)
                return;
            RecipeShoppingList shoppinglist = GetShoppingListById(shoppinglistid);

            _RecipeShoppingListRepository.Delete(shoppinglist);

            //event notification
            _eventPublisher.EntityDeleted(shoppinglist);

        }

        public int GetShoppingListId(int customerId, int recipeId)
        {
            var context = _RecipeShoppingListRepository;
            //return db.RecipeShoppingList.Where( SingleOrDefault(x => x.CustomerID == customerID);
            List<RecipeShoppingList> shoppingLists = (from u in context.Table
                                                      where u.CustomerID == customerId && u.RecipeID == recipeId
                                                      select u).ToList();
            if (shoppingLists.Count > 0) return shoppingLists[0].Id;
            else return -1;
        }

        public IList<RecipeShoppingList> GetAllShoppingLists()
        {
            var db = _RecipeShoppingListRepository;
            return db.Table.OrderBy(x => x.RecipeID).ToList();
        }

        #endregion Get Methods


        #endregion

    }
}
