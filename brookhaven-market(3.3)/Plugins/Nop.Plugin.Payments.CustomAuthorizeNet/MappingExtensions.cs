﻿using Nop.Core.Domain.Customers;
using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Plugin.Payments.CustomAuthorizeNet.Models;
using CANet = Nop.Plugin.Payments.CustomAuthorizeNet.Domain;
using System;

namespace Nop.Plugin.Payments.CustomAuthorizeNet
{
    public static class MappingExtensions
		{


				#region CustomAuthorizeNet

				public static ConfigurationModel ToModel(this CANet.CustomAuthorizeNet entity)
				{
					ConfigurationModel model = new ConfigurationModel();
					
					model.Id = entity.Id;
					model.StoreId = entity.StoreId;
					model.UseSandbox = entity.UseSandbox;
					model.TransactModeId = entity.TransactModeId;
					model.TransactionKey = entity.TransactionKey;
					model.LoginId = entity.LoginId;
					model.AdditionalFee = entity.AdditionalFee;
					model.AdditionalFeePercentage = entity.AdditionalFeePercentage;

					return model;
				}

				public static CANet.CustomAuthorizeNet ToEntity(this ConfigurationModel model)
				{
					CANet.CustomAuthorizeNet entity = new CANet.CustomAuthorizeNet();
					
					entity.Id = model.Id;
					entity.StoreId = model.StoreId;
					entity.UseSandbox = model.UseSandbox;
					entity.TransactModeId = model.TransactModeId;
					entity.TransactionKey = model.TransactionKey;
					entity.LoginId = model.LoginId;
					entity.AdditionalFee = model.AdditionalFee;
					entity.AdditionalFeePercentage = model.AdditionalFeePercentage;

					return entity;
				}

				public static CANet.CustomAuthorizeNet ToEntity(this ConfigurationModel model, CANet.CustomAuthorizeNet entity)
				{

					entity.Id = model.Id;
					entity.StoreId = model.StoreId;
					entity.UseSandbox = model.UseSandbox;
					entity.TransactModeId = model.TransactModeId;
					entity.TransactionKey = model.TransactionKey;
					entity.LoginId = model.LoginId;
					entity.AdditionalFee = model.AdditionalFee;
					entity.AdditionalFeePercentage = model.AdditionalFeePercentage;

					return entity;
				}

				#endregion


		}
}