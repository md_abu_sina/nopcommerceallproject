﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Nop.Plugin.OrderForm.Models
{
    public class OrderFormModel : BaseNopEntityModel
    {
        public OrderFormModel()
        {
            DbSize = new List<SelectListItem>();
        }
        //public int Id { get; set; }
        public string StoreName { get; set; }

        #region Personal Information

        #region Name
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
        #endregion

        #region Address

        public string Cell { get; set; }

        public string OrderPicupDate { get; set; }

        public bool ShowTurkeys { get; set; }

        #endregion
        public List<SelectListItem> DbSize { get; set; }
        public string PumpkinPieQuantity { get; set; }
        public string PumpkinPieQuantityHalf { get; set; }

        public bool PumpkinPieShow { get; set; }
        public bool PumpkinPieShowFull { get; set; }
        public bool PumpkinPieShowHalf{ get; set; }
        public string PumpkinPiePrice { get; set; }
        public string PumpkinPiePriceHalf { get; set; }


        public string PecanPieQuantity { get; set; }
        public string PecanPieQuantityHalf { get; set; }

        public bool PecanPieShow { get; set; }
        public bool PecanPieShowFull { get; set; }

        public bool PecanPieShowHalf { get; set; }
        public string PecanPiePrice { get; set; }
        public string PecanPiePriceHalf { get; set; }



        public string TraditionalApplePieQuantity { get; set; }
        public string TraditionalApplePieQuantityHalf { get; set; }

        public bool TraditionalAppleShow { get; set; }
        public bool TraditionalAppleShowFull { get; set; }
        public bool TraditionalAppleShowHalf { get; set; }
        public string TraditionalApplePrice { get; set; }
        public string TraditionalApplePriceHalf { get; set; }


        public string AppleBerryDelightQuantity { get; set; }
        public string AppleBerryDelightQuantityHalf { get; set; }

        public bool AppleBerryDelightShow { get; set; }

        public bool AppleBerryDelightShowFull { get; set; }
        public bool AppleBerryDelightShowHalf { get; set; }

        public string AppleBerryDelightPrice { get; set; }
        public string AppleBerryDelightPriceHalf { get; set; }



        public string AppleBrownBettyPieQuantity { get; set; }
        public string AppleBrownBettyPieQuantityHalf { get; set; }

        public bool AppleBrownBettyPieShow { get; set; }
        public bool AppleBrownBettyPieShowFull { get; set; }
        public bool AppleBrownBettyPieShowHalf { get; set; }

        public string AppleBrownBettyPiePrice { get; set; }
        public string AppleBrownBettyPiePriceHalf { get; set; }



        public string AppleCaramelWalnutPieQuantity { get; set; }
        public string AppleCaramelWalnutPieQuantityHalf { get; set; }

        public bool AppleCaramelWalnutPieShow { get; set; }
        public bool AppleCaramelWalnutPieShowFull { get; set; }
        public bool AppleCaramelWalnutPieShowHalf { get; set; }



        public string AppleCaramelWalnutPiePrice { get; set; }
        public string AppleCaramelWalnutPiePriceHalf { get; set; }


        public string AppleCrispPieQuantity { get; set; }
        public string AppleCrispPieQuantityHalf { get; set; }

        public bool AppleCrispPieShow { get; set; }
        public bool AppleCrispPieShowFull { get; set; }
        public bool AppleCrispPieShowHalf { get; set; }


        public string AppleCrispPiePrice { get; set; }
        public string AppleCrispPiePriceHalf { get; set; }


        public string AppleRaspberryPieQuantity { get; set; }
        public string AppleRaspberryPieQuantityHalf { get; set; }

        public bool AppleRaspberryPieShow { get; set; }
        public bool AppleRaspberryPieShowFull { get; set; }
        public bool AppleRaspberryPieShowHalf { get; set; }


        public string AppleRaspberryPiePrice { get; set; }
        public string AppleRaspberryPiePriceHalf { get; set; }


        public string CountryCinnamonApplePieQuantity { get; set; }
        public string CountryCinnamonApplePieQuantityHalf { get; set; }

        public bool CountryCinnamonApplePieShow { get; set; }
        public bool CountryCinnamonApplePieShowFull { get; set; }
        public bool CountryCinnamonApplePieShowHalf { get; set; }


        public string CountryCinnamonApplePiePrice { get; set; }
        public string CountryCinnamonApplePiePriceHalf { get; set; }


        public string DutchApplePieQuantity { get; set; }
        public string DutchApplePieQuantityHalf { get; set; }

        public bool DutchApplePieShow { get; set; }
        public bool DutchApplePieShowFull { get; set; }
        public bool DutchApplePieShowHalf { get; set; }


        public string DutchApplePiePrice { get; set; }
        public string DutchApplePiePriceHalf { get; set; }


        public string BlackberryPieQuantity { get; set; }
        public string BlackberryPieQuantityHalf { get; set; }

        public bool BlackberryPieShow { get; set; }
        public bool BlackberryPieShowFull { get; set; }
        public bool BlackberryPieShowHalf { get; set; }


        public string BlackberryPiePrice { get; set; }
        public string BlackberryPiePriceHalf { get; set; }


        public string TraditionalBlueberryPieQuantity { get; set; }
        public string TraditionalBlueberryPieQuantityHalf { get; set; }

        public bool TraditionalBlueberryPieShow { get; set; }
        public bool TraditionalBlueberryPieShowFull { get; set; }
        public bool TraditionalBlueberryPieShowHalf { get; set; }


        public string TraditionalBlueberryPiePrice { get; set; }
        public string TraditionalBlueberryPiePriceHalf { get; set; }



        public string TraditionalCherryPieQuantity { get; set; }
        public string TraditionalCherryPieQuantityHalf { get; set; }

        public bool TraditionalCherryPieShow { get; set; }
        public bool TraditionalCherryPieShowFull { get; set; }
        public bool TraditionalCherryPieShowHalf { get; set; }


        public string TraditionalCherryPiePrice { get; set; }
        public string TraditionalCherryPiePriceHalf { get; set; }


        public string BlackCherryPieQuantity { get; set; }
        public string BlackCherryPieQuantityHalf { get; set; }

        public bool BlackCherryPieShow { get; set; }
        public bool BlackCherryPieShowFull { get; set; }
        public bool BlackCherryPieShowHalf { get; set; }


        public string BlackCherryPiePrice { get; set; }
        public string BlackCherryPiePriceHalf { get; set; }


        public string CherryCrumbPieQuantity { get; set; }
        public string CherryCrumbPieQuantityHalf { get; set; }

        public bool CherryCrumbPieShow { get; set; }
        public bool CherryCrumbPieShowFull { get; set; }
        public bool CherryCrumbPieShowHalf { get; set; }


        public string CherryCrumbPiePrice { get; set; }
        public string CherryCrumbPiePriceHalf { get; set; }


        public string CherryVanillawithAlmondsPieQuantity { get; set; }
        public string CherryVanillawithAlmondsPieQuantityHalf { get; set; }

        public bool CherryVanillawithAlmondsPieShow { get; set; }
        public bool CherryVanillawithAlmondsPieShowFull { get; set; }
        public bool CherryVanillawithAlmondsPieShowHalf { get; set; }


        public string CherryVanillawithAlmondsPiePrice { get; set; }
        public string CherryVanillawithAlmondsPiePriceHalf { get; set; }


        public string TraditionalPeachPieQuantity { get; set; }
        public string TraditionalPeachPieQuantityHalf { get; set; }

        public bool TraditionalPeachPieShow { get; set; }
        public bool TraditionalPeachPieShowFull { get; set; }
        public bool TraditionalPeachPieShowHalf { get; set; }


        public string TraditionalPeachPiePrice { get; set; }
        public string TraditionalPeachPiePriceHalf { get; set; }


        public string MixedPeachandBlueberryPieQuantity { get; set; }
        public string MixedPeachandBlueberryPieQuantityHalf { get; set; }

        public bool MixedPeachandBlueberryPieShow { get; set; }
        public bool MixedPeachandBlueberryPieShowFull { get; set; }
        public bool MixedPeachandBlueberryPieShowHalf { get; set; }


        public string MixedPeachandBlueberryPiePrice { get; set; }
        public string MixedPeachandBlueberryPiePriceHalf { get; set; }


        public string PeachPralinePieQuantity { get; set; }
        public string PeachPralinePieQuantityHalf { get; set; }

        public bool PeachPralinePieShow { get; set; }
        public bool PeachPralinePieShowFull { get; set; }
        public bool PeachPralinePieShowHalf { get; set; }


        public string PeachPralinePiePrice { get; set; }
        public string PeachPralinePiePriceHalf { get; set; }


        public string PineappleUpsideDownPieQuantity { get; set; }
        public string PineappleUpsideDownPieQuantityHalf { get; set; }

        public bool PineappleUpsideDownPieShow { get; set; }
        public bool PineappleUpsideDownPieShowFull { get; set; }
        public bool PineappleUpsideDownPieShowHalf { get; set; }


        public string PineappleUpsideDownPiePrice { get; set; }
        public string PineappleUpsideDownPiePriceHalf { get; set; }


        public string StrawberryPieQuantity { get; set; }
        public string StrawberryPieQuantityHalf { get; set; }

        public bool StrawberryPieShow { get; set; }
        public bool StrawberryPieShowFull { get; set; }
        public bool StrawberryPieShowHalf { get; set; }


        public string StrawberryPiePrice { get; set; }
        public string StrawberryPiePriceHalf { get; set; }


        public string StrawberryRhubarbPieQuantity { get; set; }
        public string StrawberryRhubarbPieQuantityHalf { get; set; }

        public bool StrawberryRhubarbPieShow { get; set; }
        public bool StrawberryRhubarbPieShowFull { get; set; }
        public bool StrawberryRhubarbPieShowHalf { get; set; }


        public string StrawberryRhubarbPiePrice { get; set; }
        public string StrawberryRhubarbPiePriceHalf { get; set; }


        public string VeryBerryPieQuantity { get; set; }
        public string VeryBerryPieQuantityHalf { get; set; }

        public bool VeryBerryPieShow { get; set; }
        public bool VeryBerryPieShowFull { get; set; }
        public bool VeryBerryPieShowHalf { get; set; }


        public string VeryBerryPiePrice { get; set; }
        public string VeryBerryPiePriceHalf { get; set; }



        public string SizeTurkey { get; set; }

        public string PieSize { get; set; }

        public string Specialinstructions { get; set; }

        #endregion

    }

}
