USE [Brookhaven nopComm 3.3]
GO

INSERT INTO [dbo].[Product_Category_Mapping]
           ([ProductId]
           ,[CategoryId]
           ,[IsFeaturedProduct]
           ,[DisplayOrder])
     SELECT [ProductId]
      ,[CategoryId]
      ,[IsFeaturedProduct]
      ,[DisplayOrder]
  FROM [Brookhaven nopComm 3.2].[dbo].[Product_Category_Mapping]
GO


