﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Messages;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nop.Plugin.Widget.PromoSlider
{
    public class SocialPostTask : ITask
    {
        private readonly ILogger _logger;
        

        public SocialPostTask(ILogger logger)
        {
            this._logger = logger;
            
        }

        ///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Execute task
		/// </summary>
        public void Execute()
        {
            throw new NotImplementedException();
            
        }



        public int Order
        {
            //ensure that this task is run first 
            get { return 0; }
        }
    }
}
