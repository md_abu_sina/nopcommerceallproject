﻿using Nop.Core.Data;
using Nop.Plugin.Widget.NopSlider.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.NopSlider.Service
{
    public partial class SliderService : ISliderService
    {
        #region Fields
        
        private readonly IRepository<NopSliderRecord> _sliderRepository; 
        
        #endregion

        #region Ctor
        public SliderService(IRepository<NopSliderRecord> sliderRepository)
        {
            this._sliderRepository = sliderRepository;
        } 
        
        #endregion

        #region Methods

        /// <summary>
        /// Gets a blog post
        /// </summary>
        /// <param name="sliderId">Blog post identifier</param>
        /// <returns>Blog post</returns>
        public virtual NopSliderRecord GetSliderById(int sliderId)
        {
            if (sliderId == 0)
                return null;

            return _sliderRepository.GetById(sliderId);
        }

        #endregion
    }
}
