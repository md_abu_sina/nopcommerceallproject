﻿using Nop.Plugin.Widget.NopSlider.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.Widgets.NopSlider.Service
{
    public partial interface ISliderService
    {

        NopSliderRecord GetSliderById(int sliderId);
    }
}
