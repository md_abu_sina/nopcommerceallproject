﻿using Nop.Plugin.Widget.NopSlider.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widget.NopSlider.Data
{
    public class NopSliderMap : EntityTypeConfiguration<NopSliderRecord>
    {
        public NopSliderMap()
        {
            ToTable("NopSlider_NopSliders");

            HasKey(m => m.NopSliderId);

            Property(m => m.NopSliderName);
            Property(m => m.ZoneName);
            Property(m => m.IsNavigationButton);
            Property(m => m.NoOfItems);
            Property(m => m.NavigationButtonPosition);
            Property(m => m.IsBanner);
            Property(m => m.ProgressBarPostition);
            Property(m => m.TransitionStyle);
            Property(m => m.IconChevronActive);
            Property(m => m.IsMouseDragOn);
          

        }
    }
}
