﻿using Nop.Core.Infrastructure.DependencyManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Mvc;
using Nop.Data;
using Autofac.Core;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Core.Data;
using Autofac;


namespace Nop.Plugin.Widgets.NopSlider.Data
{
    public class NopSliderDependencyRegistrar : IDependencyRegistrar
    {
        private const string CONTEXT_NAME = "nop_object_context_Nop_slider";
        public int Order
        {
            get { return 1; }
        }

        public void Register(Autofac.ContainerBuilder builder, Core.Infrastructure.ITypeFinder typeFinder)
        {
            this.RegisterPluginDataContext<NopSliderObjectContext>(builder, CONTEXT_NAME);

            builder.RegisterType<EfRepository<NopSliderRecord>>()
                .As<IRepository<NopSliderRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<NopItemRecord>>()
                .As<IRepository<NopItemRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
                .InstancePerLifetimeScope();

        }
    }
}
