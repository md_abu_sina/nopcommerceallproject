﻿using Nop.Plugin.Widget.NopSlider.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widget.NopSlider.Data
{
    public class NopItemMap : EntityTypeConfiguration<NopItemRecord>
    {
        public NopItemMap()
        {
            ToTable("NopSlider_NopItems");

            HasKey(m => m.NopItemId);

            Property(m => m.NopSliderId);
            Property(m => m.Caption);
            Property(m => m.DisplayOrder);
            Property(m => m.Url);
            Property(m => m.ProductName);
            Property(m => m.IsActive);
            Property(m => m.EntityId);
            Property(m => m.EntityName);
            Property(m => m.ProductId); 
         
         

            //navigation
            this.HasRequired(i => i.NopSlider)
                .WithMany(s => s.Items)
                .HasForeignKey(i => i.NopSliderId);
          
        }
    }
}
