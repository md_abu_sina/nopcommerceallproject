﻿
using System.Collections;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Media;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Plugin.Widgets.NopSlider.Domain;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Plugin.Widgets.NopSlider.Model;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Extensions;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using System.Linq;
using System.Web;
using System;
using System.Web.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Catalog;


namespace Nop.Plugin.Widgets.NopSlider.Controllers
{
    [AdminAuthorize]
    public class NopSliderController : BasePluginController
    {
        private IRepository<NopSliderRecord> _sliderRepo;
        private IRepository<NopItemRecord> _itemRepo;
        private IPictureService _pictureService;
        private ICacheManager _cacheService;
        private readonly IProductService _productService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ICustomerService _customerService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly IProductTagService _productTagService;
        private readonly ICopyProductService _copyProductService;
        private readonly IPdfService _pdfService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IAclService _aclService;
        private readonly IStoreService _storeService;
        private readonly IOrderService _orderService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IShippingService _shippingService;
        private readonly IShipmentService _shipmentService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IMeasureService _measureService;
        private readonly MeasureSettings _measureSettings;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IDiscountService _discountService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IDownloadService _downloadService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IStoreContext _storeContext;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ITaxService _taxService;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly MediaSettings _mediaSettings;
        private readonly CatalogSettings _catalogSettings;

        public NopSliderController(IRepository<NopSliderRecord> sliderRepo,
           IRepository<NopItemRecord> itemRepo, ICacheManager cacheManager, IPictureService pictureService, IPermissionService permissionService,
            IProductService productService,
            IProductTemplateService productTemplateService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            ICustomerService customerService,
            IUrlRecordService urlRecordService,
            IWorkContext workContext,
            ILanguageService languageService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            ISpecificationAttributeService specificationAttributeService,
            ITaxCategoryService taxCategoryService,
            IProductTagService productTagService,
            ICopyProductService copyProductService,
            IPdfService pdfService,
            IExportManager exportManager,
            IImportManager importManager,
            ICustomerActivityService customerActivityService,
            IAclService aclService,
            IStoreService storeService,
            IOrderService orderService,
            IStoreMappingService storeMappingService,
             IVendorService vendorService,
            IShippingService shippingService,
            IShipmentService shipmentService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IMeasureService measureService,
            MeasureSettings measureSettings,
            AdminAreaSettings adminAreaSettings,
            IDateTimeHelper dateTimeHelper,
            IDiscountService discountService,
            IProductAttributeService productAttributeService,
            IBackInStockSubscriptionService backInStockSubscriptionService,
            IShoppingCartService shoppingCartService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            IDownloadService downloadService,
            IStoreContext storeContext,
             IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            ITaxService taxService,
            IWebHelper webHelper,
            MediaSettings mediaSettings,
            CatalogSettings catalogSettings
            )
        {
            this._cacheService = cacheManager;
            _pictureService = pictureService;
            _sliderRepo = sliderRepo;
            _itemRepo = itemRepo;
            this._permissionService = permissionService;
            this._productService = productService;
            this._productTemplateService = productTemplateService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._customerService = customerService;
            this._urlRecordService = urlRecordService;
            this._workContext = workContext;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._specificationAttributeService = specificationAttributeService;
            this._pictureService = pictureService;
            this._taxCategoryService = taxCategoryService;
            this._productTagService = productTagService;
            this._copyProductService = copyProductService;
            this._pdfService = pdfService;
            this._exportManager = exportManager;
            this._importManager = importManager;
            this._customerActivityService = customerActivityService;
            this._permissionService = permissionService;
            this._aclService = aclService;
            this._storeService = storeService;
            this._orderService = orderService;
            this._storeMappingService = storeMappingService;
            this._vendorService = vendorService;
            this._shippingService = shippingService;
            this._shipmentService = shipmentService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._measureService = measureService;
            this._measureSettings = measureSettings;
            this._adminAreaSettings = adminAreaSettings;
            this._dateTimeHelper = dateTimeHelper;
            this._discountService = discountService;
            this._productAttributeService = productAttributeService;
            this._backInStockSubscriptionService = backInStockSubscriptionService;
            this._shoppingCartService = shoppingCartService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._downloadService = downloadService;
            this._priceCalculationService = priceCalculationService;
            this._taxService = taxService;
            this._priceFormatter = priceFormatter;
            this._webHelper = webHelper;
            this._mediaSettings = mediaSettings;
            this._catalogSettings = catalogSettings;
            this._storeContext = storeContext;
            this._cacheManager = cacheManager;
        }

        public ActionResult CreateUpdateNopSliderBanner(int NopSliderId = 0)
        {
            NopSliderRecord Slider = new NopSliderRecord() { Interval = 3 };
            if (NopSliderId > 0)
            {
                Slider = _sliderRepo.GetById(NopSliderId);
            }
            return View(Slider);
        }
        [HttpPost]
        public ActionResult CreateUpdateNopSliderBanner(NopSliderRecord record)
        {
         
            if (ModelState.IsValid)
            {
                NopSliderRecord slider = _sliderRepo.GetById(record.NopSliderId);
                if (slider == null)
                {
                    record.IsBanner = true;
                    _sliderRepo.Insert(record);
                    SuccessNotification("Slider Created Successfullly!");
                    _itemRepo.Table.Where(a => a.NopSliderId == record.NopSliderId);
                    NopItemRecord irecord = new NopItemRecord();
                    return RedirectToRoute(new
                    {
                        Action = "CreateUpdateNopSliderBanner",
                        Controller = "NopSlider",
                        NopSliderID = record.NopSliderId
                    });
                }
                else
                {
                    slider.NopSliderName = record.NopSliderName;
                    slider.Interval = record.Interval;
                    slider.IsActive = record.IsActive;
                    slider.IsBanner = true;
                    slider.ZoneName = record.ZoneName;

                    slider.NavigationButtonPosition = record.NavigationButtonPosition;
                  
                    slider.IsProgressBar = record.IsProgressBar;
                    slider.ProgressBarPostition = record.ProgressBarPostition;
                    slider.TransitionStyle = record.TransitionStyle;
                    slider.IconChevronActive = record.IconChevronActive;
                    slider.IsMouseDragOn = record.IsMouseDragOn;
                    _sliderRepo.Update(slider);
                    _cacheService.Clear();
                    SuccessNotification("Changed Save!");
                    return View(slider);
                }
            }
            else
            {
                return View();
            }
        }

        [ChildActionOnly]
        public ActionResult ManageNopItems(int NopSliderId = 0)
        {

            NopItemRecord item = new NopItemRecord();
            NopSliderRecord slider = _sliderRepo.GetById(NopSliderId);

            item.NopSlider = slider;
            return View(item);
        }
        [HttpPost]
        public ActionResult CreateNopItem(NopItemRecord item)
        {
            var slider = _sliderRepo.GetById(item.NopSliderId);

            item.ItemType = ItemType.Banner;
            item.FilePath = _pictureService.GetPictureUrl(item.NopItemId);

            //else
            //{
            //    item.ItemType = ItemType.Carousel;
            //}
            slider.Items.Add(item);
            _sliderRepo.Update(slider);
            SuccessNotification("Item Added!", false);
            return new NullJsonResult();
        }
   
        public ActionResult CreateUpdateNopSliderCarousel(int NopSliderId = 0)
        {
            NopSliderRecord Slider = new NopSliderRecord() { Interval = 3 };
            if (NopSliderId > 0)
            {
                Slider = _sliderRepo.GetById(NopSliderId);
            }
            return View(Slider);
        }
        [HttpPost]
        public ActionResult CreateUpdateNopSliderCarousel(NopSliderRecord record)
        {
     
            if (ModelState.IsValid)
            {
                NopSliderRecord slider = _sliderRepo.GetById(record.NopSliderId);
                if (slider == null)
                {

                    record.IsBanner = false;
                    _sliderRepo.Insert(record);
                    SuccessNotification("Slider Created Successfullly!");
                    _itemRepo.Table.Where(a => a.NopSliderId == record.NopSliderId);
                    NopItemRecord irecord = new NopItemRecord();
                    return RedirectToRoute(new
                    {
                        Action = "CreateUpdateNopSliderCarousel",
                        Controller = "NopSlider",
                        NopSliderID = record.NopSliderId
                    });
                }
                else
                {
                    slider.NopSliderName = record.NopSliderName;
                    slider.Interval = record.Interval;
                    slider.IsActive = record.IsActive;
                    slider.IsBanner = false;
                    slider.ZoneName = record.ZoneName;
                    _sliderRepo.Update(slider);
                    _cacheService.Clear();
                    SuccessNotification("Changed Save!");
                    return View(slider);
                }
            }
            else
            {
                return View();
            }
        }
        [ChildActionOnly]
        public ActionResult ManageNopItemsCarousel(int NopSliderId = 0)
        {
            NopItemRecord item = new NopItemRecord();
            NopSliderRecord slider = _sliderRepo.GetById(NopSliderId);
            ViewBag.id = NopSliderId;

            item.NopSlider = slider;
            return View(item);
        }
        public ActionResult CreateNopItemCarousel(NopItemRecord item)
        {
            var slider = _sliderRepo.GetById(item.NopSliderId);

            item.ItemType = ItemType.Carousel;
            slider.Items.Add(item);
            _sliderRepo.Update(slider);
            SuccessNotification("Item Added!", false);
            return new NullJsonResult();
        }



        [HttpPost]
        public ActionResult GetNopItemsList(int NopSliderId)
        {
            var sliderItems = _sliderRepo.GetById(NopSliderId).Items.OrderBy(x => x.DisplayOrder);

            var gridModel = new DataSourceResult
            {
                Data = sliderItems.Select(x => new
                {
                    NopItemId = x.NopItemId,
                    Caption = x.Caption,
                    Url = x.Url,
                    FilePath = x.FilePath,
                    DisplayOrder = x.DisplayOrder
                }),
                Total = sliderItems.Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult UpdateNopItem(NopItemRecord itemUpdate)
        {
            NopItemRecord item = _itemRepo.GetById(itemUpdate.NopItemId);

            item.Caption = itemUpdate.Caption;
            item.Url = itemUpdate.Url;
            item.DisplayOrder = itemUpdate.DisplayOrder;
            _itemRepo.Update(item);

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult DeleteNopItem(int NopItemId)
        {
            NopItemRecord item = _itemRepo.GetById(NopItemId);
            _itemRepo.Delete(item);

            return new NullJsonResult();
        }

        public ActionResult ManageNopSliders()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ListNopSliders()
        {
            var sliders = _sliderRepo.Table.ToList();

            var gridModel = new DataSourceResult
            {
                Data = sliders.Select(x => new
                {
                    NopSliderId = x.NopSliderId,
                    NopSliderName = x.NopSliderName,
                    IsActive = x.IsActive,
                    IsBanner = x.IsBanner,
                    IconChevronActive=x.IconChevronActive
                }),
                Total = sliders.Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult UpdateNopSlider(NopSliderRecord sliderUpdate)
        {
            NopSliderRecord slider = _sliderRepo.GetById(sliderUpdate.NopSliderId);
            slider.IsActive = sliderUpdate.IsActive;
            slider.IconChevronActive = sliderUpdate.IconChevronActive;
            _sliderRepo.Update(slider);
            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult DeleteNopSlider(int NopSliderId)
        {

            NopSliderRecord record = _sliderRepo.GetById(NopSliderId);
            _sliderRepo.Delete(record);

            return new NullJsonResult();
        }

        [AllowAnonymous]
        public ActionResult NopSliderWidget(string widgetZone)
        {
            var slider = _sliderRepo.Table.Where(x => x.ZoneName == widgetZone && x.IsActive).ToList();

            //foreach (var item in slider)
            //{
            //    if (item != null)
            //    {
            //        if (item.IsBanner)
            //        {
                        
            //            ViewBag.isCarousel = 0;
            //        }
            //        else if (!item.IsBanner)
            //        {

            //            ViewBag.isCarousel = 1;
            //            ViewBag.id = item.NopSliderId;
            //        }
            //        item.Interval = item.Interval * 1000;
            //        item.Items = item.Items.OrderBy(x => x.DisplayOrder).ToList();
            //        return View(item);
            //    } 
            //}
            return View(slider);

      
        }
        public ActionResult NopSliderHomepageProducts(int? productThumbPictureSize, int? NopSliderId)
        {
            NopSliderRecord record = _sliderRepo.GetById(NopSliderId);
            if (record.IsNavigationButton == true)
            {
                ViewBag.IsNavigationButton = 1;
                if (record.NavigationButtonPosition == "banner_top")
                {
                    ViewBag.NavigationButtonPosition = 1;
                }
                else
                {
                    ViewBag.NavigationButtonPosition = 0;
                }
            }
            else
            {
                ViewBag.IsNavigationButton = 0;
            }
            ViewBag.NoOfItems = record.NoOfItems;
            if (record.IconChevronActive == true)
            {
                ViewBag.IconChevronActive = 1;
            }
            else
            {
                ViewBag.IconChevronActive = 0;
            }
           
            var items = _sliderRepo.GetById(NopSliderId).Items;
            var products = new List<Product>();
            foreach (var i in items)
            {
                if (i.ProductId != 0)
                {
                    var product = _productService.GetProductById(i.ProductId);
                    products.Add(product);
                }
            }
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();

            if (products.Count == 0)
                return Content("");

            var model = PrepareProductOverviewModels(products, true, true, productThumbPictureSize).ToList();
            return PartialView(model);
        } 
        [NonAction]
        protected virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
            bool preparePriceModel = true, bool preparePictureModel = true,
            int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
            bool forceRedirectionAfterAddingToCart = false)
        {
            return this.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService, _specificationAttributeService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, products,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes,
                forceRedirectionAfterAddingToCart);
        }
        public ActionResult Configuration()
        {
            return View();
        }

        //[HttpPost]
        public ActionResult AddProductPopup(string btnId, string formId)
        {

            var allProducts = _productService.SearchProducts();
            if (allProducts == null)
                throw new ArgumentException("No product attribute mapping found with the specified id");
            var model = new NopSliderProductModel();


            //pictures
            foreach (var product in allProducts)
            {
                model = new NopSliderProductModel()
                {
                    PictureUrl = _pictureService.GetPictureUrl(product.ProductPictures.FirstOrDefault().PictureId)
                };
            }
            return View(model);
        }
        [NonAction]
        protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
        {
            var categoriesIds = new List<int>();
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, true);
            foreach (var category in categories)
            {
                categoriesIds.Add(category.Id);
                categoriesIds.AddRange(GetChildCategoryIds(category.Id));
            }
            return categoriesIds;
        }

        public ActionResult List(int? NopSliderId)
        {
            if (NopSliderId == 0)
            {
                ViewBag.error = "Please save your widget before proceeding";
            }
            ViewBag.NopSliderId = NopSliderId;

            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
            //    return AccessDeniedView();

            var model = new ProductListModel();
            model.DisplayProductPictures = _adminAreaSettings.DisplayProductPictures;
            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //stores
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var wh in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = wh.Name, Value = wh.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            return View(model);
        }
        [HttpPost]
        public ActionResult ProductList(DataSourceRequest command, ProductListModel model)
        {
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.SearchVendorId = _workContext.CurrentVendor.Id;

            }
            var categoryIds = new List<int> { model.SearchCategoryId };
            //include subcategories
            if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
                categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));
            var products = _productService.SearchProducts(
                categoryIds: categoryIds,
                manufacturerId: model.SearchManufacturerId,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,

                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                keywords: model.SearchProductName,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true
                );
            var gridModel = new DataSourceResult
            {
                Data = products.Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name,
                    Published = x.Published
                }),
                Total = products.TotalCount
            };
            return Json(gridModel);

        }
        [HttpPost]
        public ActionResult ProductSave(ICollection<int> selectedIds, int NopSliderId)
        {

            NopSliderRecord slider = _sliderRepo.GetById(NopSliderId);


            bool isProduct = true;
            var products = new List<Product>();
            if (selectedIds != null)
            {
                products.AddRange(_productService.GetProductsByIds(selectedIds.ToArray()));
            }
            foreach (var item in products)
            {
                var nopItem = new NopItemRecord();

                nopItem.FilePath = _pictureService.GetPictureUrl(item.ProductPictures.FirstOrDefault().PictureId);
                nopItem.ProductId = item.Id;
                slider.Items.Add(nopItem);
                _sliderRepo.Update(slider);
            }

            return Content("");
        }

    }
}

