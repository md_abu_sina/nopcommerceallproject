﻿using Nop.Core.Data;
using Nop.Core.Plugins;
using Nop.Plugin.Widgets.NopSlider.Data;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Services.Cms;
using Nop.Web.Framework.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using Nop.Services.Localization;

namespace Nop.Plugin.Widgets.NopSlider
{
    public class NopSliderPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        private NopSliderObjectContext _context;
        private IRepository<NopSliderRecord> _sliderRepo;
        public NopSliderPlugin(NopSliderObjectContext context, IRepository<NopSliderRecord> sliderRepo)
        {
            _context = context;
            _sliderRepo = sliderRepo;
        }

        public override void Install()
        {
            _context.Install();
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.CreateitemMessage", "Please create and save slider before creating an item");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NopSliderName", "Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NopSliderName.Hint", "The Name of your plugin");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.interval", "Interval");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.interval.Hint", "Interval");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.IsActive", "IsActive");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.IsActive.Hint", "IsActive");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover", "PauseOnHover");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover.Hint", "PauseOnHover");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.Wrap", "Wrap");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.Wrap.Hint", "Wrap");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.ZoneName", "ZoneName");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.ZoneName.Hint", "ZoneName");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NoOfItems", "NumberofItems");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NoOfItems.Hint", "Number of items per page");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsNavigationButton", "IsNavigationButton");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsNavigationButton.Hint", "IsNavigationButton");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NavigationButtonPosition", "NavigationButtonPosition");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NavigationButtonPosition.Hint", "NavigationButtonPosition");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsProgressBar", "IsProgressBar");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsProgressBar.Hint", "IsProgressBar");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.ProgressBarPostition", "ProgressBarPostition");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.ProgressBarPostition.Hint", "ProgressBarPostition");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.TransitionStyle", "TransitionStyle");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.TransitionStyle.Hint", "TransitionStyle");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IconChevronActive", "IconChevronActive");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IconChevronActive.Hint", "IconChevronActive");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsMouseDragOn", "IsMouseDragOn");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsMouseDragOn.Hint", "IsMouseDragOn");

 

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.SliderNameRequired", "Please Provide a Slider Name");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.SliderIntervalRequired", "Interval must be greater than 0");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.GreaterThanZero", "Interval must between 1 and 10");

            base.Install();
        }

        public override void Uninstall()
        {
            _context.Uninstall();
            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.CreateItemMessage");

            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.NopSliderName");
            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.NopSliderName.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.interval");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.interval.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.IsActive");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.IsActive.Hint");



            this.DeletePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.Wrap");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.Wrap.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.ZoneName");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.ZoneName.Hint");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.NoOfItems");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.NoOfItems.Hint");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsNavigationButton");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsNavigationButton");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsProgressBar");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsProgressBar");


            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.ProgressBarPostition");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.ProgressBarPostition");


            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.TransitionStyle");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.TransitionStyle");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IconChevronActive");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IconChevronActive");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsMouseDragOn");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsMouseDragOn");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.isProduct");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.isProduct"); 

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.SliderNameRequired");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.SliderIntervalRequired");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.GreaterThanZero");

            base.Uninstall();
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out System.Web.Routing.RouteValueDictionary routeValues)
        {
            actionName = "Configuration";
            controllerName = "NopSlider";
            routeValues = new RouteValueDictionary(){
                { "Namespaces", "Nop.Plugin.Widget.Nopslider.Controller"},
                { "area", null}
            };
        }

        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out System.Web.Routing.RouteValueDictionary routeValues)
        {
            actionName = "NopSliderWidget";
            controllerName = "NopSlider";
            routeValues = new RouteValueDictionary(){
                { "Namespaces", "Nop.Plugin.Widget.Nopslider.Controller"},
                { "area", null},
                { "widgetZone",widgetZone}
            };

        }

        public IList<string> GetWidgetZones()
        {
            var sliders = _sliderRepo.Table.Where(x => x.IsActive == true).ToList();
            var zoneNames = new List<string>();
            foreach (var slider in sliders)
            {
                zoneNames.Add(slider.ZoneName);
            }
            return zoneNames;
        }

        public bool Authenticate()
        {
            return true;
        }

        public SiteMapNode BuildMenuItem()
        {
            var parentNode = new SiteMapNode()
            {
                Visible = true,
                Title = "Nop Slider",
                Url = "/NopSlider/CreateUpdateNopSlider"
            };

            var manageSliders = new SiteMapNode()
            {
                Visible = true,
                Title = "Manage Sliders",
                Url = "/NopSlider/ManageNopSliders"
            };

          
            parentNode.ChildNodes.Add(manageSliders);
            return parentNode;
        }
    }

}
