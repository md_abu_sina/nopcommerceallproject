﻿using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Localization;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.NopSlider.Infrastructure
{
    class RouteProvider : IRouteProvider
    {
        public int Priority
        {
            get { return 1; }
        }

        public void RegisterRoutes(System.Web.Routing.RouteCollection routes)
        {
            ViewEngines.Engines.Insert(0, new NopViewEngine());
            //NopSlider
            routes.MapLocalizedRoute("AddProductPopup", "NopSlider/List/{id}",
                            new { controller = "NopSlider", action = "List", NopSliderId = UrlParameter.Optional },
                            new[] { "Nop.Plugin.Widgets.NopSlider.Controllers" });
            //NopSlider Kendo Grid
            routes.MapLocalizedRoute("NopSliderProductList", "NopSlider/ProductList",
                           new { controller = "NopSlider", action = "ProductList" },
                           new[] { "Nop.Plugin.Widgets.NopSlider.Controllers" });
            routes.MapLocalizedRoute("NopSliderHomepageProducts", "NopSlider/HomepageProducts",
                          new { controller = "NopSlider", action = "HomepageProducts", NopSliderId = UrlParameter.Optional, productThumbPictureSize = UrlParameter.Optional },
                          new[] { "Nop.Plugin.Widgets.NopSlider.Controllers" });
        }
            
    }
}
