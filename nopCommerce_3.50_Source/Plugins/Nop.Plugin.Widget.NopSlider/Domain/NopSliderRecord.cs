﻿using FluentValidation.Attributes;
using Nop.Core;
using Nop.Plugin.Widgets.NopSlider.Domain;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widget.NopSlider.Domain
{
    [Validator(typeof(NopSliderValidator))]
   public class NopSliderRecord : BaseEntity
    {
        public NopSliderRecord()
        {
            Items = new List<NopItemRecord>();
        }
        public virtual int NopSliderId { get; set; }
       [NopResourceDisplayName("Plugins.Misc.NopSlider.NopSliderName")]
        public virtual String NopSliderName { get; set; }
       [NopResourceDisplayName("plugins.misc.Nopslider.IsActive")]
        public bool IsActive { get; set; }
       [NopResourceDisplayName("plugins.misc.Nopslider.ZoneName")]
        public virtual String  ZoneName { get; set; }

       [NopResourceDisplayName("plugins.misc.Nopslider.Interval")]
        public virtual int Interval { get; set; }
     

          [NopResourceDisplayName("Plugins.Misc.NopSlider.NoOfItems")]
        public int NoOfItems { get; set; }
       
        

        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsNavigationButton")]
        public bool IsNavigationButton { get; set; }

        [NopResourceDisplayName("Plugins.Misc.NopSlider.NavigationButtonPosition")]
        public string NavigationButtonPosition { get; set; }
      
        public bool IsBanner { get; set; }

         [NopResourceDisplayName("Plugins.Misc.NopSlider.IsProgressBar")]
        public bool IsProgressBar { get; set; }

         [NopResourceDisplayName("Plugins.Misc.NopSlider.ProgressBarPostition")]
        public string ProgressBarPostition { get; set; }

         [NopResourceDisplayName("Plugins.Misc.NopSlider.TransitionStyle")]
        public string TransitionStyle { get; set; }

        [NopResourceDisplayName("Plugins.Misc.NopSlider.IconChevronActive")]
        public bool IconChevronActive { get; set; }

          [NopResourceDisplayName("Plugins.Misc.NopSlider.IsMouseDragOn")]
        public bool IsMouseDragOn { get; set; }

           [NopResourceDisplayName("Plugins.Misc.NopSlider.isProduct")]
        public bool isProduct { get; set; }
      
        public virtual List<NopItemRecord> Items { get; set; }
      


    }
}
