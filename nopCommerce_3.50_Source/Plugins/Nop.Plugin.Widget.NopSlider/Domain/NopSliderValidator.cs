﻿using FluentValidation;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.NopSlider.Domain
{
    class NopSliderValidator : AbstractValidator<NopSliderRecord>
    {
        public NopSliderValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.NopSliderName)
                .NotEmpty().WithMessage(localizationService.GetResource("plugins.misc.Nopslider.SliderNameRequired"));

            RuleFor(x => x.Interval)
                .NotEmpty().WithMessage(localizationService.GetResource("plugins.misc.Nopslider.SliderIntervalRequired"))
                .InclusiveBetween(1, 10).WithMessage(localizationService.GetResource("plugins.misc.Nopslider.GreaterThanZero"));
        }
    }
}
