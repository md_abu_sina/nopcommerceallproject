﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;

namespace Nop.Plugin.Widget.NopSlider.Domain
{
    public class NopItemRecord : BaseEntity
    {
        [UIHint("Picture")]
        public virtual int NopItemId { get; set; }
        public  int EntityId { get; set; }
        public int ProductId { get; set; }
        public string EntityName { get; set; }
        public virtual int NopSliderId { get; set; }
        public virtual String Caption { get; set; }
        public virtual String Url { get; set; }
        public virtual String FilePath { get; set; }
        public virtual int DisplayOrder { get; set; }
        public string ProductName { get; set; }
        public bool IsActive { get; set; }
        public ItemType ItemType { get; set; }
        public NopSliderRecord NopSlider { get; set; }


    }
}
