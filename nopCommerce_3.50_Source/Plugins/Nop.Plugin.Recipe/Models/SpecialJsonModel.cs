﻿
namespace Nop.Plugin.Recipe.Models
{
    public class SpecialJsonModel
    {
        public string name { get; set; }
        public string html { get; set; }
    }
}