﻿using Nop.Plugin.Recipe.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Nop.Plugin.Recipe.Models
{
    public class TipResults
    {
        public TipsModel tips = new TipsModel();
        public TipDetail tipDetail = new TipDetail();
        public TipsModel GetTips(string accessToken, string serviceUrl, ICookBookService _cookbookservice)
        {

            byte[] tipsResponse = _cookbookservice.GetResponseFromUrl(accessToken, serviceUrl);
            MemoryStream tipsStream = new MemoryStream(tipsResponse);

            XDocument xmlPost = new XDocument();
            try
            {
                xmlPost = XDocument.Load(tipsStream);
            }
            catch (Exception e)
            {
                return null;
            }
            XElement xServiceResult = xmlPost.Root;

            tips.Tips = new List<Tip>();
            tips.Pagination = new Pagination();
            foreach (var tip in xServiceResult.Element("tips").Elements("tip"))
            {
                tips.Tips.Add(new Tip { id = tip.Attribute("id") != null ? tip.Attribute("id").Value : string.Empty, title = tip.Element("title") != null ? @tip.Element("title").Value : string.Empty });
            }
            tips.Pagination.current_page = xServiceResult.Element("pagination").Element("current_page") != null ? xServiceResult.Element("pagination").Element("current_page").Value : string.Empty;
            tips.Pagination.next_page = xServiceResult.Element("pagination").Element("next_page") != null ? xServiceResult.Element("pagination").Element("next_page").Value : string.Empty;
            tips.Pagination.total_pages = xServiceResult.Element("pagination").Element("total_pages") != null ? xServiceResult.Element("pagination").Element("total_pages").Value : string.Empty;
            tips.Pagination.per_page = xServiceResult.Element("pagination").Element("per_page") != null ? xServiceResult.Element("pagination").Element("per_page").Value : string.Empty;
            tips.Pagination.start_record = xServiceResult.Element("pagination").Element("start_record") != null ? xServiceResult.Element("pagination").Element("start_record").Value : string.Empty;
            tips.Pagination.end_record = xServiceResult.Element("pagination").Element("end_record") != null ? xServiceResult.Element("pagination").Element("end_record").Value : string.Empty;
            tips.Pagination.total_records = xServiceResult.Element("pagination").Element("total_records") != null ? xServiceResult.Element("pagination").Element("total_records").Value : string.Empty;
            return tips;
        }

        public TipDetail GetTipsDetails(string serviceUrl)
        {
            XDocument xmlPost = new XDocument();
            try
            {
                xmlPost = XDocument.Load(serviceUrl);
            }
            catch (Exception e)
            {
                return null;
            }
            XElement xServiceResult = xmlPost.Root;

            tipDetail.id = xServiceResult.Attribute("id") != null ? xServiceResult.Attribute("id").Value : string.Empty;
            tipDetail.title = xServiceResult.Element("title") != null ? @xServiceResult.Element("title").Value : string.Empty;
            tipDetail.body = xServiceResult.Element("body") != null ? @xServiceResult.Element("body").Value : string.Empty;
            return tipDetail;
        }

        public TipDetail GetTipsDetail(string id)
        {
            try
            {
                TipDetail tip;
                if ((tip = GetTipsDetails(@"http://3nerds.services.procomarketing.com/tips/" + id)) != null)
                {
                    return tip;
                }
                else return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }     
        
    }
}
