﻿using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Core.Data;
using Nop.Plugin.Recipe.Domain;
using System.Collections.Generic;
using Nop.Services.Events;

namespace Nop.Plugin.Recipe.Services
{
    public class SpecialShoppingListService : ISpecialShoppingListService
    {
        private readonly IRepository<SpecialShoppingList> _SpecialShoppingListRepository;

        private readonly IEventPublisher _eventPublisher;

        public SpecialShoppingListService(IRepository<SpecialShoppingList> SpecialShoppingListRepository, IEventPublisher eventPublisher)
         {
             _SpecialShoppingListRepository = SpecialShoppingListRepository;
             
             _eventPublisher = eventPublisher;
        }

        public IList<SpecialShoppingList> GetAllSpecialShoppingListsByCustomerID(int customerId)
        {
            var context = _SpecialShoppingListRepository;
            return context.Table.Where(s => s.CustomerId == customerId).ToList();
        }

        public IList<SpecialShoppingList> GetOldItem()
        {
            var targetDate = DateTime.UtcNow.AddMonths(-1);
            var context = _SpecialShoppingListRepository;
            return context.Table.Where(s => s.TimeStampUtc < targetDate).ToList();
        }

        /// <summary>
        /// Inserts a special shoppinglist item
        /// </summary>
        /// <param name="specialshoppinglistitem">specialshoppinglistitem</param>
        public virtual void InserttospecialShoppingList(SpecialShoppingList specialshoppinglistitem)
        {
            if (specialshoppinglistitem == null)
                throw new ArgumentNullException("specialshoppinglistitem");

            _SpecialShoppingListRepository.Insert(specialshoppinglistitem);

            //event notification
            _eventPublisher.EntityInserted(specialshoppinglistitem);
        }

        #region Get Methods
        public SpecialShoppingList GetSpecialShoppingList(int customerId, int specailId)
        {
            try
            {
                var context = _SpecialShoppingListRepository;
                return context.Table.Where(s => s.CustomerId == customerId && s.SpecialId == specailId).FirstOrDefault();
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public IList<SpecialShoppingList> GetCustomerShoppingListItem(int customerId)
        {
            var context = _SpecialShoppingListRepository;
            return context.Table.Where(s => s.CustomerId == customerId).ToList();
        }

        public int GetSpecialShoppingListId(int customerId, int specailId)
        {
            var context = _SpecialShoppingListRepository;
            var specialShoppingList = context.Table.Where(s => s.CustomerId == customerId && s.SpecialId == specailId).FirstOrDefault();
            return specialShoppingList != null ? specialShoppingList.Id : -1;
        }

        public IList<SpecialShoppingList> GetAllSpecialShoppingLists()
        {
            var db = _SpecialShoppingListRepository;
            return db.Table.OrderBy(s => s.SpecialId).ToList();
        }

        /// <summary>
        /// delete special item from specialshoppinglist
        /// </summary>
        /// <param name="specialshoppinglist">specialshoppinglist</param>
        public void DeletespecialshoppinglistItem(SpecialShoppingList specialshoppinglist)
        {
            _SpecialShoppingListRepository.Delete(specialshoppinglist);

            //event notification
            _eventPublisher.EntityDeleted(specialshoppinglist);

        }


        #endregion Get Methods
    }
}
