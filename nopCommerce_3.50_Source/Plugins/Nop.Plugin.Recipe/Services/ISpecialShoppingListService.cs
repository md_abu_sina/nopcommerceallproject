﻿using Nop.Plugin.Recipe.Domain;
using System.Collections.Generic;
using System;

namespace Nop.Plugin.Recipe.Services
{
    public interface ISpecialShoppingListService
    {
        /// <summary>
        /// get all GetAllSpecialShoppingLists by customer id
        /// </summary>
        /// <param name="customerID">customerID</param>
        IList<SpecialShoppingList> GetAllSpecialShoppingListsByCustomerID(int customerID);
        void InserttospecialShoppingList(SpecialShoppingList specialshoppinglistitem);
        SpecialShoppingList GetSpecialShoppingList(int customerId, int specailId);
        int GetSpecialShoppingListId(int customerId, int specailId);
        IList<SpecialShoppingList> GetCustomerShoppingListItem(int customerId);
        IList<SpecialShoppingList> GetAllSpecialShoppingLists();
        IList<SpecialShoppingList> GetOldItem();
        void DeletespecialshoppinglistItem(SpecialShoppingList specialshoppinglist);
    }
}
