﻿using Nop.Core;

namespace Nop.Plugin.Recipe.Domain
{
    public class RecipeShoppingList : BaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int CustomerID { get; set; }
        public virtual int RecipeID { get; set; }
        public virtual int Servings { get; set; }

    }
}
