using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Configuration;
using Nop.Core.Domain;
using Nop.Core.Domain.Tasks;
using Nop.Core.Infrastructure;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using System.Collections.Specialized;
using Newtonsoft.Json;
using Nop.Services.Configuration;

namespace Nop.Plugin.Recipe
{
    /// <summary>
    /// Represents a task for keeping the site alive
    /// </summary>
    public partial class UpdateTokenTask : ITask
    {
        private readonly ISettingService _settingContext;

        public UpdateTokenTask(ISettingService settingContext)
        {
            _settingContext = settingContext;
        }

        public void Execute()
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection();
                //values["UserName"] = _settingContext.GetSettingByKey<string>("recipeplugin.storeinformationsettings.chainuserid");
                //values["Password"] = _settingContext.GetSettingByKey<string>("recipeplugin.storeinformationsettings.chainpassword");

                values["UserName"] = "mealsforyou";
                values["Password"] = "Sin@01923";

                values["grant_type"] = "password";

                var response = client.UploadValues("http://69.65.19.37:8023/oauth/token", values);

                var responseString = Encoding.Default.GetString(response);
                var results = JsonConvert.DeserializeObject<dynamic>(responseString);

                var test = results.access_token;

                //_settingContext.SetSetting<string>("recipeplugin.storeinformationsettings.chaintoken", results.access_token);

                _settingContext.SetSetting("recipeplugin.storeinformationsettings.chainuserid", values["UserName"]);

                _settingContext.SetSetting("storeinformationsettings.chainpass", values["Password"]);

                _settingContext.SetSetting("storeinformationsettings.chainusername", values["UserName"]);

                _settingContext.SetSetting("recipeplugin.storeinformationsettings.chainpassword", values["Password"]);

                _settingContext.SetSetting("recipeplugin.storeinformationsettings.chaintoken", results.access_token, 0);
            }

            //ILogger logger = EngineContext.Current.Resolve<ILogger>();
            //logger.InsertLog(Core.Domain.Logging.LogLevel.Information, result, result, null);
        }
    }
}