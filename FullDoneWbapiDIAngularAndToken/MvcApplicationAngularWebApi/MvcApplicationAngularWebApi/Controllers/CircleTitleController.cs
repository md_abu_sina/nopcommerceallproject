﻿using DataAccessLayer.DomainModel;
using DataAccessLayer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplicationAngularWebApi.Controllers
{
    public class CircleTitleController : ApiController
    {
        private readonly ICircleTitleService _circleTitleService;
        HttpResponseMessage response = new HttpResponseMessage();

        public CircleTitleController(ICircleTitleService circleTitleService)
        {
            _circleTitleService = circleTitleService;
        }

        [Authorize]
        public List<CircleTitle> Get()
        {
            
            var CircleTitle= new List<CircleTitle>();
            CircleTitle = _circleTitleService.GetCircleTitles();
            return CircleTitle;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        
        public HttpResponseMessage Post(CircleTitle CircleTitle)
        {
            bool iSDataInserted=_circleTitleService.InsertCircleTitle(CircleTitle);
            if (iSDataInserted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                return response;
            }
        }

  
        public HttpResponseMessage Put(CircleTitle CircleTitle)
        {
            bool iSUpdated= _circleTitleService.UpdateCircleTitle(CircleTitle);
            if (iSUpdated)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }
        }

        public HttpResponseMessage Delete(CircleTitle CircleTitle)
        {
            bool iSDeleted = _circleTitleService.DeleteCircleTitle(CircleTitle);
            if (iSDeleted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
    }
}
