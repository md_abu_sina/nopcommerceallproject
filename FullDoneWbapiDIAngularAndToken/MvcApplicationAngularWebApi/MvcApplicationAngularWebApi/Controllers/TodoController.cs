﻿using DataAccessLayer.DomainModel;
using DataAccessLayer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplicationAngularWebApi.Controllers
{
    public class TodoController : ApiController
    {
        private readonly ITodo _todoService;
        HttpResponseMessage response = new HttpResponseMessage();

        public TodoController()
        {
            _todoService = new TodoService();
        }

        public List<Todo> Get()
        {
            var todo = new List<Todo>();
            todo = _todoService.GetTodo();
            return todo;
        }

        public HttpResponseMessage Post(Todo Todo)
        {
            bool iSDataInserted = _todoService.InsertTodo(Todo);
            if (iSDataInserted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                return response;
            }
        }
        public HttpResponseMessage Put(Todo Todo)
        {
            bool iSUpdated = _todoService.UpdateTodo(Todo);
            if (iSUpdated)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }
        }
        public HttpResponseMessage Delete(Todo Todo)
        {
            bool iSDeleted = _todoService.DeleteTodo(Todo);
            if (iSDeleted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
    }
}
