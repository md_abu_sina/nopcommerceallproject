﻿using Microsoft.AspNet.Identity.EntityFramework;
using MvcApplicationAngularWebApi.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplicationAngularWebApi.DBBoject
{
    public class TrustCircleContext : IdentityDbContext<ApplicationUser>
    {
        public TrustCircleContext()
            : base("TrustCircleContext")
        {

        }

        public static TrustCircleContext Create()
        {
            return new TrustCircleContext();
        }
    }
}