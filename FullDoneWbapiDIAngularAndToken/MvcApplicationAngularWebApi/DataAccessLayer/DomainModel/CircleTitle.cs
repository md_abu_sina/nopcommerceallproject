﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DomainModel
{
    [Table("CircleTitle")]
    public class CircleTitle 
    {
        [Key]
        public int Id { get; set; }
        public string CircleName { get; set; }
    }
}
