﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DomainModel
{
    [Table("Message")]
    public class Message
    {
        [Key]
        public Int64 Id { get; set; }

        public Int64 PositionId { get; set; }
        public string FBUReceiverId { get; set; }
        public string FBUSenderId { get; set; }
        public string HeaderText { get; set; }
        public string MessageText { get; set; }
        public string MessageType { get; set; }
        public int CircleId { get; set; }
        public DateTime CreatedOn { get; set; }

        //public virtual FriendCircle FriendCircle { get; set; }
    }
}
