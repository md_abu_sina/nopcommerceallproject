﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public class MessageService :IMessage
    {
        TrustCircleContextDBContext objOfTrustCircleContextDBContext = new TrustCircleContextDBContext();
        public List<Message> GetMessages()
        {
            var message = objOfTrustCircleContextDBContext.Message.ToList();

            return message;
        }

        public bool InsertMessage(Message Message)
        {
            Message.CreatedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.Message.Add(Message);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool UpdateMessage(Message Message)
        {
            var message = objOfTrustCircleContextDBContext.Message.FirstOrDefault(x => x.Id == Message.Id);
            message.Id = Message.Id;
            message.PositionId = Message.PositionId;
            message.FBUReceiverId = Message.FBUReceiverId;
            message.FBUSenderId = Message.FBUSenderId;
            message.HeaderText = Message.HeaderText;
            message.MessageText = Message.MessageText;
            message.MessageType = Message.MessageType;
            message.CircleId = Message.CircleId;
            message.CreatedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool DeleteMessage(Message Message)
        {
            var message = objOfTrustCircleContextDBContext.Message.FirstOrDefault(x => x.Id == Message.Id);
            objOfTrustCircleContextDBContext.Message.Remove(message);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }
    }
}
