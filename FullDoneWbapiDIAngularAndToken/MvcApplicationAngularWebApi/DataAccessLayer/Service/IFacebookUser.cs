﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public interface IFacebookUser
    {
        List<FacebookUser> GetFacebookUsers();

        bool InsertFacebookUser(FacebookUser FacebookUser);
        bool UpdateFacebookUser(FacebookUser FacebookUser);
        bool DeleteFacebookUser(FacebookUser FacebookUser);
    }
}
