﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public class FriendCircleService : IFriendCircle
    {
        TrustCircleContextDBContext objOfTrustCircleContextDBContext = new TrustCircleContextDBContext();

        public List<FriendCircle> GetFriendCircles()
        {
            var friendCircle = objOfTrustCircleContextDBContext.FriendCircle.ToList();

            return friendCircle;
        }

        public bool InsertFriendCircle(FriendCircle friendCircle)
        {
            friendCircle.CreatedOn = DateTime.Now;
            friendCircle.ModifiedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.FriendCircle.Add(friendCircle);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool UpdateFriendCircle(FriendCircle FriendCircle)
        {
            var friendCircle = objOfTrustCircleContextDBContext.FriendCircle.FirstOrDefault(x => x.PositionId == FriendCircle.PositionId);
            friendCircle.PositionId = FriendCircle.PositionId;
            friendCircle.FBUserId = FriendCircle.FBUserId;
            friendCircle.FBFriendId = FriendCircle.FBFriendId;
            friendCircle.XCoordinate = FriendCircle.XCoordinate;
            friendCircle.YCoordinate = FriendCircle.YCoordinate;
            friendCircle.CircleId = FriendCircle.CircleId;
            friendCircle.CreatedOn = DateTime.Now;
            friendCircle.ModifiedOn = DateTime.Now;

            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool DeleteFriendCircle(FriendCircle FriendCircle)
        {
            var friendCircle = objOfTrustCircleContextDBContext.FriendCircle.FirstOrDefault(x => x.PositionId == FriendCircle.PositionId);
            objOfTrustCircleContextDBContext.FriendCircle.Remove(friendCircle);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }
    }
}
