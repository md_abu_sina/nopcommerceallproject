﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public class TodoService : ITodo
    {
        TrustCircleContextDBContext objOfTrustCircleContextDBContext = new TrustCircleContextDBContext();

        public List<Todo> GetTodo()
        {
            var todo = objOfTrustCircleContextDBContext.Todo.ToList();

            return todo;
        }

        public bool InsertTodo(Todo Todo)
        {
            Todo.CreatedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.Todo.Add(Todo);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool UpdateTodo(Todo Todo)
        {
            var todo = objOfTrustCircleContextDBContext.Todo.FirstOrDefault(x => x.ToDoId == Todo.ToDoId);
            todo.ToDoId = Todo.ToDoId;
            todo.MessageId = Todo.MessageId;
            todo.TodoText = Todo.TodoText;
            todo.FBUSenderId = Todo.FBUSenderId;
            todo.FBUReceiverId = Todo.FBUReceiverId;
            todo.IsActive = Todo.IsActive;
            todo.CreatedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool DeleteTodo(Todo Todo)
        {
            var toDo = objOfTrustCircleContextDBContext.Todo.FirstOrDefault(x => x.ToDoId == Todo.ToDoId);
            objOfTrustCircleContextDBContext.Todo.Remove(toDo);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }
    }
}
