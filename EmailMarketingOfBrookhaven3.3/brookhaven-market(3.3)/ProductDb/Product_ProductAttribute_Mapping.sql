USE [Brookhaven nopComm 3.3]
GO

--Alter Table Product_ProductAttribute_Mapping
--Add Id_new Int Identity(1, 1)
--Go

--Alter Table Product_ProductAttribute_Mapping Drop Column ID
--Go

--Exec sp_rename 'Names.Id_new', 'ID', 'Column'

SET IDENTITY_INSERT Product_ProductAttribute_Mapping ON

INSERT INTO [dbo].[Product_ProductAttribute_Mapping]
           ([Id],[ProductId]
           ,[ProductAttributeId]
           ,[TextPrompt]
           ,[IsRequired]
           ,[AttributeControlTypeId]
           ,[DisplayOrder]
           ,[ValidationMinLength]
           ,[ValidationMaxLength]
           ,[ValidationFileAllowedExtensions]
           ,[ValidationFileMaximumSize])
     SELECT [Id],[ProductId]
      ,[ProductAttributeId]
      ,[TextPrompt]
      ,[IsRequired]
      ,[AttributeControlTypeId]
      ,[DisplayOrder]
	  ,NULL
	  ,NULL
	  ,NULL
	  ,NULL
  FROM [Brookhaven nopComm 3.2].[dbo].[Product_ProductAttribute_Mapping]
GO


