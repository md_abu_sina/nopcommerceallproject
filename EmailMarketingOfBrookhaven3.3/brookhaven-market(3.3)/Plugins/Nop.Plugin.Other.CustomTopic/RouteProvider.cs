﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Plugin.Other.CustomTopic.ViewEngines;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Other.CustomTopic
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {

            /*System.Web.Mvc.ViewEngines.Engines.Clear();
            System.Web.Mvc.ViewEngines.Engines.Add(new CustomViewEngine());*/

            routes.MapRoute("Nop.Plugin.Other.CustomTopic.List", "Admin/Plugin/Other/CustomTopic/List",
                        new { controller = "CustomTopic", action = "List" },
                       new[] { "Nop.Plugin.Other.CustomTopic.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Nop.Plugin.Other.CustomTopic.Edit", "Admin/Plugin/Other/CustomTopic/Edit/{id}",
                     new { controller = "CustomTopic", action = "Edit", id = UrlParameter.Optional },
                     new[] { "Nop.Plugin.Other.CustomTopic.Controllers" }).DataTokens.Add("area", "admin");



            routes.MapRoute("Nop.Plugin.Other.CustomTopic.ViewCustomTopic", "t/{SeName}",
                            new { controller = "CustomTopic", action = "TopicDetails", SeName = UrlParameter.Optional },
                            new[] { "Nop.Plugin.Other.CustomTopic.Controllers" });


            routes.MapRoute("Nop.Plugin.Other.CustomTopic.ViewCustomTopicPopup", "t-popup/{SystemName}",
                            new { controller = "CustomTopic", action = "TopicDetailsPopup" },
                            new[] { "Nop.Plugin.Other.CustomTopic.Controllers" });


        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}