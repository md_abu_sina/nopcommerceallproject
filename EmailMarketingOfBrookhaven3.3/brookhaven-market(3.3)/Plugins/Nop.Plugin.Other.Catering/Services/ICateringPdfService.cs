﻿using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using System.IO;
using Nop.Plugin.Other.Catering.Domain;
using Nop.Plugin.Other.Catering.Models;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Other.Catering.Services
{
	public interface ICateringPdfService
    {
			/// <summary>
			/// Print an order to PDF
			/// </summary>
			/// <param name="order">Order</param>
			/// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
			/// <returns>A path of generates file</returns>
		string PrintOrderToPdf(Order order, int languageId, CateringEvent cateringEvent, Dictionary<int, string> secondaryStore = null);

			/// <summary>
			/// Print orders to PDF
			/// </summary>
			/// <param name="stream">Stream</param>
			/// <param name="orders">Orders</param>
			/// <param name="languageId">Language identifier; 0 to use a language used when placing an order</param>
			void PrintOrdersToPdf(Stream stream, IList<Order> orders, CateringEvent cateringEvent, int languageId = 0, Dictionary<int, string> secondaryStore = null);

            void PrintHtmlToPdf(Stream stream, String htmlText);
            void PrintReportToPdf(Stream stream, ReportSummaryModel model, int languageId = 0);

            void PrintSummaryReportToPdf(Stream stream, ReportSummaryModel model, int languageId = 0);

		
    }
}
