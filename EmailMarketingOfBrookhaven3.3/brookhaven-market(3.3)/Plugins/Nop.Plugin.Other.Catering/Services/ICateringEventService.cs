using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Other.Catering.Domain;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Payments;

namespace Nop.Plugin.Other.Catering.Services
{
		public interface ICateringEventService
    {
        
				void InsertCateringEvent(CateringEvent cateringEvent);

				CateringEvent GetCateringEventById(int id);

				CateringEvent GetCateringEventByOrderId(int orderId);

				IPagedList<CateringEvent> GetAllCateringEvent(int pageIndex, int pageSize, DateTime? deliveryDateFrom, DateTime? deliveryDateTo, int storeId = 0, int cateringOrderStatusId = -1);

				CateringEvent GetInProgressCateringEventByCustomerId(int customerId);

				void UpdateCateringEvent(CateringEvent cateringEvent);

				void DeleteCateringEvent(CateringEvent cateringEvent);

				bool IsValidDeliveryTime(DateTime deliveryTime);

				Dictionary<int, string> SecondaryStoreInfo(CateringEvent cateringEvent, Order order, out List<string> emails);

                IPagedList<Order> SearchOrders(int storeId = 0,
                        int vendorId = 0, int customerId = 0,
                        int productId = 0, int affiliateId = 0,
                        DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
                        OrderStatus? os = null, PaymentStatus? ps = null, ShippingStatus? ss = null,
                        string billingEmail = null, string orderGuid = null,
                        int pageIndex = 0, int pageSize = int.MaxValue,
                    DateTime? deliveryStartDate = null, DateTime? deliveryEndDate = null);
                IPagedList<CateringEvent> SearchOrdersForSummaryReport(int storeId = 0,
                        int vendorId = 0, int customerId = 0,
                        int productId = 0, int affiliateId = 0,
                        DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
                        OrderStatus? os = null, PaymentStatus? ps = null, ShippingStatus? ss = null,
                        string billingEmail = null, string orderGuid = null,
                        int pageIndex = 0, int pageSize = int.MaxValue,
                    DateTime? deliveryStartDate = null, DateTime? deliveryEndDate = null);
                List<CateringEvent> OrdersForWeeklySummaryReport(int storeId = 0, DateTime? createdFromUtc = null, DateTime? createdToUtc = null);
    }
}