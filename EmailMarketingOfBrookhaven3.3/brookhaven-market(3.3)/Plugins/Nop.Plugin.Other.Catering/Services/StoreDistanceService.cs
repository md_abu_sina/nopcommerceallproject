using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using Nop.Plugin.Other.Catering.Models;
using Nop.Services.Customers;
using Nop.Services.Common;
using Nop.Core.Domain.Common;

namespace Nop.Plugin.Other.Catering.Services
{
		public class StoreDistanceService : IStoreDistanceService
    {

				private readonly ICustomerService _customerService;
				private readonly IGenericAttributeService _genericAttributeService;
				private readonly IAddressService _addressService;

				public StoreDistanceService(ICustomerService customerService, IGenericAttributeService genericAttributeService, IAddressService addressService)
        {
					this._customerService = customerService;
					this._genericAttributeService = genericAttributeService;
					this._addressService = addressService;
        }

				#region Implementation of StoreDistanceService


				protected double FindDistance(string storeAddress = "San Francisco", string customerAddress = "San Francisco")
				{
					double distance = 0;
					using(var client = new WebClient())
					{
						

						var values = HttpUtility.ParseQueryString(string.Empty);
						values["origins"] = storeAddress;
						values["destinations"] = customerAddress;
						values["mode"] = "bicycling";
						values["language"] = "fr-FR";
						values["sensor"] = "false";
						var uriBuilder = new UriBuilder("http://maps.googleapis.com/maps/api/distancematrix/json");
						uriBuilder.Query = values.ToString();
						var result = client.DownloadData(uriBuilder.ToString());
						var json = Encoding.UTF8.GetString(result);

						var serializer = new JavaScriptSerializer();
						var distanceResponse = serializer.Deserialize<StoreDistanceModel.DistanceResponse>(json);

						if(string.Equals("ok", distanceResponse.Status, StringComparison.OrdinalIgnoreCase))
						{
							Console.WriteLine("origin addresses: {0}", string.Join(", ", distanceResponse.Origin_Addresses));
							Console.WriteLine("destination addresses: {0}", string.Join(", ", distanceResponse.Destination_Addresses));
							foreach(var row in distanceResponse.Rows)
							{
								foreach(var element in row.Elements)
								{
									if(string.Equals("ok", element.Status, StringComparison.OrdinalIgnoreCase))
									{
										distance = element.Distance.Value * 0.000621371; // For converting meter into miles have to multiply by 0.000621371
										Console.WriteLine("Distance: {0} {1}", element.Distance.Text, element.Distance.Value);
										Console.WriteLine("Duration: {0} {1}", element.Duration.Text, element.Duration.Value);
									}
								}
							}
						}
					}

					return distance; 

				}



				public virtual KeyValuePair<int, double> GetNearestStore(string customerAddress = "San Francisco")
				{
					var stores = _customerService.GetCustomerRoleByStoreRole().Where(sr => !sr.AdGroupId.Equals("0000"));

					Dictionary<int, double> dictionary = new Dictionary<int, double>();

					foreach(var store in stores)
					{
						int addessId = Int32.Parse(_genericAttributeService.GetAttributesForEntity(store.Id, "StoreAddress").FirstOrDefault().Value);
						var address = _addressService.GetAddressById(addessId);

						dictionary.Add(store.Id, this.FindDistance(address.Address1, customerAddress));

						//var currentStoreDistance = this.FindDistance(address, customerAddress);
						//storeAddresses.Add(this.FindDistance(address, customerAddress));
					}
					
					//double min = dictionary.Min(x => x.Value);
					var minMatchingKVPs = dictionary.Where(x => x.Value == dictionary.Min(y => y.Value));
					return minMatchingKVPs.FirstOrDefault();
					
					
				}

        #endregion
    }
}