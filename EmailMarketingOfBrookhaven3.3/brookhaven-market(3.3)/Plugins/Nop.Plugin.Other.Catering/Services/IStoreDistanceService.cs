using System;
using System.Collections.Generic;
using Nop.Core.Domain.Common;
using Nop.Plugin.Other.Catering.Domain;

namespace Nop.Plugin.Other.Catering.Services
{
		public interface IStoreDistanceService
    {
			KeyValuePair<int, double> GetNearestStore(string customerAddress = "San Francisco");
    }
}