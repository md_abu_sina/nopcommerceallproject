﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Other.Catering
{
	/// <summary>
	/// Represents an order status enumeration
	/// </summary>
	public enum CateringOrderStatus
	{
		/// <summary>
		/// Not Placed
		/// </summary>
		NotPlaced = 10,
		/// <summary>
		/// Pending
		/// </summary>
		Placed = 20,
		/// <summary>
		/// Processing
		/// </summary>
		InProduction = 30,
		/// <summary>
		/// Complete
		/// </summary>
		Complete = 40,
		/// <summary>
		/// Cancelled
		/// </summary>
		Cancelled = 50
	}

	/// <summary>
	/// Represents an order status enumeration
	/// </summary>
	public enum EmailAddressType
	{
		/// <summary>
		/// Not Placed
		/// </summary>
		CateringDirector = 10,
		/// <summary>
		/// Pending
		/// </summary>
		StoreEmail = 20,
		/// <summary>
		/// Processing
		/// </summary>
		SecondaryStoreEmail = 30,
		
	}
}



