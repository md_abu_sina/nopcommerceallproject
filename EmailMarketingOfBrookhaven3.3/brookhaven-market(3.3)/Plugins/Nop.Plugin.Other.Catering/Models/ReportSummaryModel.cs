using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Nop.Core;
using FluentValidation.Attributes;
using Nop.Plugin.Other.Catering.Validators;
using Nop.Web.Framework.Mvc;
using Nop.Admin.Models.Orders;
using Nop.Core.Domain.Orders;
using System.Collections.Generic;
using Nop.Web.Models.Catalog;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Other.Catering.Models
{
		
        public class ReportSummaryModel : BaseNopModel
        {
            public ReportSummaryModel()
            {
                PagingFilteringContext = new SearchPagingFilteringModel();
                CateringOrderList = new CateringOrderListModel();
                EventOrderItems = new List<EventOrderProductModel>();
                CateringEvents = new List<CateringEventModel>();
                GroupedOrderItems = new List<GroupedOrderModel>();
                GroupedReportItems = new List<GroupedReportModel>();
            }
            
            public virtual int CateringOrderStatusId { get; set; }
            [UIHint("DateNullable")]
            public virtual DateTime? DeliveryTimeEnd { get; set; }
            [UIHint("DateNullable")]
            public virtual DateTime? DeliveryTimeStart { get; set; }
            public virtual int SupplierStoreId { get; set; }
            public SearchPagingFilteringModel PagingFilteringContext { get; set; }

            public virtual CateringOrderListModel CateringOrderList { get; set; }
            


            public List<EventOrderProductModel> EventOrderItems { get; set; }

            public List<CateringEventModel> CateringEvents { get; set; }

            public List<GroupedOrderModel> GroupedOrderItems { get; set; }

            public List<GroupedReportModel> GroupedReportItems { get; set; }
        }

    public class EventOrderProductModel : BaseNopModel
    {
        public virtual String StoreName { get; set; }
        public virtual DateTime? CreateOrderDay { get; set; }
        public virtual int Quantity { get; set; }

        public virtual string Size { get; set; }

        public virtual string Attribute { get; set; }

        public virtual IList<ProductVariantAttributeValue> PvaCollection { get; set; }
        
        public virtual string ProductName { get; set; }
    }

    public class GroupedOrderModel : BaseNopModel
    {
        public GroupedOrderModel()
        {
            EventOrder = new List<EventOrderProductModel>();
            DarienEventOrder = new List<EventOrderProductModel>();
            MokenaEventOrder = new List<EventOrderProductModel>();
            BurridgeEventOrder = new List<EventOrderProductModel>();
        }
        
        public virtual DateTime? OrderDay { get; set; }
        public List<EventOrderProductModel> EventOrder { get; set; }
        public List<EventOrderProductModel> DarienEventOrder { get; set; }
        public List<EventOrderProductModel> MokenaEventOrder { get; set; }
        public List<EventOrderProductModel> BurridgeEventOrder { get; set; }
    }

    public class GroupedReportModel : BaseNopModel
    {
        public GroupedReportModel()
        {
            CateringEvents = new List<CateringEventModel>();
        }

        public virtual DateTime? OrderDay { get; set; }
        public List<CateringEventModel> CateringEvents { get; set; }
        
    }
}
