﻿using System;
using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Common;

namespace Nop.Plugin.Catering.Models
{
    //[Validator(typeof(EventItemValidator))]
		public class StoreAddressModel : BaseNopModel
    {
			public StoreAddressModel()
			{
				AvailableStores = new List<SelectListItem>();
			}

      public IList<SelectListItem> AvailableStores { get; set; }
			public AddressModel Address { get; set; }

    }

}
