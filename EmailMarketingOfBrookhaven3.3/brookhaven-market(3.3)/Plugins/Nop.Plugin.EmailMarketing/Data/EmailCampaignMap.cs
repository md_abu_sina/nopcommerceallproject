﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.EmailMarketing.Domain;

namespace Nop.Plugin.EmailMarketing.Data
{
    public partial class EmailCampaignMap : EntityTypeConfiguration<EmailCampaign>
    {
				public EmailCampaignMap()
        {
						ToTable("Nop_EmailCampaign");

            //Map the primary key
            HasKey(e => e.Id);
						Property(e => e.Name);
            Property(e => e.Subject);
            Property(e => e.Body);
						Property(e => e.CreatedOnUtc);
						Property(e => e.To);
            Property(e => e.Cc);
						Property(e => e.Bcc);
						Property(e => e.EmailAttachment);
						Property(e => e.GroupName);


        }
    }
}
