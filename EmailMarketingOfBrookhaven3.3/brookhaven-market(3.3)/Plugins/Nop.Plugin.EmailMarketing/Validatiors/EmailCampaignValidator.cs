﻿using FluentValidation;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Services.Localization;


namespace Nop.Plugin.EmailMarketing.Validators
{
	public class EmailCampaignValidator : AbstractValidator<EmailCampaignModel>
	{
		public EmailCampaignValidator(ILocalizationService localizationService, IEmailCampaignService emailCampaignService)
		{
			RuleFor(x => x.Name)
					.NotNull()
					.WithMessage(localizationService.GetResource("Admin.EmailMarketing.Campaign.Fields.Name.Required"))
					.Must((x, name) => !emailCampaignService.CampaignNameIsExist(name, x.Id))
					.WithMessage("This Campaign Name allready exist");

			RuleFor(x => x.Subject)
					.NotNull()
					.WithMessage(localizationService.GetResource("Admin.EmailMarketing.Campaign.Fields.Subject.Required"));
		}
	}
}