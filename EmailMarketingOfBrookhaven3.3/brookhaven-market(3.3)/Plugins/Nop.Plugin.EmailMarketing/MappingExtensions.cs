﻿using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Plugin.EmailMarketing.Models;

namespace Nop.Plugin.EmailMarketing
{
    public static class MappingExtensions
    {

				#region Campaign 

				public static EmailCampaignModel ToEmailCampaignModel(this EmailCampaign entity)
				{
					EmailCampaignModel emailCampaignModel = new EmailCampaignModel();

					emailCampaignModel.Id = entity.Id;
					emailCampaignModel.Name = entity.Name;
					emailCampaignModel.Subject = entity.Subject;
					emailCampaignModel.Body = entity.Body;
					emailCampaignModel.CreatedOnUtc = entity.CreatedOnUtc;
					emailCampaignModel.To = entity.To;
					emailCampaignModel.Cc = entity.Cc;
					emailCampaignModel.Bcc = entity.Bcc;
					emailCampaignModel.EmailAttachment = entity.EmailAttachment;
					emailCampaignModel.GroupName = entity.GroupName;
					
					return emailCampaignModel;
				}

				public static EmailCampaign ToEmailCampaignEntity(this EmailCampaignModel model)
				{
					EmailCampaign entity = new EmailCampaign();

					entity.Id = model.Id;
					entity.Name = model.Name;
					entity.Subject = model.Subject;
					entity.Body = model.Body;
					entity.CreatedOnUtc = model.CreatedOnUtc;
					entity.To = model.To;
					entity.Cc = model.Cc;
					entity.Bcc = model.Bcc;
					entity.EmailAttachment = model.EmailAttachment;
					entity.GroupName = model.GroupName;
					 

					return entity;
				}

				public static EmailCampaign ToEmailCampaignEntity(this EmailCampaignModel model, EmailCampaign destination)
				{
					EmailCampaign entity = destination;

					entity.Id = model.Id;
					entity.Name = model.Name;
					entity.Subject = model.Subject;
					entity.Body = model.Body;
					//entity.CreatedOnUtc = model.CreatedOnUtc;
					entity.To = model.To;
					entity.Cc = model.Cc;
					entity.Bcc = model.Bcc;
					entity.EmailAttachment = model.EmailAttachment;
					entity.GroupName = model.GroupName;

					//return Mapper.Map(model, destination);
					return entity;
				}

				#endregion

				#region Schedule

				public static ScheduleModel ToScheduleModel(this Schedule entity)
				{
					ScheduleModel scheduleModel = new ScheduleModel();

					scheduleModel.Id = entity.Id;
					scheduleModel.CampaignId = entity.CampaignId;
					scheduleModel.ScheduledTime = entity.ScheduledTime;
					scheduleModel.Status = entity.Status;
					scheduleModel.Type = entity.Type;

					return scheduleModel;
				}

				public static Schedule ToScheduleEntity(this ScheduleModel model)
				{
					Schedule entity = new Schedule();

					entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.ScheduledTime = model.ScheduledTime;
					entity.Status = model.Status;
					entity.Type = model.Type;
					
					return entity;
				}

				public static Schedule ToScheduleEntity(this ScheduleModel model, Schedule destination)
				{
					Schedule entity = destination;

					//entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.ScheduledTime = model.ScheduledTime;
					entity.Status = model.Status;
					entity.Type = model.Type;

					//return Mapper.Map(model, destination);
					return entity;
				}

				#endregion

        #region Smart Group Criteria
        public static CriteriaModel ToCriteriaModel(this SmartGroups entity)
        {
            CriteriaModel criteriaModel = new CriteriaModel();
            criteriaModel.Id = entity.Id;
            criteriaModel.Name = entity.Name;
            criteriaModel.Columns = entity.Columns;
            criteriaModel.Conditions = entity.Conditions;
            criteriaModel.KeyWord = entity.KeyWord;
            criteriaModel.AndOr = entity.AndOr;
            criteriaModel.Query = entity.Query;
            return criteriaModel;
        }

        public static SmartGroups ToSmartGroupEntity(this CriteriaModel model)
        {
            SmartGroups entity = new SmartGroups();

            entity.Id = model.Id;
            entity.Name = model.Name;
            entity.Columns = model.Columns;
            entity.Conditions = model.Conditions;
            entity.KeyWord = model.KeyWord;
            entity.AndOr = model.AndOr;
            entity.Query = model.Query;

            return entity;
        }

        public static SmartGroups ToSmartGroupEntity(this CriteriaModel model, SmartGroups destination)
        {
            SmartGroups entity = destination;

            entity.Id = model.Id;
            entity.Name = model.Name;
            entity.Columns = model.Columns;
            entity.Conditions = model.Conditions;
            entity.KeyWord = model.KeyWord;
            entity.AndOr = model.AndOr;
            entity.Query = model.Query;

            return entity;
        }

        #endregion

        #region Email Template

        public static EmailTemplateModel ToEmailTemplateModel(this EmailTemplate entity)
        {
            EmailTemplateModel emailTemplateModel = new EmailTemplateModel();

            emailTemplateModel.Id = entity.Id;
            emailTemplateModel.Name = entity.Name;
						emailTemplateModel.PictureId = entity.PictureId;
            emailTemplateModel.TemplatePath = entity.TemplatePath;
            emailTemplateModel.ThumbnailPath = entity.ThumbnailPath;
            emailTemplateModel.TemplateBody = entity.TemplateBody;
          
					return emailTemplateModel;
        }

        public static EmailTemplate ToEmailTemplateEntity(this EmailTemplateModel model)
        {
            EmailTemplate entity = new EmailTemplate();

            entity.Id = model.Id;
            entity.Name = model.Name;
						entity.PictureId = model.PictureId;
            entity.TemplatePath = model.TemplatePath;
            entity.ThumbnailPath = model.ThumbnailPath;
            entity.TemplateBody = model.TemplateBody;

            return entity;
        }

        public static EmailTemplate ToEmailTemplateEntity(this EmailTemplateModel model, EmailTemplate destination)
        {
            EmailTemplate entity = destination;

            entity.Id = model.Id;
            entity.Name = model.Name;
		    entity.PictureId = model.PictureId;
            entity.TemplatePath = model.TemplatePath;
            entity.ThumbnailPath = model.ThumbnailPath;
            entity.TemplateBody = model.TemplateBody;

            return entity;
        } 

        #endregion

				#region Email

				public static EmailModel ToEmailModel(this Email entity)
				{
					EmailModel emailModel = new EmailModel();

					emailModel.Id = entity.Id;
					emailModel.CampaignId = entity.CampaignId;
					emailModel.SourceId = entity.SourceId;
					emailModel.TableName = entity.TableName;
					emailModel.EmailAddress = entity.EmailAddress;
					emailModel.SentStatus = entity.SentStatus;
					emailModel.OpenStatus = entity.OpenStatus;
                    emailModel.UpdatedTime = entity.UpdatedTime;
					

					return emailModel;
				}

				public static Email ToEmailEntity(this EmailModel model)
				{
					Email entity = new Email();

					entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.SourceId = model.SourceId;
					entity.TableName = model.TableName;
					entity.EmailAddress = model.EmailAddress;
					entity.SentStatus = model.SentStatus;
					entity.OpenStatus = model.OpenStatus;

					return entity;
				}

				public static Email ToEmailEntity(this EmailModel model, Email destination)
				{
					Email entity = destination;

					entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.SourceId = model.SourceId;
					entity.TableName = model.TableName;
					entity.EmailAddress = model.EmailAddress;
					entity.SentStatus = model.SentStatus;
					entity.OpenStatus = model.OpenStatus;
					
					return entity;
				}

				public static Email ToEmailEntity(this UpdateReportModel model, Email destination)
				{
					Email entity = destination;

					entity.Id = model.JobId;
					entity.CampaignId = model.EnvId;
					entity.SentStatus = model.DsnStatus;
					entity.UpdatedTime = model.UpdateTime;
					
					return entity;
				}

				#endregion


				#region Campaign Queue

				public static CampaignQueueModel ToCampaignQueueModel(this CampaignQueue entity)
				{
					CampaignQueueModel campaignQueueModel = new CampaignQueueModel();

					campaignQueueModel.Id = entity.Id;
					campaignQueueModel.CampaignId = entity.CampaignId;
					campaignQueueModel.EmailInsertStatus = entity.EmailInsertStatus;
					campaignQueueModel.BroadCastTime = entity.BroadCastTime;


					return campaignQueueModel;
				}

				public static CampaignQueue ToCampaignQueueEntity(this CampaignQueueModel model)
				{
					CampaignQueue entity = new CampaignQueue();

					entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.EmailInsertStatus = model.EmailInsertStatus;
					entity.BroadCastTime = model.BroadCastTime;

					return entity;
				}

				public static CampaignQueue ToCampaignQueueEntity(this CampaignQueueModel model, CampaignQueue destination)
				{
					CampaignQueue entity = destination;

					entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.EmailInsertStatus = model.EmailInsertStatus;
					entity.BroadCastTime = model.BroadCastTime;

					return entity;
				}

				#endregion


				#region Link

				public static LinkModel ToLinkModel(this Link entity)
				{
					LinkModel linkModel = new LinkModel();

					linkModel.Id = entity.Id;
					linkModel.CampaignId = entity.CampaignId;
					linkModel.LinkHref = entity.LinkHref;
					linkModel.Click = entity.Click;

					return linkModel;
				}

				public static Link ToLinkEntity(this LinkModel model)
				{
					Link entity = new Link();

					entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.LinkHref = model.LinkHref;
					entity.Click = model.Click;
					
					return entity;
				}

				public static Link ToLinkEntity(this LinkModel model, Link destination)
				{
					Link entity = destination;

					entity.Id = model.Id;
					entity.CampaignId = model.CampaignId;
					entity.LinkHref = model.LinkHref;
					entity.Click = model.Click;

					return entity;
				}

				#endregion


				#region LinkClick

				public static LinkClickModel ToLinkClickModel(this LinkClick entity)
				{
				  LinkClickModel linkClickModel = new LinkClickModel();

				  //linkClickModel.Id = entity.Id;
					linkClickModel.LinkId = entity.LinkId;
					linkClickModel.EmailId = entity.EmailId;

				  return linkClickModel;
				}

				public static LinkClick ToLinkClickEntity(this LinkClickModel model)
				{
					LinkClick entity = new LinkClick();

					//entity.Id = model.Id;
					entity.LinkId = model.LinkId;
					entity.EmailId = model.EmailId;

					return entity;
				}

				public static LinkClick ToLinkClickEntity(this LinkClickModel model, LinkClick destination)
				{
					LinkClick entity = destination;

					//entity.Id = model.Id;
					entity.LinkId = model.LinkId;
					entity.EmailId = model.EmailId;

					return entity;
				}

				#endregion


				#region LinkClickDetail

				public static LinkClickDetailModel ToLinkClickDetailModel(this Email entity)
				{
					LinkClickDetailModel linkClicDetailkModel = new LinkClickDetailModel();

					//linkClickModel.Id = entity.Id;
					//linkClicDetailkModel.Id = entity.LinkId;
					linkClicDetailkModel.EmailAddress = entity.EmailAddress;

					return linkClicDetailkModel;
				}

				#endregion

				public static CampaignReportModel ToCampaignReportModel(this CampaignQueue entity)
				{
					CampaignReportModel campaignReportModel = new CampaignReportModel();

					campaignReportModel.Id = entity.Id;
					campaignReportModel.CampaignId = entity.CampaignId;
					campaignReportModel.BroadCastTime = entity.BroadCastTime;
					campaignReportModel.Delivered = entity.Delivered;
					campaignReportModel.Opened = entity.Opened;
					campaignReportModel.ClickedThrough = entity.ClickedThrough;


					return campaignReportModel;
				}

		}
}