﻿using Nop.Core.Plugins;
using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using Nop.Core.Data;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Core.Domain.Tasks;
using Nop.Services.Tasks;
using Nop.Web.Framework.Menu;
using System.Web.Routing;
using Nop.Services.Configuration;
using Nop.Services.Messages;
using System.Linq;

namespace Nop.Plugin.EmailMarketing
{
    public class EmailMarketingPlugin : BasePlugin, IAdminMenuPlugin
    {
        private readonly SmartGroupsObjectContext _context;
        private readonly ISettingService _settingContext;
        private readonly EmailTemplateObjectContext _emailTemplateContext;
        private readonly IEmailAccountService _emailAccountService;

        private readonly ISettingService _settingService;

        private readonly EmailObjectContext _emailContext;

        private readonly CampaignQueueObjectContext _campaignQueueContext;

        private readonly EmailCampaignObjectContext _emailCampaignContext;

        private readonly LinkObjectContext _linkContext;

        private readonly LinkClickObjectContext _linkClickContext;

        private readonly ScheduleObjectContext _scheduleContext;

        private readonly IScheduleTaskService _scheduleTaskService;

        public EmailMarketingPlugin(SmartGroupsObjectContext context,
            IEmailAccountService emailAccountService,
            ISettingService settingContext,
            EmailTemplateObjectContext emailTemplateContext,
            EmailObjectContext emailContext,
            EmailCampaignObjectContext emailCampaignContext,
            IScheduleTaskService scheduleTaskService,
            ScheduleObjectContext scheduleContext,
            CampaignQueueObjectContext campaignQueueContext,
            LinkObjectContext linkContext,
            LinkClickObjectContext linkClickContext)
        {

            this._context = context;
            this._emailTemplateContext = emailTemplateContext;
            this._emailContext = emailContext;
            this._campaignQueueContext = campaignQueueContext;
            this._linkContext = linkContext;
            this._linkClickContext = linkClickContext;
            this._emailCampaignContext = emailCampaignContext;
            this._scheduleContext = scheduleContext;
            this._scheduleTaskService = scheduleTaskService;
            _settingContext = settingContext;
            _emailAccountService = emailAccountService;
        }

        //public void BuildMenuItem(Telerik.Web.Mvc.UI.MenuItemBuilder menuItemBuilder)
        //{
        //    menuItemBuilder.Text("");
        //    menuItemBuilder.Url("/Admin/Plugin/EmailMarketing/Main");
        //    menuItemBuilder.Route("Admin.Plugin.EmailMarketing.Main");
        //}

        public SiteMapNode BuildMenuItem()
        {
            SiteMapNode node = new SiteMapNode
            {
                Visible = true,
                Title = "Email Marketing",
                Url = "~/Plugin/EmailMarketing/Main",
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };

            return node;
        }

        private ScheduleTask FindScheduledTask()
        {
            return _scheduleTaskService.GetTaskByType("Nop.Plugin.EmailMarketing.SendScheduledMailTask, Nop.Plugin.EmailMarketing");
        }

        private ScheduleTask FindUpdateReportScheduledTask()
        {
            return _scheduleTaskService.GetTaskByType("Nop.Plugin.EmailMarketing.UpdateReportTask, Nop.Plugin.EmailMarketing");
        }

        public override void Install()
        {

            //_settingContext.SetSetting("sendgridusername", "proco-test");
            //_settingContext.SetSetting("sendgridpassword", "pr0c0P@ss");



            EmailAccount emailAccount = new EmailAccount();
            emailAccount.Email = "services@procomarketing.com";
            emailAccount.DisplayName = "SendGrid Email Provider For Brookhaven";
            emailAccount.Username = "proco-test";
            emailAccount.Password = "pr0c0P@ss";
            emailAccount.Host = "SendGridApi";
            _emailAccountService.InsertEmailAccount(emailAccount);

            this.AddOrUpdatePluginLocaleResource("admin.emailmarketing.landpage", "Email Marketing");

            //Campaign
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Name", "Name");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Subject", "Subject");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Body", "Body");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.CreatedOnUtc", "CreatedOn");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.To", "To");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Cc", "Cc");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Bcc", "Bcc");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Schedule", "Schedule");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.GroupName", "Group Name");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.AllowedTokens", "Allowed Tokens");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Title", "Email Campaign");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Header", "Email Campaign");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Title.AddNew", "Create Campaign");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.BackToList", "Back to Campaign List");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Title.Edit", "Edit Campaign");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Campaigns.Button.SendCampaign", "Start Campaign");

            //Template
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Name", "Template Name");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Picture", "Add Thumb");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Fields.ZipFileUpload", "Import Template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Thumb", "Add Thumb");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Fields.MessageToken", "Message Token");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Body", "Template Body");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Title", "Email Template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Header", "Email Template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Header.Hint", "Upload a pre formated zip file to import template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Title.AddNew", "Add Template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Header.AddNew", "Create Template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.BackToList", "Back to Template List");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Title.EditTemplate", "Edit Template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Header.EditTemplate", "Edit Template");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Template.Button.Use", "Use");

            //Groups
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.Title", "Groups");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.Header", "Groups");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.Title.AddNew", "Create Group");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.Header.AddNew", "Create Group");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.BackToList", "Back to Group List");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.Title.EditGroup", "Edit Group");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.Header.EditGroup", "Edit Group");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.Fields.Name", "Name");

            //Report
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Report.Title", "Report");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Report.Header", "Report");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.DetailReport.Title", "Details Report");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.DetailReport.Header", "Details Report");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Report.BackToList", "Back to Report");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.DetailReport.Links.Title", "Links");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.DetailReport.LinkClicks.Title", "Link Clicks");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.SmartContacts.Title", "SmartContacts");
            this.AddOrUpdatePluginLocaleResource("Admin.EmailMarketing.Groups.SmartContacts.Header", "Smart Contacts");

            _context.InstallSchema();
            _emailTemplateContext.InstallSchema();

            _emailContext.InstallSchema();
            _campaignQueueContext.InstallSchema();

            _linkClickContext.InstallSchema();
            //_linkContext.InstallSchema();

            _emailCampaignContext.InstallSchema();
            //_scheduleContext.InstallSchema();

            //install a schedule task
            var task = FindScheduledTask();
            if (task == null)
            {
                task = new ScheduleTask
                {
                    Name = "Email Marketing Send Scheduled Email",
                    Seconds = 180,
                    Type = "Nop.Plugin.EmailMarketing.SendScheduledMailTask, Nop.Plugin.EmailMarketing",
                    Enabled = true,
                    StopOnError = false,
                };
                _scheduleTaskService.InsertTask(task);
            }

            var updateReportTask = FindUpdateReportScheduledTask();
            if (updateReportTask == null)
            {
                updateReportTask = new ScheduleTask
                {
                    Name = "Email Marketing Update Report",
                    Seconds = 180,
                    Type = "Nop.Plugin.EmailMarketing.UpdateReportTask, Nop.Plugin.EmailMarketing",
                    Enabled = true,
                    StopOnError = false,
                };
                _scheduleTaskService.InsertTask(updateReportTask);
            }

            base.Install();

        }





        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //database objects
            //_objectContext.Uninstall();

            this.DeletePluginLocaleResource("Admin.EmailMarketing.landpage");

            //Campaign
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Name");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Subject");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Body");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.CreatedOnUtc");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.To");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Cc");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Bcc");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.Schedule");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.GroupName");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Fields.AllowedTokens");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Header");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Title.AddNew");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.BackToList");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Title.Edit");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Campaigns.Button.SendCampaign");


            //Template
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Name");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Picture");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Fields.ZipFileUpload");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Thumb");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Fields.MessageToken");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Fields.Body");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Header");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Header.Hint");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Title.AddNew");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Header.AddNew");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.BackToList");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Title.EditTemplate");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Header.EditTemplate");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Template.Button.Use");


            //Groups
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.Header");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.Title.AddNew");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.Header.AddNew");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.BackToList");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.Title.EditGroup");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.Header.EditGroup");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.Fields.Name");

            //Report
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Report.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Report.Header");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.DetailReport.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.DetailReport.Header");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Report.BackToList");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.DetailReport.Links.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.DetailReport.LinkClicks.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.SmartContacts.Title");
            this.DeletePluginLocaleResource("Admin.EmailMarketing.Groups.SmartContacts.Header");

            var task = FindScheduledTask();
            var updateReportTask = FindUpdateReportScheduledTask();
            if (task != null)
                _scheduleTaskService.DeleteTask(task);
            if (updateReportTask != null)
                _scheduleTaskService.DeleteTask(updateReportTask);


            _context.Uninstall();
            base.Uninstall();


            var allEmailAcc = _emailAccountService.GetAllEmailAccounts();
            for (int i = 0; i < allEmailAcc.Count; i++)
            {
                if (allEmailAcc[i].Host == "SendGridApi")
                {
                    EmailAccount toDeleteEmail = _emailAccountService.GetEmailAccountById(allEmailAcc[i].Id);
                    _emailAccountService.DeleteEmailAccount(toDeleteEmail);
                }
            }

        }



        public bool Authenticate()
        {
            return true;
        }
    }
}
