﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class CampaignModel : BaseNopEntityModel
  {
		public int Id { get; set; }

		public string To { get; set; }

		public string ToGroup { get; set; }

		public DateTime? Schedule { get; set; }

		public DateTime? CreatedOn { get; set; }

		public string Cc { get; set; }

		public string Bcc { get; set; }

		public string Subject { get; set; }

		public string Body { get; set; }

		public string Name { get; set; }

  }

}
