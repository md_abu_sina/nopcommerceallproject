﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
  public class BounceReportModel : BaseNopEntityModel
  {
      public string status { get; set; }
      public string reason { get; set; }
      public string email { get; set; }
		
  }

}
