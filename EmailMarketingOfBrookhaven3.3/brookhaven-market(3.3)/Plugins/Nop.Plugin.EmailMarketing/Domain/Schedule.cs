﻿using System;
using Nop.Core;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nop.Plugin.EmailMarketing.Domain
{
	public class Schedule : BaseEntity
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the entity identifier
			/// </summary>
			/// <value></value>
      public virtual int Id { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the campaign id.
			/// </summary>
			/// <value>The campaign id.</value>
			public virtual int CampaignId { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the schedule id.
			/// </summary>
			/// <value>The schedule id.</value>
			public virtual string ScheduleId { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the email campaign.
			/// </summary>
			/// <value>The email campaign.</value>
			public virtual EmailCampaign EmailCampaign {	get;	set;	}

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the scheduled time.
			/// </summary>
			/// <value>The scheduled time.</value>
			public virtual DateTime ScheduledTime { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets a value indicating whether this <see cref="Schedule"/> is status.
			/// </summary>
			/// <value><c>true</c> if status; otherwise, <c>false</c>.</value>
			public virtual bool Status { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the type.
			/// </summary>
			/// <value>The type.</value>
			public virtual string Type { get; set; }


    }

}
