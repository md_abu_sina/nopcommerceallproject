﻿using Nop.Core;
using System.Data;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.EmailMarketing;
using Nop.Core.Domain.Customers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nop.Plugin.EmailMarketing.Domain
{
		[Table("Nop_Email")]
    public class Email : BaseEntity
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// 
			/// </summary>
			private ICollection<Link> _links;

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the entity identifier
			/// </summary>
			/// <value></value>
      public virtual int Id { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the campaign id.
			/// </summary>
			/// <value>The campaign id.</value>
      public virtual int CampaignId { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the source id.
			/// </summary>
			/// <value>The source id.</value>
			public virtual int SourceId {	get;	set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the name of the table.
			/// </summary>
			/// <value>The name of the table.</value>
      public virtual string TableName { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the email address.
			/// </summary>
			/// <value>The email address.</value>
      public virtual string EmailAddress { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the sent status.
			/// </summary>
			/// <value>The sent status.</value>
      public virtual string SentStatus { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the open status.
			/// </summary>
			/// <value>The open status.</value>
			public virtual string OpenStatus { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the updated time.
			/// </summary>
			/// <value>The updated time.</value>
			public virtual DateTime? UpdatedTime { get; set; }

			//public ICollection<Link> Links { get; set; }
			//public ICollection<Link> Links;
			//public List<LinkClick> LinkClicked { get; set; }	

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the links.
			/// </summary>
			/// <value>The links.</value>
			public virtual ICollection<Link> Links
			{
				get
				{
					return _links ?? (_links = new List<Link>());
				}
				protected set
				{
					_links = value;
				}
			}


    }

}
