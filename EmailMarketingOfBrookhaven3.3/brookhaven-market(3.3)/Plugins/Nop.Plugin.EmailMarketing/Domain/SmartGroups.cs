﻿using Nop.Core;
using System.Data;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.EmailMarketing;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.EmailMarketing.Domain
{
    public class SmartGroups : BaseEntity
    {

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the Id.
			/// </summary>
			/// <value>
			/// The Id.
			/// </value>
			public virtual int Id { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the Name.
			/// </summary>
			/// <value>
			/// The Name.
			/// </value>
			public virtual string Name { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the key word.
			/// </summary>
			/// <value>The key word.</value>
			public virtual string KeyWord { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the columns.
			/// </summary>
			/// <value>The columns.</value>
			public virtual string Columns { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the conditions.
			/// </summary>
			/// <value>The conditions.</value>
			public virtual string Conditions { get; set; }


			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the and or.
			/// </summary>
			/// <value>The and or.</value>
			public virtual string AndOr { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the Query.
			/// </summary>
			/// <value>
			/// The Query.
			/// </value>
			public virtual string Query { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the is deleted.
			/// </summary>
			/// <value>The is deleted.</value>
			public virtual bool IsDeleted { get; set; }

    }

}
