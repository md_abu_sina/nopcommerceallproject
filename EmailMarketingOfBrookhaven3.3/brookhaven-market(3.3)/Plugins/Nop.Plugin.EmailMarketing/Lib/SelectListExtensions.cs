﻿using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Plugin.EmailMarketing.Domain;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Lib
{
	public static class SelectListExtensions
	{

		#region Smart Group Criteria

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Generate the token select list.
		/// </summary>
		/// <param name="tokens">The tokens.</param>
		/// <returns></returns>
		public static IEnumerable<SelectListItem> ToTokenSelectList(this string[] tokens)
		{
			//var tokenSelectList = new SelectList(tokens);
			List<SelectListItem> tokenSelectList = new List<SelectListItem>();

			foreach(string token in tokens)
			{
				tokenSelectList.Add(new SelectListItem()
				{
					Text = token.Replace("%", ""),
					Value = token
				});
			}

			return tokenSelectList;
		}
		
		
		#endregion



	}
}