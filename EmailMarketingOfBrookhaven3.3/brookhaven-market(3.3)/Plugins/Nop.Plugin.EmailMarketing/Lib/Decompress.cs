﻿using System;
using System.IO;
using System.IO.Compression;
using Ionic.Zip;
using HtmlAgilityPack;

namespace Nop.Plugin.EmailMarketing.Lib
{
    public static class Decompress
    {

        /*public static void ConvertToHtml(string html, string filePath )
        {
            TextWriter tw = new StreamWriter(filePath);

            // write a line of text to the file
            tw.WriteLine(html);

            // close the stream
            tw.Close();
        }*/

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Decompresses the file.
        /// </summary>
        /// <param name="fileInfo">The file info.</param>
        /// <param name="baseUrl">The base URL.</param>
        /// <returns></returns>
        public static string DecompressFile(FileInfo fileInfo, string baseUrl)
        {
            string zipToUnpack = fileInfo.FullName;
            string fileName = fileInfo.Name.Split('.')[0];
            string random = Guid.NewGuid().ToString();
            string unpackDirectory = zipToUnpack.Substring(0, zipToUnpack.Length - 4) + random;
            using (ZipFile zip1 = ZipFile.Read(zipToUnpack))
            {
                // here, we extract every entry, but we could extract conditionally
                // based on entry name, size, date, checkbox status, etc.  
                foreach (ZipEntry e in zip1)
                {
                    e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                }
            }

            //Delete the zip folder after unzip
            System.IO.FileInfo dir = new System.IO.FileInfo(zipToUnpack);
            if (dir.Exists)
                dir.Delete();

            string folderName = fileInfo.Name.Substring(0, fileInfo.Name.Length - 4) + random;
            return ReWriteHtml(unpackDirectory, baseUrl, folderName, fileName);

        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Res the write HTML.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="baseUrl">The base URL.</param>
        /// <param name="folderName">Name of the folder.</param>
        /// <returns></returns>
        private static string ReWriteHtml(string filePath, string baseUrl, string folderName, string fileName)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.Load(filePath + "/" + fileName + "/index.html");

            foreach (HtmlNode image in doc.DocumentNode.SelectNodes("//img[@src]"))
            {
                HtmlAttribute att = image.Attributes["src"];
                att.Value = baseUrl + "Content/Template/" + folderName + "/" + fileName + "/" + att.Value;
            }

            /*var node = HtmlNode.CreateNode(string.Format("<img src='{0}Admin/Plugin/Other/EmailMarketing/EmailOpen' height='1px' width='1px'/>", baseUrl));
            HtmlNode body = doc.DocumentNode.SelectSingleNode("//body");
            body.PrependChild(node);*/

            doc.Save(filePath + "/" + fileName + "/index.html");

            return System.IO.File.ReadAllText(filePath + "/" + fileName + "/index.html");

        }



    }
}