﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Events;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Services.Events;

namespace Nop.Plugin.EmailMarketing.Services
{
    public class ScheduleService : IScheduleService
    {
        #region fields

        private readonly IRepository<Schedule> _scheduleRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ScheduleObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

        public ScheduleService(IRepository<Schedule> scheduleRepository, IEventPublisher eventPublisher,
                                                            ScheduleObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
            this._scheduleRepository = scheduleRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of IScheduleService

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all schedule.
				/// </summary>
				/// <param name="pageIndex">Index of the page.</param>
				/// <param name="pageSize">Size of the page.</param>
				/// <returns></returns>
        public virtual IPagedList<Schedule> GetAllSchedule(int pageIndex, int pageSize)
        {
            var query = (from s in _scheduleRepository.Table
												 orderby s.ScheduledTime
                         select s);
						var schedules = new PagedList<Schedule>(query, pageIndex, pageSize);
						return schedules;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all schedule by time.
				/// </summary>
				/// <param name="time">The time.</param>
				/// <returns></returns>
				public virtual IEnumerable<Schedule> GetAllScheduleByTime(DateTime time)
				{
					return
						from s in _scheduleRepository.Table
						where s.ScheduledTime < time && s.Status.Equals(false)
						select s;
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the schedule.
				/// </summary>
				/// <param name="schedule">The schedule.</param>
				/// <returns></returns>
        public virtual Schedule InsertSchedule(Schedule schedule)
        {
						if(schedule == null)
							throw new ArgumentNullException("schedule");

						_scheduleRepository.Insert(schedule);
						//event notification
						_eventPublisher.EntityInserted(schedule);

						return schedule;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the schedule by id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
        public Schedule GetScheduleById(int id)
        {
            var db = _scheduleRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }



				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the schedule.
				/// </summary>
				/// <param name="schedule">The schedule.</param>
        public void UpdateSchedule(Schedule schedule)
        {
            if (schedule == null)
                throw new ArgumentNullException("schedule");

            _scheduleRepository.Update(schedule);
            //event notification
            _eventPublisher.EntityUpdated(schedule);

        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Deletes the schedule.
				/// </summary>
				/// <param name="schedule">The schedule.</param>
        public void DeleteSchedule(Schedule schedule)
        {
            _scheduleRepository.Delete(schedule);
            _eventPublisher.EntityDeleted(schedule);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the schedule by schedule id.
				/// </summary>
				/// <returns></returns>
				public List<Schedule> GetScheduleByScheduleId(string uniqueScheduleId)
				{
					var schedules = from s in _scheduleRepository.Table
													where s.ScheduleId.Equals(uniqueScheduleId) && !s.Status
													select s;
					return schedules.ToList();
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Saves the type of the schedule on.
				/// </summary>
				/// <param name="type">The type.</param>
				public void SaveScheduleOnType(Schedule schedule, string type)
				{
					schedule.ScheduleId = Guid.NewGuid().ToString();

					DateTime targetDate = schedule.ScheduledTime;

					DateTime lastDate = new DateTime(targetDate.Year, targetDate.Month, 1);
					lastDate = lastDate.AddMonths(1);
					lastDate = lastDate.AddDays(-(lastDate.Day));
			
					switch(type)
					{
						case "Once":
							this.InsertSchedule(schedule);
							break;

						case "Daily":
							while(targetDate.Day <= lastDate.Day && targetDate.Month == lastDate.Month)
							{
								schedule.ScheduledTime = targetDate;
								this.InsertSchedule(schedule);
								targetDate = targetDate.AddDays(1);
							}		
							break;

						case "Weekly":
							while(targetDate.Day <= lastDate.Day && targetDate.Month == lastDate.Month)
							{
								schedule.ScheduledTime = targetDate;
								this.InsertSchedule(schedule);
								targetDate = targetDate.AddDays(7);
							}		
							break;

						default:
							break;
					}
				}


				/// --------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the type of the schedule on.
				/// </summary>
				/// <param name="schedule">The schedule.</param>
				//public void UpdateScheduleOnType(Schedule schedule)
				//{
				//  var schedules = this.GetScheduleByScheduleId(schedule.ScheduleId);
				//  foreach(var scheduleToBeUpdate in schedules)
				//  {
				//    this.UpdateSchedule(scheduleToBeUpdate);
				//  }
				//}
        #endregion
    }
}
