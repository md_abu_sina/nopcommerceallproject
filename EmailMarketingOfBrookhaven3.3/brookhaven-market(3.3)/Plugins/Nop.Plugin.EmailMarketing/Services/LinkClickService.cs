﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Events;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Plugin.EmailMarketing.Domain;
using System.Collections.Generic;
using Nop.Services.Events;

namespace Nop.Plugin.EmailMarketing.Services
{
    public class LinkClickService : ILinkClickService
    {
        #region fields

        private readonly IRepository<LinkClick> _linkClickRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly EmailObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
				private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

				public LinkClickService(IRepository<LinkClick> linkClickRepository, IEventPublisher eventPublisher,
                                EmailObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
						this._linkClickRepository = linkClickRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of ILinkClickService

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all link click.
				/// </summary>
				/// <param name="pageIndex">Index of the page.</param>
				/// <param name="pageSize">Size of the page.</param>
				/// <param name="campaignId">The campaign id.</param>
				/// <returns></returns>
        public virtual IPagedList<LinkClick> GetAllLinkClick(int pageIndex, int pageSize, int campaignId)
        {
            var query = (from l in _linkClickRepository.Table
												// orderby l.LinkId
                         select l);
						var linkClicks = new PagedList<LinkClick>(query, pageIndex, pageSize);
						return linkClicks;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all link click.
				/// </summary>
				/// <param name="campaignId">The campaign id.</param>
				/// <returns></returns>
				public virtual List<LinkClick> GetAllLinkClick(int campaignId)
				{
					var query = (from l in _linkClickRepository.Table
											 // orderby l.LinkId
											 select l);
					var linkClicks = query.ToList();
					return linkClicks;
				}
				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the link.
				/// </summary>
				/// <param name="linkClick">The link click.</param>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
				public virtual LinkClick InsertLinkClick(LinkClick linkClick)
        {
            if (linkClick == null)
                throw new ArgumentNullException("linkClick");
						
						_linkClickRepository.Insert(linkClick);
						//event notification
            _eventPublisher.EntityInserted(linkClick);

						return linkClick;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the link click by id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
				public LinkClick GetLinkClickById(int id)
        {
						var db = _linkClickRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the link click.
				/// </summary>
				/// <param name="linkClick">The link click.</param>
				/// --------------------------------------------------------------------------------------------
				public void UpdateLinkClick(LinkClick linkClick)
        {
						if(linkClick == null)
                throw new ArgumentNullException("linkClick");

            _linkClickRepository.Update(linkClick);
            //event notification
            _eventPublisher.EntityUpdated(linkClick);

        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Deletes the link click.
				/// </summary>
				/// <param name="linkClick">The link click.</param>
				/// --------------------------------------------------------------------------------------------
				public void DeleteLinkClick(LinkClick linkClick)
        {
            _linkClickRepository.Delete(linkClick);
            _eventPublisher.EntityDeleted(linkClick);
        }

        

        #endregion
    }
}
