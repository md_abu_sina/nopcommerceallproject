﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Events;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Plugin.EmailMarketing.Services;
using System.Collections.Generic;
using Nop.Services.Events;

namespace Nop.Plugin.EmailMarketing
{
    public class CampaignService : IEmailCampaignService
    {
        #region fields

				private readonly IRepository<EmailCampaign> _emailCampaignRepository;
				private readonly IEventPublisher _eventPublisher;
				

        #endregion

        #region ctor

				public CampaignService(IRepository<EmailCampaign> emailCampaignRepository, IEventPublisher eventPublisher)
        {
						this._emailCampaignRepository = emailCampaignRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Implementation of ICampaignService

				///--------------------------------------------------------------------------------------------
        /// <summary>
        /// get all unique coupons
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<UniqueCoupon></returns>
        public virtual IPagedList<EmailCampaign> GetAllCampaigns(int pageIndex, int pageSize)
        {
            var query = (from u in _emailCampaignRepository.Table
                         orderby u.Id
                         select u);
						var campaigns = new PagedList<EmailCampaign>(query, pageIndex, pageSize);
						return campaigns;
        }

				public virtual List<EmailCampaign> GetAllCampaignsList()
				{
					var campaigns = (from u in _emailCampaignRepository.Table
													 orderby u.Id
													 select u);
					return campaigns.ToList();
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the email campaign.
				/// </summary>
				/// <param name="campaign">The campaign.</param>
				/// <returns></returns>
				public virtual EmailCampaign InsertEmailCampaign(EmailCampaign campaign)
				{
					if(campaign == null)
						throw new ArgumentNullException("campaign");
					
					_emailCampaignRepository.Insert(campaign);
					//event notification
					_eventPublisher.EntityInserted(campaign);

					return campaign;
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the email campaign by id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				public EmailCampaign GetEmailCampaignById(int id)
				{
					var db = _emailCampaignRepository;
					return db.Table.SingleOrDefault(x => x.Id == id);
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the email campaign.
				/// </summary>
				/// <param name="campaign">The campaign.</param>
				public void UpdateEmailCampaign(EmailCampaign campaign)
				{
					if(campaign == null)
						throw new ArgumentNullException("campaign");

					_emailCampaignRepository.Update(campaign);
					//event notification
					_eventPublisher.EntityUpdated(campaign);

				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Deletes the email campaign.
				/// </summary>
				/// <param name="campaign">The campaign.</param>
				public void DeleteEmailCampaign(EmailCampaign campaign)
				{
					_emailCampaignRepository.Delete(campaign);
					_eventPublisher.EntityDeleted(campaign);
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Campaigns the name is exist.
				/// </summary>
				/// <param name="name">The name.</param>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				public bool CampaignNameIsExist(string name, int id = 0)
				{
					return
						_emailCampaignRepository.Table.Any(sg => sg.Name.Equals(name) && sg.Id != id);
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the list of allowed campaign tokens.
				/// </summary>
				/// <returns></returns>
				public virtual string[] GetListOfAllowedCampaignTokens()
				{
					var allowedTokens = new List<string>()
            {
                "%Store.Name%",
                "%Store.URL%",
                "%Store.Email%",
                "%Customer.Email%", 
                "%Customer.Username%", 
                "%Customer.FullName%", 
								"%Customer.FirstName%",
								"%Customer.LastName%",
                           
            };
					return allowedTokens.ToArray();
				}
			
        #endregion
    }
}
