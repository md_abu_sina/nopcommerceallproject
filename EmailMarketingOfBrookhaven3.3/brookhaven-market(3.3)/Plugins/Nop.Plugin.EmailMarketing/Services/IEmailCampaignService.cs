﻿using Nop.Core;
using Nop.Plugin.EmailMarketing.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.EmailMarketing.Services
{
	public interface IEmailCampaignService
    {
				///--------------------------------------------------------------------------------------------
        /// <summary>
        /// get all unique coupons
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<UniqueCoupon></returns>
        IPagedList<EmailCampaign> GetAllCampaigns(int pageIndex, int pageSize);

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all campaigns list.
				/// </summary>
				/// <returns></returns>
				List<EmailCampaign> GetAllCampaignsList();

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the email campaign by id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				EmailCampaign GetEmailCampaignById(int id);

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the email campaign.
				/// </summary>
				/// <param name="campaign">The campaign.</param>
				/// <returns></returns>
				EmailCampaign InsertEmailCampaign(EmailCampaign campaign);

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the email campaign.
				/// </summary>
				/// <param name="campaign">The campaign.</param>
				void UpdateEmailCampaign(EmailCampaign campaign);

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Deletes the email campaign.
				/// </summary>
				/// <param name="campaign">The campaign.</param>
				void DeleteEmailCampaign(EmailCampaign campaign);

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Campaigns the name is exist.
				/// </summary>
				/// <param name="name">The name.</param>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				bool CampaignNameIsExist(string name, int id = 0);

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the list of allowed campaign tokens.
				/// </summary>
				/// <returns></returns>
				string[] GetListOfAllowedCampaignTokens();

       
    }
}
