﻿using Nop.Core.Plugins;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Services.Messages;
using Nop.Core.Domain;
using Nop.Plugin.EmailMarketing.Lib;
using Nop.Web.Framework.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using Nop.Plugin.EmailMarketing.Models;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Controllers;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Nop.Services.Configuration;

namespace Nop.Plugin.EmailMarketing
{

    public class UpdateReportTask : ITask
    {
        private readonly ILogger _logger;

        private readonly ICampaignQueueService _campaignQueueService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IEmailService _emailService;
        private readonly ISettingService _settingContext;
        private readonly ISettingService _settingService;
        public UpdateReportTask(ILogger logger, ISettingService settingContext, ISettingService settingService, ICampaignQueueService campaignQueueService, IEmailService emailService, IEmailAccountService emailAccountService)
        {
            this._logger = logger;
            this._campaignQueueService = campaignQueueService;
            this._emailService = emailService;
            _settingContext = settingContext;
            _emailAccountService = emailAccountService;
            _settingService = settingService;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Execute task
        /// </summary>
        public void Execute()
        {
            try
            {
                //May need future.
                this.UpdateBouncesReport();
                this.UpdateBlockReport();
                _logger.Error("Update Report Success");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
        }

        private void UpdateBouncesReport()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.sendgrid.com/api/bounces.get.json");

            //string sendgridusername = _settingContext.GetSettingByKey<string>("sendgridusername");
            //string sendgridpassword = _settingContext.GetSettingByKey<string>("sendgridpassword");

            string sendgridusername = "";
            string sendgridpassword = "";

            var allEmailAcc = _emailAccountService.GetAllEmailAccounts();
            for (int i = 0; i < allEmailAcc.Count; i++)
            {
                if (allEmailAcc[i].Host == "SendGridApi")
                {
                    sendgridusername = allEmailAcc[i].Username;
                    sendgridpassword = allEmailAcc[i].Password;
                }
            }


            string urlParameters = "?api_user=" + sendgridusername + "&api_key=" + sendgridpassword + "";
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                var bounceReport = response.Content.ReadAsStringAsync().Result;
                var updatedReportList=JsonConvert.DeserializeObject<List<BounceReportModel>>(bounceReport);

                foreach (var reportToBeUpdate in updatedReportList)
                {
                    var targetEmails = _emailService.GetEmailByEmailAddress(reportToBeUpdate.email);
                    if (targetEmails.Count>0)
                    {
                        for (int i = 0; i < targetEmails.Count; i++)
                        {
                            targetEmails[i].SentStatus = reportToBeUpdate.reason;
                            targetEmails[i].UpdatedTime = DateTime.UtcNow.Date;
                            _emailService.UpdateEmail(targetEmails[i]);
                        }
                    }
                }


            }
        }


        private void UpdateBlockReport()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.sendgrid.com/api/blocks.get.json");

            //string sendgridusername = _settingContext.GetSettingByKey<string>("sendgridusername");
            //string sendgridpassword = _settingContext.GetSettingByKey<string>("sendgridpassword");

            string sendgridusername = "";
            string sendgridpassword = "";

            var allEmailAcc = _emailAccountService.GetAllEmailAccounts();
            for (int i = 0; i < allEmailAcc.Count; i++)
            {
                if (allEmailAcc[i].Host == "SendGridApi")
                {
                    sendgridusername = allEmailAcc[i].Username;
                    sendgridpassword = allEmailAcc[i].Password;
                }
            }


            string urlParameters = "?api_user=" + sendgridusername + "&api_key=" + sendgridpassword + "";
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                var bounceReport = response.Content.ReadAsStringAsync().Result;
                var updatedReportList = JsonConvert.DeserializeObject<List<BounceReportModel>>(bounceReport);

                foreach (var reportToBeUpdate in updatedReportList)
                {
                    var targetEmails = _emailService.GetEmailByEmailAddress(reportToBeUpdate.email);
                    if (targetEmails.Count > 0)
                    {
                        for (int i = 0; i < targetEmails.Count; i++)
                        {
                            targetEmails[i].SentStatus = "block";
                            targetEmails[i].UpdatedTime = DateTime.UtcNow.Date;
                            _emailService.UpdateEmail(targetEmails[i]);
                        }
                    }
                }


            }
        }
    }

}
