﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Services.Messages;

namespace Nop.Plugin.EmailMarketing
{
    public class ScheduleDependencyRegister : IDependencyRegistrar 
    {
        private const string CONTEXT_NAME = "nop_object_context_Schedule";

        #region Implementation of IDependencyRegistrar

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //Load custom data settings
            var dataSettingsManager = new DataSettingsManager();
            DataSettings dataSettings = dataSettingsManager.LoadSettings();

            //Register custom object context
            builder.Register<IDbContext>(c => RegisterIDbContext(c, dataSettings)).Named<IDbContext>(CONTEXT_NAME).InstancePerHttpRequest();
            builder.Register(c => RegisterIDbContext(c, dataSettings)).InstancePerHttpRequest();

            //Register services
            builder.RegisterType<SmartGroupsService>().As<ISmartGroupsService>();
            builder.RegisterType<ScheduleService>().As<IScheduleService>();

            //Override the repository injection
            builder.RegisterType<EfRepository<SmartGroups>>().As<IRepository<SmartGroups>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME)).InstancePerHttpRequest();
            builder.RegisterType<EfRepository<Schedule>>().As<IRepository<Schedule>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME)).InstancePerHttpRequest();

        }

        #endregion

        #region Implementation of IDependencyRegistrar

        public int Order
        {
            get { return 0; }
        }

        #endregion


				/// <summary>
				/// Registers the I db context.
				/// </summary>
				/// <param name="componentContext">The component context.</param>
				/// <param name="dataSettings">The data settings.</param>
				/// <returns></returns>
        private ScheduleObjectContext RegisterIDbContext(IComponentContext componentContext, DataSettings dataSettings)
        {
            string dataConnectionStrings;

            if (dataSettings != null && dataSettings.IsValid())
            {
                dataConnectionStrings = dataSettings.DataConnectionString;
            }
            else
            {
                dataConnectionStrings = componentContext.Resolve<DataSettings>().DataConnectionString;
            }

            return new ScheduleObjectContext(dataConnectionStrings);
        }
    }
}
