﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Upon.Models
{
    public class Coupon
    {
        public int id { get; set; }
        public string title { get; set; }
        public string offer { get; set; }
        public string restrictions { get; set; }
        public string expiration { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string upc { get; set; }
        public string status { get; set; }
    }

    public class Coupons
    {
        public Coupon coupon { get; set; }
    }

    public class RootObject
    {
        public Coupons coupons { get; set; }
    }
}
