﻿using System;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Nop.Plugin.Upon.Validators;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Upon.Models
{
     [Validator(typeof(UniqueCouponValidator))]
    public class UniqueCouponModel : BaseNopEntityModel
    {
        /// <summary>
        /// Gets or sets the UniqueCoupon id.
        /// </summary>
        /// <value>
        /// The UniqueCoupon id.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the StartDate.
        /// </summary>
        /// <value>
        /// The StartDate. when the uPon will be published
        /// </value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate.
        /// </summary>
        /// <value>
        /// The EndDate.  when the uPon will be taken down
        /// </value>
        public DateTime EndDate { get; set; }

                           
        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title.
        /// </value>
        public string Title { get; set; }


        /// <summary>
        /// Gets or sets the Offer.
        /// </summary>
        /// <value>
        /// The Offer.
        /// </value>
        public string Offer { get; set; }

        /// <summary>
        /// Gets or sets the Restrictions.
        /// </summary>
        /// <value>
        /// The Restrictions.
        /// </value>
        public string Restrictions { get; set; }


        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        /// <value>
        /// The Description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Quantity.
        /// </summary>
        /// <value>
        /// The Quantity.
        /// </value>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the DataRange.
        /// </summary>
        /// <value>
        /// The DataRange.
        /// </value>
        public DateTime DateRange { get; set; }


        /// <summary>
        /// Gets or sets the Coupon Image For Website.
        /// </summary>
        /// <value>
        /// The Coupon Image For Website.
        /// </value>
				[UIHint("Picture")]
        public string CouponImageForWebsite { get; set; }


        /// <summary>
        /// Gets or sets the Banner Coupon Image.
        /// </summary>
        /// <value>
        /// The Banner Coupon Image.
        /// </value>
				[UIHint("Picture")]
        public string BannerCouponImage { get; set; }

        /// <summary>
        /// Gets or sets the standard Coupon Image.
        /// </summary>
        /// <value>
        /// The standard Coupon Image.
        /// </value>
        public string StandardCouponImage { get; set; }


        /// <summary>
        /// Gets or sets the Disclaimer.
        /// </summary>
        /// <value>
        /// The Disclaimer.
        /// </value>
        public string Disclaimer { get; set; }


        /// <summary>
        /// Gets or sets the UPC.
        /// </summary>
        /// <value>
        /// The UPC.
        /// </value>
        public string UPC { get; set; }


        /// <summary>
        /// Gets or sets the barcodeImagePath.
        /// </summary>
        /// <value>
        /// The barcodeImagePath.
        /// </value>
        public virtual string barcodeImagePath { get; set; }

        /// <summary>
        /// Gets or sets the Upon is active or not.
        /// </summary>
        /// <value>
        /// true or false.
        /// </value>
        public bool Active { get; set; }

    }

}
