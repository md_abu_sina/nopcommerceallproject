﻿using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Upon.Models
{
    public class ImprintedCouponModel : BaseNopEntityModel
    {
        /// <summary>
        /// Gets or sets the ImprintedCoupon id.
        /// </summary>
        /// <value>
        /// The ImprintedCoupon id.
        /// </value>
        public int Id { get; set; }


        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title.
        /// </value>
        public string Title { get; set; }


        /// <summary>
        /// Gets or sets the Offer.
        /// </summary>
        /// <value>
        /// The Offer.
        /// </value>
        public string Offer { get; set; }

        /// <summary>
        /// Gets or sets the Restrictions.
        /// </summary>
        /// <value>
        /// The Restrictions.
        /// </value>
        public string Restrictions { get; set; }

        /// <summary>
        /// Gets or sets the first name of customer.
        /// </summary>
        /// <value>
        /// Customer's first name.
        /// </value>
        public string First_Name { get; set; }

        /// <summary>
        /// Gets or sets the last name of customer.
        /// </summary>
        /// <value>
        /// Customer's last name.
        /// </value>
        public string Last_Name { get; set; }


        /// <summary>
        /// Gets or sets the Email of customer.
        /// </summary>
        /// <value>
        /// Customer's Email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Coupon id.
        /// </summary>
        /// <value>
        /// The Coupon id.
        /// </value>
        public int CouponId { get; set; }

        /// <summary>
        /// Gets or sets the Unique Id imprinted Coupon.
        /// </summary>
        /// <value>
        /// The Unique Id imprinted Coupon.
        /// </value>
        public Guid UniqueId_imprintedCoupon { get; set; }


        /// <summary>
        /// Gets or sets the CustomerID.
        /// </summary>
        /// <value>
        /// The CustomerID.
        /// </value>
        public int CustomerID { get; set; }


        /// <summary>
        /// Gets or sets the Expiration Date.
        /// </summary>
        /// <value>
        /// The Expiration Date.
        /// </value>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets Status.
        /// </summary>
        /// <value>
        /// Status.
        /// </value>
        public String Status { get; set; }


        /// <summary>
        /// Gets or sets Redemption Date.
        /// </summary>
        /// <value>
        /// Redemption Date.
        /// </value>
        public DateTime RedemptionDate { get; set; }

     
        [DataType(DataType.Currency)]
        public string Amount { get; set; }

        /// <summary>
        /// Gets or sets Amount In Decimal.
        /// </summary>
        /// <value>
        /// Amount in decimal.
        /// </value>
        public double? AmountInDecimal { get; set; }

        /// <summary>
        /// Gets or sets the StartDate.
        /// </summary>
        /// <value>
        /// The StartDate. when the uPon will be published
        /// </value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate.
        /// </summary>
        /// <value>
        /// The EndDate.  when the uPon will be taken down
        /// </value>
        public DateTime EndDate { get; set; }


        public string PdfPath { get; set; }

        public string QrcodeImgPath { get; set; }

        public string UPC { get; set; }

        /// <summary>
        /// Gets or sets Imprinting Date.
        /// </summary>
        /// <value>
        /// Imprinting Date
        public DateTime? ImprintingDate { get; set; }

    }

}
