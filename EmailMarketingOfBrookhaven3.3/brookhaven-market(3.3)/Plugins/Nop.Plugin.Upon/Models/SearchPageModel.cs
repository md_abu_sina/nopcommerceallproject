﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.Upon.Domain;
using Nop.Web.Framework.Mvc;
using FluentValidation.Attributes;
using Nop.Plugin.Upon.Validators;
using System.Web;
using Nop.Web.Models.Customer;

namespace Nop.Plugin.Upon.Models
{
	 
    public class SearchPageModel : BaseNopEntityModel
    {
        
        /// <summary>
        /// Gets or sets the Email of customer.
        /// </summary>
        /// <value>
        /// Customer's Email.
        /// </value>
        public string Email { get; set; }

				/// <summary>
        /// Gets or sets the Coupon id.
        /// </summary>
        /// <value>
        /// The Coupon id.
        /// </value>
        public int CouponId { get; set; }

				/// <summary>
				/// Gets or sets the offer.
				/// </summary>
				/// <value>The offer.</value>
				public string Offer { get; set; }

				/// <summary>
        /// Gets or sets Status.
        /// </summary>
        /// <value>
        /// Status.
        /// </value>
        public String Status { get; set; }

				/// <summary>
        /// Gets or sets Imprinting Date.
        /// </summary>
        /// <value>
        /// Imprinting Date
        public DateTime? ImprintingDateStart { get; set; }

				/// <summary>
        /// Gets or sets Imprinting Date.
        /// </summary>
        /// <value>
        /// Imprinting Date
        public DateTime? ImprintingDateEnd { get; set; }

				/// <summary>
				/// Gets or sets the expiration date start.
				/// </summary>
				/// <value>The expiration date start.</value>
   			public DateTime? ExpirationDateStart { get; set; }

				/// <summary>
				/// Gets or sets the expiration date end.
				/// </summary>
				/// <value>The expiration date end.</value>
				public DateTime? ExpirationDateEnd { get; set; }

				/// <summary>
				/// Gets or sets the redemption date start.
				/// </summary>
				/// <value>The redemption date start.</value>
				public DateTime? RedemptionDateStart { get; set; }

				/// <summary>
				/// Gets or sets the redemption date end.
				/// </summary>
				/// <value>The redemption date end.</value>
				public DateTime? RedemptionDateEnd { get; set; }

				/// <summary>
				/// Gets or sets the amount range from.
				/// </summary>
				/// <value>The amount range from.</value>
				public string AmountRangeFrom { get; set; }

				/// <summary>
				/// Gets or sets the amount range to.
				/// </summary>
				/// <value>The amount range to.</value>
				public string AmountRangeTo { get; set; }

				/// <summary>
				/// Gets or sets the filter key.
				/// </summary>
				/// <value>The filter key.</value>
				public string FilterKey { get; set; }
        
    }

}
