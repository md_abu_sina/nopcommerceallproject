﻿using AutoMapper;


using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
//using Nop.Services.PromotionFeed;
using Nop.Services.Shipping;
using Nop.Services.Tax;

using Nop.Plugin.Upon.Domain;
using Nop.Plugin.Upon.Models;
using Nop.Core;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;
using System;
using Nop.Core.Domain.Localization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Web.Hosting;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Nop.Plugin.Upon.Services;

namespace Nop.Plugin.Upon
{
    public static class generateUponjsonmodule
    {

        #region methodes

        /// <summary>
        /// Export imprinted coupo to xml
        /// </summary>
        /// <param name="imprintedcouponmodel">imprintedcouponmodel</param>
        /// <returns>Result in json string</returns>
        public static string ExportimprintedcouponToXml(ImprintedCouponModel imprintedcouponmodel, IImprintedCouponService imprintedcouponservice)
        {
            string jsondata = "{ coupons:{ coupon:{  id:" + imprintedcouponmodel.Id + ",";
            jsondata += " title:'" + imprintedcouponmodel.Title + "',";
            jsondata += " offer:'" + imprintedcouponmodel.Offer + "',";
            jsondata += " restrictions:'" + imprintedcouponmodel.Restrictions + "',";
            jsondata += " expiration:'" + imprintedcouponmodel.ExpirationDate.ToShortDateString() + "',";
            jsondata += " first_name:'" + imprintedcouponmodel.First_Name + "',";
            jsondata += " last_name:'" + imprintedcouponmodel.Last_Name + "',";
            jsondata += " email:'" + imprintedcouponmodel.Email + "',";
            jsondata += " upc:'" + imprintedcouponmodel.UPC + "',";
            if (imprintedcouponmodel.ExpirationDate < DateTime.Today)
            {
                jsondata += " status:'" + "Expired" + "'}}}";
                imprintedcouponmodel.Status = "Expired";
                ImprintedCoupon imprintedcoupon = imprintedcouponmodel.ToImprintedCouponEntity();
                imprintedcouponservice.UpdateImprintedCoupon(imprintedcoupon);
            }
            else
            {
                jsondata += " status:'" + imprintedcouponmodel.Status + "'}}}";
            }

            return jsondata;
        }


        public static RootObject ExportimprintedcouponToXmlNew(ImprintedCouponModel imprintedcouponmodel, IImprintedCouponService imprintedcouponservice)
        {
            string Status = "";
            //string jsondata = @"{""coupons"":{""coupon"":{""id"":" + imprintedcouponmodel.Id + ",";
            //jsondata += "\"title\": " + "\""+imprintedcouponmodel.Title+"\"" +",";
            //jsondata += "\"offer\": " + "\"" + imprintedcouponmodel.Offer + "\"" + ",";
            //jsondata += "\"restrictions\": " + "\"" + imprintedcouponmodel.Restrictions + "\"" + ",";
            //jsondata += "\"expiration\": " + "\"" + imprintedcouponmodel.ExpirationDate.ToShortDateString() + "\"" + ",";
            //jsondata += "\"first_name\": " + "\"" + imprintedcouponmodel.First_Name + "\"" + ",";
            //jsondata += "\"last_name\": " + "\"" + imprintedcouponmodel.Last_Name + "\"" + ",";
            //jsondata += "\"email\": " + "\"" + imprintedcouponmodel.Email + "\"" + ",";
            //jsondata += "\"upc\": " + "\"" + imprintedcouponmodel.UPC + "\"" + ",";


            if (imprintedcouponmodel.ExpirationDate < DateTime.Today)
            {
                Status = "Expired";
                imprintedcouponmodel.Status = "Expired";
                ImprintedCoupon imprintedcoupon = imprintedcouponmodel.ToImprintedCouponEntity();
                imprintedcouponservice.UpdateImprintedCoupon(imprintedcoupon);
            }
            else
            {
                Status = "" + imprintedcouponmodel.Status + "";
            }



            var jsondata = new RootObject
            {
                coupons = new Coupons
                {
                    coupon = new Coupon
                    {
                        id = imprintedcouponmodel.Id,
                        title = "" + imprintedcouponmodel.Title + "",
                        offer = "" + imprintedcouponmodel.Offer + "",
                        restrictions = "" + imprintedcouponmodel.Restrictions + "",
                        expiration = "" + imprintedcouponmodel.ExpirationDate.ToShortDateString() + "",
                        first_name = "" + imprintedcouponmodel.First_Name + "",
                        last_name = "" + imprintedcouponmodel.Last_Name + "",
                        email = "" + imprintedcouponmodel.Email + "",
                        upc = "" + imprintedcouponmodel.UPC + "",
                        status = "" + Status + ""
                    }
                }
            };

            return jsondata;


        }


        #endregion


    }
}