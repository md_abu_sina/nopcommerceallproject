﻿using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Plugin.Upon.Models;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Upon.Services
{
    public interface IImprintedCouponService
    {

        /// <summary>
        /// get Imprinted Coupon  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ImprintedCoupon GetImprintedcouponById(int id);

        /// <summary>
        /// Update Imprinted Coupon
        /// </summary>
        /// <param name="imprintedcouponItem"></param>
        void UpdateImprintedCoupon(ImprintedCoupon imprintedcouponItem);

        /// <summary>
        /// Assign Unique Coupon
        /// </summary>
        /// <param name="imprintedcouponItem"></param>
        void AssignUniqueCoupon(ImprintedCoupon imprintedcouponItem);

        /// <summary>
        /// Get Unique Coupon By Customer Id
        /// </summary>
        /// <param name="CustomerId"></param>
        ImprintedCoupon GetUniqueCouponByCustomerId(int CustomerId);


        /// <summary>
        /// get Imprinted Coupon By Unique coupon Id
        /// </summary>
        /// <param name="unique_coupon_id"></param>
        ImprintedCoupon getImprintedCouponByUniqueId(string unique_coupon_id);

        /// <summary>
        /// Get All Unique Coupon By Customer Id
        /// </summary>
        /// <param name="CustomerId"></param>
        List<ImprintedCoupon> GetAllUniqueCouponByCustomerId(int CustomerId);

        List<ImprintedCoupon> GetAllImprintedcouponsForExport();

				List<ImprintedCoupon> GetActiveExpiredImprintedcoupons();

				List<ImprintedCoupon> FilterImprintedCouponsForExport(SearchPageModel filter,string status, string filterKey);

        /// <summary>
        /// get All Imprinted Coupon
        /// </summary>
        IPagedList<ImprintedCoupon> GetAllImprintedcoupon(int pageIndex, int pageSize);

        IPagedList<ImprintedCoupon> GetAllImprintedcoupon(string Email, int? CouponId,
           string Offer, string Status,
           DateTime? ImprintingDateFrom, DateTime? ImprintingDateTo, DateTime? ExpirationDateFrom, DateTime? ExpirationDateTo,
           DateTime? RedemptionDateFrom, DateTime? RedemptionDateTo, string AmountFrom, string AmountTo, int pageIndex, int pageSize);

        #region added by razib
        
				///--------------------------Added by Razib-------------------------------------------------------
        /// <summary>
        /// Gets the filter imprintedcoupon.
        /// </summary>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="status">The status.</param>
        /// <param name="filterKey">The filter key.</param>
        /// <returns></returns>
        IPagedList<ImprintedCoupon> GetFilterImprintedcoupon(UserPageModelList model, int pageIndex, int pageSize, string status, string filterKey);


        #endregion
        List<ImprintedCoupon> GetImprintedcouponByIds(int[] ids);

        IPagedList<ImprintedCoupon> GetAllActiveImprintedcoupon(int pageIndex, int pageSize);

        IPagedList<ImprintedCoupon> GetAllActiveImprintedcoupon(string Email, int? CouponId,
          string Offer,
          DateTime? ImprintingDateFrom, DateTime? ImprintingDateTo, DateTime? ExpirationDateFrom, DateTime? ExpirationDateTo,
          DateTime? RedemptionDateFrom, DateTime? RedemptionDateTo, string AmountFrom, string AmountTo, int pageIndex, int pageSize);

        List<ImprintedCoupon> GetAllActiveImprintedcouponsForExport();

        upontotal upontotal(string Status);
    }
}
