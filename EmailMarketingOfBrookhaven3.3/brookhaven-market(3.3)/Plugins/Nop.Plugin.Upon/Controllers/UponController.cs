﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Upon;
using Nop.Plugin.Upon.Domain;
using Nop.Plugin.Upon.Models;
using Nop.Plugin.Upon.Services;
using Nop.Services.Common;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Customer;
using OnBarcode.Barcode;
using Newtonsoft.Json;


namespace Nop.Plugin.Events.Controllers
{
		public class UponController : BasePluginController
    {
        #region Fields

        private readonly IUniqueCouponService _uniquecouponService;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly IWorkContext _workContext;
        private readonly IImprintedCouponService _imprintedcouponService;
        private readonly IPdfService _pdfService;
        private readonly PdfSettings _pdfSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
				private readonly IStoreContext _storeContext;

        #region email fields


        private readonly ITokenizer _tokenizer;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ILanguageService _languageService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IMessageTokenProvider _messageTokenProvider;
        // private readonly IMessageTemplateForUponService _messageTemplateForUponService;
        private readonly IRepository<MessageTemplate> _messageTemplateRepository;
        private readonly IRepository<EmailAccount> _emailAccountRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion
        // private readonly ICouponPdfService _couponpdfService;

        #region customerinfo section

        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly ForumSettings _forumSettings;
        private readonly OrderSettings _orderSettings;
        private readonly IAddressService _addressService;
        private readonly IOrderService _orderService;

        #endregion

        #endregion

        #region ctor

        public UponController(IUniqueCouponService uniquecouponService,
            AdminAreaSettings adminAreaSettings, IWorkContext workContext, IStoreContext storeContext, 
            IImprintedCouponService imprintedcouponService, PdfSettings pdfSettings, IPdfService pdfService, ITokenizer tokenizer,
            IQueuedEmailService queuedEmailService, IMessageTemplateService messageTemplateService, ILanguageService languageService
            , IEmailAccountService emailAccountService, EmailAccountSettings emailAccountSettings, IMessageTokenProvider messageTokenProvider
            , RewardPointsSettings rewardPointsSettings,
            CustomerSettings customerSettings, ForumSettings forumSettings,
            OrderSettings orderSettings, IAddressService addressService, IOrderService orderService,
            IRepository<MessageTemplate> messageTemplateRepository, IRepository<EmailAccount> emailAccountRepository,
           IEventPublisher eventPublisher, StoreInformationSettings storeInformationSettings)
        {
            _uniquecouponService = uniquecouponService;
            _adminAreaSettings = adminAreaSettings;
            _workContext = workContext;
            _imprintedcouponService = imprintedcouponService;
            _pdfService = pdfService;
            _pdfSettings = pdfSettings;
            _tokenizer = tokenizer;
            _queuedEmailService = queuedEmailService;
            _messageTemplateService = messageTemplateService;
            _languageService = languageService;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
            _messageTokenProvider = messageTokenProvider;
            //  _messageTemplateForUponService = messageTemplateForUponService;
            _messageTemplateRepository = messageTemplateRepository;
            _emailAccountRepository = emailAccountRepository;
            _eventPublisher = eventPublisher;

            _rewardPointsSettings = rewardPointsSettings;
            _customerSettings = customerSettings;
            _forumSettings = forumSettings;
            _orderSettings = orderSettings;
            _addressService = addressService;
            _orderService = orderService;

            _storeInformationSettings = storeInformationSettings;
						_storeContext = storeContext;


        }

        #endregion

        #region special coupon offer


        public ActionResult SpecialCouponOffer()
        {
            //Send our model to the view
            var UniqueCouponList = _uniquecouponService.GetAllUniqueCouponsForUser();
            List<UniqueCouponModel> uniquecouponmodellist = new List<UniqueCouponModel>();
            ViewBag.registered = "false";
            ViewBag.nocoupon = "";
            for (int i = 0; i < UniqueCouponList.Count; i++)
            {
							//if(UniqueCouponList[i].EndDate <= DateTime.Today)
							//  {
							//    UniqueCouponList[i].Active = false;
							//  }
                UniqueCouponModel uniquecouponmodel = UniqueCouponList[i].ToModel();
                uniquecouponmodellist.Add(uniquecouponmodel);
            }
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                var coupon = _imprintedcouponService.GetUniqueCouponByCustomerId(_workContext.CurrentCustomer.Id);
                ViewBag.registered = "true";
                if (coupon == null)
                {
                    ViewBag.nocoupon = "";

                }
                else
                {
                    ViewBag.nocoupon = "You have already chosen an U-Pick for this week!";
                }
            }
						DeactivateCoupon();
            // return View("Nop.Plugin.Upon.Views.Upon.SpecialCouponOffer", uniquecouponmodellist);
            return View("SpecialCouponOffer", uniquecouponmodellist);
        }

				[NonAction]
				public void DeactivateCoupon()
				{
					var uniqueCouponList = _uniquecouponService.GetUniqueCouponsForDeactivate();
					foreach(UniqueCoupon uniqueCoupon in uniqueCouponList)
					{
						uniqueCoupon.Active = false;
						_uniquecouponService.UpdateUniqueCoupon(uniqueCoupon);
					}
				}

        //public ActionResult GetAllCouponOftheUser()
        //{
        //    //Send our model to the view

        //    var allCoupons = _imprintedcouponService.GetAllUniqueCouponByCustomerId(_workContext.CurrentCustomer.Id);



        //    if (allCoupons == null)
        //    {
        //        ViewBag.noCoupon = "true";
        //    }

        //    List<ImprintedCouponModel> modelList = new List<ImprintedCouponModel>();
        //    foreach (ImprintedCoupon coupon in allCoupons)
        //    {
        //        ImprintedCouponModel model = coupon.ToImprintedCouponModel();
        //        modelList.Add(model);
        //    }

        //   return View("Nop.Plugin.Upon.Views.Upon.AllCoupons", modelList);
        //    //return View("AllCoupons", modelList);
        //}

        //public ActionResult UniqueCouponDetails(int id)
        //{
        //    var uniquecouponItem = _uniquecouponService.GetuniquecouponById(id);
        //    UniqueCouponModel unicouponmodel = uniquecouponItem.ToModel();
        //    return View("Nop.Plugin.Upon.Views.Upon.UniqueCouponDetails", unicouponmodel);
        //}

        public ActionResult UserPage()
        {
            List<UserPageModel> modelList = new List<UserPageModel>();
            if (_workContext.CurrentCustomer.IsRegistered())
            {

                int CustomerId = _workContext.CurrentCustomer.Id;
                List<ImprintedCoupon> imprintedCouponList = _imprintedcouponService.GetAllUniqueCouponByCustomerId(CustomerId);
                int i = 0;

                foreach (ImprintedCoupon imprintedcoupon in imprintedCouponList)
                {
                    UserPageModel model = MappingExtensions.toUserPageModel(imprintedcoupon);
                    UniqueCoupon uniquecoupon = _uniquecouponService.GetuniquecouponById(imprintedcoupon.CouponId);
                    model.CouponImageForWebsite = uniquecoupon.CouponImageForWebsite;
                    model.Description = uniquecoupon.Description;

										if(model.Status == "Active" && model.ExpirationDate <= DateTime.Now) //model.EndDate < DateTime.Today || 
                    {
                        model.Status = "Expired";
                        imprintedcoupon.Status = "Expired";
                        _imprintedcouponService.UpdateImprintedCoupon(imprintedcoupon);
                    }

                    //E:\nop files\nopCommerce_2.60_Source\Presentation\Nop.Web\content\files\ExportImport\coupon_130_John.pdf
                    if (i == 0)
                    {
                        model.NavigationModel = GetCustomerNavigationModel(_workContext.CurrentCustomer);
                    }
                    i++;
                    modelList.Add(model);
                }

                if (i == 0)
                {
                    UserPageModel model = new UserPageModel();
                    model.Id = -1;
                    model.NavigationModel = GetCustomerNavigationModel(_workContext.CurrentCustomer);
                    modelList.Add(model);
                }


            }
            //return View("Nop.Plugin.Upon.Views.Upon.YourCoupon", modelList);
            return View("YourCoupon", modelList);
        }

        protected CustomerNavigationModel GetCustomerNavigationModel(Customer customer)
        {
            var model = new CustomerNavigationModel();
            model.HideAvatar = !_customerSettings.AllowCustomersToUploadAvatars;
            model.HideRewardPoints = !_rewardPointsSettings.Enabled;
            model.HideForumSubscriptions = !_forumSettings.ForumsEnabled || !_forumSettings.AllowCustomersToManageSubscriptions;
						model.HideReturnRequests = !_orderSettings.ReturnRequestsEnabled || _orderService.SearchReturnRequests(0, customer.Id, 0, null, 0, int.MaxValue).Count == 0;
            model.HideDownloadableProducts = _customerSettings.HideDownloadableProductsTab;
            model.HideBackInStockSubscriptions = _customerSettings.HideBackInStockSubscriptionsTab;
            return model;
        }

        public ActionResult AssignUniqueCouponToThisUser(int id)
        {
            var coupon = _imprintedcouponService.GetUniqueCouponByCustomerId(_workContext.CurrentCustomer.Id);
            if (coupon == null)
            {

                var uniquecouponItem = _uniquecouponService.GetuniquecouponById(id);
                ImprintedCoupon assignedcoupon = new ImprintedCoupon();
                assignedcoupon.UniqueId_imprintedCoupon = Guid.NewGuid();
                assignedcoupon.CouponId = id;
                assignedcoupon.CustomerID = _workContext.CurrentCustomer.Id;
                assignedcoupon.ExpirationDate = uniquecouponItem.DateRange;
                assignedcoupon.Status = "Active";
                assignedcoupon.Title = uniquecouponItem.Title;
                assignedcoupon.Offer = uniquecouponItem.Offer;
                assignedcoupon.Restrictions = uniquecouponItem.Restrictions;
                assignedcoupon.StartDate = uniquecouponItem.StartDate;
                assignedcoupon.EndDate = uniquecouponItem.EndDate;

                assignedcoupon.First_Name = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                assignedcoupon.Last_Name = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
                assignedcoupon.Email = _workContext.CurrentCustomer.Email;
                assignedcoupon.RedemptionDate = null;
                assignedcoupon.UPC = uniquecouponItem.barcodeImagePath;
                assignedcoupon.ImprintingDate = DateTime.Now;

                uniquecouponItem.Quantity = uniquecouponItem.Quantity - 1;

                var data = assignedcoupon.UniqueId_imprintedCoupon;
                var error = 0;
                var url = "";
                QRCode qrcode = new QRCode();
                try
                {
                    url = string.Format("http://chart.apis.google.com/chart?cht=qr&chld={2}|{3}&chs={0}x{0}&chl={1}", 200, data, "low", 0);


                    qrcode.Data = data.ToString();

                    //qrcode.ImageFormat =;

                    //qrcode.drawBarcode("c:\QR Code.tiff");
                }
                catch
                {
                    error = 1;
                }
                if (error == 0)
                {
                    _imprintedcouponService.AssignUniqueCoupon(assignedcoupon);
                    _uniquecouponService.UpdateUniqueCoupon(uniquecouponItem);
                    ViewBag.qrcode = url;
                    //ViewBag.qrcode = qrcode;
                    string imagefilePath = this.Request.PhysicalApplicationPath + "content\\files\\ExportImport\\" + "qrCode" + assignedcoupon.Id + ".jpg";

                    assignedcoupon.QrcodeImgPath = "content/files/ExportImport/" + "qrCode" + assignedcoupon.Id + ".jpg";//"../../content/files/ExportImport/" + "qrCode" + assignedcoupon.Id + ".jpg";//imagefilePath;

                    #region qrcode using onBarcode library
                    var fileNameQRCode = "qrCode" + assignedcoupon.Id + ".jpg";

                    var qrpath = Path.Combine(Server.MapPath("\\content\\files\\ExportImport"), fileNameQRCode);
                    qrcode.X = 6;
                    qrcode.BarcodeWidth = 100;
                    qrcode.BarcodeHeight = 100;
                    qrcode.AutoResize = false;
                    qrcode.drawBarcode(qrpath);
                    #endregion

                    ImprintedCouponModel model = assignedcoupon.ToImprintedCouponModel();

                    string fileName = string.Format("coupon_{0}.pdf", assignedcoupon.UniqueId_imprintedCoupon);
                    string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);
                    CouponPdfmodule.PrintToPdf(uniquecouponItem, assignedcoupon, assignedcoupon.QrcodeImgPath, _workContext.WorkingLanguage, filePath, _pdfSettings, this.Request.PhysicalApplicationPath);
                    var pdfBytes = System.IO.File.ReadAllBytes(filePath);

                    assignedcoupon.PdfPath = "../../content/files/ExportImport/" + fileName;

                    #region previous qrcode saving code
                    using (WebClient webClient = new WebClient())
                    {
                        using (Stream stream = webClient.OpenRead(url))
                        {
                            using (Bitmap bitmap = new Bitmap(stream))
                            {
                                stream.Flush();
                                stream.Close();
                                bitmap.Save(imagefilePath);
                            }
                        }
                    }
                    #endregion

                    _imprintedcouponService.UpdateImprintedCoupon(assignedcoupon);

                    UserPageModel userpageModel = MappingExtensions.toUserPageModel(assignedcoupon);
                    userpageModel.CouponImageForWebsite = uniquecouponItem.CouponImageForWebsite;
                    userpageModel.BannerCouponImage = uniquecouponItem.BannerCouponImage;
                    userpageModel.StandardCouponImage = uniquecouponItem.StandardCouponImage;
                    SendUponNotificationMessage(_workContext.CurrentCustomer, _workContext.WorkingLanguage.Id, userpageModel);
                    return RedirectToAction("UserPage", "Upon");
                    // return File(pdfBytes, "application/pdf", fileName);
                }
                else
                {
                    return RedirectToAction("SpecialCouponOffer", "Upon");
                }

            }
            else
            {
                return RedirectToAction("SpecialCouponOffer", "Upon");
            }
        }

        #region Email

        public virtual void installUponMessageTemplates()
        {

            var eaGeneral = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("General contact")).FirstOrDefault();
            //var eaSale = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Sales representative")).FirstOrDefault();
            //var eaCustomer = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Customer support")).FirstOrDefault();
            var messageTemplates = new List<MessageTemplate>
                               {
                                   new MessageTemplate
                                       {
                                           Name = "Upon.ImprintNotification",
                                           Subject = "%ImprintedCoupon.Title%. Has Imprinted For you.",
                                           Body = "<p>A Upon has been Imprinted \"%ImprintedCoupon.Title%\".</p>",
                                           IsActive = true,
                                           EmailAccountId = eaGeneral.Id,
                                       },
                                   new MessageTemplate
                                       {
                                           Name = "Upon.RedemptionNotification",
                                           Subject = "%ImprintedCoupon.Title% Redeemed.",
                                           Body = "<p>%ImprintedCoupon.Title%&nbsp; has Redeemed.</p><p></p><p>Redeemption Date: %ImprintedCoupon.RedemptionDate%</p>",
                                           IsActive = true,
                                           EmailAccountId = eaGeneral.Id,
                                       }
                                  
                               };
            messageTemplates.ForEach(mt => _messageTemplateRepository.Insert(mt));
        }

        public virtual void AddImprintedcouponTokens(IList<Token> tokens, UserPageModel userpageModel)
        {
            tokens.Add(new Token("ImprintedCoupon.First_Name", userpageModel.First_Name));
            tokens.Add(new Token("ImprintedCoupon.Last_Name", userpageModel.Last_Name));
            tokens.Add(new Token("ImprintedCoupon.Email", userpageModel.Email));
            tokens.Add(new Token("ImprintedCoupon.ExpirationDate", userpageModel.ExpirationDate.ToString()));
            tokens.Add(new Token("ImprintedCoupon.StartDate", userpageModel.StartDate.ToString()));
            tokens.Add(new Token("ImprintedCoupon.EndDate", userpageModel.EndDate.ToString()));
            tokens.Add(new Token("ImprintedCoupon.Offer", userpageModel.Offer));
            tokens.Add(new Token("ImprintedCoupon.Restrictions", userpageModel.Restrictions));
            tokens.Add(new Token("ImprintedCoupon.Title", userpageModel.Title));
            tokens.Add(new Token("ImprintedCoupon.UniqueId_imprintedCoupon", userpageModel.UniqueId_imprintedCoupon.ToString()));
            tokens.Add(new Token("ImprintedCoupon.CouponImageForWebsite", userpageModel.CouponImageForWebsite));
            tokens.Add(new Token("ImprintedCoupon.BannerCouponImage", userpageModel.BannerCouponImage));
            tokens.Add(new Token("ImprintedCoupon.StandardCouponImage", userpageModel.StandardCouponImage));
            tokens.Add(new Token("ImprintedCoupon.QrcodeImgPath", userpageModel.QrcodeImgPath));
            tokens.Add(new Token("ImprintedCoupon.PdfPath", userpageModel.PdfPath));
            tokens.Add(new Token("ImprintedCoupon.RedemptionDate", userpageModel.RedemptionDate.ToString()));
            tokens.Add(new Token("ImprintedCoupon.Amount", userpageModel.Amount));
            //event notification
            // _eventPublisher.TokensAdded(userpageModel, tokens);
        }

        private IList<Token> GenerateTokens(UserPageModel userpageModel)
        {
            var tokens = new List<Token>();
            //_messageTokenProvider.AddStoreTokens(tokens);
            AddImprintedcouponTokens(tokens, userpageModel);
            return tokens;
        }

        public virtual int SendUponNotificationMessage(Customer customer, int languageId, UserPageModel userpageModel)
        {


            if (customer == null)
                throw new ArgumentNullException("customer");

            languageId = EnsureLanguageIsActive(languageId);

            var messageTemplate = GetLocalizedActiveMessageTemplate("Upon.ImprintNotification", languageId);
            if (messageTemplate == null)
                return 0;

            // var tokens = new List<Token>();
            var couponTokens = GenerateTokens(userpageModel);

            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);
            var toEmail = userpageModel.Email;
            var toName = emailAccount.DisplayName;
            return SendNotification(messageTemplate, emailAccount,
                languageId, couponTokens,
                toEmail, toName);
        }

        public virtual int SendUponRedemptionNotificationMessage(Customer customer, int languageId, UserPageModel userpageModel)
        {


            if (customer == null)
                throw new ArgumentNullException("customer");

            languageId = EnsureLanguageIsActive(languageId);

            var messageTemplate = GetLocalizedActiveMessageTemplate("Upon.RedemptionNotification", languageId);
            if (messageTemplate == null)
                return 0;

            // var tokens = new List<Token>();
            var couponTokens = GenerateTokens(userpageModel);

            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);
            var toEmail = userpageModel.Email;
            var toName = emailAccount.DisplayName;
            return SendNotification(messageTemplate, emailAccount,
                languageId, couponTokens,
                toEmail, toName);
        }

        private EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
        {
            var emailAccounId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
            var emailAccount = _emailAccountService.GetEmailAccountById(emailAccounId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            return emailAccount;

        }

        private IList<Token> GenerateTokens(Customer customer)
        {
            var tokens = new List<Token>();
            //  _messageTokenProvider.AddStoreTokens(tokens);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            return tokens;
        }

        private int EnsureLanguageIsActive(int languageId)
        {
            var language = _languageService.GetLanguageById(languageId);
            if (language == null || !language.Published)
                language = _languageService.GetAllLanguages().FirstOrDefault();
            return language.Id;
        }

        private MessageTemplate GetLocalizedActiveMessageTemplate(string messageTemplateName, int languageId)
        {
            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, _storeContext.CurrentStore.Id);
            if (messageTemplate == null)
                return null;

            //var isActive = messageTemplate.GetLocalized((mt) => mt.IsActive, languageId);
            //use
            var isActive = messageTemplate.IsActive;
            if (!isActive)
                return null;

            return messageTemplate;
        }

        private int SendNotification(MessageTemplate messageTemplate,
             EmailAccount emailAccount, int languageId, IEnumerable<Token> tokens,
             string toEmailAddress, string toName)
        {
            //retrieve localized message template data
            var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
            var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
            var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            var bodyReplaced = _tokenizer.Replace(body, tokens, true);

            var email = new QueuedEmail()
            {
                Priority = 5,
                From = emailAccount.Email,
                FromName = emailAccount.DisplayName,
                To = toEmailAddress,
                ToName = toName,
                CC = string.Empty,
                Bcc = bcc,
                Subject = subjectReplaced,
                Body = bodyReplaced,
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id
            };

            _queuedEmailService.InsertQueuedEmail(email);
            return email.Id;
        }
        #endregion

        #endregion

        #region upon webservice

        public string couponlookup(string guid)
        {

            string unique_coupon_id = guid.Substring(0, guid.Length - 4);

            ImprintedCoupon imprintedcoupon = _imprintedcouponService.getImprintedCouponByUniqueId(unique_coupon_id);


            ImprintedCouponModel model = imprintedcoupon.ToImprintedCouponModel();

            // var fileName = string.Format("{0}.xml", unique_coupon_id);/*this.Request.Url.ServerVariables["SERVER_NAME"] this.Request.ServerVariables["REFERER"]*/
            model.UPC = this.Request.ServerVariables["HTTP_HOST"] + model.UPC;
            var jsonData = generateUponjsonmodule.ExportimprintedcouponToXml(model, _imprintedcouponService);

            return jsonData;

        }

        public JsonResult couponlookupnew(string guid)
        {
            string unique_coupon_id = "";
            if (guid.Length > 36)
            {
                unique_coupon_id = guid.Substring(0, guid.Length - 4);
            }
            else
            {
                unique_coupon_id = guid;
            }


            ImprintedCoupon imprintedcoupon = _imprintedcouponService.getImprintedCouponByUniqueId(unique_coupon_id);


            ImprintedCouponModel model = imprintedcoupon.ToImprintedCouponModel();

            // var fileName = string.Format("{0}.xml", unique_coupon_id);/*this.Request.Url.ServerVariables["SERVER_NAME"] this.Request.ServerVariables["REFERER"]*/
            model.UPC = this.Request.ServerVariables["HTTP_HOST"] + model.UPC;
            var coupninfo = generateUponjsonmodule.ExportimprintedcouponToXmlNew(model, _imprintedcouponService);

            //jsonData = jsonData.Replace(@"\\\", " ");

            return Json(new
            {
                coupninfo
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult couponredemption()
        {
            // string unique_coupon_id = guid.Substring(0, guid.Length - 4);

            // ImprintedCoupon imprintedcoupon = _imprintedcouponService.getImprintedCouponByUniqueId(unique_coupon_id);

            ImprintedCouponModel model = new ImprintedCouponModel();

            // return View("Nop.Plugin.Upon.Views.Upon.CouponRedeem", model);
            return View("CouponRedeem", model);

        }

        [HttpPost, FormValueRequired("save", "", "")]
        public string couponredemption(ImprintedCouponModel model)
        {
            string redeemptionstatus = "Error";
            try
            {
                ImprintedCoupon imprintedcoupon = _imprintedcouponService.getImprintedCouponByUniqueId(model.UniqueId_imprintedCoupon.ToString());



                if (imprintedcoupon.ExpirationDate <= DateTime.Today)
                {
                    redeemptionstatus = "Expired";
                    imprintedcoupon.Status = "Expired";
                    _imprintedcouponService.UpdateImprintedCoupon(imprintedcoupon);
                }

                else
                {
                    if (imprintedcoupon.Status == "redeemed")
                    {
                        redeemptionstatus = "Duplicate";
                    }
                    else if (imprintedcoupon.Status == "Active")
                    {
                        if (string.IsNullOrEmpty(model.Amount))
                        {
                            redeemptionstatus = "Error";
                        }
                        else
                        {
                            imprintedcoupon.Status = "Redeemed";
                            imprintedcoupon.Amount = model.Amount;
                            imprintedcoupon.AmountInDecimal = double.Parse((model.Amount as string));//.Trim('$')
                            imprintedcoupon.RedemptionDate = DateTime.Now;
                            _imprintedcouponService.UpdateImprintedCoupon(imprintedcoupon);
                            redeemptionstatus = "Success";
                            UserPageModel userpageModel = MappingExtensions.toUserPageModel(imprintedcoupon);

                            UniqueCoupon uniquecouponItem = _uniquecouponService.GetuniquecouponById(imprintedcoupon.CouponId);

                            userpageModel.RedemptionDate = DateTime.Now;
                            userpageModel.CouponImageForWebsite = uniquecouponItem.CouponImageForWebsite;
                            userpageModel.BannerCouponImage = uniquecouponItem.BannerCouponImage;
                            userpageModel.StandardCouponImage = uniquecouponItem.StandardCouponImage;

                            SendUponRedemptionNotificationMessage(_workContext.CurrentCustomer, _workContext.WorkingLanguage.Id, userpageModel);

                        }
                    }
                }
            }
            catch
            {
                redeemptionstatus = "Error";
            }

            string jsondata = "{ redemption:'" + redeemptionstatus + "'}";
            return jsondata;
            // return Json(new { redemption = redeemptionstatus }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region admin section

        #region unique coupon

        [AdminAuthorize]
        public ActionResult UniqueCouponList()
        {

            // return View("Nop.Plugin.Upon.Views.Upon.UniqueCouponList", GridModel);
            return View("UniqueCouponList");
        }

        [AdminAuthorize]
        [HttpPost]
				public ActionResult UniqueCouponList(DataSourceRequest command)
        {
            var uniquecoupons = _uniquecouponService.GetAllUniqueCoupons(command.Page - 1, command.PageSize);
						var GridModel = new DataSourceResult
            {
                Data = uniquecoupons.Select(x =>
                {
                    UniqueCouponModel m = x.ToModel();

                    return m;
                }),

                Total = uniquecoupons.TotalCount

            };

            //return View("Nop.Plugin.Upon.Views.Upon.UniqueCouponList", GridModel);
            
						return new JsonResult
						{
							Data = GridModel
						};
        }

        [AdminAuthorize]
        public ActionResult CreateUniqueCoupon()
        {

            var model = new UniqueCouponModel();
            //return View("Nop.Plugin.Upon.Views.Upon.UniqueCouponCreate", model);
            return View("UniqueCouponCreate", model);
        }

        [AdminAuthorize]
				[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateUniqueCoupon(UniqueCouponModel model, bool continueEditing, HttpPostedFileBase CouponImageForWebsite, HttpPostedFileBase BannerCouponImage, HttpPostedFileBase StandardCouponImage)
        {

            if (ModelState.IsValid)
            {
                UniqueCoupon uniquecouponItem = model.ToEntity();
                _uniquecouponService.InsertUniqueCoupon(uniquecouponItem);

                Linear barcode = new Linear();

                barcode.Type = BarcodeType.UPCA;

                barcode.Data = uniquecouponItem.UPC;

                //uniquecouponItem.UPC

                barcode.BarcodeWidth = 500;
                //  barcode.BarcodeHeight = 200;
                var fileNameupc = "upca" + uniquecouponItem.Id + ".jpeg";
                var pathupc = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileNameupc);
                barcode.drawBarcode(pathupc);

                uniquecouponItem.barcodeImagePath = "/Plugins/Upon/Content/images/uploads/" + fileNameupc;
                // int i = 0;
                var fname1 = Path.GetFileName(CouponImageForWebsite.FileName);
                var fname = Path.GetFileName(BannerCouponImage.FileName);
                var f2name = Path.GetFileName(StandardCouponImage.FileName);

                var fileName = "CI" + uniquecouponItem.Id + "_" + fname1;
                var dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName;
                uniquecouponItem.CouponImageForWebsite = dbsavepath;
                var path = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileName);
                //CouponImageForWebsite.SaveAs(path);
                Bitmap mg3 = new Bitmap(CouponImageForWebsite.InputStream);
                //double width3 = mg3.Width;
                //double height3 = mg3.Height;
                //double ratio3 = (width3 / height3);

                //double newHeight3 = 400 / ratio3;
                //Size newSize3 = new Size(Convert.ToInt32(600), Convert.ToInt32(newHeight3));
                //Bitmap bp3 = ResizeImage(mg3, newSize3);
                if (mg3 != null)
                    mg3.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);



                var fileName1 = "BI" + uniquecouponItem.Id + "_" + fname;
                dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName1;
                uniquecouponItem.BannerCouponImage = dbsavepath;
                path = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileName1);

                //BannerCouponImage.SaveAs(path);
                Bitmap mg = new Bitmap(BannerCouponImage.InputStream);
                double width = mg.Width;
                double height = mg.Height;
                double ratio = width / height;

                double newHeight1 = 600 / ratio;
                Size newSize = new Size(Convert.ToInt32(600), Convert.ToInt32(newHeight1));
                Bitmap bp = ResizeImage(mg, newSize);
                if (bp != null)
                    bp.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);



                var fileName2 = "SI" + uniquecouponItem.Id + "_" + f2name;
                dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName2;
                uniquecouponItem.StandardCouponImage = dbsavepath;

                path = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileName2);
                // StandardCouponImage.SaveAs(path);
                Bitmap mg2 = new Bitmap(StandardCouponImage.InputStream);
                double width2 = mg2.Width;
                double height2 = mg2.Height;
                double ratio2 = width2 / height2;

                double newHeight2 = 600 / ratio2;
                Size newSize2 = new Size(Convert.ToInt32(600), Convert.ToInt32(newHeight2));

                Bitmap bp2 = ResizeImage(mg2, newSize2);
                if (bp2 != null)
                    bp2.Save(path, System.Drawing.Imaging.ImageFormat.Png);

                #region previous filearray code

                //foreach (var file in files)
                //{

                //    if (file.ContentLength > 0)
                //    {
                //        var fileName = "";
                //        var fileNamefromPath = Path.GetFileName(file.FileName);


                //        var dbsavepath = "";
                //        if (i == 0)
                //        {
                //            fileName += "BI" + uniquecouponItem.Id + "_" + fileNamefromPath;
                //            dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName;
                //            uniquecouponItem.BannerCouponImage = dbsavepath;
                //        }
                //        else
                //        {
                //            fileName += "PI" + uniquecouponItem.Id + "_" + fileNamefromPath;
                //            dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName;
                //            uniquecouponItem.PrintableCouponImage = dbsavepath;
                //        }
                //        var path = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileName);
                //        i++;
                //        file.SaveAs(path);
                //    }
                //}

                #endregion

                _uniquecouponService.UpdateUniqueCoupon(uniquecouponItem);

                // SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Events.Added"));
                return continueEditing ? RedirectToAction("Edit", "/Plugin/Upon/UniqueCoupon", new { id = uniquecouponItem.Id }) : RedirectToAction("List", "/Plugin/Upon/UniqueCoupon");
            }

            //return View("Nop.Plugin.Upon.Views.Upon.UniqueCouponCreate", model);
            return View("UniqueCouponCreate", model);
        }

        [AdminAuthorize]
        public ActionResult EditUniqueCoupon(int id)
        {

            var uniquecouponItem = _uniquecouponService.GetuniquecouponById(id);
            if (uniquecouponItem == null)

                return RedirectToAction("UniqueCouponList");


            UniqueCouponModel model = uniquecouponItem.ToModel();

            // return View("Nop.Plugin.Upon.Views.Upon.UniqueCouponEdit", model);
            return View("UniqueCouponEdit", model);
        }

        [AdminAuthorize]
				[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditUniqueCoupon(UniqueCouponModel model, bool continueEditing, IEnumerable<HttpPostedFileBase> files)// BannerCouponImage, HttpPostedFileBase PrintableCouponImage)
        {

            var uniquecouponItem = _uniquecouponService.GetuniquecouponById(model.Id);

            if (uniquecouponItem == null)

                return RedirectToAction("List", "/Plugin/Upon/UniqueCoupon");

            if (ModelState.IsValid)
            {
                uniquecouponItem.Title = model.Title;
                uniquecouponItem.StartDate = model.StartDate;
                uniquecouponItem.EndDate = model.EndDate;
                uniquecouponItem.Offer = model.Offer;
                uniquecouponItem.Restrictions = model.Restrictions;
                uniquecouponItem.Description = model.Description;
                uniquecouponItem.Quantity = model.Quantity;
                uniquecouponItem.DateRange = model.DateRange;
                uniquecouponItem.CouponImageForWebsite = model.CouponImageForWebsite;
                uniquecouponItem.BannerCouponImage = model.BannerCouponImage;
                uniquecouponItem.StandardCouponImage = model.StandardCouponImage;
                uniquecouponItem.Disclaimer = model.Disclaimer;
                uniquecouponItem.Active = model.Active;
                uniquecouponItem.UPC = model.UPC;
                Linear barcode = new Linear();
                barcode.Type = BarcodeType.UPCA;
                barcode.Data = uniquecouponItem.UPC;
                var fileNameupc = "upca" + uniquecouponItem.Id + ".jpeg";
                var pathupc = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileNameupc);
                barcode.drawBarcode(pathupc);

                uniquecouponItem.barcodeImagePath = "/Plugins/Upon/Content/images/uploads/" + fileNameupc;
                int i = 0;
                foreach (var file in files)
                {
                    if (file == null)
                    {
                        if (i == 0)
                        {
                            uniquecouponItem.CouponImageForWebsite = model.CouponImageForWebsite;
                        }
                        if (i == 1)
                        {
                            uniquecouponItem.BannerCouponImage = model.BannerCouponImage;
                        }
                        else
                        {
                            uniquecouponItem.StandardCouponImage = model.StandardCouponImage;
                        }
                    }
                    else
                    {
                        if (file.ContentLength > 0)
                        {
                            var fileName = "";
                            var fileNamefromPath = Path.GetFileName(file.FileName);


                            var dbsavepath = "";
                            double newWidth = 600;
                            if (i == 0)
                            {
                                fileName += "CI" + uniquecouponItem.Id + "_" + fileNamefromPath;
                                dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName;
                                uniquecouponItem.CouponImageForWebsite = dbsavepath;
                                // newWidth = 400;
                            }
                            else if (i == 1)
                            {
                                fileName += "BI" + uniquecouponItem.Id + "_" + fileNamefromPath;
                                dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName;
                                uniquecouponItem.BannerCouponImage = dbsavepath;
                            }
                            else
                            {
                                fileName += "SI" + uniquecouponItem.Id + "_" + fileNamefromPath;
                                dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName;
                                uniquecouponItem.StandardCouponImage = dbsavepath;
                            }
                            var path = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileName);

                            //file.SaveAs(path);
                            Bitmap mg = new Bitmap(file.InputStream);
                            double width = mg.Width;
                            double height = mg.Height;
                            double ratio = width / height;

                            double newHeight1 = newWidth / ratio;
                            Size newSize = new Size(Convert.ToInt32(600), Convert.ToInt32(newHeight1));

                            if (i != 0)
                            {
                                Bitmap bp = ResizeImage(mg, newSize);
                                if (bp != null)
                                    bp.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                            }
                            else
                            {
                                mg.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                            }

                        }
                    }
                    i++;
                }

                #region previous code
                //var path = "";
                //var dbsavepath = "";
                //if (BannerCouponImage == null)
                //{
                //    uniquecouponItem.BannerCouponImage = model.BannerCouponImage;
                //}
                //else
                //{
                //    var fname = Path.GetFileName(BannerCouponImage.FileName);


                //    var fileName1 = "BI" + uniquecouponItem.Id + "_" + fname;
                //     dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName1;
                //    uniquecouponItem.BannerCouponImage = dbsavepath;
                //     path = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileName1);
                //    BannerCouponImage.SaveAs(path);

                //}

                //if (PrintableCouponImage == null)
                //{
                //    uniquecouponItem.PrintableCouponImage = model.PrintableCouponImage;
                //}
                //else
                //{
                //    var f2name = Path.GetFileName(PrintableCouponImage.FileName);
                //    var fileName2 = "PI" + uniquecouponItem.Id + "_" + f2name;
                //    dbsavepath = "Plugins/Upon/Content/images/uploads/" + fileName2;
                //    uniquecouponItem.PrintableCouponImage = dbsavepath;

                //    path = Path.Combine(Server.MapPath("\\Plugins\\Upon\\Content\\images\\uploads"), fileName2);
                //    PrintableCouponImage.SaveAs(path);
                //}
                #endregion

                _uniquecouponService.UpdateUniqueCoupon(uniquecouponItem);

                // SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsItems.Updated"));
                return continueEditing ? RedirectToAction("Edit", "/Plugin/Upon/UniqueCoupon", new { id = uniquecouponItem.Id }) : RedirectToAction("List", "/Plugin/Upon/UniqueCoupon");
            }

            // return View("Nop.Plugin.Upon.Views.Upon.UniqueCouponEdit", model);
            return View("UniqueCouponEdit", model);
        }

        [AdminAuthorize]
        public ActionResult DeleteConfirmedUniqueCoupon(int id)
        {
            var uniquecouponItem = _uniquecouponService.GetuniquecouponById(id);
            if (uniquecouponItem == null)
                //No news item found with the specified id
                return RedirectToAction("List", "/Plugin/Upon/UniqueCoupon");

            //delete all entries in event_customerRole_customMapping Table for that event 
            _uniquecouponService.DeleteUniqueCoupon(uniquecouponItem);

            //  SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsItems.Deleted"));
            return RedirectToAction("List", "/Plugin/Upon/UniqueCoupon");
        }

        public ActionResult SaveFile(HttpPostedFileBase file)
        {

            if (file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                file.SaveAs(path);
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Utility

        private Bitmap ResizeImage(Bitmap mg, Size newSize)
        {
            double ratio = 0d;
            double myThumbWidth = 0d;
            double myThumbHeight = 0d;
            int x = 0;
            int y = 0;

            Bitmap bp;

            if ((mg.Width / Convert.ToDouble(newSize.Width)) > (mg.Height /
            Convert.ToDouble(newSize.Height)))
                ratio = Convert.ToDouble(mg.Width) / Convert.ToDouble(newSize.Width);
            else
                ratio = Convert.ToDouble(mg.Height) / Convert.ToDouble(newSize.Height);
            myThumbHeight = Math.Ceiling(mg.Height / ratio);
            myThumbWidth = Math.Ceiling(mg.Width / ratio);

            //Size thumbSize = new Size((int)myThumbWidth, (int)myThumbHeight);
            Size thumbSize = new Size((int)newSize.Width, (int)newSize.Height);
            bp = new Bitmap(newSize.Width, newSize.Height);
            x = (newSize.Width - thumbSize.Width) / 2;
            y = (newSize.Height - thumbSize.Height);
            // Had to add System.Drawing class in front of Graphics ---
            System.Drawing.Graphics g = Graphics.FromImage(bp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            Rectangle rect = new Rectangle(x, y, thumbSize.Width, thumbSize.Height);
            g.DrawImage(mg, rect, 0, 0, mg.Width, mg.Height, GraphicsUnit.Pixel);

            return bp;

        }


        #endregion

        #region report

        #region dashboard index page
       
        public ActionResult uPonDashboard()
        {
					List<ImprintedCoupon> imprintedCouponList = _imprintedcouponService.GetActiveExpiredImprintedcoupons();

					foreach(ImprintedCoupon imprintedcoupon in imprintedCouponList)
					{
						imprintedcoupon.Status = "Expired";
						_imprintedcouponService.UpdateImprintedCoupon(imprintedcoupon);
					}
          return View("UponDashboard");
        }
        
        #endregion

        #region all upons search page
        /// <summary>
        /// This is the View search list from admin dashboard
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult List()
        {
				  return View("viewallupon");
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command, UserPageModelList model)
        {
            int page = command.Page - 1;
            int pagesize = command.PageSize;


            var allimprintedupons = _imprintedcouponService.GetAllImprintedcoupon(model.Email, model.CouponId,
            model.Offer, model.Status, model.ImprintingDateStart, model.ImprintingDateEnd, model.ExpirationDateStart, model.ExpirationDateEnd,
            model.RedemptionDateStart, model.RedemptionDateEnd, model.AmountRangeFrom, model.AmountRangeTo, page, pagesize);

            List<UserPageModel> allimprintedcouponsModels = new List<UserPageModel>();
            for (var i = 0; i < allimprintedupons.Count; i++)
            {

                UserPageModel model1 = allimprintedupons[i].toUserPageModel();

                allimprintedcouponsModels.Add(model1);

            }

            var gridModel = new DataSourceResult
            {

                Data = allimprintedcouponsModels,
                Total = allimprintedupons.TotalCount
            };

            return new JsonResult
            {
                Data = gridModel
            };



        }

        #endregion

        #region all active upons search page
       
        public ActionResult ActiveList()
        {
            return View("viewallactiveupon");
        }

        [HttpPost]
        public ActionResult ActiveList(DataSourceRequest command, UserPageModelList model)
        {
            int page = command.Page - 1;
            int pagesize = command.PageSize;

            var allimprintedupons = _imprintedcouponService.GetAllActiveImprintedcoupon(model.Email, model.CouponId,
            model.Offer, model.ImprintingDateStart, model.ImprintingDateEnd, model.ExpirationDateStart, model.ExpirationDateEnd,
            model.RedemptionDateStart, model.RedemptionDateEnd, model.AmountRangeFrom, model.AmountRangeTo, page, pagesize);


            List<UserPageModel> allimprintedcouponsModels = new List<UserPageModel>();
            for (var i = 0; i < allimprintedupons.Count; i++)
            {

                UserPageModel model1 = allimprintedupons[i].toUserPageModel();
                allimprintedcouponsModels.Add(model1);


            }

            var gridModel = new DataSourceResult
            {

                Data = allimprintedcouponsModels,
                Total = allimprintedupons.TotalCount
            };

            return new JsonResult
            {
                Data = gridModel
            };



        }

        #endregion

        

        #region report utility

        //not used
        [NonAction]
        public bool checkTheAmount(string AmountRangeFrom, string AmountRangeTo, string Amount)
        {
            var flag1 = 0;
            var flag2 = 0;
            //UserPageModel model1 = allimprintedupons[i].toUserPageModel();

            string amountstring = (Amount as string).Trim('$');
            double amount = double.Parse(amountstring);

            if (!string.IsNullOrWhiteSpace(AmountRangeFrom))
            {

                string amountrangefromstring = (AmountRangeFrom as string).Trim('$');
                double amountrangefrom = double.Parse(amountrangefromstring);
                if (amount < amountrangefrom)
                {

                    flag1 = 1;

                }
            }
            if (!string.IsNullOrWhiteSpace(AmountRangeTo) && flag1 == 0)
            {
                string amountrangetostring = (AmountRangeTo as string).Trim('$');
                double amountrangeto = double.Parse(amountrangetostring);
                if (amount > amountrangeto)
                {
                    flag2 = 1;

                }
            }

            if (flag1 == 0 && flag2 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        [NonAction]
        protected virtual List<upontotal> GetUponTotalReportModel()
        {
            var report = new List<upontotal>();
            var uponTotalActive = _imprintedcouponService.upontotal("Active");
            var uponTotalRedeemed = _imprintedcouponService.upontotal("Redeemed");
            var uponTotalExpired = _imprintedcouponService.upontotal("Expired");

            report.Add(uponTotalActive);
            report.Add(uponTotalRedeemed);
            report.Add(uponTotalExpired);



            return report;
        }

        //[NonAction]
        //protected virtual List<UniqueCouponModel> GetAvailableUponsReportModel()
        //{
            
           
           
        //}

        #endregion

        #region upon total


        [ChildActionOnly]
        public ActionResult UponTotalReport()
        {
            //var model = GetUponTotalReportModel();
            //return PartialView("Nop.Plugin.Upon.Views.Upon.UponTotalReport", model);
            return PartialView("UponTotalReport");
        }

        [AdminAuthorize]
        [HttpPost]
        public ActionResult UponTotalReport(DataSourceRequest command)
        {
            var model = GetUponTotalReportModel();
            var gridModel = new DataSourceResult
            {
                Data = model,
                Total = model.Count
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        #region added by Razib

        ///----------------------------------Added by Razib------------------------------------------------
        /// <summary>
        /// Filters the imprinted coupon list.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="filterKey">The filter key.</param>
        /// <returns></returns>
        [AdminAuthorize, HttpGet]
        public ActionResult FilterImprintedCouponList(string status = "", string filterKey = "")
        {

            var listModel = new UserPageModelList();

            var filterimprintedupons = _imprintedcouponService.GetFilterImprintedcoupon(null, 0, _adminAreaSettings.GridPageSize, status, filterKey);
            List<UserPageModel> allimprintedcouponsModels = new List<UserPageModel>();
            for (var i = 0; i < filterimprintedupons.Count; i++)
            {
                UserPageModel model = filterimprintedupons[i].toUserPageModel();
                allimprintedcouponsModels.Add(model);
            }

            listModel.imprintedcoupons = new DataSourceResult
            {

                Data = allimprintedcouponsModels,
                Total = filterimprintedupons.TotalCount
            };

						ViewBag.Status = status;
						ViewBag.FilterKey = filterKey;
            return View("FilterImprintedCoupon", listModel);
        }

        ///----------------------------------Added by Razib------------------------------------------------
        [HttpPost]
        public ActionResult FilterImprintedCouponList(DataSourceRequest command, UserPageModelList model, string status = "", string filterKey = "")
        {

            int page = command.Page - 1;
            int pagesize = command.PageSize;

            var allimprintedupons = _imprintedcouponService.GetFilterImprintedcoupon(model, page, pagesize, status, filterKey);


            List<UserPageModel> allimprintedcouponsModels = new List<UserPageModel>();
            for (var i = 0; i < allimprintedupons.Count; i++)
            {

                UserPageModel model1 = allimprintedupons[i].toUserPageModel();
                allimprintedcouponsModels.Add(model1);


            }

            var gridModel = new DataSourceResult
            {

                Data = allimprintedcouponsModels,
                Total = allimprintedupons.TotalCount
            };

            return new JsonResult
            {
                Data = gridModel
            };
        }
        
        #endregion

        #endregion

        #region Available Upons

        [ChildActionOnly]
        public ActionResult AvailableUponsReport()
        {
            return PartialView("AvailableUposReport");            
        }
       
        [AdminAuthorize]
        [HttpPost]
        public ActionResult AvailableUponsReport(DataSourceRequest command)
        {
            var uniquecoupons = _uniquecouponService.GetAllAvailableUniqueCoupons(command.Page - 1, command.PageSize);
            var GridModel = new DataSourceResult
            {
                Data = uniquecoupons.Select(x =>
                {
                    UniqueCouponModel m = x.ToModel();

                    return m;
                }),

                Total = uniquecoupons.TotalCount

            };


						return new JsonResult
						{
							Data = GridModel
						};
        }

        #endregion
        
        #region Export

        public ActionResult ExportExcelAll(string active = null)
        {

            try
            {
                List<ImprintedCoupon> allimprintedupons = new List<ImprintedCoupon>();
                if (string.IsNullOrEmpty(active))
                {
                    allimprintedupons = _imprintedcouponService.GetAllImprintedcouponsForExport();

                }
                else
                {
                    allimprintedupons = _imprintedcouponService.GetAllActiveImprintedcouponsForExport();
                }
                string fileName = string.Format("Imprinted_uPons_{0}_{1}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
                string filePath = System.IO.Path.Combine(Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);

                ExportManagerUpon.ExportImprintedUponsToXlsx(filePath, allimprintedupons, _storeInformationSettings, _storeContext);

                var bytes = System.IO.File.ReadAllBytes(filePath);
                return File(bytes, "text/xls", fileName);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

				[AdminAuthorize, HttpGet]
				public ActionResult ExportFilterExcelAll(SearchPageModel filter)
				{
					try
					{
						List<ImprintedCoupon> allimprintedupons = new List<ImprintedCoupon>();

						allimprintedupons = _imprintedcouponService.FilterImprintedCouponsForExport(filter, filter.Status, filter.FilterKey);
						
						string fileName = string.Format("Imprinted_uPons_{0}_{1}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
						string filePath = System.IO.Path.Combine(Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);

						ExportManagerUpon.ExportImprintedUponsToXlsx(filePath, allimprintedupons, _storeInformationSettings, _storeContext);

						var bytes = System.IO.File.ReadAllBytes(filePath);
						return File(bytes, "text/xls", fileName);
					}
					catch(Exception exc)
					{
						ErrorNotification(exc);
						return RedirectToAction("List");
					}
				}

        public ActionResult ExportExcelSelected(string selectedIds)
        {
            var imprintedupons = new List<ImprintedCoupon>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                imprintedupons.AddRange(_imprintedcouponService.GetImprintedcouponByIds(ids));
            }

            string fileName = string.Format("Imprinted_uPons_{0}_{1}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
            string filePath = System.IO.Path.Combine(Request.PhysicalApplicationPath, "content\\files\\ExportImport", fileName);

						ExportManagerUpon.ExportImprintedUponsToXlsx(filePath, imprintedupons, _storeInformationSettings, _storeContext);

            var bytes = System.IO.File.ReadAllBytes(filePath);
            return File(bytes, "text/xls", fileName);
        }

        public ActionResult ExportXmlAll(string active = null)
        {

            try
            {
                List<ImprintedCoupon> allimprintedupons = new List<ImprintedCoupon>();
                if (string.IsNullOrEmpty(active))
                {
                    allimprintedupons = _imprintedcouponService.GetAllImprintedcouponsForExport();
                }
                else
                {
                    allimprintedupons = _imprintedcouponService.GetAllActiveImprintedcouponsForExport();
                }

                var fileName = string.Format("Imprinted_uPons_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
                var xml = ExportManagerUpon.ExportImprintedUponsToXml(allimprintedupons);
                return new XmlDownloadResult(xml, fileName);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

				[AdminAuthorize, HttpGet]
				public ActionResult ExportFilterXmlAll(SearchPageModel filter)
				{

					try
					{
						List<ImprintedCoupon> allimprintedupons = new List<ImprintedCoupon>();

						allimprintedupons = _imprintedcouponService.FilterImprintedCouponsForExport(filter,filter.Status, filter.FilterKey);

						var fileName = string.Format("Imprinted_uPons_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
						var xml = ExportManagerUpon.ExportImprintedUponsToXml(allimprintedupons);
						return new XmlDownloadResult(xml, fileName);
					}
					catch(Exception exc)
					{
						ErrorNotification(exc);
						return RedirectToAction("List");
					}
				}

        public ActionResult ExportXmlSelected(string selectedIds)
        {

            var imprintedupons = new List<ImprintedCoupon>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                imprintedupons.AddRange(_imprintedcouponService.GetImprintedcouponByIds(ids));
            }

            var fileName = string.Format("Imprinted_uPons_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            var xml = ExportManagerUpon.ExportImprintedUponsToXml(imprintedupons);
            return new XmlDownloadResult(xml, fileName);
        }

        #endregion

        #endregion
       
        #endregion

      

    }
}

