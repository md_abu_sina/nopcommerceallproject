﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Upon
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("specialCouponOffer", "specialCouponOffer",
                            new { controller = "Upon", action = "SpecialCouponOffer" },
                            new[] { "Nop.Plugin.Upon.Controllers" });
            routes.MapRoute("UserPage", "UserPage",
                            new { controller = "Upon", action = "UserPage" },
                            new[] { "Nop.Plugin.Upon.Controllers" });

            routes.MapRoute("UniqueCouponDetails", "UniqueCoupon/{id}",
                            new { controller = "Upon", action = "UniqueCouponDetails", id = UrlParameter.Optional },
                            new[] { "Nop.Plugin.Upon.Controllers" });
            routes.MapRoute("YourCoupon", "YourCoupon/{id}",
                            new { controller = "Upon", action = "AssignUniqueCouponToThisUser", id = UrlParameter.Optional },
                            new[] { "Nop.Plugin.Upon.Controllers" });
            routes.MapRoute("installMessageTemplateForUpon", "installMessageTemplateForUpon",
                            new { controller = "Upon", action = "installUponMessageTemplates" },
                            new[] { "Nop.Plugin.Upon.Controllers" });


            routes.MapRoute("CouponLookup", "uPon/lookup/{guid}",
                            new { controller = "Upon", action = "couponlookup", guid = "" },
                            new[] { "Nop.Plugin.Upon.Controllers" });

            routes.MapRoute("CouponLookupnew", "uPon/lookupnew/{guid}",
                     new { controller = "Upon", action = "couponlookupnew", guid = "" },
                     new[] { "Nop.Plugin.Upon.Controllers" });


            routes.MapRoute("CouponRedeem", "uPon/redeem",
                           new { controller = "Upon", action = "couponredemption" },
                           new[] { "Nop.Plugin.Upon.Controllers" });


            routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.Index", "Admin/Plugin/Upon/uPonDashboard/Index",
                   new { controller = "Upon", action = "uPonDashboard" },
                   new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.List", "Admin/Plugin/Upon/uPonDashboard/List",
                        new { controller = "Upon", action = "List" },
                        new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.ActiveList", "Admin/Plugin/Upon/uPonDashboard/ActiveList",
                       new { controller = "Upon", action = "ActiveList" },
                       new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("UponTotalReport", "UponTotalReport",
                        new { controller = "Upon", action = "UponTotalReport" },
                        new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            #region Added by Razib
           
						routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.FilterImprintedCouponList", "Admin/Plugin/Upon/uPonDashboard/FilterImprintedCouponList",
												new { controller = "Upon", action = "FilterImprintedCouponList" },
												new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            #endregion
            //routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.UponTotalReportList", "Admin/Plugin/Upon/uPonDashboard/UponTotalReportList",
            //           new { controller = "Upon", action = "UponTotalReportList" },
            //           new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("AvailableUponsReport", "AvailableUponsReport",
                      new { controller = "Upon", action = "AvailableUponsReport" },
                      new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            //routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.AvailableUponsReportList", "Admin/Plugin/Upon/uPonDashboard/AvailableUponsReportList",
            //          new { controller = "Upon", action = "AvailableUponsReportList" },
            //          new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.ExportXmlAll", "Admin/Plugin/Upon/uPonDashboard/ExportXmlAll",
                      new { controller = "Upon", action = "ExportXmlAll" },
                      new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");
						routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.ExportFilterXmlAll", "Admin/Plugin/Upon/uPonDashboard/ExportFilterXmlAll",
                      new { controller = "Upon", action = "ExportFilterXmlAll" },
                      new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.ExportXmlSelected", "Admin/Plugin/Upon/uPonDashboard/ExportXmlSelected",
                    new { controller = "Upon", action = "ExportXmlSelected" },
                    new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.ExportExcelAll", "Admin/Plugin/Upon/uPonDashboard/ExportExcelAll",
                       new { controller = "Upon", action = "ExportExcelAll" },
                       new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

						routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.ExportFilterExcelAll", "Admin/Plugin/Upon/uPonDashboard/ExportFilterExcelAll",
                       new { controller = "Upon", action = "ExportFilterExcelAll" },
                       new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.uPonDashboard.ExportExcelSelected", "Admin/Plugin/Upon/uPonDashboard/ExportExcelSelected",
                     new { controller = "Upon", action = "ExportExcelSelected" },
                     new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.UniqueCoupon.List", "Admin/Plugin/Upon/UniqueCoupon/List",
                        new { controller = "Upon", action = "UniqueCouponList" },
                        new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.UniqueCoupon.Create", "Admin/Plugin/Upon/UniqueCoupon/Create",
                       new { controller = "Upon", action = "CreateUniqueCoupon" },
                       new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.UniqueCoupon.Edit", "Admin/Plugin/Upon/UniqueCoupon/Edit/{id}",
                     new { controller = "Upon", action = "EditUniqueCoupon", id = UrlParameter.Optional },
                     new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Upon.UniqueCoupon.Delete", "Admin/Plugin/Upon/UniqueCoupon/Delete/{id}",
                         new { controller = "Upon", action = "DeleteConfirmedUniqueCoupon", id = UrlParameter.Optional },
                        new[] { "Nop.Plugin.Upon.Controllers" }).DataTokens.Add("area", "admin");




        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}