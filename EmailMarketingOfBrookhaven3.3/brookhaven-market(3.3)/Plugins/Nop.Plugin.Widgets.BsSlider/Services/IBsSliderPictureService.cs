using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.BsSlider.Domain;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Payments;

namespace Nop.Plugin.Widgets.BsSlider.Services
{
    public interface IBsSliderPictureService
    {

        BsSliderPicture GetSliderPictureById(int sliderId, int pictureId);
        IPagedList<BsSliderPicture> GetAllSliderPictures(int pageIndex, int pageSize, int sliderId);
        List<BsSliderPicture> GetAllPictures(int sliderId);
        void InsertSliderPicture(BsSliderPicture sliderPicture);
        void UpdateSliderPicture(BsSliderPicture sliderPicture);
        void DeleteSlider(BsSliderPicture sliderPicture);
    }
}