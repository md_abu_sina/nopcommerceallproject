using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Core;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.Widgets.BsSlider.Models;
using Nop.Web.Framework;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Widgets.BsSlider.Models
{
    public class SliderPictureModel : BaseNopEntityModel
    {
        public virtual int SliderId { get; set; }
        
        [NopResourceDisplayName("Plugins.Widgets.NivoSlider.Picture")]
        [UIHint("Picture")]
        public virtual int ImageId { get; set; }
        public virtual string PictureUrl { get; set; }
        public virtual string PictureLink { get; set; }
        public virtual int DisplayOrder { get; set; }
        [AllowHtml]
        public virtual string HtmlCode { get; set; }
        public virtual bool Transition { get; set; }
        public virtual bool IsProductPicture { get; set; }
        public virtual SliderModel Slider { get; set; }
    }
}
