using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Core;
//using Nop.Plugin.Other.Catering.Validators;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Widgets.BsSlider.Models
{
		//[Validator(typeof(CateringEmailValidator))]
		public class SliderModel : BaseNopEntityModel
    {
        //private ICollection<SliderPictureModel> _bsSliderPictures;

        public virtual string Name { get; set; }
        public List<SliderPictureModel> BsSliderPictures;
        public virtual string WidgetZone { get; set; }
        public virtual bool Published { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual int Height { get; set; }
        public virtual int EffectId { get; set; }
        public virtual int AnimSpeed { get; set; }
        public virtual int PauseTime { get; set; }
        public virtual bool DirectionNav { get; set; }
        public virtual bool ControlNav { get; set; }
        public virtual bool ControlNavThumbs { get; set; }
        public virtual bool PauseOnHover { get; set; }
        public virtual bool ManualAdvance { get; set; }
        public virtual int ThemeId { get; set; }

        //public virtual ICollection<SliderPictureModel> BsSliderPictures
        //{
        //    get { return _bsSliderPictures ?? (_bsSliderPictures = new List<SliderPictureModel>()); }
        //    protected set { _bsSliderPictures = value; }
        //}
    }
}
