﻿using Nop.Core;
using System;

namespace Nop.Plugin.RecipeNew.Domain
{
    public class RecipeShoppingList : BaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int CustomerID { get; set; }
        public virtual int RecipeID { get; set; }
        public virtual int Servings { get; set; }
        public virtual DateTime? TimeStampUtc { get; set; }

    }
}
