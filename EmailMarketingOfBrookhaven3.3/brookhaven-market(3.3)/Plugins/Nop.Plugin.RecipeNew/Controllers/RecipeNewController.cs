﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Plugin.RecipeNew.Models;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Messages;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Data;
using System.Web.UI.WebControls;
using Nop.Plugin.RecipeNew.Services;
using Nop.Plugin.RecipeNew.Domain;
using Nop.Services.Security;
using Nop.Services.Directory;
//using Nop.Admin;
using System.Net;
using System.Collections;
using System.Text.RegularExpressions;
using Nop.Core.Domain;
using System.IO;
using Nop.Core.Domain.Directory;
using Nop.Services.Common;
using System.Globalization;
using Nop.Services.Topics;
using Nop.Services.Logging;
using Nop.Services.Configuration;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace Nop.Plugin.RecipeNew.Controllers
{
    public class RecipeNewController : Controller
    {
        public string AdGroupId { get; private set; }

        #region Field
        double pageSize = 20;
        int pagerCount = 9;
        int tipspagerCount = 3;
        double itemCount = 395;
        int totalPage;
        string searchTitle = "chicken";
        //string urlprefix = "../recipes/";
        string urlprefix = "../";
        string tipsurlprefix = "";
        string currentTag = null;
        string displayableItems = null;
        private readonly ICookBookService _cookbookservice;
        private readonly IShoppingListService _shoppinglistservice;
        private readonly ISpecialShoppingListService _specialshoppinglistservice;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerContext;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IStateProvinceService _stateproviceservice;
        private readonly IEncryptionService _encryptedService;
        private readonly ICustomerRegistrationService _customerRegistrationService;
        private readonly IAddressService _addressService;
        private readonly CustomerSettings _customerSettings;
        private readonly IShoppinglistOwnItemsService _shoppinglistownitemsService;
        private readonly IRecipeCalenderService _recipeCalenderService;
        private readonly ITopicService _topicService;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _looger;
        private readonly ISettingService _settingContext;

        RecipeResults objrecipe = new RecipeResults();

        double SpecialpageSize = 9;
        int specialpagerCount = 9;
        double specialitemCount = 100;
        int specialtotalPage;
        int specialcurrentPage;
        string specialurlprefix = "";
        string category = "<strong>All</strong>";
        string specialpagination = null;
        string displayableSpecialItems = null;
        string accesstoken = null;
        string chainid = null;
        #endregion Field

        #region columname
        string cTitle;
        string cServe;
        string cTime;
        string cCal;
        string cFat;
        string cCerbs;
        int currentPage;
        string displayableTipsItems = null;
        string tipspagination = null;

        #endregion columname

        #region ctor

        public RecipeNewController(ICookBookService cookbookservice, IShoppingListService shoppinglistservice, ISpecialShoppingListService specialshoppinglistservice, ITopicService topicService,
            IWorkContext workContext, ICustomerService customerContext, StoreInformationSettings storeInformationSettings, IStateProvinceService stateproviceservice,
            IEncryptionService encryptedService, ICustomerRegistrationService customerRegistrationService, IAddressService addressService, CustomerSettings customerSettings,
                        IShoppinglistOwnItemsService shoppinglistownitemsService, IRecipeCalenderService recipeCalenderService, IStoreContext storeContext, ILogger logger,
                        ISettingService settingContext)
        {
            _cookbookservice = cookbookservice;
            _shoppinglistservice = shoppinglistservice;
            _specialshoppinglistservice = specialshoppinglistservice;
            _workContext = workContext;
            _customerContext = customerContext;
            _storeInformationSettings = storeInformationSettings;
            _stateproviceservice = stateproviceservice;
            _encryptedService = encryptedService;
            _customerRegistrationService = customerRegistrationService;
            _addressService = addressService;
            _customerSettings = customerSettings;
            _shoppinglistownitemsService = shoppinglistownitemsService;
            _recipeCalenderService = recipeCalenderService;
            _topicService = topicService;
            _storeContext = storeContext;
            _looger = logger;
            _settingContext = settingContext;
            accesstoken = _settingContext.GetSettingByKey<string>("recipeplugin.storeinformationsettings.chaintoken");
            chainid = _settingContext.GetSettingByKey<string>("storeinformationsettings.chainid");
        }

        #endregion

        #region recipeIndex

        public ActionResult Index()
        {

            //Send our model to the view
            var recipeGalleryList = GetRecipeGallary();

            //  return View("Nop.Plugin.RecipeNew.Views.Recipe.Index", recipeGalleryList);
            return View("Index", recipeGalleryList);
        }

        private List<RecipeGallary> GetRecipeGallary()
        {
            List<RecipeGallary> recipeGallary = new List<RecipeGallary>();
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Appetizers", TagId = "appetizers", Photo = "http://static.procomarketing.com/chrysalis/images/appetizers.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Beverages", TagId = "beverages", Photo = "http://static.procomarketing.com/chrysalis/images/beverages.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Breads", TagId = "breads", Photo = "http://static.procomarketing.com/chrysalis/images/breads.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Breakfast", TagId = "breakfast", Photo = "http://static.procomarketing.com/chrysalis/images/breakfast.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Desserts", TagId = "desserts", Photo = "http://static.procomarketing.com/chrysalis/images/desserts.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Lunch", TagId = "lunch", Photo = "http://static.procomarketing.com/chrysalis/images/lunch.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Main Course", TagId = "main_course", Photo = "http://static.procomarketing.com/chrysalis/images/main_course.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Salads", TagId = "salads", Photo = "http://static.procomarketing.com/chrysalis/images/salads.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Side Dishes", TagId = "side_dishes", Photo = "http://static.procomarketing.com/chrysalis/images/side_dishes.jpg" });
            recipeGallary.Add(new RecipeGallary { RecipeTitle = "Snacks", TagId = "snacks", Photo = "http://static.procomarketing.com/chrysalis/images/snacks.jpg" });
            return recipeGallary;
        }

        #endregion

        #region recipeSearch

        public ActionResult Search(string searchType)
        {
            if (searchType == "ingredient") ViewBag.Ingredients = "checked";
            else ViewBag.Title = "checked";
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.Search");
            return View("Search");
        }

        [HttpPost]
        public ActionResult getautocompleteSearchresults(string title = null, string ingredient = null, string autocomplete = null)
        {


            List<Recipemodel> itemList = new List<Recipemodel>();
            List<RecipeCalenderItem> itemList2 = new List<RecipeCalenderItem>();
            RecipeResults recipes = new RecipeResults();
            recipes.GetSearchedRecipesAutoComplete(null, title, ingredient, accesstoken, Convert.ToInt16( chainid), _cookbookservice);
            DataTable dtrecipes = recipes.RecipeData;

            if (dtrecipes.Rows.Count != 0)
            {
                int i = 0;
                foreach (DataRow row in dtrecipes.Rows)
                {
                    if (autocomplete == "true")
                    {
                        Recipemodel item = new Recipemodel();
                        item.id = row.ItemArray[0].ToString();
                        item.title = row.ItemArray[1].ToString();
                        itemList.Add(item);
                        i++;

                        if (i == 10) break;
                    }
                    else
                    {
                        RecipeCalenderItem item2 = new RecipeCalenderItem();
                        item2.id = int.Parse(row.ItemArray[0].ToString());
                        item2.title = row.ItemArray[1].ToString();
                        item2.Photo = row.ItemArray[3].ToString();
                        item2.url = "/recipesnew/" + int.Parse(row.ItemArray[0].ToString());
                        i++;
                        itemList2.Add(item2);

                    }
                }
            }
            if (autocomplete == "true")
            {
                return Json(itemList);
            }
            else
            {
                return Json(itemList2);
            }
        }


        [HttpPost]
        public ActionResult getRecipeListForCalenderPage(string title = null, string ingredient = null, string page = null)
        {
            List<RecipeCalenderItem> itemList2 = new List<RecipeCalenderItem>();
            int currentPage = page != null ? int.Parse(page) : 1;
            RecipeResults recipes = new RecipeResults();
            recipes.GetSearchedRecipes(currentPage.ToString(), title, ingredient, null, null, null, null, null, accesstoken,Convert.ToInt16( chainid), _cookbookservice);
            DataTable dtrecipes = recipes.RecipeData;


            string previousQurey = "/searchinrecipecalendernew";
            string searchTitle = "";
            string searchType = "";
            if (title != null)
            {
                searchTitle = title;
                searchType = "title";
            }
            else
            {

                searchTitle = ingredient;
                searchType = "ingredient";
            }
            string pager = "";
            if (dtrecipes.Rows.Count != 0)
            {


                double itemCountRecipecalender = double.Parse(recipes.total_records);
                double pageSizeRecipecalender = Math.Ceiling(double.Parse(recipes.total_records) / double.Parse(recipes.total_pages));
                pager = BuildRecipeListCalenderPager(currentPage, itemCountRecipecalender, pageSizeRecipecalender, previousQurey, searchTitle, searchType);


                foreach (DataRow row in dtrecipes.Rows)
                {

                    RecipeCalenderItem item2 = new RecipeCalenderItem();
                    item2.id = int.Parse(row.ItemArray[0].ToString());
                    item2.title = row.ItemArray[1].ToString();
                    item2.Photo = row.ItemArray[3].ToString();
                    item2.url = "/recipesnew/" + int.Parse(row.ItemArray[0].ToString());

                    itemList2.Add(item2);

                }
            }

            // return Json(itemList2);
            return Json(new { arr = itemList2, pagerStr = pager });

        }

        #endregion

        #region show recipe list
        public ActionResult showRecipeList(string page = null, string tags = null, string order = null, string direction = null, string title = null, string ingredient = null, string mobileScroll = null, string autocomplete = null, string recipeCalender = null)
        {
            if (string.IsNullOrEmpty(tags))
            {
                tags = currentTag;
            }
            else
            {
                currentTag = tags;
            }


            RecipeResults recipes = new RecipeResults();
            recipes.GetSearchedRecipes(page, title, ingredient, null, tags, null, order, direction, accesstoken,Convert.ToInt16( chainid), _cookbookservice);
            DataTable dtrecipes = recipes.RecipeData;
            int currentPage = page != null ? int.Parse(page) : 1;
            ViewBag.noResult = null;


            if (dtrecipes.Rows.Count != 0)
            {
                searchTitle = recipes.SearchWith;
                itemCount = recipes.total_records != "" ? double.Parse(recipes.total_records) : 0;
                pageSize = recipes.total_records != "" ? Math.Ceiling(double.Parse(recipes.total_records) / double.Parse(recipes.total_pages)) : 0;
                ViewBag.pager = BuildPager(currentPage);
                BuildColumnSorter(order, direction);
                ViewBag.cTitle = cTitle;
                ViewBag.cServe = cServe;
                ViewBag.cTime = cTime;
                ViewBag.cCal = cCal;
                ViewBag.cFat = cFat;
                ViewBag.cCerbs = cCerbs;
                ViewBag.displayableItems = displayableItems;
                ViewBag.tags = tags;
                ViewBag.totalpage = recipes.total_pages;
                ViewBag.title = title;
                ViewBag.ingredient = ingredient;

            }
            else
            {
                ViewBag.noResult = "No recipes were found.";

                if (!string.IsNullOrEmpty(title))
                {
                    ViewBag.displayableItems = "No entries found containing <strong>'" + title + "'</strong>.";
                }
                else
                {
                    ViewBag.displayableItems = "No entries found containing <strong>'" + ingredient + "'</strong>.";
                }
            }
            ViewBag.searchType = "title";
            if (!string.IsNullOrEmpty(ingredient))
            {
                ViewBag.searchType = "ingredient";
            }
            // return View("Nop.Plugin.RecipeNew.Views.Recipe.showRecipeList", dtrecipes);
            if (recipeCalender == "true")
            {
                List<Recipemodel> itemList = new List<Recipemodel>();
                if (dtrecipes.Rows.Count != 0)
                {
                    int i = 0;
                    foreach (DataRow row in dtrecipes.Rows)
                    {
                        Recipemodel item = new Recipemodel();
                        item.id = row.ItemArray[0].ToString();
                        item.title = row.ItemArray[1].ToString();
                        itemList.Add(item);
                        i++;
                        if (i == 10) break;
                    }
                }
                return Json(itemList);
            }
            else
            {
                return View("showRecipeList", dtrecipes);
            }

        }

        public string showrecipelist_mobile(string page = null, string tags = null, string title = null, string ingredient = null, string mobileScroll = null)
        {
            if (string.IsNullOrEmpty(tags))
            {
                tags = currentTag;
            }
            else
            {
                currentTag = tags;
            }


            RecipeResults recipes = new RecipeResults();
            recipes.GetSearchedRecipes(page, title, ingredient, null, tags, null, null, null, accesstoken,Convert.ToInt16( chainid), _cookbookservice);
            DataTable dtrecipes = recipes.RecipeData;
            int currentPage = page != null ? int.Parse(page) : 1;
            ViewBag.noResult = null;
            string RecipelistMobile = null;
            if (!string.IsNullOrEmpty(mobileScroll))
            {

                foreach (DataRow row in dtrecipes.Rows)
                {

                    RecipelistMobile += "<li  data-theme='c' class='ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-li-has-thumb ui-btn-up-c'><div class='ui-btn-inner ui-li'><div class='ui-btn-text'><a href='/recipesnew/" + row.ItemArray[0] + "' class='ui-link-inherit'><img class='ui-li-thumb' src='" + row.ItemArray[3] + "' /> " + row.ItemArray[1] + "<div class='newLine'></div>Serve:" + row.ItemArray[2] + ",Time:" + row.ItemArray[6] + ",Cal:" + row.ItemArray[7] + ",Fat:" + row.ItemArray[8] + ",Cerbs:" + row.ItemArray[11] + "</a></div><span class='ui-icon ui-icon-arrow-r ui-icon-shadow'></span></div></li>";
                }

            }

            return RecipelistMobile;
        }

        private void BuildColumnSorter(string order, string direction)
        {
            string takenUrl = string.Empty;
            if (Request.RawUrl.Contains("&order"))
            {
                takenUrl = (string.IsNullOrEmpty(order)) ? Request.RawUrl : Request.RawUrl.Substring(0, Request.RawUrl.LastIndexOf("&order"));
            }
            else if (Request.RawUrl.Contains("?order"))
            {
                takenUrl = (string.IsNullOrEmpty(order)) ? Request.RawUrl : Request.RawUrl.Substring(0, Request.RawUrl.LastIndexOf("?order"));
            }
            else
            {
                takenUrl = Request.RawUrl;//: Request.RawUrl.Substring(0, Request.RawUrl.LastIndexOf("&order"));
            }
            string antidirection = (string.IsNullOrEmpty(direction) || direction == "desc") ? "asc" : "desc";
            if (takenUrl.Contains("?"))
            {
                takenUrl += "&";
            }
            else
            {
                takenUrl += "?";
            }

            var cTitleLink = (order == "title") ? takenUrl + "order=" + order + "&direction=" + antidirection + "" : takenUrl + "order=title&direction=asc";
            cTitle = "<a href='" + cTitleLink.ToString() + "'>Title</a>";

            var cServeLink = (order == "servings") ? takenUrl + "order=" + order + "&direction=" + antidirection + "" : takenUrl + "order=servings&direction=asc";
            cServe = "<a href='" + cServeLink.ToString() + "'>Serve</a>";

            var cTimeLink = (order == "total_time") ? takenUrl + "order=" + order + "&direction=" + antidirection + "" : takenUrl + "order=total_time&direction=asc";
            cTime = "<a href='" + cTimeLink.ToString() + "'>Time</a>";

            var cCalLink = (order == "calories") ? takenUrl + "order=" + order + "&direction=" + antidirection + "" : takenUrl + "order=calories&direction=asc";
            cCal = "<a href='" + cCalLink.ToString() + "'>Cal.</a>";

            var cFatLink = (order == "fat") ? takenUrl + "order=" + order + "&direction=" + antidirection + "" : takenUrl + "order=fat&direction=asc";
            cFat = "<a href='" + cFatLink.ToString() + "'>Fat(g)</a>";

            var cCerbsLink = (order == "carbs") ? takenUrl + "order=" + order + "&direction=" + antidirection + "" : takenUrl + "order=carbs&direction=asc";
            cCerbs = "<a href='" + cCerbsLink.ToString() + "'>Carbs(g)</a>";

        }

        private string BuildPager(int currentPage)
        {
            string previousQurey = string.Empty;
            string lastStr = string.Empty;
            // previousQurey = "&tags=" + currentTag;
            Regex re = new Regex(@"\d+");
            Match m = re.Match(Request.Url.Query);
            if (m.Success && Request.Url.Query.Contains("?page="))
            {
                previousQurey = Request.Url.ToString();
                var pagestring = "?page=";
                var ind = previousQurey.LastIndexOf(pagestring);
                if (previousQurey.Contains("&"))
                {
                    var laststringind = previousQurey.IndexOf("&");
                    lastStr = previousQurey.Substring(laststringind);
                }
                previousQurey = previousQurey.Substring(0, ind);
            }
            else if (m.Success && Request.Url.Query.Contains("&page="))
            {
                previousQurey = Request.Url.ToString();
                var pagestring = "&page=";
                var ind = previousQurey.LastIndexOf(pagestring);

                previousQurey = previousQurey.Substring(0, ind);
            }
            else
            {
                previousQurey = "&" + Request.Url.Query.Replace("?", string.Empty);
                if (previousQurey == "&")
                {
                    previousQurey = Request.Url.ToString();
                    //var recipestring = "/recipes/";
                    //var ind = previousQurey.LastIndexOf(recipestring);

                    //previousQurey = previousQurey.Substring(ind + recipestring.Length - 1);
                }
                else
                {

                    var laststringind = previousQurey.IndexOf("&");
                    lastStr = previousQurey.Substring(laststringind);
                    previousQurey = previousQurey.Substring(0, laststringind);
                }
            }
            totalPage = (int)Math.Ceiling(itemCount / pageSize);
            StringBuilder pagerHtml = new StringBuilder();
            if (currentPage - 1 == 0) pagerHtml.Append("<span class='disabled prev_page'>&lt;&lt;Previous</span>");
            else pagerHtml.Append("<a href='" + previousQurey + "?page=" + (currentPage - 1) + lastStr + "'>&lt;&lt;Previous</a>");
            // else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + (currentPage - 1) + "'>&lt;&lt;Previous</a>");
            for (int i = 1; i <= totalPage; i++)
            {
                if (currentPage >= pagerCount && currentPage <= (totalPage - pagerCount))
                {
                    //<<Previous12 ... 5678910111213 ... 3940Next>>
                    if (i == 3 || i == totalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if ((i > 2 && i < (currentPage - (pagerCount / 2))) || (i < totalPage - 1 && i > (currentPage + (pagerCount / 2)))) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + previousQurey + "?page=" + i + lastStr + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");

                }
                else if (currentPage > totalPage - pagerCount)
                {
                    //<<Previous12 ... 31323334353637383940Next>>
                    if (i == 3 && totalPage > pagerCount) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i > 2 && i < totalPage - pagerCount) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + previousQurey + "?page=" + i + lastStr + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");
                }
                else
                {
                    //<<Previous123456789 ... 3940Next>>
                    if (i == totalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i < totalPage - 1 && i > pagerCount) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + previousQurey + "?page=" + i + lastStr + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");
                }
            }
            if (currentPage == totalPage) pagerHtml.Append("<span class='disabled next_page'>Next&gt;&gt;</span>");
            else pagerHtml.Append("<a href='" + previousQurey + "?page=" + (currentPage + 1) + lastStr + "'>Next&gt;&gt;</a>");
            //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + (currentPage + 1) + "'>Next&gt;&gt;</a>");
            int startIndex = (int)(currentPage * pageSize - pageSize) + 1;
            int endIndex = Math.Min((int)(currentPage * pageSize), (int)itemCount);
            displayableItems = "Displaying recipes <b>" + startIndex + " - " + endIndex + "</b> of  <b>" + itemCount + "</b> in total with " + searchTitle;

            return pagerHtml.ToString();

        }

        #endregion

        #region single recipe page
        public ActionResult showSingleRecipe(string recipeid = null, string servings = null, string units = null, string commit = null)
        {
            string RecipeTitle = null;
            var noresult = 0;
            if (string.IsNullOrEmpty(servings))
            {
                try
                {
                    //string recipeurl = @"http://69.65.19.37:8023/api/recipe/" + recipeid + "?type=xml";
                    //string recipeurl = @"http://3nerds.services.procomarketing.com/recipes/" + recipeid; 
                    string recipeurl = @"http://69.65.19.37:8023/api/recipe/" + recipeid + "?type=xml&chainid=" + chainid + "";

                    //byte[] recipeTrack = _cookbookservice.GetResponseFromUrl(accesstoken, recipetrackurl);
                    //MemoryStream recipeTrackStream = new MemoryStream(recipeTrack);


                    byte[] singleRecipe = _cookbookservice.GetResponseFromUrl(accesstoken, recipeurl);
                    MemoryStream recipeStream = new MemoryStream(singleRecipe);

                    XElement xRecipe = XElement.Load(recipeStream);
                    if (xRecipe != null)
                    {
                        servings = (xRecipe.Element("servings") == null) ? string.Empty : xRecipe.Element("servings").Value;
                    }
                }
                catch { noresult = 1; }
            }
            if (string.IsNullOrEmpty(units))
            {
                units = "US";
            }

            RecipeResults objrecipe = new RecipeResults();
            try
            {
                objrecipe.GetSingleRecipe(recipeid, servings, units, commit, _cookbookservice, accesstoken);
                ViewBag.recipe_title = RecipeTitle = objrecipe.Title;
                ViewData["recipe_title"] = objrecipe.Title;
                if (objrecipe.PhotoUrl == string.Empty || objrecipe.PhotoUrl.EndsWith("NULL"))
                {
                    objrecipe.PhotoUrl = "http://images.api.procomarketing.com/RecipeImages/recipe_default.jpg";
                }
                ViewBag.img_recipe_photo = objrecipe.PhotoUrl;

                ViewBag.recipe_instructions = objrecipe.Instructions.Replace("'e", "&#233;");
                ViewBag.recipe_instructions = objrecipe.Instructions.Replace(@"\", "");
                ViewBag.servings = servings;
                ViewBag.recipeId = recipeid;
                ViewBag.dontshowaddcookbook = false;
                ViewBag.dontshowaddshoppinglist = false;
                ViewBag.dontshowviewshoppinglist = false;
                ViewBag.totalshoppinglistitems = 0;
                ViewBag.unregistered = false;
                if (_workContext.CurrentCustomer.IsRegistered())
                {

                    int customerId = _workContext.CurrentCustomer.Id;
                    int recipeId = int.Parse(recipeid);
                    if (_cookbookservice.GetCookbookId(customerId, recipeId) != -1)
                    {
                        ViewBag.dontshowaddcookbook = true;
                    }
                    if (_shoppinglistservice.GetShoppingListId(customerId, recipeId) != -1)
                    {
                        ViewBag.dontshowaddshoppinglist = true;
                    }

                    IList<RecipeShoppingList> shoppinglist = _shoppinglistservice.GetAllShoppingListsByCustomerID(customerId);
                    if (shoppinglist.Count == 0)
                    {
                        ViewBag.dontshowviewshoppinglist = true;
                    }
                    ViewBag.totalshoppinglistitems = shoppinglist.Count;
                }
                else
                {
                    ViewBag.dontshowaddcookbook = true;
                    ViewBag.dontshowaddshoppinglist = true;
                    ViewBag.dontshowviewshoppinglist = true;
                    ViewBag.unregistered = true;
                }
            }
            catch
            {
                noresult = 1;
            }
            ViewBag.noresult = "";
            if (noresult == 1)
            {
                objrecipe = null;
                ViewBag.noresult = "No recipes were found.";
                ViewBag.dontshowaddcookbook = true;
                ViewBag.dontshowaddshoppinglist = true;
                ViewBag.dontshowviewshoppinglist = true;
                ViewBag.unregistered = true;
                if (_workContext.CurrentCustomer.IsRegistered())
                {
                    ViewBag.unregistered = false;
                }
            }
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.showSingleRecipe", objrecipe);
            return View("showSingleRecipe", objrecipe);
        }


        public string getRecipeTitleById(string recipeid)
        {

            RecipeResults objrecipe = new RecipeResults();
            try
            {
                objrecipe.GetSingleRecipe(recipeid, null, null, null, _cookbookservice, accesstoken);

                return objrecipe.Title;
            }
            catch
            {
                return recipeid;
            }
        }

        #endregion

        #region cookbook

        public ActionResult showAllCookBook()
        {


            if (_workContext.CurrentCustomer.IsRegistered())
            {
                int customerID = _workContext.CurrentCustomer.Id;


                IList<CookBook> cookbooks = _cookbookservice.GetAllCookbooksByCustomerID(customerID);
                //if (cookbooks.Count > 0) this.empty_msg.Attributes["style"] = "display:none";

                //return View("Nop.Plugin.RecipeNew.Views.Recipe.CookBook", cookbooks);
                return View("CookBook", cookbooks);
            }
            else
            {
                IList<CookBook> re = null;
                // return View("Nop.Plugin.RecipeNew.Views.Recipe.CookBook", re);
                return View("CookBook", re);

            }
            // cookbook_recipes.InnerHtml = sb.ToString();
        }

        public ActionResult AddTocookBook(int recipeid, string recipetitle)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {

                CookBook cookbook = new CookBook();
                cookbook.CustomerID = _workContext.CurrentCustomer.Id;
                cookbook.RecipeID = recipeid;
                cookbook.RecipeTitle = recipetitle;
                _cookbookservice.InsertCookBook(cookbook);
                showSingleRecipe(recipeid.ToString());

            }
            return RedirectToAction("showSingleRecipe", "RecipeNew", new { recipeid = recipeid.ToString() });
        }

        [HttpPost]
        public ActionResult AddTocookBook_mobile(int recipeid, string recipetitle)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                CookBook cookbook = new CookBook();
                cookbook.CustomerID = _workContext.CurrentCustomer.Id;
                cookbook.RecipeID = recipeid;
                cookbook.RecipeTitle = recipetitle;
                _cookbookservice.InsertCookBook(cookbook);
            }

            return Json(new { added = true }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteRecipeFromCookBook(int recipeid)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {


                int cookbookId = _cookbookservice.GetCookbookId(_workContext.CurrentCustomer.Id, recipeid);

                _cookbookservice.DeleteCookBookItem(cookbookId);
                showSingleRecipe(recipeid.ToString());

            }
            return Json(new { delete = true }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("showSingleRecipe", "Recipe", new { recipeid = recipeid.ToString() });
        }


        public ActionResult DeleteRecipeFromCookBookForMobile(int recipeid)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {


                int cookbookId = _cookbookservice.GetCookbookId(_workContext.CurrentCustomer.Id, recipeid);

                _cookbookservice.DeleteCookBookItem(cookbookId);
                showSingleRecipe(recipeid.ToString());

            }

            return RedirectToAction("showSingleRecipe", "RecipeNew", new { recipeid = recipeid.ToString() });
        }

        #endregion

        #region shoppingList

        public ActionResult ShoppingList()
        {
            ViewBag.showIstable = true;
            ViewBag.isRegistered = false;

            if (_workContext.CurrentCustomer.IsRegistered())
            {
                ViewBag.isRegistered = true;
                DataSet aset = getshoppinglistdataset();
                if (aset == null)
                {
                    ViewBag.showIstable = false;
                }
                //return View("Nop.Plugin.RecipeNew.Views.Recipe.ShoppingList", aset);
                return View("ShoppingList", aset);

            }
            else
            {
                ViewBag.showIstable = false;
                DataSet dset = null;
                // return View("Nop.Plugin.RecipeNew.Views.Recipe.ShoppingList", dset);
                return View("ShoppingList", dset);
            }

        }

        public ActionResult ShoppingListUserItems()
        {
            int customerId = _workContext.CurrentCustomer.Id;
            IList<ShoppinglistOwnItems> shoppinglistUserItems = _shoppinglistownitemsService.GetAllshoppinglistuserownitemsByCustomerID(customerId);
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.ShoppingListUserItems", shoppinglistUserItems);
            return View("ShoppingListUserItems", shoppinglistUserItems);
        }

        [HttpPost]
        public string addtoShoppingListUserItems(string itemTitle)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                ShoppinglistOwnItems userItem = new ShoppinglistOwnItems();
                userItem.CustomerID = _workContext.CurrentCustomer.Id;
                userItem.ItemTitle = itemTitle;
                _shoppinglistownitemsService.InsertShoppinglistOwnItems(userItem);
            }
            string alluseritems = getAllShoppinglistUserItems();

            return alluseritems;
        }

        [HttpPost]
        public string deleteShoppingListUserItems(int userItemId)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                ShoppinglistOwnItems userItem = _shoppinglistownitemsService.GetshoppinglistuseritemById(userItemId);

                _shoppinglistownitemsService.DeleteshoppinglistOwnItem(userItem);
            }
            string alluseritems = getAllShoppinglistUserItems();

            return alluseritems;
        }

        private string getAllShoppinglistUserItems()
        {
            int customerId = _workContext.CurrentCustomer.Id;
            IList<ShoppinglistOwnItems> shoppinglistUserItems = _shoppinglistownitemsService.GetAllshoppinglistuserownitemsByCustomerID(customerId);
            string userItemList = "";
            if (shoppinglistUserItems.Count != 0)
            {
                userItemList += "<h2>Other Items</h2>";
            }
            for (int i = 0; i < shoppinglistUserItems.Count; i++)
            {

                var item = shoppinglistUserItems[i];
                userItemList += "<div><button id='delete_" + item.Id + "' class='deleteIconshoppinglistuseritem btn-danger' onclick='deleteshoppinglistuseritem(" + item.Id + ")'>Remove</button><a>" + item.ItemTitle + "</a></div>";

            }

            return userItemList;


        }

        private DataSet getshoppinglistdataset()
        {
            int customerId = _workContext.CurrentCustomer.Id;
            IList<RecipeShoppingList> shoppinglist = _shoppinglistservice.GetAllShoppingListsByCustomerID(customerId);
            string recipe_ids = string.Empty;
            if (shoppinglist.Count != 0)
            {
                recipe_ids = GetRecipeIds(shoppinglist);
            }
            IList<SpecialShoppingList> specialList = _specialshoppinglistservice.GetAllSpecialShoppingListsByCustomerID(customerId);
            string qs_special_ids = GetQsSpecialShoppingList(specialList);

            //if (string.IsNullOrEmpty(recipe_ids) && string.IsNullOrEmpty(qs_special_ids)) return View("Nop.Plugin.RecipeNew.Views.Recipe.ShoppingList");

            ICollection<CustomerRole> customerRolesCollection = _workContext.CurrentCustomer.CustomerRoles;
            string storeID = "";

            //IList iList = customerRolesCollection as IList;

            foreach (CustomerRole i in customerRolesCollection)
            {
                storeID = i.StoreId;
                if (storeID != null)
                {
                    break;
                }
            }
            RecipeResults recipeResult = new RecipeResults();
            recipeResult.GetShoppingListInfo(storeID, recipe_ids, qs_special_ids, chainid, _cookbookservice, accesstoken);
            DataSet aset = recipeResult.AislesSet;
            return aset;
            //}
            //else
            //{
            //    DataSet dset = null;
            //    return dset;
            //}

        }

        public ActionResult RecipesOnMyShoppingList()
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                DataTable ShoppingListTable = CreateShoppingListTable();
                int customerId = _workContext.CurrentCustomer.Id;
                IList<RecipeShoppingList> shoppinglist = _shoppinglistservice.GetAllShoppingListsByCustomerID(customerId);

                foreach (var sl in shoppinglist)
                {
                    RecipeResults recipe = new RecipeResults();
                    if (recipe.GetRecipeInfoForShoppingList(sl.RecipeID.ToString(), sl.Servings.ToString()) == 1)
                    {
                        ShoppingListTable.Rows.Add(new object[] { 
                        recipe.RecipeId,        
                        recipe.Title,
                        sl.Servings.ToString(),
                        recipe.Preparation,
                        recipe.Cooking,
                        recipe.Calories,
                        recipe.Fat,
                        recipe.Carbs,
                        recipe.photoU
                      });
                    }
                }
                //return View("Nop.Plugin.RecipeNew.Views.Recipe.RecipesOnMyShoppingList", ShoppingListTable);
                return View("RecipesOnMyShoppingList", ShoppingListTable);
            }
            else
            {
                // return View("Nop.Plugin.RecipeNew.Views.Recipe.RecipesOnMyShoppingList");
                return View("RecipesOnMyShoppingList");
            }

        }

        public ActionResult AddToshoppingList(int recipeid, string servings)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                RecipeShoppingList shoppingList = new RecipeShoppingList();
                shoppingList.CustomerID = _workContext.CurrentCustomer.Id;
                shoppingList.RecipeID = recipeid;
                shoppingList.Servings = int.Parse(servings);
                shoppingList.TimeStampUtc = DateTime.UtcNow;

                _shoppinglistservice.InserttoShoppingList(shoppingList);
                showSingleRecipe(recipeid.ToString());

            }
            return RedirectToAction("showSingleRecipe", "RecipeNew", new { recipeid = recipeid.ToString() });
        }

        [HttpPost]
        public ActionResult AddToshoppingList_mobile(int recipeid, string servings)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                RecipeShoppingList shoppingList = new RecipeShoppingList();
                shoppingList.CustomerID = _workContext.CurrentCustomer.Id;
                shoppingList.RecipeID = recipeid;
                shoppingList.Servings = int.Parse(servings);

                _shoppinglistservice.InserttoShoppingList(shoppingList);


            }
            return Json(new { added = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string DeleteRecipeFromshoppinglist(int recipeid, string mobileview)
        {
            string aisletablehtml = null;
            if (_workContext.CurrentCustomer.IsRegistered())
            {

                int shoppinglistid = _shoppinglistservice.GetShoppingListId(_workContext.CurrentCustomer.Id, recipeid);
                _shoppinglistservice.DeleteshoppinglistItem(shoppinglistid);
                try
                {
                    DataSet aset = getshoppinglistdataset();
                    aisletablehtml = getAisleTableHtml(aset, mobileview);

                }
                catch
                {
                    aisletablehtml = "";
                }


            }
            return aisletablehtml;
            // return RedirectToAction("showSingleRecipe", "Recipe", new { recipeid = recipeid.ToString() });
        }


        public ActionResult DeleteAllRecipeFromshoppinglistNew()
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {

                var shoppingListItems = _shoppinglistservice.GetAllShoppingListsByCustomerID(_workContext.CurrentCustomer.Id);
                var specialShoppingListItems = _specialshoppinglistservice.GetCustomerShoppingListItem(_workContext.CurrentCustomer.Id);

                foreach (var shoppingListItem in shoppingListItems)
                {
                    _shoppinglistservice.DeleteshoppinglistItem(shoppingListItem.Id);
                }

                foreach (var specialShoppingListItem in specialShoppingListItems)
                {
                    _specialshoppinglistservice.DeletespecialshoppinglistItem(specialShoppingListItem);
                }

            }
            return RedirectToAction("ShoppingList");
        }
        public ActionResult DeleteRecipeFromshoppinglistMobile(int recipeid)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {

                int shoppinglistid = _shoppinglistservice.GetShoppingListId(_workContext.CurrentCustomer.Id, recipeid);

                _shoppinglistservice.DeleteshoppinglistItem(shoppinglistid);

                //showSingleRecipe(recipeid.ToString());

            }
            //return Json(new { delete = true }, JsonRequestBehavior.AllowGet);
            return RedirectToAction("showSingleRecipe", "RecipeNew", new { recipeid = recipeid.ToString() });
        }

        public string DeleteSpecialsFromshoppinglist(int specialid, string mobileview)
        {
            string aisletablehtml = null;
            if (_workContext.CurrentCustomer.IsRegistered())
            {

                SpecialShoppingList specialshoppinglist = _specialshoppinglistservice.GetSpecialShoppingList(_workContext.CurrentCustomer.Id, specialid);

                _specialshoppinglistservice.DeletespecialshoppinglistItem(specialshoppinglist);
                try
                {
                    DataSet aset = getshoppinglistdataset();
                    aisletablehtml = getAisleTableHtml(aset, mobileview);


                }
                catch
                {
                    aisletablehtml = "";
                }
                //showSingleRecipe(recipeid.ToString());

            }
            return aisletablehtml;
            //return Json(new { delete = true }, JsonRequestBehavior.AllowGet);

        }

        public string getAisleTableHtml(DataSet aset, string mobileview)
        {
            string aisletablehtml = null;
            for (int i = 0; i < aset.Tables.Count; i++)
            {
                if (mobileview == "false")
                {
                    aisletablehtml += "<div id='divParent' class='shoppinglist-column'><h2>" + aset.Tables[i].TableName + "</h2><div><ul id='ul_specials_normal_view'>";
                }
                else
                {

                    aisletablehtml += "<div id='divParent'><h2>" + aset.Tables[i].TableName + "</h2><div><ul id='ul_specials_normal_view'>";
                }
                for (int j = 0; j < aset.Tables[i].Rows.Count; j++)
                {
                    if (!string.IsNullOrEmpty(aset.Tables[i].Rows[j]["recipe_ids"].ToString()))
                    {
                        aisletablehtml += "<li id='liNormal'>" + aset.Tables[i].Rows[j]["value"].ToString() + "</li>";
                    }
                    else
                    {
                        var specialId = aset.Tables[i].Rows[j]["special_id"];
                        var mobile = "\"" + mobileview + "\"";
                        if (mobileview == "false")
                        {
                            aisletablehtml += "<li id='li_" + aset.Tables[i].Rows[j]["special_id"] + "' class='color_green'> <button id='delete_" + aset.Tables[i].Rows[j]["special_id"] + "' class='deleteIconSpecial btn-danger'  onclick='deletespecial(" + specialId + "," + mobile + ")'>Remove</button>Special: " + aset.Tables[i].Rows[j]["value"].ToString() + "<span class='color_red'>Offer: " + aset.Tables[i].Rows[j]["Offer"].ToString() + "</span></li>  ";
                        }
                        else
                        {
                            aisletablehtml += "<li id='li_" + specialId + "' class='color_green'>Special: " + aset.Tables[i].Rows[j]["value"].ToString() + "<span class='color_red'>Offer: " + aset.Tables[i].Rows[j]["Offer"].ToString() + "</span><a href='javascript:deletespecial(" + specialId + "," + mobile + ")'  class='ui-link'><div class='deletespecial'></div></a></li>";
                        }
                    }
                }
                aisletablehtml += "</ul></div></div>";

            }

            return aisletablehtml;
        }

        private string GetRecipeIds(IList<RecipeShoppingList> shoppinglist)
        {
            string recipeids = string.Empty;
            foreach (var item in shoppinglist)
            {
                recipeids += item.RecipeID + ",";
            }
            if (recipeids == string.Empty) return string.Empty;
            recipeids = recipeids.Substring(0, recipeids.LastIndexOf(','));
            return recipeids;
        }

        private DataTable CreateShoppingListTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RecipeId");
            dt.Columns.Add("RecipeTitle");
            dt.Columns.Add("Servings");
            dt.Columns.Add("PrepTime");
            dt.Columns.Add("CookTime");
            dt.Columns.Add("Calories");
            dt.Columns.Add("Fat");
            dt.Columns.Add("Carbs");
            dt.Columns.Add("PhotoU");
            return dt;
        }

        private void CreateShoppingList(DataSet aisleSet)
        {
            throw new NotImplementedException();
        }

        private string GetQsSpecialShoppingList(IList<SpecialShoppingList> specialList)
        {
            try
            {
                //&chain_id=2002&special_ids[]=1448&special_ids[]=1449";
                string qs = string.Empty;
                for (int i = 0; i < specialList.Count; i++)
                {
                    qs += specialList[i].SpecialId.ToString() + ",";
                }
                return qs;
            }
            catch (WebException ex)
            {
                return string.Empty;
            }
        }

        #endregion

        #region specials

        public ActionResult specials(string department_id = null, string search = null, string page = null, string pagesize = null, string storeName = null)
        {
            
            SpecialResults specialresults = new SpecialResults();
            SpecialsModel specials = new SpecialsModel();
            ViewBag.noresult = false;
            ViewBag.unregistered = false;

            var currentCustomerRoleName = _workContext.CurrentCustomer.CustomerRoles.Where(cr => !cr.SystemName.Equals(SystemCustomerRoleNames.Registered) && !cr.SystemName.Equals(SystemCustomerRoleNames.Administrators)).FirstOrDefault().Name;
            storeName = string.IsNullOrEmpty(storeName) ? currentCustomerRoleName : storeName;
            //_customerContext.GetCustomerRoleBySystemName(_workContext.CurrentCustomer.SystemName);
            ViewBag.StoreName = storeName;
            ViewBag.DontLiveNearStore = storeName.Equals("I do not live near a store", StringComparison.Ordinal) || _workContext.CurrentCustomer.IsGuest() ? _topicService.GetTopicBySystemName("Specials-Store-Selector").Body : "";


            List<SelectListItem> customerRoles = _customerContext.GetAllCustomerRoles(true).Where(sr => !string.IsNullOrEmpty(sr.StoreRole) && sr.StoreRole.Equals("True") && !sr.Name.Equals("I do not live near a store"))
                                                                                                                                                             .Select(x => new SelectListItem
                                                                                                                                                             {
                                                                                                                                                                 Text = x.Name,
                                                                                                                                                                 Value = x.AdGroupId
                                                                                                                                                                }).ToList();
            ViewBag.RoleName = new SelectList(customerRoles, "Value", "Text", AdGroupId);

            if (pagesize != null)
            {
                if (pagesize != "All")
                {
                    SpecialpageSize = int.Parse(pagesize);
                }

            }
            if (string.IsNullOrEmpty(department_id))
            {
                specials = specialresults.GetDepartmentsByServiceUrl(GetSpecialLandingServiceUrl(), accesstoken, _cookbookservice);
                ViewBag.noresult = false;
                if (specials != null)
                {
                    //return View("Nop.Plugin.RecipeNew.Views.Recipe.specialsLanding", specials.Departments);
                    return View("specialsLanding", specials.Departments);
                }
                else
                {
                    IList dep = null;
                    // return View("Nop.Plugin.RecipeNew.Views.Recipe.specialsLanding", dep);
                    return View("specialsLanding", dep);

                }
            }
            else
            {
                ViewBag.pagesize = SpecialpageSize.ToString();
                string serviceurl = GetspecialServiceUrl(department_id, search, page);
                specials = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                ViewBag.search = search;
                if (specials != null)
                {
                    if (pagesize == "All")
                    {
                        SpecialpageSize = int.Parse(specials.Pagination.total_records);
                        ViewBag.pagesize = "All";
                        serviceurl = GetspecialServiceUrl(department_id, search, page);
                        specials = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                    }
                    LoadDropdownListSpecials_select_category();
                    ViewBag.totalpage = specials.Pagination.total_pages;

                    SetspecialPagerInfos(specials, page);
                    string department_name = null;
                    for (int i = 0; i < specials.Departments.Count; i++)
                    {
                        if (specials.Departments[i].id == department_id)
                        {
                            department_name = specials.Departments[i].name;
                        }
                    }
                    if (string.IsNullOrEmpty(department_name))
                    {
                        department_name = search;
                    }
                    BuildSpecialPager(specialcurrentPage, department_name);
                    ViewBag.displayableSpecialItems = displayableSpecialItems;
                    ViewBag.department_name = department_name;
                    ViewBag.specialpagination = specialpagination;
                    ViewBag.department_id = department_id;

                    ViewBag.dontshowaddshoppinglist = false;
                    ViewBag.dontshowviewshoppinglist = false;
                    if (_workContext.CurrentCustomer.IsRegistered())
                    {

                        for (int i = 0; i < specials.Specials.Count; i++)
                        {
                            SpecialShoppingList specialShoppingList = _specialshoppinglistservice.GetSpecialShoppingList(_workContext.CurrentCustomer.Id, int.Parse(specials.Specials[i].id));
                            if (specialShoppingList != null)
                            {
                                specials.Specials[i].isAlreadyOnDatabase = "true";
                            }
                            else
                            {
                                specials.Specials[i].isAlreadyOnDatabase = "false";
                            }
                        }

                        ViewBag.dontshowaddshoppinglist = false;
                        ViewBag.dontshowviewshoppinglist = false;
                    }
                    else
                    {
                        ViewBag.dontshowaddshoppinglist = true;
                        ViewBag.dontshowviewshoppinglist = true;
                        ViewBag.unregistered = true;
                    }
                    ViewBag.noresult = false;
                    // return View("Nop.Plugin.RecipeNew.Views.Recipe.specials", specials);
                    return View("specials", specials);
                }
                else
                {
                    specials = new SpecialsModel();
                    specials.Departments = new List<Department>();
                    //specials.Departments = GetDefaultCategories();
                    //specials.Specials = new Special() ;
                    ViewBag.displayableSpecialItems = "No entries found containing <strong>'" + search + "'</strong>.";
                    ViewBag.specialpagination = null;

                    ViewBag.department_id = department_id;
                    ViewBag.totalpage = null;
                    ViewBag.noresult = true;
                    ViewBag.dontshowaddshoppinglist = false;
                    ViewBag.dontshowviewshoppinglist = false;
                    //return View("Nop.Plugin.RecipeNew.Views.Recipe.specials", specials);
                    return View("specials", specials);
                }

            }

        }

        public ActionResult SpecialsWithAdGroup(string department_id = null, string search = null, string page = null, string pagesize = null, string storeName = null)
        {
            SpecialResults specialresults = new SpecialResults();
            SpecialsModel specials = new SpecialsModel();
            ViewBag.noresult = false;
            ViewBag.unregistered = false;
            //ViewBag.IsLoggedIn = _workContext.CurrentCustomer.IsRegistered();
            var currentCustomerRoleName = _workContext.CurrentCustomer.CustomerRoles.Where(cr => !cr.SystemName.Equals(SystemCustomerRoleNames.Registered) && !cr.SystemName.Equals(SystemCustomerRoleNames.Administrators)).FirstOrDefault().Name;
            storeName = string.IsNullOrEmpty(storeName) ? currentCustomerRoleName : storeName;
            //_customerContext.GetCustomerRoleBySystemName(_workContext.CurrentCustomer.SystemName);
            ViewBag.StoreName = storeName;
            ViewBag.DontLiveNearStore = storeName.Equals("I do not live near a store", StringComparison.Ordinal) || _workContext.CurrentCustomer.IsGuest() ? _topicService.GetTopicBySystemName("Specials-Store-Selector").Body : "";

            //_workContext.CurrentCustomer.CustomerRoles
            //	List<CustomerRole> role = new List<CustomerRole>();
            //var	currentCustomerId = 	_workContext.CurrentCustomer.Id;

            List<SelectListItem> customerRoles = _customerContext.GetAllCustomerRoles(true).Where(sr => !string.IsNullOrEmpty(sr.StoreRole) && sr.StoreRole.Equals("True") && !sr.Name.Equals("I do not live near a store"))
                                                                                                                                                                         .Select(x => new SelectListItem
                                                                                                                                                                         {
                                                                                                                                                                             Text = x.Name,
                                                                                                                                                                             Value = x.AdGroupId
                                                                                                                                                                         }).ToList();

            AdGroupId = _customerContext.GetAllCustomerRoles(true).Where(x => x.Name.Equals(storeName)).FirstOrDefault().AdGroupId;//customerRoles.Where(x => x.Text.Equals(storeName)).FirstOrDefault().Value.ToString();
            ViewBag.AdGroupId = AdGroupId;

            ViewBag.RoleName = new SelectList(customerRoles, "Value", "Text", AdGroupId);

            if (pagesize != null)
            {
                if (pagesize != "All")
                {
                    SpecialpageSize = int.Parse(pagesize);
                }

            }
            if (string.IsNullOrEmpty(department_id))
            {

                specials = specialresults.GetDepartmentsByServiceUrl(GetAdGroupSpecialLandingServiceUrl(), accesstoken, _cookbookservice);
                ViewBag.noresult = false;
                if (specials != null)
                {
                    //return View("Nop.Plugin.RecipeNew.Views.Recipe.specialsLanding", specials.Departments);
                    //if(storeName.Equals("I do not live near a store", StringComparison.Ordinal))
                    return View("specialsLandingWithAdGroup", specials.Departments);
                }
                else
                {
                    IList dep = null;
                    // return View("Nop.Plugin.RecipeNew.Views.Recipe.specialsLanding", dep);
                    return View("specialsLandingWithAdGroup", dep);

                }
            }
            else
            {
                ViewBag.pagesize = SpecialpageSize.ToString();
                string serviceurl = GetAdGroupSpecialServiceUrl(department_id, search, page);
                specials = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                ViewBag.search = search;
                if (specials != null)
                {
                    if (pagesize == "All")
                    {
                        SpecialpageSize = int.Parse(specials.Pagination.total_records);
                        ViewBag.pagesize = "All";
                        serviceurl = GetAdGroupSpecialServiceUrl(department_id, search, page);
                        specials = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                    }
                    LoadDropdownListSpecials_select_category();
                    ViewBag.totalpage = specials.Pagination.total_pages;

                    SetspecialPagerInfos(specials, page);
                    string department_name = null;
                    for (int i = 0; i < specials.Departments.Count; i++)
                    {
                        if (specials.Departments[i].id == department_id)
                        {
                            department_name = specials.Departments[i].name;
                        }
                    }
                    if (string.IsNullOrEmpty(department_name))
                    {
                        department_name = search;
                    }
                    BuildSpecialPager(specialcurrentPage, department_name);
                    ViewBag.displayableSpecialItems = displayableSpecialItems;
                    ViewBag.specialpagination = specialpagination;
                    ViewBag.department_id = department_id;

                    ViewBag.dontshowaddshoppinglist = false;
                    ViewBag.dontshowviewshoppinglist = false;
                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        for (int i = 0; i < specials.Specials.Count; i++)
                        {
                            SpecialShoppingList specialShoppingList = _specialshoppinglistservice.GetSpecialShoppingList(_workContext.CurrentCustomer.Id, int.Parse(specials.Specials[i].id));
                            if (specialShoppingList != null)
                            {
                                specials.Specials[i].isAlreadyOnDatabase = "true";
                            }
                            else
                            {
                                specials.Specials[i].isAlreadyOnDatabase = "false";
                            }
                        }

                        ViewBag.dontshowaddshoppinglist = false;
                        ViewBag.dontshowviewshoppinglist = false;
                    }
                    else
                    {
                        ViewBag.dontshowaddshoppinglist = true;
                        ViewBag.dontshowviewshoppinglist = true;
                        ViewBag.unregistered = true;
                    }
                    ViewBag.noresult = false;
                    // return View("Nop.Plugin.RecipeNew.Views.Recipe.specials", specials);
                    return View("specials", specials);
                }
                else
                {
                    specials = new SpecialsModel();
                    specials.Departments = new List<Department>();
                    //specials.Departments = GetDefaultCategories();
                    //specials.Specials = new Special() ;
                    ViewBag.displayableSpecialItems = "No entries found containing <strong>'" + search + "'</strong>.";
                    ViewBag.specialpagination = null;

                    ViewBag.department_id = department_id;
                    ViewBag.totalpage = null;
                    ViewBag.noresult = true;
                    ViewBag.dontshowaddshoppinglist = false;
                    ViewBag.dontshowviewshoppinglist = false;
                    //return View("Nop.Plugin.RecipeNew.Views.Recipe.specials", specials);
                    return View("specials", specials);
                }

            }

        }
        public ActionResult featured()
        {

            ViewBag.unregistered = !_workContext.CurrentCustomer.IsRegistered();
            ViewBag.dontshowaddshoppinglist = !_workContext.CurrentCustomer.IsRegistered();

            var serviceurl = GetFeaturedServiceUrl("", 100);
            var featuredList = new SpecialsModel();
            SpecialResults specialresults = new SpecialResults();

            featuredList = specialresults.GetFeaturedListByServiceUrl(serviceurl, accesstoken, _cookbookservice);

            for (int j = 0; j < featuredList.Specials.Count; j++)
            {
                SpecialShoppingList specialShoppingList = _specialshoppinglistservice.GetSpecialShoppingList(_workContext.CurrentCustomer.Id, int.Parse(featuredList.Specials[j].id));
                if (specialShoppingList != null)
                {
                    featuredList.Specials[j].isAlreadyOnDatabase = "true";
                }
                else
                {
                    featuredList.Specials[j].isAlreadyOnDatabase = "false";
                }
            }


            ViewBag.noresult = featuredList.Specials.Count.Equals(0);


            return View("featured", featuredList); ;
        }

        public ActionResult FeaturedProductAtHomePage()
        {

            ViewBag.unregistered = !_workContext.CurrentCustomer.IsRegistered();
            ViewBag.dontshowaddshoppinglist = !_workContext.CurrentCustomer.IsRegistered();

            var serviceurl = GetHomePagefeaturedSpecialServiceUrl("1");
            var featuredList = new SpecialsModel();
            SpecialResults specialresults = new SpecialResults();

            featuredList = specialresults.GetFeaturedProductAtHomePageByServiceUrl(serviceurl, accesstoken, _cookbookservice);

            for (int j = 0; j < featuredList.Specials.Count; j++)
            {
                SpecialShoppingList specialShoppingList = _specialshoppinglistservice.GetSpecialShoppingList(_workContext.CurrentCustomer.Id, int.Parse(featuredList.Specials[j].id));
                if (specialShoppingList != null)
                {
                    featuredList.Specials[j].isAlreadyOnDatabase = "true";
                }
                else
                {
                    featuredList.Specials[j].isAlreadyOnDatabase = "false";
                }
            }


            ViewBag.noresult = featuredList.Specials.Count.Equals(0);


            return View("_FeaturedItems", featuredList); ;
        }

        public ActionResult specials_all()
        {
            SpecialResults specialresults = new SpecialResults();
            SpecialsModel specials = new SpecialsModel();
            SpecialsModel specials_first = new SpecialsModel();
            SpecialsModel specials_dep = new SpecialsModel();
            List<Department_With_Special> Department_With_Special_list = new List<Department_With_Special>();
            //ViewBag.noresult = false;
            ViewBag.unregistered = false;
            specials_dep = specialresults.GetDepartmentsByServiceUrl(GetSpecialLandingServiceUrl(), accesstoken, _cookbookservice);

            for (int i = 0; i < specials_dep.Departments.Count; i++)
            {
                var department = specials_dep.Departments[i];
                string serviceurl = GetspecialServiceUrl(department.id, "", "1");
                specials_first = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                var totalPageInSpecial = int.Parse(specials_first.Pagination.total_pages);
                List<Special> specialsList = new List<Special>();
                for (int k = 1; k <= totalPageInSpecial; k++)
                {
                    serviceurl = "";
                    serviceurl = GetspecialServiceUrl(department.id, "", k.ToString());
                    specials = new SpecialsModel();
                    specials = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        for (int j = 0; j < specials.Specials.Count; j++)
                        {
                            //SpecialShoppingList specialShoppingList = _specialshoppinglistservice.GetSpecialShoppingList(_workContext.CurrentCustomer.Id, int.Parse(specials.Specials[j].id));
                            //if (specialShoppingList != null)
                            //{
                            //    specials.Specials[j].isAlreadyOnDatabase = "true";
                            //}
                            //else
                            //{
                            //    specials.Specials[j].isAlreadyOnDatabase = "false";
                            //}
                        }

                        ViewBag.dontshowaddshoppinglist = false;
                        ViewBag.dontshowviewshoppinglist = false;
                    }
                    else
                    {
                        ViewBag.dontshowaddshoppinglist = true;
                        ViewBag.dontshowviewshoppinglist = true;
                        ViewBag.unregistered = true;
                    }

                    for (int j = 0; j < specials.Specials.Count; j++)
                    {
                        specialsList.Add(specials.Specials[j]);
                    }



                }

                Department_With_Special department_with_special = new Department_With_Special();
                department_with_special.department = department;

                department_with_special.Specials = specialsList.OrderBy(o => o.brand).ToList(); ;
                Department_With_Special_list.Add(department_with_special);
            }

            ViewBag.noresult = false;
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.specials", specials);
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.specials_all", Department_With_Special_list);
            return View("specials_all", Department_With_Special_list);
        }

        public ActionResult AdGroupSpecialsAll(string storeName = null)
        {
            SpecialResults specialresults = new SpecialResults();
            SpecialsModel specials = new SpecialsModel();
            SpecialsModel specials_first = new SpecialsModel();
            SpecialsModel specials_dep = new SpecialsModel();
            List<Department_With_Special> Department_With_Special_list = new List<Department_With_Special>();
            //ViewBag.noresult = false;
            ViewBag.unregistered = false;

            storeName = string.IsNullOrEmpty(storeName) ? _customerContext.GetAllCustomerRoles(true).Where(sr => sr.StoreRole.Equals("True")).FirstOrDefault().Name : storeName;
            ViewBag.StoreName = storeName;

            //List<SelectListItem> customerRoles = _customerContext.GetAllCustomerRoles(true).Where(sr => sr.StoreRole.Equals("True"))
            //                                                                               .Select(x => new SelectListItem
            //                                                                               {
            //                                                                                 Text = x.Name,
            //                                                                                 Value = x.AdGroupId
            //                                                                               }).ToList();

            AdGroupId = _customerContext.GetAllCustomerRoles(true).Where(x => x.Name.Equals(storeName)).FirstOrDefault().AdGroupId;
            //ViewBag.AdGroupId = AdGroupId;

            //ViewBag.RoleName = new SelectList(customerRoles, "Value", "Text", AdGroupId);


            specials_dep = specialresults.GetDepartmentsByServiceUrl(GetAdGroupSpecialLandingServiceUrl(), accesstoken, _cookbookservice);

            for (int i = 0; i < specials_dep.Departments.Count; i++)
            {
                var department = specials_dep.Departments[i];
                string serviceurl = GetAdGroupSpecialServiceUrl(department.id, "", "1");
                specials_first = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                var totalPageInSpecial = int.Parse(specials_first.Pagination.total_pages);
                List<Special> specialsList = new List<Special>();
                for (int k = 1; k <= totalPageInSpecial; k++)
                {
                    serviceurl = "";
                    serviceurl = GetAdGroupSpecialServiceUrl(department.id, "", k.ToString());
                    specials = new SpecialsModel();
                    specials = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);
                    if (_workContext.CurrentCustomer.IsRegistered())
                    {
                        for (int j = 0; j < specials.Specials.Count; j++)
                        {
                            SpecialShoppingList specialShoppingList = _specialshoppinglistservice.GetSpecialShoppingList(_workContext.CurrentCustomer.Id, int.Parse(specials.Specials[j].id));
                            if (specialShoppingList != null)
                            {
                                specials.Specials[j].isAlreadyOnDatabase = "true";
                            }
                            else
                            {
                                specials.Specials[j].isAlreadyOnDatabase = "false";
                            }
                        }

                        ViewBag.dontshowaddshoppinglist = false;
                        ViewBag.dontshowviewshoppinglist = false;
                    }
                    else
                    {
                        ViewBag.dontshowaddshoppinglist = true;
                        ViewBag.dontshowviewshoppinglist = true;
                        ViewBag.unregistered = true;
                    }

                    for (int j = 0; j < specials.Specials.Count; j++)
                    {
                        specialsList.Add(specials.Specials[j]);
                    }



                }

                Department_With_Special department_with_special = new Department_With_Special();
                department_with_special.department = department;

                department_with_special.Specials = specialsList.OrderBy(o => o.brand).ToList();
                ;
                Department_With_Special_list.Add(department_with_special);
            }

            ViewBag.noresult = false;
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.specials", specials);
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.specials_all", Department_With_Special_list);
            return View("specials_all", Department_With_Special_list);
        }

        public string specials_mobile(string department_id = null, string search = null, string page = null, string mobileScroll = null, string unregistered = null, string dontshowaddshoppinglist = null, string storeName = null)
        {
            SpecialResults specialresults = new SpecialResults();
            SpecialsModel specials = new SpecialsModel();

            //var currentCustomerRoleName = _workContext.CurrentCustomer.CustomerRoles.Where(cr => !cr.SystemName.Equals(SystemCustomerRoleNames.Registered) && !cr.SystemName.Equals(SystemCustomerRoleNames.Administrators)).FirstOrDefault().Name;
            //storeName = string.IsNullOrEmpty(storeName) ? currentCustomerRoleName : storeName;
            ////_customerContext.GetCustomerRoleBySystemName(_workContext.CurrentCustomer.SystemName);
            //ViewBag.StoreName = storeName;
            //ViewBag.DontLiveNearStore = storeName.Equals("I do not live near a store", StringComparison.Ordinal) || _workContext.CurrentCustomer.IsGuest() ? _topicService.GetTopicBySystemName("Specials-Store-Selector").Body : "";

            ////_workContext.CurrentCustomer.CustomerRoles
            ////	List<CustomerRole> role = new List<CustomerRole>();
            ////var	currentCustomerId = 	_workContext.CurrentCustomer.Id;

            //List<SelectListItem> customerRoles = _customerContext.GetAllCustomerRoles(true).Where(sr => sr.StoreRole.Equals("True") && !sr.Name.Equals("I do not live near a store"))
            //                                                                               .Select(x => new SelectListItem
            //                                                                               {
            //                                                                                 Text = x.Name,
            //                                                                                 Value = x.AdGroupId
            //                                                                               }).ToList();

            //AdGroupId = _customerContext.GetAllCustomerRoles(true).Where(x => x.Name.Equals(storeName)).FirstOrDefault().AdGroupId;//customerRoles.Where(x => x.Text.Equals(storeName)).FirstOrDefault().Value.ToString();
            //ViewBag.AdGroupId = AdGroupId;

            //ViewBag.RoleName = new SelectList(customerRoles, "Value", "Text", AdGroupId);


            string serviceurl = GetspecialServiceUrl(department_id, search, page);
            specials = specialresults.GetSpecialsByServiceUrl(serviceurl, accesstoken, _cookbookservice);

            string specialList = null;
            for (int i = 0; i < specials.Specials.Count; i++)
            {
                var item = specials.Specials[i];
                specialList += "<li><div id='specials-box'><div class='special_mobile'><div class='special-image'><img src='" + item.image + "'></div><span class='brand'><label id='litBrand' ></label></span><span class='product'>" + item.product + "</span> <p class='special-offer'>" + item.offer + "</p><a href='recipesnew/" + item.recipe_id + "'> " + item.recipe_title + "</a><div class='shoppinglist-add'><div>";
                if (unregistered == "false")
                {
                    if (dontshowaddshoppinglist == "false")
                    {
                        if (item.isAlreadyOnDatabase == "false")
                        {
                            specialList += "<a class='sidebar-action' id='special_adding_" + (item.id) + "' onclick='saveSpecialToShoppingList('" + item.id + "', '" + item.brand.Replace("'", "\\'") + "', '" + item.product.Replace("'", "\\'") + "', '" + item.description.Replace("'", "\\'") + "', '" + item.offer.Replace("'", "\\'") + "', '" + item.image + "','" + item.recipe_title.Replace("'", "\\'") + "', '" + item.recipe_id + "', '" + item.featured.Replace("'", "\\'") + "')'><div class='AddIconSpecial'></div>Add to my Shopping List</a><div id='special_added_" + (item.id) + "' class='specialAdded' ><div class='AcceptIconCookBook'></div><span>This is in your Shopping List</span></div>";
                        }
                        else
                        {
                            specialList += "<div id='special_added_" + (item.id) + "' class='newLine'><div class='AcceptIconCookBook'></div><span>This is in your Shopping List</span></div>";
                        }

                    }
                    else
                    {
                        specialList += "<div id='special_added_" + (item.id) + "'><div class='AcceptIconSpecial'></div><span>This is in your Shopping List</span></div>";
                    }
                }
                else
                {
                    specialList += "<a class='sidebar-action' id='special_adding_" + (item.id) + "' href='/Login'><div class='AddIconSpecial'></div>Add to my Shopping List</a>";
                }
                specialList += "</div></div></div></div></li>";
            }
            return specialList;
        }

        private string GetSpecialLandingServiceUrl()
        {
            string chainId = _storeInformationSettings.ChainID.ToString();


            //return @"http://3nerds.services.procomarketing.com/specials?chain_id=" + chainId + "";
            //return @"http://69.65.19.37:8023/api/special?chainId=" + chainId + "&type=xml";

            //return @"http://69.65.19.37:8023/api/special?chainId=" + chainId + "&type=xml";

            return @"http://69.65.19.37:8023/api/special?chainId=" + chainId + "&type=xml";
        }

        private string GetAdGroupSpecialLandingServiceUrl()
        {
            return @"http://69.65.19.37:8023/api/special?chainId=" + AdGroupId + "&type=xml";
            //return @"http://69.65.19.37:8023/api/special?chainId=" + AdGroupId + "&type=xml";
        }

        private string GetAdGroupSpecialServiceUrl(string department_id, string search, string page)
        {
            string serviceUrl = @"http://69.65.19.37:8023/api/special?chainId=" + AdGroupId + "&type=xml";
            //string serviceUrl = @"http://69.65.19.37:8023/api/special?chainId=" + AdGroupId + "&type=xml" + "&per_page=" + SpecialpageSize;

            serviceUrl += "&department_id=" + department_id;

            if (!string.IsNullOrEmpty(search))
            {
                serviceUrl += "&search=" + search;
            }
            if (!string.IsNullOrEmpty(page))
            {
                serviceUrl += "&page=" + page;
            }
            return @serviceUrl;
        }

        private string GetspecialServiceUrl(string department_id, string search, string page)
        {

            string chainId = _storeInformationSettings.ChainID.ToString();
            //string serviceUrl = @"http://3nerds.services.procomarketing.com/specials?chain_id=" + chainId + "" + "&per_page=" + SpecialpageSize;
            //string serviceUrl = @"http://69.65.19.37:8023/api/special?chainId=" + chainId + "&type=xml" + "&per_page=" + SpecialpageSize;
            string serviceUrl = @"http://localhost:1923/api/special?chainId=" + chainId + "&type=xml";
            serviceUrl += "&department_id=" + department_id;

            if (!string.IsNullOrEmpty(search))
            {
                serviceUrl += "&search=" + search;
            }
            if (!string.IsNullOrEmpty(page))
            {
                serviceUrl += "&page=" + page;
            }
            return @serviceUrl;
        }

        private string GetHomePagefeaturedSpecialServiceUrl(string page)
        {

            string chainId = _storeInformationSettings.ChainID.ToString();
            //string serviceUrl = @"http://3nerds.services.procomarketing.com/specials?chain_id=" + chainId + "" + "&per_page=" + SpecialpageSize;
            //string serviceUrl = @"http://69.65.19.37:8023/api/special?chainId=" + chainId + "&type=xml" + "&per_page=" + SpecialpageSize;
            string serviceUrl = @"http://69.65.19.37:8023/api/special?chainId=" + chainId + "&type=xml" + "&isFeatured=true";
            if (!string.IsNullOrEmpty(page))
            {
                serviceUrl += "&page=" + page;
            }
            return @serviceUrl;
        }

        private string GetFeaturedServiceUrl(string search, int page)
        {
            string chainId = _storeInformationSettings.ChainID.ToString();
            string serviceUrl = @"http://69.65.19.37:8023/api/specials?chainId=" + chainId + "" + "&per_page=" + page;
            //string serviceUrl = @"http://69.65.19.37:8023/api/special?chainId=" + chainId + "&type=xml" + "&per_page=" + page;

            return @serviceUrl;
        }

        private void SetspecialPagerInfos(SpecialsModel specials, string page)
        {
            //double.TryParse(specials.Pagination.per_page, out pageSize);
            double.TryParse(specials.Pagination.total_records, out specialitemCount);
            //   SpecialpageSize = Math.Ceiling(double.Parse(specials.Pagination.total_records) / double.Parse(specials.Pagination.total_pages));
            int.TryParse(specials.Pagination.total_pages, out specialtotalPage);
            specialcurrentPage = page != null ? int.Parse(page) : 1;
            //string a = null;
        }

        private void BuildSpecialPager(int specialcurrentPage, string department_name)
        {
            string previousQurey = string.Empty;
            Regex re = new Regex(@"\d+");
            Match m = re.Match(Request.Url.Query);
            if (m.Success && Request.Url.Query.Contains("?page="))
            {
                previousQurey = "&" + Request.Url.Query.Replace("?page=" + m.Value + "&", string.Empty);

            }
            else
            {
                previousQurey = "&" + Request.Url.Query.Replace("?", string.Empty);
            }
            specialtotalPage = (int)Math.Ceiling(specialitemCount / SpecialpageSize);
            StringBuilder pagerHtml = new StringBuilder();
            if (specialcurrentPage - 1 == 0) pagerHtml.Append("<span class='disabled prev_page'>&lt;&lt;Previous</span>");
            else pagerHtml.Append("<a href='" + specialurlprefix + "?page=" + (specialcurrentPage - 1) + previousQurey + "'>&lt;&lt;Previous</a>");
            // else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + (currentPage - 1) + "'>&lt;&lt;Previous</a>");
            for (int i = 1; i <= specialtotalPage; i++)
            {
                if (specialcurrentPage >= specialpagerCount && specialcurrentPage <= (specialtotalPage - specialpagerCount))
                {
                    //<<Previous12 ... 5678910111213 ... 3940Next>>
                    if (i == 3 || i == specialtotalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if ((i > 2 && i < (specialcurrentPage - (specialpagerCount / 2))) || (i < specialtotalPage - 1 && i > (specialcurrentPage + (specialpagerCount / 2)))) continue;
                    if (i == specialcurrentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + specialurlprefix + "?page=" + i + previousQurey + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");

                }
                else if (specialcurrentPage > specialtotalPage - specialpagerCount)
                {
                    //<<Previous12 ... 31323334353637383940Next>>
                    if (i == 3 && specialtotalPage > specialpagerCount) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i > 2 && i < specialtotalPage - specialpagerCount) continue;
                    if (i == specialcurrentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + specialurlprefix + "?page=" + i + previousQurey + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");
                }
                else
                {
                    //<<Previous123456789 ... 3940Next>>
                    if (i == specialtotalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i < specialtotalPage - 1 && i > specialpagerCount) continue;
                    if (i == specialcurrentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + specialurlprefix + "?page=" + i + previousQurey + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");
                }
            }
            if (specialcurrentPage == specialtotalPage) pagerHtml.Append("<span class='disabled next_page'>Next&gt;&gt;</span>");
            else pagerHtml.Append("<a href='" + specialurlprefix + "?page=" + (specialcurrentPage + 1) + previousQurey + "'>Next&gt;&gt;</a>");
            //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + (currentPage + 1) + "'>Next&gt;&gt;</a>");
            specialpagination = pagerHtml.ToString();
            int startIndex = (int)(specialcurrentPage * SpecialpageSize - SpecialpageSize) + 1;
            int endIndex = Math.Min((int)(specialcurrentPage * SpecialpageSize), (int)specialitemCount);
            displayableSpecialItems = "Displaying specials <b>" + startIndex + " - " + endIndex + "</b> of  <b>" + specialitemCount + "</b> in <strong>" + department_name + "</strong>.";
        }

        private void LoadDropdownListSpecials_select_category()
        {

        }

        private List<Department> GetDefaultCategories()
        {
            List<Department> categories = new List<Department>();
            categories.Add(new Department { id = "23", name = "Supplements and Nutrition" });
            categories.Add(new Department { id = "24", name = "Grocery" });
            categories.Add(new Department { id = "26", name = "Refrigerated" });
            categories.Add(new Department { id = "27", name = "Health and Beauty" });
            return categories;
        }

        [HttpPost]
        public ActionResult AddTospecialshoppingListNew(string id = null, string brand = null, string product = null, string description = null, string offer = null, string image = null, string recipe_title = null, string recipe_id = null, string featured = null)
        {
            if (_workContext.CurrentCustomer.IsRegistered())
            {
                SpecialShoppingList specialshoppinglist = new SpecialShoppingList();

                specialshoppinglist.SpecialId = int.Parse(id);
                specialshoppinglist.CustomerId = _workContext.CurrentCustomer.Id;
                specialshoppinglist.Brand = brand;
                specialshoppinglist.Product = product;
                specialshoppinglist.Description = description;
                specialshoppinglist.Offer = offer;
                specialshoppinglist.Image = image;
                specialshoppinglist.TimeStampUtc = DateTime.UtcNow;

                if (!string.IsNullOrEmpty(recipe_id))
                {
                    specialshoppinglist.RecipeTitle = recipe_title;
                    specialshoppinglist.RecipeId = int.Parse(recipe_id);
                }
                else 
                {
                    specialshoppinglist.RecipeId = 0;
                    specialshoppinglist.RecipeTitle = "";
                }

                if (!string.IsNullOrEmpty(featured))
                {
                    specialshoppinglist.IsFeatured = true;
                }
                else
                {
                    specialshoppinglist.IsFeatured = false;
                }

                _specialshoppinglistservice.InserttospecialShoppingList(specialshoppinglist);

            }
            return Json(new { added = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region AddTokenTolocalhostSettingTable
        //[HttpPost]
        //public ActionResult AddToken(string Userid, string Password, string Token)
        //{

        //    using (var client = new WebClient())
        //    {
        //        var values = new NameValueCollection();
        //        values["UserName"] = "SuperPowerUser";
        //        values["Password"] = "Sin@01923";
        //        values["grant_type"] = "password";
        //        var response = client.UploadValues("http://localhost:1923/oauth/token", values);

        //        var responseString = Encoding.Default.GetString(response);
        //        var results = JsonConvert.DeserializeObject<dynamic>(responseString);
        //        _settingContext.SetSetting("recipeplugin.storeinformationsettings.chaintoken", results.access_token, 0);
        //    }

        //    return Json(new { tokenAdded = true }, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region tips

        public ActionResult tipsAndGuids(string id = null, string search = null, string page = null)
        {
            TipDetail tipInfo = new TipDetail();
            if (id != null)
            {
                TipResults tipResults = new TipResults();
                TipDetail tipdetail = tipResults.GetTipsDetail(id);
                tipInfo = tipdetail;
                ViewBag.title = "<h1 class='header2'>" + tipdetail.title + "</h1>";
                ViewBag.body = tipdetail.body;
                ViewBag.hidSearch = "true";
            }
            else
            {
                ViewBag.title = "Need a little guidance when it comes to preparing, cooking, or storing your favorite foods? We have tips to help you with everything from cooking techniques to keeping your kitchen sanitary.";
                ViewBag.body = "<b>Click a tip to the right to display it.</b>";
                tipInfo.title = "Tips";
                ViewBag.hidSearch = "false";
            }
            if (string.IsNullOrEmpty(search))
            {
                ViewBag.search = null;
            }
            else
            {
                ViewBag.search = search;
            }
            if (string.IsNullOrEmpty(page))
            {
                ViewBag.page = null;
            }
            else
            {
                ViewBag.page = page;
            }
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.tipsAndGuids", tipInfo);
            return View("tipsAndGuids", tipInfo);
        }

        public ActionResult TipsdisplayableItems(string search = null, string page = null, string hidSearch = null)
        {
            TipsModel tips;
            TipResults tipResults = new TipResults();
            ViewBag.hidSearch = hidSearch;
            ViewBag.search = search;
            if ((tips = tipResults.GetTips(accesstoken, GetServiceUrl(search, page), _cookbookservice)) != null)
            {
                SetPagerInfos(tips, search, page);
                BuildTipsPager(currentPage);
                ViewBag.displayableTipsItems = "";
                if (search != null)
                {
                    ViewBag.displayableTipsItems = displayableTipsItems;
                }
                ViewBag.tipspagination = tipspagination;
                ViewBag.totalpage = tips.Pagination.total_pages;

                //return View("Nop.Plugin.RecipeNew.Views.Recipe.TipsdisplayableItems", tips.Tips);
                return View("TipsdisplayableItems", tips.Tips);
            }
            else
            {
                ViewBag.tipspagination = "";
                ViewBag.displayableTipsItems = "No entries found containing <strong>'" + search + "'</strong>.";
                ViewBag.totalpage = null;
                string lastseg = Request.RawUrl.Substring(0, Request.RawUrl.LastIndexOf('/'));
                try
                {
                    int.Parse(lastseg);
                }
                catch
                {
                    //Page.Title = SettingManager.StoreName + " - " + "Tips";
                }
                IList tp = null;
                // return View("Nop.Plugin.RecipeNew.Views.Recipe.TipsdisplayableItems", tp);
                return View("TipsdisplayableItems", tp);
            }
        }

        [HttpPost]
        public ActionResult getautocompleteTipsSearchresults(string search = null)
        {
            List<Tip> titlearray = new List<Tip>();
            TipsModel tips;
            TipResults tipResults = new TipResults();
            tips = tipResults.GetTips(accesstoken, GetServiceUrl(search, null), _cookbookservice);
            for (int i = 0; i < tips.Tips.Count; i++)
            {
                Tip tipItem = new Tip();

                tipItem.title = tips.Tips[i].title;
                tipItem.id = tips.Tips[i].id;
                titlearray.Add(tipItem);
                if (i == 9) break;
            }
            return Json(titlearray);
        }

        public string TipsdisplayableItems_mobile(string search = null, string page = null, string hidSearch = null, string mobileScroll = null)
        {
            string tipsList = null;
            if (!string.IsNullOrEmpty(mobileScroll))
            {



                TipsModel tips;
                TipResults tipResults = new TipResults();
                tips = tipResults.GetTips(accesstoken, GetServiceUrl(search, page), _cookbookservice);
                for (int i = 0; i < tips.Tips.Count; i++)
                {
                    var item = tips.Tips[i];

                    tipsList += "<li data-theme='b' class='ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-b'><div class='ui-btn-inner ui-li'><div class='ui-btn-text'><a href='/tipsandguides/" + item.id + "' class='ui-link-inherit'>" + item.title + "</a></div><span class='ui-icon ui-icon-arrow-r ui-icon-shadow'></span></div></li>";

                }
            }
            return tipsList;
        }

        private string GetServiceUrl(string search, string page)
        {
            string serviceUrl = @"http://3nerds.services.procomarketing.com/tips";
            if (!string.IsNullOrEmpty(search))
            {
                serviceUrl += "?search=" + search;
            }
            if (!string.IsNullOrEmpty(page))
            {
                if (!string.IsNullOrEmpty(search))
                {
                    serviceUrl += "&";
                }
                else
                {
                    serviceUrl += "?";
                }
                serviceUrl += "page=" + page;
            }
            return @serviceUrl;
        }

        private void SetPagerInfos(TipsModel tips, string search, string page)
        {

            //double.TryParse(specials.Pagination.per_page, out pageSize);
            double.TryParse(tips.Pagination.total_records, out itemCount);
            pageSize = Math.Ceiling(double.Parse(tips.Pagination.total_records) / double.Parse(tips.Pagination.total_pages));
            int.TryParse(tips.Pagination.total_pages, out totalPage);
            currentPage = page != null ? int.Parse(page) : 1;
            searchTitle = search;// Request.QueryString["search"] != null ? Request.QueryString["search"] : string.Empty;

        }

        private void BuildTipsPager(int currentPage)
        {
            string previousQurey = string.Empty;
            Regex re = new Regex(@"\d+");
            Match m = re.Match(Request.Url.Query);
            if (m.Success && Request.Url.Query.Contains("?page="))
            {
                previousQurey = "&" + Request.Url.Query.Replace("?page=" + m.Value + "&", string.Empty);

            }
            else
            {
                previousQurey = "&" + Request.Url.Query.Replace("?", string.Empty);
            }
            totalPage = (int)Math.Ceiling(itemCount / pageSize);
            StringBuilder pagerHtml = new StringBuilder();
            if (currentPage - 1 == 0) pagerHtml.Append("<span class='disabled prev_page'>&lt;&lt;Previous</span>");
            else pagerHtml.Append("<a href='" + tipsurlprefix + "?page=" + (currentPage - 1) + previousQurey + "'>&lt;&lt;Previous</a>");
            // else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + (currentPage - 1) + "'>&lt;&lt;Previous</a>");
            for (int i = 1; i <= totalPage; i++)
            {
                if (currentPage >= tipspagerCount && currentPage <= (totalPage - tipspagerCount))
                {
                    //<<Previous12 ... 5678910111213 ... 3940Next>>
                    if (i == 3 || i == totalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if ((i > 2 && i < (currentPage - (tipspagerCount / 2))) || (i < totalPage - 1 && i > (currentPage + (tipspagerCount / 2)))) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + tipsurlprefix + "?page=" + i + previousQurey + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");

                }
                else if (currentPage > totalPage - tipspagerCount)
                {
                    //<<Previous12 ... 31323334353637383940Next>>
                    if (i == 3 && totalPage > tipspagerCount) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i > 2 && i < totalPage - tipspagerCount) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + tipsurlprefix + "?page=" + i + previousQurey + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");
                }
                else
                {
                    //<<Previous123456789 ... 3940Next>>
                    if (i == totalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i < totalPage - 1 && i > tipspagerCount) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='" + tipsurlprefix + "?page=" + i + previousQurey + "'>" + i + "</a>");
                    //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + i + "'>" + i + "</a>");
                }
            }
            if (currentPage == totalPage) pagerHtml.Append("<span class='disabled next_page'>Next&gt;&gt;</span>");
            else pagerHtml.Append("<a href='" + tipsurlprefix + "?page=" + (currentPage + 1) + previousQurey + "'>Next&gt;&gt;</a>");
            //else pagerHtml.Append("<a href='../Recipe/Recipes.aspx?page=" + (currentPage + 1) + "'>Next&gt;&gt;</a>");
            tipspagination = pagerHtml.ToString();
            int startIndex = (int)(currentPage * pageSize - pageSize) + 1;
            int endIndex = Math.Min((int)(currentPage * pageSize), (int)itemCount);
            displayableTipsItems = "Displaying Tips <b>" + startIndex + " - " + endIndex + "</b> of  <b>" + itemCount + "</b> in total with " + searchTitle;

        }

        #endregion

        #region Recipe Calender

        public ActionResult GetRecipeCalender()
        {
            return View("Calender");
        }

        [HttpGet]
        public ActionResult getAllCalenderRecepies()
        {
            List<RecipesInCalender> recipeCalenderList = new List<RecipesInCalender>();
            List<RecipeCalender> recipeCalender = new List<RecipeCalender>();

            recipeCalender = _recipeCalenderService.GetAllRecipeCalender();

            for (int i = 0; i < recipeCalender.Count; i++)
            {
                RecipesInCalender item = new RecipesInCalender();

                item.start = recipeCalender[i].Date.ToShortDateString();
                item.title = recipeCalender[i].RecipeTitle;
                item.url = "/recipesnew/" + recipeCalender[i].RecipeID;
                item.id = recipeCalender[i].RecipeID;
                recipeCalenderList.Add(item);
            }
            return Json(recipeCalenderList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult addrecipestothisday(string date, string RecipeTitle, int RecipeID)
        {

            //  var format = "g";
            CultureInfo provider = CultureInfo.CurrentUICulture;

            RecipeCalender recipeCalenderItem = new RecipeCalender();
            DateTime RecipeDate = new DateTime();
            RecipeDate = DateTime.Parse(date);// Exact(date, "ddd dd MMM yyyy h:mm tt zzz", provider);

            if (RecipeTitle == "false")
            {
                RecipeTitle = getRecipeTitleById(RecipeID.ToString());

            }
            recipeCalenderItem.Date = RecipeDate;
            recipeCalenderItem.RecipeID = RecipeID;
            recipeCalenderItem.RecipeTitle = RecipeTitle;
            _recipeCalenderService.InsertRecipetoADay(recipeCalenderItem);
            return Json("added");

        }

        [HttpPost]
        public string checkIfDayisAlreadyOccupied(string date)
        {

            DateTime RecipeDate = new DateTime();
            RecipeDate = DateTime.Parse(date);
            var recipecalender = _recipeCalenderService.GetRecipeBydate(RecipeDate);
            if (recipecalender != null)
            {
                return recipecalender.RecipeID.ToString();
            }
            else
            {
                return "false";
            }


        }

        [HttpPost]
        public ActionResult deleteRecipeforThisDay(string date)
        {
            DateTime RecipeDate = new DateTime();
            RecipeDate = DateTime.Parse(date);

            _recipeCalenderService.DeleteRecipeCalenderItem(RecipeDate);
            return Json("");
        }

        public ActionResult Public_recipe_display()
        {
            DateTime RecipeDate = new DateTime();
            RecipeDate = DateTime.Today;

            var recipecalender = _recipeCalenderService.GetRecipeBydate(RecipeDate);
            int recipeid = 0;
            if (recipecalender != null)
            {
                recipeid = recipecalender.RecipeID;
            }
            else
            {
                recipecalender = _recipeCalenderService.GetLastAddedRecipe();
                recipeid = recipecalender.RecipeID;
            }
            var noresult = 0;
            ViewBag.noresult = null;
            if (recipeid == 0)
            {
                noresult = 1;
            }

            RecipeResults objrecipe = new RecipeResults();
            try
            {
                objrecipe.GetSingleRecipe(recipeid.ToString(), null, null, null, _cookbookservice, accesstoken);
                ViewBag.recipe_title = objrecipe.Title;
                ViewData["recipe_title"] = objrecipe.Title;
                if (objrecipe.PhotoUrl == string.Empty) objrecipe.PhotoUrl = "/Content/images/sp.gif";
                ViewBag.img_recipe_photo = objrecipe.PhotoUrl;

                ViewBag.recipeId = recipeid.ToString();

            }
            catch
            {
                noresult = 1;
            }
            ViewBag.noresult = "";
            if (noresult == 1)
            {
                objrecipe = null;
                ViewBag.noresult = "No recipes were found.";

            }
            //return View("Nop.Plugin.RecipeNew.Views.Recipe.DailyRecipe", objrecipe);

            return View("DailyRecipeNew", objrecipe);
        }

        [HttpPost]
        public ActionResult ShowRecipeInCalenderPage(int recipeid)
        {
            var noresult = 0;

            RecipeResults objrecipe = new RecipeResults();
            try
            {
                objrecipe.GetSingleRecipe(recipeid.ToString(), null, null, null, _cookbookservice, accesstoken);
                if (objrecipe.PhotoUrl == string.Empty) objrecipe.PhotoUrl = "/Content/images/sp.gif";

            }
            catch
            {
                noresult = 1;
            }

            if (noresult == 1)
            {
                objrecipe = null;
            }

            return Json("false");
        }

        private string BuildRecipeListCalenderPager(int currentPage, double itemCountRecipecalender, double pageSizeRecipecalender, string previousQurey, string searchTitle, string searchType)
        {

            var totalPage = (int)Math.Ceiling(itemCountRecipecalender / pageSizeRecipecalender);
            StringBuilder pagerHtml = new StringBuilder();
            int pagerCountRecipeCalender = 9;
            var pageNumber = (currentPage - 1);
            string pg = pageNumber.ToString();
            if (currentPage - 1 == 0) pagerHtml.Append("<span class='disabled prev_page'>&lt;&lt;Previous</span>");
            else pagerHtml.Append("<a href='Javascript:recipeListInCalender(\"" + searchType + "\",\" " + searchTitle + " \", " + pg + ")'>&lt;&lt;Previous</a>");

            for (int i = 1; i <= totalPage; i++)
            {
                if (currentPage >= pagerCountRecipeCalender && currentPage <= (totalPage - pagerCountRecipeCalender))
                {
                    //<<Previous12 ... 5678910111213 ... 3940Next>>
                    pageNumber = i;
                    pg = pageNumber.ToString();
                    if (i == 3 || i == totalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if ((i > 2 && i < (currentPage - (pagerCountRecipeCalender / 2))) || (i < totalPage - 1 && i > (currentPage + (pagerCountRecipeCalender / 2)))) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='Javascript:recipeListInCalender(\"" + searchType + "\" , \"" + searchTitle + "\" , " + pg + ")'>" + i + "</a>");


                }
                else if (currentPage > totalPage - pagerCountRecipeCalender)
                {
                    //<<Previous12 ... 31323334353637383940Next>>
                    pageNumber = i;
                    pg = pageNumber.ToString();
                    if (i == 3 && totalPage > pagerCountRecipeCalender) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i > 2 && i < totalPage - pagerCountRecipeCalender) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='Javascript:recipeListInCalender(\"" + searchType + "\" , \"" + searchTitle + "\" , " + pg + ")'>" + i + "</a>");

                }
                else
                {
                    //<<Previous123456789 ... 3940Next>>
                    pageNumber = i;
                    pg = pageNumber.ToString();
                    if (i == totalPage - 1) pagerHtml.Append("<span class='gap'> ... </span>");
                    if (i < totalPage - 1 && i > pagerCountRecipeCalender) continue;
                    if (i == currentPage) pagerHtml.Append("<span class='current'>" + i + "</span>");
                    else pagerHtml.Append("<a href='Javascript:recipeListInCalender(\"" + searchType + "\" , \"" + searchTitle + "\" , " + pg + ")'>" + i + "</a>");

                }
            }
            pageNumber = (currentPage + 1);
            pg = pageNumber.ToString();
            if (currentPage == totalPage) pagerHtml.Append("<span class='disabled next_page'>Next&gt;&gt;</span>");
            else pagerHtml.Append("<a href='Javascript:recipeListInCalender(\"" + searchType + "\" , \"" + searchTitle + "\" , " + pg + ")'>Next&gt;&gt;</a>");
            int startIndex = (int)(currentPage * pageSizeRecipecalender - pageSizeRecipecalender) + 1;
            int endIndex = Math.Min((int)(currentPage * pageSizeRecipecalender), (int)itemCountRecipecalender);
            displayableItems = "Displaying recipes <b>" + startIndex + " - " + endIndex + "</b> of  <b>" + itemCountRecipecalender + "</b> in total with " + searchTitle;

            return pagerHtml.ToString();
        }

        #endregion

        #region encripted password for Bluegoose Users

        public string EncryptedPassword()
        {
            StreamReader sr = new StreamReader(Server.MapPath("/Plugins/Recipe/Content/bluegoose users.csv"));
            string inputLine = "";

            string password = "loginID, Password, PasswordSalt\n";
            var j = 0;

            String[] values = null;
            while ((inputLine = sr.ReadLine()) != null)
            {
                if (j > 0)
                {
                    values = inputLine.Split(',');

                    //sql += values;
                    string saltKey = _encryptedService.CreateSaltKey(5);

                    // var id = j - 1;
                    password += values[0] + ",";
                    password += _encryptedService.CreatePasswordHash(values[3], saltKey, _customerSettings.HashedPasswordFormat) + ",";//Password
                    password += saltKey;
                    password += "\n";
                }

                j++;
            }
            sr.Close();
            TextWriter tw = new StreamWriter(Server.MapPath("/Plugins/Recipe/Content/passwordImport_bluegoose_users.csv"));
            // write a line of text to the file
            tw.Write(password);
            // close the stream
            tw.Close();
            return password;
        }

        #endregion
    }
}
