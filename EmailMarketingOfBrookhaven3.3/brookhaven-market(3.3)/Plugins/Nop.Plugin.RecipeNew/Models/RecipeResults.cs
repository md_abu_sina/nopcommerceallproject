﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Linq;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.IO;
using Nop.Services.Configuration;
using Nop.Plugin.RecipeNew.Services;
using System.Xml;
using System.Text.RegularExpressions;

namespace Nop.Plugin.RecipeNew.Models
{
    public class RecipeResults
    {

        #region Recipe Search Properties

        public string current_page { get; set; }
        public string next_page { get; set; }
        public string total_pages { get; set; }
        public string per_page { get; set; }
        public string start_record { get; set; }
        public string end_record { get; set; }
        public string total_records { get; set; }
        public string SearchWith { get; set; }
        public DataTable RecipeData { get; set; }

        #endregion

        #region Single Recipe Properties

        public string RecipeId { get; set; }
        public string Title { get; set; }
        public string Serves { get; set; }
        public string PhotoUrl { get; set; }
        public string Preparation { get; set; }
        public string Cooking { get; set; }
        public string Total { get; set; }
        public string Instructions { get; set; }
        public List<string> arr_ingredients { get; set; }

        public struct Nutrient
        {
            public string Category { get; set; }
            public string Units { get; set; }
            public string Name { get; set; }
            public string percent_rdi { get; set; }
            public string Value { get; set; }
        }
        public DataTable NutrientTable { get; set; }
        public List<Nutrient> NutrientList { get; set; }
        public DataTable briefNutrientList { get; set; }
        public DataTable NutrientsDetailsTable { get; set; }
        public DataTable FatsDetailsTable { get; set; }
        public DataTable VitaminsDetailsTable { get; set; }
        public DataTable MineralsDetailsTable { get; set; }
        public DataSet AislesSet { get; set; }

        #endregion

        #region Recipe Search Methods
        private void ClearSearchRecipes()
        {
            current_page = next_page = total_pages = per_page = start_record = end_record = total_records = string.Empty;
            RecipeData = null;
            NutrientList = null;
        }

        public void GetSearchedRecipes(string page, string titletext, string ingredient, string quicksearch, string tags, string photo, string order, string direction, string accesstoken, int chainId, ICookBookService _cookbookservice)
        {
            try
            {
                ClearSearchRecipes();
                //Create the DataTable structure for Recipes
                DataTable RecipeTable = CreateRecipeTable();

                // Create the web request 
                //string searchUrl = @"http://3nerds.services.procomarketing.com/recipes?page=" + page + "&quicksearch=Bread&x=0&y=0";

                //string searchUrl = @"http://3nerds.services.procomarketing.com/recipes?";
                string searchUrl = @"http://69.65.19.37:8023/api/recipe?type=xml&";

                if (!string.IsNullOrEmpty(page))
                {
                    searchUrl += "page=" + page + "&";
                }
                if (!string.IsNullOrEmpty(tags))
                {
                    string[] mtags = tags.Split(',');
                    for (int i = 0; i < mtags.Length; i++)
                    {
                        searchUrl += "tags=" + mtags[i] + "&";
                    }
                    //searchUrl += "tags=" + tags + "&";
                    SearchWith = "tagged <b>'" + tags + "'</b>";
                }
                if (!string.IsNullOrEmpty(titletext))
                {
                    searchUrl += "title=" + titletext + "&";
                    SearchWith = "<b>'" + titletext + "'</b> in the title";
                }
                if (!string.IsNullOrEmpty(ingredient))
                {
                    searchUrl += "ingredient=" + ingredient + "&";
                    SearchWith = "<b>'" + ingredient + "'</b> in the ingredient";
                }
                if (!string.IsNullOrEmpty(quicksearch))
                {
                    searchUrl += "quicksearch=" + quicksearch + "&";
                    SearchWith = "<b>'" + quicksearch + "'</b> in the quicksearch";
                }
                if (!string.IsNullOrEmpty(photo))
                {
                    searchUrl += "photo=" + photo + "&";
                }
                if (!string.IsNullOrEmpty(order))
                {
                    searchUrl += "order=" + order + "&";
                }
                if (chainId>0)
                {
                    searchUrl += "chainId=" + chainId + "&";
                }
                if (!string.IsNullOrEmpty(direction))
                {
                    searchUrl += "direction=" + direction + "&";
                }

                if (searchUrl.EndsWith("&")) searchUrl = searchUrl.Remove(searchUrl.Length - 1);

                // string searchUrl = @"F:/Development/nopCommerce/recipesingredient.xml";

                byte[] totalResponse = _cookbookservice.GetResponseFromUrl(accesstoken, searchUrl);


                MemoryStream stream = new MemoryStream(totalResponse);
                XDocument xSearchedDoc = XDocument.Load(stream);

                var xRecipes = from p in xSearchedDoc.Descendants("recipe") select p;

                foreach (var recipe in xRecipes)
                {
                    string recipeid = recipe.Attribute("id").Value;
                    string title = (recipe.Element("title") == null) ? string.Empty : recipe.Element("title").Value;
                    string servings = (recipe.Element("servings") == null) ? string.Empty : recipe.Element("servings").Value;
                    string photourl = (recipe.Element("photo") == null) ? "/Content/images/sp.gif" : recipe.Element("photo").Value;

                    string preparation = string.Empty, cooking = string.Empty, total = string.Empty;
                    var timesEle = recipe.Element("times");
                    if (timesEle != null)
                    {
                        preparation = (timesEle.Element("preparation") == null) ? string.Empty : timesEle.Element("preparation").Value;
                        cooking = (timesEle.Element("cooking") == null) ? string.Empty : timesEle.Element("cooking").Value;
                        total = (timesEle.Element("total") == null) ? string.Empty : timesEle.Element("total").Value;
                    }

                    string calories = string.Empty, fat = string.Empty, fat_percent = string.Empty, cholesterol = string.Empty, carbs = string.Empty;
                    var nutrients = from p in recipe.Elements("nutrition").Elements("nutrient") select p;
                    if (nutrients != null)
                    {
                        foreach (var nutrient in nutrients)
                        {
                            if (nutrient.Attribute("name").Value.Trim() == "calories") calories = nutrient.Value;
                            else if (nutrient.Attribute("name").Value.Trim() == "fat") fat = nutrient.Value;
                            else if (nutrient.Attribute("name").Value.Trim() == "fat_percent") fat_percent = nutrient.Value;
                            else if (nutrient.Attribute("name").Value.Trim() == "cholesterol") cholesterol = nutrient.Value;
                            else if (nutrient.Attribute("name").Value.Trim() == "carbs") carbs = nutrient.Value;
                        }
                    }
                    RecipeTable.Rows.Add(new object[] { recipeid, title, servings, photourl, preparation, cooking, total, calories, fat, fat_percent, cholesterol, carbs });
                }
                RecipeData = RecipeTable.Copy();

                var xpagination = from p in xSearchedDoc.Descendants("pagination") select p;
                //XElement xpagination = xSearchedDoc.Element("pagination");

                foreach (var xpag in xpagination)
                {
                    current_page = (xpag.Element("current_page") != null) ? xpag.Element("current_page").Value : string.Empty;
                    next_page = (xpag.Element("next_page") != null) ? xpag.Element("next_page").Value : string.Empty;
                    total_pages = (xpag.Element("total_pages") != null) ? xpag.Element("total_pages").Value : string.Empty;
                    per_page = (xpag.Element("per_page") != null) ? xpag.Element("per_page").Value : string.Empty;
                    start_record = (xpag.Element("start_record") != null) ? xpag.Element("start_record").Value : string.Empty;
                    end_record = (xpag.Element("end_record") != null) ? xpag.Element("end_record").Value : string.Empty;
                    total_records = (xpag.Element("total_records") != null) ? xpag.Element("total_records").Value : string.Empty;
                }
            }
            catch (WebException wbEx)
            {
                RecipeData = new DataTable();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetSearchedRecipesAutoComplete(string page, string titletext, string ingredient, string accesstoken, int chainId, ICookBookService _cookbookservice)
        {
            try
            {
                ClearSearchRecipes();
                //Create the DataTable structure for Recipes
                DataTable RecipeTable = CreateRecipeTable();

                // Create the web request 
                //string searchUrl = @"http://3nerds.services.procomarketing.com/recipes?page=" + page + "&quicksearch=Bread&x=0&y=0";

                //string searchUrl = @"http://3nerds.services.procomarketing.com/recipes?";
                string searchUrl = @"http://69.65.19.37:8023/api/recipe?type=xml&";

                if (!string.IsNullOrEmpty(page))
                {
                    searchUrl += "page=" + page + "&";
                }
                if (!string.IsNullOrEmpty(titletext))
                {
                    searchUrl += "title=" + titletext + "&";
                    searchUrl += "searchForAutoComplete=" + true + "&";
                    SearchWith = "<b>'" + titletext + "'</b> in the title";
                }
                if (chainId > 0)
                {
                    searchUrl += "chainId=" + chainId + "&";
                }
                if (!string.IsNullOrEmpty(ingredient))
                {
                    searchUrl += "ingredient=" + ingredient + "&";
                    searchUrl += "searchForAutoComplete=" + true + "&";
                    SearchWith = "<b>'" + ingredient + "'</b> in the ingredient";
                }


                if (searchUrl.EndsWith("&")) searchUrl = searchUrl.Remove(searchUrl.Length - 1);

                // string searchUrl = @"F:/Development/nopCommerce/recipesingredient.xml";

                byte[] totalResponse = _cookbookservice.GetResponseFromUrl(accesstoken, searchUrl);


                MemoryStream stream = new MemoryStream(totalResponse);
                XDocument xSearchedDoc = XDocument.Load(stream);

                var xRecipesAutoComplete = from p in xSearchedDoc.Descendants("recipe") select p;

                foreach (var recipe in xRecipesAutoComplete)
                {
                    string recipeid = recipe.Attribute("id").Value;
                    string title = (recipe.Element("title") == null) ? string.Empty : recipe.Element("title").Value;
                    RecipeTable.Rows.Add(new object[] { recipeid, title });
                }
                RecipeData = RecipeTable.Copy();
            }
            catch (WebException wbEx)
            {
                RecipeData = new DataTable();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable CreateRecipeTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("recipeid");
            dt.Columns.Add("title");
            dt.Columns.Add("serve");
            dt.Columns.Add("photourl");
            dt.Columns.Add("preparation");
            dt.Columns.Add("cooking");
            dt.Columns.Add("total");
            dt.Columns.Add("calories");
            dt.Columns.Add("fat");
            dt.Columns.Add("fat_percent");
            dt.Columns.Add("cholesterol");
            dt.Columns.Add("carbs");

            return dt;
        }
        #endregion

        #region Single Recipe Methods


        private DataTable CreateNutrientTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("category");
            dt.Columns.Add("units");
            dt.Columns.Add("name");
            dt.Columns.Add("percent_rdi");
            dt.Columns.Add("value");
            return dt;
        }

        private string GetFormatedText(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            value = value.Replace("_", ", ");
            char[] chars = value.ToCharArray();
            chars[0] = char.ToUpper(chars[0]);
            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] == ' ')
                    chars[i + 1] = char.ToUpper(chars[i + 1]);
            }
            return new string(chars);
        }
        private string GetFormattedText(string value)
        {
            value = value.Replace("_", ", ");
            return System.Globalization.CultureInfo.CurrentUICulture.TextInfo.ToTitleCase(value);
        }
        private void ClearSingleRecipe()
        {
            RecipeId = Title = Serves = PhotoUrl = Preparation = Cooking = Total = Instructions = string.Empty;
            arr_ingredients = null;
            NutrientTable = null;
        }

        public string[] BreifNutrients = { "Calories", "Fat", "Carbs", "Protein", "Fiber", "Sugar", "Cholesterol", "Sodium" };
        private DataTable GetBriefNutrientList(DataTable NutrientList)
        {
            DataTable briefNutrientList = NutrientList.Clone();
            for (int i = 0; i < BreifNutrients.Length; i++)
            {
                NutrientList.Select("name = '" + BreifNutrients[i] + "'").CopyToDataTable(briefNutrientList, LoadOption.Upsert);


            }
            return briefNutrientList;
        }

        private DataTable GetDetailDataNutrientList(DataTable NutrientList, string category)
        {
            DataTable detailNutrientList = NutrientList.Copy();
            for (int i = 0; i < detailNutrientList.Rows.Count; i++)
            {
                if (detailNutrientList.Rows[i]["category"].ToString() != category)
                {
                    detailNutrientList.Rows.RemoveAt(i);
                    i--;
                }
            }
            return detailNutrientList;
        }

        public void GetSingleRecipe(string recipeid, string servings, string units, string commit, ICookBookService _cookbookservice, string accesstoken)
        {
            try
            {
                ClearSingleRecipe();
                //string recipeurl = @"http://69.65.19.37:8023/api/recipe/" + recipeid + "?type=xml";
                //string recipeurl = @"http://3nerds.services.procomarketing.com/recipes/" + recipeid;
                string recipeurl = @"http://69.65.19.37:8023/api/recipe/" + recipeid + "?type=xml";

                if (!string.IsNullOrEmpty(servings)) // && !string.IsNullOrEmpty(units)
                    recipeurl = recipeurl + "&servings=" + servings + "&unit=" + units + "&commit=Scale";
                else recipeurl = recipeurl + "&servings=2&unit=US&commit=Scale";

                byte[] singleRecipe = _cookbookservice.GetResponseFromUrl(accesstoken, recipeurl);
                MemoryStream recipeStream = new MemoryStream(singleRecipe);

                XElement xRecipe = XElement.Load(recipeStream);

                if (xRecipe != null)
                {
                    RecipeId = xRecipe.Attribute("id").Value;
                    Title = (xRecipe.Element("title") == null) ? string.Empty : xRecipe.Element("title").Value;
                    Serves = (xRecipe.Element("servings") == null) ? string.Empty : xRecipe.Element("servings").Value;
                    PhotoUrl = (xRecipe.Element("photo") == null) ? string.Empty : xRecipe.Element("photo").Value;

                    var timesEle = xRecipe.Element("times");
                    if (timesEle != null)
                    {
                        Preparation = (timesEle.Element("preparation") == null) ? string.Empty : timesEle.Element("preparation").Value;
                        Cooking = (timesEle.Element("cooking") == null) ? string.Empty : timesEle.Element("cooking").Value;
                        Total = (timesEle.Element("total") == null) ? string.Empty : timesEle.Element("total").Value;
                    }

                    this.arr_ingredients = new List<string>();
                    var ingredients = from p in xRecipe.Descendants("ingredients") select p;
                    var ingredientList = from p in ingredients.Descendants("ingredient") select p;
                    foreach (var ingredient in ingredientList)
                    {
                        this.arr_ingredients.Add(ingredient.Value);
                    }

                    Instructions = (xRecipe.Element("instructions") == null) ? string.Empty : xRecipe.Element("instructions").Value;

                    NutrientTable = CreateNutrientTable();
                    var nutrition = from p in xRecipe.Descendants("nutrition") select p;
                    var nutritionList = from p in nutrition.Descendants("nutrient") select p;
                    foreach (var nutritionItem in nutritionList)
                    {
                        NutrientTable.Rows.Add(new object[] { 
              nutritionItem.Attribute("category").Value,
              nutritionItem.Attribute("units").Value,
              GetFormattedText(nutritionItem.Attribute("name").Value),
              nutritionItem.Attribute("percent_rdi").Value,
              nutritionItem.Value
            });

                    }
                    this.briefNutrientList = GetBriefNutrientList(NutrientTable);

                    this.NutrientsDetailsTable = GetDetailDataNutrientList(NutrientTable, "proximates");
                    this.FatsDetailsTable = GetDetailDataNutrientList(NutrientTable, "lipids");
                    this.VitaminsDetailsTable = GetDetailDataNutrientList(NutrientTable, "vitamins");
                    this.MineralsDetailsTable = GetDetailDataNutrientList(NutrientTable, "minerals");


                }
            }
            catch (WebException wbEx)
            {
                throw wbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Shopping List


        private void ClearRecipeInfoForShoppingList()
        {
            RecipeId = Title = Preparation = Cooking = Total = Instructions = string.Empty;
            //arr_ingredients = null;
            //NutrientTable = null;
        }

        public string Calories { get; set; }
        public string Fat { get; set; }
        public string Carbs { get; set; }
        public string photoU { get; set; }
        public int GetRecipeInfoForShoppingList(string recipeid, string servings)
        {
            try
            {
                ClearRecipeInfoForShoppingList();
                string recipeurl = @"http://69.65.19.37:8023/api/RecipeShoppingList?type=xml&" + "id=" + recipeid;
                XElement xRecipe = XElement.Load(recipeurl);
                if (xRecipe != null)
                {
                    RecipeId = xRecipe.Element("Id").Value;
                    Title = (xRecipe.Element("Title") == null) ? string.Empty : xRecipe.Element("Title").Value;
                    photoU = (xRecipe.Element("Photo") == null) ? string.Empty : xRecipe.Element("Photo").Value;

                    Preparation = (xRecipe.Element("PreparationTime") == null) ? string.Empty : xRecipe.Element("PreparationTime").Value;
                    Cooking = (xRecipe.Element("CookTime") == null) ? string.Empty : xRecipe.Element("CookTime").Value;
                    int total = Convert.ToInt16(Preparation) + Convert.ToInt16(Cooking);
                    Total = total.ToString();
                    //(xRecipe.Element("total") == null) ? string.Empty : xRecipe.Element("total").Value;

                    //var nutrition = from p in xRecipe.Descendants("nutrition") select p;
                    var nutritionList = from p in xRecipe.Descendants("Nutrition") select p;
                    foreach (var nutritionItem in nutritionList)
                    {
                        XmlDocument xd = new XmlDocument();
                        xd.LoadXml("" + nutritionItem + "");

                        var fat = xd.SelectNodes("Nutrition/fat");
                        var carbs = xd.SelectNodes("Nutrition/carbs");
                        var calories = xd.SelectNodes("Nutrition/calories");


                        if (fat.ToString() != "")
                        {
                            double fatdbl = Convert.ToDouble(fat[0].InnerText);
                            int fatint = Convert.ToInt16(fatdbl);
                            Fat = fatint.ToString();
                        }
                        if (carbs.ToString() != "")
                        {
                            double carbsdbl = Convert.ToDouble(carbs[0].InnerText);
                            int carbint = Convert.ToInt16(carbsdbl);

                            Carbs = carbint.ToString();
                        }
                        if (calories.ToString() != "")
                        {
                            double caloriesdbl = Convert.ToDouble(calories[0].InnerText);
                            int caloriesint = Convert.ToInt16(caloriesdbl);

                            Calories = caloriesint.ToString();
                        }
                    }
                }
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public void GetShoppingListInfo(string store_id, string recipe_ids, string qs_specials_ids, string chainId, ICookBookService _cookbookservice, string accesstoken)
        {
            DataSet aislesSet = new DataSet();
            if (string.IsNullOrEmpty(store_id))
            {
                store_id = "4007";
            }

            //string recipeids = (recipe_ids == string.Empty) ? "" : "&recipe_ids=" + recipe_ids;
            string recipeids = (recipe_ids == string.Empty) ? "" : recipe_ids;
            if (recipe_ids == "" && qs_specials_ids == "")
            {
                AislesSet = null;
            }
            else
            {
                try
                {
                    //string url = "http://3nerds.services.procomarketing.com/shoppinglists.xml?store_id=" + store_id + recipeids + qs_specials_ids;


                    string url = "http://69.65.19.37:8023/api/ShoppingList?type=xml&";

                    if (!string.IsNullOrEmpty(recipeids))
                    {
                        url += "recipeId=" + recipeids + "&";
                    }

                    if (!string.IsNullOrEmpty(chainId))
                    {
                        url += "chainId=" + chainId + "&";
                    }

                    if (!string.IsNullOrEmpty(qs_specials_ids))
                    {
                        url += "specialIds=" + qs_specials_ids + "&";
                    }
                    if (!string.IsNullOrEmpty(store_id))
                    {
                        url += "storeId=" + store_id + "&";
                    }



                    // string url = "http://3nerds.services.procomarketing.com/shoppinglists.xml?store_id=4007&recipe_ids=2528,6323&special_ids[]=2105&special_ids[]=2112&special_ids[]=2113&special_ids[]=2719&special_ids[]=2841&special_ids[]=3177&special_ids[]=3184&special_ids[]=3179&special_ids[]=4700";
                    XElement xRecipe = XElement.Load(url);

                    //byte[] totalResponse = _cookbookservice.GetResponseFromUrl(accesstoken, url);


                    //MemoryStream stream = new MemoryStream(totalResponse);
                    //XDocument xRecipe = XDocument.Load(stream);

                    if (xRecipe != null)
                    {
                        Regex regex = new Regex("^[0-9]+$");
                        var specialDataList = from p in xRecipe.Descendants("ShoppingListSpecialModel") select p;
                        var aislesList = from p in xRecipe.Descendants("ShoppingListRecipeModel") select p;
                        string uniqueAilseName = "";
                        string uniqueAilseNameSpecial = "";
                        DataTable aisleTable = new DataTable();
                        foreach (var aisleItem in aislesList)
                        {

                            XmlDocument xd = new XmlDocument();
                            xd.LoadXml("" + aisleItem + "");
                            var Description = xd.SelectNodes("/ShoppingListRecipeModel/Description");
                            var amount = xd.SelectNodes("/ShoppingListRecipeModel/amount");
                            var recipeId = xd.SelectNodes("/ShoppingListRecipeModel/recipe_ids");
                            var ailseName = xd.SelectNodes("/ShoppingListRecipeModel/aisle_name");




                            var currentAilseName = ailseName[0].InnerText.ToString();
                            if (uniqueAilseName != currentAilseName)
                            {
                                uniqueAilseName = currentAilseName;
                                aisleTable = CreateAislesTable(uniqueAilseName);
                            }

                            var recipeDetails = amount[0].InnerText.ToString() + " " + Description[0].InnerText.ToString();
                            var singleRecipeId = recipeId[0].InnerText.ToString();

                            aisleTable.Rows.Add(new object[] { 
                            (Convert.ToInt16(singleRecipeId)),
                            (""),
                            (""),
                            (recipeDetails),
                            ("")
                            });

                            for (int i = 0; i < aisleTable.Rows.Count; i++)
                            {

                                string val = aisleTable.Rows[i]["value"].ToString().Split(' ').Last();

                                int splitedLength = aisleTable.Rows[i]["value"].ToString().Split(' ').Count();
                                int splitedLengthpre = 0;
                                int amountOfValue = 0;
                                if (!aisleTable.Rows[i]["value"].ToString().Split(' ').First().Contains('/'))
                                {
                                    string toConverint = aisleTable.Rows[i]["value"].ToString().Split(' ').First();
                                    if (regex.IsMatch(toConverint))
                                    {
                                        if (!string.IsNullOrEmpty(toConverint))
                                        {
                                            amountOfValue = Convert.ToInt16(toConverint);
                                        }
                                    }
                                }
                                string val1 = "";
                                int amountOfvaliePre = 0;
                                if (i > 0)
                                {
                                    val1 = aisleTable.Rows[i - 1]["value"].ToString().Split(' ').Last();
                                    if (!aisleTable.Rows[i - 1]["value"].ToString().Split(' ').First().Contains('/'))
                                    {
                                        string toConverintPre = aisleTable.Rows[i - 1]["value"].ToString().Split(' ').First();
                                        splitedLengthpre = aisleTable.Rows[i - 1]["value"].ToString().Split(' ').Count();
                                        if (regex.IsMatch(toConverintPre))
                                        {
                                            if (!string.IsNullOrEmpty(toConverintPre))
                                            {
                                                amountOfvaliePre = Convert.ToInt16(toConverintPre);
                                            }
                                        }
                                    }
                                }
                                if (val == val1)
                                {
                                    if (splitedLengthpre == splitedLength)
                                    {
                                        amountOfValue = amountOfValue + amountOfvaliePre;
                                        aisleTable.Rows[i].Delete();
                                        aisleTable.Rows[i - 1]["value"] = amountOfValue.ToString() + " " + val;
                                    }
                                }
                            }

                            if (aislesSet.Tables.Count > 0)
                            {
                                for (int i = 0; i < aislesSet.Tables.Count; i++)
                                {
                                    if (aislesSet.Tables[i].TableName != currentAilseName)
                                    {
                                        bool isTable = aislesSet.Tables.Contains("" + currentAilseName + "");
                                        if (!isTable)
                                        {
                                            aislesSet.Tables.Add(aisleTable);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                aislesSet.Tables.Add(aisleTable);
                            }

                        }

                        bool isTableSpecialExist = true;
                        foreach (var specialData in specialDataList)
                        {
                            XmlDocument specialDoc = new XmlDocument();
                            specialDoc.LoadXml("" + specialData + "");

                            var specialDescription = specialDoc.SelectNodes("/ShoppingListSpecialModel/description");
                            var ailseSpecial = specialDoc.SelectNodes("/ShoppingListSpecialModel/aisle_name");
                            var specialId = specialDoc.SelectNodes("/ShoppingListSpecialModel/special_id");
                            var offer = specialDoc.SelectNodes("/ShoppingListSpecialModel/offer");


                            var specialDetails = specialDescription.Count == 0 ? "" : specialDescription[0].InnerText.ToString();
                            var ailseSpecialName = ailseSpecial.Count == 0 ? "" : ailseSpecial[0].InnerText.ToString();
                            var specialIdText = specialId.Count == 0 ? "" : specialId[0].InnerText.ToString();
                            var specialOffer = offer.Count == 0 ? "" : offer[0].InnerText.ToString();

                            if (aislesSet.Tables.Count == 0)
                            {
                                //aislesSet.Tables.Add(ailseSpecialName.ToString());
                                aisleTable = CreateAislesTable(ailseSpecialName.ToString());
                                aislesSet.Tables.Add(aisleTable);
                            }

                            for (int i = 0; i < aislesSet.Tables.Count; i++)
                            {
                                if (aislesSet.Tables[i].ToString() == ailseSpecialName)
                                {
                                    aislesSet.Tables[i].Rows.Add(new object[] { 
                                (""),
                                (""),
                                (Convert.ToInt64(specialIdText)),
                                (specialDetails),
                                (specialOffer)
                                });
                                }
                                else
                                {
                                    if (uniqueAilseNameSpecial != ailseSpecialName)
                                    {
                                        isTableSpecialExist = aislesSet.Tables.Contains("" + ailseSpecialName + "");
                                        if (!isTableSpecialExist)
                                        {
                                            uniqueAilseName = ailseSpecialName;
                                            aisleTable = CreateAislesTable(uniqueAilseName);
                                        }
                                    }
                                    else
                                    {
                                        aislesSet.Tables[i].Rows.Add(new object[] { 
                                            (""),
                                            (""),
                                            (Convert.ToInt64(specialIdText)),
                                            (specialDetails),
                                            (specialOffer)
                                        });
                                    }
                                    if (!isTableSpecialExist)
                                    {
                                        aislesSet.Tables.Add(aisleTable);
                                    }
                                }
                            }
                        }

                    }
                    AislesSet = aislesSet;
                }
                catch
                {
                    AislesSet = null;
                }
            }
        }


        private DataTable CreateAislesTable(string tableName)
        {
            DataTable dt = new DataTable(tableName);
            dt.Columns.Add("recipe_ids");
            dt.Columns.Add("source");
            dt.Columns.Add("special_id");
            dt.Columns.Add("value");
            dt.Columns.Add("Offer");
            return dt;
        }
        #endregion
    }
}
