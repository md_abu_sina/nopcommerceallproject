﻿using Nop.Plugin.RecipeNew.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Services.Events;

namespace Nop.Plugin.RecipeNew.Services 
{
    public class RecipeCalenderService : IRecipeCalenderService
    {
        
        private readonly IRepository<RecipeCalender> _RecipeCalenderRepository;
        private readonly IEventPublisher _eventPublisher;

        public RecipeCalenderService(IRepository<RecipeCalender> RecipeCalenderRepository, IEventPublisher eventPublisher)
         {
             _RecipeCalenderRepository = RecipeCalenderRepository;
             _eventPublisher = eventPublisher;
        }

        #region Implementation of IRecipeCalenderService

        /// <summary>
        /// Get All Recipe Calender
        /// </summary>
        /// <returns></returns>
        public List<RecipeCalender> GetAllRecipeCalender()
        {
           
            List<RecipeCalender> recipesInCalender = (from u in _RecipeCalenderRepository.Table
                                        
                                        select u).ToList();
            return recipesInCalender;
        }

        /// <summary>
        /// Inserts  recipe to a Day in calender
        /// </summary>
        /// <param name="recipecalenderItem">recipecalenderItem</param>
        public virtual void InsertRecipetoADay(RecipeCalender recipecalenderItem)
        {
            if (recipecalenderItem == null)
                throw new ArgumentNullException("recipecalenderItem");

            _RecipeCalenderRepository.Insert(recipecalenderItem);

            //event notification
            _eventPublisher.EntityInserted(recipecalenderItem);
        }

        /// <summary>
        /// get Recipe by date
        /// </summary>
        /// <param name="date">date</param>
        public RecipeCalender GetRecipeBydate(DateTime date)
        {
            var db = _RecipeCalenderRepository;
            return db.Table.SingleOrDefault(x => x.Date == date);
        }

        /// <summary>
        /// get Recipe by date
        /// </summary>
        /// <param name="date">date</param>
        public RecipeCalender GetLastAddedRecipe()
        {
            var db = _RecipeCalenderRepository;
       
            
            var lastRecipe =    (from rc in db.Table 
                                    orderby rc.Date 
                                    select rc).FirstOrDefault();
            return lastRecipe;
        }

        /// <summary>
        /// delete recipe calender item from recipe calender
        /// </summary>
        /// <param name="date">date</param>
        public void DeleteRecipeCalenderItem(DateTime date)
        {

            RecipeCalender recipecalenderItem = GetRecipeBydate(date);

            _RecipeCalenderRepository.Delete(recipecalenderItem);

            //event notification
            _eventPublisher.EntityDeleted(recipecalenderItem);

        }
        #endregion

    }
}
