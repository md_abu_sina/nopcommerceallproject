﻿using Nop.Plugin.RecipeNew.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Services.Events;

namespace Nop.Plugin.RecipeNew.Services
{
    public class ShoppinglistOwnItemsService : IShoppinglistOwnItemsService
    {
         private readonly IRepository<ShoppinglistOwnItems> _shoppinglistOwnItemsRepository;
         private readonly IEventPublisher _eventPublisher;

         public ShoppinglistOwnItemsService(IRepository<ShoppinglistOwnItems> shoppinglistOwnItemsRepository, IEventPublisher eventPublisher)
         {
             _shoppinglistOwnItemsRepository = shoppinglistOwnItemsRepository;
             _eventPublisher = eventPublisher;
         }


        #region Implementation of IShoppinglistOwnItemsService

        /// <summary>
         /// get all shoppinglistuserownitems by customer id
         /// </summary>
         /// <param name="customerID">customerID</param>
         public IList<ShoppinglistOwnItems> GetAllshoppinglistuserownitemsByCustomerID(int customerID)
         {
             var context = _shoppinglistOwnItemsRepository;
             //return db.RecipeCookbook.Where( SingleOrDefault(x => x.CustomerID == customerID);
             List<ShoppinglistOwnItems> ownItems = (from u in _shoppinglistOwnItemsRepository.Table
                                         where u.CustomerID == customerID
                                         select u).ToList();
             return ownItems;
         }

         /// <summary>
         /// Inserts a users Own item
         /// </summary>
         /// <param name="userownitem">userownitem</param>
         public virtual void InsertShoppinglistOwnItems(ShoppinglistOwnItems userownitem)
         {
             if (userownitem == null)
                 throw new ArgumentNullException("userownitem");

             _shoppinglistOwnItemsRepository.Insert(userownitem);

             //event notification
             _eventPublisher.EntityInserted(userownitem);
         }


         /// <summary>
         /// delete item from 
         /// </summary>
         /// <param name="ShoppinglistOwnItems">userownitem</param>
         public void DeleteshoppinglistOwnItem(ShoppinglistOwnItems userownitem)
         {
             if (userownitem == null)
                 return;
             // ShoppinglistOwnItems userownitem = GetCookbookById(shoppinglistOwnItemsId);

              _shoppinglistOwnItemsRepository.Delete(userownitem);

             //event notification
              _eventPublisher.EntityDeleted(userownitem);

         }

         /// <summary>
         /// get shoppinglistuseritem by id
         /// </summary>
         /// <param name="userItemId">userItemId</param>
         public ShoppinglistOwnItems GetshoppinglistuseritemById(int userItemId)
         {
             var db = _shoppinglistOwnItemsRepository;
             return db.Table.SingleOrDefault(x => x.Id == userItemId);
         } 

        #endregion
    }
}
