﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.OrderForm
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("OrderForm", "OrderForm",
                            new { controller = "OrderForm", action = "ShowForm" },
                            new[] { "Nop.Plugin.OrderForm.Controllers" });

            routes.MapRoute("SubmitOrderForm", "Plugin/OrderForm/SubmitOrderForm",
                            new { controller = "OrderForm", action = "SubmitOrderForm" },
                            new[] { "Nop.Plugin.OrderForm.Controllers" });

            routes.MapRoute("installMessageTemplateForOrderForm", "installMessageTemplateForOrderForm",
                new { controller = "OrderForm", action = "installOrderFormMessageTemplates" },
                                            new[] { "Nop.Plugin.OrderForm.Controllers" });

					/*routes.MapRoute("EmailSent", "t/{SystemName}",
													new { controller = "Topic", action = "TopicDetails" },
													new[] { "Nop.Web.Controllers" });
					routes.MapLocalizedRoute("Topic","t/{SystemName}",
													new { controller = "Topic", action = "TopicDetails" },
													new[] { "Nop.Web.Controllers" });*/
        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}