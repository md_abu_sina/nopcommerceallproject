﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Other.AnotherBlog
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
					routes.MapRoute("Plugin.Other.AnotherBlog",
                            "blog/{blogType}",
                            new { controller = "AnotherBlog", action = "List", blogType = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" });

					/*routes.MapRoute("Admin.Plugin.Other.AnotherBlog.Campaign", "Admin/Plugins/AnotherBlog/Admin/Blog/List",//"Admin/Plugin/Other/AnotherBlog/List"
                            new { controller = "AnotherBlog", action = "Posts", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" }).DataTokens.Add("Area", "Admin");*/
					routes.MapRoute("Plugin.Other.AnotherBlog.BlogByTag",
														"blog/tag/{tag}/{blogType}",
                            new { controller = "AnotherBlog", action = "BlogByTag", blogType = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" });
					routes.MapRoute("Plugin.Other.AnotherBlog.BlogByMonth",
														"blog/month/{month}/{blogType}",
                            new { controller = "AnotherBlog", action = "BlogByMonth", blogType = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" });
					routes.MapRoute("Plugin.Other.AnotherBlog.BlogRSS",
														"blog/rss/{languageId}/{blogType}",
                            new { controller = "AnotherBlog", action = "ListRss", blogType = UrlParameter.Optional },
                            new { languageId = @"\d+" },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" });
					routes.MapRoute("Plugin.Other.AnotherBlog.BlogPost",
														"blog/{blogPostId}/{SeName}/{blogType}",
                            new { controller = "AnotherBlog", action = "BlogPost", SeName = UrlParameter.Optional, blogType = UrlParameter.Optional },
                            new { blogPostId = @"\d+" },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" });

						routes.MapRoute("Admin.Plugin.Other.AnotherBlog.AdminList", "Admin/Plugin/Other/AnotherBlog/AdminList",
                            new { controller = "AnotherBlogAdmin", action = "AdminList" },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" }).DataTokens.Add("area", "admin");
						routes.MapRoute("Admin.Plugin.Other.AnotherBlog.Create", "Admin/Plugin/Other/AnotherBlog/Create",
                            new { controller = "AnotherBlogAdmin", action = "Create"},
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" }).DataTokens.Add("area", "admin");
						routes.MapRoute("Admin.Plugin.Other.AnotherBlog.Delete", "Admin/Plugin/Other/AnotherBlog/Delete/{id}",
                            new { controller = "AnotherBlogAdmin", action = "Delete", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" }).DataTokens.Add("area", "admin");
						routes.MapRoute("Admin.Plugin.Other.AnotherBlog.Edit", "Admin/Plugin/Other/AnotherBlog/Edit/{id}",
                            new { controller = "AnotherBlogAdmin", action = "Edit", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Other.AnotherBlog.Controllers" }).DataTokens.Add("area", "admin");
					
					
        }

        public int Priority
        {
            get { return 3; }
        }

        #endregion
    }
}