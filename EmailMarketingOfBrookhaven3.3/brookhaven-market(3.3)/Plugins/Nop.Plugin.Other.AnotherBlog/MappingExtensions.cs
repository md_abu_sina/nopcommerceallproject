﻿using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Plugin.Other.AnotherBlog.Models;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Stores;
using Nop.Admin.Models.Stores;

namespace Nop.Plugin.Other.AnotherBlog
{
    public static class MappingExtensions
    {

				#region Another Blog

				public static BlogPostAdminModel ToModel(this BlogPost entity)
				{
					BlogPostAdminModel anotherBlogPostAdminModel = new BlogPostAdminModel();

					anotherBlogPostAdminModel.Id = entity.Id;
					anotherBlogPostAdminModel.Title = entity.Title;
					anotherBlogPostAdminModel.Body = entity.Body;
					anotherBlogPostAdminModel.AllowComments = entity.AllowComments;
					//anotherBlogPostAdminModel.Comments = entity.CommentCount;
					anotherBlogPostAdminModel.CreatedOn = entity.CreatedOnUtc;
					anotherBlogPostAdminModel.StartDate = entity.StartDateUtc;
					anotherBlogPostAdminModel.EndDate = entity.EndDateUtc;
					anotherBlogPostAdminModel.Tags = entity.Tags;
					anotherBlogPostAdminModel.LanguageId = entity.LanguageId;
					anotherBlogPostAdminModel.MetaTitle = entity.MetaTitle;
					anotherBlogPostAdminModel.MetaDescription = entity.MetaDescription;
					anotherBlogPostAdminModel.MetaKeywords = entity.MetaKeywords;

					return anotherBlogPostAdminModel;
				}

				public static BlogPost ToEntity(this BlogPostAdminModel model)
				{
					BlogPost entity = new BlogPost();

					entity.Id = model.Id;
					entity.Title = model.Title;
					entity.Body = model.Body;
					entity.AllowComments = model.AllowComments;
					entity.Tags = model.Tags;
					entity.LanguageId = model.LanguageId;
					entity.MetaTitle = model.MetaTitle;
					entity.MetaDescription = model.MetaDescription;
					entity.MetaKeywords = model.MetaKeywords;

					return entity;
				}

				public static BlogPost ToEntity(this BlogPostAdminModel model, BlogPost destination)
				{


					destination.Id = model.Id;
					destination.Title = model.Title;
					destination.Body = model.Body;
					destination.AllowComments = model.AllowComments;
					destination.Tags = model.Tags;
					destination.LanguageId = model.LanguageId;
					destination.MetaTitle = model.MetaTitle;
					destination.MetaDescription = model.MetaDescription;
					destination.MetaKeywords = model.MetaKeywords;

					return destination;
				}

				public static BlogPost ToBlogPost(this BlogPost anotherBlogPost)
				{
					BlogPost entity = new BlogPost();

					entity.Id = anotherBlogPost.Id;
					
					/*entity.Name = model.Name;
					entity.Phone = model.Phone;
					entity.Email = model.Email;
					entity.GuestNumbers = model.GuestNumbers;
					entity.BookingDate = model.BookingDate;*/

					return entity;
				}

				public static BlogComment ToBlogComment(this BlogComment BlogComment)
				{
					BlogComment entity = new BlogComment();

					entity.Id = BlogComment.Id;

					/*entity.Name = model.Name;
					entity.Phone = model.Phone;
					entity.Email = model.Email;
					entity.GuestNumbers = model.GuestNumbers;
					entity.BookingDate = model.BookingDate;*/

					return entity;
				}

				#endregion

				public static StoreModel ToModel(this Store entity)
				{
					StoreModel storeModel = new StoreModel();

					storeModel.Id = entity.Id;
					storeModel.Name = entity.Name;
					storeModel.SecureUrl = entity.SecureUrl;
					storeModel.SslEnabled = entity.SslEnabled;
					storeModel.Url = entity.Url;
					storeModel.Hosts = entity.Hosts;
					storeModel.DisplayOrder = entity.DisplayOrder;

					return storeModel;
				}

    }
}