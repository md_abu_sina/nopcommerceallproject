using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Common;
using Nop.Core.Events;
using Nop.Services.Events;

namespace Nop.Plugin.Other.AnotherBlog.Services
{
    /// <summary>
    /// Blog service
    /// </summary>
    public partial class AnotherBlogService : IAnotherBlogService
    {
        #region Constants
        private const string BLOGPOST_BY_ID_KEY = "Nop.blogpost.id-{0}";
        private const string BLOGPOST_PATTERN_KEY = "Nop.blogpost.";
        #endregion

        #region Fields

				private readonly IRepository<BlogPost> _blogPostRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
				private readonly IRepository<GenericAttribute> _genericAttributeRepository;//Added by Razib Mahmud Brainstation-23

        #endregion

        #region Ctor

				public AnotherBlogService(IRepository<BlogPost> blogPostRepository, IRepository<GenericAttribute> genericAttributeRepository, ICacheManager cacheManager, IEventPublisher eventPublisher)
        {
            _blogPostRepository = blogPostRepository;
						_genericAttributeRepository = genericAttributeRepository;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        

        /// <summary>
        /// Gets all blog posts
        /// </summary>
        /// <param name="languageId">Language identifier; 0 if you want to get all records</param>
        /// <param name="dateFrom">Filter by created date; null if you want to get all records</param>
        /// <param name="dateTo">Filter by created date; null if you want to get all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Blog posts</returns>
				public virtual IPagedList<BlogPost> GetAllBlogPosts(int languageId,
						DateTime? dateFrom, DateTime? dateTo, int pageIndex, int pageSize, bool showHidden = false, string blogType = "", string SearchKey = "")
        {
            var query = _blogPostRepository.Table;
						var genericAttribute = _genericAttributeRepository.Table;
            if (dateFrom.HasValue)
                query = query.Where(b => dateFrom.Value <= b.CreatedOnUtc);
            if (dateTo.HasValue)
                query = query.Where(b => dateTo.Value >= b.CreatedOnUtc);
            if (languageId > 0)
                query = query.Where(b => languageId == b.LanguageId);
            if (!showHidden)
            {
                var utcNow = DateTime.UtcNow;
                query = query.Where(b => !b.StartDateUtc.HasValue || b.StartDateUtc <= utcNow);
                query = query.Where(b => !b.EndDateUtc.HasValue || b.EndDateUtc >= utcNow);
            }

						//Added By Razib Mahmud Brainstation-23	
						//if(blogType.Equals("Default"))
							//query = query.Where(posts => !genericAttribute.Any(ga => ga.EntityId == posts.Id && ga.KeyGroup.Equals("BlogPost")));
						if(blogType.Equals("2"))
							query = query.Where(posts => genericAttribute.Any(ga => ga.EntityId == posts.Id && ga.KeyGroup.Equals("BlogPost")));
						if(!String.IsNullOrEmpty(SearchKey))
						{
							query = query.Where(posts => posts.Title.Contains(SearchKey) || posts.Body.Contains(SearchKey) || genericAttribute.Any(ga => ga.EntityId == posts.Id && ga.KeyGroup.Equals("BlogPost") && ga.Key.Equals("ShortDescription") && ga.Value.Contains(SearchKey)));
							//query = query.Where(posts => posts.Title.Contains(SearchKey) || posts.Body.Contains(SearchKey));
						}

            query = query.OrderByDescending(b => b.CreatedOnUtc);

						var blogPosts = new PagedList<BlogPost>(query, pageIndex, pageSize);
            return blogPosts;

        }

        /// <summary>
        /// Gets all blog posts
        /// </summary>
        /// <param name="languageId">Language identifier. 0 if you want to get all news</param>
        /// <param name="tag">Tag</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Blog posts</returns>
				public virtual IPagedList<BlogPost> GetAllBlogPostsByTag(int languageId, string tag,
						int pageIndex, int pageSize, bool showHidden = false, string blogType = "")// Parameter added by Razib Mahmud Brainstation-23
				{
					tag = tag.Trim();

					//we laod all records and only then filter them by tag
					//Added By Razib Mahmud Brainstation-23	
					IPagedList<BlogPost> blogPostsAll;

					if(blogType.Equals("Default"))
					{
						blogPostsAll = GetAllBlogPosts(languageId, null, null, 0, int.MaxValue, showHidden, "Default");
					}
					else if(blogType.Equals("2"))
					{
						blogPostsAll = GetAllBlogPosts(languageId, null, null, 0, int.MaxValue, showHidden, "2");
					}
					else
					{
						blogPostsAll = GetAllBlogPosts(languageId, null, null, 0, int.MaxValue, showHidden, "");
					}

					var taggedBlogPosts = new List<BlogPost>();
					foreach(var blogPost in blogPostsAll)
					{
						var tags = blogPost.ParseTags();
						if(!String.IsNullOrEmpty(tags.FirstOrDefault(t => t.Equals(tag, StringComparison.InvariantCultureIgnoreCase))))
							taggedBlogPosts.Add(blogPost);
					}

					//server-side paging
					var result = new PagedList<BlogPost>(taggedBlogPosts, pageIndex, pageSize);
					return result;
				}

				
				/// <summary>
				/// Gets all blog post tags
				/// </summary>
				/// <param name="languageId">Language identifier. 0 if you want to get all news</param>
				/// <param name="showHidden">A value indicating whether to show hidden records</param>
				/// <returns>Blog post tags</returns>
				public virtual IList<BlogPostTag> GetAllBlogPostTags(int languageId, bool showHidden = false, string blogType = "Default")
				{
					var blogPostTags = new List<BlogPostTag>();

					var blogPosts = GetAllBlogPosts(languageId, null, null, 0, int.MaxValue, showHidden, blogType);
					foreach(var blogPost in blogPosts)
					{
						var tags = blogPost.ParseTags();
						foreach(string tag in tags)
						{
							var foundBlogPostTag = blogPostTags.Find(bpt => bpt.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
							if(foundBlogPostTag == null)
							{
								foundBlogPostTag = new BlogPostTag()
								{
									Name = tag,
									BlogPostCount = 1
								};
								blogPostTags.Add(foundBlogPostTag);
							}
							else
								foundBlogPostTag.BlogPostCount++;
						}
					}

					return blogPostTags;
				}

        #endregion
    }
}
