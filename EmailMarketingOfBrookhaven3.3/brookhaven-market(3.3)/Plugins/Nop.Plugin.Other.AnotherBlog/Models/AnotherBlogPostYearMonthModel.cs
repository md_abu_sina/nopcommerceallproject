﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Other.AnotherBlog.Models
{
    public partial class AnotherBlogPostYearModel : BaseNopModel
    {
				public AnotherBlogPostYearModel()
        {
						Months = new List<AnotherBlogPostMonthModel>();
        }
        public int Year { get; set; }
        public IList<AnotherBlogPostMonthModel> Months { get; set; }
    }
		public partial class AnotherBlogPostMonthModel : BaseNopModel
    {
        public int Month { get; set; }

        public int BlogPostCount { get; set; }
    }
}