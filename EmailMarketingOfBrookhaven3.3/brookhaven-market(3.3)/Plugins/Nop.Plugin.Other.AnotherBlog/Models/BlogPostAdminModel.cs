﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Nop.Web.Framework.Mvc;
using Nop.Web.Validators.Blogs;
using Nop.Web.Framework;
using System.Web.Mvc;
using Nop.Admin.Models.Blogs;

namespace Nop.Plugin.Other.AnotherBlog.Models
{
			//[Validator(typeof(AnotherBlogPostValidator))]
			public partial class BlogPostAdminModel : BlogPostModel
			{
			
					[NopResourceDisplayName("Admin.Plugin.Other.AnotherBlog.BlogPosts.Fields.ShortDescription")]
					public string ShortDescription { get; set; }


			}
}