﻿using System;
using System.Web.Mvc;
using Nop.Admin.Validators.News;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using FluentValidation.Attributes;
using Nop.Plugin.Events.Validators;
using System.Collections.Generic;
using Nop.Plugin.Events.Domain;
using Nop.Admin.Models.Customers;

namespace Nop.Plugin.Events.Models
{
    [Validator(typeof(EventItemValidator))]
    public class EventModel : BaseNopEntityModel
    {
        public int Id { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.Title")]
        [AllowHtml]
        public string Title { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.StoreName")]
        public string CustomerRoles { get; set; }

        public List<CustomerRoleModel> AllCustomerRoles { get; set; }

        public int[] SelectedCustomerRoleIds { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.DayTime")]
        public DateTime DayTime { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.ShortDescription")]
        [AllowHtml]
        public string ShortDescription { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.FullDescription")]
        [AllowHtml]
        public string FullDescription { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.Published")]
        public  bool Published { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.AllowComments")]
        public  bool AllowComments { get; set; }

				//[NopResourceDisplayName("Admin.ContentManagement.Events.Fields.Comments")]
				//[AllowHtml]
				//public ICollection<EventComment> EventComments { get; set; }

        public int totalComments { get; set; }

       

    }

    public class EventObjectModel
    {
        public string url { get; set; }
        public string title { get; set; }
        public string start { get; set; }
    }
    
  
}
