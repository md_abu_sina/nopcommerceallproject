﻿using Nop.Plugin.Events.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Plugin.Events.Services;
using Nop.Core;
using Nop.Services.Events;


namespace Nop.Plugin.Events.Services
{
    public class EventCommentService : IEventCommentService
    {
        #region fields

        private readonly IRepository<EventComment> _eventCommentRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region ctor

        public EventCommentService(IRepository<EventComment> eventCommentRepository, IEventPublisher eventPublisher)
        {
            _eventCommentRepository = eventCommentRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region implementation of IEventCommentService

        /// <summary>
        /// Get All Event Comment
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public virtual IPagedList<EventComment> GetAllEventComment(int pageIndex, int pageSize)
        {
            //var query = _eventRepository.Table;
            var query = (from u in _eventCommentRepository.Table
                         orderby u.createdOn
                         select u);
            var eventComments = new PagedList<EventComment>(query, pageIndex, pageSize);
            return eventComments;
        }

        /// <summary>
        /// Get All Event Comment By EventId
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="EventId"></param>
        /// <returns></returns>
        public virtual IPagedList<EventComment> GetAllEventCommentByEventId(int pageIndex, int pageSize, int EventId)
        {
            //var query = _eventRepository.Table;
            var query = (from u in _eventCommentRepository.Table
                         orderby u.createdOn
                         where u.EventId == EventId
                         select u);
            var eventComments = new PagedList<EventComment>(query, pageIndex, pageSize);
            return eventComments;
        }

				/// <summary>
				/// Numbers the of comments by event id.
				/// </summary>
				/// <param name="EventId">The event id.</param>
				/// <returns></returns>
				public virtual int NumberOfCommentsByEventId(int EventId)
				{
					//var query = _eventRepository.Table;
					var numberOfComment = (from u in _eventCommentRepository.Table
																 orderby u.createdOn
																 where u.EventId == EventId
																 select u).Count();
					//var eventComments = new PagedList<EventComment>(query, pageIndex, pageSize);
					return numberOfComment;
				}

        /// <summary>
        /// Get Event Comment By Event Comment Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>EventComment</returns>
        public EventComment GetEventCommentByEventCommentId(int Id)
        {
            var db = _eventCommentRepository;
            return db.Table.SingleOrDefault(x => x.Id == Id);
        }

        /// <summary>
        /// Delete Event Comment
        /// </summary>
        /// <param name="eventcommentItem"></param>
        public void DeleteEventComment(EventComment eventcommentItem)
        {
            _eventCommentRepository.Delete(eventcommentItem);
            _eventPublisher.EntityDeleted(eventcommentItem);
        }

        /// <summary>
        /// Insert Event Comment
        /// </summary>
        /// <param name="eventcommentItem"></param>
        public virtual void InsertEventComment(EventComment eventcommentItem)
        {
            if (eventcommentItem == null)
                throw new ArgumentNullException("eventcommentItem");

            _eventCommentRepository.Insert(eventcommentItem);

            //event notification
            _eventPublisher.EntityInserted(eventcommentItem);
        }
        #endregion

    }
}
