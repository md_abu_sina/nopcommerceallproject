﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Events.Domain;

namespace Nop.Plugin.Events.Data
{
    public partial class Event_customerRole_customMappingMap : EntityTypeConfiguration<Event_CustomerRole_CustomMapping>
    {
        public Event_customerRole_customMappingMap()
        {
            this.ToTable("Event_customerRole_customMapping");

            HasKey(ecrcmm => ecrcmm.Id);
            
            Property(ecrcmm => ecrcmm.EventId);
            Property(ecrcmm => ecrcmm.CustomerRoleId);

        }
    }
}
