﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Widgets.QuickView
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapRoute("Plugin.Widgets.QuickView.QuickViewBlock",
            //     "QuickViewBlock",
            //     new { controller = "WidgetsQuickView", action = "QuickViewBlock" },
            //     new[] { "Nop.Plugin.Widgets.QuickView.Controllers" }
            //);

            routes.MapRoute("Plugin.Widgets.QuickView.QuickViewBlock",
                 "qv/{productId}",
                 new { controller = "WidgetsQuickView", action = "QuickViewBlock" ,productId=UrlParameter.Optional},
                 new[] { "Nop.Plugin.Widgets.QuickView.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
