using System.Data.Entity.ModelConfiguration;
using CANet = Nop.Plugin.Payments.CustomAuthorizeNet.Domain;

namespace Nop.Plugin.Payments.CustomAuthorizeNet.Data

{
	public class CustomAuthorizeNetMap : EntityTypeConfiguration<CANet.CustomAuthorizeNet>
    {
				public CustomAuthorizeNetMap()
        {
						ToTable("Nop_CustomAuthorizeNet");

            //Map the primary key
            HasKey(m => m.Id);
            //Map the additional properties
						Property(m => m.StoreId);
						Property(m => m.UseSandbox);
						Property(m => m.TransactModeId);
						Property(m => m.TransactionKey);
						Property(m => m.LoginId);
						Property(m => m.AdditionalFeePercentage);
						Property(m => m.AdditionalFee);
        }
    }

}
