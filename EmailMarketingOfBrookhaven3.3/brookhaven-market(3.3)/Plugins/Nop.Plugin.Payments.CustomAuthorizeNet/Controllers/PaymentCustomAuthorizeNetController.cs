﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Plugin.Payments.CustomAuthorizeNet.Models;
using Nop.Plugin.Payments.CustomAuthorizeNet.Services;
using Nop.Plugin.Payments.CustomAuthorizeNet.Validators;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Payments.CustomAuthorizeNet.Controllers
{
    public class PaymentCustomAuthorizeNetController : BasePaymentController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
				private readonly ICustomerService _customerService;
				private readonly ICustomAuthorizeNetService _customAuthorizeNetService;

        public PaymentCustomAuthorizeNetController(IWorkContext workContext,
            IStoreService storeService, ICustomerService customerService,
            ISettingService settingService, ICustomAuthorizeNetService customAuthorizeNetService,
            ILocalizationService localizationService)
        {
            this._workContext = workContext;
            this._storeService = storeService;
            this._settingService = settingService;
            this._localizationService = localizationService;
						this._customerService = customerService;
						this._customAuthorizeNetService = customAuthorizeNetService;
        }
        
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(int StoreId = 0)
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var authorizeNetPaymentSettings = _settingService.LoadSetting<AuthorizeNetPaymentSettings>(storeScope);

            var model = new ConfigurationModel();


						model = (StoreId > 0 && _customAuthorizeNetService.GetcustomAuthorizeNetByStoreId(StoreId) != null) ?
											_customAuthorizeNetService.GetcustomAuthorizeNetByStoreId(StoreId).ToModel() : _customAuthorizeNetService.GetcustomAuthorizeNetFirst().ToModel();

						model.Stores = new SelectList(_customerService.GetCustomerRoleByStoreRole(), "ID", "SystemName", new
						{
							id = model.StoreId
						});
						
						model.TransactModeValues = authorizeNetPaymentSettings.TransactMode.ToSelectList();
						return View("Nop.Plugin.Payments.CustomAuthorizeNet.Views.PaymentAuthorizeNet.Configure", model);
						
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
                return Configure();

						var configuration = _customAuthorizeNetService.GetcustomAuthorizeNetByStoreId(model.StoreId);

						if(configuration != null)
						{
							configuration = model.ToEntity(configuration);
							_customAuthorizeNetService.UpdateCustomAuthorizeNet(configuration);
						}
						else
						{
							_customAuthorizeNetService.InsertCustomAuthorizeNet(model.ToEntity());
						}
            //load settings for a chosen store scope
						//var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
						//var authorizeNetPaymentSettings = _settingService.LoadSetting<AuthorizeNetPaymentSettings>(storeScope);

						////save settings
						//authorizeNetPaymentSettings.UseSandbox = model.UseSandbox;
						//authorizeNetPaymentSettings.TransactMode = (TransactMode)model.TransactModeId;
						//authorizeNetPaymentSettings.TransactionKey = model.TransactionKey;
						//authorizeNetPaymentSettings.LoginId = model.LoginId;
						//authorizeNetPaymentSettings.AdditionalFee = model.AdditionalFee;
						//authorizeNetPaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;

						///* We do not clear cache after each setting update.
						// * This behavior can increase performance because cached settings will not be cleared 
						// * and loaded from database after each update */
						//if (model.UseSandbox_OverrideForStore || storeScope == 0)
						//		_settingService.SaveSetting(authorizeNetPaymentSettings, x => x.UseSandbox, storeScope, false);
						//else if (storeScope > 0)
						//		_settingService.DeleteSetting(authorizeNetPaymentSettings, x => x.UseSandbox, storeScope);

						//if (model.TransactModeId_OverrideForStore || storeScope == 0)
						//		_settingService.SaveSetting(authorizeNetPaymentSettings, x => x.TransactMode, storeScope, false);
						//else if (storeScope > 0)
						//		_settingService.DeleteSetting(authorizeNetPaymentSettings, x => x.TransactMode, storeScope);

						//if (model.TransactionKey_OverrideForStore || storeScope == 0)
						//		_settingService.SaveSetting(authorizeNetPaymentSettings, x => x.TransactionKey, storeScope, false);
						//else if (storeScope > 0)
						//		_settingService.DeleteSetting(authorizeNetPaymentSettings, x => x.TransactionKey, storeScope);

						//if (model.LoginId_OverrideForStore || storeScope == 0)
						//		_settingService.SaveSetting(authorizeNetPaymentSettings, x => x.LoginId, storeScope, false);
						//else if (storeScope > 0)
						//		_settingService.DeleteSetting(authorizeNetPaymentSettings, x => x.LoginId, storeScope);

						//if (model.AdditionalFee_OverrideForStore || storeScope == 0)
						//		_settingService.SaveSetting(authorizeNetPaymentSettings, x => x.AdditionalFee, storeScope, false);
						//else if (storeScope > 0)
						//		_settingService.DeleteSetting(authorizeNetPaymentSettings, x => x.AdditionalFee, storeScope);

						//if (model.AdditionalFeePercentage_OverrideForStore || storeScope == 0)
						//		_settingService.SaveSetting(authorizeNetPaymentSettings, x => x.AdditionalFeePercentage, storeScope, false);
						//else if (storeScope > 0)
						//		_settingService.DeleteSetting(authorizeNetPaymentSettings, x => x.AdditionalFeePercentage, storeScope);

						////now clear settings cache
						//_settingService.ClearCache();

            return Configure();
        }

        [ChildActionOnly]
				public ActionResult PaymentInfo(int StoreId = 0)
        {
            var model = new PaymentInfoModel();
            
            //CC types
            model.CreditCardTypes.Add(new SelectListItem()
                {
                    Text = "Visa",
                    Value = "Visa",
                });
            model.CreditCardTypes.Add(new SelectListItem()
            {
                Text = "Master card",
                Value = "MasterCard",
            });
            model.CreditCardTypes.Add(new SelectListItem()
            {
                Text = "Discover",
                Value = "Discover",
            });
            model.CreditCardTypes.Add(new SelectListItem()
            {
                Text = "Amex",
                Value = "Amex",
            });
            
            //years
            for (int i = 0; i < 15; i++)
            {
                string year = Convert.ToString(DateTime.Now.Year + i);
                model.ExpireYears.Add(new SelectListItem()
                {
                    Text = year,
                    Value = year,
                });
            }

            //months
            for (int i = 1; i <= 12; i++)
            {
                string text = (i < 10) ? "0" + i.ToString() : i.ToString();
                model.ExpireMonths.Add(new SelectListItem()
                {
                    Text = text,
                    Value = i.ToString(),
                });
            }

            //set postback values
            var form = this.Request.Form;
            model.CardholderName = form["CardholderName"];
            model.CardNumber = form["CardNumber"];
            model.CardCode = form["CardCode"];
            var selectedCcType = model.CreditCardTypes.FirstOrDefault(x => x.Value.Equals(form["CreditCardType"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedCcType != null)
                selectedCcType.Selected = true;
            var selectedMonth = model.ExpireMonths.FirstOrDefault(x => x.Value.Equals(form["ExpireMonth"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedMonth != null)
                selectedMonth.Selected = true;
            var selectedYear = model.ExpireYears.FirstOrDefault(x => x.Value.Equals(form["ExpireYear"], StringComparison.InvariantCultureIgnoreCase));
            if (selectedYear != null)
                selectedYear.Selected = true;

            return View("Nop.Plugin.Payments.CustomAuthorizeNet.Views.PaymentAuthorizeNet.PaymentInfo", model);
        }

        [NonAction]
        public override IList<string> ValidatePaymentForm(FormCollection form)
        {
            var warnings = new List<string>();

            //validate
            var validator = new PaymentInfoValidator(_localizationService);
            var model = new PaymentInfoModel()
            {
                CardholderName = form["CardholderName"],
                CardNumber = form["CardNumber"],
                CardCode = form["CardCode"],
                ExpireMonth = form["ExpireMonth"],
                ExpireYear = form["ExpireYear"]
            };
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
                foreach (var error in validationResult.Errors)
                    warnings.Add(error.ErrorMessage);
            return warnings;
        }

        [NonAction]
        public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
        {
            var paymentInfo = new ProcessPaymentRequest();
            paymentInfo.CreditCardType = form["CreditCardType"];
            paymentInfo.CreditCardName = form["CardholderName"];
            paymentInfo.CreditCardNumber = form["CardNumber"];
            paymentInfo.CreditCardExpireMonth = int.Parse(form["ExpireMonth"]);
            paymentInfo.CreditCardExpireYear = int.Parse(form["ExpireYear"]);
            paymentInfo.CreditCardCvv2 = form["CardCode"];
            return paymentInfo;
        }
    }
}