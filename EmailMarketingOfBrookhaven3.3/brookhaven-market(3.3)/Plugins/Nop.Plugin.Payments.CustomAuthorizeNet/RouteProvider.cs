﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Payments.CustomAuthorizeNet
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
						routes.MapRoute("Admin.Plugin.Payments.CustomAuthorizeNet.Configure", "Admin/Plugin/Payments/CustomAuthorizeNet/Configure/{id}",
                          new { controller = "PaymentCustomAuthorizeNet", action = "Configure", id = UrlParameter.Optional },
														new[] { "Nop.Plugin.Payments.CustomAuthorizeNet.Controllers" }).DataTokens.Add("area", "admin");

        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}