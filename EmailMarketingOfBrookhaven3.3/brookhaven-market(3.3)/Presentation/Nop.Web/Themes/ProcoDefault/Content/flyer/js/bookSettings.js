flippingBook.pages = [
	"/themes/ProcoDefault/Content/flyer/pages/page1.jpg",
	"/themes/ProcoDefault/Content/flyer/pages/page2.jpg",
	"/themes/ProcoDefault/Content/flyer/pages/page3.jpg",
	"/themes/ProcoDefault/Content/flyer/pages/page4.jpg",


];


flippingBook.contents = [
	/*[ "Specials", 1 ],
	[ "Produce / Meat", 2 ],
	[ "Bulk Shop / Floral", 3 ],
	[ "Dairy / Frozen", 4 ],
	[ "Seafood", 5 ],
	[ "Deli / Wine & Spirits", 6 ],
	[ "Holiday Catering", 8 ],
	[ "Deli / Prepared Food", 9 ],
	[ "Wine / Cheese", 10 ],
	[ "Bakery", 11 ],
	[ "Holiday Catering", 12 ] */
	
	[ "Specials", 1 ],
	[ "Grocery / Meat & Deli", 2 ],
	[ "Fresh produce / Bakery", 3 ],
	[ "Fresh produce / Bakery", 4 ],	
	[ "Frozen", 5 ],
	[ "Deli / Wine & Spirits", 6 ],
	[ "Catering", 7 ],
	
];

// define custom book settings here
flippingBook.settings.bookWidth = 1000;
flippingBook.settings.bookHeight = 600;
flippingBook.settings.pageBackgroundColor = 0x006699;
flippingBook.settings.backgroundColor = 0xAABCD7;
flippingBook.settings.zoomUIColor = 0x919d6c;
flippingBook.settings.useCustomCursors = false;
flippingBook.settings.dropShadowEnabled = false;
flippingBook.settings.zoomImageWidth = 1024;
flippingBook.settings.zoomImageHeight = 1177;
flippingBook.settings.downloadURL = "/themes/ProcoDefault/Content/flyer/pdf/flyer.pdf";
flippingBook.settings.flipSound = "";
flippingBook.settings.flipCornerStyle = "first page only";
flippingBook.settings.zoomHintEnabled = true;

// default settings can be found in the flippingbook.js file
flippingBook.create();
