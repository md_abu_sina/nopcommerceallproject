public string dataMigrate()
        {
            string show = null;
            if (_workContext.CurrentCustomer.IsAdmin())
            {
                StreamReader sr = new StreamReader(Server.MapPath("/Plugins/Recipe/Content/caputos-import2.csv"));
                string inputLine = "";


                var j = 0;

                String[] values = null;
                while ((inputLine = sr.ReadLine()) != null)
                {
                    if (j > 0)
                    {
                        values = inputLine.Split(',');

                        show += values;
                        Customer customer = new Customer()
                       {

                           CustomerGuid = Guid.NewGuid(),
                           Email = values[1],
                           Username = values[0],
                           Password = values[2].Trim(),

                           Active = true,
                           CreatedOnUtc = DateTime.UtcNow,
                           LastActivityDateUtc = DateTime.UtcNow,
                           TimeZoneId = "Eastern Standard Time"
                       };
                        var registrationRequest = new CustomerRegistrationRequest(customer, customer.Email,
                        customer.Username, customer.Password, PasswordFormat.Hashed, customer.Active, "5");//addision
                        var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
                        if (registrationResult.Success)
                        {
                            //form fields


                            _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.FirstName, values[3]);
                            _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.LastName, values[4]);



                            _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.StreetAddress, values[5]);

                            _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, values[6]);

                            _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, values[9]);

                            _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.City, values[7]);
                            _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.CountryId, "1");


                            try
                            {
                                StateProvince state = _stateproviceservice.GetStateProvinceByAbbreviation(values[8]);
                                _customerContext.SaveCustomerAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, state.Id);
                            }
                            catch
                            {

                            }

                            ////insert default address (if possible)
                            //var defaultAddress = new Address()
                            //{
                            //    FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
                            //    LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
                            //    Email = customer.Email,
                            //    // Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
                            //    CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0 ?
                            //        (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) : null,
                            //    StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0 ?
                            //        (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) : null,
                            //    City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City),
                            //    Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
                            //    Address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
                            //    ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
                            //  //  PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                            //    // FaxNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
                            //    // CreatedOnUtc = customer.CreatedOnUtc
                            //};
                            //if (this._addressService.IsAddressValid(defaultAddress))
                            //{
                            //    //some validation
                            //    if (defaultAddress.CountryId == 0)
                            //        defaultAddress.CountryId = null;
                            //    if (defaultAddress.StateProvinceId == 0)
                            //        defaultAddress.StateProvinceId = null;
                            //    //set default address
                            //    customer.AddAddress(defaultAddress);
                            //    customer.SetBillingAddress(defaultAddress);
                            //    customer.SetShippingAddress(defaultAddress);
                            //    _customerContext.UpdateCustomer(customer);
                            //}
                        }


                    }
                    if (j == 500)
                    {
                        break;
                    }
                    j++;

                }
                sr.Close();
            }
            return "successfull";
        }
  public string EncryptedPassword()
        {
            StreamReader sr = new StreamReader(Server.MapPath("/Plugins/Recipe/Content/caputos-import2.csv"));
            string inputLine = "";

            string password = "loginID, Password, PasswordSalt\n";
            var j = 0;

            String[] values = null;
            while ((inputLine = sr.ReadLine()) != null)
            {
                if (j > 0)
                {
                    values = inputLine.Split(',');

                    //sql += values;
                    string saltKey = _encryptedService.CreateSaltKey(5);

                   // var id = j - 1;
                    password += values[0] + ",";
                    password += _encryptedService.CreatePasswordHash(values[2], saltKey, _customerSettings.HashedPasswordFormat) + ",";//Password
                    password += saltKey;
                    password += "\n";
                }
               
                j++;
            }
            sr.Close();
            TextWriter tw = new StreamWriter(Server.MapPath("/Plugins/Recipe/Content/passwordImport.csv"));
            // write a line of text to the file
            tw.Write(password);
            // close the stream
            tw.Close();
            return password;
        }
        public string generateSQL()
        {
            string sql = "INSERT INTO [nop25-Dev].[dbo].[Customer]  ([CustomerGuid] ,[Username]  ,[Email],[Password]  ,[PasswordFormatId] ,[PasswordSalt]  ,[AdminComment] ,[LanguageId]  ,[CurrencyId] ,[TaxDisplayTypeId] ,[IsTaxExempt]  ,[VatNumber] ,[VatNumberStatusId]  ,[SelectedPaymentMethodSystemName] ,[CheckoutAttributes]   ,[DiscountCouponCode]   ,[GiftCardCouponCodes] ,[UseRewardPointsDuringCheckout],[TimeZoneId]  ,[AffiliateId]  ,[Active] ,[Deleted] ,[IsSystemAccount] ,[SystemName],[LastIpAddress]  ,[CreatedOnUtc] ,[LastLoginDateUtc] ,[LastActivityDateUtc] ,[BillingAddress_Id]  ,[ShippingAddress_Id]) VALUES (";
            //'DD057C08-2F52-48F5-B956-48E28AE66AF1','ssdsd','sdsd' ,''  ,''   ,''  ,''   ,''  ,'' ,'','','','','','','','','','1','','','','','','','','','','','');GO";

            StreamReader sr = new StreamReader(Server.MapPath("/Plugins/Recipe/Content/caputos-import2.csv"));
            string inputLine = "";


            var j = 0;

            String[] values = null;
            while ((inputLine = sr.ReadLine()) != null)
            {
                if (j > 0)
                {
                    values = inputLine.Split(',');

                    //sql += values;
                    string saltKey = _encryptedService.CreateSaltKey(5);
                    if (j >= 2)
                    {
                        sql += ",(";
                    }
                    var id = j - 1;

                    sql += "'" + Guid.NewGuid() + "',";//guid
                    sql += "'" + values[0] + "',";//username
                    sql += "'" + values[1] + "',";//email
                    sql += "'" + _encryptedService.CreatePasswordHash(values[2], saltKey, _customerSettings.HashedPasswordFormat) + "',";//Password
                    sql += "'" + "1" + "',";//PasswordFormatId
                    sql += "'" + saltKey + "',";//PasswordSalt
                    sql += "null" + ",";//AdminComment
                    sql += "null" + ",";//LanguageId
                    sql += "null" + ",";//CurrencyId
                    sql += "'',";//TaxDisplayTypeId
                    sql += "'',";//IsTaxExempt
                    sql += "null" + ",";//VatNumber
                    sql += "'',";//VatNumberStatusId
                    sql += "null" + ",";//SelectedPaymentMethodSystemName
                    sql += "null" + ",";//CheckoutAttributes
                    sql += "null" + ",";//DiscountCouponCode
                    sql += "null" + ",";//GiftCardCouponCodes
                    sql += "'',";//UseRewardPointsDuringCheckout
                    sql += "'" + "Eastern Standard Time" + "',";//TimeZoneId
                    sql += "null" + ",";//AffiliateId  
                    sql += "'" + "true" + "',";//Active
                    sql += "'" + "false" + "',";//Deleted
                    sql += "'" + "false" + "',";//IsSystemAccount
                    sql += "null" + ",";//SystemName
                    sql += "null" + ",";//LastIpAddress
                    sql += "'" + DateTime.UtcNow + "',";//CreatedOnUtc]
                    sql += "null" + ",";//LastLoginDateUtc 
                    sql += "'" + DateTime.UtcNow + "',";//LastActivityDateUtc
                    sql += "null" + ",";//BillingAddress_Id
                    sql += "null" + ")";//ShippingAddress_Id
                    //yield return sql;
                }
                j++;
                if (j == 5)
                {
                    break;
                }

            }

            sr.Close();
            TextWriter tw = new StreamWriter(Server.MapPath("/Plugins/Recipe/Content/customerImport.sql"));
            // write a line of text to the file
            tw.Write(sql);
            // close the stream
            tw.Close();
            return "written" + sql;

        }