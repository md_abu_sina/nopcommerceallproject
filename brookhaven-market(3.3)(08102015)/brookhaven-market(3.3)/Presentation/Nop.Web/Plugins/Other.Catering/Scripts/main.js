﻿/*!
 * Vallenato 1.0
 * A Simple JQuery Accordion
 *
 * Designed by Switchroyale
 * 
 * Use Vallenato for whatever you want, enjoy!
 */

$(document).ready(function () {
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');

    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({ 'width': contentwidth });

    //Open The First Accordion Section When Page Loads
    //$('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    //$('.accordion-content').first().slideDown().toggleClass('open-content');

    // The Accordion Effect
    $('.accordion-header').click(function () {
        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }

        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });

    
    //Search form submit event add
    $("#search-food").submit(function () {
        var search_terms = $("#food-searchterms");
        if (search_terms.val() == "" || search_terms.val() == "Search store") {
            alert('Please enter some search keyword');
            search_terms.focus();
        }
        else {
            var url = "/catering/search-food"; // the script where you handle the form input.
            displayAjaxLoading(true);
            $.ajax({
                type: "POST",
                url: url,
                data: $("#search-food").serialize(), // serializes the form's elements.
                success: function (data) {
                    $(".foodmenu-middle").html(data); // show response from the php script.
                    api.viewSearchProductInit();
                    $("#food-searchterms").data("search-text",$("#food-searchterms").val());
                    displayAjaxLoading(false);
                }
            });
        }
        return false; // avoid to execute the actual submit of the form.
    });

    return false;
});



var app = $.sammy('#catering-main', function () {

    //this.get('#/', function (context) {
    //    displayAjaxLoading(true);
    //    api.main();
    //});
    this.get('#/test', function (context) {
        displayAjaxLoading(true);
        api.main();
    });

    this.get('#/shipping', function (context) {
        displayAjaxLoading(true);
        api.loadShippingForm({
            success: function (html) {
                api.displayPopupContent(html, true);
                displayAjaxLoading(false);
                window.history.back();
                //window.location.hash = '#/';
            }
        });
    });

    this.get('#/storepick', function (context) {
        displayAjaxLoading(true);
        api.storeSelect();
    });

    this.get('#/viewmap/:systemName', function (context) {
        //displayAjaxLoading(true);
        //api.storeSelect();
        //alert(this.params['systemName']);
        displayAjaxLoading(true);
        api.viewMap({
            data: { systemName: this.params['systemName'] }
        });
    });

    this.get('#/productDetail/:productName/:quantity', function (context) {
        displayAjaxLoading(true);
        api.viewProductDetail("/" + this.params['productName']);
    });

    /*this.get('#/addToCart/:url', function (context) {
        displayAjaxLoading(true);
        AjaxCart.addproducttocart_catalog(this.params['url']); /catering/search-food?pagenumber=2 pagenumber
    });*/

    this.get('#/search-food/:pagenumber', function (context) {
        displayAjaxLoading(true);
        api.viewSearchProduct({
            data: {
                q: $("#food-searchterms").data("search-text"),
                pageSize:6,
                pageNumber: this.params['pagenumber']
            }
        });
    });

    this.get(/\#\/addToCart\/(.*)/, function () {
        AjaxCart.addproducttocart_catalog("/" + this.params['splat']);
        console.log("'/" + this.params['splat'] + "'");
    });

    this.get('#/category/:categoryId', function (context) {
        displayAjaxLoading(true);
        api.viewSubCategory({
            data: {
                categoryId: this.params['categoryId'],
            }
        });
    });

    this.get('#/product/:categoryId', function (context) {
        displayAjaxLoading(true);
        api.viewCategoryProduct({
            data: { categoryId: this.params['categoryId'] }
        });
    });

    this.get('#/product/:categoryId/:pageNumber', function (context) {
        displayAjaxLoading(true);
        api.viewCategoryProduct({
            data: {
                categoryId: this.params['categoryId'],
                pageNumber: this.params['pageNumber']
            }
        });
    });

    this.get('#/productDetail/:productName', function (context) {
        displayAjaxLoading(true);
        api.viewProductDetail({
            url: this.params['productName'],
            success: function (html) {
                api.displayPopupContent(html, true);
                displayAjaxLoading(false);
                $("#dialog-content").find(".qty-input").val(12);
                window.history.back();
                //window.location.hash = '#/';
            }
        });
    });
    this.get('#/Campaign', function (context) {
        this.load('/Admin/Plugin/Other/EmailMarketing/Campaign')
				.swap()
				.then(function (items) {
				    ChangeTab(0);
				    $(".t-grid .t-refresh").first().trigger('click');
				});

    });



    this.get('#/Create', function (context) {
        this.load('/Admin/Plugin/Other/EmailMarketing/Create').swap();
    });

    this.get('#/', function (context) {
        displayAjaxLoading(true);
        api.viewSubCategory();
    });



});

$(function () {
    app.run('#/');
});


