﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Web;
using Nop.Core.Plugins;

namespace Nop.Plugin.Upon
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("DonationForm", "donationform",
                            new { controller = "Donation", action = "ShowForm" },
                            new[] { "Nop.Plugin.Donation.Controllers" });

            routes.MapRoute("FileUpload", "fileupload",
                            new { controller = "Donation", action = "FileUpload" },
                            new[] { "Nop.Plugin.Donation.Controllers" });

            routes.MapRoute("Donation.SubmitForm", "Plugin/Donation/SubmitForm",
                            new { controller = "Donation", action = "SubmitForm" },
                            new[] { "Nop.Plugin.Donation.Controllers" });

            routes.MapRoute("installMessageTemplateForDonation", "installMessageTemplateForDonation",
                new { controller = "Donation", action = "installDonationMessageTemplates" },
                                            new[] { "Nop.Plugin.Donation.Controllers" });

					/*routes.MapRoute("EmailSent", "t/{SystemName}",
													new { controller = "Topic", action = "TopicDetails" },
													new[] { "Nop.Web.Controllers" });
					routes.MapLocalizedRoute("Topic","t/{SystemName}",
													new { controller = "Topic", action = "TopicDetails" },
													new[] { "Nop.Web.Controllers" });*/
        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}