﻿using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Catalog;
using Nop.Admin.Models.Orders;

namespace Nop.Plugin.Other.Catering.Models
{
    //[Validator(typeof(EventItemValidator))]
    public class CateringOrderModel : OrderModel
    {
			public int EventId { get; set; }
			public string StorePickup { get; set; }
			public DateTime DeliveryTime { get; set; }      			
    }

}
