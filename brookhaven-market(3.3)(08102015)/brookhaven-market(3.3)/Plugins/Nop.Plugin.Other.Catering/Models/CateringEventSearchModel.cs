using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Core;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Other.Catering.Models
{
		public class CateringEventSearchModel : BaseNopModel
    {
				[UIHint("DateNullable")]
				public virtual DateTime? DeliveryTimeStart { get; set; }
				[UIHint("DateNullable")]
				public virtual DateTime? DeliveryTimeEnd { get; set; }
				//public virtual IList<SelectList> SupplierStore { get; set; }
				//public virtual IList<SelectList> CateringOrderStatus { get; set; }
				public virtual int SupplierStoreId { get; set; }
				public virtual int CateringOrderStatusId { get; set; }
    }
}
