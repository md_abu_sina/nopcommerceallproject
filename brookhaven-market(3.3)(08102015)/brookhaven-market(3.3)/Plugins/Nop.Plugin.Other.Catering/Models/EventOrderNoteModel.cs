using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Core;
using Nop.Plugin.Other.Catering.Validators;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Other.Catering.Models
{
    public class EventOrderNoteModel
    {
        public virtual int EventId { get; set; }
        public virtual List<string> EventOrderNotes { get; set; }

    }
}
