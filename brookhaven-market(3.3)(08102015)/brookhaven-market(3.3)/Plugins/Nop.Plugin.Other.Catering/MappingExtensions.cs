﻿using Nop.Admin.Models.Customers;
using Nop.Core.Domain.Customers;
using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
using AutoMapper;
using Nop.Services.Shipping;
using Nop.Services.Tax;
using Nop.Plugin.Other.Catering.Models;
using Nop.Plugin.Other.Catering.Domain;
using System.Web.Mvc;
using System;
using System.Linq;
using Nop.Core.Infrastructure;
using Nop.Services.Localization;
using Nop.Core;

namespace Nop.Plugin.Other.Catering
{
    public static class MappingExtensions
    {
       

        #region CustomerRole

				public static CustomerRoleModel ToModel(this CustomerRole entity)
        {
						CustomerRoleModel customerRoleModel = new CustomerRoleModel();
						customerRoleModel.Id = entity.Id;
						customerRoleModel.Name = entity.Name;
						customerRoleModel.Active = entity.Active;
						customerRoleModel.FreeShipping = entity.FreeShipping;
						customerRoleModel.TaxExempt = entity.TaxExempt;
						customerRoleModel.StoreRole = entity.StoreRole;
						customerRoleModel.StoreId = entity.StoreId;
						customerRoleModel.IsSystemRole = entity.IsSystemRole;
						customerRoleModel.AdGroupId = entity.AdGroupId;


						return customerRoleModel;
        }

				public static CustomerRole ToEntity(this CustomerRoleModel model)
        {
						CustomerRole customerRolEntity = new CustomerRole();
						customerRolEntity.Id = model.Id;
						customerRolEntity.Name = model.Name;
						customerRolEntity.Active = model.Active;
						customerRolEntity.FreeShipping = model.FreeShipping;
						customerRolEntity.TaxExempt = model.TaxExempt;
						customerRolEntity.StoreRole = model.StoreRole;
						customerRolEntity.StoreId = model.StoreId;
						customerRolEntity.IsSystemRole = model.IsSystemRole;
						customerRolEntity.AdGroupId = model.AdGroupId;

						return customerRolEntity;
        }

				public static CustomerRole ToEntity(this CustomerRoleModel model, CustomerRole destination)
        {
            return Mapper.Map(model, destination);
        }

        #endregion

		#region CateringEvent

		public static CateringEventModel ToModel(this CateringEvent entity)
		{
			int value = entity.CateringOrderStatus;
			CateringOrderStatus cateringOrderStatus = ((CateringOrderStatus)value);
			//string stringValue = enumDisplayStatus.ToString();

			CateringEventModel model = new CateringEventModel();
					
			model.Id = entity.Id;
			model.NumberOfPeople = entity.NumberOfPeople;
			model.CafePickup = entity.CafePickup;
			model.DeliveryTime = entity.DeliveryTime;
			model.OrderCreatedTime = entity.OrderCreatedTime;
			model.OrderUpdatedTime = entity.OrderUpdatedTime;
			model.SupplierStoreId = entity.SupplierStoreId;
			model.SupplierStoreDistance = entity.SupplierStoreDistance;
			model.AddressId = entity.AddressId;
			model.CurrentCustomerId = entity.CurrentCustomerId;
			model.OrderId = entity.OrderId;
			model.CateringOrderStatusId = entity.CateringOrderStatus;
			model.CateringOrderStatus = cateringOrderStatus.ToString();

			return model;
		}

		public static CateringEvent ToEntity(this CateringEventModel model)
		{
			CateringEvent entity = new CateringEvent();
					
			entity.Id = model.Id;
			entity.NumberOfPeople = model.NumberOfPeople;
			entity.CafePickup = model.CafePickup;
			entity.DeliveryTime = model.DeliveryTime;
			entity.OrderCreatedTime = model.OrderCreatedTime;
			entity.OrderUpdatedTime = model.OrderUpdatedTime;
			entity.SupplierStoreId = model.SupplierStoreId;
			entity.SupplierStoreDistance = model.SupplierStoreDistance;
			entity.AddressId = model.AddressId;
			entity.CurrentCustomerId = model.CurrentCustomerId;
			entity.OrderId = model.OrderId;
			//entity.CateringOrderStatus = model.CateringOrderStatus;

			return entity;
		}

		public static CateringEvent ToEntity(this CateringEventModel model, CateringEvent entity)
		{
					
			entity.Id = model.Id;
			entity.NumberOfPeople = model.NumberOfPeople;
			entity.CafePickup = model.CafePickup;
			entity.DeliveryTime = model.DeliveryTime;
			entity.OrderCreatedTime = model.OrderCreatedTime;
			entity.OrderUpdatedTime = model.OrderUpdatedTime;
			entity.SupplierStoreId = model.SupplierStoreId;
			entity.SupplierStoreDistance = model.SupplierStoreDistance;
			entity.AddressId = model.AddressId;
			entity.CurrentCustomerId = model.CurrentCustomerId;
			entity.OrderId = model.OrderId;
			//entity.CateringOrderStatus = model.CateringOrderStatus;

			return entity;
		}

		#endregion

		#region CateringEmail

		public static CateringEmailModel ToModel(this CateringEmail entity)
		{
			CateringEmailModel model = new CateringEmailModel();

			model.Id = entity.Id;
			model.StoreId = entity.StoreId;
			model.EmailAddress = entity.EmailAddress;
			model.EmailAddressType = entity.EmailAddressType;
			model.EmailType = ((EmailAddressType)entity.EmailAddressType).ToString();
					
			return model;
		}

		public static CateringEmail ToEntity(this CateringEmailModel model)
		{
			CateringEmail entity = new CateringEmail();

			entity.Id = model.Id;
			entity.StoreId = model.StoreId;
			entity.EmailAddress = model.EmailAddress;
			entity.EmailAddressType = model.EmailAddressType;
					
			return entity;
		}

		public static CateringEmail ToEntity(this CateringEmailModel model, CateringEmail entity)
		{

			entity.Id = model.Id;
			entity.StoreId = model.StoreId;
			entity.EmailAddress = model.EmailAddress;
			entity.EmailAddressType = model.EmailAddressType;

			return entity;
		}

		#endregion

		public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
		{
			var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
										select new
										{
											ID = e,
											Name = e.ToString()
										}).ToList();

			return new SelectList(values, "Id", "Name", enumObj);
		}

		public static DateTime Ceil(this DateTime date, TimeSpan span)
		{
			//long ticks = (date.Ticks + span.Ticks - 1) / span.Ticks;

			//return new DateTime(ticks * span.Ticks);
			long ticks = (date.Ticks + (span.Ticks / 2) + 1) / span.Ticks;

			return new DateTime(ticks * span.Ticks);
		}

        public static SelectList ToSelectList<TEnum>(this TEnum enumObj,
            bool markCurrentAsSelected = true, int[] valuesToExclude = null) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum) throw new ArgumentException("An Enumeration type is required.", "enumObj");

            var localizationService = EngineContext.Current.Resolve<ILocalizationService>();
            var workContext = EngineContext.Current.Resolve<IWorkContext>();

            var values = from TEnum enumValue in Enum.GetValues(typeof(TEnum))
                         where valuesToExclude == null || !valuesToExclude.Contains(Convert.ToInt32(enumValue))
                         select new { ID = Convert.ToInt32(enumValue), Name = enumValue.GetLocalizedEnum(localizationService, workContext) };
            object selectedValue = null;
            if (markCurrentAsSelected)
                selectedValue = Convert.ToInt32(enumObj);
            return new SelectList(values, "ID", "Name", selectedValue);
        }
	}
}