﻿using Nop.Core;
using Nop.Plugin.Other.Catering.Models;
using Nop.Plugin.Other.Catering.Services;
using Nop.Core.Domain.Messages;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Tasks;
using Nop.Services.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Services.Events;
using Nop.Core.Infrastructure;
using Nop.Services.Customers;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;

namespace Nop.Plugin.Other.Catering
{
    public class WeeklyReportEmailTask : ITask
    {
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly IOrderService _orderService;
        private readonly IStoreContext _storeContext;
        private readonly IMessageTemplateService _messageTemplateService;//
		private readonly IQueuedEmailService _queuedEmailService;//
		private readonly ILanguageService _languageService;//
		private readonly ITokenizer _tokenizer;//
        private readonly ICateringEventService _cateringEventService;
        private readonly ICateringEmailService _cateringEmailService;
        private readonly ICateringPdfService _cateringPdfService;
        private readonly IMessageTokenProvider _messageTokenProvider;//
        private readonly IStoreService _storeService;//
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IEventPublisher _eventPublisher;//
        private readonly IEmailAccountService _emailAccountService;
        private readonly ICustomerService _customerService;//
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductService _productService;
       
        public WeeklyReportEmailTask(ILogger logger, IOrderService orderService, ICateringEventService cateringEventService, ICateringPdfService cateringPdfService, IWebHelper webHelper,
                                     IStoreContext storeContext, IMessageTemplateService messageTemplateService, IQueuedEmailService queuedEmailService, ILanguageService languageService,
                                     ITokenizer tokenizer, IMessageTokenProvider messageTokenProvider, IStoreService storeService, EmailAccountSettings emailAccountSettings,
                                     IEventPublisher eventPublisher, IEmailAccountService emailAccountService, ICateringEmailService cateringEmailService, ICustomerService customerService,
                                     IProductAttributeParser productAttributeParser, IProductService productService)
        {
            this._logger = logger;
            this._orderService = orderService;
            this._cateringEventService = cateringEventService;
            this._cateringEmailService = cateringEmailService;
            this._cateringPdfService = cateringPdfService;
            this._webHelper = webHelper;

            this._storeContext = storeContext;
            this._messageTemplateService = messageTemplateService;
            this._queuedEmailService = queuedEmailService;
            this._languageService = languageService;
            this._tokenizer = tokenizer;
            this._messageTokenProvider = messageTokenProvider;
            this._storeService = storeService;
            this._emailAccountSettings = emailAccountSettings;
            this._eventPublisher = eventPublisher;
            this._emailAccountService = emailAccountService;
            this._customerService = customerService;
            this._productAttributeParser = productAttributeParser;
            this._productService = productService;
        }

        ///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Execute task
		/// </summary>
        public void Execute()
        {
            var storeNames = this.GetStoreNames();

            foreach (var storeName in storeNames)
            {
                var weeklyReportPdfPath = this.GetWeeklyReportPdfPath(storeName);
                var storeEmail = _cateringEmailService.GetCateringEmailByStoreName(storeName);
                this.SendWeeklySummaryReport(1, storeEmail, weeklyReportPdfPath);
            }
            
        }

        #region Weekly Report Utility
        protected string GetWeeklyReportPdfPath(string storeName)
        {
            var model = new ReportSummaryModel();


            var orders = _cateringEventService.OrdersForWeeklySummaryReport();


            var eventOrderItem = orders.GroupBy(o => o.DeliveryTime.Date).Select(o => new
            {
                Date = o.Key,
                EventOrder = o.Select(oi =>
                {
                    var ce = _orderService.GetOrderById(oi.OrderId).OrderItems;
                    return ce;
                })
            }).ToList();

            foreach (var item in eventOrderItem)
            {
                var groupedItem = new GroupedOrderModel();

                groupedItem.OrderDay = item.Date;

                var eventOrderProduct = new EventOrderProductModel();

                foreach (var childItem in item.EventOrder)
                {
                    if ((model.SupplierStoreId.Equals(0) || model.SupplierStoreId.Equals(12)) && storeName.Equals("Darien"))
                    {
                        var darienItems = childItem.Where(ci => this.GetStoreName(ci.OrderId, ci.Id).Equals("Darien")).Select(ci => new EventOrderProductModel()
                        {
                            Quantity = ci.Quantity,
                            ProductName = ci.Product.Name,
                            Attribute = ci.AttributeDescription,
                            PvaCollection = _productAttributeParser.ParseProductVariantAttributeValues(ci.AttributesXml).Where(pva => pva.AttributeValueType.Equals(AttributeValueType.AssociatedToProduct)).ToList(),
                            StoreName = "Darien",
                            Size = this.GetSize(ci.AttributeDescription)
                        }).ToList();


                        foreach (var darienItem in darienItems)
                        {
                            if (!groupedItem.DarienEventOrder.Any(x => darienItem.ProductName.Equals(x.ProductName)))//&& !groupedItem.DarienEventOrder.Any(x => darienItem.Attribute.Equals(x.Attribute))
                            {
                                groupedItem.DarienEventOrder.Add(darienItem);
                            }
                            else
                            {
                                var existingItem = groupedItem.DarienEventOrder.Where(x => x.ProductName.Equals(darienItem.ProductName)).FirstOrDefault();// && x.Attribute.Equals(darienItem.Attribute))
                                if (existingItem != null)
                                {
                                    existingItem.Quantity = existingItem.Quantity + darienItem.Quantity;
                                }

                            }
                        }

                    }

                    if ((model.SupplierStoreId.Equals(0) || model.SupplierStoreId.Equals(11)) && storeName.Equals("Mokena"))
                    {
                        var mokenaItems = childItem.Where(ci => this.GetStoreName(ci.OrderId, ci.Id).Equals("Mokena")).Select(ci => new EventOrderProductModel()
                        {
                            Quantity = ci.Quantity,
                            ProductName = ci.Product.Name,
                            Attribute = ci.AttributeDescription,
                            PvaCollection = _productAttributeParser.ParseProductVariantAttributeValues(ci.AttributesXml).Where(pva => pva.AttributeValueType.Equals(AttributeValueType.AssociatedToProduct)).ToList(),
                            StoreName = "Mokena",
                            Size = this.GetSize(ci.AttributeDescription)
                        }).ToList();

                        foreach (var mokenaItem in mokenaItems)
                        {
                            if (!groupedItem.MokenaEventOrder.Any(x => mokenaItem.ProductName.Equals(x.ProductName)))//&& !groupedItem.MokenaEventOrder.Any(x => mokenaItem.Attribute.Equals(x.Attribute))
                            {
                                groupedItem.MokenaEventOrder.Add(mokenaItem);
                            }
                            else
                            {
                                var existingItem = groupedItem.MokenaEventOrder.Where(x => x.ProductName.Equals(mokenaItem.ProductName)).FirstOrDefault();//&& x.Attribute.Equals(mokenaItem.Attribute)
                                if (existingItem != null)
                                {
                                    existingItem.Quantity = existingItem.Quantity + mokenaItem.Quantity;
                                }
                            }
                            //groupedItem.MokenaEventOrder.Add(mokenaItem);
                        }

                    }

                    if ((model.SupplierStoreId.Equals(0) || model.SupplierStoreId.Equals(10)) && storeName.Equals("Burr Ridge"))
                    {
                        var burridgeItems = childItem.Where(ci => this.GetStoreName(ci.OrderId, ci.Id).Equals("Burr Ridge")).Select(ci => new EventOrderProductModel()
                        {
                            Quantity = ci.Quantity,
                            ProductName = ci.Product.Name,
                            Attribute = ci.AttributeDescription,
                            PvaCollection = _productAttributeParser.ParseProductVariantAttributeValues(ci.AttributesXml).Where(pva => pva.AttributeValueType.Equals(AttributeValueType.AssociatedToProduct)).ToList(),
                            StoreName = "Burr Ridge",
                            Size = this.GetSize(ci.AttributeDescription)
                        }).ToList();

                        /*burridgeItems.GroupBy(o => o.ProductName)
                            .Select(g => g.Skip(1).Aggregate(
                            g.First(), (a, o) => { a.Quantity += o.Quantity; return a; }));

                        groupedItem.BurridgeEventOrder = burridgeItems;*/

                        foreach (var burridgeItem in burridgeItems)
                        {
                            if (!groupedItem.BurridgeEventOrder.Any(x => burridgeItem.ProductName.Equals(x.ProductName)))//&& !groupedItem.BurridgeEventOrder.Any(x => burridgeItem.Attribute.Equals(x.Attribute))
                            {
                                groupedItem.BurridgeEventOrder.Add(burridgeItem);
                            }
                            else
                            {
                                var existingItem = groupedItem.BurridgeEventOrder.Where(x => x.ProductName.Equals(burridgeItem.ProductName)).FirstOrDefault();//&& x.Attribute.Equals(burridgeItem.Attribute)
                                if (existingItem != null)
                                {
                                    existingItem.Quantity = existingItem.Quantity + burridgeItem.Quantity;
                                }
                                //groupedItem.BurridgeEventOrder.Add(burridgeItem);
                            }
                            //groupedItem.BurridgeEventOrder.Add(burridgeItem);
                        }
                    }
                }

                foreach (var attributeProductItem in groupedItem.DarienEventOrder.ToList())
                {
                    foreach (var productItem in attributeProductItem.PvaCollection)
                    {
                        var attributeAsProduct = new EventOrderProductModel();
                        attributeAsProduct.ProductName = _productService.GetProductById(productItem.AssociatedProductId).Name;
                        attributeAsProduct.Quantity = this.GetQuantity(productItem, attributeProductItem.Attribute);
                        attributeAsProduct.StoreName = "Darien";
                        groupedItem.DarienEventOrder.Add(attributeAsProduct);
                    }
                }

                foreach (var attributeProductItem in groupedItem.MokenaEventOrder.ToList())
                {
                    foreach (var productItem in attributeProductItem.PvaCollection)
                    {
                        var attributeAsProduct = new EventOrderProductModel();
                        attributeAsProduct.ProductName = _productService.GetProductById(productItem.AssociatedProductId).Name;
                        attributeAsProduct.Quantity = this.GetQuantity(productItem, attributeProductItem.Attribute);
                        attributeAsProduct.StoreName = "Mokena";
                        groupedItem.MokenaEventOrder.Add(attributeAsProduct);
                    }
                }

                foreach (var attributeProductItem in groupedItem.BurridgeEventOrder.ToList())
                {
                    foreach (var productItem in attributeProductItem.PvaCollection)
                    {
                        var attributeAsProduct = new EventOrderProductModel();
                        attributeAsProduct.ProductName = _productService.GetProductById(productItem.AssociatedProductId).Name;
                        attributeAsProduct.Quantity = this.GetQuantity(productItem, attributeProductItem.Attribute);
                        attributeAsProduct.StoreName = "Burr Ridge";
                        groupedItem.BurridgeEventOrder.Add(attributeAsProduct);
                    }
                }

                model.GroupedOrderItems.Add(groupedItem);
            }




            string fileName = string.Format("Weekly_Summary_Report_{0}_{1:MM-dd-yyyy_hh-mm-ss-tt}_{2}.pdf", storeName, DateTime.Now, CommonHelper.GenerateRandomDigitCode(4));
            string filePath = Path.Combine(_webHelper.MapPath("~/content/files/WeeklyReport"), fileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                _cateringPdfService.PrintSummaryReportToPdf(fileStream, model, 0);
            }

            return filePath;
        }

        protected string GetStoreName(int orderId, int orderItemId)
        {
            var order = _orderService.GetOrderById(orderId);
            var cateringEvent = _cateringEventService.GetCateringEventByOrderId(orderId);
            List<string> neededEmails = new List<string>();
            var storeInfo = _cateringEventService.SecondaryStoreInfo(cateringEvent, order, out neededEmails);

            return storeInfo[orderItemId];
        }

        
        protected int GetQuantity(ProductVariantAttributeValue pva, string description)
        {
            try
            {
                var price = description.Split(new[] { "<br />" }, StringSplitOptions.None)
                        .Where(x => x.Contains(pva.Name))
                        .Select(x => x.Split(':'))
                        .ToDictionary(x => x[0], x => x[1]).FirstOrDefault().Value.Split(new char[] { '$', ']' })[1]; ;

                //string price = dict.Substring(dict.IndexOf('$'), dict.IndexOf(']'));

                var targetProductPrice = _productService.GetProductById(pva.AssociatedProductId).Price;
                Decimal quantity = Decimal.Parse(price) / targetProductPrice;
                quantity = Decimal.Round(quantity);

                return Decimal.ToInt32(quantity);
            }
            catch (Exception)
            {

                return 0;
            }

        }

        protected string GetSize(string attributeDescription)
        {
            if (attributeDescription.Contains("Size:"))
            {
                string[] attributeString = attributeDescription.Split(new[] { "<br />" }, StringSplitOptions.None);

                try
                {
                    return attributeString[0].Split(':')[1].Substring(0, attributeString[0].Split(':')[1].IndexOf('['));
                }
                catch (Exception)
                {

                    return attributeString[0].Split(':')[1];
                }
            }
            else
            {
                return String.Empty;
            }
        }  
        
        #endregion

        #region Weekly Report Summary Email Utility

        protected List<string> GetStoreNames()
        { 
            var stores = _customerService.GetCustomerRoleByStoreRole().Where(sr => sr.AdGroupId == "2004");

            List<string> storeNames = new List<string>();

            foreach (var store in stores)
	        {
                storeNames.Add(store.Name);
	        }

            return storeNames;
        }

        /// <summary>
        /// Get store URL
        /// </summary>
        /// <param name="storeId">Store identifier; Pass 0 to load URL of the current store</param>
        /// <param name="useSsl">Use SSL</param>
        /// <returns></returns>
        protected virtual string GetStoreUrl(int storeId = 0, bool useSsl = false)
        {
            var store = _storeService.GetStoreById(storeId) ?? _storeContext.CurrentStore;

            if (store == null)
                throw new Exception("No store could be loaded");

            return useSsl ? store.SecureUrl : store.Url;
        }

        protected virtual int SendNotification(MessageTemplate messageTemplate,
                        EmailAccount emailAccount, int languageId, IEnumerable<Token> tokens,
                        string toEmailAddress, string toName,
                        string attachmentFilePath = null, string attachmentFileName = null)
        {
            //retrieve localized message template data
            var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
            var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
            var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            var bodyReplaced = _tokenizer.Replace(body, tokens, true);

            var email = new QueuedEmail()
            {
                Priority = 5,
                From = emailAccount.Email,
                FromName = emailAccount.DisplayName,
                To = toEmailAddress,
                ToName = toName,
                CC = string.Empty,
                Bcc = bcc,
                Subject = subjectReplaced,
                Body = bodyReplaced,
                AttachmentFilePath = attachmentFilePath,
                AttachmentFileName = attachmentFileName,
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id
            };

            _queuedEmailService.InsertQueuedEmail(email);
            return email.Id;
        }

        protected virtual MessageTemplate GetActiveMessageTemplate(string messageTemplateName, int storeId)
        {
            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, storeId);

            //no template found
            if (messageTemplate == null)
                return null;

            //ensure it's active
            var isActive = messageTemplate.IsActive;
            if (!isActive)
                return null;

            return messageTemplate;
        }

        protected virtual EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
        {
            var emailAccounId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
            var emailAccount = _emailAccountService.GetEmailAccountById(emailAccounId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            return emailAccount;

        }

        protected virtual int EnsureLanguageIsActive(int languageId, int storeId)
        {
            //load language by specified ID
            var language = _languageService.GetLanguageById(languageId);

            if (language == null || !language.Published)
            {
                //load any language from the specified store
                language = _languageService.GetAllLanguages(storeId: storeId).FirstOrDefault();
            }
            if (language == null || !language.Published)
            {
                //load any language
                language = _languageService.GetAllLanguages().FirstOrDefault();
            }

            if (language == null)
                throw new Exception("No active language could be loaded");
            return language.Id;
        }

        /// <summary>
        /// Sends an order placed notification to a store owner
        /// </summary>
        /// <param name="order">Order instance</param>
        /// <param name="languageId">Message language identifier</param>
        /// <returns>Queued email identifier</returns>
        protected virtual int SendWeeklySummaryReport(int languageId, string emailAddress, string pdfPath)
        {
            
            var store =  _storeContext.CurrentStore;
            languageId = this.EnsureLanguageIsActive(languageId, store.Id);

            var messageTemplate = this.GetActiveMessageTemplate("Catering.WeeklyReportEmail", store.Id);
            if (messageTemplate == null)
                return 0;

            //email account
            var emailAccount = this.GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

            //tokens
            var tokens = new List<Token>();
            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            //this.AddOrderTokens(tokens, order, languageId, secondaryStoreInfo);
            //_messageTokenProvider.AddCustomerTokens(tokens, order.Customer);

            //event notification
            _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            var toEmail = emailAddress;
            var toName = emailAccount.DisplayName;
            var filePath = pdfPath;
            var fileName = Path.GetFileName(filePath);
            return SendNotification(messageTemplate, emailAccount,
                    languageId, tokens,
                    toEmail, toName, filePath, fileName);
        } 
        #endregion


        public int Order
        {
            //ensure that this task is run first 
            get { return 0; }
        }
    }
}
