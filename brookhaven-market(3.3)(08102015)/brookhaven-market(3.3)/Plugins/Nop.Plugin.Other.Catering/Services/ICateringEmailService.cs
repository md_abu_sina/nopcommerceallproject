using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.Other.Catering.Domain;

namespace Nop.Plugin.Other.Catering.Services
{
		public interface ICateringEmailService
    {
        
				void InsertCateringEmail(CateringEmail cateringEmail);

				CateringEmail GetCateringEmailById(int id);

				string GetCateringEmailByStoreName(string storeName);

				List<CateringEmail> GetCateringEmailByStoreId(int storeId);

				IPagedList<CateringEmail> GetAllCateringEmail(int pageIndex, int pageSize);

				void UpdateCateringEmail(CateringEmail cateringEmail);

				void DeleteCateringEmail(CateringEmail cateringEmail);
    }
}