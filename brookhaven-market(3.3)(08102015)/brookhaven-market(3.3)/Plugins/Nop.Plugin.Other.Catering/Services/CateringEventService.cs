using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Other.Catering.Domain;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Services.Orders;

namespace Nop.Plugin.Other.Catering.Services
{
		public class CateringEventService : ICateringEventService
    {
        private readonly IRepository<CateringEvent> _cateringEventRepository;
        private readonly IRepository<Order> _orderRepository;
				private readonly ICateringEmailService _cateringEmailService;
				private readonly IWorkContext _workContext;//
				private readonly ISpecificationAttributeService _specificationAttributeService;//
				private readonly ICustomerService _customerService;//
                private readonly IOrderService _orderService;

				public CateringEventService(IRepository<CateringEvent> CateringEventRepository, ICateringEmailService cateringEmailService, IWorkContext workContext, ISpecificationAttributeService specificationAttributeService, IOrderService orderService, ICustomerService customerService, IRepository<Order> orderRepository)
        {
					_cateringEventRepository = CateringEventRepository;
					_cateringEmailService = cateringEmailService;
					_workContext = workContext;
					_specificationAttributeService = specificationAttributeService;
					_customerService = customerService;
                    _orderRepository = orderRepository;
                    _orderService = orderService;
        }

        #region Implementation of CateringEventService

				/// <summary>
				/// insert event
				/// </summary>
				/// <param name="eventItem"></param>
				public virtual void InsertCateringEvent(CateringEvent cateringEvent)
				{
					if(cateringEvent == null)
						throw new ArgumentNullException("cateringEvent");

					cateringEvent.OrderCreatedTime = DateTime.UtcNow;

					_cateringEventRepository.Insert(cateringEvent);

					//event notification
					//_eventPublisher.EntityInserted(eventItem);
				}

				/// <summary>
				/// get event  by id
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public CateringEvent GetCateringEventById(int id)
				{
					var db = _cateringEventRepository.Table;
					return db.SingleOrDefault(c => c.Id.Equals(id));
				}

				/// <summary>
				/// get event  by id
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public CateringEvent GetCateringEventByOrderId(int orderId)
				{
					var db = _cateringEventRepository.Table;
					return db.SingleOrDefault(c => c.OrderId.Equals(orderId));
				}

				/// <summary>
				/// get all cateringevents
				/// </summary>
				/// <param name="pageIndex"></param>
				/// <param name="pageSize"></param>
				/// <returns>IPagedList<cateringevents></returns>
				public virtual IPagedList<CateringEvent> GetAllCateringEvent(int pageIndex, int pageSize, DateTime? deliveryDateFrom, DateTime? deliveryDateTo, int storeId = 0, int cateringOrderStatusId = 0)
				{
					var query = _cateringEventRepository.Table;

					if(storeId > 0)
						query = query.Where(c => c.SupplierStoreId == storeId);
					if(cateringOrderStatusId > 0)
						query = query.Where(c => c.CateringOrderStatus == cateringOrderStatusId);

					if(deliveryDateFrom.HasValue)
						query = query.Where(c => deliveryDateFrom.Value <= c.DeliveryTime);
					if(deliveryDateTo.HasValue)
						query = query.Where(c => deliveryDateTo.Value >= c.DeliveryTime);

					
					query = query.OrderByDescending(c => c.Id);

					var events = new PagedList<CateringEvent>(query, pageIndex, pageSize);
					return events;
				}

                /// <summary>
                /// Search orders
                /// </summary>
                /// <param name="storeId">Store identifier; 0 to load all orders</param>
                /// <param name="vendorId">Vendor identifier; null to load all orders</param>
                /// <param name="customerId">Customer identifier; 0 to load all orders</param>
                /// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
                /// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
                /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
                /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
                /// <param name="os">Order status; null to load all orders</param>
                /// <param name="ps">Order payment status; null to load all orders</param>
                /// <param name="ss">Order shipment status; null to load all orders</param>
                /// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
                /// <param name="orderGuid">Search by order GUID (Global unique identifier) or part of GUID. Leave empty to load all orders.</param>
                /// <param name="pageIndex">Page index</param>
                /// <param name="pageSize">Page size</param>
                /// <returns>Order collection</returns>
                public virtual IPagedList<Order> SearchOrders(int storeId = 0,
                    int vendorId = 0, int customerId = 0,
                    int productId = 0, int affiliateId = 0,
                    DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
                    OrderStatus? os = null, PaymentStatus? ps = null, ShippingStatus? ss = null,
                    string billingEmail = null, string orderGuid = null,
                    int pageIndex = 0, int pageSize = int.MaxValue,
                    DateTime? deliveryStartDate = null, DateTime? deliveryEndDate = null)
                {
                    int? orderStatusId = null;
                    if (os.HasValue)
                        orderStatusId = (int)os.Value;

                    int? paymentStatusId = null;
                    if (ps.HasValue)
                        paymentStatusId = (int)ps.Value;

                    int? shippingStatusId = null;
                    if (ss.HasValue)
                        shippingStatusId = (int)ss.Value;

                    var query = _orderRepository.Table;

                    if (vendorId > 0)
                    {
                        query = query
                            .Where(o => o.OrderItems
                            .Any(orderItem => orderItem.Product.VendorId == vendorId));
                    }
                    if (customerId > 0)
                        query = query.Where(o => o.CustomerId == customerId);
                    if (productId > 0)
                    {
                        query = query
                            .Where(o => o.OrderItems
                            .Any(orderItem => orderItem.Product.Id == productId));
                    }
                    if (affiliateId > 0)
                        query = query.Where(o => o.AffiliateId == affiliateId);
                    if (createdFromUtc.HasValue)
                        query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
                    if (createdToUtc.HasValue)
                        query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
                    if (orderStatusId.HasValue)
                        query = query.Where(o => orderStatusId.Value == o.OrderStatusId);
                    if (paymentStatusId.HasValue)
                        query = query.Where(o => paymentStatusId.Value == o.PaymentStatusId);
                    if (shippingStatusId.HasValue)
                        query = query.Where(o => shippingStatusId.Value == o.ShippingStatusId);
                    if (!String.IsNullOrEmpty(billingEmail))
                        query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));

                    query = query.Where(o => !o.Deleted);
                    query = query.OrderByDescending(o => o.CreatedOnUtc);

                    /* if (storeId > 0)
                     {
                         var IDs = (from ce in _cateringEventRepository.Table
                                    where ce.SupplierStoreId.Equals(storeId) && ce.OrderId > 0
                                    select  ce.OrderId ).ToArray();

                         query = from o in query
                                 where IDs.Contains(o.Id)
                                 select o;
                     }*/

                    var eventQuery = _cateringEventRepository.Table;

                    if (storeId > 0)
                        eventQuery = eventQuery.Where(c => c.SupplierStoreId == storeId);


                    if (deliveryStartDate.HasValue)
                        eventQuery = eventQuery.Where(c => deliveryStartDate.Value <= c.DeliveryTime);
                    if (deliveryEndDate.HasValue)
                        eventQuery = eventQuery.Where(c => deliveryEndDate.Value >= c.DeliveryTime);

                    var IDs = (from ce in eventQuery where ce.OrderId > 0 select ce.OrderId).ToArray();

                    query = from o in query
                            where IDs.Contains(o.Id)
                            select o;
                    //query.Where(o => o.StoreId == storeId);
                    

                    if (!String.IsNullOrEmpty(orderGuid))
                    {
                        //filter by GUID. Filter in BLL because EF doesn't support casting of GUID to string
                        var orders = query.ToList();
                        orders = orders.FindAll(o => o.OrderGuid.ToString().ToLowerInvariant().Contains(orderGuid.ToLowerInvariant()));
                        return new PagedList<Order>(orders, pageIndex, pageSize);
                    }
                    else
                    {
                        //database layer paging
                       
                        return new PagedList<Order>(query, pageIndex, pageSize);
                    }

                }

                /// <summary>
                /// Search orders
                /// </summary>
                /// <param name="storeId">Store identifier; 0 to load all orders</param>
                /// <param name="vendorId">Vendor identifier; null to load all orders</param>
                /// <param name="customerId">Customer identifier; 0 to load all orders</param>
                /// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
                /// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
                /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
                /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
                /// <param name="os">Order status; null to load all orders</param>
                /// <param name="ps">Order payment status; null to load all orders</param>
                /// <param name="ss">Order shipment status; null to load all orders</param>
                /// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
                /// <param name="orderGuid">Search by order GUID (Global unique identifier) or part of GUID. Leave empty to load all orders.</param>
                /// <param name="pageIndex">Page index</param>
                /// <param name="pageSize">Page size</param>
                /// <returns>Order collection</returns>
                public virtual IPagedList<CateringEvent> SearchOrdersForSummaryReport(int storeId = 0,
                    int vendorId = 0, int customerId = 0,
                    int productId = 0, int affiliateId = 0,
                    DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
                    OrderStatus? os = null, PaymentStatus? ps = null, ShippingStatus? ss = null,
                    string billingEmail = null, string orderGuid = null,
                    int pageIndex = 0, int pageSize = int.MaxValue, 
                    DateTime? deliveryStartDate = null, DateTime? deliveryEndDate = null)
                {
                    int? orderStatusId = null;
                    if (os.HasValue)
                        orderStatusId = (int)os.Value;

                    int? paymentStatusId = null;
                    if (ps.HasValue)
                        paymentStatusId = (int)ps.Value;

                    int? shippingStatusId = null;
                    if (ss.HasValue)
                        shippingStatusId = (int)ss.Value;

                    var query = _orderRepository.Table;
                    
                    if (vendorId > 0)
                    {
                        query = query
                            .Where(o => o.OrderItems
                            .Any(orderItem => orderItem.Product.VendorId == vendorId));
                    }
                    if (customerId > 0)
                        query = query.Where(o => o.CustomerId == customerId);
                    if (productId > 0)
                    {
                        query = query
                            .Where(o => o.OrderItems
                            .Any(orderItem => orderItem.Product.Id == productId));
                    }
                    if (affiliateId > 0)
                        query = query.Where(o => o.AffiliateId == affiliateId);
                    if (createdFromUtc.HasValue)
                        query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
                    if (createdToUtc.HasValue)
                        query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
                    if (orderStatusId.HasValue)
                        query = query.Where(o => orderStatusId.Value == o.OrderStatusId);
                    if (paymentStatusId.HasValue)
                        query = query.Where(o => paymentStatusId.Value == o.PaymentStatusId);
                    if (shippingStatusId.HasValue)
                        query = query.Where(o => shippingStatusId.Value == o.ShippingStatusId);
                    if (!String.IsNullOrEmpty(billingEmail))
                        query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));
                    
                    query = query.Where(o => !o.Deleted);
                    query = query.OrderByDescending(o => o.CreatedOnUtc);


                    var IDs = (from o in query select o.Id).ToArray();
                   /* if (storeId > 0)
                    {
                        var IDs = (from ce in _cateringEventRepository.Table
                                   where ce.SupplierStoreId.Equals(storeId) && ce.OrderId > 0
                                   select  ce.OrderId ).ToArray();

                        query = from o in query
                                where IDs.Contains(o.Id)
                                select o;
                    }*/

                    var eventQuery = _cateringEventRepository.Table;

                    /*if (storeId > 0)
                        eventQuery = eventQuery.Where(c => c.SupplierStoreId == storeId);*/
                    

                    if (deliveryStartDate.HasValue)
                        eventQuery = eventQuery.Where(c => deliveryStartDate.Value <= c.DeliveryTime);
                    /*if (deliveryEndDate.HasValue)
                        eventQuery = eventQuery.Where(c => deliveryEndDate.Value >= c.DeliveryTime);*/



                    eventQuery = from ce in eventQuery
                                where IDs.Contains(ce.OrderId)
                                orderby ce.DeliveryTime descending
                                select ce;
                            //query.Where(o => o.StoreId == storeId);


                    var orders = eventQuery.ToList();
                    /*if (storeId > 0)
                    {
                        orders = orders.FindAll(o => this.IsOrderContainSpecificStore(o.OrderId, storeId));
                    }*/
                    return new PagedList<CateringEvent>(orders, pageIndex, pageSize);

                }



                public virtual List<CateringEvent> OrdersForWeeklySummaryReport(int storeId = 0, DateTime? deliveryStartDate = null, DateTime? deliveryEndDate = null)
                {
                    

                    var query = _orderRepository.Table;

                    

                    query = query.Where(o => !o.Deleted);
                    query = query.OrderByDescending(o => o.CreatedOnUtc);

                   



                    var IDs = (from o in query select o.Id).ToArray();

                    var eventQuery = _cateringEventRepository.Table;

                    //deliveryEndDate = DateTime.Now;
                    deliveryStartDate = DateTime.Today;//deliveryEndDate.Value.AddDays(-7);
                    //deliveryEndDate = deliveryStartDate.Value.AddDays(7);
                    
                    if (deliveryStartDate.HasValue)
                        eventQuery = eventQuery.Where(c => deliveryStartDate.Value <= c.DeliveryTime);
                    if (deliveryEndDate.HasValue)
                        eventQuery = eventQuery.Where(c => deliveryEndDate.Value >= c.DeliveryTime);


                    eventQuery = from ce in eventQuery
                                 where IDs.Contains(ce.OrderId)
                                 orderby ce.DeliveryTime descending
                                 select ce;


                    var orders = eventQuery.ToList();
                    if (storeId > 0)
                    {
                        orders = orders.FindAll(o => this.IsOrderContainSpecificStore(o.OrderId, storeId));
                    }

                    return orders.ToList();
                    

                }
                
				public CateringEvent GetInProgressCateringEventByCustomerId(int customerId)
				{
					var db = _cateringEventRepository.Table;

					var inProgressEvent = from evnt in db
																where evnt.CurrentCustomerId.Equals(customerId) && evnt.OrderId.Equals(0)
																orderby evnt.Id descending
																select evnt;
					return 
						inProgressEvent.FirstOrDefault();
				}

				/// <summary>
				/// Update Event
				/// </summary>
				/// <param name="eventItem"></param>
				public void UpdateCateringEvent(CateringEvent cateringEvent)
				{
					if(cateringEvent == null)
						throw new ArgumentNullException("cateringEvent");

					cateringEvent.OrderUpdatedTime = DateTime.UtcNow;

					_cateringEventRepository.Update(cateringEvent);

					//event notification
					//_eventPublisher.EntityUpdated(eventItem);
				}

				/// <summary>
				/// delete event
				/// </summary>
				/// <param name="eventItem"></param>
				public void DeleteCateringEvent(CateringEvent cateringEvent)
				{
					_cateringEventRepository.Delete(cateringEvent);
					//_eventPublisher.EntityDeleted(eventItem);
				}

                #endregion

				public bool IsValidDeliveryTime(DateTime deliveryTime)
				{
					return this.ValidateDeliveryTime(deliveryTime);
				}

				private bool ValidateDeliveryTime(DateTime deliveryTime)
				{
					

					if(deliveryTime > DateTime.Now.AddHours(2)
							//&& deliveryTime.DayOfWeek != DayOfWeek.Saturday 
							//&& deliveryTime.DayOfWeek != DayOfWeek.Sunday 
							&& this.IsTimeValid(deliveryTime.TimeOfDay))
					{
						return true;
					}
					else
					{
						return false;
					}
					
				}

				private bool IsTimeValid(TimeSpan delivery)
				{
					TimeSpan start = new TimeSpan(06, 0, 0);
					TimeSpan end = new TimeSpan(21, 0, 0);
					
					return delivery >= start && delivery <= end;
				}

				public Dictionary<int, string> SecondaryStoreInfo(CateringEvent cateringEvent, Order order, out List<string> emails)
				{
					emails = new List<string>();

					string supplierStoreName = _customerService.GetCustomerRoleById(cateringEvent.SupplierStoreId).Name;

					//Add the store where the order is placed to send an email
					var initialStoreEmail = _cateringEmailService.GetCateringEmailByStoreName(supplierStoreName);
					if(!emails.Contains(initialStoreEmail))
						emails.Add(initialStoreEmail);

					var items = order.OrderItems;

					//int will contain orderitem id string will contain provider store name
					Dictionary<int, string> secondaryStore = new Dictionary<int, string>();

					foreach(var item in items)
					{
						var spAttributes = _specificationAttributeService.GetProductSpecificationAttributesByProductId(item.ProductId)
																						.Where(sp => sp.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("PrimaryLocation") || sp.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("SecondaryLocation"));

						var isNeedSecondaryLocation = (spAttributes.Count() > 0) ? !spAttributes.Where(sp => sp.SpecificationAttributeOption.Name.Equals(supplierStoreName)).Any() : false;

						if(isNeedSecondaryLocation)
						{

							var secondaryLocation = spAttributes.FirstOrDefault();
							var secondaryLocationEmail = _cateringEmailService.GetCateringEmailByStoreName(secondaryLocation.SpecificationAttributeOption.Name);

							secondaryStore.Add(item.Id, secondaryLocation.SpecificationAttributeOption.Name);

							if(!emails.Contains(secondaryLocationEmail))
								emails.Add(secondaryLocationEmail);
						}
						else
						{
							if(!cateringEvent.CafePickup)
							{
								var storeRole = _workContext.CurrentCustomer.CustomerRoles.Where(cr => cr.StoreRole != null && cr.StoreRole.Equals("True")).FirstOrDefault();

								if(storeRole != null)
									supplierStoreName = storeRole.SystemName;

								var supplierStoreEmail = _cateringEmailService.GetCateringEmailByStoreName(supplierStoreName);

								secondaryStore.Add(item.Id, supplierStoreName);

								if(!emails.Contains(supplierStoreEmail))
									emails.Add(supplierStoreEmail);
							}
							else
							{
								var supplierStoreEmail = _cateringEmailService.GetCateringEmailByStoreName(supplierStoreName);

								secondaryStore.Add(item.Id, supplierStoreName);

								if(!emails.Contains(supplierStoreEmail))
									emails.Add(supplierStoreEmail);
							}
						}
					}

					return secondaryStore;
				}

                protected bool IsOrderContainSpecificStore(int orderId, int storeId)
                {
                    var order = _orderService.GetOrderById(orderId);
                    var cateringEvent = this.GetCateringEventByOrderId(order.Id);
                    string supplierStoreName = _customerService.GetCustomerRoleById(cateringEvent.SupplierStoreId).Name;

                    var items = order.OrderItems;
                    string secondaryLocation = string.Empty;
                    

                    foreach (var item in items)
                    {
                        var spAttributes = _specificationAttributeService.GetProductSpecificationAttributesByProductId(item.ProductId)
                                                                                        .Where(sp => sp.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("PrimaryLocation") || sp.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("SecondaryLocation"));

                        var isNeedSecondaryLocation = (spAttributes.Count() > 0) ? !spAttributes.Where(sp => sp.SpecificationAttributeOption.Name.Equals(supplierStoreName)).Any() : false;

                        if (isNeedSecondaryLocation)
                        {

                            secondaryLocation = spAttributes.FirstOrDefault().SpecificationAttributeOption.Name;

                        }
                        else
                        {
                            if (!cateringEvent.CafePickup)
                            {
                                var storeRole = _workContext.CurrentCustomer.CustomerRoles.Where(cr => cr.StoreRole != null && cr.StoreRole.Equals("True")).FirstOrDefault();

                                if (storeRole != null)
                                    supplierStoreName = storeRole.SystemName;

                                secondaryLocation = supplierStoreName;

                                
                            }
                            else
                            {
                                secondaryLocation = supplierStoreName;

                            }
                        }
                    }

                    var secondaryStoreId = string.IsNullOrEmpty(secondaryLocation) ? 0 : _customerService.GetCustomerRoleBySystemName(secondaryLocation).Id;

                    return (secondaryStoreId == storeId);
                }
    }
}