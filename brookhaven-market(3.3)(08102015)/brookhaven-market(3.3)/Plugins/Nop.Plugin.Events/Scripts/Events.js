$(document).ready(function () {

    //showCalender(-1)

});

function showCalender(i) {
    var str = "";
    for (var j = 0; j <= i; j++) {
        var current_customerrole_id = $('#SelectedCustomerRoleIds' + j).val();
        if ($('#SelectedCustomerRoleIds' + j).attr('checked')) {

            var customerrole_id = parseInt(current_customerrole_id);
            if (str == "") {
                str += "?";
            }
            else {
                str += "&";
            }
            str += "customerrole_ids=" + customerrole_id

        }
    }  

    $('#calendar').empty();
    str = (str === "") ? "?customerrole_ids=-1" : str;
    $('#calendar').fullCalendar({
        height: 100,
        //aspectRatio: 2

        events: "/Event/getAllFutureEvents" + str


    })

}

function storeroleselectorchange(i) {

    var str = "";
    if (i == -1) {
        str += "&customerrole_ids=-1"
    }
    else {
        for (var j = 0; j <= i; j++) {
            var current_customerrole_id = $('#SelectedCustomerRoleIds' + j).val();
            if ($('#SelectedCustomerRoleIds' + j).attr('checked')) {

                var customerrole_id = parseInt(current_customerrole_id);
                str += "&customerrole_ids=" + customerrole_id
                
            }
        }
    }

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Event/ChangeStoreSelector",
        data: str,
        success: function (data) {

            $('.eventListDiv').empty();
            alert("changestoreselector");
            $('.eventListDiv').append(data);



        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });




}