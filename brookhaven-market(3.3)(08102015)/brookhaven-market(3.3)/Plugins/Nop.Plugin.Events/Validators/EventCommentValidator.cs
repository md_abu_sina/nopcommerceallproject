﻿using FluentValidation;
using Nop.Plugin.Events.Models;
using Nop.Services.Localization;


namespace Nop.Plugin.Events.Validators
{
    public class EventCommentValidator : AbstractValidator<EventCommentModel>
    {
        public EventCommentValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.CommentTitle)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Events.Fields.Title.Required"));

            RuleFor(x => x.CommentText)
                .NotNull()
                .WithMessage(localizationService.GetResource("Admin.ContentManagement.Events.Fields.Comment.Required"));

        }
    }
}