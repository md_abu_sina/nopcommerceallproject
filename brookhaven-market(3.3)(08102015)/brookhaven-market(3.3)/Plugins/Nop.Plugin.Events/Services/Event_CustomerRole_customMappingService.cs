﻿using Nop.Plugin.Events.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Plugin.Events.Services;
using Nop.Core;
using Nop.Services.Events;

namespace Nop.Plugin.Events.Services
{
    public class Event_CustomerRole_customMappingService : IEvent_CustomerRole_customMappingService
    {
        #region fields

        private readonly IRepository<Event_CustomerRole_CustomMapping> _event_CustomerRole_customMappingRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region ctor

        public Event_CustomerRole_customMappingService(IRepository<Event_CustomerRole_CustomMapping> event_CustomerRole_customMappingRepository, IEventPublisher eventPublisher)
        {
            _event_CustomerRole_customMappingRepository = event_CustomerRole_customMappingRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion


        #region implementataion of IEvent_CustomerRole_customMappingService

        /// <summary>
        /// get All Customer Role Id By Event Id
        /// </summary>
        /// <param name="EventId"></param>
        /// <returns></returns>
        public List<int> getAllCustomerRoleIdByEventId(int EventId)
        {
            List<int> CustomerRoleIds = (from u in _event_CustomerRole_customMappingRepository.Table
                                         where u.EventId == EventId
                                         select u.CustomerRoleId).ToList();

            return CustomerRoleIds;
        }

        /// <summary>
        /// get All Event Id By Customer Role Id
        /// </summary>
        /// <param name="CustomerRoleId"></param>
        /// <returns></returns>
        public List<int> getAllEventIdByCustomerRoleId(int CustomerRoleId)
        {
            List<int> EventIds = (from u in _event_CustomerRole_customMappingRepository.Table
                                         where u.CustomerRoleId == CustomerRoleId
                                         select u.EventId).ToList();

            return EventIds;
        }

        /// <summary>
        /// Insert to Event Customer Role Custom Mapping table
        /// </summary>
        /// <param name="item"></param>
        public virtual void InsertEvent_CustomerRole_CustomMapping(Event_CustomerRole_CustomMapping item)
        {
            if (item == null)
                throw new ArgumentNullException("InsertEvent_CustomerRole_CustomMapping");

            _event_CustomerRole_customMappingRepository.Insert(item);

            //event notification
            _eventPublisher.EntityInserted(item);
        }

        /// <summary>
        /// delete All entry By EventId
        /// </summary>
        /// <param name="EventId"></param>
        public void deleteAllEntryByEventId(int EventId)
        {
            List<Event_CustomerRole_CustomMapping> event_customerrole_customMappingList = (from u in _event_CustomerRole_customMappingRepository.Table
                                                                                           where u.EventId == EventId
                                                                                           select u).ToList();
            foreach(Event_CustomerRole_CustomMapping event_customerrole_customMapping in event_customerrole_customMappingList)
            {
            _event_CustomerRole_customMappingRepository.Delete(event_customerrole_customMapping);
            _eventPublisher.EntityDeleted(event_customerrole_customMapping);
            }
        }

        #endregion
    }
}
