﻿using Nop.Core;

namespace Nop.Plugin.Events.Domain
{
    public class Event_CustomerRole_CustomMapping : BaseEntity
    {



        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        /// <value>
        /// The Id.
        /// </value>
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title.
        /// </value>
        public virtual int EventId { get; set; }

        
        /// <summary>
        ///    Gets or sets the StoreId.
        ///   </summary>
        ///   <value>
        ///   The StoreId.
        ///  </value>
        public virtual int CustomerRoleId { get; set; }


    }

}
