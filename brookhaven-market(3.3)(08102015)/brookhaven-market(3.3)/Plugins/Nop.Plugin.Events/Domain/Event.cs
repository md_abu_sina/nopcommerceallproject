﻿using System;
using System.Collections.Generic;
using Nop.Core;

namespace Nop.Plugin.Events.Domain
{
    public class Event : BaseEntity
    {

        private ICollection<EventComment> _eventComments;
      //  private ICollection<CustomerRole> _customerRoles;

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        /// <value>
        /// The Id.
        /// </value>
        public virtual int Id { get; set; }
        
        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title.
        /// </value>
        public virtual string Title { get; set; }

        /// <summary>
        /// Gets or sets the StoreId.
        /// </summary>
        /// <value>
        /// The StoreId.
        /// </value>
        //public virtual int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the daytime.
        /// </summary>
        /// <value>
        /// The daytime.
        /// </value>
        public virtual DateTime DayTime { get; set; }

        /// <summary>
        /// Gets or sets the Short Description.
        /// </summary>
        /// <value>
        /// The Short Description.
        /// </value>
        public virtual string ShortDescription { get; set; }

        /// <summary>
        /// Gets or sets the Full Description.
        /// </summary>
        /// <value>
        /// The Full Description.
        /// </value>
        public virtual string FullDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the news item is published
        /// </summary>
        public virtual bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the news post comments are allowed 
        /// </summary>
       
        public virtual bool AllowComments { get; set; }

       /// <summary>
        /// Gets or sets the news comments
        /// </summary>
        public virtual ICollection<EventComment> EventComments
        {
            get { return _eventComments ?? (_eventComments = new List<EventComment>()); }
            protected set { _eventComments = value; }
        }

        ///// <summary>
        ///// Gets or sets the customer roles
        ///// </summary>
        //public virtual ICollection<CustomerRole> CustomerRoles
        //{
        //    get { return _customerRoles ?? (_customerRoles = new List<CustomerRole>()); }
        //    protected set { _customerRoles = value; }
        //}
    }
   
}
