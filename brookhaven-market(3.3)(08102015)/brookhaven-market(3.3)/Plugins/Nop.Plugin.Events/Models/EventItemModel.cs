﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Nop.Web.Framework.Mvc;
using Nop.Web.Validators.News;
using Nop.Plugin.Events.Models;
using Nop.Web.Framework;
using System.Web.Mvc;
using Nop.Plugin.Events.Validators;

namespace Nop.Plugin.Events.Models
{
    [Validator(typeof(EventItemwithCommentValidator))]
    public class EventItemModel : BaseNopEntityModel
    {
       public EventItemModel()
        {
            Tags = new List<string>();
            Comments = new List<EventCommentModel>();
            AddNewComment = new AddEventsCommentModel();
        }

        public int Id { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.Title")]
        [AllowHtml]
        public string Title { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.StoreName")]
        public string CustomerRoles { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.DayTime")]
        public DateTime DayTime { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.ShortDescription")]
        [AllowHtml]
        public string ShortDescription { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.FullDescription")]
        [AllowHtml]
        public string FullDescription { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.Published")]
        public  bool Published { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.AllowComments")]
        public  bool AllowComments { get; set; }

        public int totalComments { get; set; }

        public IList<string> Tags { get; set; }

        public IList<EventCommentModel> Comments { get; set; }

        public AddEventsCommentModel AddNewComment { get; set; }

				public bool DisplayCaptcha { get; set; }
    }
}