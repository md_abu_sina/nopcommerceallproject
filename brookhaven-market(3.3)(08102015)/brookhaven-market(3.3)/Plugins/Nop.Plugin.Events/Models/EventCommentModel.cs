﻿using System;
using System.Web.Mvc;
using Nop.Admin.Validators.News;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using FluentValidation.Attributes;
using Nop.Plugin.Events.Validators;


namespace Nop.Plugin.Events.Models
{
    [Validator(typeof(EventCommentValidator))]
    public class EventCommentModel : BaseNopEntityModel
    {
        public int Id { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.Title")]
        [AllowHtml]
        public string CommentTitle { get; set; }

        [NopResourceDisplayName("Admin.ContentManagement.Events.Fields.Comment")]
        [AllowHtml]
        public string CommentText { get; set; }

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAvatarUrl { get; set; }

        public DateTime createdOn { get; set; }

        public string IpAddress { get; set; }

        public int EventId { get; set; }

        public string EventTitle { get; set; }

        public bool AllowViewingProfiles { get; set; }
    }


}
