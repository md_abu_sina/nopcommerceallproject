﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.Upon.Domain;
using Nop.Web.Framework.Mvc;
using FluentValidation.Attributes;
using Nop.Plugin.Upon.Validators;
using System.Web;
using Nop.Web.Models.Customer;

namespace Nop.Plugin.Upon.Models
{

    public class upontotal : BaseNopEntityModel
    {
        public int Id { get; set; }

        public string UponStatus { get; set; }

        public string Today { get; set; }

        public string ThisWeek { get; set; }

        public string ThisMonth { get; set; }

        public string ThisYear { get; set; }

        public string AllTime { get; set; }
    }

}
