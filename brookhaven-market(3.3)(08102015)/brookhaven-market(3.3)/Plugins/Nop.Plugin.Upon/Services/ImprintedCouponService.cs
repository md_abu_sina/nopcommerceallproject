﻿using Nop.Plugin.Upon.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Plugin.Upon.Services;
using Nop.Plugin.Upon.Models;
using Nop.Core;
using System.Globalization;
using Nop.Services.Events;

namespace Nop.Plugin.Upon.Services
{
    public class ImprintedCouponService : IImprintedCouponService
    {
        #region fields

        private readonly IRepository<ImprintedCoupon> _imprintedCouponRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region ctor

        public ImprintedCouponService(IRepository<ImprintedCoupon> imprintedCouponRepository, IEventPublisher eventPublisher)
        {
            _imprintedCouponRepository = imprintedCouponRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region Implementation of IImprintedCouponService


        /// <summary>
        /// get Imprinted Coupon  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ImprintedCoupon GetImprintedcouponById(int id)
        {
            var db = _imprintedCouponRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Update Imprinted Coupon
        /// </summary>
        /// <param name="imprintedcouponItem"></param>
        public void UpdateImprintedCoupon(ImprintedCoupon imprintedcouponItem)
        {
            if (imprintedcouponItem == null)
                throw new ArgumentNullException("imprintedcouponItem");

            _imprintedCouponRepository.Update(imprintedcouponItem);

            //event notification
            _eventPublisher.EntityUpdated(imprintedcouponItem);
        }

        /// <summary>
        /// Assign Unique Coupon
        /// </summary>
        /// <param name="imprintedcouponItem"></param>
        public virtual void AssignUniqueCoupon(ImprintedCoupon imprintedcouponItem)
        {
            if (imprintedcouponItem == null)
                throw new ArgumentNullException("imprintedcouponItem");

            _imprintedCouponRepository.Insert(imprintedcouponItem);

            //event notification
            _eventPublisher.EntityInserted(imprintedcouponItem);
        }

        /// <summary>
        /// Get Unique Coupon By Customer Id
        /// </summary>
        /// <param name="CustomerId"></param>
        public virtual ImprintedCoupon GetUniqueCouponByCustomerId(int CustomerId)
        {

            //  var db = _imprintedCouponRepository;
            //  ImprintedCoupon Query = db.Table.SingleOrDefault(x => x.CustomerID == CustomerId);
            ImprintedCoupon imprintedcoupon = new ImprintedCoupon();
            try
            {
                imprintedcoupon = (from u in _imprintedCouponRepository.Table
                                   orderby u.EndDate
                                   where u.CustomerID == CustomerId && u.StartDate <= DateTime.Now && u.EndDate >= DateTime.Now
                                   select u).First();
            }

            catch
            {
                imprintedcoupon = null;

            }
            return imprintedcoupon;

        }

        /// <summary>
        /// Get All Unique Coupon By Customer Id
        /// </summary>
        /// <param name="CustomerId"></param>
        public virtual List<ImprintedCoupon> GetAllUniqueCouponByCustomerId(int CustomerId)
        {

            //  var db = _imprintedCouponRepository;
            //  ImprintedCoupon Query = db.Table.SingleOrDefault(x => x.CustomerID == CustomerId);
            List<ImprintedCoupon> imprintedcoupon = new List<ImprintedCoupon>();
            try
            {
                imprintedcoupon = (from u in _imprintedCouponRepository.Table
                                   orderby u.Id
                                   where u.CustomerID == CustomerId
                                   select u).ToList();
            }

            catch
            {
                imprintedcoupon = null;

            }
            return imprintedcoupon;

        }
        /// <summary>
        /// get Imprinted Coupon By Unique coupon Id
        /// </summary>
        /// <param name="unique_coupon_id"></param>
        public virtual ImprintedCoupon getImprintedCouponByUniqueId(string unique_coupon_id)
        {
            Guid unique_couponID = Guid.Parse(unique_coupon_id);
            ImprintedCoupon imprintedcoupon = (from u in _imprintedCouponRepository.Table
                                               where u.UniqueId_imprintedCoupon == unique_couponID
                                               select u).First();
            return imprintedcoupon;
        }


				/// <summary>
				/// Gets all imprintedcoupons for export.
				/// </summary>
				/// <returns></returns>
        public virtual List<ImprintedCoupon> GetAllImprintedcouponsForExport()
        {
            // var query = _uniqueCouponRepository.Table;
            List<ImprintedCoupon> imprintedcoupons = (from u in _imprintedCouponRepository.Table
                                                      orderby u.Id
                                                      select u).ToList();

            return imprintedcoupons;


        }

				/// <summary>
				/// Gets the active expired imprintedcoupons.
				/// </summary>
				/// <returns></returns>
				public virtual List<ImprintedCoupon> GetActiveExpiredImprintedcoupons()
				{
					// var query = _uniqueCouponRepository.Table;
					List<ImprintedCoupon> imprintedcoupons = (from u in _imprintedCouponRepository.Table
																										orderby u.Id
																										where u.Status == "Active" && u.ExpirationDate <= DateTime.Now
																										select u).ToList();

					return imprintedcoupons;


				}

        /// <summary>
        /// get All Imprinted Coupon
        /// </summary>
        public virtual IPagedList<ImprintedCoupon> GetAllImprintedcoupon(int pageIndex, int pageSize)
        {
            // var query = _uniqueCouponRepository.Table;
            var query = (from u in _imprintedCouponRepository.Table
                         orderby u.Id descending
                         select u);
            var imprintedcoupon = new PagedList<ImprintedCoupon>(query, pageIndex, pageSize);
            return imprintedcoupon;


        }


        public virtual IPagedList<ImprintedCoupon> GetAllImprintedcoupon(string Email, int? CouponId,
            string Offer, string Status,
            DateTime? ImprintingDateFrom, DateTime? ImprintingDateTo, DateTime? ExpirationDateFrom, DateTime? ExpirationDateTo,
            DateTime? RedemptionDateFrom, DateTime? RedemptionDateTo, string AmountFrom, string AmountTo, int pageIndex, int pageSize)
        {
            var query = _imprintedCouponRepository.Table;

            if (ImprintingDateFrom.HasValue)
                query = query.Where(c => ImprintingDateFrom.Value <= c.ImprintingDate);
            if (ImprintingDateTo.HasValue)
                query = query.Where(c => ImprintingDateTo.Value >= c.ImprintingDate);

            if (ExpirationDateFrom.HasValue)
                query = query.Where(c => ExpirationDateFrom.Value <= c.ExpirationDate);
            if (ExpirationDateTo.HasValue)
                query = query.Where(c => ExpirationDateTo.Value >= c.ExpirationDate);

            if (RedemptionDateFrom.HasValue)
                query = query.Where(c => RedemptionDateFrom.Value <= c.RedemptionDate);
            if (RedemptionDateTo.HasValue)
                query = query.Where(c => RedemptionDateTo.Value >= c.RedemptionDate);

            if (!String.IsNullOrWhiteSpace(Email))
                query = query.Where(c => c.Email.Contains(Email));

            if (CouponId > 0)
                query = query.Where(c => c.CouponId == CouponId);

            if (!String.IsNullOrWhiteSpace(Offer))
                query = query.Where(c => c.Offer.Contains(Offer));

            if (!String.IsNullOrWhiteSpace(Status))
                query = query.Where(c => c.Status == Status);


            if (!String.IsNullOrWhiteSpace(AmountFrom))
            {
                double amountrangefrom = double.Parse((AmountFrom as string).Trim('$'));
                query = query.Where(c => amountrangefrom <= c.AmountInDecimal);

            }

            if (!String.IsNullOrWhiteSpace(AmountTo))
            {
                double amountrangeTo = double.Parse((AmountTo as string).Trim('$'));
                query = query.Where(c => amountrangeTo >= c.AmountInDecimal);

            }
						query = query.OrderByDescending(c => c.Id);

            var coupons = new PagedList<ImprintedCoupon>(query, pageIndex, pageSize);
            return coupons;
        }

        #region added by Razib
        ///--------------------------------Added by Razib----------------------------------------
        /// <summary>
        /// Gets the filter imprintedcoupon.
        /// </summary>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="status">The status.</param>
        /// <param name="filterKey">The filter key.</param>
        /// <returns></returns>
        public virtual IPagedList<ImprintedCoupon> GetFilterImprintedcoupon(UserPageModelList model, int pageIndex, int pageSize, string status, string filterKey)
        {
          	DateTime today = DateTime.Today;
						int start = DayOfWeek.Sunday - today.DayOfWeek;
						DateTime sunday = today.AddDays(start);

						int end = DayOfWeek.Saturday - today.DayOfWeek;
						DateTime saturday = today.AddDays(end);

            IQueryable<ImprintedCoupon> query;
            switch (filterKey)
            {
                case "Today":
										//query = (from u in _imprintedCouponRepository.Table
										//     where u.Status == status && u.ImprintingDate.Value.Year.Equals(DateTime.Today)
										//     orderby u.Id descending
										//     select u);
										if(status == "Active")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status &&
															 u.ImprintingDate.Value.Day.Equals(DateTime.Now.Day)
															 && u.ImprintingDate.Value.Month.Equals(DateTime.Now.Month)
															 && u.ImprintingDate.Value.Year.Equals(DateTime.Now.Year)
															 orderby u.Id descending
															 select u);
										}
										else if(status == "Expired")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status &&
															 u.ExpirationDate.Day.Equals(DateTime.Now.Day)
															 && u.ExpirationDate.Month.Equals(DateTime.Now.Month)
															 && u.ExpirationDate.Year.Equals(DateTime.Now.Year)
															 orderby u.Id descending
															 select u);
										}
										else
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status &&
															 u.RedemptionDate.Value.Day.Equals(DateTime.Now.Day)
															 && u.RedemptionDate.Value.Month.Equals(DateTime.Now.Month)
															 && u.RedemptionDate.Value.Year.Equals(DateTime.Now.Year)
															 orderby u.Id descending
															 select u);
										}
                    break;
                case "ThisWeek":
										if(status == "Active")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && (u.ImprintingDate >= sunday && u.ImprintingDate <= saturday)
															 orderby u.Id descending
															 select u);
										}
										else if(status == "Expired")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && (u.ExpirationDate >= sunday && u.ExpirationDate <= saturday)
															 orderby u.Id descending
															 select u);
										}
										else
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && (u.RedemptionDate >= sunday && u.RedemptionDate <= saturday)
															 orderby u.Id descending
															 select u);
										}
                    break;
                case "ThisMonth":
										if(status == "Active")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && u.ImprintingDate.Value.Month.Equals(DateTime.Today.Month)
															 orderby u.Id descending
															 select u);
										}
										else if(status == "Expired")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && u.ExpirationDate.Month.Equals(DateTime.Today.Month)
															 orderby u.Id descending
															 select u);
										}
										else
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && u.RedemptionDate.Value.Month.Equals(DateTime.Today.Month)
															 orderby u.Id descending
															 select u);
										}
                    
                    break;
                case "ThisYear":
										if(status == "Active")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
															 orderby u.Id descending
															 select u);
										}
										else if(status == "Expired")
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && u.ExpirationDate.Year.Equals(DateTime.Today.Year)
															 orderby u.Id descending
															 select u);
										}
										else
										{
											query = (from u in _imprintedCouponRepository.Table
															 where u.Status == status && u.RedemptionDate.Value.Year.Equals(DateTime.Today.Year)
															 orderby u.Id descending
															 select u);
										}
                    break;
                case "AllTime":
                    query = (from u in _imprintedCouponRepository.Table
                             where u.Status == status //&& u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
                             orderby u.Id descending
                             select u);
                    break;
                default:
                    goto case "AllTime";
            }

						if(model != null)
						{
							if(model.ImprintingDateStart.HasValue)
								query = query.Where(u => u.ImprintingDate >= model.ImprintingDateStart.Value);
							if(model.ImprintingDateEnd.HasValue)
								query = query.Where(u => u.ImprintingDate <= model.ImprintingDateEnd.Value);

							if(model.ExpirationDateStart.HasValue)
								query = query.Where(u => u.ExpirationDate >= model.ExpirationDateStart.Value);
							if(model.ExpirationDateEnd.HasValue)
								query = query.Where(u => u.ExpirationDate <= model.ExpirationDateEnd.Value);

							if(model.RedemptionDateStart.HasValue)
								query = query.Where(u => u.RedemptionDate >= model.RedemptionDateStart.Value);
							if(model.RedemptionDateEnd.HasValue)
								query = query.Where(u => u.RedemptionDate <= model.RedemptionDateEnd.Value);

							if(!String.IsNullOrWhiteSpace(model.Email))
								query = query.Where(u => u.Email.Contains(model.Email));

							if(model.CouponId > 0)
								query = query.Where(u => u.CouponId == model.CouponId);

							if(!String.IsNullOrWhiteSpace(model.Offer))
								query = query.Where(u => u.Offer.Contains(model.Offer));

							
							if(!String.IsNullOrWhiteSpace(model.AmountRangeFrom))
							{
								int amountrangefrom = int.Parse((model.AmountRangeFrom as string).Trim('$'));
								query = query.Where(u => u.AmountInDecimal >= amountrangefrom);

							}

							if(!String.IsNullOrWhiteSpace(model.AmountRangeTo))
							{
								int amountrangeTo = int.Parse((model.AmountRangeTo as string).Trim('$'));
								query = query.Where(u => u.AmountInDecimal <= amountrangeTo);

							}

							query = query.OrderByDescending(u => u.Id);
						}

            var imprintedcoupon = new PagedList<ImprintedCoupon>(query, pageIndex, pageSize);
            return imprintedcoupon;


        }

        #endregion

        public virtual List<ImprintedCoupon> GetImprintedcouponByIds(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return new List<ImprintedCoupon>();

            var query = from c in _imprintedCouponRepository.Table
                        where ids.Contains(c.Id)
                        select c;
            var imprintedupons = query.ToList();
            //sort by passed identifiers
            var sortedimprintedcoupons = new List<ImprintedCoupon>();
            foreach (int id in ids)
            {
                var imprintedupon = imprintedupons.Find(x => x.Id == id);
                if (imprintedupon != null)
                    sortedimprintedcoupons.Add(imprintedupon);
            }
            return sortedimprintedcoupons;
        }


        #region active upons for reports

        /// <summary>
        /// get All Active Imprinted Coupon
        /// </summary>
        public virtual IPagedList<ImprintedCoupon> GetAllActiveImprintedcoupon(int pageIndex, int pageSize)
        {
            // var query = _uniqueCouponRepository.Table;
            var query = (from u in _imprintedCouponRepository.Table
                         where u.Status == "Active"
                         orderby u.Id descending
                         select u);
            var imprintedcoupon = new PagedList<ImprintedCoupon>(query, pageIndex, pageSize);
            return imprintedcoupon;


        }

        public virtual IPagedList<ImprintedCoupon> GetAllActiveImprintedcoupon(string Email, int? CouponId,
          string Offer,
          DateTime? ImprintingDateFrom, DateTime? ImprintingDateTo, DateTime? ExpirationDateFrom, DateTime? ExpirationDateTo,
          DateTime? RedemptionDateFrom, DateTime? RedemptionDateTo, string AmountFrom, string AmountTo, int pageIndex, int pageSize)
        {
            var query = _imprintedCouponRepository.Table;

            if (ImprintingDateFrom.HasValue)
                query = query.Where(c => ImprintingDateFrom.Value <= c.ImprintingDate);
            if (ImprintingDateTo.HasValue)
                query = query.Where(c => ImprintingDateTo.Value >= c.ImprintingDate);

            if (ExpirationDateFrom.HasValue)
                query = query.Where(c => ExpirationDateFrom.Value <= c.ExpirationDate);
            if (ExpirationDateTo.HasValue)
                query = query.Where(c => ExpirationDateTo.Value >= c.ExpirationDate);

            if (RedemptionDateFrom.HasValue)
                query = query.Where(c => RedemptionDateFrom.Value <= c.RedemptionDate);
            if (RedemptionDateTo.HasValue)
                query = query.Where(c => RedemptionDateTo.Value >= c.RedemptionDate);

            if (!String.IsNullOrWhiteSpace(Email))
                query = query.Where(c => c.Email.Contains(Email));

            if (CouponId > 0)
                query = query.Where(c => c.CouponId == CouponId);

            if (!String.IsNullOrWhiteSpace(Offer))
                query = query.Where(c => c.Offer.Contains(Offer));

            query = query.Where(c => c.Status == "Active");

            //if (!String.IsNullOrWhiteSpace(AmountFrom))
            //    query = query.Where(c => Convert.ToInt32((c.Amount as string).Trim('$')) <= Convert.ToInt32((AmountFrom as string).Trim('$')));

            if (!String.IsNullOrWhiteSpace(AmountFrom))
            {
                double amountrangefrom = double.Parse((AmountFrom as string));
                query = query.Where(c => amountrangefrom <= c.AmountInDecimal);

            }

            if (!String.IsNullOrWhiteSpace(AmountTo))
            {
                double amountrangeTo = double.Parse((AmountTo as string));
                query = query.Where(c => amountrangeTo >= c.AmountInDecimal);

            }

            query = query.OrderByDescending(c => c.Id);

            var coupons = new PagedList<ImprintedCoupon>(query, pageIndex, pageSize);
            return coupons;
        }

        public virtual List<ImprintedCoupon> GetAllActiveImprintedcouponsForExport()
        {
            // var query = _uniqueCouponRepository.Table;
            List<ImprintedCoupon> imprintedcoupons = (from u in _imprintedCouponRepository.Table
                                                      where u.Status == "Active"
                                                      orderby u.Id
                                                      select u).ToList();

            return imprintedcoupons;


        }
				public virtual List<ImprintedCoupon> FilterImprintedCouponsForExport(SearchPageModel model, string status, string filterKey)
				{
					DateTime today = DateTime.Today;
					int start = DayOfWeek.Sunday - today.DayOfWeek;
					DateTime sunday = today.AddDays(start);

					int end = DayOfWeek.Saturday - today.DayOfWeek;
					DateTime saturday = today.AddDays(end);

					IQueryable<ImprintedCoupon> query;
					switch(filterKey)
					{
						case "Today":
							//query = (from u in _imprintedCouponRepository.Table
							//     where u.Status == status && u.ImprintingDate.Value.Year.Equals(DateTime.Today)
							//     orderby u.Id descending
							//     select u);
							if(status == "Active")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status &&
												 u.ImprintingDate.Value.Day.Equals(DateTime.Now.Day)
												 && u.ImprintingDate.Value.Month.Equals(DateTime.Now.Month)
												 && u.ImprintingDate.Value.Year.Equals(DateTime.Now.Year)
												 orderby u.Id descending
												 select u);
							}
							else if(status == "Expired")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status &&
												 u.ExpirationDate.Day.Equals(DateTime.Now.Day)
												 && u.ExpirationDate.Month.Equals(DateTime.Now.Month)
												 && u.ExpirationDate.Year.Equals(DateTime.Now.Year)
												 orderby u.Id descending
												 select u);
							}
							else
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status &&
												 u.RedemptionDate.Value.Day.Equals(DateTime.Now.Day)
												 && u.RedemptionDate.Value.Month.Equals(DateTime.Now.Month)
												 && u.RedemptionDate.Value.Year.Equals(DateTime.Now.Year)
												 orderby u.Id descending
												 select u);
							}
							break;
						case "ThisWeek":
							if(status == "Active")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && (u.ImprintingDate >= sunday && u.ImprintingDate <= saturday)
												 orderby u.Id descending
												 select u);
							}
							else if(status == "Expired")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && (u.ExpirationDate >= sunday && u.ExpirationDate <= saturday)
												 orderby u.Id descending
												 select u);
							}
							else
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && (u.RedemptionDate >= sunday && u.RedemptionDate <= saturday)
												 orderby u.Id descending
												 select u);
							}
							break;
						case "ThisMonth":
							if(status == "Active")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && u.ImprintingDate.Value.Month.Equals(DateTime.Today.Month)
												 orderby u.Id descending
												 select u);
							}
							else if(status == "Expired")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && u.ExpirationDate.Month.Equals(DateTime.Today.Month)
												 orderby u.Id descending
												 select u);
							}
							else
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && u.RedemptionDate.Value.Month.Equals(DateTime.Today.Month)
												 orderby u.Id descending
												 select u);
							}

							break;
						case "ThisYear":
							if(status == "Active")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
												 orderby u.Id descending
												 select u);
							}
							else if(status == "Expired")
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && u.ExpirationDate.Year.Equals(DateTime.Today.Year)
												 orderby u.Id descending
												 select u);
							}
							else
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status && u.RedemptionDate.Value.Year.Equals(DateTime.Today.Year)
												 orderby u.Id descending
												 select u);
							}
							break;
						case "AllTime":
							if(!String.IsNullOrEmpty(status))
							{
								query = (from u in _imprintedCouponRepository.Table
												 where u.Status == status //&& u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
												 orderby u.Id descending
												 select u);
							}
							else
							{
								query = (from u in _imprintedCouponRepository.Table
												 orderby u.Id descending
												 select u);
							}
							break;
						default:
							goto case "AllTime";
					}

					if(model != null)
					{
						if(model.ImprintingDateStart.HasValue)
							query = query.Where(u => u.ImprintingDate >= model.ImprintingDateStart.Value);
						if(model.ImprintingDateEnd.HasValue)
							query = query.Where(u => u.ImprintingDate <= model.ImprintingDateEnd.Value);

						if(model.ExpirationDateStart.HasValue)
							query = query.Where(u => u.ExpirationDate >= model.ExpirationDateStart.Value);
						if(model.ExpirationDateEnd.HasValue)
							query = query.Where(u => u.ExpirationDate <= model.ExpirationDateEnd.Value);

						if(model.RedemptionDateStart.HasValue)
							query = query.Where(u => u.RedemptionDate >= model.RedemptionDateStart.Value);
						if(model.RedemptionDateEnd.HasValue)
							query = query.Where(u => u.RedemptionDate <= model.RedemptionDateEnd.Value);

						if(!String.IsNullOrWhiteSpace(model.Email))
							query = query.Where(u => u.Email.Contains(model.Email));

						if(model.CouponId > 0)
							query = query.Where(u => u.CouponId == model.CouponId);

						/*if(!String.IsNullOrWhiteSpace(model.Offer))
							query = query.Where(u => u.Offer.Contains(model.Offer));*/


						if(!String.IsNullOrWhiteSpace(model.AmountRangeFrom))
						{
							int amountrangefrom = int.Parse((model.AmountRangeFrom as string).Trim('$'));
							query = query.Where(u => u.AmountInDecimal >= amountrangefrom);

						}

						if(!String.IsNullOrWhiteSpace(model.AmountRangeTo))
						{
							int amountrangeTo = int.Parse((model.AmountRangeTo as string).Trim('$'));
							query = query.Where(u => u.AmountInDecimal <= amountrangeTo);

						}

						query = query.OrderByDescending(u => u.Id);
					}

					//var imprintedcoupon = new PagedList<ImprintedCoupon>(query, pageIndex, pageSize);
					return query.ToList();


				}
				//public virtual List<ImprintedCoupon> FilterImprintedCouponsForExport(string status, string filterKey)
				//{
				//  DateTime today = DateTime.Today;
				//  int start = DayOfWeek.Sunday - today.DayOfWeek;
				//  DateTime sunday = today.AddDays(start);

				//  int end = DayOfWeek.Saturday - today.DayOfWeek;
				//  DateTime saturday = today.AddDays(end);

				//  List<ImprintedCoupon> imprintedcoupons;

				//  switch(filterKey)
				//  {
				//    case "Today":
				//      if(status == "Active")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status &&
				//                            u.ImprintingDate.Value.Day.Equals(DateTime.Now.Day)
				//                            && u.ImprintingDate.Value.Month.Equals(DateTime.Now.Month)
				//                            && u.ImprintingDate.Value.Year.Equals(DateTime.Now.Year)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else if(status == "Expired")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status &&
				//                            u.ExpirationDate.Day.Equals(DateTime.Now.Day)
				//                            && u.ExpirationDate.Month.Equals(DateTime.Now.Month)
				//                            && u.ExpirationDate.Year.Equals(DateTime.Now.Year)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status &&
				//                            u.RedemptionDate.Value.Day.Equals(DateTime.Now.Day)
				//                            && u.RedemptionDate.Value.Month.Equals(DateTime.Now.Month)
				//                            && u.RedemptionDate.Value.Year.Equals(DateTime.Now.Year)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
							
				//      break;
				//    case "ThisWeek":
				//      if(status == "Active")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && (u.ImprintingDate >= sunday && u.ImprintingDate <= saturday)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else if(status == "Expired")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && (u.ExpirationDate >= sunday && u.ExpirationDate <= saturday)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && (u.RedemptionDate >= sunday && u.RedemptionDate <= saturday)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      break;
				//    case "ThisMonth":
				//      if(status == "Active")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && u.ImprintingDate.Value.Month.Equals(DateTime.Today.Month)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else if(status == "Expired")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && u.ExpirationDate.Month.Equals(DateTime.Today.Month)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && u.RedemptionDate.Value.Month.Equals(DateTime.Today.Month)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      break;
				//    case "ThisYear":
				//      if(status == "Active")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else if(status == "Expired")
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      else
				//      {
				//        imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                            where u.Status == status && u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
				//                            orderby u.Id descending
				//                            select u).ToList();
				//      }
				//      break;
				//    case "AllTime":
				//      imprintedcoupons = (from u in _imprintedCouponRepository.Table
				//                          where u.Status == status //&& u.ImprintingDate.Value.Year.Equals(DateTime.Today.Year)
				//                          orderby u.Id descending
				//                          select u).ToList();
				//      break;
				//    default:
				//      goto case "AllTime";
				//  }

				//  return imprintedcoupons;
				//}

        #endregion

      
        

        #region upon total report

        public virtual upontotal upontotal(string Status)
        {
            var imprintedcouponList = (from u in _imprintedCouponRepository.Table
                                       where u.Status == Status
                                       select u).ToList();

            var today = 0;
            var thisweek = 0;
            var thismonth = 0;
            var thisyear = 0;
            var alltime = imprintedcouponList.Count;

						DateTime thisDay = DateTime.Today;
						int start = DayOfWeek.Sunday - thisDay.DayOfWeek;
						DateTime sunday = thisDay.AddDays(start);

						int end = DayOfWeek.Saturday - thisDay.DayOfWeek;
						DateTime saturday = thisDay.AddDays(end);
            //DateTime startOneWeekAgo = DateTime.Today.AddDays(-7).Date.AddDays(-(int)DateTime.Today.DayOfWeek);
            //DateTime endOneWeekAgo = startOneWeekAgo.AddDays(7);
            //DateTime startOneYearAgo = DateTime.Today.Year;

            /*DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            Calendar c = dfi.Calendar;
            int weekoftoday = c.GetWeekOfYear(DateTime.Today, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);*/
						DateTime? imprintingdate;
            for (int i = 0; i < imprintedcouponList.Count; i++)
            {
							switch(Status)
							{
								case "Redeemed":
									imprintingdate = imprintedcouponList[i].RedemptionDate;
									break;
								case "Expired":
									imprintingdate = imprintedcouponList[i].ExpirationDate;
									break;
								default:
									imprintingdate = imprintedcouponList[i].ImprintingDate;
									break;
							}


              if (imprintingdate != null)
              {
                  //int weekofimprintingdate = c.GetWeekOfYear(imprintingdate.Value, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
									if(imprintingdate.Value.Day.Equals(DateTime.Now.Day) && imprintingdate.Value.Month.Equals(DateTime.Now.Month) && imprintingdate.Value.Year.Equals(DateTime.Now.Year))//(imprintingdate >= DateTime.Today && imprintingdate <= DateTime.Today.AddDays(1).AddSeconds(-1))
                  {
                      today++;
                  }

                  if (imprintingdate >= sunday && imprintingdate <= saturday)//(weekoftoday == weekofimprintingdate)
                  {
                      thisweek++;
                  }

                  if (imprintingdate.Value.Month == DateTime.Today.Month)
                  {
                      thismonth++;
                  }


                  if (imprintingdate.Value.Year == DateTime.Today.Year)
                  {
                      thisyear++;
                  }
              }

            }

            upontotal model = new upontotal();
            model.UponStatus = Status;
            model.Today = today.ToString();
            model.ThisWeek = thisweek.ToString();
            model.ThisMonth = thismonth.ToString();
            model.ThisYear = thisyear.ToString();
            model.AllTime = alltime.ToString();
            return model;
        }

        #endregion

        #endregion
    }
}
