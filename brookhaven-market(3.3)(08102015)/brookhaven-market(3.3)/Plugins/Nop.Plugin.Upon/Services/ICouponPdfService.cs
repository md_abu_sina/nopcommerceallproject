﻿using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Localization;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.Upon.Services
{
    public interface ICouponPdfService
    {
        void PrintProductsToPdf(UniqueCoupon uniquecoupon, ImprintedCoupon imprintedcoupon, string qrCode, Language lang, string filePath);
        
    }
}
