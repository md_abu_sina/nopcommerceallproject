﻿using System;
using Nop.Core;

namespace Nop.Plugin.Upon.Domain
{
    public class ImprintedCoupon : BaseEntity
    {
        /// <summary>
        /// Gets or sets the ImprintedCoupon id.
        /// </summary>
        /// <value>
        /// The ImprintedCoupon id.
        /// </value>
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        /// <value>
        /// The Title.
        /// </value>
        public virtual string Title { get; set; }


        /// <summary>
        /// Gets or sets the Offer.
        /// </summary>
        /// <value>
        /// The Offer.
        /// </value>
        public virtual string Offer { get; set; }

        /// <summary>
        /// Gets or sets the Restrictions.
        /// </summary>
        /// <value>
        /// The Restrictions.
        /// </value>
        public virtual string Restrictions { get; set; }

        /// <summary>
        /// Gets or sets the first name of customer.
        /// </summary>
        /// <value>
        /// Customer's first name.
        /// </value>
        public virtual string First_Name { get; set; }

        /// <summary>
        /// Gets or sets the last name of customer.
        /// </summary>
        /// <value>
        /// Customer's last name.
        /// </value>
        public virtual string Last_Name { get; set; }


        /// <summary>
        /// Gets or sets the Email of customer.
        /// </summary>
        /// <value>
        /// Customer's Email.
        /// </value>
        public virtual string Email { get; set; }

        /// <summary>
        /// Gets or sets the Coupon id.
        /// </summary>
        /// <value>
        /// The Coupon id.
        /// </value>
        public virtual int CouponId { get; set; }

        /// <summary>
        /// Gets or sets the Unique Id imprinted Coupon.
        /// </summary>
        /// <value>
        /// The Unique Id imprinted Coupon.
        /// </value>
        public virtual Guid UniqueId_imprintedCoupon { get; set; }


        /// <summary>
        /// Gets or sets the CustomerID.
        /// </summary>
        /// <value>
        /// The CustomerID.
        /// </value>
        public virtual int CustomerID { get; set; }


        /// <summary>
        /// Gets or sets the Expiration Date.
        /// </summary>
        /// <value>
        /// The Expiration Date.
        /// </value>
        public virtual DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets Status.
        /// </summary>
        /// <value>
        /// Status.
        /// </value>
        public virtual String Status { get; set; }


        /// <summary>
        /// Gets or sets Redemption Date.
        /// </summary>
        /// <value>
        /// Redemption Date.
        /// </value>
        public virtual DateTime? RedemptionDate { get; set; }

        /// <summary>
        /// Gets or sets Amount.
        /// </summary>
        /// <value>
        /// Amount.
        /// </value>
        public virtual string Amount { get; set; }

          /// <summary>
        /// Gets or sets Amount in decimal.
        /// </summary>
        /// <value>
        /// Amount in decimal.
        /// </value>
        public virtual double? AmountInDecimal { get; set; }
        
        /// <summary>
        /// Gets or sets the StartDate.
        /// </summary>
        /// <value>
        /// The StartDate. when the uPon will be published
        /// </value>
        public virtual DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate.
        /// </summary>
        /// <value>
        /// The EndDate.  when the uPon will be taken down
        /// </value>
        public virtual DateTime EndDate { get; set; }

        public virtual string PdfPath { get; set; }

        public virtual string QrcodeImgPath { get; set; }

        public virtual string UPC { get; set; }

        /// <summary>
        /// Gets or sets Imprinting Date.
        /// </summary>
        /// <value>
        /// Imprinting Date
        public virtual DateTime? ImprintingDate { get; set; }
        

    }

}
