﻿using System.Data.Entity.ModelConfiguration;
using System;
using Nop.Plugin.Upon.Domain;

namespace Nop.Plugin.Upon.Data
{
    public class ImprintedCouponMap : EntityTypeConfiguration<ImprintedCoupon>
    {

        public ImprintedCouponMap()
        {
            ToTable("Nop_ImprintedCoupon");

            //Map the primary key
            HasKey(m => m.Id);

            //Map the additional properties

            Property(m => m.CouponId);
            Property(m => m.UniqueId_imprintedCoupon);
            Property(m => m.CustomerID);
            Property(m => m.ExpirationDate);
            Property(m => m.Status);
            Property(m => m.RedemptionDate);
            Property(m => m.Amount);
            Property(m => m.AmountInDecimal);
            Property(m => m.StartDate); 
            Property(m => m.EndDate);

            Property(m => m.PdfPath);
            Property(m => m.QrcodeImgPath);
            Property(m => m.ImprintingDate);
           
        }
    }
}
