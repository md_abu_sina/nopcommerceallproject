﻿using Nop.Core;
using System;

namespace Nop.Plugin.Recipe.Domain
{
    public class SpecialShoppingList : BaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int CustomerId { get; set; }
        public virtual int SpecialId { get; set; }
        public virtual string Brand { get; set; }
        public virtual string Product { get; set; }
        public virtual string Description { get; set; }
        public virtual string Offer { get; set; }
        public virtual string Image { get; set; }
        public virtual string RecipeTitle { get; set; }
        public virtual int RecipeId { get; set; }
        public virtual bool IsFeatured { get; set; }
        public virtual DateTime? TimeStampUtc { get; set; }
    }
}
