﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.Recipe.Models
{
    public class RecipeGallary
    {
        public string RecipeTitle { set; get; }
        public string TagId { set; get; }
        public string Photo { set; get; }
    }

    public class Recipemodel
    {
        public string title { set; get; }
        public string id { set; get; }
    }

    public class RecipeCalenderItem
    {
        public string title { set; get; }
        public int id { set; get; }
        public string Photo { set; get; }
        public string url { set; get; }
    }

    public class RecipesInCalender
    {
        public int id { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string start { get; set; }

    }
}
