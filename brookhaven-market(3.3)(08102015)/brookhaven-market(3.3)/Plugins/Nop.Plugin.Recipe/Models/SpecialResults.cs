﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Nop.Plugin.Recipe.Models
{
    public class SpecialResults
    {
        public SpecialsModel GetDepartmentsByServiceUrl(string serviceUrl)
        {
            SpecialsModel specials = new SpecialsModel();
            specials.Departments = new List<Department>();
            XDocument xmlPost = new XDocument();
            try
            {
                xmlPost = XDocument.Load(serviceUrl);
            }
            catch (Exception e)
            {
                return null;
            }
            XElement xServiceResult = xmlPost.Root;
            foreach (var department in xServiceResult.Element("departments").Elements("department"))
            {
                specials.Departments.Add(new Department { name = department.Attribute("name") != null ? department.Attribute("name").Value : string.Empty, id = department.Attribute("id") != null ? department.Attribute("id").Value : string.Empty, image = department.Attribute("image") != null ? department.Attribute("image").Value : string.Empty });
            }

            return specials;
        }

        public SpecialsModel GetSpecialsByServiceUrl(string serviceUrl)
        {
            SpecialsModel specials = new SpecialsModel();
            Special special_all = new Special();
            specials.Departments = new List<Department>();
            specials.Pagination = new SpecialPagination();
            specials.Specials = new List<Special>();
            XDocument xmlPost = new XDocument();
            try
            {
                xmlPost = XDocument.Load(serviceUrl);
            }
            catch (Exception e)
            {
                return null;
            }
            XElement xServiceResult = xmlPost.Root;
            foreach (var department in xServiceResult.Element("departments").Elements("department"))
            {
                //specials.Departments.Add(new Department { name = department.Attribute("name") != null ? department.Attribute("name").Value : string.Empty, id = department.Attribute("id") != null ? department.Attribute("id").Value : string.Empty });
                specials.Departments.Add(new Department { name = department.Attribute("name") != null ? department.Attribute("name").Value : string.Empty, id = department.Attribute("id") != null ? department.Attribute("id").Value : string.Empty, image = department.Attribute("image") != null ? department.Attribute("image").Value : string.Empty });
            }
            foreach (var special in xServiceResult.Element("specials").Elements("special"))
            {
                string recipe_title = string.Empty;
                string recipe_id = string.Empty;
                if (special.Element("recipe") != null)
                {
                    recipe_id = special.Element("recipe").Attribute("id") != null ? special.Element("recipe").Attribute("id").Value : string.Empty;
                    recipe_title = special.Element("recipe").Attribute("title") != null ? "Recipe: " + special.Element("recipe").Attribute("title").Value : string.Empty;
                }
                specials.Specials.Add(new Special { featured = special.Attribute("featured") != null ? special.Attribute("featured").Value : "false", id = special.Attribute("id") != null ? special.Attribute("id").Value : string.Empty, brand = special.Element("brand") != null ? special.Element("brand").Value : string.Empty, product = special.Element("product") != null ? special.Element("product").Value : string.Empty, description = special.Element("description") != null ? special.Element("description").Value : string.Empty, offer = special.Element("offer") != null ? special.Element("offer").Value : string.Empty, image = special.Element("image") != null ? special.Element("image").Value : string.Empty, recipe_id = recipe_id, recipe_title = recipe_title });
                //specials.Specials.Add(new Special { featured = special.Attribute("featured") != null ? special.Attribute("featured").Value : "false", id = special.Attribute("id") != null ? special.Attribute("id").Value : string.Empty, brand = special.Element("brand") != null ? special.Element("brand").Value : string.Empty, product = special.Element("product") != null ? special.Element("product").Value : string.Empty, description = special.Element("description") != null ? special.Element("description").Value : string.Empty, offer = special.Element("offer") != null ? special.Element("offer").Value : string.Empty, image = special.Element("image") != null ? special.Element("image").Value : string.Empty, recipe_id = recipe_id, recipe_title = recipe_title });
            }
            specials.Pagination.current_page = xServiceResult.Element("pagination").Element("current_page") != null ? xServiceResult.Element("pagination").Element("current_page").Value : string.Empty;
            specials.Pagination.next_page = xServiceResult.Element("pagination").Element("next_page") != null ? xServiceResult.Element("pagination").Element("next_page").Value : string.Empty;
            specials.Pagination.total_pages = xServiceResult.Element("pagination").Element("total_pages") != null ? xServiceResult.Element("pagination").Element("total_pages").Value : string.Empty;
            specials.Pagination.per_page = xServiceResult.Element("pagination").Element("per_page") != null ? xServiceResult.Element("pagination").Element("per_page").Value : string.Empty;
            specials.Pagination.start_record = xServiceResult.Element("pagination").Element("start_record") != null ? xServiceResult.Element("pagination").Element("start_record").Value : string.Empty;
            specials.Pagination.end_record = xServiceResult.Element("pagination").Element("end_record") != null ? xServiceResult.Element("pagination").Element("end_record").Value : string.Empty;
            specials.Pagination.total_records = xServiceResult.Element("pagination").Element("total_records") != null ? xServiceResult.Element("pagination").Element("total_records").Value : string.Empty;

            return specials;
        }
     
    }
}
