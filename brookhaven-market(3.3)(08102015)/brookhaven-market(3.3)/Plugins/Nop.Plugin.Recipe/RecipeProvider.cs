﻿using Nop.Core.Plugins;
using Nop.Plugin.Recipe.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Web;
//using Nop.Admin;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Recipe
{
    public class RecipeProvider : BasePlugin, IAdminMenuPlugin
    {
        private readonly CookCookObjectContext _context;
        private readonly ShoppingListObjectContext _shoppinglistcontext;
        private readonly SpecialShoppingListObjectContext _specialshoppinglistcontext;
        private readonly ShoppinglistOwnitemsObjectContext _shoppinglistownitemscontext;
        private readonly RecipeCalenderObjectContext _recipeCalendercontext;

        public RecipeProvider(CookCookObjectContext context, ShoppingListObjectContext shoppinglistcontext, SpecialShoppingListObjectContext specialshoppinglistcontext, ShoppinglistOwnitemsObjectContext shoppinglistownitemscontext, RecipeCalenderObjectContext recipeCalendercontext)
        {
            _context = context;
            _shoppinglistcontext = shoppinglistcontext;
            _specialshoppinglistcontext = specialshoppinglistcontext;
            _shoppinglistownitemscontext = shoppinglistownitemscontext;
            _recipeCalendercontext = recipeCalendercontext;
        }

        /*public void BuildMenuItem(Telerik.Web.Mvc.UI.MenuItemBuilder menuItemBuilder)
        {
            menuItemBuilder.Text("Recipe Calender");
            menuItemBuilder.Url("/Admin/Plugin/Recipe/Celender/Index");
            menuItemBuilder.Route("Admin.Plugin.Recipe.Celender.Index");
  
        }*/

				public SiteMapNode BuildMenuItem()
				{
					SiteMapNode node = new SiteMapNode
					{
						Visible = true,
						Title = "Recipe Calender",
						Url = "/Admin/Plugin/Recipe/Celender/Index"
					};
					
					return node;
				}

        public override void Install() {
           // _context.InstallSchema();
            //_shoppinglistcontext.InstallSchema();
            //_specialshoppinglistcontext.InstallSchema();
            //_shoppinglistownitemscontext.InstallSchema();
           // _recipeCalendercontext.InstallSchema();
            base.Install();

            this.AddOrUpdatePluginLocaleResource("Account.Fields.StoreRole", "Store Role");
            this.AddOrUpdatePluginLocaleResource("Admin.Customers.CustomerRoles.Fields.StoreRole", "Store Role");
            this.AddOrUpdatePluginLocaleResource("Admin.Customers.CustomerRoles.Fields.StoreId", "Store Id");
            this.AddOrUpdatePluginLocaleResource("Admin.Configuration.Settings.GeneralCommon.ChainID", "Chain Id");
            this.AddOrUpdatePluginLocaleResource("Account.PreferredStore", "Preferred Store");
        }

				public bool Authenticate()
				{
					return true;
				}
		}
}
