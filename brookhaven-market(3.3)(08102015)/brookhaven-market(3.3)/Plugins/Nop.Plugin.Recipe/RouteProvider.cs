﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Recipe
{
    public class RouteProvider : IRouteProvider
    {
        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Admin.Plugin.Recipe.Celender.Index", "Admin/Plugin/Recipe/Celender/Index",
                new { controller = "Recipe", action = "GetRecipeCalender" },
                         new[] { "Nop.Plugin.Recipe.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.Recipe.Celender.getAllCalenderRecipes", "Admin/Plugin/Recipe/Celender/getAllCalenderRecepies",
                           new { controller = "Recipe", action = "getAllCalenderRecepies" },
                           new[] { "Nop.Plugin.Recipe.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Recipe.Celender.addrecipestothisday", "Admin/Plugin/Recipe/Celender/addrecipestothisday",
                           new { controller = "Recipe", action = "addrecipestothisday" },
                           new[] { "Nop.Plugin.Recipe.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Recipe.Celender.checkIfDayisAlreadyOccupied", "Admin/Plugin/Recipe/Celender/checkIfDayisAlreadyOccupied",
                       new { controller = "Recipe", action = "checkIfDayisAlreadyOccupied" },
                       new[] { "Nop.Plugin.Recipe.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Recipe.Celender.ShowRecipeInCalenderPage", "Admin/Plugin/Recipe/Celender/ShowRecipeInCalenderPage",
                       new { controller = "Recipe", action = "ShowRecipeInCalenderPage"},
                        new { recipeid = @"\d+" },
                       new[] { "Nop.Plugin.Recipe.Controllers" }).DataTokens.Add("area", "admin");
           
            routes.MapRoute("Admin.Plugin.Recipe.Celender.deleteRecipeforThisDay", "Admin/Plugin/Recipe/Celender/deleteRecipeforThisDay",
                       new { controller = "Recipe", action = "deleteRecipeforThisDay" },
                       new[] { "Nop.Plugin.Recipe.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("searchInRecipeCalender", "searchinrecipecalender",
                       new { controller = "Recipe", action = "getRecipeListForCalenderPage" },
                       new[] { "Nop.Plugin.Recipe.Controllers" }).DataTokens.Add("area", "admin"); ;

            routes.MapRoute("dailyrecipe", "dailyrecipe",
               new { controller = "Recipe", action = "Public_recipe_display" },
                             new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("autocomplete", "getautocompleteSearchresults",
                         new { controller = "Recipe", action = "getautocompleteSearchresults" },
                         new[] { "Nop.Plugin.Recipe.Controllers" });


            routes.MapRoute("recipeindex", "recipes",
                            new { controller = "Recipe", action = "Index" },
                            new[] { "Nop.Plugin.Recipe.Controllers" });


            routes.MapRoute("showSingleRecipe", "recipes/{recipeid}",
                           new { controller = "Recipe", action = "showSingleRecipe", recipeid = "" },
                           new { recipeid = @"\d+" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });


            routes.MapRoute("search_ingredient", "recipes/search/ingredient/{ingredient}",
                           new { controller = "Recipe", action = "showRecipeList", ingredient = "" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("search_title", "recipes/search/title/{title}",
                           new { controller = "Recipe", action = "showRecipeList", title = "" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });



            routes.MapRoute("recipewithtags", "recipes/tags/{tags}",
                           new { controller = "Recipe", action = "showRecipeList", tags = "" },

                           new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("recipewithpagesforMobile", "Recipe/showRecipeList/",
                           new { controller = "Recipe", action = "showrecipelist_mobile" },

                           new[] { "Nop.Plugin.Recipe.Controllers" });


            routes.MapLocalizedRoute("showSingleRecipeafterscale", "recipes/{recipeid}/{servings}/{units}/{commit}",
                         new { controller = "Recipe", action = "showSingleRecipe", recipeid = "", servings = "", units = "", commit = "" },

                         new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("Nop.Plugin.Recipe.AddTocookBook", "AddTocookBook",
                   new { controller = "Recipe", action = "AddTocookBook" },
                   new[] { "Nop.Plugin.Recipe.Controllers" });
            routes.MapRoute("AddTocookBook_mobile", "AddTocookBook_mobile",
                 new { controller = "Recipe", action = "AddTocookBook_mobile" },
                 new[] { "Nop.Plugin.Recipe.Controllers" });
            routes.MapRoute("Nop.Plugin.Recipe.AddToshoppingList", "AddToshoppingList",
                   new { controller = "Recipe", action = "AddToshoppingList" },
                   new[] { "Nop.Plugin.Recipe.Controllers" });
            routes.MapRoute("Nop.Plugin.Recipe.DeleteRecipeFromshoppinglist", "DeleteRecipeFromshoppinglist",
                  new { controller = "Recipe", action = "DeleteRecipeFromshoppinglist" },
                  new[] { "Nop.Plugin.Recipe.Controllers" });
            routes.MapRoute("Nop.Plugin.Recipe.DeleteAllRecipeFromshoppinglist", "DeleteAllRecipeFromshoppinglist",
                  new { controller = "Recipe", action = "DeleteAllRecipeFromshoppinglist" },
                  new[] { "Nop.Plugin.Recipe.Controllers" });
            routes.MapRoute("Nop.Plugin.Recipe.DeleteRecipeFromshoppinglistMobile", "DeleteRecipeFromshoppinglistMobile",
                  new { controller = "Recipe", action = "DeleteRecipeFromshoppinglistMobile" },
                  new[] { "Nop.Plugin.Recipe.Controllers" });
            routes.MapRoute("Nop.Plugin.Recipe.DeleteRecipeFromCookBook", "DeleteRecipeFromCookBook",
                   new { controller = "Recipe", action = "DeleteRecipeFromCookBook" },
                   new[] { "Nop.Plugin.Recipe.Controllers" });
            routes.MapRoute("Nop.Plugin.Recipe.DeleteRecipeFromCookBookForMobile", "DeleteRecipeFromCookBookForMobile",
                   new { controller = "Recipe", action = "DeleteRecipeFromCookBookForMobile" },
                   new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("AddToshoppingList_mobile", "AddToshoppingList_mobile",
                  new { controller = "Recipe", action = "AddToshoppingList_mobile" },
                  new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("addtoShoppingListUserItems", "recipes/addtoShoppingListUserItems",
                  new { controller = "Recipe", action = "addtoShoppingListUserItems" },
                  new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("deleteShoppingListUserItems", "recipes/deleteShoppingListUserItems",
                  new { controller = "Recipe", action = "deleteShoppingListUserItems" },
                  new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("Nop.Plugin.Recipe.ShoppingList", "ShoppingList",
                 new { controller = "Recipe", action = "ShoppingList" },
                 new[] { "Nop.Plugin.Recipe.Controllers" });


            routes.MapRoute("tipsAndGuidesindex", "tipsandguides",
                          new { controller = "Recipe", action = "tipsAndGuids" },
                          new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("tipsAndGuides", "tipsandguides/{id}",
                           new { controller = "Recipe", action = "tipsAndGuids", id = "" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("searchTips", "tipsandguides/search/{search}",
                           new { controller = "Recipe", action = "tipsAndGuids", search = "" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("TipsListMobile", "nextpage/TipsdisplayableItems_mobile",
                           new { controller = "Recipe", action = "TipsdisplayableItems_mobile" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("getautocompleteTipsSearchresults", "getautocompleteTipsSearchresults",
                       new { controller = "Recipe", action = "getautocompleteTipsSearchresults" },
                       new[] { "Nop.Plugin.Recipe.Controllers" });


            routes.MapRoute("specials", "specials",
                            new { controller = "Recipe", action = "specials" },
                            new[] { "Nop.Plugin.Recipe.Controllers" });
						routes.MapRoute("SpecialsWithAdGroup", "SpecialsWithAdGroup/{storeName}",
                            new { controller = "Recipe", action = "SpecialsWithAdGroup", storeName = ""},
                            new[] { "Nop.Plugin.Recipe.Controllers" });
						routes.MapRoute("SpecialListWithAdGroup", "SpecialsWithAdGroup/{storeName}/department_id/{department_id}",
                           new { controller = "Recipe", action = "SpecialsWithAdGroup", storeName = "", department_id = ""},
                           new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("specials_all", "specials_all",
                            new { controller = "Recipe", action = "specials_all" },
                            new[] { "Nop.Plugin.Recipe.Controllers" });

						routes.MapRoute("AdGroupSpecialsAll", "AdGroupSpecialsAll/{storeName}",
                            new { controller = "Recipe", action = "AdGroupSpecialsAll", storeName = "" },
                            new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("specialList", "specials/department_id/{department_id}",
                           new { controller = "Recipe", action = "specials", department_id = "" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });


            routes.MapRoute("specialsearch", "specials/department_id/{department_id}/search/{search}",
                           new { controller = "Recipe", action = "specials", department_id = "", search = "" },
                           new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("specialpagesize", "specials/department_id/{department_id}/pagesize/{pagesize}/search/{search}",
                          new { controller = "Recipe", action = "specials", department_id = "", pagesize = "", search = "" },
                          new[] { "Nop.Plugin.Recipe.Controllers" });



            routes.MapRoute("specialListMobile", "specials/specials_mobile",
                          new { controller = "Recipe", action = "specials_mobile" },
                          new[] { "Nop.Plugin.Recipe.Controllers" });

            routes.MapRoute("Nop.Plugin.Recipe.AddTospecialshoppingList", "AddTospecialshoppingList",
                  new { controller = "Recipe", action = "AddTospecialshoppingList" },
                  new[] { "Nop.Plugin.Recipe.Controllers" });


        }

        public int Priority
        {
            get { return 0; }
        }

        #endregion
    }
}