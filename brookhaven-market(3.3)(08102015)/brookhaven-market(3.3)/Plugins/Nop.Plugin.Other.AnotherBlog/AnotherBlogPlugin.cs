﻿using Nop.Core.Plugins;
using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using Nop.Core.Data;
using System;
using Nop.Web.Framework.Menu;


namespace Nop.Plugin.Other.AnotherBlog
{
	public class AnotherBlogPlugin : BasePlugin, IAdminMenuPlugin
	{


		public AnotherBlogPlugin()
		{
		}

		public SiteMapNode BuildMenuItem()
		{
			SiteMapNode node = new SiteMapNode
			{
				Visible = true,
				Title = "Custom Blog",
				Url = "/Admin/Plugin/Other/AnotherBlog/AdminList"
			};


			return node;
		}

		public override void Install()
		{
      //_context.Install();
			//_commentContext.Install();
      base.Install();

			/*this.AddOrUpdatePluginLocaleResource("admin.plugin.other.specialevent.list", "Registered Users for Special Event");

			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Name", "Full name");
			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Phone", "Phone number");
			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Email", "E-mail address");
			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.GuestNumber", "Number of guests");
			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.BookingDate", "Booking Date");

			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Name.Required", "Full Name is Required");
			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Email.Required", "Email address is Required");
			this.AddOrUpdatePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Phone.Required", "Phone Number is Required");

			this.AddOrUpdatePluginLocaleResource("admin.plugin.other.specialevent.formmessage", "Thank you for registering for the Healthy Weight Event with Dr. Lindsey Duncan Wed. May 15 7:00 p.m. at The Colleyville Center. Please complete the following fields to reserve your seat(s). ");*/
		}

		public bool Authenticate()
		{
			return true;
		}

		/// <summary>
		/// Uninstall plugin
		/// </summary>
		public override void Uninstall()
		{
			//database objects
			//_context.Uninstall();
			//_commentContext.Uninstall();
			//locales
			/*this.DeletePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Name");
			this.DeletePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Phone");
			this.DeletePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.Email");
			this.DeletePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.GuestNumber");
			this.DeletePluginLocaleResource("Plugin.Other.SpecialEvents.Fields.BookingDate");
			this.DeletePluginLocaleResource("admin.plugin.other.specialevent.formmessage");*/

			base.Uninstall();
		}

	}
}
