﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Services.Blogs;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Seo;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.UI.Captcha;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Blogs;
using Nop.Admin.Controllers;
using Nop.Services.Security;
using Nop.Core.Domain.Common;
using Nop.Plugin.Other.AnotherBlog.Models;
using Nop.Web.Controllers;
using Nop.Plugin.Other.AnotherBlog.Services;
using Nop.Web.Framework.Kendoui;
using Nop.Services.Stores;

namespace Nop.Plugin.Other.AnotherBlog.Controllers
{
    [NopHttpsRequirement(SslRequirement.No)]
		public partial class AnotherBlogAdminController : BaseAdminController
    {
				#region Fields

        private readonly IBlogService _blogService;
				private readonly IAnotherBlogService _anotherBlogService;
        private readonly IWorkContext _workContext;
				private readonly IStoreContext _storeContext;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;

        private readonly MediaSettings _mediaSettings;
        private readonly BlogSettings _blogSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly CaptchaSettings _captchaSettings;

				private readonly IPermissionService _permissionService;
				private readonly AdminAreaSettings _adminAreaSettings;
				private readonly ILanguageService _languageService;
				private readonly IGenericAttributeService _genericAttributeService;
				private readonly IUrlRecordService _urlRecordService;
				private readonly IStoreService _storeService;
				private readonly IStoreMappingService _storeMappingService;

        
        #endregion

				#region Constructors

        public AnotherBlogAdminController(IBlogService blogService, IAnotherBlogService anotherBlogService, IPermissionService permissionService,
            IWorkContext workContext, IPictureService pictureService, ILocalizationService localizationService,
            IDateTimeHelper dateTimeHelper, IWorkflowMessageService workflowMessageService, IWebHelper webHelper,
            ICacheManager cacheManager, AdminAreaSettings adminAreaSettings, IGenericAttributeService genericAttributeService,
            MediaSettings mediaSettings, BlogSettings blogSettings, ILanguageService languageService,
            LocalizationSettings localizationSettings, CustomerSettings customerSettings, IStoreContext storeContext,
						StoreInformationSettings storeInformationSettings, CaptchaSettings captchaSettings,
						IUrlRecordService urlRecordService, IStoreService storeService, IStoreMappingService storeMappingService)
        {
            this._blogService = blogService;
						this._anotherBlogService = anotherBlogService;
            this._workContext = workContext;
						this._storeContext = storeContext;
            this._pictureService = pictureService;
            this._localizationService = localizationService;
            this._dateTimeHelper = dateTimeHelper;
            this._workflowMessageService = workflowMessageService;
            this._webHelper = webHelper;
            this._cacheManager = cacheManager;

            this._mediaSettings = mediaSettings;
            this._blogSettings = blogSettings;
            this._localizationSettings = localizationSettings;
            this._customerSettings = customerSettings;
            this._storeInformationSettings = storeInformationSettings;
            this._captchaSettings = captchaSettings;

						this._permissionService = permissionService;
						this._adminAreaSettings = adminAreaSettings;
						this._languageService = languageService;
						this._genericAttributeService = genericAttributeService;
						this._urlRecordService = urlRecordService;
						this._storeService = storeService;
						this._storeMappingService = storeMappingService;
        }

				#endregion

        #region Utilities
				[NonAction]
				private void PrepareStoresMappingModel(BlogPostAdminModel model, BlogPost blogPost, bool excludeProperties)
				{
					if(model == null)
						throw new ArgumentNullException("model");

					model.AvailableStores = _storeService
							.GetAllStores()
							.Select(s => s.ToModel())
							.ToList();
					if(!excludeProperties)
					{
						if(blogPost != null)
						{
							model.SelectedStoreIds = _storeMappingService.GetStoresIdsWithAccess(blogPost);
						}
						else
						{
							model.SelectedStoreIds = new int[0];
						}
					}
				}

				[NonAction]
				protected void SaveStoreMappings(BlogPost blogPost, BlogPostAdminModel model)
				{
					var existingStoreMappings = _storeMappingService.GetStoreMappings(blogPost);
					var allStores = _storeService.GetAllStores();
					foreach(var store in allStores)
					{
						if(model.SelectedStoreIds != null && model.SelectedStoreIds.Contains(store.Id))
						{
							//new role
							if(existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
								_storeMappingService.InsertStoreMapping(blogPost, store.Id);
						}
						else
						{
							//removed role
							var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
							if(storeMappingToDelete != null)
								_storeMappingService.DeleteStoreMapping(storeMappingToDelete);
						}
					}
				}
				[NonAction]
				protected bool ShortDescriptionIsExist(int entityId)
				{
					return _genericAttributeService.GetAttributesForEntity(entityId, "BlogPost").Where(ga => ga.Key.Equals("ShortDescription")).Any();
				}

				[NonAction]
				protected void SaveShortDescription(BlogPostAdminModel model)
				{
					if(this.ShortDescriptionIsExist(model.Id))
					{
						var shortDescription = _genericAttributeService.GetAttributesForEntity(model.Id, "BlogPost").Where(ga => ga.Key.Equals("ShortDescription")).FirstOrDefault();
						shortDescription.Value = model.ShortDescription;
						_genericAttributeService.UpdateAttribute(shortDescription);
					}
					else
					{
						GenericAttribute ga = new GenericAttribute();
						ga.KeyGroup = "BlogPost";
						ga.EntityId = model.Id;
						ga.Key = "ShortDescription";
						ga.Value = string.IsNullOrEmpty(model.ShortDescription) ? "" : model.ShortDescription;
						ga.StoreId = 0;

						_genericAttributeService.InsertAttribute(ga);
					}
				}

        #endregion

      

				#region Admin Region
				public ActionResult AdminList()
				{
					if(!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
						return AccessDeniedView();

					return View();
				}

				[HttpPost]
				public ActionResult AdminList(DataSourceRequest command)
				{
					if(!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
						return AccessDeniedView();

					var blogPosts = _blogService.GetAllBlogPosts(0, 0, null, null, command.Page - 1, command.PageSize, true);
					var gridModel = new DataSourceResult
					{
						Data = blogPosts.Select(x =>
						{
							var m = x.ToModel();
							if(x.StartDateUtc.HasValue)
								m.StartDate = _dateTimeHelper.ConvertToUserTime(x.StartDateUtc.Value, DateTimeKind.Utc);
							if(x.EndDateUtc.HasValue)
								m.EndDate = _dateTimeHelper.ConvertToUserTime(x.EndDateUtc.Value, DateTimeKind.Utc);
							m.CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc);
							m.LanguageName = x.Language.Name;
							m.ShortDescription = this.ShortDescriptionIsExist(x.Id) ? _genericAttributeService.GetAttributesForEntity(x.Id, "BlogPost").Where(ga => ga.Key.Equals("ShortDescription")).FirstOrDefault().Value : String.Empty;
							return m;
						}),
						Total = blogPosts.TotalCount
					};
					return new JsonResult
					{
						Data = gridModel
					};
				}

				public ActionResult Create()
				{
					if(!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
						return AccessDeniedView();

					ViewBag.AllLanguages = _languageService.GetAllLanguages(true);
					var model = new BlogPostAdminModel();
					//Stores
					PrepareStoresMappingModel(model, null, false);
					//default values
					model.AllowComments = true;
					return View(model);
				}

				[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
				public ActionResult Create(BlogPostAdminModel model, bool continueEditing)
				{
					if(!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
						return AccessDeniedView();

					if(ModelState.IsValid)
					{
						var blogPost = model.ToEntity();
						blogPost.StartDateUtc = model.StartDate;
						blogPost.EndDateUtc = model.EndDate;
						blogPost.CreatedOnUtc = DateTime.UtcNow;
						_blogService.InsertBlogPost(blogPost);

						//search engine name
						var seName = blogPost.ValidateSeName(model.SeName, model.Title, true);
						_urlRecordService.SaveSlug(blogPost, seName, blogPost.LanguageId);

						//Stores
						SaveStoreMappings(blogPost, model);

						model.Id = blogPost.Id;
						SaveShortDescription(model);

						SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Added"));
						return continueEditing ? RedirectToAction("Edit", "/Plugin/Other/AnotherBlog", new
						{
							id = blogPost.Id
						}) : RedirectToAction("AdminList", "/Plugin/Other/AnotherBlog");
					}

					//If we got this far, something failed, redisplay form
					ViewBag.AllLanguages = _languageService.GetAllLanguages(true);
					//Stores
					PrepareStoresMappingModel(model, null, true);
					return View(model);
				}

				public ActionResult Edit(int id)
				{
					if(!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
						return AccessDeniedView();

					var blogPost = _blogService.GetBlogPostById(id);
					if(blogPost == null)
						//No blog post found with the specified id
						return RedirectToAction("List");

					ViewBag.AllLanguages = _languageService.GetAllLanguages(true);
					var model = blogPost.ToModel();
					model.ShortDescription = this.ShortDescriptionIsExist(blogPost.Id) ? _genericAttributeService.GetAttributesForEntity(blogPost.Id, "BlogPost").Where(ga => ga.Key.Equals("ShortDescription")).FirstOrDefault().Value : String.Empty;
					model.StartDate = blogPost.StartDateUtc;
					model.EndDate = blogPost.EndDateUtc;
					
					//Store
					PrepareStoresMappingModel(model, blogPost, false);
					model.SeName = _urlRecordService.GetActiveSlug(blogPost.Id, "BlogPost", blogPost.LanguageId);
					return View(model);
				}

				[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
				public ActionResult Edit(BlogPostAdminModel model, bool continueEditing)
				{
					if(!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
						return AccessDeniedView();

					var blogPost = _blogService.GetBlogPostById(model.Id);
					if(blogPost == null)
						//No blog post found with the specified id
						return RedirectToAction("List");

					if(ModelState.IsValid)
					{
						blogPost = model.ToEntity(blogPost);
						blogPost.StartDateUtc = model.StartDate;
						blogPost.EndDateUtc = model.EndDate;
						_blogService.UpdateBlogPost(blogPost);

						//search engine name
						var seName = blogPost.ValidateSeName(model.SeName, model.Title, true);
						_urlRecordService.SaveSlug(blogPost, seName, blogPost.LanguageId);

						//Stores
						SaveStoreMappings(blogPost, model);
						
						SaveShortDescription(model);
						
						SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Updated"));
						
						if(continueEditing)
						{
							//selected tab
							SaveSelectedTabIndex();

							return RedirectToAction("Edit", "/Plugin/Other/AnotherBlog", new
							{
								id = blogPost.Id
							});
						}
						else
						{
							return RedirectToAction("AdminList", "/Plugin/Other/AnotherBlog");
						}
					}

					//If we got this far, something failed, redisplay form
					ViewBag.AllLanguages = _languageService.GetAllLanguages(true);
					//Store
					PrepareStoresMappingModel(model, blogPost, true);
					return View(model);
				}

				[HttpPost]
				public ActionResult Delete(int id)
				{
					if(!_permissionService.Authorize(StandardPermissionProvider.ManageBlog))
						return AccessDeniedView();

					var blogPost = _blogService.GetBlogPostById(id);
					if(blogPost == null)
						//No blog post found with the specified id
						return RedirectToAction("List");

					_blogService.DeleteBlogPost(blogPost);

					if(ShortDescriptionIsExist(blogPost.Id))
					{
						var shortDescription = _genericAttributeService.GetAttributesForEntity(blogPost.Id, "BlogPost").Where(ga => ga.Key.Equals("ShortDescription")).FirstOrDefault();
						_genericAttributeService.DeleteAttribute(shortDescription);
					}

					SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Blog.BlogPosts.Deleted"));
					return RedirectToAction("AdminList", "/Plugin/Other/AnotherBlog");
				}

				#endregion
			
    }
}
