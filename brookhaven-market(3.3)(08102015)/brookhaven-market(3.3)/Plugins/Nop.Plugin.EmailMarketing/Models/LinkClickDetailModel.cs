﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class LinkClickDetailModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
    public virtual int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		/// <value>The email address.</value>
    public virtual string EmailAddress { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the link click count.
		/// </summary>
		/// <value>The link click count.</value>
		public virtual int LinkClickCount { get; set; }
		
  }

}
