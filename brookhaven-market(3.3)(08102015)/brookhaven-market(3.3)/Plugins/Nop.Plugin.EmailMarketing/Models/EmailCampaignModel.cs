﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Web.Framework;
using Nop.Plugin.EmailMarketing.Validators;

namespace Nop.Plugin.EmailMarketing.Models
{
	[Validator(typeof(EmailCampaignValidator))]

	public class EmailCampaignModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.Name")]
		public string Name { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the subject.
		/// </summary>
		/// <value>The subject.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.Subject")]
		public string Subject { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the body.
		/// </summary>
		/// <value>The body.</value>
		[AllowHtml]
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.Body")]
		public string Body { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the created on UTC.
		/// </summary>
		/// <value>The created on UTC.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[UIHint("EmailAttachment")]
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.EmailAttachment")]
		public virtual string EmailAttachment {	get;	set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets to.
		/// </summary>
		/// <value>To.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.To")]
		public string To { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the cc.
		/// </summary>
		/// <value>The cc.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.Cc")]
		public string Cc { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the BCC.
		/// </summary>
		/// <value>The BCC.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.Bcc")]
		public string Bcc { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the schedule.
		/// </summary>
		/// <value>The schedule.</value>
		//[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.Schedule")]
		//public DateTime? Schedule { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the name of the group.
		/// </summary>
		/// <value>The name of the group.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.GroupName")]
		public string GroupName { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the allowed tokens.
		/// </summary>
		/// <value>The allowed tokens.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Campaigns.Fields.AllowedTokens")]
		public string AllowedTokens { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets a value indicating whether [from schedule].
		/// </summary>
		/// <value><c>true</c> if [from schedule]; otherwise, <c>false</c>.</value>
		public bool FromSchedule { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the tokens.
		/// </summary>
		/// <value>The tokens.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.MessageToken")]
		public IEnumerable<SelectListItem> Tokens { get; set; }

		
  }

}
