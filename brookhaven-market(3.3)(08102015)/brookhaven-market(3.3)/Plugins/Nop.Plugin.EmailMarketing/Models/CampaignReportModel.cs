﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class CampaignReportModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public virtual int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the campaign id.
		/// </summary>
		/// <value>The campaign id.</value>
		public virtual int CampaignId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		public virtual string Name { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the subject.
		/// </summary>
		/// <value>The subject.</value>
		public virtual string Subject { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the broad cast time.
		/// </summary>
		/// <value>The broad cast time.</value>
		public virtual DateTime BroadCastTime {	get;	set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the delivered.
		/// </summary>
		/// <value>The delivered.</value>
		public virtual int Delivered { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the opened.
		/// </summary>
		/// <value>The opened.</value>
		public virtual int Opened { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the clicked through.
		/// </summary>
		/// <value>The clicked through.</value>
		public virtual int ClickedThrough { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the delivered percentage.
		/// </summary>
		/// <value>The delivered percentage.</value>
		public virtual string DeliveredPercentage { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the opened percentage.
		/// </summary>
		/// <value>The opened percentage.</value>
		public virtual string OpenedPercentage { get; set; }
		
  }

}
