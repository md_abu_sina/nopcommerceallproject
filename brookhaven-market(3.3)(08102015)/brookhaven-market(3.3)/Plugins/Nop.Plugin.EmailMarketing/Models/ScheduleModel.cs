﻿using System;
using Nop.Web.Framework.Mvc;
using System.Web.Mvc;
using System.Collections;
using System.Collections.Generic;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class ScheduleModel : BaseNopEntityModel
  {

		public ScheduleModel()
    {
			EmailCampaign = new EmailCampaignModel();
    }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public virtual int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the campaign id.
		/// </summary>
		/// <value>The campaign id.</value>
		public virtual int CampaignId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the schedule id.
		/// </summary>
		/// <value>The schedule id.</value>
		public virtual string ScheduleId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the email campaign.
		/// </summary>
		/// <value>The email campaign.</value>
		public virtual EmailCampaignModel EmailCampaign { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the campaign.
		/// </summary>
		/// <value>The campaign.</value>
		public virtual SelectList Campaign { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the scheduled time.
		/// </summary>
		/// <value>The scheduled time.</value>
		public virtual DateTime ScheduledTime { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="Schedule"/> is status.
		/// </summary>
		/// <value><c>true</c> if status; otherwise, <c>false</c>.</value>
		public virtual bool Status { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the type. Help to decide if the schedule is Daily / Weekly / Yearly
		/// </summary>
		/// <value>The type.</value>
		public virtual string Type { get; set; }

		public virtual string ScheduledTimeString { get; set; }
  }

}
