﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class EmailTemplateModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.Name")]
		public string Name { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the picture id.
		/// </summary>
		/// <value>The picture id.</value>
		[UIHint("Picture")]
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.Picture")]
		public virtual int PictureId {	get;	set; }


		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the template path.
		/// </summary>
		/// <value>The template path.</value>
		[UIHint("ZipFile")]
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.ZipFileUpload")]
    public string TemplatePath { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the thumbnail path.
		/// </summary>
		/// <value>The thumbnail path.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.Thumb")]
		public string ThumbnailPath { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the tokens.
		/// </summary>
		/// <value>The tokens.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.MessageToken")]
		public IEnumerable<SelectListItem> Tokens { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the template body.
		/// </summary>
		/// <value>The template body.</value>
		[AllowHtml]
		[NopResourceDisplayName("Admin.EmailMarketing.Template.Fields.Body")]
		public string TemplateBody { get; set; }
		
  }

}
