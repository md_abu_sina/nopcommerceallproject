﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.EmailMarketing.Domain;

namespace Nop.Plugin.EmailMarketing.Data
{
    public partial class SmartGroupsMap : EntityTypeConfiguration<SmartGroups>
    {
        public SmartGroupsMap()
        {
						ToTable("SmartGroups");

						//Map the primary key
						HasKey(m => m.Id);

						//Map the additional properties
                       

            Property(e => e.Name);
						Property(e => e.Columns);
						Property(e => e.Conditions);
						Property(e => e.KeyWord);
						Property(e => e.AndOr);
            Property(e => e.Query);
     

        }
    }
}
