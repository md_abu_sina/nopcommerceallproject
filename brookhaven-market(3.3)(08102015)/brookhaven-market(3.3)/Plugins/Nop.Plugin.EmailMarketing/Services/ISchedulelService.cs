﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.EmailMarketing.Domain;
using System;

namespace Nop.Plugin.EmailMarketing.Services
{
    public interface IScheduleService
    {

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all schedule.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <returns></returns>
			IPagedList<Schedule> GetAllSchedule(int pageIndex, int pageSize);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all schedule by time.
			/// </summary>
			/// <param name="time">The time.</param>
			/// <returns></returns>
			IEnumerable<Schedule> GetAllScheduleByTime(DateTime time);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Inserts the schedule.
			/// </summary>
			/// <param name="schedule">The schedule.</param>
			/// <returns></returns>
      Schedule InsertSchedule(Schedule schedule);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets the schedule by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
      Schedule GetScheduleById(int id);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Updates the schedule.
			/// </summary>
			/// <param name="schedule">The schedule.</param>
      void UpdateSchedule(Schedule schedule);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Deletes the schedule.
			/// </summary>
			/// <param name="schedule">The schedule.</param>
      void DeleteSchedule(Schedule schedule);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Saves the type of the schedule on.
			/// </summary>
			/// <param name="type">The type.</param>
			void SaveScheduleOnType(Schedule schedule, string type);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Updates the type of the schedule on.
			/// </summary>
			/// <param name="schedule">The schedule.</param>
			List<Schedule> GetScheduleByScheduleId(string uniqueScheduleId);
		}
}
