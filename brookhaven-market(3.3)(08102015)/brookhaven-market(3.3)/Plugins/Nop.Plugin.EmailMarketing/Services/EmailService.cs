﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Events;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Services.Events;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.EmailMarketing.Services
{
    public class EmailService : IEmailService
    {
        #region fields

        private readonly IRepository<Email> _emailRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly EmailObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;
        private readonly IRepository<Customer> _customerRepository;
        #endregion

        #region ctor

        public EmailService(IRepository<Email> emailRepository, IEventPublisher eventPublisher, IRepository<Customer> customerRepository,
                                                            EmailObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
            this._emailRepository = emailRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
            _customerRepository = customerRepository;
        }

        #endregion

        #region Implementation of IEmailTemplateService

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all smart group.
				/// </summary>
				/// <param name="pageIndex">Index of the page.</param>
				/// <param name="pageSize">Size of the page.</param>
				/// <returns></returns>
        public virtual IPagedList<Email> GetAllEmail(int pageIndex, int pageSize)
        {
            var query = (from u in _emailRepository.Table
                         orderby u.EmailAddress
                         select u);
            var emails = new PagedList<Email>(query, pageIndex, pageSize);
            return emails;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all email .
				/// </summary>
				/// <returns></returns>
				public virtual List<Email> GetAllEmail()
				{
					var emails = (from u in _emailRepository.Table
											 orderby u.EmailAddress
											 select u);
					return emails.ToList();
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the smart group.
				/// </summary>
				/// <param name="email"></param>
				/// <returns></returns>
        public virtual Email InsertEmail(Email email)
        {
            if (email == null)
                throw new ArgumentNullException("email");

            _emailRepository.Insert(email);
						//event notification
            _eventPublisher.EntityInserted(email);

						return email;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the smart group by id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
        public Email GetEmailById(int id)
        {
            var db = _emailRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gers the sent emails by campaign id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				public int GerSentEmailsByCampaignId(int id)
				{
					var emails = (from u in _emailRepository.Table
												where u.CampaignId == id && u.SentStatus.Equals("sent")
												select u).Count();
					return emails;
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all email by campaign id.
				/// </summary>
				/// <param name="pageIndex">Index of the page.</param>
				/// <param name="pageSize">Size of the page.</param>
				/// <param name="id">The id.</param>
				/// <param name="status"></param>
				/// <returns></returns>
				public IPagedList<Email> GetAllEmailByCampaignId(int pageIndex, int pageSize, int id, string status)
				{
					IQueryable<Email> query;

					switch(status)
					{
						case "all":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id)
											 orderby e.EmailAddress
											 select e);
							break;

						case "delivered":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.SentStatus == "sent"
											 orderby e.EmailAddress
											 select e);
							break;
						
						case "bounced":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.SentStatus != "sent"
											 orderby e.EmailAddress
											 select e);
							break;

						case "opened":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.OpenStatus == "opened"
											 orderby e.EmailAddress
											 select e);
							break;
						
						default:
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id)
											 orderby e.EmailAddress
											 select e);
							break;
					}

					var emails = new PagedList<Email>(query, pageIndex, pageSize);
					return emails;
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all email by campaign id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <param name="status">The status.</param>
				/// <returns></returns>
				public List<Email> GetAllEmailByCampaignId(int id, string status)
				{
					IQueryable<Email> query;

					switch(status)
					{
						case "all":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id)
											 orderby e.EmailAddress
											 select e);
							break;

						case "delivered":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.SentStatus == "sent"
											 orderby e.EmailAddress
											 select e);
							break;

						case "bounced":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.SentStatus != "sent"
											 orderby e.EmailAddress
											 select e);
							break;

						case "opened":
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id) && e.OpenStatus == "opened"
											 orderby e.EmailAddress
											 select e);
							break;

						default:
							query = (from e in _emailRepository.Table
											 where e.CampaignId.Equals(id)
											 orderby e.EmailAddress
											 select e);
							break;
					}

					var emails = query.ToList();
					return emails;
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Counts the emails by campaign id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				public int CountEmailsByCampaignId(int id)
				{
					return
						(from e in _emailRepository.Table
						 where e.CampaignId.Equals(id)
						 select e).Count();
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Counts the opened emails by campaign id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				public int CountOpenedEmailsByCampaignId(int id)
				{
					return
						(from e in _emailRepository.Table
						 where e.CampaignId.Equals(id) && e.OpenStatus == "opened"
						 select e).Count();
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the smart group.
				/// </summary>
				/// <param name="email"></param>
        public void UpdateEmail(Email email)
        {
            if (email == null)
                throw new ArgumentNullException("email");

            _emailRepository.Update(email);
            //event notification
            _eventPublisher.EntityUpdated(email);

        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Deletes the smart group.
				/// </summary>
				/// <param name="email"></param>
        public void DeleteEmail(Email email)
        {
            _emailRepository.Delete(email);
            _eventPublisher.EntityDeleted(email);
        }

        

        #endregion


        public IEnumerable<string> SearchCustomerByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;

            var customers = (from c in _customerRepository.Table
                             orderby c.Id
                             where c.Email.Contains(email)
                             select c.Email).Distinct();
            return customers;
        }


        public List<Email> GetEmailByEmailAddress(string EmailAddress)
        {
            var db = _emailRepository;
            return _emailRepository.Table.Where(x => x.EmailAddress == EmailAddress).ToList();
        }
    }
}
