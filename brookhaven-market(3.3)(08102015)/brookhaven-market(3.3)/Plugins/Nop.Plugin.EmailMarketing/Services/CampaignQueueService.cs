﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Events;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Services.Messages;
using Nop.Services.Customers;
using Nop.Services.Events;
using System.Net.Mail;
using SendGrid;
using System.Net.Mime;
using System.Threading.Tasks;
using Nop.Core.Domain.Messages;
using System.Net;
using SendGrid.SmtpApi;

namespace Nop.Plugin.EmailMarketing.Services
{
    public class CampaignQueueService : ICampaignQueueService
    {
        #region fields

        private readonly IRepository<CampaignQueue> _campaignQueueRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly CampaignQueueObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly ICustomerService _customerService;
        private readonly ITokenizer _tokenizer;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;


        #endregion

        #region ctor

        public CampaignQueueService(IRepository<CampaignQueue> campaignQueueRepository, IQueuedEmailService queuedEmailService, IEventPublisher eventPublisher, IMessageTokenProvider messageTokenProvider,
            IEmailAccountService emailAccountService, ICustomerService customerService,
            EmailAccountSettings emailAccountSettings,
            ITokenizer tokenizer, CampaignQueueObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
            this._campaignQueueRepository = campaignQueueRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
            this._messageTokenProvider = messageTokenProvider;
            this._tokenizer = tokenizer;
            this._customerService = customerService;
            _queuedEmailService = queuedEmailService;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;

        }

        #endregion

        #region Implementation of IEmailCampaignService

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets all smart group.
        /// </summary>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        public virtual IPagedList<CampaignQueue> GetAllQueuedCampaigns(int pageIndex, int pageSize)
        {
            var query = (from u in _campaignQueueRepository.Table
                         orderby u.BroadCastTime descending
                         select u);
            var campaigns = new PagedList<CampaignQueue>(query, pageIndex, pageSize);
            return campaigns;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets all email .
        /// </summary>
        /// <returns></returns>
        public virtual List<CampaignQueue> GetAllQueuedCampaignsList()
        {
            var emails = (from u in _campaignQueueRepository.Table
                          orderby u.BroadCastTime
                          select u);
            return emails.ToList();
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Inserts the smart group.
        /// </summary>
        /// <param name="campaign"></param>
        /// <returns></returns>
        public virtual CampaignQueue InsertCampaignQueue(CampaignQueue campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            _campaignQueueRepository.Insert(campaign);
            //event notification
            _eventPublisher.EntityInserted(campaign);

            return campaign;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the smart group by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public CampaignQueue GetQueuedCampaignById(int id)
        {
            var db = _campaignQueueRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Updates the smart group.
        /// </summary>
        /// <param name="campaign"></param>
        public void UpdateCampaignQueue(CampaignQueue campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            _campaignQueueRepository.Update(campaign);
            //event notification
            _eventPublisher.EntityUpdated(campaign);

        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Deletes the smart group.
        /// </summary>
        /// <param name="campaign"></param>
        public void DeleteEmail(CampaignQueue campaign)
        {
            _campaignQueueRepository.Delete(campaign);
            _eventPublisher.EntityDeleted(campaign);
        }


        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends the test email.
        /// </summary>
        /// <param name="destinationMail">The email.</param>
        /// <param name="campaign">The campaign.</param>
        public async Task SendEmailCampaign(EmailModel destinationMail, EmailCampaignModel campaign)
        {
            try
            {
                SendGridMessage mailMsg = new SendGridMessage();

                if (!string.IsNullOrEmpty(campaign.To))
                {
                    string[] toEmail = campaign.To.Split(',');
                    mailMsg.AddTo(toEmail);
                }

                String recipient = destinationMail.EmailAddress;

                if (!string.IsNullOrEmpty(destinationMail.EmailAddress))
                {
                    mailMsg.AddTo(destinationMail.EmailAddress);
                }

                
                if (!string.IsNullOrEmpty(campaign.EmailAttachment))
                {

                    string[] commaSeparatedAttachment = campaign.EmailAttachment.Split(',');
                    for (int i = 0; i < commaSeparatedAttachment.Length; i++)
                    {
                        mailMsg.AddAttachment(commaSeparatedAttachment[i]);
                    }
                }

                if (!string.IsNullOrEmpty(campaign.Cc))
                {
                    mailMsg.AddCc(campaign.Cc);
                }
                if (!string.IsNullOrEmpty(campaign.Bcc))
                {
                    mailMsg.AddBcc(campaign.Bcc);
                }

           

                // From
                mailMsg.From = new MailAddress("services@procomarketing.com");
                String body = this.BodyReplaceToken(destinationMail.EmailAddress, campaign);

                mailMsg.Subject = campaign.Subject;
                mailMsg.Html = body;


                var header = new Header();
                var headers = new Dictionary<string, string> {
                        {"From","" + mailMsg.From + "\n"},
                        {"To","" + recipient + "\n"},
                        {"Subject","" + campaign.Subject + "\n"}
                        };
                header.AddUniqueArgs(headers);
                var xmstpapiJson = header.JsonString();
                mailMsg.Headers.Add("X-SMTPAPI", xmstpapiJson);

                var transportWeb = new SendGrid.Web(new System.Net.NetworkCredential("proco-test", "pr0c0P@ss"));


                await transportWeb.DeliverAsync(mailMsg);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Generates the message.
        /// </summary>
        /// <param name="destinationMail">The destination mail.</param>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        //private MailMessage GenerateMessage(EmailModel destinationMail, EmailCampaignModel campaign)
        //{
        //    String mailfrom = "services@procomarketing.com";
        //    String recipient = destinationMail.EmailAddress;

        //    Message msg = new Message(mailfrom);
        //    msg.AddDateHeader();

        //    String headers =
        //            "From: Email Campaign <" + mailfrom + ">\n" +
        //            "To: Our Customer <" + recipient + ">\n" +
        //            "Subject: " + campaign.Subject + "\n" +
        //            "Content-Type: text/html; charset=\"utf-8\"\n" +
        //            "\n"; // separate headers from body by an empty line
        //    msg.AddData(headers);

        //    String body = this.BodyReplaceToken(destinationMail.EmailAddress, campaign);
        //    msg.AddData(body);

        //    msg.JobID = destinationMail.Id.ToString();
        //    msg.EnvID = destinationMail.CampaignId.ToString();

        //    Recipient rcpt = new Recipient(recipient);
        //    msg.AddRecipient(rcpt);

        //    return msg;
        //}

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Bodies the replace token.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        private String BodyReplaceToken(string email, EmailCampaignModel campaign)
        {
            var tokens = new List<Token>();
            //May need to enable it later.

            //_messageTokenProvider.AddStoreTokens(tokens);

            var customer = _customerService.GetCustomerByEmail(email);
            if (customer != null)
                _messageTokenProvider.AddCustomerTokens(tokens, customer);

            string body = _tokenizer.Replace(campaign.Body, tokens, true);

            return body;
        }


        #endregion
    }
}
