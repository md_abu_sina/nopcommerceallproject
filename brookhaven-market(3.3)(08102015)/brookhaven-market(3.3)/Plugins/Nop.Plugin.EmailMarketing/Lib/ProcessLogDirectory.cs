﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using Nop.Plugin.EmailMarketing.Models;

namespace Nop.Plugin.EmailMarketing.Lib
{
	class ProcessLogDirectory
	{
		#region Fields

		private string _directoryPath { get; set; }
		private string _archiveDirectoryPath { get; set; }
		private string _archiveBlankDirectoryPath { get; set; }

		#endregion

		public ProcessLogDirectory(string directoryPath)
		{
			this._directoryPath = System.IO.Path.Combine(directoryPath, "bounce-acct");
			this._archiveDirectoryPath = System.IO.Path.Combine(directoryPath, "archive-bounce-log");
			this._archiveBlankDirectoryPath = System.IO.Path.Combine(directoryPath, "archive-blank-bounce-log");
		}

		public List<UpdateReportModel> GetUpdatedReport()
		{
			string logDir = _directoryPath;
			string[] fileNames = Directory.GetFiles(_directoryPath, "*.csv");

			var updatedReport = new List<UpdateReportModel>();
			for(int i = 0; i < fileNames.Length; i++)
			{
				string str = fileNames[i];

				FileInfo file = new FileInfo(str);
				
				string fileProcessDir = _directoryPath;
				string fileArchiveDir = _archiveDirectoryPath;

				//File.Move(file.FullName, fileProcessDir);
				

				try
				{
					DataTable csvData = this.ReadCsvFile(file.FullName);

					if(csvData.Rows.Count > 0)
					{
						foreach(DataRow dr in csvData.Rows)
						{
							var reportToBeUpdate = new UpdateReportModel();

							reportToBeUpdate.DsnStatus = dr["dsnStatus"].ToString();
							reportToBeUpdate.BounceCategory = dr["bounceCat"].ToString();
							reportToBeUpdate.EnvId = Convert.ToInt16(dr["envId"]);
							reportToBeUpdate.JobId = Convert.ToInt16(dr["jobId"]);
							reportToBeUpdate.UpdateTime = Convert.ToDateTime(dr["timeLogged"]);

							updatedReport.Add(reportToBeUpdate);
						}

						File.Move(file.FullName, System.IO.Path.Combine(_archiveDirectoryPath,file.Name));
					}
					else
					{
						File.Move(file.FullName, System.IO.Path.Combine(_archiveBlankDirectoryPath,file.Name));
					}
				}
				catch(Exception ex)
				{
					if(!Directory.Exists(logDir))
						Directory.CreateDirectory(logDir);

					StreamWriter sw = new StreamWriter(logDir + System.DateTime.Now.Ticks + ".log");
					sw.WriteLine("Error in file: " + file.Name);
					sw.WriteLine("Message: " + ex.Message);
					sw.WriteLine(ex.StackTrace);
					sw.Flush();
					sw.Close();
				}
			
			}
			/*if(fileNames.Length > 0)
			{
				Array.Sort(fileNames);
			}*/
			return updatedReport;
		}

		private DataTable ReadCsvFile(string file)
		{
			String[] csvData = File.ReadAllLines(file);
			DataTable csvDataTable = new DataTable();
			DataRow row;
			//string column = "";
			Dictionary<string, int> d = new Dictionary<string, int>();

			using(CsvReader csv = new CsvReader(new StreamReader(file), true))
			{
				int fieldCount = csvData[0].Split(',').Length;
				string[] headers = csvData[0].Replace("\"", "").Split(',');

				foreach(string str in headers)
				{
					csvDataTable.Columns.Add(str);
				}

				while(csv.ReadNextRecord())
				{
					string[] rowData = new string[fieldCount];
					for(int i = 0; i < fieldCount; i++)
					{
						rowData[i] = csv[i];
					}
					row = csvDataTable.NewRow();
					for(int i = 0; i < fieldCount; i++)
					{
						row[i] = rowData[i];
					}
					csvDataTable.Rows.Add(row);
				}
			}
			csvDataTable.AcceptChanges();
			return csvDataTable;
		}
	}
}
