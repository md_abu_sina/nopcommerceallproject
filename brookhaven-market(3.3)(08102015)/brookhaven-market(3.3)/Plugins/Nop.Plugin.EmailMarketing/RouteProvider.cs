﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.EmailMarketing
{
    public class RouteProvider : IRouteProvider
    {

        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.EmailMarketing.Main", "Plugin/EmailMarketing/Main",
                    new { controller = "EmailMarketing", 
                         action = "Main" 
                        },
                    new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");


            routes.MapRoute("Plugin.EmailMarketing.Campaign", "Plugin/EmailMarketing/Campaign",
                    new { controller = "EmailMarketing", action = "Campaign" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.EmailMarketing.Create", "Plugin/EmailMarketing/Create",
                            new { controller = "EmailMarketing", action = "Create" },
                                                        new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.EmailMarketing.Edit", "Plugin/EmailMarketing/Edit/{id}",
                    new { controller = "EmailMarketing", action = "Edit", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.EmailMarketing.To", "Plugin/EmailMarketing/To/{term}",
                    new { controller = "EmailMarketing", action = "To", term = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.EmailMarketing.GroupName", "Plugin/EmailMarketing/GroupName/{term}",
                    new { controller = "EmailMarketing", action = "GroupName", term = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");


            routes.MapRoute("Admin.Plugin.EmailMarketing.ScheduleList", "Admin/Plugin/EmailMarketing/ScheduleList",
                    new { controller = "EmailMarketing", action = "ScheduleList" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.CreateSchedule", "Plugin/EmailMarketing/CreateSchedule",
                    new { controller = "EmailMarketing", action = "CreateSchedule" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.EditSchedule", "Plugin/EmailMarketing/EditSchedule/{id}",
                    new { controller = "EmailMarketing", action = "EditSchedule", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.DeleteSchedule", "Plugin/EmailMarketing/DeleteSchedule",
                    new { controller = "EmailMarketing", action = "DeleteSchedule" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");


            routes.MapRoute("Admin.Plugin.EmailMarketing.CreateGroup", "Plugin/EmailMarketing/CreateGroup",
                    new { controller = "EmailMarketing", action = "CreateGroup" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.EditGroup", "Plugin/EmailMarketing/EditGroup/{id}",
                    new { controller = "EmailMarketing", action = "EditGroup", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.DeleteGroup", "Plugin/EmailMarketing/DeleteGroup",
                    new { controller = "EmailMarketing", action = "DeleteGroup" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.Group", "Plugin/EmailMarketing/Group",
                    new { controller = "EmailMarketing", action = "Group" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.SmartGroup", "Plugin/EmailMarketing/SmartGroup/{id}",
                    new { controller = "EmailMarketing", action = "SmartGroup", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.SmartGroupAjax", "Plugin/EmailMarketing/SmartGroupAjax",
                    new { controller = "EmailMarketing", action = "SmartGroupAjax" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");


            routes.MapRoute("Admin.Plugin.EmailMarketing.TemplateList", "Plugin/EmailMarketing/TemplateList",
                            new { controller = "EmailMarketing", action = "TemplateList" },
                            new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.CreateTemplate", "Plugin/EmailMarketing/CreateTemplate",
                    new { controller = "EmailMarketing", action = "CreateTemplate" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.EditTemplate", "Plugin/EmailMarketing/EditTemplate/{id}",
                    new { controller = "EmailMarketing", action = "EditTemplate", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.TemplateThumbnailAdd", "Plugin/EmailMarketing/TemplateThumbnailAdd",
                    new { controller = "EmailMarketing", action = "TemplateThumbnailAdd" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.ZipFileUpload", "Plugin/EmailMarketing/ZipFileUpload",
                    new { controller = "EmailMarketing", action = "ZipFileUpload" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.EmailAttachment", "Plugin/EmailMarketing/EmailAttachment",
                    new { controller = "EmailMarketing", action = "EmailAttachment" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.DeleteAttachment", "Plugin/EmailMarketing/DeleteAttachment/{savedFile}",
                    new { controller = "EmailMarketing", action = "DeleteAttachment", savedFile = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.ViewTemplates", "Plugin/EmailMarketing/ViewTemplates",
                    new { controller = "EmailMarketing", action = "ViewTemplates" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.GetTemplate", "Plugin/EmailMarketing/GetTemplate",
                    new { controller = "EmailMarketing", action = "GetTemplate" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.DeleteTemplate", "Plugin/EmailMarketing/DeleteTemplate",
                    new { controller = "EmailMarketing", action = "DeleteTemplate" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");


            routes.MapRoute("Admin.Plugin.EmailMarketing.Report", "Plugin/EmailMarketing/Report",
                    new { controller = "EmailMarketing", action = "Report" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.ReportExportExcelAll", "Plugin/EmailMarketing/ReportExportExcelAll",
                    new { controller = "EmailMarketing", action = "ReportExportExcelAll" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.ReportExportXmlAll", "Plugin/EmailMarketing/ReportExportXmlAll",
                    new { controller = "EmailMarketing", action = "ReportExportXmlAll" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.DetailReportExportExcelAll", "Plugin/EmailMarketing/DetailReportExportExcelAll",
                    new { controller = "EmailMarketing", action = "DetailReportExportExcelAll" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.DetailReportExportXmlAll", "Plugin/EmailMarketing/DetailReportExportXmlAll",
                    new { controller = "EmailMarketing", action = "DetailReportExportXmlAll" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.EmailOpen", "Plugin/EmailMarketing/EmailOpen",
                    new { controller = "EmailMarketing", action = "EmailOpen" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" });

            routes.MapRoute("Admin.Plugin.EmailMarketing.LinkClick", "Plugin/EmailMarketing/LinkClick",
                    new { controller = "EmailMarketing", action = "LinkClick" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.TestEmail", "Plugin/EmailMarketing/TestEmail",
                    new { controller = "EmailMarketing", action = "TestEmail" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");
            routes.MapRoute("Admin.Plugin.EmailMarketing.ShowTemplate", "Plugin/EmailMarketing/ShowTemplate/{id}",
                    new { controller = "EmailMarketing", action = "ShowTemplate", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.DetailReport", "Plugin/EmailMarketing/DetailReport/{id}",
                    new { controller = "EmailMarketing", action = "DetailReport", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.Link", "Plugin/EmailMarketing/Link/{id}",
                    new { controller = "EmailMarketing", action = "Link", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.LinkClickDetail", "Plugin/EmailMarketing/LinkClickDetail/{id}",
                    new { controller = "EmailMarketing", action = "LinkClickDetail", id = UrlParameter.Optional },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.EmailMarketing.UpdateReport", "Plugin/EmailMarketing/UpdateReport",
                    new { controller = "EmailMarketing", action = "UpdateReport" },
                                                new[] { "Nop.Plugin.EmailMarketing.Controllers" }).DataTokens.Add("area", "admin");

        }

        public int Priority
        {
            get { return int.MaxValue; }
        }

        #endregion
    }
}