﻿using FluentValidation;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Services.Localization;


namespace Nop.Plugin.EmailMarketing.Validators
{
	public class SmartGroupsValidator : AbstractValidator<CriteriaModel>
	{
		public SmartGroupsValidator(ILocalizationService localizationService, ISmartGroupsService smartGroupsService)
		{
			RuleFor(x => x.Name)
					.NotNull()
					.WithMessage(localizationService.GetResource("Admin.EmailMarketing.Groups.Fields.Name.Required"))
					.Must((x, name) => !smartGroupsService.GroupNameIsExist( name, x.Id ) )
					.WithMessage("This Group Name allready exist");
		}
	}
}