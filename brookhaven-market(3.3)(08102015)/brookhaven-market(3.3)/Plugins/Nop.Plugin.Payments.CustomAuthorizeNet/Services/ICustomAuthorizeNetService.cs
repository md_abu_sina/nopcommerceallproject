using System;
using CANet = Nop.Plugin.Payments.CustomAuthorizeNet.Domain;

namespace Nop.Plugin.Payments.CustomAuthorizeNet.Services
{
		public interface ICustomAuthorizeNetService
    {

				void InsertCustomAuthorizeNet(CANet.CustomAuthorizeNet customAuthorizeNet);

				CANet.CustomAuthorizeNet GetcustomAuthorizeNetById(int id);

				CANet.CustomAuthorizeNet GetcustomAuthorizeNetByStoreId(int storeId);

				CANet.CustomAuthorizeNet GetcustomAuthorizeNetFirst();

				void UpdateCustomAuthorizeNet(CANet.CustomAuthorizeNet customAuthorizeNet);

				void DeleteCateringEvent(CANet.CustomAuthorizeNet customAuthorizeNet);
    }
}