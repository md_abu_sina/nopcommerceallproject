﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using System.Linq;
using Nop.Plugin.Widgets.BsSlider.Infrastructure.Cache;
using Nop.Plugin.Widgets.BsSlider.Models;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Plugin.Widgets.BsSlider.Services;
using Nop.Plugin.Widgets.BsSlider.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.BsSlider.Controllers
{
    public class WidgetsBsSliderController : BasePluginController
    {
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly ICacheManager _cacheManager;
        
        private readonly IBsSliderService _sliderService;
        private readonly IBsSliderPictureService _sliderPictureService;

        public WidgetsBsSliderController(IWorkContext workContext,
            IStoreContext storeContext,
            IStoreService storeService, 
            IPictureService pictureService,
            ISettingService settingService,
            IBsSliderService sliderService,
            IBsSliderPictureService sliderPictureService,
            ICacheManager cacheManager)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._pictureService = pictureService;
            this._settingService = settingService;
            this._cacheManager = cacheManager;
            this._sliderService = sliderService;
            this._sliderPictureService = sliderPictureService;
        }

        protected string GetPictureUrl(int pictureId, int targetSize = 0)
        {
            string cacheKey = string.Format(ModelCacheEventConsumer.PICTURE_URL_MODEL_KEY, pictureId);
            return _cacheManager.Get(cacheKey, () =>
            {
                var url = _pictureService.GetPictureUrl(pictureId, targetSize, showDefaultPicture: false);
                //little hack here. nulls aren't cacheable so set it to ""
                if (url == null)
                    url = "";

                return url;
            });
        }

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var model = new ConfigurationModel();
           
            model.ActiveStoreScopeConfiguration = storeScope;
           
            return View("Nop.Plugin.Widgets.BsSlider.Views.WidgetsBsSlider.Configure", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            //load settings for a chosen store scope
            var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            

            
            
            return Configure();
        }

        [ChildActionOnly]
        public ActionResult PublicInfo(string widgetZone)
        {

            //var sliders = _sliderService.GetSlidersByWidget(widgetZone);

            //var model = new List<SliderModel>();

            var model = _sliderService.GetSlidersByWidget(widgetZone).Select(x =>
                                {
                                    var cr = x.ToModel();
                                    cr.BsSliderPictures = _sliderPictureService.GetAllPictures(x.Id).Select(p =>
                                                                                                                {
                                                                                                                    var pic = p.ToSliderPictureModel();
                                                                                                                    pic.PictureUrl = this.GetPictureUrl(pic.ImageId);
                                                                                                                    return pic;
                                                                                                                }
                                                                                                            ).ToList();
                                    return cr;
                                }).FirstOrDefault();
           
            //if (string.IsNullOrEmpty(model.Picture1Url) && string.IsNullOrEmpty(model.Picture2Url) &&
            //    string.IsNullOrEmpty(model.Picture3Url) && string.IsNullOrEmpty(model.Picture4Url) &&
            //    string.IsNullOrEmpty(model.Picture5Url))
            //    //no pictures uploaded
            //    return Content("");
            

            return View("PublicInfo", model);
        }

        #region Admin Section
        public ActionResult List()
        {
            return View("List");
        }

        [AdminAuthorize]
        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            var sliders = _sliderService.GetAllSliders(command.Page - 1, command.PageSize);
            
            var gridModel = new DataSourceResult
            {
                Data = sliders.Select(x =>
                                {
                                    var cr = x.ToModel();
                                    return cr;
                                }),
                Total = sliders.TotalCount
            };

            return new JsonResult
            {
                Data = gridModel
            };
        }

        [AdminAuthorize]
        public ActionResult Create()
        {
            var sliderModel = new SliderModel();

            return View("Create", sliderModel);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(SliderModel model, bool continueEditing)
        {

            if (ModelState.IsValid)
            {
                var slider = model.ToEntity();

                _sliderService.InsertSlider(slider);


                return continueEditing ? RedirectToAction("Edit", new
                {
                    id = slider.Id
                }) : RedirectToAction("List", "/Plugin/BsSlider/Slider/");
            }

            return View("Create", model);
        }

        [AdminAuthorize]
        public ActionResult Edit(int sliderId)
        {
            var slider = _sliderService.GetSliderById(sliderId);
            if (slider == null)
                return RedirectToAction("List", "/Plugin/BsSlider/Slider/");


            SliderModel model = slider.ToModel();

            return View("Edit", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(SliderModel model, bool continueEditing)
        {
            var slider = _sliderService.GetSliderById(model.Id);
            if (slider == null)
                return RedirectToAction("List", "/Plugin/BsSlider/Slider/");

            if (ModelState.IsValid)
            {
                slider = model.ToEntity(slider);

                _sliderService.UpdateSlider(slider);


                return continueEditing ? RedirectToAction("Edit", new
                {
                    id = slider.Id
                }) : RedirectToAction("List", "/Plugin/BsSlider/Slider/");
            }

            return View("Edit", model);
        }

        [AdminAuthorize]
        [HttpPost]
        public ActionResult PictureList(DataSourceRequest command, int sliderId)
        {
            var sliders = _sliderPictureService.GetAllSliderPictures(command.Page - 1, command.PageSize, sliderId);

            var gridModel = new DataSourceResult
            {
                Data = sliders.Select(x =>
                {
                    var cr = x.ToSliderPictureModel();
                    cr.PictureUrl = this.GetPictureUrl(x.PictureId, 150);
                    return cr;
                }),
                Total = sliders.TotalCount
            };

            return new JsonResult
            {
                Data = gridModel
            };
        }

        [AdminAuthorize]
        public ActionResult PictureCreate(int sliderId)
        {
            var sliderPictureModel = new SliderPictureModel();

            sliderPictureModel.SliderId = sliderId;

            return View("PictureCreate", sliderPictureModel);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult PictureCreate(SliderPictureModel model, bool continueEditing)
        {

            if (ModelState.IsValid)
            {
                var sliderPicture = model.ToSliderPictureEntity();

                sliderPicture.Slider = _sliderService.GetSliderById(model.SliderId);

                _sliderPictureService.InsertSliderPicture(sliderPicture);


                return continueEditing ? RedirectToAction("Edit", new
                {
                    id = sliderPicture.Id
                }) : RedirectToAction("Edit", "/Plugin/BsSlider/Slider/", new { sliderId = model.SliderId });
            }

            return View("PictureCreate", model);
        }

        [AdminAuthorize]
        public ActionResult PictureEdit(int sliderId, int pictureId)
        {
            var sliderPicture = _sliderPictureService.GetSliderPictureById(sliderId, pictureId);
            if (sliderPicture == null)
                RedirectToAction("Edit", "/Plugin/BsSlider/Slider/", new { sliderId = sliderId });


            SliderPictureModel model = sliderPicture.ToSliderPictureModel();
            model.SliderId = sliderId;
            return View("PictureEdit", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult PictureEdit(SliderPictureModel model, bool continueEditing)
        {
            var sliderPicture = _sliderPictureService.GetSliderPictureById(model.SliderId, model.Id);
            if (sliderPicture == null)
                RedirectToAction("Edit", "/Plugin/BsSlider/Slider/", new { sliderId = model.SliderId });

            if (ModelState.IsValid)
            {
                sliderPicture = model.ToSliderPictureEntity(sliderPicture);

                sliderPicture.Slider = _sliderService.GetSliderById(model.SliderId);

                _sliderPictureService.UpdateSliderPicture(sliderPicture);

                return continueEditing ? RedirectToAction("PictureEdit", new
                {
                    sliderId = sliderPicture.Slider.Id,
                    pictureId = sliderPicture.Id
                }) : RedirectToAction("Edit", "/Plugin/BsSlider/Slider/", new { sliderId = model.SliderId });
                
            }

            return View("PictureEdit", model);
        }

        //[AdminAuthorize]
        //public ActionResult EditEmail(int id)
        //{
        //    var cateringEmail = _cateringEmailService.GetCateringEmailById(id);
        //    if (cateringEmail == null)
        //        return RedirectToAction("EmailList", "/Plugin/Other/Catering/");


        //    CateringEmailModel cateringEmailModel = cateringEmail.ToModel();

        //    var emailType = from EmailAddressType d in Enum.GetValues(typeof(EmailAddressType))
        //                    select new
        //                    {
        //                        ID = (int)d,
        //                        Name = d.ToString(),
        //                        Selected = (int)d == cateringEmail.Id
        //                    };

        //    cateringEmailModel.StoreList = new SelectList(_customerService.GetCustomerRoleByStoreRole(), "ID", "SystemName", new { id = cateringEmailModel.StoreId });

        //    cateringEmailModel.EmailAddressTypes = new SelectList(emailType, "ID", "Name");


        //    return View("EmailAddressEdit", cateringEmailModel);
        //}

        //[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        //public ActionResult EditEmail(CateringEmailModel model, bool continueEditing)
        //{
        //    var cateringEmail = _cateringEmailService.GetCateringEmailById(model.Id);
        //    if (cateringEmail == null)
        //        return RedirectToAction("EmailList", "/Plugin/Other/Catering");

        //    if (ModelState.IsValid)
        //    {
        //        cateringEmail = model.ToEntity(cateringEmail);

        //        _cateringEmailService.UpdateCateringEmail(cateringEmail);


        //        return continueEditing ? RedirectToAction("EditEmail", new
        //        {
        //            id = cateringEmail.Id
        //        }) : RedirectToAction("EmailList", "/Plugin/Other/Catering/");
        //    }

        //    return View("EmailAddressEdit", model);
        //}

        //[AdminAuthorize]
        //public ActionResult DeleteEmailConfirmed(int id)
        //{
        //    var cateringEmail = _cateringEmailService.GetCateringEmailById(id);
        //    if (cateringEmail == null)
        //        return RedirectToAction("EmailList", "/Plugin/Other/Catering/");

        //    _cateringEmailService.DeleteCateringEmail(cateringEmail);

        //    return RedirectToAction("EmailList", "/Plugin/Other/Catering/");
        //}
        #endregion
    }
}