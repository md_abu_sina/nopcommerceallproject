USE [Brookhaven nopComm 3.3]
GO

INSERT INTO [dbo].[Product_SpecificationAttribute_Mapping]
           ([ProductId]
           ,[SpecificationAttributeOptionId]
           ,[CustomValue]
           ,[AllowFiltering]
           ,[ShowOnProductPage]
           ,[DisplayOrder])
     SELECT [ProductId]
      ,[SpecificationAttributeOptionId]
      ,[CustomValue]
      ,[AllowFiltering]
      ,[ShowOnProductPage]
      ,[DisplayOrder]
  FROM [Brookhaven nopComm 3.2].[dbo].[Product_SpecificationAttribute_Mapping]
GO


