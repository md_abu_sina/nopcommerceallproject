USE [Brookhaven nopComm 3.3]
GO

--SELECT Id FROM Product WHERE Id NOT IN (SELECT ProductVariantAttributeId FROM [Brookhaven nopComm 3.2].[dbo].[ProductVariantAttributeValue]);

--ALTER TABLE ProductVariantAttributeValue DROP CONSTRAINT ProductVariantAttributeId

INSERT INTO [dbo].[ProductVariantAttributeValue]
           ([ProductVariantAttributeId]
           ,[AttributeValueTypeId]
           ,[AssociatedProductId]
           ,[Name]
           ,[ColorSquaresRgb]
           ,[PriceAdjustment]
           ,[WeightAdjustment]
           ,[Cost]
           ,[Quantity]
           ,[IsPreSelected]
           ,[DisplayOrder]
           ,[PictureId])
     SELECT [ProductVariantAttributeId]
      ,[AttributeValueTypeId]
      ,[AssociatedProductId]
      ,[Name]
      ,[ColorSquaresRgb]
      ,[PriceAdjustment]
      ,[WeightAdjustment]
      ,[Cost]
      ,[Quantity]
      ,[IsPreSelected]
      ,[DisplayOrder]
      ,[PictureId]
  FROM [Brookhaven nopComm 3.2].[dbo].[ProductVariantAttributeValue]
GO


