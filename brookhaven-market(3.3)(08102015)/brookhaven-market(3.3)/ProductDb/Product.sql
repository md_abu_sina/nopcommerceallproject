USE [Brookhaven nopComm 3.3]
GO

INSERT INTO [dbo].[Product]
           ([ProductTypeId]
           ,[ParentGroupedProductId]
           ,[VisibleIndividually]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AdminComment]
           ,[ProductTemplateId]
           ,[VendorId]
           ,[ShowOnHomePage]
           ,[MetaKeywords]
           ,[MetaDescription]
           ,[MetaTitle]
           ,[AllowCustomerReviews]
           ,[ApprovedRatingSum]
           ,[NotApprovedRatingSum]
           ,[ApprovedTotalReviews]
           ,[NotApprovedTotalReviews]
           ,[SubjectToAcl]
           ,[LimitedToStores]
           ,[Sku]
           ,[ManufacturerPartNumber]
           ,[Gtin]
           ,[IsGiftCard]
           ,[GiftCardTypeId]
           ,[RequireOtherProducts]
           ,[RequiredProductIds]
           ,[AutomaticallyAddRequiredProducts]
           ,[IsDownload]
           ,[DownloadId]
           ,[UnlimitedDownloads]
           ,[MaxNumberOfDownloads]
           ,[DownloadExpirationDays]
           ,[DownloadActivationTypeId]
           ,[HasSampleDownload]
           ,[SampleDownloadId]
           ,[HasUserAgreement]
           ,[UserAgreementText]
           ,[IsRecurring]
           ,[RecurringCycleLength]
           ,[RecurringCyclePeriodId]
           ,[RecurringTotalCycles]
           ,[IsShipEnabled]
           ,[IsFreeShipping]
           ,[AdditionalShippingCharge]
           ,[DeliveryDateId]
           ,[WarehouseId]
           ,[IsTaxExempt]
           ,[TaxCategoryId]
           ,[ManageInventoryMethodId]
           ,[StockQuantity]
           ,[DisplayStockAvailability]
           ,[DisplayStockQuantity]
           ,[MinStockQuantity]
           ,[LowStockActivityId]
           ,[NotifyAdminForQuantityBelow]
           ,[BackorderModeId]
           ,[AllowBackInStockSubscriptions]
           ,[OrderMinimumQuantity]
           ,[OrderMaximumQuantity]
           ,[AllowedQuantities]
           ,[AllowAddingOnlyExistingAttributeCombinations]
           ,[DisableBuyButton]
           ,[DisableWishlistButton]
           ,[AvailableForPreOrder]
           ,[PreOrderAvailabilityStartDateTimeUtc]
           ,[CallForPrice]
           ,[Price]
           ,[OldPrice]
           ,[ProductCost]
           ,[SpecialPrice]
           ,[SpecialPriceStartDateTimeUtc]
           ,[SpecialPriceEndDateTimeUtc]
           ,[CustomerEntersPrice]
           ,[MinimumCustomerEnteredPrice]
           ,[MaximumCustomerEnteredPrice]
           ,[HasTierPrices]
           ,[HasDiscountsApplied]
           ,[Weight]
           ,[Length]
           ,[Width]
           ,[Height]
           ,[AvailableStartDateTimeUtc]
           ,[AvailableEndDateTimeUtc]
           ,[DisplayOrder]
           ,[Published]
           ,[Deleted]
           ,[CreatedOnUtc]
           ,[UpdatedOnUtc])
     SELECT [ProductTypeId]
      ,[ParentGroupedProductId]
      ,[VisibleIndividually]
      ,[Name]
      ,[ShortDescription]
      ,[FullDescription]
      ,[AdminComment]
      ,[ProductTemplateId]
      ,[VendorId]
      ,[ShowOnHomePage]
      ,[MetaKeywords]
      ,[MetaDescription]
      ,[MetaTitle]
      ,[AllowCustomerReviews]
      ,[ApprovedRatingSum]
      ,[NotApprovedRatingSum]
      ,[ApprovedTotalReviews]
      ,[NotApprovedTotalReviews]
      ,[SubjectToAcl]
      ,[LimitedToStores]
      ,[Sku]
      ,[ManufacturerPartNumber]
      ,[Gtin]
      ,[IsGiftCard]
      ,[GiftCardTypeId]
      ,[RequireOtherProducts]
      ,[RequiredProductIds]
      ,[AutomaticallyAddRequiredProducts]
      ,[IsDownload]
      ,[DownloadId]
      ,[UnlimitedDownloads]
      ,[MaxNumberOfDownloads]
      ,[DownloadExpirationDays]
      ,[DownloadActivationTypeId]
      ,[HasSampleDownload]
      ,[SampleDownloadId]
      ,[HasUserAgreement]
      ,[UserAgreementText]
      ,[IsRecurring]
      ,[RecurringCycleLength]
      ,[RecurringCyclePeriodId]
      ,[RecurringTotalCycles]
      ,[IsShipEnabled]
      ,[IsFreeShipping]
      ,[AdditionalShippingCharge]
      ,[DeliveryDateId]
      ,[WarehouseId]
      ,[IsTaxExempt]
      ,[TaxCategoryId]
      ,[ManageInventoryMethodId]
      ,[StockQuantity]
      ,[DisplayStockAvailability]
      ,[DisplayStockQuantity]
      ,[MinStockQuantity]
      ,[LowStockActivityId]
      ,[NotifyAdminForQuantityBelow]
      ,[BackorderModeId]
      ,[AllowBackInStockSubscriptions]
      ,[OrderMinimumQuantity]
      ,[OrderMaximumQuantity]
      ,[AllowedQuantities]
	  ,1
      ,[DisableBuyButton]
      ,[DisableWishlistButton]
      ,[AvailableForPreOrder]
      ,[PreOrderAvailabilityStartDateTimeUtc]
      ,[CallForPrice]
      ,[Price]
      ,[OldPrice]
      ,[ProductCost]
      ,[SpecialPrice]
      ,[SpecialPriceStartDateTimeUtc]
      ,[SpecialPriceEndDateTimeUtc]
      ,[CustomerEntersPrice]
      ,[MinimumCustomerEnteredPrice]
      ,[MaximumCustomerEnteredPrice]
      ,[HasTierPrices]
      ,[HasDiscountsApplied]
      ,[Weight]
      ,[Length]
      ,[Width]
      ,[Height]
      ,[AvailableStartDateTimeUtc]
      ,[AvailableEndDateTimeUtc]
      ,[DisplayOrder]
      ,[Published]
      ,[Deleted]
      ,[CreatedOnUtc]
      ,[UpdatedOnUtc]
  FROM [Brookhaven nopComm 3.2].[dbo].[Product]
GO


