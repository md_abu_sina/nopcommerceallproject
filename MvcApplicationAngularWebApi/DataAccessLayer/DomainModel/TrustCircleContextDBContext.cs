﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DomainModel
{
    public class TrustCircleContextDBContext : DbContext
    {
        public TrustCircleContextDBContext()
            : base("name=TrustCircleContext")
        {
        }

        public virtual DbSet<CircleTitle> CircleTitles { get; set; }
        public virtual DbSet<FacebookUser> FacebookUser { get; set; }
        public virtual DbSet<FriendCircle> FriendCircle { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<ReplyMessage> ReplyMessage { get; set; }
        public virtual DbSet<Todo> Todo { get; set; }
        public virtual DbSet<UserReturnModel> UserReturnModel { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
