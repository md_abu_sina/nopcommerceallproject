﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DomainModel
{
    [Table("Todo")]
    public class Todo
    {
        [Key]
        public Int64 ToDoId  { get; set; }

        public Int64 MessageId { get; set; }
        [StringLength(350)]
        public string TodoText { get; set; }
        [StringLength(50)]
        public string FBUSenderId { get; set; }
        [StringLength(50)]
        public string FBUReceiverId { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
    }
}
