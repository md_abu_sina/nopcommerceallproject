﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DomainModel
{
    [Table("FriendCircle")]
    public class FriendCircle
    {
        [Key]
        public Int64 PositionId { get; set; }

        
        public string FBUserId { get; set; }   
        public string FBFriendId { get; set; }
        public string XCoordinate { get; set; }
        public string YCoordinate { get; set; }

        
        public int CircleId { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        //public virtual ICollection<Message> Message{ get; set; }
    }
}
