﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DomainModel
{
    [Table("ReplyMessage")]
    public class ReplyMessage
    {
        [Key]
        public Int64 ReplyId { get; set; }

        public Int64 MessageId { get; set; }
        public string ReplyText { get; set; }
        public string FBUSenderId { get; set; }
        public string FBUReceiverId { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
