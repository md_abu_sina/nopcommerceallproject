﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public interface ICircleTitleService
    {
        List<CircleTitle> GetCircleTitles();

        bool InsertCircleTitle(CircleTitle CircleTitle);
        bool UpdateCircleTitle(CircleTitle CircleTitle);
        bool DeleteCircleTitle(CircleTitle CircleTitle);
    }
}
