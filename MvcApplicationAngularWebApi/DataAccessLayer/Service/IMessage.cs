﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public interface IMessage
    {
        List<Message> GetMessages();

        bool InsertMessage(Message Message);
        bool UpdateMessage(Message Message);
        bool DeleteMessage(Message Message);
    }
}
