﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public interface ITodo
    {
        List<Todo> GetTodo();

        bool InsertTodo(Todo Todo);
        bool UpdateTodo(Todo Todo);
        bool DeleteTodo(Todo Todo);
    }
}
