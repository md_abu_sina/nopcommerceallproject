﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public class CircleTitleService : ICircleTitleService
    {
        TrustCircleContextDBContext objOfTrustCircleContextDBContext = new TrustCircleContextDBContext();

        public List<CircleTitle> GetCircleTitles()
        {
            var circletitles = objOfTrustCircleContextDBContext.CircleTitles.ToList();

            return circletitles;
        }
        public bool InsertCircleTitle(CircleTitle CircleTitle)
        {
            objOfTrustCircleContextDBContext.CircleTitles.Add(CircleTitle);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool UpdateCircleTitle(CircleTitle CircleTitle)
        {
            var circleTitle = objOfTrustCircleContextDBContext.CircleTitles.FirstOrDefault(x => x.Id == CircleTitle.Id);
            circleTitle.CircleName = CircleTitle.CircleName;
            circleTitle.Id = CircleTitle.Id;
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }
        public bool DeleteCircleTitle(CircleTitle CircleTitle)
        {
            var circleTitle = objOfTrustCircleContextDBContext.CircleTitles.FirstOrDefault(x => x.Id == CircleTitle.Id);
            objOfTrustCircleContextDBContext.CircleTitles.Remove(circleTitle);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

    }
}
