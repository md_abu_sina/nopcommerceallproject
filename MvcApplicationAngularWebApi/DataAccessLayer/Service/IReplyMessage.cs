﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public interface IReplyMessage
    {
        List<ReplyMessage> GetReplyMessage();

        bool InsertReplyMessage(ReplyMessage ReplyMessage);
        bool UpdateReplyMessage(ReplyMessage ReplyMessage);
        bool DeleteReplyMessage(ReplyMessage ReplyMessage);
    }
}
