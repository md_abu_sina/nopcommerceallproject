﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public class ReplyMessageService : IReplyMessage
    {
        TrustCircleContextDBContext objOfTrustCircleContextDBContext = new TrustCircleContextDBContext();

        public List<ReplyMessage> GetReplyMessage()
        {
            var replymessage = objOfTrustCircleContextDBContext.ReplyMessage.ToList();

            return replymessage;
        }

        public bool InsertReplyMessage(ReplyMessage ReplyMessage)
        {
            ReplyMessage.CreatedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.ReplyMessage.Add(ReplyMessage);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool UpdateReplyMessage(ReplyMessage ReplyMessage)
        {
            var replymessage = objOfTrustCircleContextDBContext.ReplyMessage.FirstOrDefault(x => x.ReplyId == ReplyMessage.ReplyId);
            replymessage.MessageId = ReplyMessage.MessageId;
            replymessage.ReplyText = ReplyMessage.ReplyText;
            replymessage.FBUSenderId = ReplyMessage.FBUSenderId;
            replymessage.FBUReceiverId = ReplyMessage.FBUReceiverId;
            replymessage.CreatedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool DeleteReplyMessage(ReplyMessage ReplyMessage)
        {
            var replymessage = objOfTrustCircleContextDBContext.ReplyMessage.FirstOrDefault(x => x.ReplyId == ReplyMessage.ReplyId);
            objOfTrustCircleContextDBContext.ReplyMessage.Remove(replymessage);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }
    }
}
