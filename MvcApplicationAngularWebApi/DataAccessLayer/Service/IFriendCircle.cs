﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public interface IFriendCircle
    {
        List<FriendCircle> GetFriendCircles();

        bool InsertFriendCircle(FriendCircle FriendCircle);
        bool UpdateFriendCircle(FriendCircle FriendCircle);
        bool DeleteFriendCircle(FriendCircle FriendCircle);
    }
}
