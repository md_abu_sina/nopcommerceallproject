﻿using DataAccessLayer.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Service
{
    public class FacebookUserService:IFacebookUser
    {
        TrustCircleContextDBContext objOfTrustCircleContextDBContext = new TrustCircleContextDBContext();

        public List<FacebookUser> GetFacebookUsers()
        {
            var facebookUserService = objOfTrustCircleContextDBContext.FacebookUser.ToList();

            return facebookUserService;
        }

        public bool InsertFacebookUser(FacebookUser facebookUser)
        {
            facebookUser.CreatedOn = DateTime.Now;
            facebookUser.ModifiedOn = DateTime.Now;
            objOfTrustCircleContextDBContext.FacebookUser.Add(facebookUser);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool UpdateFacebookUser(FacebookUser FacebookUser)
        {
            var facebookUser = objOfTrustCircleContextDBContext.FacebookUser.FirstOrDefault(x => x.FBUserId == FacebookUser.FBUserId);
            facebookUser.FBUserId = FacebookUser.FBUserId;
            facebookUser.FBUserName = FacebookUser.FBUserName;
            facebookUser.FirstName = FacebookUser.FirstName;
            facebookUser.LastName = FacebookUser.LastName;
            facebookUser.ProfilePicture = FacebookUser.ProfilePicture;
            facebookUser.AccessToken = FacebookUser.AccessToken;
            facebookUser.CreatedOn = DateTime.Now;
            facebookUser.ModifiedOn = DateTime.Now;

            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }

        public bool DeleteFacebookUser(FacebookUser FacebookUser)
        {
            var facebookUser = objOfTrustCircleContextDBContext.FacebookUser.FirstOrDefault(x => x.FBUserId == FacebookUser.FBUserId);
            objOfTrustCircleContextDBContext.FacebookUser.Remove(facebookUser);
            objOfTrustCircleContextDBContext.SaveChanges();
            return true;
        }
    }
}
