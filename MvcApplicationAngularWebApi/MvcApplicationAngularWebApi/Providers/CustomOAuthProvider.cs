﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using MvcApplicationAngularWebApi.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using MvcApplicationAngularWebApi.DBBoject;


namespace MvcApplicationAngularWebApi.Providers
{

    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        private TrustCircleRepository _repo;
        private TrustCircleContext _ctx;
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {

            _repo = new TrustCircleRepository();
            context.Validated();
            return Task.FromResult<object>(null);
            _ctx = new TrustCircleContext();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            
            
            var allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();




            //IdentityUser userIdenTity = await _repo.FindUser(context.UserName, context.Password);


                ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);
            


            //ApplicationUser user = (ApplicationUser)userIdenTity;

            //using (TrustCircleRepository _repo = new TrustCircleRepository())
            //{
            //    IdentityUser userIdenTity = await _repo.FindUser(context.UserName, context.Password);


            //    if (user == null)
            //    {
            //        context.SetError("invalid_grant", "The user name or password is incorrect.");
            //        return;
            //    }
            //}

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
            if (!user.EmailConfirmed)
            {
                context.SetError("invalid_grant", "User did not confirm email.");
                return;
            }

            ClaimsIdentity oAuthIdentity =  await user.GenerateUserIdentityAsync(userManager, "JWT");

  
            
            var ticket = new AuthenticationTicket(oAuthIdentity, null);

            context.Validated(ticket);

        }
    }
}