(function () {
    filmmanageApp.controller('filmManageController', function ($scope, $http, $routeParams) {
        $scope.movies = [
        { title: 'The Matrix', rating: 7.5, category: 'Action' },
        { title: 'Focus', rating: 6.9, category: 'Comedy' },
        { title: 'The Lazarus Effect', rating: 6.4, category: 'Thriller' },
        { title: 'Everly', rating: 5.0, category: 'Action' },
        { title: 'Maps to the Stars', rating: 7.5, category: 'Drama' }
        ];

        //$scope.changepassword = function () {
        //    $scope.Model.UserId = $routeParams.userId;
        //    var chnagepasswordmodel = $scope.Model;
        //    $http({
        //        method: 'POST',
        //        url: '/api/accounts/changepassword',
        //        data: chnagepasswordmodel,
        //        headers: $scope.getHeaders()
        //    }).then(onChangePasswordComplete, onError);
        //};


        //var onChangePasswordComplete = function () {
        //    window.location.hash = "user";

        //};

        //var onError = function (reason) {
        //    $scope.Error = "Error happened and couldn't change password";
        //    $scope.errors = reason.data.modelState;
        //};




    });
})();

filmmanageApp.directive('fundooRating', function () {
      return {
          restrict: 'A',
          template: '<ul class="rating">' +
                      '<li ng-repeat="star in stars" class="filled">' +
                        '\u2605' +
                      '</li>' +
                    '</ul>',
          scope: {
              ratingValue: '='
          },
          link: function (scope, elem, attrs) {
              scope.stars = [];
              for (var i = 0; i < scope.ratingValue; i++) {
                  scope.stars.push({});
              }
          }
      }
  });