﻿using DataAccessLayer.DomainModel;
using DataAccessLayer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplicationAngularWebApi.Controllers
{
    public class MessageController : ApiController
    {
        private readonly IMessage _messageService;
        HttpResponseMessage response = new HttpResponseMessage();

        public MessageController()
        {
            _messageService = new MessageService();
        }
        public List<Message> Get()
        {
            var msgResult = new List<Message>();
            msgResult = _messageService.GetMessages();
            return msgResult;
        }


        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        public HttpResponseMessage Post(Message Message)
        {
            bool iSDataInserted = _messageService.InsertMessage(Message);
            if (iSDataInserted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                return response;
            }
        }

        public HttpResponseMessage Put(Message Message)
        {
            bool iSUpdated = _messageService.UpdateMessage(Message);
            if (iSUpdated)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }
        }

        public HttpResponseMessage Delete(Message Message)
        {
            bool iSDeleted = _messageService.DeleteMessage(Message);
            if (iSDeleted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }

    }
}
