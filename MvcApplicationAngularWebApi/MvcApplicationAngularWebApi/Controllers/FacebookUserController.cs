﻿using DataAccessLayer.DomainModel;
using DataAccessLayer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplicationAngularWebApi.Controllers
{
    public class FacebookUserController : ApiController
    {
        private readonly IFacebookUser _facebookUserService;
        HttpResponseMessage response = new HttpResponseMessage();

        public FacebookUserController()
        {
            _facebookUserService = new FacebookUserService();
        }

        public List<FacebookUser> Get()
        {
            var FacebookUser = new List<FacebookUser>();
            FacebookUser = _facebookUserService.GetFacebookUsers();
            return FacebookUser;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }


        public HttpResponseMessage Post(FacebookUser FacebookUser)
        {
            bool iSDataInserted = _facebookUserService.InsertFacebookUser(FacebookUser);
            if (iSDataInserted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                return response;
            }
        }


        public HttpResponseMessage Put(FacebookUser FacebookUser)
        {
            bool iSUpdated = _facebookUserService.UpdateFacebookUser(FacebookUser);
            if (iSUpdated)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }
        }

        public HttpResponseMessage Delete(FacebookUser FacebookUser)
        {
            bool iSDeleted = _facebookUserService.DeleteFacebookUser(FacebookUser);
            if (iSDeleted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
    }
}
