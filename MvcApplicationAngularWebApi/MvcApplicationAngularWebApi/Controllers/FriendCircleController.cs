﻿using DataAccessLayer.DomainModel;
using DataAccessLayer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplicationAngularWebApi.Controllers
{
    public class FriendCircleController : ApiController
    {
        private readonly IFriendCircle _friendCircleService;
        HttpResponseMessage response = new HttpResponseMessage();

        public FriendCircleController()
        {
            _friendCircleService = new FriendCircleService();
        }

        public List<FriendCircle> Get()
        {
            var FriendCircle = new List<FriendCircle>();
            FriendCircle = _friendCircleService.GetFriendCircles();
            return FriendCircle;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }


        public HttpResponseMessage Post(FriendCircle FriendCircle)
        {
            bool iSDataInserted = _friendCircleService.InsertFriendCircle(FriendCircle);
            if (iSDataInserted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                return response;
            }
        }


        public HttpResponseMessage Put(FriendCircle FriendCircle)
        {
            bool iSUpdated = _friendCircleService.UpdateFriendCircle(FriendCircle);
            if (iSUpdated)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }
        }

        public HttpResponseMessage Delete(FriendCircle FriendCircle)
        {
            bool iSDeleted = _friendCircleService.DeleteFriendCircle(FriendCircle);
            if (iSDeleted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
    }
}
