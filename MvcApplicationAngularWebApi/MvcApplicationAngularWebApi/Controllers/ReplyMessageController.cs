﻿using DataAccessLayer.DomainModel;
using DataAccessLayer.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplicationAngularWebApi.Controllers
{
    public class ReplyMessageController : ApiController
    {
         private readonly IReplyMessage _replyMessageService;
        HttpResponseMessage response = new HttpResponseMessage();

        public ReplyMessageController()
        {
            _replyMessageService = new ReplyMessageService();
        }

        public List<ReplyMessage> Get()
        {
            var replyMessage = new List<ReplyMessage>();
            replyMessage = _replyMessageService.GetReplyMessage();
            return replyMessage;
        }
        public HttpResponseMessage Post(ReplyMessage ReplyMessage)
        {
            bool iSDataInserted = _replyMessageService.InsertReplyMessage(ReplyMessage);
            if (iSDataInserted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable);
                return response;
            }
        }
        public HttpResponseMessage Put(ReplyMessage ReplyMessage)
        {
            bool iSUpdated = _replyMessageService.UpdateReplyMessage(ReplyMessage);
            if (iSUpdated)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }
        }
        public HttpResponseMessage Delete(ReplyMessage ReplyMessage)
        {
            bool iSDeleted = _replyMessageService.DeleteReplyMessage(ReplyMessage);
            if (iSDeleted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;
            }
        }
    }
}
