﻿using System.Collections.Generic;
using System.Net.Mail;
using Nop.Core.Domain.Messages;

namespace Nop.Services.Messages
{
    /// <summary>
    /// Email sender
    /// </summary>
    public partial interface IEmailSender
    {
        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="emailAccount">Email account to use</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="fromAddress">From address</param>
        /// <param name="fromName">From display name</param>
        /// <param name="toAddress">To address</param>
        /// <param name="toName">To display name</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses ist</param>
        /// <param name="attachmentFilePath">Attachment file path</param>
        /// <param name="attachmentFileName">Attachment file name. If specified, then this file name will be sent to a recipient. Otherwise, "AttachmentFilePath" name will be used.</param>
        void SendEmail(EmailAccount emailAccount, string subject, string body,
            string fromAddress, string fromName, string toAddress, string toName,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            string attachmentFilePath = null, string attachmentFileName = null);

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="emailAccount">Email account to use</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="from">From address</param>
        /// <param name="to">To address</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses ist</param>
        /// <param name="attachmentFilePath">Attachment file path</param>
        /// <param name="attachmentFileName">Attachment file name. If specified, then this file name will be sent to a recipient. Otherwise, "AttachmentFilePath" name will be used.</param>
        void SendEmail(EmailAccount emailAccount, string subject, string body,
            MailAddress from, MailAddress to,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            string attachmentFilePath = null, string attachmentFileName = null);


				#region Code By Razib Mahmud From BrainStation-23
				/// <summary>
				/// Sends the attached email.
				/// </summary>
				/// <param name="emailAccount">The email account.</param>
				/// <param name="subject">The subject.</param>
				/// <param name="body">The body.</param>
				/// <param name="fromAddress">From address.</param>
				/// <param name="fromName">From name.</param>
				/// <param name="toAddress">To address.</param>
				/// <param name="toName">To name.</param>
				/// <param name="bcc">The BCC.</param>
				/// <param name="cc">The cc.</param>
				void SendAttachedEmail(EmailAccount emailAccount, string subject, string body,
						string fromAddress, string fromName, string toAddress, string toName, string attachedFilePath,
						IEnumerable<string> bcc = null, IEnumerable<string> cc = null);
				#endregion
    }
}
