﻿using System;

namespace Nop.Core.Domain.Messages
{
    /// <summary>
    /// Represents NewsLetterSubscription entity
    /// </summary>
    public partial class NewsLetterSubscription : BaseEntity
    {       
        /// <summary>
        /// Gets or sets the newsletter subscription GUID
        /// </summary>
        public Guid NewsLetterSubscriptionGuid { get; set; }

        /// <summary>
        /// Gets or sets the subcriber email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether subscription is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the date and time when subscription was created
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

				#region Code by Iffat from Brainstation-23

				/// <summary>
        ///  Gets or sets a value indicating whether subscription is daily or not
        /// </summary>
        public virtual bool Daily { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether subscription is weekly or not
        /// </summary>
        public virtual bool Weekly { get; set; }

				#endregion
    }
}
