USE [Brookhaven nopComm 3.3]
GO

INSERT INTO [dbo].[SpecificationAttributeOption]
           ([SpecificationAttributeId]
           ,[Name]
           ,[DisplayOrder])
     SELECT [SpecificationAttributeId]
      ,[Name]
      ,[DisplayOrder]
  FROM [Brookhaven nopComm 3.2].[dbo].[SpecificationAttributeOption]
GO


