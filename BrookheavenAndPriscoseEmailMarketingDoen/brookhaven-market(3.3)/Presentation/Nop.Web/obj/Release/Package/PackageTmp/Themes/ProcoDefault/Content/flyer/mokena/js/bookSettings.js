flippingBook.pages = [
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page1.jpg",
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page2.jpg",
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page3.jpg",
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page4.jpg",
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page5.jpg",
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page6.jpg",
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page7.jpg",
	"/themes/ProcoDefault/Content/flyer/mokena/pages/page8.jpg"


];


flippingBook.contents = [
	[ "Specials", 1 ],
	[ "Meat/Seafood", 2 ],
	[ "Seafood", 3 ],
	[ "Dairy/Grocery/Frozen", 4 ],
	[ "Frozen", 5 ],
	[ "Wine, Spirits & Beer / Deli", 6 ],
	[ "Fresh Produce", 7 ],
	[ "Fresh Produce", 8 ]

	
];

// define custom book settings here
flippingBook.settings.bookWidth = 1000;
flippingBook.settings.bookHeight = 450;
flippingBook.settings.pageBackgroundColor = 0x006699;
flippingBook.settings.backgroundColor = 0xAABCD7;
flippingBook.settings.zoomUIColor = 0x919d6c;
flippingBook.settings.useCustomCursors = false;
flippingBook.settings.dropShadowEnabled = false;
flippingBook.settings.zoomImageWidth = 1024;
flippingBook.settings.zoomImageHeight = 922;
flippingBook.settings.downloadURL = "/themes/ProcoDefault/Content/flyer/mokena/pdf/flyer.pdf";
flippingBook.settings.flipSound = "";
flippingBook.settings.flipCornerStyle = "first page only";
flippingBook.settings.zoomHintEnabled = true;

// default settings can be found in the flippingbook.js file
flippingBook.create();
