﻿using Nop.Core;

namespace Nop.Plugin.Recipe.Domain
{
    public class ShoppinglistOwnItems : BaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int CustomerID { get; set; }
        public virtual string ItemTitle { get; set; }
    }
}
