﻿using System.Data.Entity.ModelConfiguration;
using System;
using Nop.Plugin.Recipe.Domain;

namespace Nop.Plugin.Recipe.Data
{
    public class SpecialShoppingListMap: EntityTypeConfiguration<SpecialShoppingList> {

        public SpecialShoppingListMap()
        {
            ToTable("Nop_SpecialShoppingList");

            //Map the primary key
            HasKey(m => m.Id);

            //Map the additional properties
            Property(m => m.CustomerId);
            Property(m => m.SpecialId);
           
            Property(m => m.RecipeId);

            //Avoiding truncation/failure so we set the same max length used in the product tame
            Property(m => m.RecipeTitle).HasMaxLength(400);
            Property(m => m.Brand).HasMaxLength(400);
            Property(m => m.Product).HasMaxLength(400);
            Property(m => m.Description).HasMaxLength(400);
            Property(m => m.Offer).HasMaxLength(400);
            Property(m => m.Image).HasMaxLength(400);

            Property(m => m.IsFeatured);
            Property(m => m.TimeStampUtc);
           
           
           
        }
    }
}
