﻿using System.Data.Entity.ModelConfiguration;
using System;
using Nop.Plugin.Recipe.Domain;

namespace Nop.Plugin.Recipe.Data
{
    public class RecipeCalenderMap : EntityTypeConfiguration<RecipeCalender>
    {

        public RecipeCalenderMap()
        {
            ToTable("Nop_RecipeCalender");

            //Map the primary key
            HasKey(m => m.Id);

            //Map the additional properties
            Property(m => m.Date);
            
            Property(m => m.RecipeID);
            Property(m => m.RecipeTitle);

        }
    }
}
