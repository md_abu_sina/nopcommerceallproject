﻿using System.Data.Entity.ModelConfiguration;
using System;
using Nop.Plugin.Recipe.Domain;

namespace Nop.Plugin.Recipe.Data
{
    public class ShoppinglistOwnitemsMap: EntityTypeConfiguration<ShoppinglistOwnItems> {

        public ShoppinglistOwnitemsMap()
        {
            ToTable("Nop_ShoppinglistOwnitems");

            //Map the primary key
            HasKey(m => m.Id);

            //Map the additional properties
            Property(m => m.CustomerID);
            Property(m => m.ItemTitle);
           
           
        }
    }
}
