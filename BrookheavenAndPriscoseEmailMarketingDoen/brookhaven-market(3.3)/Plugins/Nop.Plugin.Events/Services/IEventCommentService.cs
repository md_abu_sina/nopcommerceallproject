﻿using Nop.Plugin.Events.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Plugin.Events.Models;

namespace Nop.Plugin.Events.Services
{
    public interface IEventCommentService
    {
      
        /// <summary>
        /// Get All Event Comment
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IPagedList<EventComment> GetAllEventComment(int pageIndex, int pageSize);

				/// <summary>
				/// Numbers the of comments by event id.
				/// </summary>
				/// <param name="EventId">The event id.</param>
				/// <returns></returns>
				int NumberOfCommentsByEventId(int EventId);
        
        /// <summary>
        /// Get All Event Comment By EventId
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="EventId"></param>
        /// <returns></returns>
        IPagedList<EventComment> GetAllEventCommentByEventId(int pageIndex, int pageSize, int EventId);

        /// <summary>
        /// Get Event Comment By Event Comment Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>EventComment</returns>
        EventComment GetEventCommentByEventCommentId(int Id);

        /// <summary>
        /// Delete Event Comment
        /// </summary>
        /// <param name="eventcommentItem"></param>
        void DeleteEventComment(EventComment eventcommentItem);

         /// <summary>
        /// Insert Event Comment
        /// </summary>
        /// <param name="eventcommentItem"></param>
        void InsertEventComment(EventComment eventcommentItem);
    }
}
