﻿using Nop.Plugin.Events.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Plugin.Events.Models;

namespace Nop.Plugin.Events.Services
{
    public interface IEvent_CustomerRole_customMappingService
    {
        /// <summary>
        /// get All Customer Role Id By Event Id
        /// </summary>
        /// <param name="EventId"></param>
        /// <returns></returns>
        List<int> getAllCustomerRoleIdByEventId(int EventId);


         /// <summary>
        /// get All Event Id By Customer Role Id
        /// </summary>
        /// <param name="CustomerRoleId"></param>
        /// <returns></returns>
        List<int> getAllEventIdByCustomerRoleId(int CustomerRoleId);

        /// <summary>
        /// delete All entry By EventId
        /// </summary>
        /// <param name="EventId"></param>
        void deleteAllEntryByEventId(int EventId);

        /// <summary>
        /// Insert to Event Customer Role Custom Mapping table
        /// </summary>
        /// <param name="item"></param>
        void InsertEvent_CustomerRole_CustomMapping(Event_CustomerRole_CustomMapping item);
    }
}
