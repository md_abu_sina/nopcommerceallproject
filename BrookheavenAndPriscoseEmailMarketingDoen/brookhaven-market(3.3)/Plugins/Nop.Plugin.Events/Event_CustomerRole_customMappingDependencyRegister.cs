﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Events.Data;
using Nop.Plugin.Events.Domain;
using Nop.Plugin.Events.Services;

namespace Nop.Plugin.Events
{
    public class Event_CustomerRole_customMappingDependencyRegister: IDependencyRegistrar 
    {
        private const string CONTEXT_NAME = "nop_object_context_event_customerRole_customMapping";

        #region Implementation of IDependencyRegistrar

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //Load custom data settings
            var dataSettingsManager = new DataSettingsManager();
            DataSettings dataSettings = dataSettingsManager.LoadSettings();

            //Register custom object context
            builder.Register<IDbContext>(c => RegisterIDbContext(c, dataSettings)).Named<IDbContext>(CONTEXT_NAME).InstancePerHttpRequest();
            builder.Register(c => RegisterIDbContext(c, dataSettings)).InstancePerHttpRequest();

            //Register services
            builder.RegisterType<Event_CustomerRole_customMappingService>().As<IEvent_CustomerRole_customMappingService>();

            //Override the repository injection
            builder.RegisterType<EfRepository<Event_CustomerRole_CustomMapping>>().As<IRepository<Event_CustomerRole_CustomMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME)).InstancePerHttpRequest();
        }

        #endregion

        #region Implementation of IDependencyRegistrar

        public int Order
        {
            get { return 0; }
        }

        #endregion

        /// <summary>
        /// Registers the I db context.
        /// </summary>
        /// <param name="componentContext">The component context.</param>
        /// <param name="dataSettings">The data settings.</param>
        /// <returns></returns>
        private Event_customerRole_customMappingObjectContext RegisterIDbContext(IComponentContext componentContext, DataSettings dataSettings)
        {
            string dataConnectionStrings;

            if (dataSettings != null && dataSettings.IsValid())
            {
                dataConnectionStrings = dataSettings.DataConnectionString;
            }
            else
            {
                dataConnectionStrings = componentContext.Resolve<DataSettings>().DataConnectionString;
            }

            return new Event_customerRole_customMappingObjectContext(dataConnectionStrings);
        }
    }
}
