﻿using System;
using Nop.Core;

namespace Nop.Plugin.Events.Domain
{
    public class EventComment : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        /// <value>
        /// The Id.
        /// </value>
        public virtual int Id { get; set; }

        /// <summary>
        /// Gets or sets the CommentTitle.
        /// </summary>
        /// <value>
        /// The CommentTitle.
        /// </value>
        public virtual string CommentTitle { get; set; }

        /// <summary>
        /// Gets or sets the CommentText.
        /// </summary>
        /// <value>
        /// The CommentText.
        /// </value>
        public virtual string CommentText { get; set; }

        /// <summary>
        /// gets or sets the Customer Id
        /// </summary>
        public virtual int CustomerId { get; set; }

        /// <summary>
        /// gets or sets created time
        /// </summary>
        public virtual DateTime createdOn { get; set; }

        /// <summary>
        /// gets or sets IpAddress
        /// </summary>
        public virtual string IpAddress { get; set; }
       
        /// <summary>
        /// Gets or sets the EventId.
        /// </summary>
        /// <value>
        /// The EventId.
        /// </value>
        public virtual int EventId { get; set; }

        /// <summary>
        /// get or set Event Item
        /// </summary>
        public virtual Event eventItem { get; set; }

    }

}
