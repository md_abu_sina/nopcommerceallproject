﻿using System.Data.Entity.ModelConfiguration;
using System;
using Nop.Plugin.Events.Domain;

namespace Nop.Plugin.Events.Data
{
    public class EventCommentMap : EntityTypeConfiguration<EventComment>
    {

        public EventCommentMap()
        {
          //  ToTable("Nop_EventComment");

            //Map the primary key
            HasKey(ec => ec.Id);

            //Map the additional properties
            Property(ec => ec.CommentTitle);
            Property(ec => ec.CommentText);
           // Property(ec => ec.EventId);

            this.HasRequired(ec => ec.eventItem)
                .WithMany(e => e.EventComments)
                .HasForeignKey(ec => ec.EventId);
           
            /*
             this.HasRequired(nc => nc.NewsItem)
                .WithMany(n => n.NewsComments)
                .HasForeignKey(nc => nc.NewsItemId);
             */

        }
    }
}
