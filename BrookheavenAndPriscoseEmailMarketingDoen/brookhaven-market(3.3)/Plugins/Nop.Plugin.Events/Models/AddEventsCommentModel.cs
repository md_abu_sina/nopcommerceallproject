﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Events.Models
{
    public class AddEventsCommentModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Events.Comments.CommentTitle")]
        [AllowHtml]
        public string CommentTitle { get; set; }

        [NopResourceDisplayName("Events.Comments.CommentText")]
        [AllowHtml]
        public string CommentText { get; set; }
    }
}