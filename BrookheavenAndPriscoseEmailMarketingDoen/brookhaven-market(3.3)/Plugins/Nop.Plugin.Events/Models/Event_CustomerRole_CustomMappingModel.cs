﻿using System;
using System.Web.Mvc;
using Nop.Admin.Validators.News;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using FluentValidation.Attributes;
using Nop.Plugin.Events.Validators;
using System.Collections.Generic;
using Nop.Plugin.Events.Domain;

namespace Nop.Plugin.Events.Models
{
    
    public class Event_CustomerRole_CustomMappingModel : BaseNopEntityModel
    {
        public int Id { get; set; }
        
        public int EventId { get; set; }

        public int CustomerRoleId { get; set; }

    }
    
  
}
