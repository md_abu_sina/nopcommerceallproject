﻿

//using Nop.Services.PromotionFeed;

using Nop.Plugin.Events.Domain;
using Nop.Plugin.Events.Models;

namespace Nop.Plugin.Events
{
    public static class MappingExtensions
    {
       

        #region Events

        //Event items
        public static EventModel ToModel(this Event entity)
        {
            EventModel eventmodel = new EventModel();
            eventmodel.Id = entity.Id;
            eventmodel.Title = entity.Title;
            eventmodel.ShortDescription = entity.ShortDescription;
            eventmodel.FullDescription = entity.FullDescription;
            eventmodel.DayTime = entity.DayTime;
            eventmodel.Published = entity.Published;
            eventmodel.AllowComments = entity.AllowComments;
            //eventmodel.EventComments = entity.EventComments;
            
           
            return eventmodel;
        }

        public static Event ToEntity(this EventModel model)
        {
            Event evententity = new Event();
            evententity.Id = model.Id;
            evententity.Title = model.Title;
            evententity.ShortDescription = model.ShortDescription;
            evententity.FullDescription = model.FullDescription;
            evententity.DayTime = model.DayTime;
            evententity.Published = model.Published;
            evententity.AllowComments = model.AllowComments;

            return evententity;
        }

        public static Event ToEntity(this EventModel model, Event destination)
        {

					destination.Id = model.Id;
					destination.Title = model.Title;
					destination.ShortDescription = model.ShortDescription;
					destination.FullDescription = model.FullDescription;
					destination.DayTime = model.DayTime;
					destination.Published = model.Published;
					destination.AllowComments = model.AllowComments;

					return destination;
        }

        #endregion

       

       
    }
}