﻿using Nop.Core.Plugins;
using Nop.Web.Framework.Web;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using System.Collections.Generic;
using Nop.Core.Data;
//using TBarCode10Lib;

namespace Nop.Plugin.Employment
{
    public class EmploymentProvider : BasePlugin
    {


        public EmploymentProvider()
        {
           
          
        }

        /*public void BuildMenuItem(Telerik.Web.Mvc.UI.MenuItemBuilder menuItemBuilder)
        {
            //menuItemBuilder.Text("Events");
            //menuItemBuilder.Url("/Admin/Plugin/Events/Event/List");
            //menuItemBuilder.Route("Admin.Plugin.Events.Event.List");

            //menuItemBuilder.Text("Events Comments");
            //menuItemBuilder.Url("/Admin/Plugin/Events/Event/Comment");
            //menuItemBuilder.Route("Admin.Plugin.Events.Comments.List");

            menuItemBuilder.Text("Coupons").Items(y =>
            {
                y.Add()
                    .Text("Unique Coupons")
                    .Url("/Admin/Plugin/Upon/UniqueCoupon/List");
                y.Add()
                    .Text("uPons Report")
                    .Url("/Admin/Plugin/Upon/uPonDashboard/Index");
            });
        }*/

        public override void Install()
        {

            base.Install();


            //this.AddOrUpdatePluginLocaleResource("admin.contentmanagement.uniquecoupons", "Unique Coupons");
            


        }

        /*
         
         /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //database objects
            _objectContext.Uninstall();

            //locales
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Country");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Country.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.StateProvince");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.StateProvince.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Zip");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Zip.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.TaxCategory");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.TaxCategory.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Percentage");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.Fields.Percentage.Hint");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.AddRecord");
            this.DeletePluginLocaleResource("Plugins.Tax.CountryStateZip.AddRecord.Hint");
            
            base.Uninstall();
        }
         */

       
    }
}
