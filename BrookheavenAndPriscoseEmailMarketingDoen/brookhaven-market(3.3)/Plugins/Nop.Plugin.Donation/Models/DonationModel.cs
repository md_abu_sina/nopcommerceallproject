﻿using System;
using FluentValidation.Attributes;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using Nop.Plugin.Donation.Validators;

namespace Nop.Plugin.Donation.Models
{
    [Validator(typeof(DonationValidator))]
    public class DonationModel : BaseNopModel
    {
        public string StoreName { get; set; }
        public string StoreLocation { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime CurrentDate { get; set; }

        public string Organization { get; set; }
        public string Contact { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        [NopResourceDisplayName("Admin.Address.Fields.StateProvince")]
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string OrganizationDetail { get; set; }
        public string OrganizationAchievement { get; set; }
        public string EventDetail { get; set; }
        
        public string PreviousDonation { get; set; }
        public string RequestedItem { get; set; }
        public string Benifit { get; set; }
        public string TaxId { get; set; }

        [UIHint("File")]
        public string TaxLetter { get; set; }


        

    }

}
