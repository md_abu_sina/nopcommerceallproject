﻿using Nop.Core;
using System.Data;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.EmailMarketing;
using Nop.Core.Domain.Customers;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nop.Plugin.EmailMarketing.Domain
{
		[Table("Nop_EmailCampaign")]
		public class EmailCampaign : BaseEntity
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the entity identifier
			/// </summary>
			/// <value></value>
			public virtual int Id { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
      /// Gets or sets the name
      /// </summary>
      public virtual string Name { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
      /// Gets or sets the subject
      /// </summary>
      public virtual string Subject { get; set; }

			///--------------------------------------------------------------------------------------------
      /// <summary>
      /// Gets or sets the body
      /// </summary>
			[AllowHtml]
      public virtual string Body { get; set; }

			///--------------------------------------------------------------------------------------------
      /// <summary>
      /// Gets or sets the date and time of instance creation
      /// </summary>
      public virtual DateTime CreatedOnUtc { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets to.
			/// </summary>
			/// <value>To.</value>
			public virtual string To { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
      /// Gets or sets the body
      /// </summary>
      public virtual string Cc { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
      /// Gets or sets the body
      /// </summary>
      public virtual string Bcc { get; set; }

			///--------------------------------------------------------------------------------------------
      /// <summary>
      /// Gets or sets the date and time of instance creation
      /// </summary>
      public virtual string EmailAttachment { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the name of the group.
			/// </summary>
			/// <value>The name of the group.</value>
      public virtual string GroupName { get; set; }


    }

}
