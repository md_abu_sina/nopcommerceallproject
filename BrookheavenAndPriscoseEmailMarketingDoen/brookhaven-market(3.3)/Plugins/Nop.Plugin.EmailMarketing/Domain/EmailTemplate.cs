﻿using Nop.Core;
using System.Data;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.EmailMarketing;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.EmailMarketing.Domain
{
    public class EmailTemplate : BaseEntity
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the entity identifier
			/// </summary>
			/// <value></value>
      public virtual int Id { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the name.
			/// </summary>
			/// <value>The name.</value>
      public virtual string Name { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the picture id.
			/// </summary>
			/// <value>The picture id.</value>
			public virtual int PictureId {	get;	set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the template path.
			/// </summary>
			/// <value>The template path.</value>
      public virtual string TemplatePath { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the thumbnail path.
			/// </summary>
			/// <value>The thumbnail path.</value>
      public virtual string ThumbnailPath { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the template body.
			/// </summary>
			/// <value>The template body.</value>
      public virtual string TemplateBody { get; set; }

    }

}
