﻿using Nop.Core;
using System.Data;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Plugin.EmailMarketing;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.EmailMarketing.Domain
{
		//[Table("Nop_Link")]
    public class Link : BaseEntity
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// 
			/// </summary>
			private ICollection<Email> _emails;

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the entity identifier
			/// </summary>
			/// <value></value>
      public virtual int Id { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the campaign id.
			/// </summary>
			/// <value>The campaign id.</value>
      public virtual int CampaignId { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the link href.
			/// </summary>
			/// <value>The link href.</value>
      public virtual string LinkHref { get; set; }

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the click.
			/// </summary>
			/// <value>The click.</value>
      public virtual int Click { get; set; }
				
			//public ICollection<Email> Emails { get; set; }
			//public ICollection<Email> Emails;
			//public virtual Email Email { get; set; } 

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets or sets the emails.
			/// </summary>
			/// <value>The emails.</value>
			public virtual ICollection<Email> Emails
			{
				get
				{
					return _emails ?? (_emails = new List<Email>());
				}
				protected set
				{
					_emails = value;
				}
			}

				

    }

}
