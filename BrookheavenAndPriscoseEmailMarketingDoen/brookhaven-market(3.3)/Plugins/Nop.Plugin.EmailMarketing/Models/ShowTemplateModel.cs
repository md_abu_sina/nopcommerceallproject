﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.EmailMarketing.Models
{
    public class ShowTemplateModel : BaseNopEntityModel
  {
		public string Body { get; set; }
  }

}
