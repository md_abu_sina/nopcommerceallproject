﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Nop.Web.Framework;
using Nop.Plugin.EmailMarketing.Validators;

namespace Nop.Plugin.EmailMarketing.Models
{
	[Validator(typeof(SmartGroupsValidator))]

	public class CriteriaModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		[NopResourceDisplayName("Admin.EmailMarketing.Groups.Fields.Name")]
		public string Name { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the key word.
		/// </summary>
		/// <value>The key word.</value>
		public string KeyWord { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the columns.
		/// </summary>
		/// <value>The columns.</value>
		public string Columns { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the conditions.
		/// </summary>
		/// <value>The conditions.</value>
		public string Conditions { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the and or.
		/// </summary>
		/// <value>The and or.</value>
		public string AndOr { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the query.
		/// </summary>
		/// <value>The query.</value>
		public string Query { get; set; }

  }

}
