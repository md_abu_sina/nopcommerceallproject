﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class UpdateReportModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		/// <value>The job id.</value>
		public int JobId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the env id.
		/// </summary>
		/// <value>The env id.</value>
		public int EnvId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the bounce category.
		/// </summary>
		/// <value>The bounce category.</value>
		public string BounceCategory {	get;	set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the DSN status.
		/// </summary>
		/// <value>The DSN status.</value>
    public string DsnStatus {	get;	set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the update time.
		/// </summary>
		/// <value>The update time.</value>
		public DateTime UpdateTime {	get;	set; }

	}

}
