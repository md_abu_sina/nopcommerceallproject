﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Models
{
	public class LinkModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public int Id { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the campaign id.
		/// </summary>
		/// <value>The campaign id.</value>
		public int CampaignId { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the link href.
		/// </summary>
		/// <value>The link href.</value>
		public virtual string LinkHref { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the click.
		/// </summary>
		/// <value>The click.</value>
    public virtual int Click { get; set; }
		
  }

}
