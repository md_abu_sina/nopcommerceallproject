﻿using Nop.Core;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Nop.Web.Framework;
using Nop.Plugin.EmailMarketing.Validators;

namespace Nop.Plugin.EmailMarketing.Models
{
	
	public class CustomerInfoModel : BaseNopEntityModel
  {
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		/// <value>The first name.</value>
		public string FirstName { get; set; }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		/// <value>The last name.</value>
		public string LastName { get; set; }

  }

}
