﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Common;
using Nop.Core.Events;
using Nop.Plugin.EmailMarketing.Data;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Services.Events;
using Nop.Plugin.EmailMarketing.Data;

namespace Nop.Plugin.EmailMarketing.Services
{
    public class EmailTemplateService : IEmailTemplateService
    {
        #region fields

        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly EmailTemplateObjectContext _dbContext;
        private readonly CommonSettings _commonSettings;
        private readonly IDataProvider _dataProvider;

        #endregion

        #region ctor

        public EmailTemplateService(IRepository<EmailTemplate> emailTemplateRepository, IEventPublisher eventPublisher,
                                                            EmailTemplateObjectContext dbContext, CommonSettings commonSettings, IDataProvider dataProvider)
        {
            this._emailTemplateRepository = emailTemplateRepository;
            this._eventPublisher = eventPublisher;
            this._dbContext = dbContext;
            this._commonSettings = commonSettings;
            this._dataProvider = dataProvider;
        }

        #endregion

        #region Implementation of IEmailTemplateService

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all smart group.
				/// </summary>
				/// <param name="pageIndex">Index of the page.</param>
				/// <param name="pageSize">Size of the page.</param>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
        public virtual IPagedList<EmailTemplate> GetAllEmailTemplate(int pageIndex, int pageSize)
        {
            var query = (from u in _emailTemplateRepository.Table
                         orderby u.Name
                         select u);
            var emailTemplates = new PagedList<EmailTemplate>(query, pageIndex, pageSize);
            return emailTemplates;
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets all email template.
				/// </summary>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
				public virtual List<EmailTemplate> GetAllEmailTemplate()
				{
					var emailTemplates = (from u in _emailTemplateRepository.Table
											 orderby u.Name
											 select u);
					return emailTemplates.ToList();
				}

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Inserts the smart group.
				/// </summary>
				/// <param name="emailTemplate"></param>
				/// --------------------------------------------------------------------------------------------
        public virtual void InsertEmailTemplate(EmailTemplate emailTemplate)
        {
            if (emailTemplate == null)
                throw new ArgumentNullException("emailTemplate");

            _emailTemplateRepository.Insert(emailTemplate);

            //event notification
            _eventPublisher.EntityInserted(emailTemplate);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Gets the smart group by id.
				/// </summary>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				/// --------------------------------------------------------------------------------------------
        public EmailTemplate GetEmailTemplateById(int id)
        {
            var db = _emailTemplateRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Updates the smart group.
				/// </summary>
				/// <param name="emailTemplate"></param>
				/// --------------------------------------------------------------------------------------------
        public void UpdateEmailTemplate(EmailTemplate emailTemplate)
        {
            if (emailTemplate == null)
                throw new ArgumentNullException("emailTemplate");

            _emailTemplateRepository.Update(emailTemplate);
            //event notification
            _eventPublisher.EntityUpdated(emailTemplate);

        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Deletes the smart group.
				/// </summary>
				/// <param name="emailTemplate"></param>
				/// --------------------------------------------------------------------------------------------
        public void DeleteEmailTemplate(EmailTemplate emailTemplate)
        {
            _emailTemplateRepository.Delete(emailTemplate);
            _eventPublisher.EntityDeleted(emailTemplate);
        }

				///--------------------------------------------------------------------------------------------
				/// <summary>
				/// Templates the name is exist.
				/// </summary>
				/// <param name="name">The name.</param>
				/// <param name="id">The id.</param>
				/// <returns></returns>
				public bool TemplateNameIsExist(string name, int id = 0)
				{
					return
						_emailTemplateRepository.Table.Any(et => et.Name.Equals(name) && et.Id != id);
				}

        #endregion
    }
}
