﻿using Nop.Core;
using Nop.Plugin.EmailMarketing.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.EmailMarketing.Services
{
    public interface ILinkService
    {

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all link.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <returns></returns>
			IPagedList<Link> GetAllLink(int pageIndex, int pageSize, int id = 0);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all link.
			/// </summary>
			/// <param name="campaignId">The campaign id.</param>
			/// <returns></returns>
			List<Link> GetAllLink(int campaignId);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Inserts the link.
			/// </summary>
			/// <param name="link">The link.</param>
			/// <returns></returns>
			Link InsertLink(Link link);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Inserts the link click.
			/// </summary>
			/// <param name="linkId">The link id.</param>
			/// <param name="emailId">The email id.</param>
			void InsertLinkClick(int linkId = 0, int emailId = 0);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets the link by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			Link GetLinkById(int id);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets the email by link id.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <param name="linkId">The link id.</param>
			/// <returns></returns>
			IPagedList<Email> GetEmailByLinkId(int pageIndex, int pageSize, int linkId);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Updates the link.
			/// </summary>
			/// <param name="link">The link.</param>
			void UpdateLink(Link link);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Deletes the link.
			/// </summary>
			/// <param name="link">The link.</param>
			void DeleteLink(Link link);

       
    }
}
