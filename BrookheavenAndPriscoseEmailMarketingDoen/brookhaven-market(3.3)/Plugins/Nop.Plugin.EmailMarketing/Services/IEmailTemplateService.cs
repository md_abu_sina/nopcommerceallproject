﻿//using Nop.Plugin.Upon.Domain;
using System.Collections.Generic;
using System;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Plugin.EmailMarketing.Models;
//using Nop.Plugin.Upon.Models;

namespace Nop.Plugin.EmailMarketing.Services
{
    public interface IEmailTemplateService
    {
			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all smart group.
			/// </summary>
			/// <param name="pageIndex">Index of the page.</param>
			/// <param name="pageSize">Size of the page.</param>
			/// <returns></returns>
			IPagedList<EmailTemplate> GetAllEmailTemplate(int pageIndex, int pageSize);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets all email template.
			/// </summary>
			/// <returns></returns>
			List<EmailTemplate> GetAllEmailTemplate();

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Inserts the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void InsertEmailTemplate(EmailTemplate emailTemplate);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Gets the smart group by id.
			/// </summary>
			/// <param name="id">The id.</param>
			/// <returns></returns>
      EmailTemplate GetEmailTemplateById(int id);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Updates the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void UpdateEmailTemplate(EmailTemplate emailTemplate);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Deletes the smart group.
			/// </summary>
			/// <param name="smartGroup">The smart group.</param>
      void DeleteEmailTemplate(EmailTemplate emailTemplate);

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Templates the name is exist.
			/// </summary>
			/// <param name="name">The name.</param>
			/// <param name="id">The id.</param>
			/// <returns></returns>
			bool TemplateNameIsExist(string name, int id = 0);
       
    }
}
