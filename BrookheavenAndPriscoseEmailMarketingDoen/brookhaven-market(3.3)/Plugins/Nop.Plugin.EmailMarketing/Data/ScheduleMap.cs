﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.EmailMarketing.Domain;

namespace Nop.Plugin.EmailMarketing.Data
{
    public partial class ScheduleMap : EntityTypeConfiguration<Schedule>
    {
			public ScheduleMap()
        {
						this.ToTable("Nop_Schedule");

            //Map the primary key
            this.HasKey(e => e.Id);
						this.HasRequired(e => e.EmailCampaign).WithMany().HasForeignKey(ec => ec.CampaignId).WillCascadeOnDelete(true);
						this.Property(e => e.ScheduleId);
						this.Property(e => e.ScheduledTime);
						this.Property(e => e.Status);
						this.Property(e => e.Type);

        }
    }
}
