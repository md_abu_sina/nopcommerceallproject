﻿using Nop.Core.Plugins;
using Nop.Services.Logging;
using Nop.Services.Tasks;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Services.Messages;
using Nop.Core.Domain;
using Nop.Plugin.EmailMarketing.Lib;
using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Messages;

namespace Nop.Plugin.EmailMarketing
{
	
		public class SendScheduledMailTask : ITask
		{
			private readonly ILogger _logger;

			private readonly ICampaignQueueService _campaignQueueService;
            private readonly IMessageTemplateService _messageTemplateService;
			private readonly ISmartGroupsService _smartGroupsService;
            public readonly IStoreContext _storeContext;
			private readonly IEmailService _emailService;
            private readonly IEmailAccountService _emailAccountService;
            private readonly EmailAccountSettings _emailAccountSettings;
			private readonly ILinkService _linkService;

			private readonly ITokenizer _tokenizer;

			private readonly StoreInformationSettings _storeInformationSettings;

			private readonly IScheduleService _scheduleService;

			private readonly IEmailCampaignService _emailCampaignService;
            private readonly IWorkContext _workContext;
			private readonly IMessageTokenProvider _messageTokenProvider;

			public SendScheduledMailTask(ILogger logger, ICampaignQueueService campaignQueueService,
                IMessageTemplateService messageTemplateService, IStoreContext storeContext,
                IWorkContext workContext, 
                 IEmailAccountService emailAccountService, EmailAccountSettings emailAccountSettings,
                ISmartGroupsService smartGroupsService, IEmailService emailService, IScheduleService scheduleService,
                IMessageTokenProvider messageTokenProvider, IEmailCampaignService emailCampaignService, 
                ILinkService linkService, ITokenizer tokenizer, StoreInformationSettings storeInformationSettings)
			{
				this._logger = logger;
				this._campaignQueueService = campaignQueueService;
				this._smartGroupsService = smartGroupsService;
				this._emailService = emailService;
				this._linkService = linkService;
				this._tokenizer = tokenizer;
				this._storeInformationSettings = storeInformationSettings;
				this._scheduleService = scheduleService;
				this._emailCampaignService = emailCampaignService;
				this._messageTokenProvider = messageTokenProvider;
                _messageTemplateService = messageTemplateService;
                _storeContext = storeContext;
                _emailAccountService = emailAccountService;
                _emailAccountSettings = emailAccountSettings;
                _workContext = workContext;
			}

			///--------------------------------------------------------------------------------------------
			/// <summary>
			/// Execute task
			/// </summary>
			public void Execute()
			{
				//is plugin installed?_campaignQueueService, _smartGroupsService, _emailService, _linkService, _tokenizer, _storeInformationSettings.StoreUrl,_scheduleService
				var scheduledCampaigns = _scheduleService.GetAllScheduleByTime(DateTime.UtcNow);

				List<int> scheduleIdList = new List<int>();
					
				foreach(var scheduleCampaign in scheduledCampaigns)
				{
					
					var campaign = _emailCampaignService.GetEmailCampaignById(scheduleCampaign.CampaignId);

                    ProcessCampaignFacade facade = new ProcessCampaignFacade(_campaignQueueService, _messageTemplateService, _storeInformationSettings, _storeContext, _smartGroupsService, _emailService, _messageTokenProvider, _linkService, _emailAccountService, _workContext, _emailAccountSettings, _tokenizer, _scheduleService);

                    facade.ProcessCampaign(campaign.ToEmailCampaignModel(), _storeContext.CurrentStore.Url);

					scheduleIdList.Add(scheduleCampaign.Id);
					

				}
				
				_logger.Error("Send Email scheduled task executed");

				foreach(var scheduleId in scheduleIdList)
				{
					var sentScheduledCampaign = _scheduleService.GetScheduleById(scheduleId);
					sentScheduledCampaign.Status = true;
					_scheduleService.UpdateSchedule(sentScheduledCampaign);
				}
				
			}
		}
	
}
