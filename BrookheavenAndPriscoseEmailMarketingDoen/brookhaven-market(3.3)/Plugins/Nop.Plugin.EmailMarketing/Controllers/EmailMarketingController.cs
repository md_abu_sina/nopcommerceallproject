﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Controllers;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Plugin.EmailMarketing.Lib;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Nop.Services.Logging;
using Nop.Core;
using Nop.Services.ExportImport;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Web.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.Messages;
using System.Threading.Tasks;



namespace Nop.Plugin.EmailMarketing.Controllers
{
    public class EmailMarketingController : Controller
	{
		#region Fields

		private readonly IEmailCampaignService _emailCampaignService;
        private readonly IScheduleService _scheduleService;
        private readonly IEmailTemplateService _emailTemplateService;
		private readonly IPictureService _pictureService;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly IPermissionService _permissionService;
		private readonly IMessageTokenProvider _messageTokenProvider;
        //private readonly ICustomerService _customerService;
		private readonly AdminAreaSettings _adminAreaSettings;
        //private readonly IExportManager _exportManager;
        private readonly ISmartGroupsService _smartGroupsService;
        private readonly StoreInformationSettings _storeInformationSettings;
		private readonly IEmailService _emailService;
		private readonly ICampaignQueueService _campaignQueueService;
		private readonly ILinkService _linkService;
        private readonly ILinkClickService _linkClickService;
        private readonly ITokenizer _tokenizer;
		private readonly ILogger _logger;
        public readonly IStoreContext _storeContext;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IWorkContext _workContext;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
		#region email fields




		#endregion



		#endregion

		#region ctor

		public EmailMarketingController(IEmailCampaignService emailCampaignService, 
            IEmailTemplateService emailTemplateService,
            IWorkContext workContext, 
            IEmailService emailService, 
            AdminAreaSettings adminAreaSettings,
            ILinkService linkService, 
            ILinkClickService linkClickService, 
            ISmartGroupsService smartGroupsService, 
            IPictureService pictureService, 
            ILogger logger,
            IDateTimeHelper dateTimeHelper, 
            StoreInformationSettings storeInformationSettings, 
            ICampaignQueueService campaignQueueService,
            IScheduleService scheduleService, 
            IExportManager exportManager, 
            IPermissionService permissionService,
            IMessageTokenProvider messageTokenProvider, 
            //ICustomerService customerService, 
            ITokenizer tokenizer,
            IStoreContext storeContext,
            IMessageTemplateService messageTemplateService,
            IEmailAccountService emailAccountService,            
            EmailAccountSettings emailAccountSettings
            )
		{
			this._emailCampaignService = emailCampaignService;
            this._emailTemplateService = emailTemplateService;
			this._pictureService = pictureService;
			this._adminAreaSettings = adminAreaSettings;
            this._smartGroupsService = smartGroupsService;
			this._dateTimeHelper = dateTimeHelper;
			this._permissionService = permissionService;
            this._messageTokenProvider = messageTokenProvider;
            //this._customerService = customerService;
            this._storeInformationSettings = storeInformationSettings;
            this._scheduleService = scheduleService;
			this._emailService = emailService;
			this._campaignQueueService = campaignQueueService;
			this._linkService = linkService;
            this._linkClickService = linkClickService;
            this._tokenizer = tokenizer;
			this._logger = logger;
            _storeContext = storeContext;
            _messageTemplateService = messageTemplateService;
            _workContext= workContext;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
		}

		#endregion


		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
		[AdminAuthorize]
		public ActionResult Main()
		{
            return View("Main");
		}


		#region Campaign

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Campaigns this instance.
		/// </summary>
		/// <returns></returns>  
		public ActionResult Campaign()
		{
			return View("Campaign");
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Campaigns this instance.
		/// </summary>
		/// <returns></returns>
		[HttpPost]
        public ActionResult Campaign(DataSourceRequest command)
		{
			/*if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return AccessDeniedView();*/

			var campaigns = _emailCampaignService.GetAllCampaigns(command.Page - 1, command.PageSize);

            var gridModel = new DataSourceResult();

            gridModel.Data = campaigns.Select(x =>
            {
                {
                    var model = x.ToEmailCampaignModel();
                    model.CreatedOnUtc = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc);
                    return model;
                }
            });

            gridModel.Total = campaigns.Count();
			return new JsonResult
			{
				Data = gridModel
			};
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates Campaign instance.
		/// </summary>
		/// <returns></returns>
		public ActionResult Create()
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return null;

			var model = new EmailCampaignModel();
			model.CreatedOnUtc = DateTime.UtcNow;
			//model.Schedule = DateTime.Now;
			model.AllowedTokens = FormatTokens(_emailCampaignService.GetListOfAllowedCampaignTokens());

			var allowedTokens = _emailCampaignService.GetListOfAllowedCampaignTokens();

			model.Tokens = allowedTokens.ToTokenSelectList();

			return View("CreateCampaign", model);
			//return View(model);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates the Campaign model.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
        [HttpPost, ValidateInput(false)]
		public ActionResult Create(EmailCampaignModel model)
		{
			if(!string.IsNullOrEmpty(model.EmailAttachment))
			{
				string[] files = model.EmailAttachment.Split(',');
				for(int i = 0; i < files.Length; i++)
				{
                    files[i] = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\EmailAttachment", files[i]);
				}
				model.EmailAttachment = string.Join(",", files);
			}
			//var path = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\Template", fileName);

			if(ModelState.IsValid)
			{
				var campaign = model.ToEmailCampaignEntity();
				campaign.CreatedOnUtc = DateTime.UtcNow;
				


				var insertedCampaign = _emailCampaignService.InsertEmailCampaign(campaign);

				
				//SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Added"));
                return null;
				//return RedirectToAction("Campaign", "/Plugin/Other/EmailMarketing");
			}

			//If we got this far, something failed, redisplay form
			//model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
			return View("CreateCampaign", model);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Edits the Campaign with specified id.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public ActionResult Edit(int id)
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return null;

			var campaign = _emailCampaignService.GetEmailCampaignById(id);
			if(campaign == null)
				//No campaign found with the specified id
				return RedirectToAction("Campaign");

			var model = campaign.ToEmailCampaignModel();
			model.AllowedTokens = FormatTokens(_emailCampaignService.GetListOfAllowedCampaignTokens());

			var allowedTokens = _emailCampaignService.GetListOfAllowedCampaignTokens();

			model.Tokens = allowedTokens.ToTokenSelectList();

			return View("EditCampaign", model);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Edits the specified Campaign model.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
        [HttpPost, ValidateInput(false)]
		//[FormValueExists("save", "save-continue", "continueEditing")]
		//[FormValueRequired("save", "save-continue")]
		public ActionResult Edit(EmailCampaignModel model)
		{
			if(!_permissionService.Authorize(StandardPermissionProvider.ManageCampaigns))
				return null;
			
			var campaign = _emailCampaignService.GetEmailCampaignById(model.Id);
			if(campaign == null)
				//No campaign found with the specified id
				return RedirectToAction("List");

			if(ModelState.IsValid)
			{
				campaign = model.ToEmailCampaignEntity(campaign);
                campaign.GroupName = campaign.GroupName.Remove(campaign.GroupName.Length - 1);
				_emailCampaignService.UpdateEmailCampaign(campaign);

				//SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Updated"));
				//return continueEditing ? RedirectToAction("Edit", new
				//{
				//  id = campaign.Id
				//}) : RedirectToAction("List");

				return null;
			}
			//If we got this far, something failed, redisplay form
			model.AllowedTokens = FormatTokens(_emailCampaignService.GetListOfAllowedCampaignTokens());
			return View(model);
		}

		#endregion


		#region Schedule

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Schedules the list.
		/// </summary>
		/// <returns></returns>
        [AdminAuthorize]
        public ActionResult ScheduleList()
        {
            return View("ScheduleList");
        }

        /////--------------------------------------------------------------------------------------------
        ///// <summary>
        ///// Schedules the list.
        ///// </summary>
        ///// <param name="command">The command.</param>
        ///// <returns></returns>
        [HttpPost]
        public ActionResult ScheduleList(DataSourceRequest command)
        {
            var schedules = _scheduleService.GetAllSchedule(command.Page - 1, command.PageSize);

            var gridModel = new DataSourceResult();

            gridModel.Data = schedules.Select(x =>
            {
                {
                    var model = x.ToScheduleModel();
                    model.EmailCampaign = _emailCampaignService.GetEmailCampaignById(x.CampaignId).ToEmailCampaignModel();
                    model.ScheduledTime = _dateTimeHelper.ConvertToUserTime(model.ScheduledTime, DateTimeKind.Utc);//TimeZoneInfo.ConvertTimeToUtc(model.ScheduledTime).AddHours(timeDiffer);
                    return model;
                }
            });

            gridModel.Total = schedules.TotalCount;
            return new JsonResult
            {
                Data = gridModel
            };
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates the schedule.
		/// </summary>
		/// <param name="campaignId">The campaign id.</param>
		/// <returns></returns>
		public ActionResult CreateSchedule(int campaignId = 0)
		{
			var model = new ScheduleModel();
			model.ScheduledTime = DateTime.Now;
			var campaigns = _emailCampaignService.GetAllCampaignsList();
			model.CampaignId = campaignId;
			model.Campaign = new SelectList(campaigns, "Id", "Name");
			return View("CreateSchedule", model);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates the schedule.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
        [HttpPost]
        public ActionResult CreateSchedule(ScheduleModel model)
        {
            if (ModelState.IsValid)
            {
                //var timeOffset = DateTime.Now - DateTime.UtcNow;

                model.ScheduledTime = _dateTimeHelper.ConvertToUtcTime(model.ScheduledTime, DateTimeKind.Local);//model.ScheduledTime.Add(timeOffset);

                var schedule = model.ToScheduleEntity();

                _scheduleService.SaveScheduleOnType(schedule, schedule.Type);
                //_scheduleService.InsertSchedule(schedule);

                return Json(new
                {
                    success = true,
                    //pictureId = picture.Id,
                    message = "ShowSchedule"
                });
            }

            //If we got this far, something failed, redisplay form
            return View("CreateSchedule", model);
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Edits the schedule.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
        public ActionResult EditSchedule(int id)
        {
            var schedule = _scheduleService.GetScheduleById(id);
            //var timeZoneConverted = TimeZoneInfo.ConvertTimeToUtc(schedule.ScheduledTime).AddHours(timeDiffer);

            if (schedule == null)
                return RedirectToAction("ScheduleList");

            var model = schedule.ToScheduleModel();

            model.ScheduledTime = _dateTimeHelper.ConvertToUserTime(model.ScheduledTime, DateTimeKind.Utc);

            var campaigns = _emailCampaignService.GetAllCampaignsList();
            model.Campaign = new SelectList(campaigns, "Id", "Name");
            var allowedTokens = _emailCampaignService.GetListOfAllowedCampaignTokens();

            return View("EditSchedule", model);
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Edits the schedule.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditSchedule(ScheduleModel model)
        {
            var schedule = _scheduleService.GetScheduleById(model.Id);
            if (schedule == null)
                //No emailTemplate found with the specified id
                return RedirectToAction("TemplateList");

            if (ModelState.IsValid)
            {
                //var timeOffset = DateTime.Now - DateTime.UtcNow;

                model.ScheduledTime = _dateTimeHelper.ConvertToUtcTime(model.ScheduledTime, _dateTimeHelper.CurrentTimeZone);

                var schedules = _scheduleService.GetScheduleByScheduleId(schedule.ScheduleId);

                foreach (var scheduleToBeDelete in schedules)
                {
                    _scheduleService.DeleteSchedule(scheduleToBeDelete);
                }

                schedule = model.ToScheduleEntity(schedule);
                _scheduleService.SaveScheduleOnType(schedule, schedule.Type);

                return null;
            }

            //If we got this far, something failed, redisplay form
            return View("EditSchedule", model);
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Deletes the schedule.
		/// </summary>
		/// <param name="selectedIds">The selected ids.</param>
		/// <returns></returns>
        public ActionResult DeleteSchedule(string selectedIds)
        {
            foreach (var id in selectedIds.Split(','))
            {
                var schedule = _scheduleService.GetScheduleById(Int32.Parse(id));
                if (schedule == null)
                    //No customer found with the specified id
                    return RedirectToAction("ScheduleList");

                try
                {
                    _scheduleService.DeleteSchedule(schedule);


                    //SuccessNotification("Selected schedules deleted success");
                }
                catch (Exception exc)
                {
                    //ErrorNotification(exc.Message);
                }
            }

            return null;

        }

		#endregion


		#region Template

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Templates the list.
		/// </summary>
		/// <returns></returns>
        [AdminAuthorize]
        public ActionResult TemplateList()
        {
            return View("TemplateList");
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Templates the list.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <returns></returns>
        [HttpPost]
        public ActionResult TemplateList(DataSourceRequest command)
        {
            var templates = _emailTemplateService.GetAllEmailTemplate(command.Page - 1, command.PageSize);

            foreach (var template in templates)
            {
                template.ThumbnailPath = _pictureService.GetPictureUrl(template.PictureId);
            }

            var gridModel = new DataSourceResult();

            gridModel.Data = templates.Select(x =>
            {
                {
                    var model = x.ToEmailTemplateModel();
                    return model;
                }
            });

            gridModel.Total = templates.Count();

            //var gridModel = new GridModel<EmailTemplateModel>
            //{
            //    Data = templates.Select(x =>
            //    {
            //        var model = x.ToEmailTemplateModel();
            //        return model;
            //    }),
            //    Total = templates.TotalCount
            //};

            return new JsonResult
            {
                Data = gridModel
            };
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Views the templates.
		/// </summary>
		/// <returns></returns>
        [AdminAuthorize]
        public ActionResult ViewTemplates()
        {
            var templates = _emailTemplateService.GetAllEmailTemplate();

            foreach (var template in templates)
            {
                template.ThumbnailPath = _pictureService.GetPictureUrl(template.PictureId);
            }

            return View("ViewTemplates", templates);
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Gets the template.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
        [AdminAuthorize]
        public string GetTemplate(int id = 0)
        {
            var template = _emailTemplateService.GetEmailTemplateById(id);
            return template.TemplateBody;
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates the template.
		/// </summary>
		/// <returns></returns>
		public ActionResult CreateTemplate()
		{
			var model = new EmailTemplateModel();
			//model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());

			var allowedTokens = _emailCampaignService.GetListOfAllowedCampaignTokens();

			model.Tokens = allowedTokens.ToTokenSelectList();

			return View("CreateTemplate", model);
			//return View(model);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates the template.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
        [HttpPost]
        public ActionResult CreateTemplate(EmailTemplateModel model)
        {
            if (ModelState.IsValid)
            {
                var emailTemplate = model.ToEmailTemplateEntity();
                emailTemplate.ThumbnailPath = _pictureService.GetPictureUrl(model.PictureId);
                _emailTemplateService.InsertEmailTemplate(emailTemplate);

                return Json(new
                {
                    success = true,
                    //pictureId = picture.Id,
                    message = "ShowTemplate"
                });
            }

            return View("CreateTemplate", model);
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Edits the template.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
        public ActionResult EditTemplate(int id)
        {
            var emailTemplate = _emailTemplateService.GetEmailTemplateById(id);
            if (emailTemplate == null)
                //No emailTemplate found with the specified id
                return RedirectToAction("TemplateList");

            var model = emailTemplate.ToEmailTemplateModel();
            var allowedTokens = _emailCampaignService.GetListOfAllowedCampaignTokens();

            model.Tokens = allowedTokens.ToTokenSelectList();
            //model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
            return View("EditTemplate", model);
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Edits the template.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
        [HttpPost]
        //[FormValueExists("save", "save-continue", "continueEditing")]
        //[FormValueRequired("save", "save-continue")]
        public ActionResult EditTemplate(EmailTemplateModel model)
        {
            var emailTemplate = _emailTemplateService.GetEmailTemplateById(model.Id);
            if (emailTemplate == null)
                //No emailTemplate found with the specified id
                return RedirectToAction("TemplateList");

            if (ModelState.IsValid)
            {
                emailTemplate = model.ToEmailTemplateEntity(emailTemplate);
                _emailTemplateService.UpdateEmailTemplate(emailTemplate);

                //SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Updated"));
                //return continueEditing ? RedirectToAction("Edit", new
                //{
                //  id = campaign.Id
                //}) : RedirectToAction("List");

                return Json(new
                {
                    success = true,
                    message = "ShowTemplate"
                });
            }

            //If we got this far, something failed, redisplay form
            //model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
            return View("EditTemplate", model);
        }


		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Deletes the template.
		/// </summary>
		/// <param name="selectedIds">The selected ids.</param>
		/// <returns></returns>
        public ActionResult DeleteTemplate(string selectedIds)
        {
            foreach(var id in selectedIds.Split(','))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
            {
                var template = _emailTemplateService.GetEmailTemplateById(Int32.Parse(id));
                if(template == null)
                    //No customer found with the specified id
                    return RedirectToAction("TemplateList");

                try
                {
                    _emailTemplateService.DeleteEmailTemplate(template);


                    //SuccessNotification("Selected templates deleted success");
                }
                catch(Exception exc)
                {
                    //ErrorNotification(exc.Message);
                }
            }
            return null;
        }

        public ActionResult DeleteCampaigns(string selectedIds)
        {
            foreach (var id in selectedIds.Split(','))
            {
                var Campaign = _emailCampaignService.GetEmailCampaignById(Int32.Parse(id));
                if (Campaign == null)
                    //No customer found with the specified id
                    return RedirectToAction("TemplateList");

                try
                {
                    _emailCampaignService.DeleteEmailCampaign(Campaign);


                    //SuccessNotification("Selected templates deleted success");
                }
                catch (Exception exc)
                {
                    //ErrorNotification(exc.Message);
                }
            }
            return null;
        }


		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Links the click.
		/// </summary>
		/// <param name="linkId">The link id.</param>
		/// <param name="emailId">The email id.</param>
		/// <param name="campaignId">The campaign id.</param>
		/// <returns></returns>
		public RedirectResult LinkClick(int linkId = 0, int emailId = 0, int campaignId = 0)
		{
			var clickedLink = _linkService.GetLinkById(linkId);
			clickedLink.Click = clickedLink.Click + 1;
			_linkService.UpdateLink(clickedLink);
			_linkService.InsertLinkClick(linkId, emailId);

			var clickedCampaign = _campaignQueueService.GetQueuedCampaignById(campaignId);
			clickedCampaign.ClickedThrough = clickedCampaign.ClickedThrough + 1;
			_campaignQueueService.UpdateCampaignQueue(clickedCampaign);

			//_linkService.GetEmailByLinkId(linkId);
			/*var linkClickModel = new LinkClickModel();
			linkClickModel.LinkId = linkId;
			linkClickModel.EmailId = emailId;
			_linkClickService.InsertLinkClick(linkClickModel.ToLinkClickEntity());*/

			return Redirect(clickedLink.LinkHref);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Links the click detail.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public ActionResult LinkClickDetail(int id = 0)
		{
			ViewBag.LinkId = id;
			return View("LinkClickDetail");
		}
		
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Links the click detail.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		[HttpPost]
        public ActionResult LinkClickDetail(DataSourceRequest command, int id = 0)
		{
            var linkClickDetail = _linkService.GetEmailByLinkId(command.Page - 1, command.PageSize, id);

            var gridModel = new DataSourceResult();

            gridModel.Data = linkClickDetail.Select(x =>
            {
                {
                    LinkClickDetailModel m = x.ToLinkClickDetailModel();
                    return m;
                }
            });

            gridModel.Total = linkClickDetail.Count();

			return new JsonResult
			{
				Data = gridModel
			};
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Emails the open.
		/// </summary>
		/// <param name="emailId">The email id.</param>
		/// <param name="campaignId">The campaign id.</param>
        /// 

        [HttpGet]
        public ActionResult EmailOpen(int emailId = 0, int campaignId = 0)
		{
			var openedEmail = _emailService.GetEmailById(emailId);
			openedEmail.OpenStatus = "opened";
			_emailService.UpdateEmail(openedEmail);

			var openedCampaign = _campaignQueueService.GetQueuedCampaignById(campaignId);
			openedCampaign.Opened = openedCampaign.Opened + 1;
			_campaignQueueService.UpdateCampaignQueue(openedCampaign);
            return null;
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Tests the email.
		/// </summary>
		/// <param name="campaign">The campaign.</param>
		[HttpPost]
        public async Task TestEmail(EmailCampaignModel campaign)
		{
			if(campaign.Id == 0)
			{
				if(!string.IsNullOrEmpty(campaign.EmailAttachment))
				{
                    if (!campaign.EmailAttachment.Contains('/'))
                    {
                        string[] files = campaign.EmailAttachment.Split(',');
                        for (int i = 0; i < files.Length; i++)
                        {
                            files[i] = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\EmailAttachment", files[i]);
                        }
                        campaign.EmailAttachment = string.Join(",", files);
                    }
				}
				//var path = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\Template", fileName);

				if(ModelState.IsValid)
				{
					var campaignToBeInsert = campaign.ToEmailCampaignEntity();
					campaignToBeInsert.CreatedOnUtc = DateTime.UtcNow;



					var insertedCampaign = _emailCampaignService.InsertEmailCampaign(campaignToBeInsert);
					campaign = insertedCampaign.ToEmailCampaignModel();
				}
			}
            ProcessCampaignFacade facade = new ProcessCampaignFacade(_campaignQueueService, _messageTemplateService, _storeInformationSettings, _storeContext, _smartGroupsService, _emailService, _messageTokenProvider, _linkService, _emailAccountService, _workContext, _emailAccountSettings, _tokenizer, _scheduleService);

            await facade.ProcessCampaign(campaign, _storeContext.CurrentStore.Url);

            RedirectToAction("Main");
			/*var queuedCampaign = facade.GetQueuedCampaign(campaign);

			string campaignBody = facade.InsertCampaignLink(campaign.Body, queuedCampaign.Id);

			var campaignContacts = facade.GetCampaignContacts(queuedCampaign, campaign);

			foreach(SmartContactModel contact in campaignContacts)
			{
				var trackedEmail = facade.TrackEmail(contact, queuedCampaign, campaign);
				//campaign = facade.InsertOpenImage(campaign, trackedEmail, _storeInformationSettings.StoreUrl, campaignBody);
				campaign = facade.ReplaceLink(campaign, trackedEmail, _storeInformationSettings.StoreUrl, campaignBody);
				facade.SendEmail(trackedEmail, queuedCampaign, campaign);
			}

			facade.UpdateCampaignQue(queuedCampaign);*/
			/*queuedCampaign.EmailInsertStatus = "Complete";
			_campaignQueueService.UpdateCampaignQueue(queuedCampaign);*/

		}

		//private CampaignModel PrepareCampaignModel(CampaignModel campaign, ProcessCampaignFacade facade)
		//{
		//  campaign = facade.InsertOpenImage(campaign, _storeInformationSettings.StoreUrl);
		//  return campaign;
		//}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Zips the file upload.
		/// </summary>
		/// <returns></returns>
        [HttpPost]
        public ActionResult ZipFileUpload()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.UploadPictures))
            //    return Json(new
            //    {
            //        success = false,
            //        error = "You do not have required permissions"
            //    }, "text/plain");



            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            //Stream stream = null;
            var fileName = "";
            var stream = Request.InputStream;
            //var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);

                //contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileExtension = Path.GetExtension(fileName);

            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            if (!fileExtension.Equals(".zip"))
            {
                return Json(new
                {
                    success = false,
                    //pictureId = picture.Id,
                    errorMessage = "Please upload a zip file"
                }, "text/plain");
            }

            var path = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\Template", fileName);
            var buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            System.IO.File.WriteAllBytes(path, buffer);

            var baseUrl = _storeContext.CurrentStore.Url;

            var fileInfo = new FileInfo(path);

            string uploadedTemplateHtml = Decompress.DecompressFile(fileInfo, baseUrl);



            return Json(new
            {
                success = true,
                uploadedFileName = fileName,
                templateHtml = uploadedTemplateHtml
                //pictureId = picture.Id,
                //imageUrl = _pictureService.GetPictureUrl(picture, 100)
            }, "text/plain");
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Emails the attachment.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EmailAttachment(string savedFile = "")
        {

            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            //Stream stream = null;



            var randomString = CommonHelper.GenerateRandomInteger().ToString();
            var fileName = "";
            var stream = Request.InputStream;
            //var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)
                    throw new ArgumentException("No file uploaded");
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);
                //contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileExtension = Path.GetExtension(fileName);

            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            /*if(!fileExtension.Equals(".zip"))
            {
                return Json(new
                {
                    success = false,
                    //pictureId = picture.Id,
                    errorMessage = "Please upload a zip file"
                }, "text/plain");
            }*/

            var path = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\EmailAttachment", randomString + fileName);
            var buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            System.IO.File.WriteAllBytes(path, buffer);

            var baseUrl = _storeContext.CurrentStore.Url;

            var fileInfo = new FileInfo(path);

            //string uploadedTemplateHtml = Decompress.DecompressFile(fileInfo, baseUrl);



            return Json(new
            {
                success = true,
                random = randomString,
                uploadedFileName = fileName
                //templateHtml = uploadedTemplateHtml
                //pictureId = picture.Id,
                //imageUrl = _pictureService.GetPictureUrl(picture, 100)
            }, "text/plain");
        }

		public JsonResult DeleteAttachment(string savedFile = "")
		{
			string savedFileFullPath = Path.Combine(this.Request.PhysicalApplicationPath, "Content\\EmailAttachment", savedFile);
			if(System.IO.File.Exists(savedFileFullPath))
			{
				try
				{
					System.IO.File.Delete(savedFileFullPath);
					return Json(new
					{
						deleteSuccess = true
					}, JsonRequestBehavior.AllowGet);
				}
				catch(Exception exec)
				{
					return Json(new
					{
						deleteSuccess = false,
						error = exec.ToString()
					}, JsonRequestBehavior.AllowGet);
				}
			}
			return Json(new
			{
				deleteSuccess = false
			}, JsonRequestBehavior.AllowGet);

			
		}

		#endregion


		#region Group

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
        [AdminAuthorize]
        public ActionResult Group()
        {
            return View("Group");
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Groups the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Group(DataSourceRequest command)
        {
            var groups = _smartGroupsService.GetAllSmartGroup(command.Page - 1, command.PageSize);


            var gridModel = new DataSourceResult();

            gridModel.Data = groups.Select(x =>
            {
                {
                    CriteriaModel m = x.ToCriteriaModel();
                    return m;
                }
            });

            gridModel.Total = groups.Count();

            return new JsonResult
            {
                Data = gridModel
            };
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Smarts the group.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
        [AdminAuthorize]
        public ActionResult SmartGroup(int id)
        {
            ViewBag.SmartGroupId = id;
            return View("SmartContactList");
        }
        [HttpPost]
        public ActionResult SmartGroup(DataSourceRequest command,int Id)
        {

            var contacts = _smartGroupsService.GetContacts(Id, 0, _adminAreaSettings.GridPageSize);
            // _smartGroupsService.GetCustomerInfoById(2433);

            var gridModel = new DataSourceResult();

            gridModel.Data = contacts.Select(x =>
            {
                {
                    var model = x;

                    return model;
                }
            });

            gridModel.Total = contacts.Count();

            ViewBag.SmartGroupId = Id;
            return new JsonResult
            {
                Data = gridModel
            };
        }

        /////--------------------------------------------------------------------------------------------
        ///// <summary>
        ///// Smarts the group ajax.
        ///// </summary>
        ///// <param name="smartGroupId">The smart group id.</param>
        ///// <param name="command">The command.</param>
        ///// <returns></returns>
        //[HttpPost]
        //public ActionResult SmartGroupAjax(int smartGroupId, DataSourceRequest command)
        //{

        //    var contacts = _smartGroupsService.GetContacts(smartGroupId, command.Page - 1, command.PageSize);
        //    IEnumerable<SmartContactModel> test = contacts;


        //    var gridModel = new DataSourceResult();

        //    gridModel.Data = contacts.Select(x =>
        //    {
        //        {
        //            SmartContactModel m = x;

        //            return m;
        //        }
        //    });

        //    gridModel.Total = contacts.Count();


        //    //var GridModel = new GridModel<SmartContactModel>
        //    //{
        //    //    Data = contacts.Select(x =>
        //    //    {

        //    //        SmartContactModel m = x;

        //    //        return m;
        //    //    }),

        //    //    Total = contacts.TotalCount

        //    //};

        //    JsonResult result = new JsonResult();

        //    result.Data = gridModel;
        //    result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

        //    return result;

        //}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates the group.
		/// </summary>
		/// <returns></returns>
		public ActionResult CreateGroup()
		{
			var model = new CriteriaModel();
			return View("CreateGroup", model);
			//return View(model);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Creates the group.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
        [HttpPost]
        public ActionResult CreateGroup(CriteriaModel model)
        {

            if (ModelState.IsValid)
            {
                var criteria = model.ToSmartGroupEntity();
                _smartGroupsService.InsertSmartGroup(criteria);

                //SuccessNotification(_localizationService.GetResource("Admin.Promotions.Campaigns.Added"));
                /*return continueEditing ? RedirectToAction("Edit", new
                {
                    id = campaign.Id
                }) : RedirectToAction("Campaign");*/
                return Json(new
                {
                    success = true,
                    //pictureId = picture.Id,
                    message = "ShowGroup"
                });
            }

            //If we got this far, something failed, redisplay form
            //model.AllowedTokens = FormatTokens(_messageTokenProvider.GetListOfCampaignAllowedTokens());
            return View("CreateGroup", model);
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Edits the group.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public ActionResult EditGroup(int id)
        {
            var smartGroup = _smartGroupsService.GetSmartGroupById(id);
            if (smartGroup == null)
                //No campaign found with the specified id
                return RedirectToAction("Main");

            var model = smartGroup.ToCriteriaModel();
            return View("EditGroup", model);
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Edits the group.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
        [HttpPost]
        //[FormValueExists("save", "save-continue", "continueEditing")]
        //[FormValueRequired("save", "save-continue")]
        public ActionResult EditGroup(CriteriaModel model)
        {


            var smartGroup = _smartGroupsService.GetSmartGroupById(model.Id);
            if (smartGroup == null)
                //No campaign found with the specified id
                return RedirectToAction("Group");

            if (ModelState.IsValid)
            {
                model.ToSmartGroupEntity(smartGroup);
                //smartGroup.Name = model.Name;
                _smartGroupsService.UpdateSmartGroup(smartGroup);


                return Json(new
                {
                    success = true,
                    //pictureId = picture.Id,
                    message = "ShowGroup"
                });
            }

            //If we got this far, something failed, redisplay form
            return View("EditGroup", model);
        }

        /////--------------------------------------------------------------------------------------------
        ///// <summary>
        ///// Deletes the group.
        ///// </summary>
        ///// <param name="selectedIds">The selected ids.</param>
        ///// <returns></returns>
        public ActionResult DeleteGroup(string selectedIds)
        {
            foreach (var id in selectedIds.Split(','))
            {
                var group = _smartGroupsService.GetSmartGroupById(Int32.Parse(id));

                if (group == null)
                    //No customer found with the specified id
                    return RedirectToAction("Group");

                try
                {
                    group.IsDeleted = true;
                    _smartGroupsService.UpdateSmartGroup(group);


                    //SuccessNotification("Selected group deleted success");
                }
                catch (Exception exc)
                {
                    //ErrorNotification(exc.Message);
                }
            }
            return null;
        }

		#endregion


		#region Report
		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
		public ActionResult Report()
		{
			return View("Report");
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Reports the specified command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <returns></returns>
		[HttpPost]
        public ActionResult Report(DataSourceRequest command)
		{
			var campaigns = _campaignQueueService.GetAllQueuedCampaigns(command.Page - 1, command.PageSize);

            var gridModel = new DataSourceResult();

            gridModel.Data = campaigns.Select(x =>
            {
                {
                    var model = x.ToCampaignReportModel();
                    var campaign = _emailCampaignService.GetEmailCampaignById(x.CampaignId);
                    var totalEmail = _emailService.CountEmailsByCampaignId(x.Id);
                    model.Name = campaign.Name;
                    model.Subject = campaign.Name;
                    model.BroadCastTime = _dateTimeHelper.ConvertToUserTime(model.BroadCastTime, DateTimeKind.Utc);
                    model.DeliveredPercentage = totalEmail > 0 ? DisplayPercentage((double)model.Delivered / totalEmail) : "0";
                    model.OpenedPercentage = model.Delivered > 0 ? DisplayPercentage((double)_emailService.CountOpenedEmailsByCampaignId(x.Id) / model.Delivered) : "0";

                    return model;
			
                }
            });

            gridModel.Total = campaigns.TotalCount;
			return new JsonResult
			{
				Data = gridModel
			};
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the form.
		/// </summary>
		/// <returns></returns>
		public ActionResult Link(int id = 0)
		{
			var links = _linkService.GetAllLink(0, _adminAreaSettings.GridPageSize, id);


            var gridModel = new DataSourceResult();

            gridModel.Data = links.Select(x =>
            {
                {
                    var model = x.ToLinkModel();

                    return model;

                }
            });

            gridModel.Total = links.TotalCount;


            //var gridModel = new GridModel<LinkModel>
            //{
            //    Data = links.Select(x =>
            //    {
            //        var model = x.ToLinkModel();
					
            //        return model;

            //    }),

            //    Total = links.TotalCount
            //};

			ViewBag.CampaignId = id;
			return View("Nop.Plugin.Other.EmailMarketing.Views.EmailMarketing.Link", gridModel);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Links the specified command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		[HttpPost]
        public ActionResult Link(DataSourceRequest command, int id = 0)
		{

            id = Convert.ToInt16(Session["id"].ToString());

			var links = _linkService.GetAllLink(0, _adminAreaSettings.GridPageSize, id);


            var gridModel = new DataSourceResult();

            gridModel.Data = links.Select(x =>
            {
                {
                    var model = x.ToLinkModel();

                    return model;

                }
            });

            gridModel.Total = links.TotalCount;

            //var gridModel = new GridModel<LinkModel>
            //{
            //    Data = links.Select(x =>
            //    {
            //        var model = x.ToLinkModel();

            //        return model;

            //    }),

            //    Total = links.TotalCount
            //};

			return new JsonResult
			{
				Data = gridModel
			};
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Details the report.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <param name="status">The status.</param>
		/// <returns></returns>
		public ActionResult DetailReport(int id, string status = "all")
		{
            Session["id"] = id;
			ViewBag.CampaignId = id;
            ViewBag.Id = id;
			ViewBag.Status = status;
			return View("DetailReport");
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Details the report.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="id">The id.</param>
		/// <param name="status">The status.</param>
		/// <returns></returns>
		[HttpPost]
        public ActionResult DetailReport(DataSourceRequest command, int id, string status = "all")
		{
            //Session["id"] = id;
			var emails = _emailService.GetAllEmailByCampaignId(0, _adminAreaSettings.GridPageSize, id, status);

            var gridModel = new DataSourceResult();

            gridModel.Data = emails.Select(x =>
            {
                {
                    var model = x.ToEmailModel();

                    return model;

                }
            });

            gridModel.Total = emails.TotalCount;

			return new JsonResult
			{
				Data = gridModel
			};
		}

        [HttpPost]
        public ActionResult DeliveredReport(DataSourceRequest command, int id, string status = "delivered")
        {
            id = Convert.ToInt16(Session["id"].ToString());

            var emails = _emailService.GetAllEmailByCampaignId(0, _adminAreaSettings.GridPageSize, id, status);

            var gridModel = new DataSourceResult();

            gridModel.Data = emails.Select(x =>
            {
                {
                    var model = x.ToEmailModel();

                    return model;

                }
            });

            gridModel.Total = emails.TotalCount;
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [HttpPost]
        public ActionResult BouncedReport(DataSourceRequest command, int id, string status = "bounced")
        {
            id = Convert.ToInt16(Session["id"].ToString());
            var emails = _emailService.GetAllEmailByCampaignId(0, _adminAreaSettings.GridPageSize, id, status);

            var gridModel = new DataSourceResult();

            gridModel.Data = emails.Select(x =>
            {
                {
                    var model = x.ToEmailModel();

                    return model;

                }
            });

            gridModel.Total = emails.TotalCount;
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [HttpPost]
        public ActionResult OpenedReport(DataSourceRequest command, int id, string status = "opened")
        {
            id = Convert.ToInt16(Session["id"].ToString());
            var emails = _emailService.GetAllEmailByCampaignId(0, _adminAreaSettings.GridPageSize, id, status);

            var gridModel = new DataSourceResult();

            gridModel.Data = emails.Select(x =>
            {
                {
                    var model = x.ToEmailModel();

                    return model;

                }
            });

            gridModel.Total = emails.TotalCount;
            return new JsonResult
            {
                Data = gridModel
            };
        }


		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Shows the template.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		[AdminAuthorize]
        public ActionResult ShowTemplate(int id = 0)
		{
            ShowTemplateModel objOfSmartContactModel=new ShowTemplateModel();
			var campaign = _emailCampaignService.GetEmailCampaignById(id);
			objOfSmartContactModel.Body= campaign.Body;
            return View("ShowTemplate", objOfSmartContactModel);
		}


		#region Export / Import

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Reports the export excel all.
		/// </summary>sq
		/// <returns></returns>
        public ActionResult ReportExportExcelAll()
        {
            try
            {
                var qeuedCampaigns = _campaignQueueService.GetAllQueuedCampaigns(0, int.MaxValue);
                var campaignReport = new List<CampaignReportModel>();
                foreach (var qeuedCampaign in qeuedCampaigns)
                {
                    var campaign = _emailCampaignService.GetEmailCampaignById(qeuedCampaign.CampaignId);
                    var totalEmail = _emailService.CountEmailsByCampaignId(qeuedCampaign.Id);
                    var campaignReportModel = new CampaignReportModel
                        {
                            Name = (campaign != null) ? campaign.Name : string.Empty,
                            Subject = (campaign != null) ? campaign.Subject : string.Empty,
                            BroadCastTime = qeuedCampaign.BroadCastTime,
                            Delivered = qeuedCampaign.Delivered,
                            Opened = qeuedCampaign.Opened,
                            ClickedThrough = qeuedCampaign.ClickedThrough,
                            DeliveredPercentage = totalEmail > 0 ? DisplayPercentage((double)qeuedCampaign.Delivered / totalEmail) : "0",
                            OpenedPercentage = qeuedCampaign.Delivered > 0 ? DisplayPercentage((double)_emailService.CountOpenedEmailsByCampaignId(qeuedCampaign.Id) / qeuedCampaign.Delivered) : "0"
                        };
                    campaignReport.Add(campaignReportModel);
                }



                //var customers = _customerService.GetAllCustomers(null, null, null, null,
                //    null, null, null, 0, 0, null, null, null,
                //    false, null, 0, int.MaxValue);

                string fileName = string.Format("campaign_report_{0}_{1}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
                string filePath = System.IO.Path.Combine(Request.PhysicalApplicationPath, "content\\email_report", fileName);

                ExportManager.ExportCampaignsToXlsx(filePath, campaignReport, _storeContext);

                var bytes = System.IO.File.ReadAllBytes(filePath);
                return File(bytes, "text/xls", fileName);
            }
            catch (Exception exc)
            {
                //ErrorNotification(exc);
                return RedirectToAction("Report", "/Plugin/Other/EmailMarketing");
            }
        }


        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Reports the export XML all.
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportExportXmlAll()
        {
            try
            {
                var qeuedCampaigns = _campaignQueueService.GetAllQueuedCampaigns(0, int.MaxValue);
                var campaignReport = new List<CampaignReportModel>();
                foreach (var qeuedCampaign in qeuedCampaigns)
                {
                    var campaign = _emailCampaignService.GetEmailCampaignById(qeuedCampaign.CampaignId);
                    var totalEmail = _emailService.CountEmailsByCampaignId(qeuedCampaign.Id);
                    var campaignReportModel = new CampaignReportModel
                    {
                        Name = (campaign != null) ? campaign.Name : string.Empty,
                        Subject = (campaign != null) ? campaign.Subject : string.Empty,
                        BroadCastTime = qeuedCampaign.BroadCastTime,
                        Delivered = qeuedCampaign.Delivered,
                        Opened = qeuedCampaign.Opened,
                        ClickedThrough = qeuedCampaign.ClickedThrough,
                        DeliveredPercentage = totalEmail > 0 ? DisplayPercentage((double)qeuedCampaign.Delivered / totalEmail) : "0",
                        OpenedPercentage = qeuedCampaign.Delivered > 0 ? DisplayPercentage((double)_emailService.CountOpenedEmailsByCampaignId(qeuedCampaign.Id) / qeuedCampaign.Delivered) : "0"
                    };
                    campaignReport.Add(campaignReportModel);
                }


                var fileName = string.Format("campaign_report_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
                var xml = ExportManager.ExportCampaignsToXml(campaignReport);


                //ExportManager.ExportCampaignsToXml(campaignReport);

                return new XmlDownloadResult(xml, fileName);
            }
            catch (Exception exc)
            {
                //ErrorNotification(exc);
                return RedirectToAction("Report", "/Plugin/Other/EmailMarketing");
            }
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Details the report export excel all.
		/// </summary>
		/// <returns></returns>
        public ActionResult DetailReportExportExcelAll(int campaignId, string status = "all")
        {
            try
            {
                string fileName = string.Format("detail_campaign_report_{0}_{1}.xlsx", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
                string filePath = System.IO.Path.Combine(Request.PhysicalApplicationPath, "content\\email_report", fileName);

                if (status.Equals("link"))
                {
                    List<Link> detailLinkReport = _linkService.GetAllLink(campaignId);
                    ExportManager.ExportDetailCampaignsToXlsx(filePath, detailLinkReport, _storeContext);
                }
                /*else if(status.Equals("linkClick"))
                {
                    List<LinkClick> detailLinkClickReport = _linkClickService.GetAllLinkClick(campaignId);
                    ExportManager.ExportDetailCampaignsToXlsx(filePath, detailLinkClickReport, _storeInformationSettings);
                }*/
                else
                {
                    List<Email> detailEmailReport = _emailService.GetAllEmailByCampaignId(campaignId, status);
                    ExportManager.ExportDetailCampaignsToXlsx(filePath, detailEmailReport, _storeContext);
                }


                var bytes = System.IO.File.ReadAllBytes(filePath);
                return File(bytes, "text/xls", fileName);
            }
            catch (Exception exc)
            {
                //ErrorNotification(exc);
                return RedirectToAction("Report", "/Plugin/Other/EmailMarketing");
            }
        }

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Details the report export XML all.
		/// </summary>
		/// <returns></returns>
		public ActionResult DetailReportExportXmlAll(int campaignId, string status = "all")
		{
			try
			{
				var detailReport = _emailService.GetAllEmailByCampaignId(campaignId, status);

				var fileName = string.Format("detail_campaign_report_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
				var xml = ExportManager.ExportDetailCampaignsToXml(detailReport);

				return new XmlDownloadResult(xml, fileName);
			}
			catch(Exception exc)
			{
                //ErrorNotification(exc);
				return RedirectToAction("Report", "/Plugin/Other/EmailMarketing");
			}
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Updates the report. This method is used only test purpose
		/// </summary>
		public void UpdateReport()
		{
			try
			{
				string directoryPath = @"\\\\mta1\MTA-Logs";
				
				var test = System.IO.Path.Combine(directoryPath, "bounce-acct");
				string[] fileNames = Directory.GetFiles(test, "*.csv");
				_logger.Error(string.Join(",", fileNames));
				
				var processLog = new ProcessLogDirectory(directoryPath);
				List<UpdateReportModel> updatedReportList = processLog.GetUpdatedReport();
				var individualCampaigns = updatedReportList.Select(o => o.EnvId).Distinct();

				foreach(var reportToBeUpdate in updatedReportList)
				{
					var targetEmail = _emailService.GetEmailById(reportToBeUpdate.JobId);
					var processedEmail = reportToBeUpdate.ToEmailEntity(targetEmail);
					_emailService.UpdateEmail(processedEmail);
				}

				foreach(var campaign in individualCampaigns)
				{
					var campaignQueToBeUpdate = _campaignQueueService.GetQueuedCampaignById(campaign);
					var numberOfEmailSent = _emailService.GerSentEmailsByCampaignId(campaign);
					campaignQueToBeUpdate.Delivered = numberOfEmailSent;
					_campaignQueueService.UpdateCampaignQueue(campaignQueToBeUpdate);
				}
				_logger.Error("Update Report Success");
			}
			catch(Exception ex)
			{
				_logger.Error(ex.ToString());
			}
			
			/*var impersonateService = new ImpersonationService();
			try
			{
				if(impersonateService.Impersonate(@"3nerds\BrainStation-23", "3nerds.net", "d3v3l0pm3nt~"))
				{
					// Your business logic here
					//this.UpdateReport(@"\\mta1\MTA-Logs");
					var serverPath = @"\\mta1\MTA-Logs\bounce-acct";
					var dirInfo = new DirectoryInfo(serverPath);
					//_logger.Error(dirInfo.Exists.ToString());

					impersonateService.UndoImpersonationContext();
					//_logger.Error("Report updated successfully from PowerMta Log file @ " + DateTime.Now.ToString());
				}
				else
				{
					//logging errors
					_logger.Error("Network shared folder of Bounce log file not Found");
				}
			}
			catch(Exception ex)
			{
				// logging errors
				_logger.Error("User Impersonation error ==>> " + ex.ToString());
			}*/
		}

		#endregion

		#endregion


		#region OtherHelperMethod

		private void UpdateReport(string folderPath)
		{
			string directoryPath = folderPath;
			var processLog = new ProcessLogDirectory(directoryPath);
			
			List<UpdateReportModel> updatedReportList = processLog.GetUpdatedReport();
			var individualCampaigns = updatedReportList.Select(o => o.EnvId).Distinct();

			foreach(var reportToBeUpdate in updatedReportList)
			{
				var targetEmail = _emailService.GetEmailById(reportToBeUpdate.JobId);
				var processedEmail = reportToBeUpdate.ToEmailEntity(targetEmail);
				_emailService.UpdateEmail(processedEmail);
			}

			foreach(var campaign in individualCampaigns)
			{
				var campaignQueToBeUpdate = _campaignQueueService.GetQueuedCampaignById(campaign);
				var numberOfEmailSent = _emailService.GerSentEmailsByCampaignId(campaign);
				campaignQueToBeUpdate.Delivered = numberOfEmailSent;
				_campaignQueueService.UpdateCampaignQueue(campaignQueToBeUpdate);
			}
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Formats the tokens.
		/// </summary>
		/// <param name="tokens">The tokens.</param>
		/// <returns></returns>
		private string FormatTokens(string[] tokens)
		{
			var sb = new StringBuilder();
			for(int i = 0; i < tokens.Length; i++)
			{
				string token = tokens[i];
				sb.Append(token);
				if(i != tokens.Length - 1)
					sb.Append(", ");
			}

			return sb.ToString();
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Toes the specified term.
		/// </summary>
		/// <param name="term">The term.</param>
		/// <returns></returns>
		public JsonResult To(string term = "")
		{

            var customersEmail = _emailService.SearchCustomerByEmail(term);
			return Json(customersEmail, JsonRequestBehavior.AllowGet);
			//return View(model);
		}

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Groups the name.
		/// </summary>
		/// <param name="term">The term.</param>
		/// <returns></returns>
        public JsonResult GroupName(string term = "")
        {
            var groups = _smartGroupsService.SmartGroupAutoComplete(term);
            return Json(groups, JsonRequestBehavior.AllowGet);
            //return View(model);
        }

		private string DisplayPercentage(double ratio)
		{
			string percentage = string.Format("{0:0.0%}", ratio);
			return percentage;
		}

		

		#endregion


	}
}

