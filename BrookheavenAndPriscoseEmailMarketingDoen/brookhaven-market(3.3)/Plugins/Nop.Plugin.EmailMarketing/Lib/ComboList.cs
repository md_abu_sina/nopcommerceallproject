﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Plugin.EmailMarketing.Lib
{
	/// <summary>
	/// All of these following classes are used for creating combobox list in add/edit smart groups
	/// </summary>
	public static class ComboList
	{
		public static IEnumerable<SelectListItem> ColumnList
		{
			get
			{
				return new List<SelectListItem>
				       {
				       	{new SelectListItem{Text = "Username", Value = "Customer.Username"}},
				       	{new SelectListItem{Text = "Email", Value = "Customer.Email"}},
								{new SelectListItem{Text = "Active", Value = "Customer.Active"}},
				       	{new SelectListItem{Text = "Customer Created on", Value = "Customer.Created on"}},
								{new SelectListItem{Text = "Last Login Date", Value = "Customer.Last Login Date"}},
				       	{new SelectListItem{Text = "Last Activity Date", Value = "Customer.Last Activity Date"}},
								
								{new SelectListItem{Text = "Active", Value = "NewsLetterSubscription.Active"}},
				       	{new SelectListItem{Text = "NewsLetter Created on", Value = "NewsLetterSubscription.Created on"}},
								{new SelectListItem{Text = "NewsLetter Email", Value = "NewsLetterSubscription.Email"}},
								{new SelectListItem{Text = "Daily", Value = "NewsLetterSubscription.Daily"}},
				       	{new SelectListItem{Text = "Weekly", Value = "NewsLetterSubscription.Weekly"}},
								
								{new SelectListItem{Text = "Name", Value = "CustomerRole.Name"}},
								{new SelectListItem{Text = "FreeShipping", Value = "CustomerRole.FreeShipping"}},
								{new SelectListItem{Text = "TaxExempt", Value = "CustomerRole.TaxExempt"}},
								{new SelectListItem{Text = "Active", Value = "CustomerRole.Active"}},
								{new SelectListItem{Text = "IsSystemRole", Value = "CustomerRole.IsSystemRole"}},
								{new SelectListItem{Text = "SystemName", Value = "CustomerRole.SystemName"}},
								{new SelectListItem{Text = "StoreRole", Value = "CustomerRole.StoreRole"}},
								{new SelectListItem{Text = "StoreId", Value = "CustomerRole.StoreId"}},

								{new SelectListItem{Selected = true,Text = "City", Value = "GenericAttribute.City"}},
								{new SelectListItem{Selected = true,Text = "JoinWineLoversClub", Value = "GenericAttribute.JoinWineLoversClub"}},
								{new SelectListItem{Selected = true,Text = "Active", Value = "GenericAttribute.Active"}},

								/*{new SelectListItem{Selected = true,Text = "Role", Value = "Customer.Role"}},
				       	{new SelectListItem{Selected = true,Text = "Created on", Value = "Customer.Created on"}},*/
				       };
			}
		}

		public static IEnumerable<SelectListItem> ConditionList
		{
			get
			{
				return new List<SelectListItem>
				       {
				       	{new SelectListItem{Text = "Is Equal To", Value = "Is Equal To"}},
				       	{new SelectListItem{Text = "Begin With", Value = "Begin With"}},
								{new SelectListItem{Text = "Contains", Value = "Contains"}},
								{new SelectListItem{Text = "Does not Contain", Value = "Does not Contain"}},
								{new SelectListItem{Text = "Greater Than", Value = "Greater Than"}},
								{new SelectListItem{Text = "Less Than", Value = "Less Than"}},
								/*{new SelectListItem{Selected = true,Text = "Is Blank", Value = "Is Blank"}},
								{new SelectListItem{Selected = true,Text = "Is not Blank", Value = "Is not Blank"}},*/
				       };
			}
		}

		public static IEnumerable<SelectListItem> AndOrList
		{
			get
			{
				return new List<SelectListItem>
				       {
				       	{new SelectListItem{Text = "And", Value = "And"}},
				       	{new SelectListItem{Text = "Or", Value = "Or"}}
				       };
			}
		}

		public static IEnumerable<SelectListItem> ScheduleType
		{
			get
			{
				return new List<SelectListItem>
				       {
				       	{new SelectListItem{Text = "Once", Value = "Once"}},
				       	{new SelectListItem{Text = "Daily", Value = "Daily"}},
								{new SelectListItem{Text = "Weekly", Value = "Weekly"}},
				       };
			}
		}

	}

}
