﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using Nop.Plugin.EmailMarketing.Models;
using Nop.Admin.Models.Messages;
using Nop.Plugin.EmailMarketing.Services;
using Nop.Plugin.EmailMarketing;
using Nop.Plugin.EmailMarketing.Domain;
using Nop.Services.Messages;
using Nop.Core.Domain;
using Nop.Core;
using Nop.Core.Domain.Messages;
using System.Threading.Tasks;

namespace Nop.Plugin.EmailMarketing.Lib
{
    public class ProcessCampaignFacade
    {
        private int toEmailTrack=0;

        private readonly ICampaignQueueService _campaignQueueService;
        private readonly ISmartGroupsService _smartGroupsService;
        private readonly IEmailService _emailService;
        private readonly ILinkService _linkService;
        private readonly ITokenizer _tokenizer;
        private readonly IScheduleService _scheduleService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IStoreContext _storeContext;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IWorkContext _workContext;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;



        public ProcessCampaignFacade(ICampaignQueueService campaignQueueService,
            IMessageTemplateService messageTemplateService,
            StoreInformationSettings storeInformationSettings,
            IStoreContext storeContext, ISmartGroupsService smartGroupsService,
            IEmailService emailService,
            IMessageTokenProvider messageTokenProvider,
            ILinkService linkService,
            IEmailAccountService emailAccountService,
            IWorkContext workContext,
            EmailAccountSettings emailAccountSettings,
            ITokenizer tokenizer,
            IScheduleService scheduleService)
        {
            this._campaignQueueService = campaignQueueService;
            this._emailService = emailService;
            this._smartGroupsService = smartGroupsService;
            this._linkService = linkService;
            this._tokenizer = tokenizer;
            this._messageTokenProvider = messageTokenProvider;
            this._scheduleService = scheduleService;
            _storeInformationSettings = storeInformationSettings;
            _storeContext = storeContext;
            _messageTemplateService = messageTemplateService;
            _workContext = workContext;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
        }


        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Processes the campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <param name="storeUrl">The store URL.</param>
        public async Task ProcessCampaign(EmailCampaignModel campaign, string storeUrl)
        {
            var queuedCampaign = this.GetQueuedCampaign(campaign);

            string campaignBody = this.InsertCampaignLink(campaign.Body, queuedCampaign.Id);

            //if (!string.IsNullOrEmpty(campaign.To) || !string.IsNullOrEmpty(campaign.Cc) || !string.IsNullOrEmpty(campaign.Bcc))
            //{
            //    await this.ProcessToCcBcc(queuedCampaign, campaign, campaignBody, storeUrl);
            //}

            if (!string.IsNullOrEmpty(campaign.GroupName))
            {
                string[] groupNames = campaign.GroupName.Split(',');

                foreach (var groupName in groupNames)
                {
                    campaign.GroupName = groupName;
                    var campaignContacts = this.GetCampaignContacts(queuedCampaign, campaign);
                    if (campaignContacts.Count > 0)
                    {
                        foreach (SmartContactModel contact in campaignContacts)
                        {
                            var trackedEmail = this.TrackEmail(contact, queuedCampaign, campaign);

                            var tokens = new List<Token>();
                            tokens.Add(new Token("Customer.FirstName", contact.FirstName));
                            tokens.Add(new Token("Customer.LastName", contact.LastName));
                            tokens.Add(new Token("Customer.Username", contact.UserName));
                            tokens.Add(new Token("Customer.Email", contact.Email));
                            tokens.Add(new Token("Customer.FullName", contact.FirstName + " " + contact.LastName));

                            string replacedCampaignBody = (tokens != null) ? _tokenizer.Replace(campaignBody, tokens, true) : campaignBody;

                            campaign = this.ReplaceLink(campaign, trackedEmail, storeUrl, replacedCampaignBody);
                            await this.SendEmail(trackedEmail, queuedCampaign, campaign);

                            toEmailTrack = 1;
                        }
                    }
                }
            }
            else
            {
                    if (!string.IsNullOrEmpty(campaign.To))
                    {
                        string[] recepients = campaign.To.Split(',');
                        var trackedEmail = new EmailModel();
                        foreach (string recepient in recepients)
                        {
                            trackedEmail = this.TrackEmail(recepient, queuedCampaign, campaign);
                            campaign = this.ReplaceLink(campaign, trackedEmail, storeUrl, campaignBody);

                        }
                        await _campaignQueueService.SendEmailCampaign(trackedEmail, campaign);
                    }
            }
            this.UpdateCampaignQue(queuedCampaign);
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Processes to cc BCC.
        /// </summary>
        /// <param name="campaignQueue">The campaign queue.</param>
        /// <param name="campaign">The campaign.</param>
        /// <param name="campaignBody">The campaign body.</param>
        /// <param name="storeUrl">The store URL.</param>
        public async Task ProcessToCcBcc(CampaignQueue campaignQueue, EmailCampaignModel campaign, string campaignBody, string storeUrl)
        {
            if (!string.IsNullOrEmpty(campaign.To))
            {
                string[] recepients = campaign.To.Split(',');

                foreach (string recepient in recepients)
                {
                    var trackedEmail = this.TrackEmail(recepient, campaignQueue, campaign);
                    campaign = this.ReplaceLink(campaign, trackedEmail, storeUrl, campaignBody);
                    await this.SendEmail(trackedEmail, campaignQueue, campaign);
                }
            }

            if (!string.IsNullOrEmpty(campaign.Cc))
            {
                string[] recepientsCc = campaign.Cc.Split(',');

                foreach (string recepientCc in recepientsCc)
                {
                    var trackedEmail = this.TrackEmail(recepientCc, campaignQueue, campaign);
                    campaign = this.ReplaceLink(campaign, trackedEmail, storeUrl, campaignBody);
                    await this.SendEmail(trackedEmail, campaignQueue, campaign);
                }
            }

            if (!string.IsNullOrEmpty(campaign.Bcc))
            {
                string[] recepientsBcc = campaign.Bcc.Split(',');

                foreach (string recepientBcc in recepientsBcc)
                {
                    var trackedEmail = this.TrackEmail(recepientBcc, campaignQueue, campaign);
                    campaign = this.ReplaceLink(campaign, trackedEmail, storeUrl, campaignBody);
                    await this.SendEmail(trackedEmail, campaignQueue, campaign);
                }
            }
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the queued campaign.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        public CampaignQueue GetQueuedCampaign(EmailCampaignModel campaign)
        {
            CampaignQueueModel campaignQueue = new CampaignQueueModel();

            campaignQueue.BroadCastTime = DateTime.UtcNow;
            campaignQueue.CampaignId = campaign.Id;
            campaignQueue.Delivered = 0;

            return _campaignQueueService.InsertCampaignQueue(campaignQueue.ToCampaignQueueEntity());
        }


        protected virtual MessageTemplate GetActiveMessageTemplate(string messageTemplateName, int storeId)
        {
            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, storeId);

            //no template found
            if (messageTemplate == null)
                return null;

            //ensure it's active
            var isActive = messageTemplate.IsActive;
            if (!isActive)
                return null;

            return messageTemplate;
        }



        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Inserts the campaign link.
        /// </summary>
        /// <param name="campaignBody">The campaign body.</param>
        /// <param name="campaignId">The campaign id.</param>
        /// <returns></returns>
        /// 
        public string InsertCampaignLink(string campaignBody, int campaignId)
        {
            var tokens = new List<Token>();
            _messageTokenProvider.AddStoreTokens(tokens);
            /*var customer = _customerService.GetCustomerByEmail(email);
            if(customer != null)
                _messageTokenProvider.AddCustomerTokens(tokens, customer);
             tokens.Add(new Token("Store.Name", _storeSettings.StoreName));*/

            campaignBody = (tokens != null) ? _tokenizer.Replace(campaignBody, tokens, true) : campaignBody;


            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(campaignBody);
            HtmlNodeCollection links = doc.DocumentNode.SelectNodes("//a[@href]");

            if (links != null)
            {
                foreach (HtmlNode link in links)
                {
                    HtmlAttribute href = link.Attributes["href"];

                    Link linkURL = new Link();
                    linkURL.CampaignId = campaignId;
                    linkURL.LinkHref = href.Value;
                    linkURL.Click = 0;

                    var insertedLink = _linkService.InsertLink(linkURL);

                    href.Value = String.Format("?linkId={0}", insertedLink.Id);
                }
            }

            return doc.DocumentNode.InnerHtml;

        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the campaign contacts.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public List<SmartContactModel> GetCampaignContacts(CampaignQueue campaign, EmailCampaignModel model)
        {
            var smartGroup = _smartGroupsService.GetSmartGroupByName(model.GroupName);

            var smartContacts = (smartGroup == null) ? new List<SmartContactModel>() : _smartGroupsService.GetAllContactsAtOnce(smartGroup.Id).ToList();

            /*if(!string.IsNullOrEmpty(model.To))
            {
                var emails = model.To.Split(',');
                foreach(var email in emails)
                {
                    SmartContactModel emailContact = new SmartContactModel();
                    emailContact.Email = email;
                    smartContacts.Add(emailContact);
                }
            }*/

            return smartContacts;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Tracks the email.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <param name="queuedCampaign">The queued campaign.</param>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        public EmailModel TrackEmail(SmartContactModel contact, CampaignQueue queuedCampaign, EmailCampaignModel campaign)
        {
            Email email = new Email();

            email.CampaignId = queuedCampaign.Id;
            email.EmailAddress = contact.Email;

            var insertedEmail = _emailService.InsertEmail(email);

            return insertedEmail.ToEmailModel();
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Tracks the email.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <param name="queuedCampaign">The queued campaign.</param>
        /// <param name="campaign">The campaign.</param>
        /// <returns></returns>
        public EmailModel TrackEmail(string contact, CampaignQueue queuedCampaign, EmailCampaignModel campaign)
        {
            Email email = new Email();

            email.CampaignId = queuedCampaign.Id;
            email.EmailAddress = contact;

            var insertedEmail = _emailService.InsertEmail(email);

            return insertedEmail.ToEmailModel();
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Replaces the link.
        /// </summary>
        /// <param name="campaign">The campaign.</param>
        /// <param name="email">The email.</param>
        /// <param name="baseUrl">The base URL.</param>
        /// <param name="campaignBody">The campaign body.</param>
        /// <returns></returns>
        public EmailCampaignModel ReplaceLink(EmailCampaignModel campaign, EmailModel email, string baseUrl, string campaignBody)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(campaignBody);
            HtmlNodeCollection links = doc.DocumentNode.SelectNodes("//a[@href]");
            //baseUrl = "http://localhost:15555/";
            if (links != null)
            {
                foreach (HtmlNode link in links)
                {
                    HtmlAttribute href = link.Attributes["href"];

                    href.Value = String.Format("{0}Plugin/EmailMarketing/LinkClick{1}&emailId={2}&campaignId={3}", baseUrl, href.Value, email.Id, email.CampaignId);
                }
            }


            //Insert an image to calculate email open
            var node = HtmlNode.CreateNode(string.Format("<img src='{0}Plugin/EmailMarketing/EmailOpen?emailId={1}&campaignId={2}' alt='emailopen' height='1px' width='1px'/>", baseUrl, email.Id, email.CampaignId));
            doc.DocumentNode.AppendChild(node);

            campaign.Body = doc.DocumentNode.InnerHtml;

            return campaign;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="queuedCampaign">The queued campaign.</param>
        /// <param name="campaign">The campaign.</param>
        public async Task SendEmail(EmailModel email, CampaignQueue queuedCampaign, EmailCampaignModel campaign)
        {
            var emailEntity = _emailService.GetEmailById(email.Id);
            try
            {
                if (toEmailTrack > 0)
                {
                    campaign.To = null;
                    campaign.Cc = null;
                    campaign.Bcc = null;
                }
                await _campaignQueueService.SendEmailCampaign(email, campaign);
                emailEntity.SentStatus = "sent";
                _emailService.UpdateEmail(emailEntity);

                queuedCampaign.Delivered = queuedCampaign.Delivered + 1;
                _campaignQueueService.UpdateCampaignQueue(queuedCampaign);
            }
            catch (Exception ex)
            {
                emailEntity.SentStatus = ex.Message;
                _emailService.UpdateEmail(emailEntity);

            }
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Updates the campaign que.
        /// </summary>
        /// <param name="queuedCampaign">The queued campaign.</param>
        public void UpdateCampaignQue(CampaignQueue queuedCampaign)
        {
            queuedCampaign.EmailInsertStatus = "Complete";
            //queuedCampaign.Delivered = _emailService.GetSentEmailByCampaignId(queuedCampaign.CampaignId);
            _campaignQueueService.UpdateCampaignQueue(queuedCampaign);
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Updates the scheduled campaign status.
        /// </summary>
        /// <param name="id">The id.</param>
        public void UpdateScheduledCampaignStatus(int id)
        {
            var sentScheduledCampaign = _scheduleService.GetScheduleById(id);
            sentScheduledCampaign.Status = true;
            _scheduleService.UpdateSchedule(sentScheduledCampaign);
        }


    }
}


