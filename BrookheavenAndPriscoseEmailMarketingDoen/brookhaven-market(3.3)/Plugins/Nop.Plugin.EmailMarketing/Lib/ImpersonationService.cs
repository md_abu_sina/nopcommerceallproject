﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.IO;

namespace Nop.Plugin.EmailMarketing.Lib
{
	class ImpersonationService
	{
		[DllImport("advapi32.dll")]
		public static extern int LogonUser(String lpszUsername, String lpszDomain,
		String lpszPassword,
		int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

		[DllImport("kernel32.dll")]
		public extern static bool CloseHandle(IntPtr hToken);

		public string ParseAccountingFile()
		{
			try
			{
				if(this.Impersonate(@"3nerds.net\brainstation-23", "3nerds.net", "d3v3l0pm3nt~"))
				{
					// Your business logic here
					var serverPath = @"\\mta1\MTA-Logs\bounce-acct";
					var dirInfo = new DirectoryInfo(serverPath);

					//_logger.Error(dirInfo.Exists.ToString());

					this.impersonationContext.Undo();
					return dirInfo.Exists.ToString();
				}
				else
				{
					//logging errors
					return "Network shared folder not found";
				}
			}
			catch(Exception ex)
			{
				// logging errors
				return ex.ToString();
			}
		}

		public bool Impersonate(string userName, string domain, string password)
		{
			WindowsIdentity tempWindowsIdentity;
			IntPtr token = IntPtr.Zero;
			IntPtr tokenDuplicate = IntPtr.Zero;
			// request default security provider a logon token with
			//LOGON32_LOGON_NEW_CREDENTIALS,
			// token returned is impersonation token, no need to duplicate
			if(LogonUser(userName, domain, password, 9, 0, ref token) != 0)
			{
				tempWindowsIdentity = new WindowsIdentity(token);
				impersonationContext = tempWindowsIdentity.Impersonate();
				// close impersonation token, no longer needed
				CloseHandle(token);
				if(impersonationContext != null)
					return true;
			}
			return false; // Failed to impersonate.
		}

		WindowsImpersonationContext impersonationContext;

		///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Undoes the impersonation context.
		/// </summary>
		public void UndoImpersonationContext()
		{
			impersonationContext.Undo();
		}
	}
}
