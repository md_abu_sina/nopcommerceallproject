﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Plugin.OrderForm.Models;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;
using Nop.Plugin.OrderForm.Models;

namespace Nop.Plugin.OrderForm.Controllers
{
    public class OrderFormController : BasePluginController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly PdfSettings _pdfSettings;
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IStoreContext _storeContext;
        private readonly ILogger _logger;

        #region email fields


        private readonly ITokenizer _tokenizer;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailSender _emailSender;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly ILanguageService _languageService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IMessageTokenProvider _messageTokenProvider;
        // private readonly IMessageTemplateForUponService _messageTemplateForUponService;
        private readonly IRepository<MessageTemplate> _messageTemplateRepository;
        private readonly IRepository<EmailAccount> _emailAccountRepository;
        //private readonly IEventPublisher _eventPublisher;

        #endregion



        #endregion

        #region ctor

        public OrderFormController(IWorkContext workContext, ITokenizer tokenizer, IQueuedEmailService queuedEmailService, IMessageTemplateService messageTemplateService, StoreInformationSettings storeInformationSettings,
                                                                ILanguageService languageService, IEmailAccountService emailAccountService, EmailAccountSettings emailAccountSettings, IEmailSender emailSender,
                                                                IMessageTokenProvider messageTokenProvider, RewardPointsSettings rewardPointsSettings, CustomerSettings customerSettings, ILogger logger,
                                                                ForumSettings forumSettings, OrderSettings orderSettings, IAddressService addressService, IOrderService orderService, PdfSettings pdfSettings,
                                                                                                                                IRepository<MessageTemplate> messageTemplateRepository, IRepository<EmailAccount> emailAccountRepository, IStoreContext storeContext)
        {
            _workContext = workContext;
            _pdfSettings = pdfSettings;
            _storeInformationSettings = storeInformationSettings;
            _emailSender = emailSender;
            _tokenizer = tokenizer;
            _queuedEmailService = queuedEmailService;
            _messageTemplateService = messageTemplateService;
            _languageService = languageService;
            _emailAccountService = emailAccountService;
            _emailAccountSettings = emailAccountSettings;
            _messageTokenProvider = messageTokenProvider;
            //  _messageTemplateForUponService = messageTemplateForUponService;
            _messageTemplateRepository = messageTemplateRepository;
            _emailAccountRepository = emailAccountRepository;
            _storeContext = storeContext;
            _logger = logger;
        }

        #endregion

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Shows the form.
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowForm()
        {
            var model = new OrderFormModel();

            model.DbSize.Add(new SelectListItem() { Text = "10-11 lb.", Value = "10-11 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "12-13 lb.", Value = "12-13 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "14-15 lb.", Value = "14-15 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "16-17 lb.", Value = "16-17 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "18-19 lb.", Value = "18-19 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "20-21 lb.", Value = "20-21 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "22-23 lb.", Value = "22-23 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "24-25 lb.", Value = "24-25 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "26-27 lb.", Value = "26-27 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "28-29 lb.", Value = "28-29 lb." });
            model.DbSize.Add(new SelectListItem() { Text = "30+ lb.", Value = "30+ lb." });

            var addresses = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Email == _workContext.CurrentCustomer.Email);

            model.FirstName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
            model.LastName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
            model.Email = _workContext.CurrentCustomer.Email;
            if (addresses == null)
            {
                model.Cell = _workContext.CurrentCustomer.Addresses.Count>0? _workContext.CurrentCustomer.Addresses.FirstOrDefault().PhoneNumber : "";
            }
            else
            {
                model.Cell = addresses.PhoneNumber;
            }
            model.OrderPicupDate = DateTime.Now.ToShortDateString();

            return View("OrderForm", model);
        }



        #region Email

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Submits the form.
        /// </summary>
        /// <param name="orderFormModel">The employment model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitOrderForm(OrderFormModel orderFormModel)
        {
            orderFormModel.StoreName = _storeContext.CurrentStore.Name;

            var languageId = EnsureLanguageIsActive(_workContext.WorkingLanguage.Id);

            var messageTemplate = GetLocalizedActiveMessageTemplate("HolyDay.Order", languageId);

            var randomNumber = Guid.NewGuid();
            string fileName = string.Format("orderform_{0}-{1}_" + randomNumber + ".html", orderFormModel.FirstName, orderFormModel.LastName);//employmentform_Andrea-Montoya_485112
            string filePath = System.IO.Path.Combine(this.Request.PhysicalApplicationPath, "content\\OrderItem", fileName);

            //var emailBody = GetEmailBody(messageTemplate);

            //var tokens = GenerateTokens(employmentModel);
            //var bodyReplaced = _tokenizer.Replace(emailBody, tokens, true);

            var bodyReplaced = RenderPartialViewToString(this, "Attached", orderFormModel);


            OrderFormhtml.ConvertToHtml(bodyReplaced, filePath);

            //EmploymentPdf.PrintToPdf(employmentModel, _workContext.WorkingLanguage, bodyReplaced, filePath, _pdfSettings, this.Request.PhysicalApplicationPath);
            try
            {
                SendEmail(_workContext.WorkingLanguage.Id, orderFormModel, filePath);
                return Redirect(_storeContext.CurrentStore.Url + "t/HolyDayOrderCompletion");
                //return("Email send successfuly");
            }

            catch (Exception ex)
            {
                //string errorText = ex.ToString().Substring(0, Math.Min(ex.ToString().Length, 200));
                _logger.Error(ex.ToString());
                return Content(ex.ToString());
                //return Redirect(_storeContext.CurrentStore.Url + "t/EmploymentApplicationCompletion");
                //throw new Exception(ex); //wex.ToString();
                //return Redirect("http://www.brookhavenmarket.com/t/EmploymentApplicationCompletion");
            }
            //return View("EmploymentForm");
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Renders the partial view to string.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static string RenderPartialViewToString(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Installs the employment message templates.
        /// </summary>
        public virtual void installOrderFormMessageTemplates()
        {

            var eaGeneral = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Holiday Order")).FirstOrDefault();
            //var eaSale = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Sales representative")).FirstOrDefault();
            //var eaCustomer = _emailAccountRepository.Table.Where(ea => ea.DisplayName.Equals("Customer support")).FirstOrDefault();
            var messageTemplates = new List<MessageTemplate>
                               {
                                   new MessageTemplate
                                       {
                                           Name = "HoliDay.Order",
                                           Subject = "%EmailCv.FirstName%. Holiday Order .",
                                           Body = "<p>An email sent \"%EmailCv.FirstName%\".</p>",
                                           IsActive = true,
                                           EmailAccountId = eaGeneral.Id,
                                       },
                                  
                                  
                               };
            messageTemplates.ForEach(mt => _messageTemplateRepository.Insert(mt));
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Adds the email cv tokens.
        /// </summary>
        /// <param name="tokens">The tokens.</param>
        /// <param name="orderFormModel">The employment model.</param>
        public virtual void AddEmailCvTokens(IList<Token> tokens, OrderFormModel orderFormModel)
        {
            //personal info
            tokens.Add(new Token("EmailCv.FirstName", orderFormModel.FirstName));
            tokens.Add(new Token("EmailCv.LastName", orderFormModel.LastName));
            tokens.Add(new Token("EmailCv.Cell", orderFormModel.Cell));
            tokens.Add(new Token("EmailCv.Email", orderFormModel.Email));
            tokens.Add(new Token("EmailCv.OrderPicupDate", orderFormModel.OrderPicupDate));
            tokens.Add(new Token("EmailCv.SizeTurkey", orderFormModel.SizeTurkey));

            tokens.Add(new Token("EmailCv.PumpkinPiePrice", orderFormModel.PumpkinPiePrice));
            tokens.Add(new Token("EmailCv.PumpkinPieQuantity", orderFormModel.PumpkinPieQuantity));

            tokens.Add(new Token("EmailCv.PumpkinPiePriceHalf", orderFormModel.PumpkinPiePriceHalf));
            tokens.Add(new Token("EmailCv.PumpkinPieQuantityHalf", orderFormModel.PumpkinPieQuantityHalf));


            tokens.Add(new Token("EmailCv.PecanPiePrice", orderFormModel.PecanPiePrice));
            tokens.Add(new Token("EmailCv.PecanPieQuantity", orderFormModel.PecanPieQuantity));

            tokens.Add(new Token("EmailCv.PecanPiePriceHalf", orderFormModel.PecanPiePriceHalf));
            tokens.Add(new Token("EmailCv.PecanPieQuantityHalf", orderFormModel.PecanPieQuantityHalf));

            tokens.Add(new Token("EmailCv.TraditionalApplePrice", orderFormModel.TraditionalApplePrice));
            tokens.Add(new Token("EmailCv.TraditionalApplePieQuantity", orderFormModel.TraditionalApplePieQuantity));

            tokens.Add(new Token("EmailCv.TraditionalApplePriceHalf", orderFormModel.TraditionalApplePriceHalf));
            tokens.Add(new Token("EmailCv.TraditionalApplePieQuantityHalf", orderFormModel.TraditionalApplePieQuantityHalf));

            tokens.Add(new Token("EmailCv.AppleBerryDelightPrice", orderFormModel.AppleBerryDelightPrice));
            tokens.Add(new Token("EmailCv.AppleBerryDelightQuantity", orderFormModel.AppleBerryDelightQuantity));

            tokens.Add(new Token("EmailCv.AppleBerryDelightPriceHalf", orderFormModel.AppleBerryDelightPriceHalf));
            tokens.Add(new Token("EmailCv.AppleBerryDelightQuantityHalf", orderFormModel.AppleBerryDelightQuantityHalf));

            tokens.Add(new Token("EmailCv.AppleBrownBettyPiePrice", orderFormModel.AppleBrownBettyPiePrice));
            tokens.Add(new Token("EmailCv.AppleBrownBettyPieQuantity", orderFormModel.AppleBrownBettyPieQuantity));

            tokens.Add(new Token("EmailCv.AppleBrownBettyPiePriceHalf", orderFormModel.AppleBrownBettyPiePriceHalf));
            tokens.Add(new Token("EmailCv.AppleBrownBettyPieQuantityHalf", orderFormModel.AppleBrownBettyPieQuantityHalf));

            tokens.Add(new Token("EmailCv.AppleCaramelWalnutPiePrice", orderFormModel.AppleCaramelWalnutPiePrice));
            tokens.Add(new Token("EmailCv.AppleCaramelWalnutPieQuantity", orderFormModel.AppleCaramelWalnutPieQuantity));

            tokens.Add(new Token("EmailCv.AppleCaramelWalnutPiePriceHalf", orderFormModel.AppleCaramelWalnutPiePriceHalf));
            tokens.Add(new Token("EmailCv.AppleCaramelWalnutPieQuantityHalf", orderFormModel.AppleCaramelWalnutPieQuantityHalf));

            tokens.Add(new Token("EmailCv.AppleCrispPiePrice", orderFormModel.AppleCrispPiePrice));
            tokens.Add(new Token("EmailCv.AppleCrispPieQuantity", orderFormModel.AppleCrispPieQuantity));

            tokens.Add(new Token("EmailCv.AppleCrispPiePriceHalf", orderFormModel.AppleCrispPiePriceHalf));
            tokens.Add(new Token("EmailCv.AppleCrispPieQuantityHalf", orderFormModel.AppleCrispPieQuantityHalf));

            tokens.Add(new Token("EmailCv.AppleRaspberryPiePrice", orderFormModel.AppleRaspberryPiePrice));
            tokens.Add(new Token("EmailCv.AppleRaspberryPieQuantity", orderFormModel.AppleRaspberryPieQuantity));

            tokens.Add(new Token("EmailCv.AppleRaspberryPiePriceHalf", orderFormModel.AppleRaspberryPiePriceHalf));
            tokens.Add(new Token("EmailCv.AppleRaspberryPieQuantityHalf", orderFormModel.AppleRaspberryPieQuantityHalf));

            tokens.Add(new Token("EmailCv.CountryCinnamonApplePiePrice", orderFormModel.CountryCinnamonApplePiePrice));
            tokens.Add(new Token("EmailCv.CountryCinnamonApplePieQuantity", orderFormModel.CountryCinnamonApplePieQuantity));

            tokens.Add(new Token("EmailCv.CountryCinnamonApplePiePriceHalf", orderFormModel.CountryCinnamonApplePiePriceHalf));
            tokens.Add(new Token("EmailCv.CountryCinnamonApplePieQuantityHalf", orderFormModel.CountryCinnamonApplePieQuantityHalf));

            tokens.Add(new Token("EmailCv.DutchApplePiePrice", orderFormModel.DutchApplePiePrice));
            tokens.Add(new Token("EmailCv.DutchApplePieQuantity", orderFormModel.DutchApplePieQuantity));

            tokens.Add(new Token("EmailCv.DutchApplePiePriceHalf", orderFormModel.DutchApplePiePriceHalf));
            tokens.Add(new Token("EmailCv.DutchApplePieQuantityHalf", orderFormModel.DutchApplePieQuantityHalf));

            tokens.Add(new Token("EmailCv.BlackberryPiePrice", orderFormModel.BlackberryPiePrice));
            tokens.Add(new Token("EmailCv.BlackberryPieQuantity", orderFormModel.BlackberryPieQuantity));


            tokens.Add(new Token("EmailCv.BlackberryPiePriceHalf", orderFormModel.BlackberryPiePriceHalf));
            tokens.Add(new Token("EmailCv.BlackberryPieQuantityHalf", orderFormModel.BlackberryPieQuantityHalf));

            tokens.Add(new Token("EmailCv.TraditionalBlueberryPiePrice", orderFormModel.TraditionalBlueberryPiePrice));
            tokens.Add(new Token("EmailCv.TraditionalBlueberryPieQuantity", orderFormModel.TraditionalBlueberryPieQuantity));

            tokens.Add(new Token("EmailCv.TraditionalBlueberryPiePriceHalf", orderFormModel.TraditionalBlueberryPiePriceHalf));
            tokens.Add(new Token("EmailCv.TraditionalBlueberryPieQuantityHalf", orderFormModel.TraditionalBlueberryPieQuantityHalf));

            tokens.Add(new Token("EmailCv.TraditionalCherryPiePrice", orderFormModel.TraditionalCherryPiePrice));
            tokens.Add(new Token("EmailCv.TraditionalCherryPieQuantity", orderFormModel.TraditionalCherryPieQuantity));

            tokens.Add(new Token("EmailCv.TraditionalCherryPiePriceHalf", orderFormModel.TraditionalCherryPiePriceHalf));
            tokens.Add(new Token("EmailCv.TraditionalCherryPieQuantityHalf", orderFormModel.TraditionalCherryPieQuantityHalf));

            tokens.Add(new Token("EmailCv.BlackCherryPiePrice", orderFormModel.BlackCherryPiePrice));
            tokens.Add(new Token("EmailCv.BlackCherryPieQuantity", orderFormModel.BlackCherryPieQuantity));

            tokens.Add(new Token("EmailCv.BlackCherryPiePriceHalf", orderFormModel.BlackCherryPiePriceHalf));
            tokens.Add(new Token("EmailCv.BlackCherryPieQuantityHalf", orderFormModel.BlackCherryPieQuantityHalf));

            tokens.Add(new Token("EmailCv.CherryCrumbPiePrice", orderFormModel.CherryCrumbPiePrice));
            tokens.Add(new Token("EmailCv.CherryCrumbPieQuantity", orderFormModel.CherryCrumbPieQuantity));

            tokens.Add(new Token("EmailCv.CherryCrumbPiePriceHalf", orderFormModel.CherryCrumbPiePriceHalf));
            tokens.Add(new Token("EmailCv.CherryCrumbPieQuantityHalf", orderFormModel.CherryCrumbPieQuantityHalf));

            tokens.Add(new Token("EmailCv.CherryVanillawithAlmondsPiePrice", orderFormModel.CherryVanillawithAlmondsPiePrice));
            tokens.Add(new Token("EmailCv.CherryVanillawithAlmondsPieQuantity", orderFormModel.CherryVanillawithAlmondsPieQuantity));

            tokens.Add(new Token("EmailCv.CherryVanillawithAlmondsPiePriceHalf", orderFormModel.CherryVanillawithAlmondsPiePriceHalf));
            tokens.Add(new Token("EmailCv.CherryVanillawithAlmondsPieQuantityHalf", orderFormModel.CherryVanillawithAlmondsPieQuantityHalf));

            tokens.Add(new Token("EmailCv.TraditionalPeachPiePrice", orderFormModel.TraditionalPeachPiePrice));
            tokens.Add(new Token("EmailCv.TraditionalPeachPieQuantity", orderFormModel.TraditionalPeachPieQuantity));

            tokens.Add(new Token("EmailCv.TraditionalPeachPiePriceHalf", orderFormModel.TraditionalPeachPiePriceHalf));
            tokens.Add(new Token("EmailCv.TraditionalPeachPieQuantityHalf", orderFormModel.TraditionalPeachPieQuantityHalf));

            tokens.Add(new Token("EmailCv.MixedPeachandBlueberryPiePrice", orderFormModel.MixedPeachandBlueberryPiePrice));
            tokens.Add(new Token("EmailCv.MixedPeachandBlueberryPieQuantity", orderFormModel.MixedPeachandBlueberryPieQuantity));

            tokens.Add(new Token("EmailCv.MixedPeachandBlueberryPiePriceHalf", orderFormModel.MixedPeachandBlueberryPiePriceHalf));
            tokens.Add(new Token("EmailCv.MixedPeachandBlueberryPieQuantityHalf", orderFormModel.MixedPeachandBlueberryPieQuantityHalf));

            tokens.Add(new Token("EmailCv.PeachPralinePiePrice", orderFormModel.PeachPralinePiePrice));
            tokens.Add(new Token("EmailCv.PeachPralinePieQuantity", orderFormModel.PeachPralinePieQuantity));

            tokens.Add(new Token("EmailCv.PeachPralinePiePriceHalf", orderFormModel.PeachPralinePiePriceHalf));
            tokens.Add(new Token("EmailCv.PeachPralinePieQuantityHalf", orderFormModel.PeachPralinePieQuantityHalf));

            tokens.Add(new Token("EmailCv.PineappleUpsideDownPiePrice", orderFormModel.PineappleUpsideDownPiePrice));
            tokens.Add(new Token("EmailCv.PineappleUpsideDownPieQuantity", orderFormModel.PineappleUpsideDownPieQuantity));

            tokens.Add(new Token("EmailCv.PineappleUpsideDownPiePriceHalf", orderFormModel.PineappleUpsideDownPiePriceHalf));
            tokens.Add(new Token("EmailCv.PineappleUpsideDownPieQuantityHalf", orderFormModel.PineappleUpsideDownPieQuantityHalf));

            tokens.Add(new Token("EmailCv.StrawberryPiePrice", orderFormModel.StrawberryPiePrice));
            tokens.Add(new Token("EmailCv.StrawberryPieQuantity", orderFormModel.StrawberryPieQuantity));

            tokens.Add(new Token("EmailCv.StrawberryPiePriceHalf", orderFormModel.StrawberryPiePriceHalf));
            tokens.Add(new Token("EmailCv.StrawberryPieQuantityHalf", orderFormModel.StrawberryPieQuantityHalf));

            tokens.Add(new Token("EmailCv.StrawberryRhubarbPiePrice", orderFormModel.StrawberryRhubarbPiePrice));
            tokens.Add(new Token("EmailCv.StrawberryRhubarbPieQuantity", orderFormModel.StrawberryRhubarbPieQuantity));

            tokens.Add(new Token("EmailCv.StrawberryRhubarbPiePriceHalf", orderFormModel.StrawberryRhubarbPiePriceHalf));
            tokens.Add(new Token("EmailCv.StrawberryRhubarbPieQuantityHalf", orderFormModel.StrawberryRhubarbPieQuantityHalf));

            tokens.Add(new Token("EmailCv.VeryBerryPiePrice", orderFormModel.VeryBerryPiePrice));
            tokens.Add(new Token("EmailCv.VeryBerryPieQuantity", orderFormModel.VeryBerryPieQuantity));

            tokens.Add(new Token("EmailCv.VeryBerryPiePriceHalf", orderFormModel.VeryBerryPiePriceHalf));
            tokens.Add(new Token("EmailCv.VeryBerryPieQuantityHalf", orderFormModel.VeryBerryPieQuantityHalf));


            tokens.Add(new Token("EmailCv.PieSize", orderFormModel.PieSize));

            tokens.Add(new Token("EmailCv.Specialinstructions", orderFormModel.Specialinstructions));


            //event notification
            //_eventPublisher.TokensAdded(employmentModel, tokens);
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Generates the tokens.
        /// </summary>
        /// <param name="employmentModel">The employment model.</param>
        /// <returns></returns>
        private IList<Token> GenerateTokens(OrderFormModel employmentModel)
        {
            var tokens = new List<Token>();
            //_messageTokenProvider.AddStoreTokens(tokens);
            AddEmailCvTokens(tokens, employmentModel);
            return tokens;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="languageId">The language id.</param>
        /// <param name="employmentModel">The employment model.</param>
        /// <param name="attachedFilePath">The attached file path.</param>
        public virtual void SendEmail(int languageId, OrderFormModel employmentModel, string attachedFilePath)
        {



            languageId = EnsureLanguageIsActive(languageId);

            var messageTemplate = GetLocalizedActiveMessageTemplate("HolyDay.Order", languageId);
            if (messageTemplate == null)
                return;

            // var tokens = new List<Token>();
            var couponTokens = GenerateTokens(employmentModel);

            var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate, languageId);
            var toEmail = emailAccount.DestinationEmail;//"razib@brainstation-23.com";
            var toName = emailAccount.DisplayName;

            /*var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

//Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, couponTokens, false);
            var bodyReplaced = _tokenizer.Replace(body, couponTokens, true);*/


            SendNotification(messageTemplate, emailAccount,
                languageId, couponTokens,
                toEmail, toName, attachedFilePath);
        }



        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the email account of message template.
        /// </summary>
        /// <param name="messageTemplate">The message template.</param>
        /// <param name="languageId">The language id.</param>
        /// <returns></returns>
        private EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
        {
            var emailAccounId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
            var emailAccount = _emailAccountService.GetEmailAccountById(emailAccounId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            if (emailAccount == null)
                emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            return emailAccount;

        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the email body.
        /// </summary>
        /// <param name="messageTemplate">The message template.</param>
        /// <returns></returns>
        private String GetEmailBody(MessageTemplate messageTemplate)
        {
            return messageTemplate.Body;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Generates the tokens.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns></returns>
        private IList<Token> GenerateTokens(Customer customer)
        {
            var tokens = new List<Token>();
            //  _messageTokenProvider.AddStoreTokens(tokens);
            _messageTokenProvider.AddCustomerTokens(tokens, customer);
            return tokens;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Ensures the language is active.
        /// </summary>
        /// <param name="languageId">The language id.</param>
        /// <returns></returns>
        private int EnsureLanguageIsActive(int languageId)
        {
            var language = _languageService.GetLanguageById(languageId);
            if (language == null || !language.Published)
                language = _languageService.GetAllLanguages().FirstOrDefault();
            return language.Id;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the localized active message template.
        /// </summary>
        /// <param name="messageTemplateName">Name of the message template.</param>
        /// <param name="languageId">The language id.</param>
        /// <returns></returns>
        private MessageTemplate GetLocalizedActiveMessageTemplate(string messageTemplateName, int languageId)
        {
            var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, _storeContext.CurrentStore.Id);
            if (messageTemplate == null)
                return null;

            //var isActive = messageTemplate.GetLocalized((mt) => mt.IsActive, languageId);
            //use
            var isActive = messageTemplate.IsActive;
            if (!isActive)
                return null;

            return messageTemplate;
        }

        ///--------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends the notification.
        /// </summary>
        /// <param name="messageTemplate">The message template.</param>
        /// <param name="emailAccount">The email account.</param>
        /// <param name="languageId">The language id.</param>
        /// <param name="tokens">The tokens.</param>
        /// <param name="toEmailAddress">To email address.</param>
        /// <param name="toName">To name.</param>
        /// <param name="attachedFilePath">The attached file path.</param>
        private int SendNotification(MessageTemplate messageTemplate,
             EmailAccount emailAccount, int languageId, IEnumerable<Token> tokens,
             string toEmailAddress, string toName, string attachedFilePath)
        {
            //retrieve localized message template data
            var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
            var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
            var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            var bodyReplaced = _tokenizer.Replace(body, tokens, true);
            //_emailSender.SendAttachedEmail(emailAccount, subjectReplaced, bodyReplaced,
            // emailAccount.Email, emailAccount.Email, toEmailAddress, toName, attachedFilePath);

            var attachmentFilePath = attachedFilePath;
            var attachmentFileName = Path.GetFileName(attachedFilePath);


            var email = new QueuedEmail()
            {
                Priority = 5,
                From = emailAccount.Email,
                FromName = emailAccount.DisplayName,
                To = toEmailAddress,
                ToName = toName,
                CC = string.Empty,
                Bcc = bcc,
                Subject = subjectReplaced,
                Body = bodyReplaced,
                AttachmentFilePath = attachmentFilePath,
                AttachmentFileName = attachmentFileName,
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id
            };
            _queuedEmailService.InsertQueuedEmail(email);



            return email.Id;
        }
        #endregion



    }
}

