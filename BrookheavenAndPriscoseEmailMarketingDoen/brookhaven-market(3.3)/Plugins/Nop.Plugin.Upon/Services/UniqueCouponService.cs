﻿using Nop.Plugin.Upon.Domain;
using Nop.Core.Data;
using System.Collections.Generic;
using System.Linq;
using System;
using Nop.Core.Events;
using Nop.Plugin.Upon.Services;
using Nop.Core;
using System.Globalization;
using Nop.Services.Events;

namespace Nop.Plugin.Upon.Services
{
    public class UniqueCouponService : IUniqueCouponService
    {
        #region fields

        private readonly IRepository<UniqueCoupon> _uniqueCouponRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region ctor

        public UniqueCouponService(IRepository<UniqueCoupon> uniqueCouponRepository, IEventPublisher eventPublisher)
        {
            _uniqueCouponRepository = uniqueCouponRepository;
            _eventPublisher = eventPublisher;
        }

        #endregion

        #region Implementation of IUniqueCouponService

        /// <summary>
        /// get all unique coupons
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<UniqueCoupon></returns>
        public virtual IPagedList<UniqueCoupon> GetAllUniqueCoupons(int pageIndex, int pageSize)
        {
            // var query = _uniqueCouponRepository.Table;
            var query = (from u in _uniqueCouponRepository.Table
                         orderby u.Id
                         select u);
            var uniquecoupons = new PagedList<UniqueCoupon>(query, pageIndex, pageSize);
            return uniquecoupons;
        }

        /// <summary>
        /// get all Available unique coupons
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<UniqueCoupon></returns>
        public virtual IPagedList<UniqueCoupon> GetAllAvailableUniqueCoupons(int pageIndex, int pageSize)
        {
            // var query = _uniqueCouponRepository.Table;
            var query = (from u in _uniqueCouponRepository.Table
                         orderby u.Id
                         where u.StartDate <= DateTime.Now && u.EndDate >= DateTime.Now
                         select u);
            var uniquecoupons = new PagedList<UniqueCoupon>(query, pageIndex, pageSize);
            return uniquecoupons;
        }

        /// <summary>
        /// get all unique coupons
        /// </summary>
       /// <returns>List<UniqueCoupon></returns>
        public virtual List<UniqueCoupon> GetAllUniqueCouponsForUser()
        {
            // var query = _uniqueCouponRepository.Table;
            var uniquecoupons = (from u in _uniqueCouponRepository.Table
                                 orderby u.EndDate
												         where u.StartDate <= DateTime.Now && u.EndDate >= DateTime.Now && u.Active == true//&& u.DateRange>=DateTime.Today
                                 select u).ToList();
           
            return uniquecoupons;
        }

				/// <summary>
				/// Gets the unique coupons for deactivate.
				/// </summary>
				/// <returns></returns>
				public virtual List<UniqueCoupon> GetUniqueCouponsForDeactivate()
				{
					var uniquecoupons = (from u in _uniqueCouponRepository.Table
															 orderby u.Id
															 where u.EndDate < DateTime.Now && u.Active == true//&& u.DateRange>=DateTime.Today
															 select u).ToList();

					return uniquecoupons;
				}

        /// <summary>
        /// insert Unique Coupon
        /// </summary>
        /// <param name="uniquecouponItem"></param>
        public virtual void InsertUniqueCoupon(UniqueCoupon uniquecouponItem)
        {
            if (uniquecouponItem == null)
                throw new ArgumentNullException("uniquecouponItem");

            _uniqueCouponRepository.Insert(uniquecouponItem);

            //event notification
            _eventPublisher.EntityInserted(uniquecouponItem);
        }

        /// <summary>
        /// get Unique Coupon  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UniqueCoupon GetuniquecouponById(int id)
        {
            var db = _uniqueCouponRepository;
            return db.Table.SingleOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Update Unique Coupon
        /// </summary>
        /// <param name="uniquecouponItem"></param>
        public void UpdateUniqueCoupon(UniqueCoupon uniquecouponItem)
        {
            if (uniquecouponItem == null)
                throw new ArgumentNullException("uniquecouponItem");

            _uniqueCouponRepository.Update(uniquecouponItem);

            //event notification
            _eventPublisher.EntityUpdated(uniquecouponItem);
        }

        /// <summary>
        /// delete Unique Coupon
        /// </summary>
        /// <param name="uniquecouponItem"></param>
        public void DeleteUniqueCoupon(UniqueCoupon uniquecouponItem)
        {
            _uniqueCouponRepository.Delete(uniquecouponItem);
            _eventPublisher.EntityDeleted(uniquecouponItem);
        }


        #endregion
    }
}
