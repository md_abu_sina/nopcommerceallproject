﻿using AutoMapper;


using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Messages;
using Nop.Services.Payments;
//using Nop.Services.PromotionFeed;
using Nop.Services.Shipping;
using Nop.Services.Tax;

using Nop.Plugin.Upon.Domain;
using Nop.Plugin.Upon.Models;
using Nop.Core;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Upon
{
    public static class MappingExtensions
    {


        #region Unique Coupons

        //Unique Coupons
        public static UniqueCouponModel ToModel(this UniqueCoupon entity)
        {
            UniqueCouponModel uniquecouponmodel = new UniqueCouponModel();
            uniquecouponmodel.Id = entity.Id;
            uniquecouponmodel.Title = entity.Title;
            uniquecouponmodel.Offer = entity.Offer;
            uniquecouponmodel.Restrictions = entity.Restrictions;
            uniquecouponmodel.Description = entity.Description;
            uniquecouponmodel.Quantity = entity.Quantity;
            uniquecouponmodel.DateRange = entity.DateRange;
            uniquecouponmodel.CouponImageForWebsite = entity.CouponImageForWebsite;
            uniquecouponmodel.BannerCouponImage = entity.BannerCouponImage;
            uniquecouponmodel.StandardCouponImage = entity.StandardCouponImage;
            uniquecouponmodel.Disclaimer = entity.Disclaimer;
            uniquecouponmodel.BannerCouponImage = entity.BannerCouponImage;
            uniquecouponmodel.StartDate = entity.StartDate;
            uniquecouponmodel.EndDate = entity.EndDate;
            uniquecouponmodel.UPC = entity.UPC;
            uniquecouponmodel.barcodeImagePath = entity.barcodeImagePath;
            uniquecouponmodel.Active = entity.Active;
            return uniquecouponmodel;
        }

        public static UniqueCoupon ToEntity(this UniqueCouponModel model)
        {
            UniqueCoupon uniquecouponentity = new UniqueCoupon();
            uniquecouponentity.Id = model.Id;
            uniquecouponentity.Title = model.Title;
            uniquecouponentity.Offer = model.Offer;
            uniquecouponentity.Restrictions = model.Restrictions;
            uniquecouponentity.Description = model.Description;
            uniquecouponentity.Quantity = model.Quantity;
            uniquecouponentity.DateRange = model.DateRange;
            uniquecouponentity.BannerCouponImage = model.BannerCouponImage;
            uniquecouponentity.StandardCouponImage = model.StandardCouponImage;
            uniquecouponentity.CouponImageForWebsite = model.CouponImageForWebsite;
            uniquecouponentity.Disclaimer = model.Disclaimer;
            uniquecouponentity.BannerCouponImage = model.BannerCouponImage;
            uniquecouponentity.StartDate = model.StartDate;
            uniquecouponentity.EndDate = model.EndDate;
            uniquecouponentity.UPC = model.UPC;
            uniquecouponentity.barcodeImagePath = model.barcodeImagePath;
            uniquecouponentity.Active = model.Active;

            return uniquecouponentity;
        }

        public static UniqueCoupon ToEntity(this UniqueCouponModel model, UniqueCoupon destination)
        {
            return Mapper.Map(model, destination);
        }

        #endregion

        #region imprinted coupons 
        public static ImprintedCouponModel ToImprintedCouponModel(this ImprintedCoupon entity)
        {
            ImprintedCouponModel imprintedcouponmodel = new ImprintedCouponModel();
            imprintedcouponmodel.Id = entity.Id;
            imprintedcouponmodel.Title = entity.Title;
            imprintedcouponmodel.Offer = entity.Offer;
            imprintedcouponmodel.Restrictions = entity.Restrictions;

            imprintedcouponmodel.ExpirationDate = entity.ExpirationDate;

            imprintedcouponmodel.Status = entity.Status;
            imprintedcouponmodel.CouponId = entity.CouponId;
            imprintedcouponmodel.CustomerID = entity.CustomerID;
            imprintedcouponmodel.First_Name = entity.First_Name;
            imprintedcouponmodel.Last_Name = entity.Last_Name;
            imprintedcouponmodel.Email = entity.Email;
            imprintedcouponmodel.StartDate = entity.StartDate;
            imprintedcouponmodel.EndDate = entity.EndDate;
            imprintedcouponmodel.Amount = entity.Amount;
            imprintedcouponmodel.AmountInDecimal = entity.AmountInDecimal;
            imprintedcouponmodel.PdfPath = entity.PdfPath;
            imprintedcouponmodel.QrcodeImgPath = entity.QrcodeImgPath;
            imprintedcouponmodel.UPC = entity.UPC;
            imprintedcouponmodel.ImprintingDate = entity.ImprintingDate;
            return imprintedcouponmodel;
        }

        public static ImprintedCoupon ToImprintedCouponEntity(this ImprintedCouponModel model)
        {
            ImprintedCoupon entity = new ImprintedCoupon();

            entity.Id = model.Id;
            entity.Title = model.Title;
            entity.Offer = model.Offer;
            entity.Restrictions = model.Restrictions;
            entity.ExpirationDate = model.ExpirationDate;

            entity.Status = model.Status;
            entity.CouponId = model.CouponId;
            entity.CustomerID = model.CustomerID;
            entity.First_Name = model.First_Name;
            entity.Last_Name = model.Last_Name;
            entity.Email = model.Email;
            entity.StartDate = model.StartDate;
            entity.EndDate = model.EndDate;
            entity.Amount = model.Amount;
            entity.AmountInDecimal = model.AmountInDecimal;
            entity.PdfPath = model.PdfPath;
            entity.QrcodeImgPath = model.QrcodeImgPath;
            entity.UPC = model.UPC;
            entity.ImprintingDate = model.ImprintingDate;
            return entity;
        }

        #endregion

        #region userpagemodel

        public static UserPageModel toUserPageModel(this ImprintedCoupon entity)
        {
            UserPageModel model = new UserPageModel();
            model.Id = entity.Id;
            model.Title = entity.Title;
            model.Offer = entity.Offer;
            model.Restrictions = entity.Restrictions;
            //model.Description = entity.Description;
            model.RedemptionDate = entity.RedemptionDate;
            model.ExpirationDate = entity.ExpirationDate;
            model.Status = entity.Status;
            model.CouponId = entity.CouponId;
            model.CustomerID = entity.CustomerID;
            model.First_Name = entity.First_Name;
            model.Last_Name = entity.Last_Name;
            model.Email = entity.Email;
            model.StartDate = entity.StartDate;
            model.EndDate = entity.EndDate;
            model.Amount = entity.Amount;
            model.AmountInDecimal = entity.AmountInDecimal;
            model.PdfPath = entity.PdfPath;
            model.QrcodeImgPath = entity.QrcodeImgPath;
            model.ImprintingDate = entity.ImprintingDate;

            return model;
        }

        #endregion




    }
}