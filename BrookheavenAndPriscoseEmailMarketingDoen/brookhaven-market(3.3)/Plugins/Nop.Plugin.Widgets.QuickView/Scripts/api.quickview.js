﻿
var quickviewApi = function () { }

quickviewApi.prototype.displayPopupContent = function (message) {
    //types: success, error
    var container = $('#myModal');

    //we do not encode displayed message
    var htmlcode = '';
    if ((typeof message) == 'string') {
        htmlcode = '<p>' + message + '</p>';
    } else {
        for (var i = 0; i < message.length; i++) {
            htmlcode = htmlcode + '<p>' + message[i] + '</p>';
        }
    }

    container.html(htmlcode);
    container.modal('show');
   
}

quickviewApi.prototype.viewDetail = function () {
    displayAjaxLoading(true);
    $.apiCall({
        url: '/qv/2',
        success: function (html) {
            api.displayPopupContent(html);
            displayAjaxLoading(false);
        }
    });
}

quickviewApi.prototype.loadFoodMenu = function () {
    //var config = $.extend({
    //    success: function () { },
    //    error: function () { }
    //}, options);

    $.apiCall({
        url: '/qv/2',
        success: function (html) {
            $("#myModal").html(html);
            displayAjaxLoading(false);
        }
    });

}






quickviewApi.prototype.storeSelect = function () {
    //var config = $.extend({
    //    success: function () { },
    //    error: function () { }
    //}, options);
    displayAjaxLoading(true);
    $.apiCall({
        url: '/catering/selectstore',
        success: function (html) {
            $(".ajax-content").html(html);
            //window.history.back();
            //window.location.hash = '#/';
            displayAjaxLoading(false);
        }
    });

}
quickviewApi.prototype.test = function () {
    console.log("test");
}

var api = new quickviewApi();