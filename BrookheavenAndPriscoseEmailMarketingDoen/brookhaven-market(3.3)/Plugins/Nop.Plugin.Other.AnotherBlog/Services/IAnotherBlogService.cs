using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Blogs;

namespace Nop.Plugin.Other.AnotherBlog.Services
{
    /// <summary>
    /// Blog service interface
    /// </summary>
    public partial interface IAnotherBlogService
    {
       

        /// <summary>
        /// Gets all blog posts
        /// </summary>
        /// <param name="languageId">Language identifier; 0 if you want to get all records</param>
        /// <param name="dateFrom">Filter by created date; null if you want to get all records</param>
        /// <param name="dateTo">Filter by created date; null if you want to get all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Blog posts</returns>
				IPagedList<BlogPost> GetAllBlogPosts(int languageId,
						DateTime? dateFrom, DateTime? dateTo, int pageIndex, int pageSize, bool showHidden = false, string blogType = "", string SearchKey = "");

        /// <summary>
        /// Gets all blog posts
        /// </summary>
        /// <param name="languageId">Language identifier. 0 if you want to get all news</param>
        /// <param name="tag">Tag</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Blog posts</returns>
				IPagedList<BlogPost> GetAllBlogPostsByTag(int languageId, string tag,
						int pageIndex, int pageSize, bool showHidden = false, string blogType = "");


				/// <summary>
				/// Gets all blog post tags
				/// </summary>
				/// <param name="languageId">Language identifier. 0 if you want to get all news</param>
				/// <param name="showHidden">A value indicating whether to show hidden records</param>
				/// <returns>Blog post tags</returns>
				IList<BlogPostTag> GetAllBlogPostTags(int languageId, bool showHidden = false, string blogType = "");

        
    }
}
