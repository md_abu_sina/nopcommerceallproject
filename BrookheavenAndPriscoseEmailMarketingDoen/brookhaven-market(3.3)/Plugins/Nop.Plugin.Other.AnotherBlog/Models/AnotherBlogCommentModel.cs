﻿using System;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Other.AnotherBlog.Models
{
    public partial class AnotherBlogCommentModel : BaseNopEntityModel
    {
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerAvatarUrl { get; set; }

        public string CommentText { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool AllowViewingProfiles { get; set; }
    }
}