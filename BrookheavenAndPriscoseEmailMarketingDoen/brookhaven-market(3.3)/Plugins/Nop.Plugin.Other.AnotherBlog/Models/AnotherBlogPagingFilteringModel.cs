﻿using System;
using Nop.Web.Framework.UI.Paging;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Blogs;

namespace Nop.Plugin.Other.AnotherBlog.Models
{
    public partial class AnotherBlogPagingFilteringModel : BaseNopEntityModel
    {
      public BlogPagingFilteringModel Filtering {get;set; }
			public string SearchKey { get; set; }
			public string SearchColumn { get; set; }
    }
}