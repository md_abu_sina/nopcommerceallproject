using System;
using Nop.Core;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.BsSlider.Domain
{
		public class Slider : BaseEntity
    {
            private ICollection<BsSliderPicture> _bsSliderPictures;
				
				public virtual string Name { get; set; }
                public virtual string WidgetZone { get; set; }
                public virtual bool Published { get; set; }
                public virtual int DisplayOrder { get; set; }
                public virtual int Height { get; set; }
                public virtual int EffectId { get; set; }
                public virtual int AnimSpeed { get; set; }
                public virtual int PauseTime { get; set; }
                public virtual bool DirectionNav { get; set; }
                public virtual bool ControlNav { get; set; }
                public virtual bool ControlNavThumbs { get; set; }
                public virtual bool PauseOnHover { get; set; }
                public virtual bool ManualAdvance { get; set; }
                public virtual int ThemeId { get; set; }

                public virtual ICollection<BsSliderPicture> BsSliderPictures
                {
                    get { return _bsSliderPictures ?? (_bsSliderPictures = new List<BsSliderPicture>()); }
                    protected set { _bsSliderPictures = value; }
                }

    }
}
