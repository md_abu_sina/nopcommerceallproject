using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.BsSlider.Domain;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Services.Orders;

namespace Nop.Plugin.Widgets.BsSlider.Services
{
    public class BsSliderPictureService : IBsSliderPictureService
    {
        private readonly IRepository<BsSliderPicture> _sliderPictureRepository;

        public BsSliderPictureService(IRepository<BsSliderPicture> sliderPictureRepository)
        {
            _sliderPictureRepository = sliderPictureRepository;
        }

        #region Implementation of BsSliderPictureService


        /// <summary>
        /// get event  by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual BsSliderPicture GetSliderPictureById(int sliderId, int pictureId)
        {
            var db = _sliderPictureRepository.Table;
            return db.SingleOrDefault(s => s.Slider.Id.Equals(sliderId) && s.Id.Equals(pictureId));
        }

        ///// <summary>
        ///// get event  by id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //public virtual List<BsSliderPicture> GetSliderPictureBySliderId(int sliderId)
        //{
        //    var db = _sliderPictureRepository.Table;
        //    return db.Where(s => s.Slider.Id.Equals(sliderId)).ToList();
        //}
        /// <summary>
        /// get all cateringevents
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>IPagedList<cateringevents></returns>
        public virtual IPagedList<BsSliderPicture> GetAllSliderPictures(int pageIndex, int pageSize, int sliderId)
        {

            var query = (from s in _sliderPictureRepository.Table
                         where s.Slider.Id.Equals(sliderId)
                         orderby s.DisplayOrder
                         select s);
            var pictures = new PagedList<BsSliderPicture>(query, pageIndex, pageSize);
            return pictures;
        }

        public virtual List<BsSliderPicture> GetAllPictures(int sliderId)
        {

            var query = (from s in _sliderPictureRepository.Table
                         where s.Slider.Id.Equals(sliderId)
                         orderby s.DisplayOrder
                         select s).ToList();
            return query;
        }

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="eventItem"></param>
        public void InsertSliderPicture(BsSliderPicture sliderPicture)
        {
            if (sliderPicture == null)
                throw new ArgumentNullException("sliderpicture");


            _sliderPictureRepository.Insert(sliderPicture);


        }

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="eventItem"></param>
        public void UpdateSliderPicture(BsSliderPicture sliderPicture)
        {
            if (sliderPicture == null)
                throw new ArgumentNullException("sliderpicture");


            _sliderPictureRepository.Update(sliderPicture);


        }

        /// <summary>
        /// delete event
        /// </summary>
        /// <param name="eventItem"></param>
        public void DeleteSlider(BsSliderPicture sliderPicture)
        {
            _sliderPictureRepository.Delete(sliderPicture);
        }

        #endregion

    }
}