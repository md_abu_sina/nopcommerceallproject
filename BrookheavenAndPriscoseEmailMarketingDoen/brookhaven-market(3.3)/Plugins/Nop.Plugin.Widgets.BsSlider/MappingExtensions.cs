﻿using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Plugin.Widgets.BsSlider.Domain;
using Nop.Plugin.Widgets.BsSlider.Models;
using Nop.Services.Localization;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.BsSlider
{
    public static class MappingExtensions
    {
       

        

		#region BsSlider

		public static SliderModel ToModel(this Slider entity)
		{
            SliderModel model = new SliderModel();

			model.Id = entity.Id;
            model.Name = entity.Name;
            model.WidgetZone = entity.WidgetZone;
            model.Published = entity.Published;
            model.DisplayOrder = entity.DisplayOrder;
            model.Height = entity.Height;
            model.EffectId = entity.EffectId;
            model.AnimSpeed = entity.AnimSpeed;
            model.PauseTime = entity.PauseTime;
            model.DirectionNav = entity.DirectionNav;
            model.ControlNav = entity.ControlNav;
            model.ControlNavThumbs = entity.ControlNavThumbs;
            model.ControlNav = entity.ControlNav;
            model.PauseOnHover = entity.PauseOnHover;
            model.ManualAdvance = entity.ManualAdvance;
            model.ThemeId = entity.ThemeId;
					
			return model;
		}

        public static Slider ToEntity(this SliderModel model)
		{
            Slider entity = new Slider();

			entity.Id = model.Id;
            entity.Name = model.Name;
            entity.WidgetZone = model.WidgetZone;
            entity.Published = model.Published;
            entity.DisplayOrder = model.DisplayOrder;
            entity.Height = model.Height;
            entity.EffectId = model.EffectId;
            entity.AnimSpeed = model.AnimSpeed;
            entity.PauseTime = model.PauseTime;
            entity.DirectionNav = model.DirectionNav;
            entity.ControlNav = model.ControlNav;
            entity.ControlNavThumbs = model.ControlNavThumbs;
            entity.ControlNav = model.ControlNav;
            entity.PauseOnHover = model.PauseOnHover;
            entity.ManualAdvance = model.ManualAdvance;
            entity.ThemeId = model.ThemeId;
					
			return entity;
		}

        public static Slider ToEntity(this SliderModel model, Slider entity)
		{

            entity.Id = model.Id;
            entity.Name = model.Name;
            entity.WidgetZone = model.WidgetZone;
            entity.Published = model.Published;
            entity.DisplayOrder = model.DisplayOrder;
            entity.Height = model.Height;
            entity.EffectId = model.EffectId;
            entity.AnimSpeed = model.AnimSpeed;
            entity.PauseTime = model.PauseTime;
            entity.DirectionNav = model.DirectionNav;
            entity.ControlNav = model.ControlNav;
            entity.ControlNavThumbs = model.ControlNavThumbs;
            entity.ControlNav = model.ControlNav;
            entity.PauseOnHover = model.PauseOnHover;
            entity.ManualAdvance = model.ManualAdvance;
            entity.ThemeId = model.ThemeId;

			return entity;
		}

        public static SliderPictureModel ToSliderPictureModel(this BsSliderPicture entity)
        {
            SliderPictureModel model = new SliderPictureModel();

            model.Id = entity.Id;
            model.SliderId = entity.Slider.Id;
            model.ImageId = entity.PictureId;
            model.PictureLink = entity.PictureLink;
            model.DisplayOrder = entity.DisplayOrder;
            model.HtmlCode = entity.HtmlCode;
            model.Transition = entity.Transition;
            model.IsProductPicture = entity.IsProductPicture;

            return model;
        }

        public static BsSliderPicture ToSliderPictureEntity(this SliderPictureModel model)
        {
            BsSliderPicture entity = new BsSliderPicture();

            entity.Id = model.Id;
            //entity.Slider = model.Slider.ToEntity();
            entity.PictureId = model.ImageId;
            entity.PictureLink = model.PictureLink;
            entity.DisplayOrder = model.DisplayOrder;
            entity.HtmlCode = model.HtmlCode;
            entity.Transition = model.Transition;
            entity.IsProductPicture = model.IsProductPicture;
            

            return entity;
        }

        public static BsSliderPicture ToSliderPictureEntity(this SliderPictureModel model, BsSliderPicture entity)
        {

            entity.Id = model.Id;
            //entity.Slider = model.Slider.ToEntity();
            entity.PictureId = model.ImageId;
            entity.PictureLink = model.PictureLink;
            entity.DisplayOrder = model.DisplayOrder;
            entity.HtmlCode = model.HtmlCode;
            entity.Transition = model.Transition;
            entity.IsProductPicture = model.IsProductPicture;

            return entity;
        }

		#endregion

        //public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
        //{
        //    var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
        //                                select new
        //                                {
        //                                    ID = e,
        //                                    Name = e.ToString()
        //                                }).ToList();

        //    return new SelectList(values, "Id", "Name", enumObj);
        //}

		

        //public static SelectList ToSelectList<TEnum>(this TEnum enumObj,
        //    bool markCurrentAsSelected = true, int[] valuesToExclude = null) where TEnum : struct
        //{
        //    if (!typeof(TEnum).IsEnum) throw new ArgumentException("An Enumeration type is required.", "enumObj");

        //    var localizationService = EngineContext.Current.Resolve<ILocalizationService>();
        //    var workContext = EngineContext.Current.Resolve<IWorkContext>();

        //    var values = from TEnum enumValue in Enum.GetValues(typeof(TEnum))
        //                 where valuesToExclude == null || !valuesToExclude.Contains(Convert.ToInt32(enumValue))
        //                 select new { ID = Convert.ToInt32(enumValue), Name = enumValue.GetLocalizedEnum(localizationService, workContext) };
        //    object selectedValue = null;
        //    if (markCurrentAsSelected)
        //        selectedValue = Convert.ToInt32(enumObj);
        //    return new SelectList(values, "ID", "Name", selectedValue);
        //}
	}
}