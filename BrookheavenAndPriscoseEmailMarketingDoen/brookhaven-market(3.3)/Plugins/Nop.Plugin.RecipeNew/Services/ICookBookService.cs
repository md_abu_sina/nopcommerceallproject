﻿using Nop.Plugin.RecipeNew.Domain;
using System.Collections.Generic;
using System;

namespace Nop.Plugin.RecipeNew.Services
{
    public interface ICookBookService
    {
        /// <summary>
        /// Logs the specified record.
        /// </summary>
        /// <param name="record">The record.</param>
        void Log(CookBook record);

        /// <summary>
        /// get all cookbook by customer id
        /// </summary>
        /// <param name="customerID">customerID</param>
        IList<CookBook> GetAllCookbooksByCustomerID(int customerID);

        /// <summary>
        /// Inserts a cookbook item
        /// </summary>
        /// <param name="cookbookItem">cookbookItem</param>
        void InsertCookBook(CookBook cookbookItem);

        /// <summary>
        /// get cookbook by customer id and recipe id
        /// </summary>
        /// <param name="customerId">customerId</param>
        ///<param name="recipeId">recipeId</param>
        int GetCookbookId(int customerId, int recipeId);

        /// <summary>
         /// delete item from cookbook
         /// </summary>
         /// <param name="cookbookId">cookbookId</param>
        void DeleteCookBookItem(int cookbookId);

         /// <summary>
         /// get cookbook by id
         /// </summary>
         /// <param name="cookbookId">cookbookId</param>
        CookBook GetCookbookById(int cookbookID);

        byte[] GetResponseFromUrl(string AccessToken, string Url);
    }
}
