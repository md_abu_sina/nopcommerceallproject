﻿using Nop.Plugin.RecipeNew.Domain;
using System.Collections.Generic;
using System;

namespace Nop.Plugin.RecipeNew.Services
{
    public interface IShoppingListService
    {
        

        RecipeShoppingList GetShoppingListById(int shoppingListID);

        IList<RecipeShoppingList> GetAllShoppingListsByCustomerID(int customerID);

        void InserttoShoppingList(RecipeShoppingList shoppinglistItem);

        void DeleteshoppinglistItem(int shoppinglistid);

        int GetShoppingListId(int customerId, int recipeId);

        IList<RecipeShoppingList> GetAllShoppingLists();

    }
}
