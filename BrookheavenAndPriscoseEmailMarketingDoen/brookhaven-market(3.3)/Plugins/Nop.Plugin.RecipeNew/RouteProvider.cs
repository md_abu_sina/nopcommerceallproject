﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.RecipeNew
{
    public class RouteProvider : IRouteProvider
    {
        #region IRouteProvider Members

        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Admin.Plugin.Recipe.Celender.Indexnew", "Admin/Plugin/Recipe/Celender/IndexNew",
                new { controller = "RecipeNew", action = "GetRecipeCalender" },
                         new[] { "Nop.Plugin.RecipeNew.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Recipe.Celender.getAllCalenderRecipesnew", "Admin/Plugin/Recipe/Celender/getAllCalenderRecepiesNew",
                           new { controller = "RecipeNew", action = "getAllCalenderRecepies" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Recipe.Celender.addrecipestothisdaynew", "Admin/Plugin/Recipe/Celender/addrecipestothisdayNew",
                           new { controller = "RecipeNew", action = "addrecipestothisday" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Recipe.Celender.checkIfDayisAlreadyOccupiednew", "Admin/Plugin/Recipe/Celender/checkIfDayisAlreadyOccupiedNew",
                       new { controller = "RecipeNew", action = "checkIfDayisAlreadyOccupied" },
                       new[] { "Nop.Plugin.RecipeNew.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Admin.Plugin.Recipe.Celender.ShowRecipeInCalenderPagenew", "Admin/Plugin/Recipe/Celender/ShowRecipeInCalenderPageNew",
                       new { controller = "RecipeNew", action = "ShowRecipeInCalenderPage" },
                        new { recipeid = @"\d+" },
                       new[] { "Nop.Plugin.RecipeNew.Controllers" }).DataTokens.Add("area", "admin");
           
            routes.MapRoute("Admin.Plugin.Recipe.Celender.deleteRecipeforThisDaynew", "Admin/Plugin/Recipe/Celender/deleteRecipeforThisDayNew",
                       new { controller = "RecipeNew", action = "deleteRecipeforThisDay" },
                       new[] { "Nop.Plugin.RecipeNew.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("searchInRecipeCalendernew", "searchinrecipecalendernew",
                       new { controller = "RecipeNew", action = "getRecipeListForCalenderPage" },
                       new[] { "Nop.Plugin.RecipeNew.Controllers" }).DataTokens.Add("area", "admin"); ;

            routes.MapRoute("dailyrecipenew", "dailyrecipe",
               new { controller = "RecipeNew", action = "Public_recipe_display" },
                             new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("autocompletenew", "getautocompleteSearchresultsnew",
                         new { controller = "RecipeNew", action = "getautocompleteSearchresults" },
                         new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("recipeindexnew", "recipesnew",
                            new { controller = "RecipeNew", action = "Index" },
                            new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("showSingleRecipenew", "recipesnew/{recipeid}",
                           new { controller = "RecipeNew", action = "showSingleRecipe", recipeid = "" },
                           new { recipeid = @"\d+" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("search_ingredientnew", "recipesnew/search/ingredient/{ingredient}",
                           new { controller = "RecipeNew", action = "showRecipeList", ingredient = "" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("search_titlenew", "recipesnew/search/title/{title}",
                           new { controller = "RecipeNew", action = "showRecipeList", title = "" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });



            routes.MapRoute("recipewithtagsnew", "recipesnew/tags/{tags}",
                           new { controller = "RecipeNew", action = "showRecipeList", tags = "" },

                           new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("recipewithpagesforMobilenew", "Recipenew/showRecipeList/",
                           new { controller = "RecipeNew", action = "showrecipelist_mobile" },

                           new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapLocalizedRoute("showSingleRecipeafterscalenew", "recipesnew/{recipeid}/{servings}/{units}/{commit}",
                         new { controller = "RecipeNew", action = "showSingleRecipe", recipeid = "", servings = "", units = "", commit = "" },

                         new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.AddTocookBooknew", "AddTocookBookNew",
                   new { controller = "RecipeNew", action = "AddTocookBook" },
                   new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("AddTocookBook_mobilenew", "AddTocookBook_mobilenew",
                 new { controller = "RecipeNew", action = "AddTocookBook_mobile" },
                 new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.AddToshoppingListnew", "AddToshoppingListNew",
                   new { controller = "RecipeNew", action = "AddToshoppingList" },
                   new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.DeleteRecipeFromshoppinglistnew", "DeleteRecipeFromshoppinglistNew",
                  new { controller = "RecipeNew", action = "DeleteRecipeFromshoppinglist" },
                  new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.DeleteAllRecipeFromshoppinglistnew", "DeleteAllRecipeFromshoppinglistNew",
                  new { controller = "RecipeNew", action = "DeleteAllRecipeFromshoppinglist" },
                  new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.DeleteRecipeFromshoppinglistMobilenew", "DeleteRecipeFromshoppinglistMobileNew",
                  new { controller = "RecipeNew", action = "DeleteRecipeFromshoppinglistMobile" },
                  new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.DeleteRecipeFromCookBooknew", "DeleteRecipeFromCookBook",
                   new { controller = "RecipeNew", action = "DeleteRecipeFromCookBook" },
                   new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.DeleteRecipeFromCookBookForMobilenew", "DeleteRecipeFromCookBookForMobile",
                   new { controller = "RecipeNew", action = "DeleteRecipeFromCookBookForMobile" },
                   new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("AddToshoppingList_mobilenew", "AddToshoppingList_mobile",
                  new { controller = "RecipeNew", action = "AddToshoppingList_mobile" },
                  new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("addtoShoppingListUserItemsnew", "recipes/addtoShoppingListUserItemsnew",
                  new { controller = "RecipeNew", action = "addtoShoppingListUserItems" },
                  new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("deleteShoppingListUserItemsnew", "recipes/deleteShoppingListUserItemsnew",
                  new { controller = "RecipeNew", action = "deleteShoppingListUserItems" },
                  new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.ShoppingListnew", "ShoppingListNew",
                 new { controller = "RecipeNew", action = "ShoppingList" },
                 new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("tipsAndGuidesindexnew", "tipsandguides",
                          new { controller = "RecipeNew", action = "tipsAndGuids" },
                          new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("tipsAndGuidesnew", "tipsandguidesnew/{id}",
                           new { controller = "RecipeNew", action = "tipsAndGuids", id = "" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("searchTipsnew", "tipsandguides/search/{search}",
                           new { controller = "RecipeNew", action = "tipsAndGuids", search = "" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("TipsListMobilenew", "nextpage/TipsdisplayableItems_mobilenew",
                           new { controller = "RecipeNew", action = "TipsdisplayableItems_mobile" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("getautocompleteTipsSearchresultsnew", "getautocompleteTipsSearchresultsnew",
                       new { controller = "RecipeNew", action = "getautocompleteTipsSearchresults" },
                       new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("specialsnew", "specialsnew",
                            new { controller = "RecipeNew", action = "specials" },
                            new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("specialsnewwithstorename", "specialsnew/{storeName}",
                            new { controller = "RecipeNew", action = "specials", storeName = "" },
                            new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("featurednew", "featured",
                            new { controller = "RecipeNew", action = "featured" },
                            new[] { "Nop.Plugin.RecipeNew.Controllers" });

						routes.MapRoute("SpecialsWithAdGroupnew", "SpecialsWithAdGroupnew/{storeName}",
                            new { controller = "RecipeNew", action = "SpecialsWithAdGroup", storeName = "" },
                            new[] { "Nop.Plugin.RecipeNew.Controllers" });

						routes.MapRoute("SpecialListWithAdGroupnew", "SpecialsWithAdGroupNew/{storeName}/department_id/{department_id}",
                           new { controller = "RecipeNew", action = "SpecialsWithAdGroup", storeName = "", department_id = "" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("specials_allnew", "specialsnew_all",
                            new { controller = "RecipeNew", action = "specials_all" },
                            new[] { "Nop.Plugin.RecipeNew.Controllers" });

						routes.MapRoute("AdGroupSpecialsAllnew", "AdGroupSpecialsAllNew/{storeName}",
                            new { controller = "RecipeNew", action = "AdGroupSpecialsAll", storeName = "" },
                            new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("specialListnew", "specialsnew/department_id/{department_id}",
                           new { controller = "RecipeNew", action = "specials", department_id = "" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });


            routes.MapRoute("specialsearchnew", "specialsnew/department_id/{department_id}/search/{search}",
                           new { controller = "RecipeNew", action = "specials", department_id = "", search = "" },
                           new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("specialpagesizenew", "specialsnew/department_id/{department_id}/pagesize/{pagesize}/search/{search}",
                          new { controller = "RecipeNew", action = "specials", department_id = "", pagesize = "", search = "" },
                          new[] { "Nop.Plugin.RecipeNew.Controllers" });



            routes.MapRoute("specialListMobilenew", "specialsnew/specials_mobile",
                          new { controller = "RecipeNew", action = "specials_mobile" },
                          new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.AddTospecialshoppingListnew", "AddTospecialshoppingListNew",
                  new { controller = "RecipeNew", action = "AddTospecialshoppingList" },
                  new[] { "Nop.Plugin.RecipeNew.Controllers" });

            routes.MapRoute("Nop.Plugin.RecipeNew.AddTokennew", "recipe/addtoken",
                new { controller = "RecipeNew", action = "AddToken" },
                new[] { "Nop.Plugin.RecipeNew.Controllers" });


        }

        public int Priority
        {
            get { return 1; }
        }

        #endregion
    }
}