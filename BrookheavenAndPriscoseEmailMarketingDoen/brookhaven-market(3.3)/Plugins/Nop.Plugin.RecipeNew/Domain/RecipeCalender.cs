﻿using System;
using Nop.Core;

namespace Nop.Plugin.RecipeNew.Domain
{
    public class RecipeCalender : BaseEntity
    {
        public virtual int Id { get; set; }

        public virtual DateTime Date { get; set; }

        public virtual int RecipeID { get; set; }
        public virtual String RecipeTitle { get; set; }

    }
}
