﻿using Nop.Core;
using Nop.Plugin.RecipeNew.Models;
using Nop.Plugin.RecipeNew.Services;
using Nop.Core.Domain.Messages;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Tasks;
using Nop.Services.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Services.Events;
using Nop.Core.Infrastructure;
using Nop.Services.Customers;

namespace Nop.Plugin.RecipeNew
{
    public class ClearOldDataTask : ITask
    {
        private readonly ILogger _logger;
        private readonly ISpecialShoppingListService _specialshoppinglistservice;
        


        public ClearOldDataTask(ILogger logger, ISpecialShoppingListService specialshoppinglistservice)
        {
            this._logger = logger;
            this._specialshoppinglistservice = specialshoppinglistservice;
        }

        ///--------------------------------------------------------------------------------------------
		/// <summary>
		/// Execute task
		/// </summary>
        public void Execute()
        {
            var specialShoppingListItems = _specialshoppinglistservice.GetOldItem();

            foreach (var specialShoppingListItem in specialShoppingListItems)
            {
                _specialshoppinglistservice.DeletespecialshoppinglistItem(specialShoppingListItem);
            }
            
        }

       


        public int Order
        {
            //ensure that this task is run first 
            get { return 0; }
        }
    }
}
