using System;
using Nop.Core;

namespace Nop.Plugin.Payments.CustomAuthorizeNet.Domain
{
		public class CustomAuthorizeNet : BaseEntity
    {
				public virtual int StoreId { get; set; }
				public virtual bool UseSandbox { get; set; }
        public virtual int TransactModeId { get; set; }
        public virtual string TransactionKey { get; set; }
        public virtual string LoginId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage. true - percentage, false - fixed value.
        /// </summary>
        public virtual bool AdditionalFeePercentage { get; set; }
        /// <summary>
        /// Additional fee
        /// </summary>
        public virtual decimal AdditionalFee { get; set; }
    }
}
