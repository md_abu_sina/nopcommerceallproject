using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using CANet = Nop.Plugin.Payments.CustomAuthorizeNet.Domain;


namespace Nop.Plugin.Payments.CustomAuthorizeNet.Services
{
		public class CustomAuthorizeNetService : ICustomAuthorizeNetService
    {
			private readonly IRepository<CANet.CustomAuthorizeNet> _customAuthorizeNetRepository;


			public CustomAuthorizeNetService(IRepository<CANet.CustomAuthorizeNet> CustomAuthorizeNetRepository)
      {
				_customAuthorizeNetRepository = CustomAuthorizeNetRepository;
      }

        #region Implementation of IViewTrackingService

				/// <summary>
				/// insert event
				/// </summary>
				/// <param name="eventItem"></param>
				public virtual void InsertCustomAuthorizeNet(CANet.CustomAuthorizeNet customAuthorizeNet)
				{
					if(customAuthorizeNet == null)
						throw new ArgumentNullException("customAuthorizeNet");

					_customAuthorizeNetRepository.Insert(customAuthorizeNet);

					//event notification
					//_eventPublisher.EntityInserted(eventItem);
				}

				/// <summary>
				/// get event  by id
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public CANet.CustomAuthorizeNet GetcustomAuthorizeNetById(int id)
				{
					var db = _customAuthorizeNetRepository.Table;
					return db.SingleOrDefault(c => c.Id.Equals(id));
				}

				/// <summary>
				/// get event  by id
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public CANet.CustomAuthorizeNet GetcustomAuthorizeNetByStoreId(int storeId)
				{
					var db = _customAuthorizeNetRepository.Table;
					return db.SingleOrDefault(c => c.StoreId.Equals(storeId));
				}

				/// <summary>
				/// get all
				/// </summary>
				/// <param name="id"></param>
				/// <returns></returns>
				public CANet.CustomAuthorizeNet GetcustomAuthorizeNetFirst()
				{
					var db = _customAuthorizeNetRepository.Table;
					return db.FirstOrDefault();
				}
				

				/// <summary>
				/// Update Event
				/// </summary>
				/// <param name="eventItem"></param>
				public void UpdateCustomAuthorizeNet(CANet.CustomAuthorizeNet customAuthorizeNet)
				{
					if(customAuthorizeNet == null)
						throw new ArgumentNullException("cateringEvent");

					_customAuthorizeNetRepository.Update(customAuthorizeNet);

					//event notification
					//_eventPublisher.EntityUpdated(eventItem);
				}

				/// <summary>
				/// delete event
				/// </summary>
				/// <param name="eventItem"></param>
				public void DeleteCateringEvent(CANet.CustomAuthorizeNet customAuthorizeNet)
				{
					_customAuthorizeNetRepository.Delete(customAuthorizeNet);
					//_eventPublisher.EntityDeleted(eventItem);
				}

        #endregion
    }
}