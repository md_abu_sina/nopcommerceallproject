﻿using AutoMapper;
using Nop.Core.Domain.Topics;
using Nop.Plugin.Other.CustomTopic.Models;

namespace Nop.Plugin.Other.CustomTopic
{
    public static class MappingExtensions
    {


        #region Topics

        public static CustomTopicModel ToModel(this Topic entity)
        {



            CustomTopicModel model = new CustomTopicModel();
            model.Body = entity.Body;
            model.Id = entity.Id;
            model.IncludeInSitemap = entity.IncludeInSitemap;
            model.IsPasswordProtected = entity.IsPasswordProtected;
            model.LimitedToStores = entity.LimitedToStores;
            model.MetaDescription = entity.MetaDescription;
            model.MetaKeywords = entity.MetaKeywords;
            model.MetaTitle = entity.MetaTitle;
            model.Password = entity.Password;
            model.SystemName = entity.SystemName;
            model.Title = entity.Title;
           

            

            return model;




            /*return Mapper.Map<Topic, CustomTopicModel>(entity);*/
        }

        public static Topic ToEntity(this CustomTopicModel model)
        {

            Topic topic = new Topic();
            topic.Body = model.Body;
            topic.Id = model.Id;
            topic.IncludeInSitemap = model.IncludeInSitemap;
            topic.IsPasswordProtected = model.IsPasswordProtected;
            topic.LimitedToStores = model.LimitedToStores;
            topic.MetaDescription = model.MetaDescription;
            topic.MetaKeywords = model.MetaKeywords;

            topic.MetaTitle = model.MetaTitle;
            topic.Password = model.Password;

            topic.SystemName = model.SystemName;
            topic.Title = model.Title;

            return topic;


           /* return Mapper.Map<CustomTopicModel, Topic>(model);*/
        }

        public static Topic ToEntity(this CustomTopicModel model, Topic destination)
        {
            return Mapper.Map(model, destination);
        }

        #endregion



    }
}