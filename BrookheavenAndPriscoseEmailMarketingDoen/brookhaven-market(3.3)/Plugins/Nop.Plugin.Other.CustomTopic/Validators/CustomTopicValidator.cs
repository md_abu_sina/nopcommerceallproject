﻿using FluentValidation;
using Nop.Plugin.Other.CustomTopic.Models;
using Nop.Services.Localization;


namespace Nop.Plugin.Other.CustomTopic.Validators
{
    public class CustomTopicValidator : AbstractValidator<CustomTopicModel>
    {
        public CustomTopicValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.LayoutPath)
                .NotNull()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Other.CustomTopics.Fields.LayoutPath.Required"));

            
                  


        }
    }
}