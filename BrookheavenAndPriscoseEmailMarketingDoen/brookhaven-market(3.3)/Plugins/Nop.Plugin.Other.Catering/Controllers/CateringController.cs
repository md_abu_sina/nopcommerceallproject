﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Core.Plugins;
using Nop.Web.Controllers;
using Nop.Web.Models.Checkout;
using Nop.Web.Extensions;
using Nop.Services.Localization;
using Nop.Core.Domain.Common;
using Nop.Services.Directory;
using Nop.Web.Models.Common;
using Nop.Services.Security;
using Nop.Admin.Models.Customers;
using Nop.Plugin.Other.Catering.Models;
using Nop.Web.Framework.Controllers;
using Nop.Services.Common;
using Nop.Services.Seo;
using System;
using Nop.Services.Topics;
using System.Diagnostics;
using Nop.Web.Models.Catalog;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Media;
using Nop.Services.Tax;
using Nop.Services.Media;
using Nop.Plugin.Other.Catering.Services;
using System.Web;
using Nop.Services.Logging;
using System.Text;
using Nop.Web.Framework.Kendoui;
using Nop.Core.Domain.Orders;
using Nop.Services.Payments;
using Nop.Core.Domain.Shipping;
using System.Web.Routing;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Core.Domain.Payments;
using Nop.Services.Shipping;
using Nop.Core.Domain.Discounts;
using Nop.Services.Messages;
using Nop.Web.Framework.Security;
using Nop.Core.Domain.Media;
using System.Text.RegularExpressions;
using System.IO;
using Nop.Core.Domain.Tax;
using Nop.Admin.Models.Orders;
using Nop.Services.Helpers;
using Nop.Core.Domain.Directory;
using Nop.Services.Vendors;
using Nop.Plugin.Other.Catering.Domain;
using Nop.Core.Caching;


namespace Nop.Plugin.Other.Catering.Controllers
{
    public class CateringController : BasePublicController
    {

        #region Fields

        private readonly IWorkContext _workContext;
        private readonly ILocalizationService _localizationService;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IPermissionService _permissionService;
        private readonly IAddressService _addressService;
        private readonly HttpContextBase _httpContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ITopicService _topicService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IProductAttributeService _productattributeService;
        private readonly IStoreContext _storeContext;
        private readonly CatalogSettings _catalogSettings;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly ICateringEventService _cateringEventService;
        private readonly ICateringEmailService _cateringEmailService;
        private readonly ICurrencyService _currencyService;
        private readonly IPictureService _pictureService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IStoreDistanceService _storeDistanceService;
        private readonly ILogger _logger;
        private readonly ICacheManager _cacheManager;

        //One page Checout
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IShippingService _shippingService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IPaymentService _paymentService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IOrderProcessingService _orderProcessingService;

        private readonly OrderSettings _orderSettings;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly RewardPointsSettings _rewardPointsSettings;
        private readonly PaymentSettings _paymentSettings;
        private readonly ShippingSettings _shippingSettings;
        private readonly AddressSettings _addressSettings;
        private readonly MediaSettings _mediaSettings;

        private readonly ICateringPdfService _cateringPdfService;
        private readonly ISpecificationAttributeService _specificationAttributeService;

        private readonly IOrderReportService _orderReportService;
        private readonly IStoreService _storeService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly CurrencySettings _currencySettings;
        private readonly IVendorService _vendorService;
        private readonly IProductAttributeParser _productAttributeParser;

        #endregion


        #region Constructors

        public CateringController(IWorkContext workContext, IGenericAttributeService genericAttributeService, ICurrencyService currencyService, HttpContextBase httpContext, MediaSettings mediaSettings, ICacheManager cacheManager,
            ILocalizationService localizationService, ICustomerService customerService, CatalogSettings catalogSettings, ITaxService taxService, ICateringEventService cateringEventService, IShoppingCartService shoppingCartService,
            ICountryService countryService, IAddressService addressService, IStoreContext storeContext, IPriceCalculationService priceCalculationService, ILogger logger, ICateringEmailService cateringEmailService,
            IStateProvinceService stateProvinceService, ITopicService topicService, IPictureService pictureService, IPriceFormatter priceFormatter, IStoreDistanceService storeDistanceService, IProductAttributeService productattributeService,
            IPermissionService permissionService, ICategoryService categoryService, AddressSettings addressSettings, IProductService productService, IOrderService orderService, ISpecificationAttributeService specificationAttributeService,
            IWebHelper webHelper, IStoreMappingService storeMappingService, IShippingService shippingService, IPluginFinder pluginFinder, IPaymentService paymentService, IOrderTotalCalculationService orderTotalCalculationService,
            IOrderProcessingService orderProcessingService, OrderSettings orderSettings, RewardPointsSettings rewardPointsSettings, PaymentSettings paymentSettings, ShippingSettings shippingSettings, ICateringPdfService cateringPdfService,
            IOrderReportService orderReportService, IStoreService storeService, IDateTimeHelper dateTimeHelper, CurrencySettings currencySettings, IVendorService vendorService, IProductAttributeParser productAttributeParser)
        {
            this._workContext = workContext;
            this._genericAttributeService = genericAttributeService;
            this._localizationService = localizationService;
            this._countryService = countryService;
            this._customerService = customerService;
            this._httpContext = httpContext;
            this._stateProvinceService = stateProvinceService;
            this._permissionService = permissionService;
            this._addressService = addressService;
            this._addressSettings = addressSettings;
            this._categoryService = categoryService;
            this._productService = productService;
            this._productattributeService = productattributeService;
            this._storeContext = storeContext;
            this._topicService = topicService;
            this._catalogSettings = catalogSettings;
            this._priceCalculationService = priceCalculationService;
            this._taxService = taxService;
            this._cateringEventService = cateringEventService;
            this._cateringEmailService = cateringEmailService;
            this._currencyService = currencyService;
            this._pictureService = pictureService;
            this._storeDistanceService = storeDistanceService;
            this._priceFormatter = priceFormatter;
            this._logger = logger;
            this._cacheManager = cacheManager;

            this._orderService = orderService;
            this._shoppingCartService = shoppingCartService;
            this._webHelper = webHelper;
            this._storeMappingService = storeMappingService;
            this._shippingService = shippingService;
            this._pluginFinder = pluginFinder;
            this._paymentService = paymentService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._orderProcessingService = orderProcessingService;
            this._orderSettings = orderSettings;
            this._rewardPointsSettings = rewardPointsSettings;
            this._paymentSettings = paymentSettings;
            this._shippingSettings = shippingSettings;
            this._cateringPdfService = cateringPdfService;
            this._specificationAttributeService = specificationAttributeService;
            this._orderReportService = orderReportService;
            this._storeService = storeService;
            this._dateTimeHelper = dateTimeHelper;
            this._orderSettings = orderSettings;
            this._vendorService = vendorService;
            this._currencySettings = currencySettings;
            this._productAttributeParser = productAttributeParser;

        }

        #endregion


        #region Utilities



        [NonAction]
        protected bool RequiredBillingAddress()
        {
            var model = new CateringEventModel();
            if (_httpContext.Session["CateringEventId"] != null)
            {
                var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                model = _cateringEventService.GetCateringEventById(currentCateringEventId).ToModel();

                if (model.AddressId == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;

        }

        [NonAction]
        protected int CurrentEventInProgress()
        {
            var currentCustomerId = _workContext.CurrentCustomer.Id;

            var lastEventInProgress = (currentCustomerId > 0) ? _cateringEventService.GetInProgressCateringEventByCustomerId(currentCustomerId) : null;


            if (lastEventInProgress != null)
            {
                var storeId = lastEventInProgress.SupplierStoreId;
                var addressId = Convert.ToInt32(_genericAttributeService.GetAttributesForEntity(storeId, "StoreAddress").FirstOrDefault().Value);
                _httpContext.Session["CateringStoreAddressId"] = addressId;
                return lastEventInProgress.Id;
            }
            else
            {
                return 0;
            }
        }

        [NonAction]
        protected bool VerifyEvent(CateringEventModel model)
        {
            return _cateringEventService.IsValidDeliveryTime(model.DeliveryTime); //&& model.NumberOfPeople > 0
        }

        [NonAction]
        protected void CreateStoreAddressMap(int storeId = 0, int addressId = 0)
        {
            var IsStoreAddessExist = _genericAttributeService.GetAttributesForEntity(storeId, "StoreAddress").Any();
            if (!IsStoreAddessExist)
            {
                GenericAttribute ga = new GenericAttribute();
                ga.KeyGroup = "StoreAddress";
                ga.EntityId = storeId;
                ga.Key = "AddressId";
                ga.Value = addressId.ToString();

                _genericAttributeService.InsertAttribute(ga);
            }

        }

        [NonAction]
        protected CateringEventModel PrepareEventModel(int storeId = 0)
        {
            var model = new CateringEventModel();
            model.Id = 0;
            var defaultTime = DateTime.Now.AddHours(2).Ceil(new TimeSpan(1, 0, 0));
            if (defaultTime.Hour > 21)
                defaultTime = defaultTime.AddDays(1).AddHours(8);
            model.DeliveryTime = defaultTime;



            model.CurrentCustomerId = _workContext.CurrentCustomer.Id;

            if (_httpContext.Session["CateringEventId"] != null)
            {
                var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                model = _cateringEventService.GetCateringEventById(currentCateringEventId).ToModel();
            }

            if (storeId > 0)
            {
                model.AddressId = _workContext.CurrentCustomer.Addresses.Count() > 0 ? _workContext.CurrentCustomer.Addresses.FirstOrDefault().Id : 0;//Int32.Parse(_genericAttributeService.GetAttributesForEntity(storeId, "StoreAddress").FirstOrDefault().Value);
                model.CafePickup = true;
                model.SupplierStoreId = storeId;
                model.SupplierStoreName = this.GetStoreNameById(storeId);
            }
            else
            {
                model.AddressId = _workContext.CurrentCustomer.ShippingAddress.Id;
                model.CafePickup = false;

                var currentCustomerAddress = _workContext.CurrentCustomer.ShippingAddress;

                string fullAddress = "";

                fullAddress += currentCustomerAddress.Address1;
                fullAddress += ", " + currentCustomerAddress.City;
                fullAddress += ", " + currentCustomerAddress.StateProvince;
                fullAddress += " " + currentCustomerAddress.ZipPostalCode;

                var nearestStore = _storeDistanceService.GetNearestStore(fullAddress);

                model.SupplierStoreId = nearestStore.Key;
                model.SupplierStoreDistance = Math.Round(nearestStore.Value);
                model.SupplierStoreName = this.GetStoreNameById(nearestStore.Key);//_customerService.GetCustomerRoleById(nearestStore.Key).Name;
            }


            model.OrderId = 0;

            return model;
        }

        //just copy this method from CatalogController (removed some redundant code)
        [NonAction]
        protected IEnumerable<CateringProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
                bool preparePriceModel = true, bool preparePictureModel = true,
                int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
                bool forceRedirectionAfterAddingToCart = false)
        {
            if (products == null)
                throw new ArgumentNullException("products");

            var models = new List<CateringProductOverviewModel>();

            foreach (var product in products)
            {
                var model = new CateringProductOverviewModel()
                {
                    Id = product.Id,
                    Name = product.GetLocalized(x => x.Name),
                    ShortDescription = product.GetLocalized(x => x.ShortDescription),
                    FullDescription = product.GetLocalized(x => x.FullDescription),
                    SeName = product.GetSeName(),

                };
                //price
                if (preparePriceModel)
                {
                    #region Prepare product price

                    var priceModel = new ProductOverviewModel.ProductPriceModel();

                    switch (product.ProductType)
                    {
                        case ProductType.GroupedProduct:
                            {
                                #region Grouped product

                                var associatedProducts = _productService.SearchProducts(
                                        storeId: _storeContext.CurrentStore.Id,
                                        visibleIndividuallyOnly: false,
                                        parentGroupedProductId: product.Id);

                                switch (associatedProducts.Count)
                                {
                                    case 0:
                                        {
                                            //no associated products
                                            priceModel.OldPrice = null;
                                            priceModel.Price = null;
                                            priceModel.DisableBuyButton = true;
                                            priceModel.DisableWishlistButton = true;
                                            priceModel.AvailableForPreOrder = false;
                                        }
                                        break;
                                    default:
                                        {
                                            //we have at least one associated product
                                            priceModel.DisableBuyButton = true;
                                            priceModel.DisableWishlistButton = true;
                                            priceModel.AvailableForPreOrder = false;

                                            if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                                            {
                                                //find a minimum possible price
                                                decimal? minPossiblePrice = null;
                                                Product minPriceProduct = null;
                                                foreach (var associatedProduct in associatedProducts)
                                                {
                                                    //calculate for the maximum quantity (in case if we have tier prices)
                                                    var tmpPrice = _priceCalculationService.GetFinalPrice(associatedProduct,
                                                            _workContext.CurrentCustomer, decimal.Zero, true, int.MaxValue);
                                                    if (!minPossiblePrice.HasValue || tmpPrice < minPossiblePrice.Value)
                                                    {
                                                        minPriceProduct = associatedProduct;
                                                        minPossiblePrice = tmpPrice;
                                                    }
                                                }
                                                if (minPriceProduct != null && !minPriceProduct.CustomerEntersPrice)
                                                {
                                                    if (minPriceProduct.CallForPrice)
                                                    {
                                                        priceModel.OldPrice = null;
                                                        priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
                                                    }
                                                    else if (minPossiblePrice.HasValue)
                                                    {
                                                        //calculate prices
                                                        decimal taxRate = decimal.Zero;

                                                        //Excluding tax in producct list and product details for catering plugin (Brainstation-23 Razib Mahmud Iffat Marzan)
                                                        var customer = _workContext.CurrentCustomer;
                                                        decimal finalPriceBase = _taxService.GetProductPrice(minPriceProduct, minPossiblePrice.Value, customer, out taxRate, true);
                                                        decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

                                                        priceModel.OldPrice = null;
                                                        priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));

                                                    }
                                                    else
                                                    {
                                                        //Actually it's not possible (we presume that minimalPrice always has a value)
                                                        //We never should get here
                                                        Debug.WriteLine(string.Format("Cannot calculate minPrice for product #{0}", product.Id));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //hide prices
                                                priceModel.OldPrice = null;
                                                priceModel.Price = null;
                                            }
                                        }
                                        break;
                                }

                                #endregion
                            }
                            break;
                        case ProductType.SimpleProduct:
                        default:
                            {
                                #region Simple product

                                //add to cart button
                                priceModel.DisableBuyButton = product.DisableBuyButton ||
                                        !_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart) ||
                                        !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

                                //add to wishlist button
                                priceModel.DisableWishlistButton = product.DisableWishlistButton ||
                                        !_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) ||
                                        !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);
                                //pre-order
                                if (product.AvailableForPreOrder)
                                {
                                    priceModel.AvailableForPreOrder = !product.PreOrderAvailabilityStartDateTimeUtc.HasValue ||
                                            product.PreOrderAvailabilityStartDateTimeUtc.Value >= DateTime.UtcNow;
                                    priceModel.PreOrderAvailabilityStartDateTimeUtc = product.PreOrderAvailabilityStartDateTimeUtc;
                                }

                                //prices
                                if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                                {
                                    //calculate for the maximum quantity (in case if we have tier prices)
                                    decimal minPossiblePrice = _priceCalculationService.GetFinalPrice(product,
                                            _workContext.CurrentCustomer, decimal.Zero, true, int.MaxValue);
                                    if (!product.CustomerEntersPrice)
                                    {
                                        if (product.CallForPrice)
                                        {
                                            //call for price
                                            priceModel.OldPrice = null;
                                            priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
                                        }
                                        else
                                        {
                                            //calculate prices
                                            decimal taxRate = decimal.Zero;


                                            //Excluding tax in producct list and product details for catering plugin (Brainstation-23 Razib Mahmud Iffat Marzan)
                                            var customer = _workContext.CurrentCustomer;
                                            decimal oldPriceBase = _taxService.GetProductPrice(product, product.OldPrice, customer, out taxRate, true);
                                            decimal finalPriceBase = _taxService.GetProductPrice(product, minPossiblePrice, customer, out taxRate, true);

                                            decimal oldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(oldPriceBase, _workContext.WorkingCurrency);
                                            decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

                                            //do we have tier prices configured?
                                            var tierPrices = new List<TierPrice>();
                                            if (product.HasTierPrices)
                                            {
                                                tierPrices.AddRange(product.TierPrices
                                                        .OrderBy(tp => tp.Quantity)
                                                        .ToList()
                                                        .FilterByStore(_storeContext.CurrentStore.Id)
                                                        .FilterForCustomer(_workContext.CurrentCustomer)
                                                        .RemoveDuplicatedQuantities());
                                            }
                                            //When there is just one tier (with  qty 1), 
                                            //there are no actual savings in the list.
                                            bool displayFromMessage = tierPrices.Count > 0 &&
                                                    !(tierPrices.Count == 1 && tierPrices[0].Quantity <= 1);
                                            if (displayFromMessage)
                                            {
                                                priceModel.OldPrice = null;
                                                priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));
                                            }
                                            else
                                            {
                                                if (finalPriceBase != oldPriceBase && oldPriceBase != decimal.Zero)
                                                {
                                                    priceModel.OldPrice = _priceFormatter.FormatPrice(oldPrice);
                                                    priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
                                                }
                                                else
                                                {
                                                    priceModel.OldPrice = null;
                                                    priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //hide prices
                                    priceModel.OldPrice = null;
                                    priceModel.Price = null;
                                }

                                #endregion
                            }
                            break;
                    }

                    model.ProductPrice = priceModel;

                    #endregion
                }

                //picture
                if (preparePictureModel)
                {
                    #region Prepare product picture

                    //If a size has been set in the view, we use it in priority
                    int pictureSize = productThumbPictureSize.HasValue ? productThumbPictureSize.Value : 125;
                    //prepare picture model
                    var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                    model.DefaultPictureModel = new PictureModel()
                    {
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                        Title = string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), model.Name),
                        AlternateText = string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), model.Name)
                    };
                    #endregion
                }

                //time restriction
                var specificationAttribute = _specificationAttributeService.GetProductSpecificationAttributesByProductId(product.Id);
                var IsExistTimeRestrictions = specificationAttribute.Any(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Time Restrictions"));//specificationAttribute.Any(sa => sa.CustomValue.Equals("Time Restrictions")) ? true : false;

                if (IsExistTimeRestrictions)
                {
                    var desiredAttribute = specificationAttribute.Where(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Time Restrictions")).FirstOrDefault();
                    var timeLimit = Int32.Parse(desiredAttribute.SpecificationAttributeOption.Name.Split(' ')[0]);

                    if (_httpContext.Session["CateringEventId"] != null)
                    {
                        var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                        var eventModel = _cateringEventService.GetCateringEventById(currentCateringEventId).ToModel();

                        var deliveryTime = eventModel.DeliveryTime - DateTime.Today;
                        model.IsTimeRestricted = timeLimit > deliveryTime.TotalHours;

                    }
                }

                models.Add(model);
            }
            return models;
        }


        [NonAction]
        protected List<int> GetChildCategoryIds(int parentCategoryId)
        {
            var categoriesIds = new List<int>();
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId);
            foreach (var category in categories)
            {
                categoriesIds.Add(category.Id);
                categoriesIds.AddRange(GetChildCategoryIds(category.Id));
            }
            return categoriesIds;
        }

        #region Report Utilities


        [NonAction]
        protected string GetStoreName(int orderId, int orderItemId)
        {
            var order = _orderService.GetOrderById(orderId);
            var cateringEvent = _cateringEventService.GetCateringEventByOrderId(orderId);
            List<string> neededEmails = new List<string>();
            var storeInfo = _cateringEventService.SecondaryStoreInfo(cateringEvent, order, out neededEmails);

            return storeInfo[orderItemId];
        }

        [NonAction]
        protected string GetSize(string attributeDescription)
        {
            if (attributeDescription.Contains("Size:"))
            {
                string[] attributeString = attributeDescription.Split(new[] { "<br />" }, StringSplitOptions.None);

                try
                {
                    //var dict = attributeDescription.Split(new[] { "<br />" }, StringSplitOptions.None)
                    //    .Select(x => x.Split(':'))
                    //    .ToDictionary(x => x[0], x => x[1]);
                    //var size = dict["Size"];
                    var size = attributeString.Where(x => x.Contains("Size")).FirstOrDefault();

                    size = (size.Contains('[')) ? size.Split(':')[1].Substring(0, size.Split(':')[1].IndexOf('[')) : size.Split(':')[1];

                    return size;
                    /*var dict = attributeDescription.Split(new[] { "<br />" }, StringSplitOptions.None)
                        .Select(x => x.Split(':'))
                        .ToDictionary(x => x[0], x => x[1]);
                    return dict["size"];*/
                }
                catch (Exception)
                {

                    return attributeString[0].Split(':')[1];
                }
            }
            else
            {
                return String.Empty;
            }
        }

        [NonAction]
        protected int GetQuantity(ProductVariantAttributeValue pva, string description)
        {
            try
            {
                var price = description.Split(new[] { "<br />" }, StringSplitOptions.None)
                        .Where(x => x.Contains(pva.Name))
                        .Select(x => x.Split(':'))
                        .ToDictionary(x => x[0], x => x[1]).FirstOrDefault().Value.Split(new char[] { '$', ']' })[1]; ;

                //string price = dict.Substring(dict.IndexOf('$'), dict.IndexOf(']'));

                var targetProductPrice = _productService.GetProductById(pva.AssociatedProductId).Price;
                Decimal quantity = Decimal.Parse(price) / targetProductPrice;
                quantity = Decimal.Round(quantity);

                return Decimal.ToInt32(quantity);
            }
            catch (Exception)
            {

                return 0;
            }

        }
        #endregion

        #region Caching Utitlities
        protected IList<CustomerRole> GetAllStores()
        {
            var cacheKey = "Nop.plugin.catering.allstores"; //all stores are collected from customer roles and needed to be cached because it is used in several places
            var cachedModel = _cacheManager.Get(cacheKey, () =>
            {
                var model = _customerService.GetCustomerRoleByStoreRole();

                return model;
            });

            return cachedModel;
        }

        protected string GetStoreNameById(int storeId)
        {
            return
                this.GetAllStores().Where(s => s.Id.Equals(storeId)).FirstOrDefault().Name;
        }

        protected CustomerRole GetStoreById(int storeId)
        {
            return
                this.GetAllStores().Where(s => s.Id.Equals(storeId)).FirstOrDefault();
        }

        protected CustomerRole GetStoreByName(string storeName)
        {
            return
                this.GetAllStores().Where(s => s.SystemName.Equals(storeName)).FirstOrDefault();
        }
        #endregion

       

        #endregion


        #region Utilities copied from checkout controller

        [NonAction]
        protected bool IsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool ignoreRewardPoints = false)
        {
            bool result = true;

            //check whether order total equals zero
            decimal? shoppingCartTotalBase = _orderTotalCalculationService.GetShoppingCartTotal(cart, ignoreRewardPoints);
            if (shoppingCartTotalBase.HasValue && shoppingCartTotalBase.Value == decimal.Zero)
                result = false;
            return result;
        }

        [NonAction]
        protected CheckoutBillingAddressModel PrepareBillingAddressModel(int? selectedCountryId = null,
                bool prePopulateNewAddressWithCustomerFields = false)
        {
            var model = new CheckoutBillingAddressModel();
            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses
                //allow billing
                    .Where(a => a.Country == null || a.Country.AllowsBilling)
                //enabled for the current store
                    .Where(a => a.Country == null || _storeMappingService.Authorize(a.Country))
                    .ToList();
            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                addressModel.PrepareModel(address,
                        false,
                        _addressSettings);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;
            model.NewAddress.PrepareModel(null,
                    false,
                    _addressSettings,
                    _localizationService,
                    _stateProvinceService,
                    () => _countryService.GetAllCountriesForBilling(),
                    prePopulateNewAddressWithCustomerFields,
                    _workContext.CurrentCustomer);
            return model;
        }

        [NonAction]
        protected CheckoutShippingAddressModel PrepareShippingAddressModel(int? selectedCountryId = null,
                bool prePopulateNewAddressWithCustomerFields = false)
        {
            var model = new CheckoutShippingAddressModel();
            //existing addresses
            var addresses = _workContext.CurrentCustomer.Addresses
                //allow shipping
                    .Where(a => a.Country == null || a.Country.AllowsShipping)
                //enabled for the current store
                    .Where(a => a.Country == null || _storeMappingService.Authorize(a.Country))
                    .ToList();
            foreach (var address in addresses)
            {
                var addressModel = new AddressModel();
                addressModel.PrepareModel(address,
                        false,
                        _addressSettings);
                model.ExistingAddresses.Add(addressModel);
            }

            //new address
            model.NewAddress.CountryId = selectedCountryId;

            model.NewAddress.PrepareModel(null,
                    false,
                    _addressSettings,
                    _localizationService,
                    _stateProvinceService,
                    () => _countryService.GetAllCountriesForShipping(),
                    prePopulateNewAddressWithCustomerFields,
                    _workContext.CurrentCustomer);
            return model;
        }

        [NonAction]
        protected CheckoutShippingMethodModel PrepareShippingMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutShippingMethodModel();

            var getShippingOptionResponse = _shippingService
                    .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress,
                    "", _storeContext.CurrentStore.Id);
            if (getShippingOptionResponse.Success)
            {
                //performance optimization. cache returned shipping options.
                //we'll use them later (after a customer has selected an option).
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                                                                                             SystemCustomerAttributeNames.OfferedShippingOptions,
                                                                                             getShippingOptionResponse.ShippingOptions,
                                                                                             _storeContext.CurrentStore.Id);

                var currentEventId = this.CurrentEventInProgress();
                var cafePickUp = _cateringEventService.GetCateringEventById(currentEventId).CafePickup;

                foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                {
                    var soModel = new CheckoutShippingMethodModel.ShippingMethodModel()
                    {
                        Name = shippingOption.Name,
                        Description = shippingOption.Description,
                        ShippingRateComputationMethodSystemName = shippingOption.ShippingRateComputationMethodSystemName,
                        ShippingOption = shippingOption,
                    };

                    //adjust rate
                    Discount appliedDiscount = null;
                    var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(
                            shippingOption.Rate, cart, out appliedDiscount);

                    decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                    decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase,
                                                                                                                                                    _workContext.WorkingCurrency);
                    soModel.Fee = _priceFormatter.FormatShippingPrice(rate, true);

                    if (cafePickUp && soModel.Name.Equals("In-Store Pickup"))
                        model.ShippingMethods.Add(soModel);
                    if (!cafePickUp && soModel.Name.Equals("By Ground"))
                        model.ShippingMethods.Add(soModel);
                }

                //find a selected (previously) shipping method
                var selectedShippingOption =
                        _workContext.CurrentCustomer.GetAttribute<ShippingOption>(
                                SystemCustomerAttributeNames.SelectedShippingOption, _storeContext.CurrentStore.Id);
                if (selectedShippingOption != null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.ToList()
                                                                                        .Find(
                                                                                                so =>
                                                                                                !String.IsNullOrEmpty(so.Name) &&
                                                                                                so.Name.Equals(selectedShippingOption.Name,
                                                                                                                             StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                !String.IsNullOrEmpty(
                                                                                                        so.ShippingRateComputationMethodSystemName) &&
                                                                                                so.ShippingRateComputationMethodSystemName.Equals(
                                                                                                        selectedShippingOption
                                                                                                                .ShippingRateComputationMethodSystemName,
                                                                                                        StringComparison.InvariantCultureIgnoreCase));
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
                //if no option has been selected, let's do it for the first one
                if (model.ShippingMethods.FirstOrDefault(so => so.Selected) == null)
                {
                    var shippingOptionToSelect = model.ShippingMethods.FirstOrDefault();
                    if (shippingOptionToSelect != null)
                        shippingOptionToSelect.Selected = true;
                }
            }
            else
            {
                foreach (var error in getShippingOptionResponse.Errors)
                    model.Warnings.Add(error);
            }

            return model;
        }

        [NonAction]
        protected CheckoutPaymentMethodModel PreparePaymentMethodModel(IList<ShoppingCartItem> cart)
        {
            var model = new CheckoutPaymentMethodModel();

            //reward points
            if (_rewardPointsSettings.Enabled && !cart.IsRecurring())
            {
                int rewardPointsBalance = _workContext.CurrentCustomer.GetRewardPointsBalance();
                decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
                decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, _workContext.WorkingCurrency);
                if (rewardPointsAmount > decimal.Zero &&
                        _orderTotalCalculationService.CheckMinimumRewardPointsToUseRequirement(rewardPointsBalance))
                {
                    model.DisplayRewardPoints = true;
                    model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
                    model.RewardPointsBalance = rewardPointsBalance;
                }
            }

            //filter by country
            int filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                    _workContext.CurrentCustomer.BillingAddress != null &&
                    _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = _workContext.CurrentCustomer.BillingAddress.Country.Id;
            }

            //test code for store based payment method
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            dictionary.Add(12, "Payments.CashOnDelivery");
            dictionary.Add(13, "Payments.AuthorizeNet");
            dictionary.Add(14, "Payments.CheckMoneyOrder");

            var targetStoreId = _cateringEventService.GetCateringEventById(CurrentEventInProgress()).SupplierStoreId;

            var boundPaymentMethods = _paymentService
                    .LoadActivePaymentMethods(_workContext.CurrentCustomer.Id, _storeContext.CurrentStore.Id, filterByCountryId)
                    .Where(pm => pm.PaymentMethodType == PaymentMethodType.Standard || pm.PaymentMethodType == PaymentMethodType.Redirection)
                    .ToList();
            foreach (var pm in boundPaymentMethods)
            {
                if (cart.IsRecurring() && pm.RecurringPaymentType == RecurringPaymentType.NotSupported)
                    continue;

                var pmModel = new CheckoutPaymentMethodModel.PaymentMethodModel()
                {
                    Name = pm.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id),
                    PaymentMethodSystemName = pm.PluginDescriptor.SystemName,
                    LogoUrl = pm.PluginDescriptor.GetLogoUrl(_webHelper)
                };
                //payment method additional fee
                decimal paymentMethodAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, pm.PluginDescriptor.SystemName);
                decimal rateBase = _taxService.GetPaymentMethodAdditionalFee(paymentMethodAdditionalFee, _workContext.CurrentCustomer);
                decimal rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                if (rate > decimal.Zero)
                    pmModel.Fee = _priceFormatter.FormatPaymentMethodAdditionalFee(rate, true);

                //If customer impersonated then Payments.PayInStore will be added
                if (_workContext.OriginalCustomerIfImpersonated == null && !_workContext.CurrentCustomer.IsAdmin() || _workContext.CurrentCustomer.IsGuest())
                {
                    if (!pmModel.PaymentMethodSystemName.Equals("Payments.PayInStore"))
                        model.PaymentMethods.Add(pmModel);
                }
                else
                {
                    model.PaymentMethods.Add(pmModel);
                }
            }

            //find a selected (previously) payment method
            var selectedPaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                    SystemCustomerAttributeNames.SelectedPaymentMethod,
                    _genericAttributeService, _storeContext.CurrentStore.Id);
            if (!String.IsNullOrEmpty(selectedPaymentMethodSystemName))
            {
                var paymentMethodToSelect = model.PaymentMethods.ToList()
                        .Find(pm => pm.PaymentMethodSystemName.Equals(selectedPaymentMethodSystemName, StringComparison.InvariantCultureIgnoreCase));
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }
            //if no option has been selected, let's do it for the first one
            if (model.PaymentMethods.FirstOrDefault(so => so.Selected) == null)
            {
                var paymentMethodToSelect = model.PaymentMethods.FirstOrDefault();
                if (paymentMethodToSelect != null)
                    paymentMethodToSelect.Selected = true;
            }

            return model;
        }

        [NonAction]
        protected CheckoutPaymentInfoModel PreparePaymentInfoModel(IPaymentMethod paymentMethod)
        {
            var model = new CheckoutPaymentInfoModel();
            string actionName;
            string controllerName;
            RouteValueDictionary routeValues;
            paymentMethod.GetPaymentInfoRoute(out actionName, out controllerName, out routeValues);
            model.PaymentInfoActionName = actionName;
            model.PaymentInfoControllerName = controllerName;
            model.PaymentInfoRouteValues = routeValues;
            //routeValues.Add("StoreId", new value{14});
            model.DisplayOrderTotals = _orderSettings.OnePageCheckoutDisplayOrderTotalsOnPaymentInfoTab;
            return model;
        }

        [NonAction]
        protected CateringCheckoutConfirmModel PrepareConfirmOrderModel(IList<ShoppingCartItem> cart)
        {
            var model = new CateringCheckoutConfirmModel();
            //terms of service
            model.TermsOfServiceOnOrderConfirmPage = _orderSettings.TermsOfServiceOnOrderConfirmPage;
            //min order amount validation
            bool minOrderTotalAmountOk = _orderProcessingService.ValidateMinOrderTotalAmount(cart);
            if (!minOrderTotalAmountOk)
            {
                decimal minOrderTotalAmount = _currencyService.ConvertFromPrimaryStoreCurrency(_orderSettings.MinOrderTotalAmount, _workContext.WorkingCurrency);
                model.MinOrderTotalWarning = string.Format(_localizationService.GetResource("Checkout.MinOrderTotalAmount"), _priceFormatter.FormatPrice(minOrderTotalAmount, true, false));
            }
            model.CurrentEventId = CurrentEventInProgress();
            return model;
        }



        [NonAction]
        protected bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                    customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                    .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }



        #endregion


        #region Public Section

        public ActionResult Index()
        {
            //Session.Abandon();

            if (_workContext.OriginalCustomerIfImpersonated != null)
            {
                ViewBag.Impersonated = true;
                ViewBag.UserFullName = _workContext.CurrentCustomer.GetFullName();
                ViewBag.UserMail = _workContext.CurrentCustomer.Email;
            }
            else
            {
                ViewBag.Impersonated = false;
            }


            return View("Index");
        }

        public void SetStoreID(int orderId)
        {
            var orderEvent = _cateringEventService.GetCateringEventByOrderId(orderId);
            _httpContext.Session["CateringStoreIdForAdminOrder"] = orderEvent.SupplierStoreId;
        }

        public ActionResult PlaceOrder(bool cafePickUp = false)
        {
            var model = new CateringEventModel();
            if (CurrentEventInProgress() > 0)
            {
                var currentEvent = _cateringEventService.GetCateringEventById(CurrentEventInProgress());
                var eventAddress = _addressService.GetAddressById(currentEvent.AddressId);

                if (eventAddress != null)
                {
                    return Json(new
                    {
                        redirect = "foodmenu",
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            if (cafePickUp)
                return RedirectToAction("SelectStore", new { fromEdit = true });
            else
                return RedirectToAction("Shipping", new { fromEdit = true });
        }

        public ActionResult FoodMenu()
        {
            return View("FoodMenu");
        }

        public ActionResult CancelEvent(int eventId = 0)
        {
            if (eventId > 0)
            {
                var cateringEvent = _cateringEventService.GetCateringEventById(eventId);


                if (cateringEvent != null)
                {
                    //remove cart items
                    var cart = _workContext.CurrentCustomer.ShoppingCartItems
                            .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                            .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                            .ToList();

                    if (cart.Count > 0)
                    {
                        foreach (var item in cart)
                        {
                            _shoppingCartService.DeleteShoppingCartItem(item);
                        }
                    }

                    _cateringEventService.DeleteCateringEvent(cateringEvent);
                }
            }
            Session.Clear();
            return RedirectToAction("EventBrief");
        }

        public ActionResult CategoryTag(int productId = 0)
        {
            var model = new CateringSearchModel();
            if (productId > 0)
            {
                var parentCategory = _categoryService.GetProductCategoriesByProductId(productId).LastOrDefault().Category;


                if (!string.IsNullOrEmpty(parentCategory.Description))
                {
                    try
                    {
                        model.CategoryName = parentCategory.Description.Split(new char[] { '(', ')' })[1];
                        model.CategoryTag = parentCategory.Description.Split(new char[] { '[', ']' })[1];
                    }
                    catch (Exception)
                    {
                        model.CategoryName = parentCategory.Name;
                        model.CategoryTag = "Not Given";
                    }

                }
                else
                {
                    model.CategoryName = parentCategory.Name;
                    model.CategoryTag = "Not Given";
                }

            }

            return View("CategoryTag", model);
        }

        public ActionResult Shipping(bool fromEdit = false)
        {
            if (CurrentEventInProgress() > 0)
            {
                _httpContext.Session["CateringEventId"] = CurrentEventInProgress();
                _httpContext.Session["CateringStoreId"] = _cateringEventService.GetCateringEventById(CurrentEventInProgress()).SupplierStoreId;
            }
            else
            {
                Session.Remove("CateringEventId");
            }

            var shippingAddressModel = PrepareShippingAddressModel(1, prePopulateNewAddressWithCustomerFields: true);

            if (!fromEdit)
                return View("OpcShippingAddress", shippingAddressModel);
            else
                return View("EditShippingAddress", shippingAddressModel);
        }


        public ActionResult EventDetail(int storeId = 0)
        {
            var model = PrepareEventModel(storeId);


            //ViewBag.DefaultTime = defaultTime;

            return View("EventDetail", model);
        }

        [HttpPost]
        public ActionResult EventDetail(CateringEventModel model)
        {


            var cateringEvent = model.ToEntity();

            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    cateringEvent = _cateringEventService.GetCateringEventById(model.Id);
                    cateringEvent = model.ToEntity(cateringEvent);

                    _cateringEventService.UpdateCateringEvent(cateringEvent);
                }
                else
                {
                    _cateringEventService.InsertCateringEvent(cateringEvent);

                    _httpContext.Session["CateringEventId"] = cateringEvent.Id;

                }
                _httpContext.Session["CateringStoreId"] = _cateringEventService.GetCateringEventById(cateringEvent.Id).SupplierStoreId;
                return Json(new
                {
                    success = true
                });
            }




            return View("EventDetail", model);

        }

        public ActionResult EventBrief()
        {
            var model = new CateringEventModel();

            model.EventInProgress = false;

            if (_httpContext.Session["CateringEventId"] != null)
            {
                var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                model = _cateringEventService.GetCateringEventById(currentCateringEventId).ToModel();

                if (model.AddressId > 0)
                {
                    var address = _addressService.GetAddressById(model.AddressId);

                    if (address != null)
                        model.ShippingAddress = address.Address1;
                    
                }

                model.SupplierStoreName = this.GetStoreNameById(model.SupplierStoreId);//_customerService.GetCustomerRoleById(model.SupplierStoreId).Name;
                model.EventInProgress = true;
            }

            else if (CurrentEventInProgress() > 0)
            {
                _httpContext.Session["CateringEventId"] = CurrentEventInProgress();
                _httpContext.Session["CateringStoreId"] = _cateringEventService.GetCateringEventById(CurrentEventInProgress()).SupplierStoreId;
                var currentCateringEventId = CurrentEventInProgress();
                model = _cateringEventService.GetCateringEventById(currentCateringEventId).ToModel();

                if (model.AddressId > 0)
                {
                    var address = _addressService.GetAddressById(model.AddressId);

                    if (address != null)
                        model.ShippingAddress = address.Address1;
                    
                }

                model.SupplierStoreName = this.GetStoreNameById(model.SupplierStoreId);//_customerService.GetCustomerRoleById(model.SupplierStoreId).Name;
                model.EventInProgress = true;
            }

            return View("EventBrief", model);
        }

        public ActionResult Category()
        {
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(19, true).Select(x =>
                        new CategoryAccordionModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            SubCategories = _categoryService.GetAllCategoriesByParentCategoryId(x.Id, true)
                        }).ToList();


            return View("CateringCategory", categories);
        }

        public ActionResult ViewCategory(int categoryId = 19)
        {
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(categoryId, true).Select(x =>
                        new CategoryViewModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            SEName = x.GetSeName(),
                            SubCategories = _categoryService.GetAllCategoriesByParentCategoryId(x.Id, true),
                            PictureLink = _pictureService.GetPictureUrl(x.PictureId)
                        }).ToList();


            return View("ViewCategory", categories);
        }

        public ActionResult Product(int categoryId = 0, int pageSize = 0, int pageNumber = 0)
        {
            var category = _categoryService.GetCategoryById(categoryId);

            var categoryIds = new List<int>();
            categoryIds.Add(categoryId);

            if (pageSize <= 0)
                pageSize = _catalogSettings.SearchPageProductsPerPage;
            if (pageNumber <= 0)
                pageNumber = 1;

            IPagedList<Product> products = new PagedList<Product>(new List<Product>(), 0, 1);



            products = _productService.SearchProducts(categoryIds: categoryIds,
                                                                                                                    storeId: _storeContext.CurrentStore.Id,
                                                                                                                    visibleIndividuallyOnly: true,
                                                                                                                    pageIndex: pageNumber - 1,
                                                                                                                    pageSize: pageSize);

            //List<CateringProductOverviewModel> model = new List<CateringProductOverviewModel>();
            var model = new CateringSearchModel();

            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.LoadPagedList(products);


            if (!string.IsNullOrEmpty(category.Description))
            {
                try
                {
                    model.CategoryName = category.Description.Split(new char[] { '(', ')' })[1];
                    model.CategoryTag = category.Description.Split(new char[] { '[', ']' })[1];
                }
                catch (Exception)
                {
                    model.CategoryName = category.Name;
                    model.CategoryTag = "Not Given";
                }

            }
            else
            {
                model.CategoryName = category.Name;
                model.CategoryTag = "Not Given";
            }


            //string input = "User name (sales)";
            //string output = input.Split(new char[] { '(', ')' })[1];
            //ViewBag.NumberOfPeople = _cateringEventService.GetCateringEventById(CurrentEventInProgress()).NumberOfPeople;

            return View("CateringProduct", model);
        }

        [ValidateInput(false)]
        public ActionResult SaveShipping(FormCollection form)
        {
            try
            {

                int shippingAddressId = 0;
                int.TryParse(form["shipping_address_id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == shippingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutShippingAddressModel();
                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = PrepareShippingAddressModel(selectedCountryId: model.NewAddress.CountryId);
                        shippingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("OpcShippingAddress", shippingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                            model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                            model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                            model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                            model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);
                    if (address == null)
                    {
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //little hack here (TODO: find a better solution)
                        //EF does not load navigation properties for newly created entities (such as this "Address").
                        //we have to load them manually 
                        //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                        if (address.CountryId.HasValue)
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        if (address.StateProvinceId.HasValue)
                            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                        //other null validations
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                //return RedirectToAction("EventDetail");
                var eventModel = PrepareEventModel();
                if (!eventModel.CafePickup && eventModel.SupplierStoreDistance > 5)
                {
                    ViewBag.Distance = eventModel.SupplierStoreDistance;
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "event-detail",
                            html = this.RenderPartialViewToString("SpecialArrangement", eventModel)
                        }
                    });
                }
                else
                {
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "event-detail",
                            html = this.RenderPartialViewToString("EventDetail", eventModel)
                        }
                    });
                }

            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        public ActionResult SelectStore(bool fromEdit = false)
        {
            //Return the view, it doesn't need a model
            var stores = this.GetAllStores().Where(sr => sr.AdGroupId == "2004").OrderByDescending(sr => sr.Id);//_customerService.GetCustomerRoleByStoreRole()
            var allStores = new List<CustomerRoleModel>();

            foreach (var store in stores)
            {
                allStores.Add(store.ToModel());
            }

            if (_httpContext.Session["CateringEventId"] != null)
            {
                var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                var currentEvent = _cateringEventService.GetCateringEventById(currentCateringEventId).ToModel();

                ViewBag.SelectedStoreId = currentEvent.SupplierStoreId;
            }


            if (!fromEdit)
                return View("SelectStore", allStores);
            else
                return View("EditStore", allStores);

        }

        public ActionResult ViewMap(string systemName = "")
        {
            var topic = _topicService.GetTopicBySystemName(systemName);
            if (topic != null)
            {
                ViewBag.Topic = topic.Body;
            }
            else
            {
                ViewBag.Topic = "Map is not available for this store";
            }
            return View("ViewMap");
        }

        public ActionResult ValidateCart(bool removeRestrictedItems = false)
        {
            var model = new CateringEventModel();
            if (CurrentEventInProgress() > 0)
            {
                model = _cateringEventService.GetCateringEventById(CurrentEventInProgress()).ToModel();
                if (VerifyEvent(model))
                {
                    //validation
                    var cart = _workContext.CurrentCustomer.ShoppingCartItems
                            .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                            .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                            .ToList();

                    if (cart.Count == 0)
                        return RedirectToRoute("ShoppingCart");

                    List<string> restricted = new List<string>();

                    for (int i = cart.Count - 1; i >= 0; i--)
                    {
                        var specificationAttribute = _specificationAttributeService.GetProductSpecificationAttributesByProductId(cart[i].ProductId);
                        var IsExistTimeRestrictions = specificationAttribute.Any(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Time Restrictions"));//specificationAttribute.Any(sa => sa.CustomValue.Equals("Time Restrictions")) ? true : false;

                        if (IsExistTimeRestrictions)
                        {
                            var desiredAttribute = specificationAttribute.Where(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Time Restrictions")).FirstOrDefault();
                            var timeLimit = Int32.Parse(desiredAttribute.SpecificationAttributeOption.Name.Split(' ')[0]);

                            var deliveryTime = model.DeliveryTime - DateTime.Today;
                            if (timeLimit > deliveryTime.TotalHours)
                            {
                                restricted.Add(cart[i].Product.Name);
                                if (removeRestrictedItems)
                                {
                                    _shoppingCartService.DeleteShoppingCartItem(cart[i]);
                                    cart.RemoveAt(i);
                                }
                            };

                        }
                    }




                    if (restricted.Count == 0 || removeRestrictedItems)
                    {
                        return Json(new
                        {
                            redirect = "cart",
                        }, JsonRequestBehavior.AllowGet);
                    }

                    var restrictedProducts = string.Join(",", restricted);
                    ViewBag.Notification = string.Format("These items ('{0}') on the shopping list cannot be ordered using the pickup/delivery time. These items will be removed from cart", restrictedProducts);
                    return View("TimeRestrictionNotification");

                }

                return RedirectToAction("Shipping");
            }

            return RedirectToAction("Shipping");
        }

        public ActionResult SpecificationAttribute(int productId = 0)
        {
            if (productId > 0)
            {
                //time restriction
                var specificationAttribute = _specificationAttributeService.GetProductSpecificationAttributesByProductId(productId);
                var IsExistTimeRestrictions = specificationAttribute.Any(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Time Restrictions"));//specificationAttribute.Any(sa => sa.CustomValue.Equals("Time Restrictions")) ? true : false;

                var IsExistGlutenFree = specificationAttribute.Any(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Gluten Free"));
                var IsExistVegeterian = specificationAttribute.Any(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Vegeterian"));

                if (IsExistTimeRestrictions)
                {
                    var desiredAttribute = specificationAttribute.Where(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Time Restrictions")).FirstOrDefault();
                    var timeLimit = Int32.Parse(desiredAttribute.SpecificationAttributeOption.Name.Split(' ')[0]);

                    if (_httpContext.Session["CateringEventId"] != null)
                    {
                        var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                        var eventModel = _cateringEventService.GetCateringEventById(currentCateringEventId).ToModel();

                        var deliveryTime = eventModel.DeliveryTime - DateTime.Today;
                        ViewBag.IsTimeRestricted = timeLimit > deliveryTime.TotalHours;

                    }
                }

                if (IsExistGlutenFree)
                {
                    var glutenFree = specificationAttribute.Where(sa => sa.SpecificationAttributeOption.SpecificationAttribute.Name.Equals("Gluten Free")).FirstOrDefault().SpecificationAttributeOption.Name;
                    if (glutenFree == "Yes")
                        ViewBag.IsExistGlutenFree = true;
                }

                if (IsExistVegeterian)
                {
                    ViewBag.IsExistVegeterian = true;
                }
            }

            return View("NeededSpecificationAttribute");

        }

        public ActionResult ProductAttributeQuantity(int productAttributeId = 0, int paQuantity = 0, bool updateQuantity = false)
        {
            var productAttribute = _productattributeService.GetProductVariantAttributeValueById(productAttributeId);
            if (productAttribute.AttributeValueTypeId == 0)
                return Content("");
            ViewBag.ProductAttributeId = productAttribute.Id;
            ViewBag.ProductAttributeQuantity = productAttribute.Quantity;

            if (updateQuantity)
            {
                productAttribute.Quantity = paQuantity;
                _productattributeService.UpdateProductVariantAttributeValue(productAttribute);

                string priceString = string.Format("{0} [+${1}]", productAttribute.Name, _priceCalculationService.GetProductVariantAttributeValuePriceAdjustment(productAttribute).ToString("#.##"));

                return Json(new
                {
                    price_string = priceString,
                    html = this.RenderPartialViewToString("ProductAttributeQuantity")
                }, JsonRequestBehavior.AllowGet);
            }

            return View("ProductAttributeQuantity");
        }

        public ActionResult CateringInvoice(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            var cateringEvent = _cateringEventService.GetCateringEventByOrderId(orderId);

            if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes = null;
            using (var stream = new MemoryStream())
            {
                _cateringPdfService.PrintOrdersToPdf(stream, orders, cateringEvent, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("order_{0}.pdf", order.Id));
        }

        public ActionResult CateringInvoiceAdmin(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            var cateringEvent = _cateringEventService.GetCateringEventByOrderId(orderId);


            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes = null;
            using (var stream = new MemoryStream())
            {
                _cateringPdfService.PrintOrdersToPdf(stream, orders, cateringEvent, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("order_{0}.pdf", order.Id));
        }

        public ActionResult PdfInvoiceSelected(string selectedIds)
        {
            var orders = new List<Order>();

            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                orders.AddRange(_orderService.GetOrdersByIds(ids));
            }
            var cateringEvent = _cateringEventService.GetCateringEventByOrderId(orders.FirstOrDefault().Id);
            
            //ensure that we at least one order selected
            if (orders.Count == 0)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Orders.PdfInvoice.NoOrders"));
                return RedirectToAction("List");
            }

            byte[] bytes = null;
            using (var stream = new MemoryStream())
            {
                _cateringPdfService.PrintOrdersToPdf(stream, orders, cateringEvent, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", "orders.pdf");
        }

        public String CateringDeliveryTime(int orderId)
        {
            var cateringEvent = _cateringEventService.GetCateringEventByOrderId(orderId);

            return cateringEvent.DeliveryTime.ToString("dddd dd MMMM h:mm tt yyyy");
        }

        public String MigrateCustomer()
        {
            if (_httpContext.Session["CustomerMigrated"] != null)
            {
                if (_httpContext.Session["CateringEventId"] != null)
                {
                    var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                    var currentEvent = _cateringEventService.GetCateringEventById(currentCateringEventId);

                    currentEvent.CurrentCustomerId = _workContext.CurrentCustomer.Id;

                    _cateringEventService.UpdateCateringEvent(currentEvent);
                }
                return String.Empty;
            }

            return String.Empty;
        }

        #endregion


        #region Admin Section

        public ActionResult List()
        {
            return View("List");
        }

        public ActionResult OrderList()
        {


            //order statuses
            var model = new CateringOrderListModel();
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //shipping statuses
            model.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.AvailableShippingStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //stores
            model.AvailableStores.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in this.GetAllStores())//_customerService.GetCustomerRoleByStoreRole()
                model.AvailableStores.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(0, int.MaxValue, true))
                model.AvailableVendors.Add(new SelectListItem() { Text = v.Name, Value = v.Id.ToString() });

            //a vendor should have access only to orders with his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            return View("order/OrderList", model);
        }

        [HttpPost]
        public ActionResult OrderList(DataSourceRequest command, CateringOrderListModel model)
        {


            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.VendorId = _workContext.CurrentVendor.Id;
            }

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

            //load orders
            var orders = _cateringEventService.SearchOrders(model.StoreId, model.VendorId, 0, 0, 0,
                startDateValue, endDateValue, orderStatus,
                paymentStatus, shippingStatus, model.CustomerEmail, model.OrderGuid,
                command.Page - 1, command.PageSize, model.DeliveryStartDate, model.DeliveryEndDate);


            var gridModel = new DataSourceResult
            {
                Data = orders.Select(x =>
                {
                    var store = _storeService.GetStoreById(x.StoreId);
                    var cateringEvent = _cateringEventService.GetCateringEventByOrderId(x.Id);
                    var supplierStore = cateringEvent != null ? this.GetStoreById(cateringEvent.SupplierStoreId) : null;//_customerService.GetCustomerRoleById(cateringEvent.SupplierStoreId)
                    var impersonatedBy = _genericAttributeService.GetAttributesForEntity(x.Id, "Order");
                    return new CateringOrderModel()
                    {
                        Id = x.Id,
                        StoreName = supplierStore != null ? supplierStore.Name : "Unknown",
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderStatus = x.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                        PaymentStatus = x.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                        ShippingStatus = x.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                        CustomerEmail = x.BillingAddress.Email,
                        CustomerFullName = string.Format("{0} {1}", x.BillingAddress.FirstName, x.BillingAddress.LastName),
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc),
                        DeliveryTime = cateringEvent != null ? cateringEvent.DeliveryTime : DateTime.MinValue,
                        StorePickup = cateringEvent != null ? (cateringEvent.CafePickup ? "Pick Up" : "Delivery") : "",
                        ImpersonatedBy = impersonatedBy.Count > 0 ? impersonatedBy.FirstOrDefault().Value : "Customer"
                    };
                }),
                Total = orders.TotalCount
            };

            //summary report
            var reportSummary = _orderReportService.GetOrderAverageReportLine(model.StoreId,
                model.VendorId, orderStatus, paymentStatus, shippingStatus,
                startDateValue, endDateValue, model.CustomerEmail);
            var profit = _orderReportService.ProfitReport(model.StoreId,
                model.VendorId, orderStatus, paymentStatus, shippingStatus,
                startDateValue, endDateValue, model.CustomerEmail);
            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            if (primaryStoreCurrency == null)
                throw new Exception("Cannot load primary store currency");

            gridModel.ExtraData = new OrderAggreratorModel()
            {
                aggregatorprofit = _priceFormatter.FormatPrice(profit, true, false),
                aggregatorshipping = _priceFormatter.FormatShippingPrice(reportSummary.SumShippingExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false),
                aggregatortax = _priceFormatter.FormatPrice(reportSummary.SumTax, true, false),
                aggregatortotal = _priceFormatter.FormatPrice(reportSummary.SumOrders, true, false)
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [AdminAuthorize]
        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            var customerRoles = this.GetAllStores();//_customerService.GetCustomerRoleByStoreRole();

            var gridModel = new DataSourceResult
            {
                Data = customerRoles.Select(x =>
                {
                    var cr = x.ToModel();
                    return cr;
                }),
                Total = customerRoles.Count()
            };

            return new JsonResult
            {
                Data = gridModel
            };
        }



        public ActionResult Create(int id = 0)
        {
            var StoreAddess = _genericAttributeService.GetAttributesForEntity(id, "StoreAddress");
            var addressModel = new AddressModel();

            if (StoreAddess.Count > 0)
            {
                var addressId = Convert.ToInt32(StoreAddess.FirstOrDefault().Value);
                var existingAddres = _addressService.GetAddressById(addressId);

                addressModel.PrepareModel(existingAddres,
                    false,
                    _addressSettings,
                    _localizationService,
                    _stateProvinceService,
                    () => _countryService.GetAllCountriesForShipping(),
                    false,
                    _workContext.CurrentCustomer);
            }
            else
            {
                addressModel.PrepareModel(null,
                        false,
                        _addressSettings,
                        _localizationService,
                        _stateProvinceService,
                        () => _countryService.GetAllCountriesForShipping(),
                        false,
                        _workContext.CurrentCustomer);
            }

            ViewBag.StoreId = id;
            return View("StoreAddressCreate", addressModel);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(AddressModel model, FormCollection formCollection, bool continueEditing)
        {

            if (ModelState.IsValid)
            {
                var address = model.ToEntity();

                _addressService.InsertAddress(address);

                //Map with store
                CreateStoreAddressMap(Convert.ToInt32(formCollection["StoreId"]), address.Id);


                return continueEditing ? RedirectToAction("Edit", new
                {
                    id = address.Id
                }) : RedirectToAction("List", "/Plugin/Catering/StoreAddress/");
            }

            return View(model);
        }


        public ActionResult CateringList()
        {
            var storeList = this.GetAllStores().ToList();//_customerService.GetCustomerRoleByStoreRole().ToList();

            ViewBag.StoreList = new SelectList(storeList, "ID", "Name");

            var orderStatus = from CateringOrderStatus d in Enum.GetValues(typeof(CateringOrderStatus))
                              select new
                              {
                                  ID = (int)d,
                                  Name = d.ToString()
                              };
            ViewBag.OrderStatus = new SelectList(orderStatus, "ID", "Name");

            return View("CateringList");
        }

        [AdminAuthorize]
        [HttpPost]
        public ActionResult CateringList(DataSourceRequest command, CateringEventSearchModel model)
        {
            var cateringEvents = _cateringEventService.GetAllCateringEvent(command.Page - 1, command.PageSize, model.DeliveryTimeStart, model.DeliveryTimeEnd, model.SupplierStoreId, model.CateringOrderStatusId);

            var gridModel = new DataSourceResult
            {
                Data = cateringEvents.Select(x =>
                {
                    CateringEventModel ce = x.ToModel();
                    ce.SupplierStoreName = this.GetStoreNameById(x.SupplierStoreId);//_customerService.GetCustomerRoleById(x.SupplierStoreId).Name;
                    return ce;
                }),
                Total = cateringEvents.TotalCount
            };

            return new JsonResult
            {
                Data = gridModel
            };
        }

        [AdminAuthorize]
        public ActionResult UpdateStatus(int id)
        {
            var cateringEvent = _cateringEventService.GetCateringEventById(id);
            cateringEvent.CateringOrderStatus = (int)CateringOrderStatus.InProduction;

            _cateringEventService.UpdateCateringEvent(cateringEvent);

            return RedirectToAction("CateringList", "/Plugin/Other/Catering/");
        }

        public ActionResult EmailList()
        {
            return View("EmailList");
        }

        [AdminAuthorize]
        [HttpPost]
        public ActionResult EmailList(DataSourceRequest command)
        {
            var cateringEmails = _cateringEmailService.GetAllCateringEmail(command.Page - 1, command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = cateringEmails.Select(x =>
                {
                    var ce = x.ToModel();
                    return ce;
                }),
                Total = cateringEmails.Count()
            };

            return new JsonResult
            {
                Data = gridModel
            };
        }

        [AdminAuthorize]
        public ActionResult CreateEmail()
        {
            var cateringEmailModel = new CateringEmailModel();

            var emailType = from EmailAddressType d in Enum.GetValues(typeof(EmailAddressType))
                            select new
                            {
                                ID = (int)d,
                                Name = d.ToString()
                            };

            cateringEmailModel.StoreList = new SelectList(this.GetAllStores(), "ID", "SystemName");//_customerService.GetCustomerRoleByStoreRole()

            cateringEmailModel.EmailAddressTypes = new SelectList(emailType, "ID", "Name");

            return View("EmailAddressCreate", cateringEmailModel);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateEmail(CateringEmailModel model, bool continueEditing)
        {

            if (ModelState.IsValid)
            {
                var cateringEmail = model.ToEntity();

                _cateringEmailService.InsertCateringEmail(cateringEmail);


                return continueEditing ? RedirectToAction("EditEmail", new
                {
                    id = cateringEmail.Id
                }) : RedirectToAction("EmailList", "/Plugin/Other/Catering/");
            }

            return View("EmailAddressCreate", model);
        }

        [AdminAuthorize]
        public ActionResult EditEmail(int id)
        {
            var cateringEmail = _cateringEmailService.GetCateringEmailById(id);
            if (cateringEmail == null)
                return RedirectToAction("EmailList", "/Plugin/Other/Catering/");


            CateringEmailModel cateringEmailModel = cateringEmail.ToModel();

            var emailType = from EmailAddressType d in Enum.GetValues(typeof(EmailAddressType))
                            select new
                            {
                                ID = (int)d,
                                Name = d.ToString(),
                                Selected = (int)d == cateringEmail.Id
                            };

            cateringEmailModel.StoreList = new SelectList(this.GetAllStores(), "ID", "SystemName", new { id = cateringEmailModel.StoreId });//_customerService.GetCustomerRoleByStoreRole()

            cateringEmailModel.EmailAddressTypes = new SelectList(emailType, "ID", "Name");


            return View("EmailAddressEdit", cateringEmailModel);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditEmail(CateringEmailModel model, bool continueEditing)
        {
            var cateringEmail = _cateringEmailService.GetCateringEmailById(model.Id);
            if (cateringEmail == null)
                return RedirectToAction("EmailList", "/Plugin/Other/Catering");

            if (ModelState.IsValid)
            {
                cateringEmail = model.ToEntity(cateringEmail);

                _cateringEmailService.UpdateCateringEmail(cateringEmail);


                return continueEditing ? RedirectToAction("EditEmail", new
                {
                    id = cateringEmail.Id
                }) : RedirectToAction("EmailList", "/Plugin/Other/Catering/");
            }

            return View("EmailAddressEdit", model);
        }

        [AdminAuthorize]
        public ActionResult DeleteEmailConfirmed(int id)
        {
            var cateringEmail = _cateringEmailService.GetCateringEmailById(id);
            if (cateringEmail == null)
                return RedirectToAction("EmailList", "/Plugin/Other/Catering/");

            _cateringEmailService.DeleteCateringEmail(cateringEmail);

            return RedirectToAction("EmailList", "/Plugin/Other/Catering/");
        }


        #endregion


        #region Catering Report

        [AdminAuthorize]
        public ActionResult ReportSummary(ReportSummaryModel model, int pageSize = 20, int pageNumber = 1, bool generatePdf = false)
        {
            DateTime? startDateValue = (model.CateringOrderList.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CateringOrderList.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.CateringOrderList.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CateringOrderList.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.CateringOrderList.OrderStatusId > 0 ? (OrderStatus?)(model.CateringOrderList.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.CateringOrderList.PaymentStatusId > 0 ? (PaymentStatus?)(model.CateringOrderList.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.CateringOrderList.ShippingStatusId > 0 ? (ShippingStatus?)(model.CateringOrderList.ShippingStatusId) : null;




            var orders = _cateringEventService.SearchOrdersForSummaryReport(model.SupplierStoreId, model.CateringOrderList.VendorId, 0, 0, 0,
                startDateValue, endDateValue, orderStatus,
                paymentStatus, shippingStatus, model.CateringOrderList.CustomerEmail, model.CateringOrderList.OrderGuid,
                pageNumber - 1, pageSize, model.CateringOrderList.DeliveryStartDate, model.CateringOrderList.DeliveryEndDate);





            //order status
            model.CateringOrderList.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.CateringOrderList.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //payment statuses
            model.CateringOrderList.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.CateringOrderList.AvailablePaymentStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //shipping statuses
            model.CateringOrderList.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.CateringOrderList.AvailableShippingStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //stores
            model.CateringOrderList.AvailableStores.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in this.GetAllStores())//_customerService.GetCustomerRoleByStoreRole()
                model.CateringOrderList.AvailableStores.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString() });





            var eventOrderItem = orders.GroupBy(o => o.DeliveryTime.Date).Select(o => new
                                                                            {
                                                                                Date = o.Key,
                                                                                EventOrder = o.Select(oi =>
                                                                                                       {
                                                                                                           var ce = _orderService.GetOrderById(oi.OrderId).OrderItems;
                                                                                                           return ce;
                                                                                                       })
                                                                            }).ToList();

            foreach (var item in eventOrderItem)
            {
                var groupedItem = new GroupedOrderModel();

                groupedItem.OrderDay = item.Date;

                var eventOrderProduct = new EventOrderProductModel();

                foreach (var childItem in item.EventOrder)
                {
                    if (model.SupplierStoreId.Equals(0) || model.SupplierStoreId.Equals(12))
                    {
                        var darienItems = childItem.Where(ci => this.GetStoreName(ci.OrderId, ci.Id).Equals("Darien")).Select(ci => new EventOrderProductModel()
                        {
                            Quantity = ci.Quantity,
                            ProductName = ci.Product.Name,
                            Attribute = ci.AttributeDescription,
                            PvaCollection = _productAttributeParser.ParseProductVariantAttributeValues(ci.AttributesXml).Where(pva => pva.AttributeValueType.Equals(AttributeValueType.AssociatedToProduct)).ToList(),
                            StoreName = "Darien",
                            Size = this.GetSize(ci.AttributeDescription)
                        }).ToList();


                        foreach (var darienItem in darienItems)
                        {
                            if (!groupedItem.DarienEventOrder.Any(x => darienItem.ProductName.Equals(x.ProductName)))//&& !groupedItem.DarienEventOrder.Any(x => darienItem.Attribute.Equals(x.Attribute))
                            {
                                groupedItem.DarienEventOrder.Add(darienItem);
                            }
                            else
                            {
                                var existingItem = groupedItem.DarienEventOrder.Where(x => x.ProductName.Equals(darienItem.ProductName)).FirstOrDefault();// && x.Attribute.Equals(darienItem.Attribute))
                                if (existingItem != null)
                                {
                                    existingItem.Quantity = existingItem.Quantity + darienItem.Quantity;
                                }

                            }
                        }

                    }

                    if (model.SupplierStoreId.Equals(0) || model.SupplierStoreId.Equals(11))
                    {
                        var mokenaItems = childItem.Where(ci => this.GetStoreName(ci.OrderId, ci.Id).Equals("Mokena")).Select(ci => new EventOrderProductModel()
                        {
                            Quantity = ci.Quantity,
                            ProductName = ci.Product.Name,
                            Attribute = ci.AttributeDescription,
                            PvaCollection = _productAttributeParser.ParseProductVariantAttributeValues(ci.AttributesXml).Where(pva => pva.AttributeValueType.Equals(AttributeValueType.AssociatedToProduct)).ToList(),
                            StoreName = "Mokena",
                            Size = this.GetSize(ci.AttributeDescription)
                        }).ToList();

                        foreach (var mokenaItem in mokenaItems)
                        {
                            if (!groupedItem.MokenaEventOrder.Any(x => mokenaItem.ProductName.Equals(x.ProductName)))//&& !groupedItem.MokenaEventOrder.Any(x => mokenaItem.Attribute.Equals(x.Attribute))
                            {
                                groupedItem.MokenaEventOrder.Add(mokenaItem);
                            }
                            else
                            {
                                var existingItem = groupedItem.MokenaEventOrder.Where(x => x.ProductName.Equals(mokenaItem.ProductName)).FirstOrDefault();//&& x.Attribute.Equals(mokenaItem.Attribute)
                                if (existingItem != null)
                                {
                                    existingItem.Quantity = existingItem.Quantity + mokenaItem.Quantity;
                                }
                            }
                            //groupedItem.MokenaEventOrder.Add(mokenaItem);
                        }

                    }

                    if (model.SupplierStoreId.Equals(0) || model.SupplierStoreId.Equals(10))
                    {
                        var burridgeItems = childItem.Where(ci => this.GetStoreName(ci.OrderId, ci.Id).Equals("Burr Ridge")).Select(ci => new EventOrderProductModel()
                        {
                            Quantity = ci.Quantity,
                            ProductName = ci.Product.Name,
                            Attribute = ci.AttributeDescription,
                            PvaCollection = _productAttributeParser.ParseProductVariantAttributeValues(ci.AttributesXml).Where(pva => pva.AttributeValueType.Equals(AttributeValueType.AssociatedToProduct)).ToList(),
                            StoreName = "Burr Ridge",
                            Size = this.GetSize(ci.AttributeDescription)
                        }).ToList();

                        /*burridgeItems.GroupBy(o => o.ProductName)
                            .Select(g => g.Skip(1).Aggregate(
                            g.First(), (a, o) => { a.Quantity += o.Quantity; return a; }));

                        groupedItem.BurridgeEventOrder = burridgeItems;*/

                        foreach (var burridgeItem in burridgeItems)
                        {
                            if (!groupedItem.BurridgeEventOrder.Any(x => burridgeItem.ProductName.Equals(x.ProductName)))//&& !groupedItem.BurridgeEventOrder.Any(x => burridgeItem.Attribute.Equals(x.Attribute))
                            {
                                groupedItem.BurridgeEventOrder.Add(burridgeItem);
                            }
                            else
                            {
                                var existingItem = groupedItem.BurridgeEventOrder.Where(x => x.ProductName.Equals(burridgeItem.ProductName)).FirstOrDefault();//&& x.Attribute.Equals(burridgeItem.Attribute)
                                if (existingItem != null)
                                {
                                    existingItem.Quantity = existingItem.Quantity + burridgeItem.Quantity;
                                }
                                //groupedItem.BurridgeEventOrder.Add(burridgeItem);
                            }
                            //groupedItem.BurridgeEventOrder.Add(burridgeItem);
                        }
                    }
                }

                foreach (var attributeProductItem in groupedItem.DarienEventOrder.ToList())
                {
                    foreach (var productItem in attributeProductItem.PvaCollection)
                    {
                        var attributeAsProduct = new EventOrderProductModel();
                        attributeAsProduct.ProductName = _productService.GetProductById(productItem.AssociatedProductId).Name;
                        attributeAsProduct.Quantity = this.GetQuantity(productItem, attributeProductItem.Attribute);
                        attributeAsProduct.StoreName = "Darien";
                        groupedItem.DarienEventOrder.Add(attributeAsProduct);
                    }
                }

                foreach (var attributeProductItem in groupedItem.MokenaEventOrder.ToList())
                {
                    foreach (var productItem in attributeProductItem.PvaCollection)
                    {
                        var attributeAsProduct = new EventOrderProductModel();
                        attributeAsProduct.ProductName = _productService.GetProductById(productItem.AssociatedProductId).Name;
                        attributeAsProduct.Quantity = this.GetQuantity(productItem, attributeProductItem.Attribute);
                        attributeAsProduct.StoreName = "Mokena";
                        groupedItem.MokenaEventOrder.Add(attributeAsProduct);
                    }
                }

                foreach (var attributeProductItem in groupedItem.BurridgeEventOrder.ToList())
                {
                    foreach (var productItem in attributeProductItem.PvaCollection)
                    {
                        var attributeAsProduct = new EventOrderProductModel();
                        attributeAsProduct.ProductName = _productService.GetProductById(productItem.AssociatedProductId).Name;
                        attributeAsProduct.Quantity = this.GetQuantity(productItem, attributeProductItem.Attribute);
                        attributeAsProduct.StoreName = "Burr Ridge";
                        groupedItem.BurridgeEventOrder.Add(attributeAsProduct);
                    }
                }

                model.GroupedOrderItems.Add(groupedItem);
            }



            model.PagingFilteringContext.LoadPagedList(orders);


            if (generatePdf)
            {

                byte[] bytes = null;
                using (var stream = new MemoryStream())
                {
                    _cateringPdfService.PrintSummaryReportToPdf(stream, model, 0);
                    bytes = stream.ToArray();
                }
                return File(bytes, "application/pdf", string.Format("report_{0}.pdf", DateTime.Now.ToString()));
            }



            @ViewBag.PageNumber = pageNumber;

            return View("Report/ReportSummary", model);
        }

        [AdminAuthorize]
        public ActionResult Report(ReportSummaryModel model, int pageSize = 10, int pageNumber = 1, bool generatePdf = false)
        {


            DateTime? startDateValue = (model.CateringOrderList.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CateringOrderList.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.CateringOrderList.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CateringOrderList.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.CateringOrderList.OrderStatusId > 0 ? (OrderStatus?)(model.CateringOrderList.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.CateringOrderList.PaymentStatusId > 0 ? (PaymentStatus?)(model.CateringOrderList.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.CateringOrderList.ShippingStatusId > 0 ? (ShippingStatus?)(model.CateringOrderList.ShippingStatusId) : null;




            var orders = _cateringEventService.SearchOrdersForSummaryReport(model.SupplierStoreId, model.CateringOrderList.VendorId, 0, 0, 0,
                startDateValue, endDateValue, orderStatus,
                paymentStatus, shippingStatus, model.CateringOrderList.CustomerEmail, model.CateringOrderList.OrderGuid,
                pageNumber - 1, pageSize, model.CateringOrderList.DeliveryStartDate, model.CateringOrderList.DeliveryEndDate);



            model.CateringOrderList.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.CateringOrderList.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //payment statuses
            model.CateringOrderList.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.CateringOrderList.AvailablePaymentStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //shipping statuses
            model.CateringOrderList.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.CateringOrderList.AvailableShippingStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //stores
            model.CateringOrderList.AvailableStores.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in this.GetAllStores())//_customerService.GetCustomerRoleByStoreRole()
                model.CateringOrderList.AvailableStores.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString() });



            var reportItems = orders.GroupBy(o => o.DeliveryTime.Date).Select(o => new
                                                                        {
                                                                            OrderDate = o.Key,
                                                                            ReportOrders = o.Select(x =>
                                                                                                      {
                                                                                                          return _orderService.GetOrderById(x.OrderId);
                                                                                                      })
                                                                        });

            reportItems = reportItems.OrderByDescending(x => x.OrderDate).ToList();



            foreach (var reportItem in reportItems)
            {
                var groupedReportModel = new GroupedReportModel();
                groupedReportModel.OrderDay = reportItem.OrderDate;


                foreach (var reportOrder in reportItem.ReportOrders)
                {
                    var cateringEvent = new CateringEventModel();
                    var orderEvent = _cateringEventService.GetCateringEventByOrderId(reportOrder.Id);
                    //var impersonatedBy = _genericAttributeService.GetAttributesForEntity(reportOrder.Id, "Order");
                    

                    cateringEvent.OrderId = orderEvent.OrderId;
                    cateringEvent.OrderCreatedTime = orderEvent.OrderCreatedTime;
                    cateringEvent.DeliveryTime = orderEvent.DeliveryTime;
                    cateringEvent.CafePickup = orderEvent.CafePickup;
                    cateringEvent.SupplierStoreName = this.GetStoreNameById(orderEvent.SupplierStoreId);//_customerService.GetCustomerRoleById(orderEvent.SupplierStoreId).Name;
                    //cateringEvent.OrderedBy = impersonatedBy.Count > 0 ? String.Format("{0} ({1})", impersonatedBy.FirstOrDefault().Value, cateringEvent.SupplierStoreName) : "Customer";
                    cateringEvent.OrderNotes = reportOrder.OrderNotes.Where(on => on.DisplayToCustomer).ToList();
                    cateringEvent.OrderedBy = string.Format("{0} {1}", reportOrder.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)
                                                                     , reportOrder.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName));

                    var orderItems = new List<EventOrderProductModel>();
                    foreach (var item in reportOrder.OrderItems)
                    {
                        var orderItem = new EventOrderProductModel();
                        if (item.AttributeDescription.Contains("Size:"))
                        {
                            string[] attributeString = item.AttributeDescription.Split(new[] { "<br />" }, StringSplitOptions.None);

                            try
                            {
                                orderItem.Size = attributeString[0].Split(':')[1].Substring(0, attributeString[0].Split(':')[1].IndexOf('['));
                            }
                            catch (Exception)
                            {

                                orderItem.Size = attributeString[0].Split(':')[1];
                            }


                        }
                        orderItem.Attribute = this.GetFormattedAttributes(item.AttributeDescription);
                        orderItem.StoreName = this.GetStoreName(item.OrderId, item.Id);
                        orderItem.Quantity = item.Quantity;
                        orderItem.ProductName = item.Product.Name;
                        orderItems.Add(orderItem);
                    }
                    cateringEvent.EventOrderItems = orderItems;
                    groupedReportModel.CateringEvents.Add(cateringEvent);
                }

                groupedReportModel.CateringEvents = groupedReportModel.CateringEvents.OrderByDescending(ce => this.GetStoreByName(ce.SupplierStoreName).Id).ToList();//_customerService.GetCustomerRoleBySystemName(ce.SupplierStoreName)//new { _customerService.GetCustomerRoleBySystemName(ce.SupplierStoreName).Id, ce.DeliveryTime } 
                model.GroupedReportItems.Add(groupedReportModel);
            }

            model.GroupedReportItems = model.GroupedReportItems.OrderByDescending(gri => gri.OrderDay).ToList();



            model.PagingFilteringContext.LoadPagedList(orders);



            if (generatePdf)
            {

                byte[] bytes = null;
                using (var stream = new MemoryStream())
                {
                    _cateringPdfService.PrintReportToPdf(stream, model, 0);
                    bytes = stream.ToArray();
                }
                return File(bytes, "application/pdf", string.Format("report_{0}.pdf", DateTime.Now.ToString()));
            }

            @ViewBag.PageNumber = pageNumber;
            return View("Report/Report", model);
        }

        [NonAction]
        protected string GetFormattedAttributes(string attributes)
        {
            var attributeArray = attributes.Contains("Size:") ? Regex.Split(attributes, @"<br />").Skip(1).ToArray() : attributes.Split(new[] { "<br />" }, StringSplitOptions.None);
            //attributes.Split(new[] { "<br />" }, StringSplitOptions.None);

            //int i = 0;
            //if (attributes.Contains("Size:"))
            //    i = 1;

            for (int i = 0; i < attributeArray.Length; i++)
            {
                attributeArray[i] = attributeArray[i].Substring(attributeArray[i].IndexOf(':') + 1);
            }

            var formattedString = "&bull;" + string.Join("&bull;", attributeArray);

            if (string.IsNullOrEmpty(attributes))
                return string.Empty;
            else if (attributes.Contains("Size:") && attributeArray.Count() < 2)
                return string.Empty;
            else
                return formattedString;
        }

        #endregion


        #region Single page catering checkout(This region code is copied from one page checkout and modified)

        [NonAction]
        protected JsonResult CateringLoadStepAfterShippingMethod(List<ShoppingCartItem> cart)
        {
            //Check whether payment workflow is required
            //we ignore reward points during cart total calculation
            bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);
            if (isPaymentWorkflowRequired)
            {
                //payment is required
                var paymentMethodModel = PreparePaymentMethodModel(cart);

                if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
                        paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
                {
                    //if we have only one payment method and reward points are disabled or the current customer doesn't have any reward points
                    //so customer doesn't have to choose a payment method

                    var selectedPaymentMethodSystemName = paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName;
                    _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                            SystemCustomerAttributeNames.SelectedPaymentMethod,
                            selectedPaymentMethodSystemName, _storeContext.CurrentStore.Id);

                    var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(selectedPaymentMethodSystemName);
                    if (paymentMethodInst == null ||
                            !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                            !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                        throw new Exception("Selected payment method can't be parsed");

                    return CateringLoadStepAfterPaymentMethod(paymentMethodInst, cart);
                }
                else
                {
                    //customer have to choose a payment method
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "payment-method",
                            html = this.RenderPartialViewToString("checkout/CateringPaymentMethods", paymentMethodModel)
                        },
                        goto_section = "payment_method"
                    });
                }
            }
            else
            {
                //payment is not required
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("checkout/CateringConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }
        }

        [NonAction]
        protected JsonResult CateringLoadStepAfterPaymentMethod(IPaymentMethod paymentMethod, List<ShoppingCartItem> cart)
        {
            if (paymentMethod.SkipPaymentInfo)
            {
                //skip payment info page
                var paymentInfo = new ProcessPaymentRequest();
                //session save
                _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("checkout/CateringConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });
            }
            else
            {
                //return payment info page
                var paymenInfoModel = PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("checkout/CateringPaymentInfo", paymenInfoModel)
                    },
                    goto_section = "payment_info"
                });
            }
        }

        public ActionResult CateringPageCheckout()
        {


            //validation
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();
            if (cart.Count == 0)
                return RedirectToRoute("ShoppingCart");

            /*if(!UseOnePageCheckout())
                return RedirectToRoute("Checkout");*/

            if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                return new HttpUnauthorizedResult();

            var model = new OnePageCheckoutModel()
            {
                ShippingRequired = cart.RequiresShipping(),
                DisableBillingAddressCheckoutStep = _orderSettings.DisableBillingAddressCheckoutStep
            };

            //check if Billing address appear
            ViewBag.RequiredBillingAddress = this.RequiredBillingAddress();

            return View("checkout/CateringPageCheckout", model);
        }

        [ChildActionOnly]
        public ActionResult CateringBillingForm()
        {
            var billingAddressModel = PrepareBillingAddressModel(prePopulateNewAddressWithCustomerFields: true);
            billingAddressModel.NewAddress.CountryId = 1;
            billingAddressModel.NewAddress.StateProvinceId = 20;
            return PartialView("checkout/CateringBillingAddress", billingAddressModel);
        }

        public ActionResult CateringPopUpBillingForm()
        {
            var billingAddressModel = PrepareBillingAddressModel(prePopulateNewAddressWithCustomerFields: true);
            billingAddressModel.NewAddress.CountryId = 1;
            billingAddressModel.NewAddress.StateProvinceId = 20;
            return PartialView("checkout/CateringPopUpBillingAddress", billingAddressModel);
        }

        [ValidateInput(false)]
        public ActionResult CateringSaveBilling(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                /*if(!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId = 0;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == billingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);

                    if (!ModelState.IsValid)
                    {
                        //var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(selectedCountryId: model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "billing",
                                html = this.RenderPartialViewToString("checkout/CateringBillingAddress", billingAddressModel)
                            },
                            wrong_billing_address = true,
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                            model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                            model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                            model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                            model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        if (address.CountryId.HasValue && address.CountryId.Value > 0)
                        {
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        }
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                    var currentEvent = _cateringEventService.GetCateringEventById(CurrentEventInProgress());
                    _cateringEventService.UpdateCateringEvent(currentEvent);
                }
                    
                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("checkout/CateringConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });

            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        [ValidateInput(false)]
        public ActionResult CateringSaveBillingFromPopUp(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                /*if(!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                int billingAddressId = 0;
                int.TryParse(form["billing_address_id"], out billingAddressId);

                if (billingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == billingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutBillingAddressModel();
                    TryUpdateModel(model.NewAddress, "BillingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);

                    if (!ModelState.IsValid)
                    {
                        //var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                        //model is not valid. redisplay the form with errors
                        var billingAddressModel = PrepareBillingAddressModel(selectedCountryId: model.NewAddress.CountryId);
                        billingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "billing",
                                html = this.RenderPartialViewToString("checkout/CateringPopUpBillingAddress", billingAddressModel)
                            },
                            wrong_billing_address = true,
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                            model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                            model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                            model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                            model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);
                    if (address == null)
                    {
                        //address is not found. let's create a new one
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //some validation
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        if (address.CountryId.HasValue && address.CountryId.Value > 0)
                        {
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        }
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.BillingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                    var currentEvent = _cateringEventService.GetCateringEventById(CurrentEventInProgress());
                    _cateringEventService.UpdateCateringEvent(currentEvent);
                }

                var confirmOrderModel = PrepareConfirmOrderModel(cart);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "confirm-order",
                        html = this.RenderPartialViewToString("checkout/CateringConfirmOrder", confirmOrderModel)
                    },
                    goto_section = "confirm_order"
                });

            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        [ValidateInput(false)]
        public ActionResult CateringSaveShipping(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");


                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                int shippingAddressId = 0;
                int.TryParse(form["shipping_address_id"], out shippingAddressId);

                if (shippingAddressId > 0)
                {
                    //existing address
                    var address = _workContext.CurrentCustomer.Addresses.FirstOrDefault(a => a.Id == shippingAddressId);
                    if (address == null)
                        throw new Exception("Address can't be loaded");

                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }
                else
                {
                    //new address
                    var model = new CheckoutShippingAddressModel();
                    TryUpdateModel(model.NewAddress, "ShippingNewAddress");
                    //validate model
                    TryValidateModel(model.NewAddress);
                    if (!ModelState.IsValid)
                    {
                        //model is not valid. redisplay the form with errors
                        var shippingAddressModel = PrepareShippingAddressModel(selectedCountryId: model.NewAddress.CountryId);
                        shippingAddressModel.NewAddressPreselected = true;
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "shipping",
                                html = this.RenderPartialViewToString("checkout/CateringShippingAddress", shippingAddressModel)
                            }
                        });
                    }

                    //try to find an address with the same values (don't duplicate records)
                    var address = _workContext.CurrentCustomer.Addresses.ToList().FindAddress(
                            model.NewAddress.FirstName, model.NewAddress.LastName, model.NewAddress.PhoneNumber,
                            model.NewAddress.Email, model.NewAddress.FaxNumber, model.NewAddress.Company,
                            model.NewAddress.Address1, model.NewAddress.Address2, model.NewAddress.City,
                            model.NewAddress.StateProvinceId, model.NewAddress.ZipPostalCode, model.NewAddress.CountryId);
                    if (address == null)
                    {
                        address = model.NewAddress.ToEntity();
                        address.CreatedOnUtc = DateTime.UtcNow;
                        //little hack here (TODO: find a better solution)
                        //EF does not load navigation properties for newly created entities (such as this "Address").
                        //we have to load them manually 
                        //otherwise, "Country" property of "Address" entity will be null in shipping rate computation methods
                        if (address.CountryId.HasValue)
                            address.Country = _countryService.GetCountryById(address.CountryId.Value);
                        if (address.StateProvinceId.HasValue)
                            address.StateProvince = _stateProvinceService.GetStateProvinceById(address.StateProvinceId.Value);

                        //other null validations
                        if (address.CountryId == 0)
                            address.CountryId = null;
                        if (address.StateProvinceId == 0)
                            address.StateProvinceId = null;
                        _workContext.CurrentCustomer.Addresses.Add(address);
                    }
                    _workContext.CurrentCustomer.ShippingAddress = address;
                    _customerService.UpdateCustomer(_workContext.CurrentCustomer);
                }

                var shippingMethodModel = PrepareShippingMethodModel(cart);

                if (_shippingSettings.BypassShippingMethodSelectionIfOnlyOne &&
                        shippingMethodModel.ShippingMethods.Count == 1)
                {
                    //if we have only one shipping method, then a customer doesn't have to choose a shipping method
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                            SystemCustomerAttributeNames.SelectedShippingOption,
                            shippingMethodModel.ShippingMethods.First().ShippingOption,
                            _storeContext.CurrentStore.Id);

                    //load next step
                    return CateringLoadStepAfterShippingMethod(cart);
                }
                else
                {
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "shipping-method",
                            html = this.RenderPartialViewToString("checkout/CateringShippingMethods", shippingMethodModel)
                        },
                        goto_section = "shipping_method"
                    });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        [ValidateInput(false)]
        public ActionResult CateringSaveShippingMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                /*if(!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                //parse selected method 
                string shippingoption = form["shippingoption"];
                if (String.IsNullOrEmpty(shippingoption))
                    throw new Exception("Selected shipping method can't be parsed");
                var splittedOption = shippingoption.Split(new string[] { "___" }, StringSplitOptions.RemoveEmptyEntries);
                if (splittedOption.Length != 2)
                    throw new Exception("Selected shipping method can't be parsed");
                string selectedName = splittedOption[0];
                string shippingRateComputationMethodSystemName = splittedOption[1];

                //find it
                //performance optimization. try cache first
                var shippingOptions = _workContext.CurrentCustomer.GetAttribute<List<ShippingOption>>(SystemCustomerAttributeNames.OfferedShippingOptions, _storeContext.CurrentStore.Id);
                if (shippingOptions == null || shippingOptions.Count == 0)
                {
                    //not found? let's load them using shipping service
                    shippingOptions = _shippingService
                            .GetShippingOptions(cart, _workContext.CurrentCustomer.ShippingAddress, shippingRateComputationMethodSystemName, _storeContext.CurrentStore.Id)
                            .ShippingOptions
                            .ToList();
                }
                else
                {
                    //loaded cached results. let's filter result by a chosen shipping rate computation method
                    shippingOptions = shippingOptions.Where(so => so.ShippingRateComputationMethodSystemName.Equals(shippingRateComputationMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                            .ToList();
                }

                var shippingOption = shippingOptions
                        .Find(so => !String.IsNullOrEmpty(so.Name) && so.Name.Equals(selectedName, StringComparison.InvariantCultureIgnoreCase));
                if (shippingOption == null)
                    throw new Exception("Selected shipping method can't be loaded");

                //save
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer, SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, _storeContext.CurrentStore.Id);

                //load next step
                return CateringLoadStepAfterShippingMethod(cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        [ChildActionOnly]
        public ActionResult CateringLoadShippingMethod()
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");



                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                //model
                var model = PrepareShippingMethodModel(cart);

                return PartialView("checkout/CateringShippingMethods", model);

            }

            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }

        [ChildActionOnly]
        public ActionResult CateringLoadPaymentMethod()
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                /*if(!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                if (!cart.RequiresShipping())
                    throw new Exception("Shipping is not required");

                //Check whether payment workflow is required
                //we ignore reward points during cart total calculation
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart, true);

                var paymentMethodModel = PreparePaymentMethodModel(cart);

                return PartialView("checkout/CateringPaymentMethods", paymentMethodModel);
            }

            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }

        [ValidateInput(false)]
        public ActionResult CateringSavePaymentMethod(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                /*if(!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                string paymentmethod = form["paymentmethod"];
                //payment method 
                if (String.IsNullOrEmpty(paymentmethod))
                    throw new Exception("Selected payment method can't be parsed");


                var model = new CheckoutPaymentMethodModel();
                TryUpdateModel(model);

                //reward points
                if (_rewardPointsSettings.Enabled)
                {
                    _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                            SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, model.UseRewardPoints,
                            _storeContext.CurrentStore.Id);
                }

                //Check whether payment workflow is required
                bool isPaymentWorkflowRequired = IsPaymentWorkflowRequired(cart);
                if (!isPaymentWorkflowRequired)
                {
                    //payment is not required
                    _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                            SystemCustomerAttributeNames.SelectedPaymentMethod, null, _storeContext.CurrentStore.Id);

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("checkout/CateringConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
                if (paymentMethodInst == null ||
                        !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
                        !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, _storeContext.CurrentStore.Id))
                    throw new Exception("Selected payment method can't be parsed");

                //save
                _genericAttributeService.SaveAttribute<string>(_workContext.CurrentCustomer,
                        SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, _storeContext.CurrentStore.Id);

                return CateringLoadStepAfterPaymentMethod(paymentMethodInst, cart);
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        [ValidateInput(false)]
        public ActionResult CateringSavePaymentInfo(FormCollection form)
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                /*if(!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                var paymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                        SystemCustomerAttributeNames.SelectedPaymentMethod,
                        _genericAttributeService, _storeContext.CurrentStore.Id);
                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);
                if (paymentMethod == null)
                    throw new Exception("Payment method is not selected");

                var paymentControllerType = paymentMethod.GetControllerType();
                var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
                var warnings = paymentController.ValidatePaymentForm(form);
                foreach (var warning in warnings)
                    ModelState.AddModelError("", warning);
                if (ModelState.IsValid)
                {

                    //get payment info
                    var paymentInfo = paymentController.GetPaymentInfo(form);
                    //session save
                    //paymentInfo.StoreId = CurrentEventInProgress()


                    _httpContext.Session["OrderPaymentInfo"] = paymentInfo;

                    if (this.RequiredBillingAddress() && !paymentMethodSystemName.Equals("Payments.PayInStore"))
                    {
                        var billingAddressModel = PrepareBillingAddressModel(prePopulateNewAddressWithCustomerFields: true);
                        return Json(new
                        {
                            update_section = new UpdateSectionJsonModel()
                            {
                                name = "billing",
                                html = this.RenderPartialViewToString("checkout/CateringBillingAddress", billingAddressModel)
                            },
                            goto_section = "billing"
                        });
                    }

                    var confirmOrderModel = PrepareConfirmOrderModel(cart);
                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("checkout/CateringConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }

                //If we got this far, something failed, redisplay form
                var paymenInfoModel = PreparePaymentInfoModel(paymentMethod);
                return Json(new
                {
                    update_section = new UpdateSectionJsonModel()
                    {
                        name = "payment-info",
                        html = this.RenderPartialViewToString("checkout/CateringPaymentInfo", paymenInfoModel)
                    }
                });
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        [ValidateInput(false)]
        public ActionResult CateringConfirmOrder()
        {
            try
            {
                //validation
                var cart = _workContext.CurrentCustomer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();
                if (cart.Count == 0)
                    throw new Exception("Your cart is empty");

                /*if(!UseOnePageCheckout())
                    throw new Exception("One page checkout is disabled");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    throw new Exception("Anonymous checkout is not allowed");

                //prevent 2 orders being placed within an X seconds time frame
                if (!IsMinimumOrderPlacementIntervalValid(_workContext.CurrentCustomer))
                    throw new Exception(_localizationService.GetResource("Checkout.MinOrderPlacementInterval"));

                

                //place order
                var processPaymentRequest = _httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
                if (processPaymentRequest == null)
                {
                    //Check whether payment workflow is required
                    if (IsPaymentWorkflowRequired(cart))
                    {
                        throw new Exception("Payment information is not entered");
                    }
                    else
                        processPaymentRequest = new ProcessPaymentRequest();
                }

                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;

                //var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
                //_httpContext.Session["CateringStoreId"] = _cateringEventService.GetCateringEventById(currentCateringEventId).SupplierStoreId;

                processPaymentRequest.CustomerId = _workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _workContext.CurrentCustomer.GetAttribute<string>(
                        SystemCustomerAttributeNames.SelectedPaymentMethod,
                        _genericAttributeService, _storeContext.CurrentStore.Id);

                //Set store address as shipping address when cafepickup true
                var currentEvent = _cateringEventService.GetCateringEventById(CurrentEventInProgress());
                var storeAddressId = currentEvent.AddressId;

                if (currentEvent.CafePickup)
                {
                    _workContext.CurrentCustomer.ShippingAddress = _addressService.GetAddressById(storeAddressId);
                    //save store pickup info in session to be used in order placed email
                    _httpContext.Session["PickUpStore"] = this.GetStoreNameById(currentEvent.SupplierStoreId);//_customerService.GetCustomerRoleById(currentEvent.SupplierStoreId).Name;
                }

                _httpContext.Session["DeliveryTime"] = currentEvent.DeliveryTime.ToString("dddd dd MMMM h:mm tt yyyy");
                //avoid null shipping address error specially for catering plugin done by Razib from Brainstation23
                if (_workContext.CurrentCustomer.ShippingAddress == null)// && _workContext.CurrentCustomer.IsGuest()
                {
                    if (_workContext.CurrentCustomer.BillingAddress != null)
                    {
                        _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.BillingAddress;
                    }
                    else if (_workContext.CurrentCustomer.Addresses.Any())
                    {
                        _workContext.CurrentCustomer.ShippingAddress = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
                    }
                    else
                    {
                        _workContext.CurrentCustomer.ShippingAddress = _addressService.GetAddressById(storeAddressId);
                    }
                }

                if (_workContext.CurrentCustomer.BillingAddress == null)
                {
                    if (_workContext.CurrentCustomer.Addresses.Any())
                    {
                        _workContext.CurrentCustomer.BillingAddress = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
                    }
                    else
                    {
                        _workContext.CurrentCustomer.BillingAddress = _addressService.GetAddressById(storeAddressId);
                    }
                }

                var cateringOrderNotes = new List<string>();
                cateringOrderNotes = _genericAttributeService.GetAttributesForEntity(CurrentEventInProgress(), "EventOrderNote").Select(ga => ga.Value).ToList();

                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest, cateringOrderNotes);
                
                if (placeOrderResult.Success)
                {
                    _httpContext.Session["OrderPaymentInfo"] = null;
                    var postProcessPaymentRequest = new PostProcessPaymentRequest()
                    {
                        Order = placeOrderResult.PlacedOrder
                    };

                    
                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(placeOrderResult.PlacedOrder.PaymentMethodSystemName);



                    if (paymentMethod != null)
                    {
                        if (paymentMethod.PaymentMethodType == PaymentMethodType.Redirection)
                        {
                            //Redirection will not work because it's AJAX request.
                            //That's why we don't process it here (we redirect a user to another page where he'll be redirected)

                            //redirect
                            return Json(new
                            {
                                redirect = string.Format("{0}checkout/CateringCompleteRedirectionPayment", _webHelper.GetStoreLocation())
                            });
                        }
                        else
                        {
                            _paymentService.PostProcessPayment(postProcessPaymentRequest);
                            //success
                            return Json(new
                            {
                                success = 1
                            });
                        }
                    }
                    else
                    {
                        //payment method could be null if order total is 0

                        //success
                        return Json(new
                        {
                            success = 1
                        });
                    }
                }
                else
                {
                    //error
                    var confirmOrderModel = new CheckoutConfirmModel();
                    foreach (var error in placeOrderResult.Errors)
                        confirmOrderModel.Warnings.Add(error);

                    return Json(new
                    {
                        update_section = new UpdateSectionJsonModel()
                        {
                            name = "confirm-order",
                            html = this.RenderPartialViewToString("checkout/CateringConfirmOrder", confirmOrderModel)
                        },
                        goto_section = "confirm_order"
                    });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Json(new
                {
                    error = 1,
                    message = exc.Message
                });
            }
        }

        public ActionResult CateringCompleteRedirectionPayment()
        {
            try
            {
                //validation
                /*if(!UseOnePageCheckout())
                    return RedirectToRoute("HomePage");*/

                if ((_workContext.CurrentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed))
                    return new HttpUnauthorizedResult();

                //get the order
                var order = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                        .FirstOrDefault();
                if (order == null)
                    return RedirectToRoute("HomePage");


                var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                if (paymentMethod == null)
                    return RedirectToRoute("HomePage");
                if (paymentMethod.PaymentMethodType != PaymentMethodType.Redirection)
                    return RedirectToRoute("HomePage");

                //ensure that order has been just placed
                if ((DateTime.UtcNow - order.CreatedOnUtc).TotalMinutes > 3)
                    return RedirectToRoute("HomePage");


                //Redirection will not work on one page checkout page because it's AJAX request.
                //That's why we process it here
                var postProcessPaymentRequest = new PostProcessPaymentRequest()
                {
                    Order = order
                };

                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                {
                    //redirection or POST has been done in PostProcessPayment
                    return Content("Redirected");
                }
                else
                {
                    //if no redirection has been done (to a third-party payment page)
                    //theoretically it's not possible
                    return RedirectToRoute("CheckoutCompleted", new
                    {
                        orderId = order.Id
                    });
                }
            }
            catch (Exception exc)
            {
                _logger.Warning(exc.Message, exc, _workContext.CurrentCustomer);
                return Content(exc.Message);
            }
        }

        public ActionResult OrderNote(int eventID = 0, string message = "")
        {
            var cateringOrderNoteModel = new OrderNoteModel();
            cateringOrderNoteModel.EventId = eventID;
            cateringOrderNoteModel.EventOrderNotes = _genericAttributeService.GetAttributesForEntity(eventID, "EventOrderNote").Select(ga => ga.Value).ToList();

            
            

            if(!string.IsNullOrEmpty(message))
            {
                GenericAttribute ga = new GenericAttribute();
                ga.KeyGroup = "EventOrderNote";
                ga.EntityId = eventID;
                ga.Key = "Note";
                ga.Value = message;

                _genericAttributeService.InsertAttribute(ga);
                
                return Content(ga.Value);
            }

            return View("OrderNotes", cateringOrderNoteModel);
        }

        
        #endregion


        #region Searching



        [NopHttpsRequirement(SslRequirement.No)]
        [ValidateInput(false)]
        public ActionResult SearchFood(string q = "", int categoryId = 19, int pageSize = 0, int pageNumber = 0)
        {
            var categoryIds = new List<int>();

            categoryIds.Add(categoryId);
            categoryIds.AddRange(GetChildCategoryIds(categoryId));

            if (pageSize <= 0)
                pageSize = _catalogSettings.SearchPageProductsPerPage;
            if (pageNumber <= 0)
                pageNumber = 1;

            IPagedList<Product> products = new PagedList<Product>(new List<Product>(), 0, 1);



            products = _productService.SearchProducts(categoryIds: categoryIds,
                                                                                                                storeId: _storeContext.CurrentStore.Id,
                                                                                                                visibleIndividuallyOnly: true,
                                                                                                                pageIndex: pageNumber - 1,
                                                                                                                pageSize: pageSize,
                                                                                                                keywords: q);

            var model = new CateringSearchModel();

            model.Products = PrepareProductOverviewModels(products).ToList();

            model.PagingFilteringContext.LoadPagedList(products);

            ViewBag.SearchTitle = String.Format("You have searched for '{0}' and these are the results", q);
            return View("CateringProduct", model);
        }

        [ChildActionOnly]
        public ActionResult SearchBox()
        {
            var model = new SearchBoxModel()
            {
                AutoCompleteEnabled = _catalogSettings.ProductSearchAutoCompleteEnabled,
                ShowProductImagesInSearchAutoComplete = _catalogSettings.ShowProductImagesInSearchAutoComplete,
                SearchTermMinimumLength = _catalogSettings.ProductSearchTermMinimumLength
            };
            return PartialView("CateringSearchBox", model);
        }

        public ActionResult SearchTermAutoComplete(string term)
        {
            if (String.IsNullOrWhiteSpace(term) || term.Length < _catalogSettings.ProductSearchTermMinimumLength)
                return Content("");

            //products
            var productNumber = _catalogSettings.ProductSearchAutoCompleteNumberOfProducts > 0 ?
                    _catalogSettings.ProductSearchAutoCompleteNumberOfProducts : 10;

            var products = _productService.SearchProducts(
                    storeId: _storeContext.CurrentStore.Id,
                    keywords: term,
                    searchSku: false,
                    languageId: _workContext.WorkingLanguage.Id,
                    visibleIndividuallyOnly: true,
                    pageSize: productNumber);

            var models = this.PrepareProductOverviewModels(products, false, _catalogSettings.ShowProductImagesInSearchAutoComplete, _mediaSettings.AutoCompleteSearchThumbPictureSize).ToList();
            var result = (from p in models
                          select new
                          {
                              label = p.Name,
                              producturl = Url.RouteUrl("Product", new
                              {
                                  SeName = p.SeName
                              }),
                              productpictureurl = p.DefaultPictureModel.ImageUrl
                          })
                                        .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}