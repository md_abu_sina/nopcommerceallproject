using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Other.Catering.Domain;

namespace Nop.Plugin.Other.Catering.Data

{
		public class CateringEmailMap : EntityTypeConfiguration<CateringEmail>
    {
				public CateringEmailMap()
        {
						ToTable("Nop_CateringEmail");

            //Map the primary key
            HasKey(m => m.Id);
            //Map the additional properties
						Property(m => m.StoreId);
						Property(m => m.EmailAddress);
						Property(m => m.EmailAddressType);
						Property(m => m.StoreName);
        }
    }

}
