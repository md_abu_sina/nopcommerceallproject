using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Other.Catering.Domain;

namespace Nop.Plugin.Other.Catering.Data

{
    public class CateringEventMap : EntityTypeConfiguration<CateringEvent>
    {
				public CateringEventMap()
        {
						ToTable("Nop_CateringEvent");

            //Map the primary key
            HasKey(m => m.Id);
            //Map the additional properties
						Property(m => m.NumberOfPeople);
						Property(m => m.CafePickup);
						Property(m => m.DeliveryTime);
						Property(m => m.OrderCreatedTime);
						Property(m => m.OrderUpdatedTime);
						Property(m => m.AddressId);
						Property(m => m.CurrentCustomerId);
						Property(m => m.OrderId);
						Property(m => m.CateringOrderStatus);
						Property(m => m.SecondaryStores);
						Property(m => m.PdfInvoicePath);
        }
    }

}
