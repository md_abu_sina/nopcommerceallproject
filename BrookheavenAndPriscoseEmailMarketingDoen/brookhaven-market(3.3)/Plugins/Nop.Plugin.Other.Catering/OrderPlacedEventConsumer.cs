﻿using System;
using System.Collections.Generic;
using System.Web;
using Nop.Core;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using Nop.Plugin.Other.Catering.Domain;
using Nop.Plugin.Other.Catering.Services;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Stores;
using System.Linq;
using Nop.Services.Catalog;
using System.Text;
using System.Globalization;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Shipping;
using Nop.Services.Helpers;
using Nop.Services.Directory;
using Nop.Services.Media;
using Nop.Services.Payments;
using Nop.Core.Domain.Catalog;
using Nop.Services.Customers;
using System.IO;
using Nop.Services.Common;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.Other.Catering
{
    public class OrderPlacedEventConsumer : IConsumer<OrderPlacedEvent>
    {
				#region Fields
				private readonly IPluginFinder _pluginFinder;
				private readonly IOrderService _orderService;
				private readonly ICateringEventService _cateringEventService;
				private readonly ICateringPdfService _cateringPdfService;
				private readonly HttpContextBase _httpContext;
				private readonly IStoreContext _storeContext;
				private readonly ICateringEmailService _cateringEmailService;


				private readonly IMessageTemplateService _messageTemplateService;//
				private readonly IQueuedEmailService _queuedEmailService;//
				private readonly ILanguageService _languageService;//
				private readonly ITokenizer _tokenizer;//
				private readonly IEmailAccountService _emailAccountService;
				private readonly IMessageTokenProvider _messageTokenProvider;//
				private readonly IStoreService _storeService;//
				//private readonly IStoreContext _storeContext;
				private readonly EmailAccountSettings _emailAccountSettings;
				private readonly IEventPublisher _eventPublisher;//
				private readonly ISpecificationAttributeService _specificationAttributeService;//
				private readonly ICustomerService _customerService;//

				//private readonly ILanguageService _languageService;
				private readonly ILocalizationService _localizationService;//
				private readonly IDateTimeHelper _dateTimeHelper;//
				private readonly IPriceFormatter _priceFormatter;//
				private readonly ICurrencyService _currencyService;//
				private readonly IWorkContext _workContext;//
				private readonly IDownloadService _downloadService;//
				//private readonly IOrderService _orderService;
				private readonly IPaymentService _paymentService;//
				private readonly IProductAttributeParser _productAttributeParser;//
				//private readonly IStoreService _storeService;
				//private readonly IStoreContext _storeContext;

				private readonly MessageTemplatesSettings _templatesSettings;//
				private readonly CatalogSettings _catalogSettings;//
				private readonly TaxSettings _taxSettings;//

				private readonly IGenericAttributeService _genericAttributeService;


				#endregion

				#region Ctor
				public OrderPlacedEventConsumer(IPluginFinder pluginFinder, IMessageTemplateService messageTemplateService, IQueuedEmailService queuedEmailService, ISpecificationAttributeService specificationAttributeService,
																				HttpContextBase httpContext, IEmailAccountService emailAccountService, ILanguageService languageService, ITokenizer tokenizer, ILocalizationService localizationService,
																				IMessageTokenProvider messageTokenProvider, IStoreService storeService, IStoreContext storeContext, EmailAccountSettings emailAccountSettings, ICustomerService customerService,
																				IOrderService orderService, ICateringEventService cateringEventService, ICateringEmailService cateringEmailService, IEventPublisher eventPublisher, IDownloadService downloadService,
																				IDateTimeHelper dateTimeHelper, IPriceFormatter priceFormatter, ICurrencyService currencyService, IWorkContext workContext, IPaymentService paymentService, IGenericAttributeService genericAttributeService,
																				IProductAttributeParser productAttributeParser, MessageTemplatesSettings templatesSettings, CatalogSettings catalogSettings, TaxSettings taxSettings, ICateringPdfService cateringPdfService)
				{
					this._pluginFinder = pluginFinder;
					this._orderService = orderService;
					this._cateringEventService = cateringEventService;
					this._cateringPdfService = cateringPdfService;
					this._httpContext = httpContext;
					this._cateringEmailService = cateringEmailService;
					this._storeContext = storeContext;
					this._messageTemplateService = messageTemplateService;
					this._queuedEmailService = queuedEmailService;
					this._languageService = languageService;
					this._tokenizer = tokenizer;
					this._messageTokenProvider = messageTokenProvider;
					this._storeService = storeService;
					this._eventPublisher = eventPublisher;
					this._emailAccountService = emailAccountService;
					this._specificationAttributeService = specificationAttributeService;
					this._emailAccountSettings = emailAccountSettings;
					this._customerService = customerService;

					this._localizationService = localizationService;
					this._dateTimeHelper = dateTimeHelper;
					this._priceFormatter = priceFormatter;
					this._currencyService = currencyService;
					this._workContext = workContext;
					this._downloadService = downloadService;
					this._paymentService = paymentService;
					this._productAttributeParser = productAttributeParser;

					this._templatesSettings = templatesSettings;
					this._catalogSettings = catalogSettings;
					this._taxSettings = taxSettings;
					this._eventPublisher = eventPublisher;
					this._genericAttributeService = genericAttributeService;
				} 
				#endregion


				#region Utilities

				/// <summary>
				/// Get store URL
				/// </summary>
				/// <param name="storeId">Store identifier; Pass 0 to load URL of the current store</param>
				/// <param name="useSsl">Use SSL</param>
				/// <returns></returns>
				protected virtual string GetStoreUrl(int storeId = 0, bool useSsl = false)
				{
					var store = _storeService.GetStoreById(storeId) ?? _storeContext.CurrentStore;

					if(store == null)
						throw new Exception("No store could be loaded");

					return useSsl ? store.SecureUrl : store.Url;
				}

				/// <summary>
				/// Convert a collection to a HTML table
				/// </summary>
				/// <param name="order">Order</param>
				/// <param name="languageId">Language identifier</param>
				/// <param name="vendorId">Vendor identifier (used to limit products by vendor</param>
				/// <returns>HTML table of products</returns>
				protected virtual string ProductListToHtmlTable(Order order, int languageId, Dictionary<int, string> secondaryStoreInfo, int vendorId)
				{
					var result = "";

					var language = _languageService.GetLanguageById(languageId);

					var sb = new StringBuilder();
					sb.AppendLine("<table border=\"0\" style=\"width:100%;\">");

					#region Products
					sb.AppendLine(string.Format("<tr style=\"background-color:{0};text-align:center;\">", _templatesSettings.Color1));
					sb.AppendLine(string.Format("<th>{0}</th>", _localizationService.GetResource("Messages.Order.Product(s).Name", languageId)));
					sb.AppendLine(string.Format("<th>{0}</th>", _localizationService.GetResource("Messages.Order.Product(s).Price", languageId)));
					sb.AppendLine(string.Format("<th>{0}</th>", _localizationService.GetResource("Messages.Order.Product(s).Quantity", languageId)));
					sb.AppendLine(string.Format("<th>{0}</th>", _localizationService.GetResource("Messages.Order.Product(s).SecondaryStore", languageId)));
					sb.AppendLine(string.Format("<th>{0}</th>", _localizationService.GetResource("Messages.Order.Product(s).Total", languageId)));
					sb.AppendLine("</tr>");

					var table = order.OrderItems.ToList();
					for(int i = 0; i <= table.Count - 1; i++)
					{
						var orderItem = table[i];
						var product = orderItem.Product;
						if(product == null)
							continue;

						if(vendorId > 0 && product.VendorId != vendorId)
							continue;

						sb.AppendLine(string.Format("<tr style=\"background-color: {0};text-align: center;\">", _templatesSettings.Color2));
						//product name
						string productName = product.GetLocalized(x => x.Name, languageId);

						sb.AppendLine("<td style=\"padding: 0.6em 0.4em;text-align: left;\">" + HttpUtility.HtmlEncode(productName));
						//add download link
						if(_downloadService.IsDownloadAllowed(orderItem))
						{
							//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
							string downloadUrl = string.Format("{0}download/getdownload/{1}", GetStoreUrl(order.StoreId), orderItem.OrderItemGuid);
							string downloadLink = string.Format("<a class=\"link\" href=\"{0}\">{1}</a>", downloadUrl, _localizationService.GetResource("Messages.Order.Product(s).Download", languageId));
							sb.AppendLine("&nbsp;&nbsp;(");
							sb.AppendLine(downloadLink);
							sb.AppendLine(")");
						}
						//attributes
						if(!String.IsNullOrEmpty(orderItem.AttributeDescription))
						{
							sb.AppendLine("<br />");
							sb.AppendLine(orderItem.AttributeDescription);
						}
						//sku
						if(_catalogSettings.ShowProductSku)
						{
							var sku = product.FormatSku(orderItem.AttributesXml, _productAttributeParser);
							if(!String.IsNullOrEmpty(sku))
							{
								sb.AppendLine("<br />");
								sb.AppendLine(string.Format(_localizationService.GetResource("Messages.Order.Product(s).SKU", languageId), HttpUtility.HtmlEncode(sku)));
							}
						}
						sb.AppendLine("</td>");

						string unitPriceStr = string.Empty;
						//excluding tax
						var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
						unitPriceStr = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
						/*if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
						{
							//including tax
							var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
							unitPriceStr = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, true);
						}
						else
						{
							//excluding tax
							var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
							unitPriceStr = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
						}*/
						sb.AppendLine(string.Format("<td style=\"padding: 0.6em 0.4em;text-align: right;\">{0}</td>", unitPriceStr));

						sb.AppendLine(string.Format("<td style=\"padding: 0.6em 0.4em;text-align: center;\">{0}</td>", orderItem.Quantity));

						string secondarySupplier = secondaryStoreInfo.ContainsKey(orderItem.Id) ? secondaryStoreInfo[orderItem.Id] : string.Empty;
						sb.AppendLine(string.Format("<td style=\"padding: 0.6em 0.4em;text-align: right;font-weight: bold;\">{0}</td>", secondarySupplier));

						string priceStr = string.Empty;
						//excluding tax
						var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
						priceStr = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
						/*if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
						{
							//including tax
							var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
							priceStr = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, true);
						}
						else
						{
							//excluding tax
							var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
							priceStr = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
						}*/
						sb.AppendLine(string.Format("<td style=\"padding: 0.6em 0.4em;text-align: right;\">{0}</td>", priceStr));

						sb.AppendLine("</tr>");
					}
					#endregion

					if(vendorId == 0)
					{
						//we render checkout attributes and totals only for store owners (hide for vendors)

						#region Checkout Attributes

						if(!String.IsNullOrEmpty(order.CheckoutAttributeDescription))
						{
							sb.AppendLine("<tr><td style=\"text-align:right;\" colspan=\"1\">&nbsp;</td><td colspan=\"3\" style=\"text-align:right\">");
							sb.AppendLine(order.CheckoutAttributeDescription);
							sb.AppendLine("</td></tr>");
						}

						#endregion

						#region Totals

						//subtotal
						string cusSubTotal = string.Empty;
						bool dislaySubTotalDiscount = false;
						string cusSubTotalDiscount = string.Empty;
						if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
						{
							//including tax

							//subtotal
							var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
							cusSubTotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, true);
							//discount (applied to order subtotal)
							var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
							if(orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
							{
								cusSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, true);
								dislaySubTotalDiscount = true;
							}
						}
						else
						{
							//exсluding tax

							//subtotal
							var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
							cusSubTotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
							//discount (applied to order subtotal)
							var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
							if(orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
							{
								cusSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
								dislaySubTotalDiscount = true;
							}
						}

						//shipping, payment method fee
						string cusShipTotal = string.Empty;
						string cusPaymentMethodAdditionalFee = string.Empty;
						var taxRates = new SortedDictionary<decimal, decimal>();
						string cusTaxTotal = string.Empty;
						string cusDiscount = string.Empty;
						string cusTotal = string.Empty;
						if(order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
						{
							//including tax

							//shipping
							var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
							cusShipTotal = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, true);
							//payment method additional fee
							var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
							cusPaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, true);
						}
						else
						{
							//excluding tax

							//shipping
							var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
							cusShipTotal = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
							//payment method additional fee
							var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
							cusPaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, language, false);
						}

						//shipping
						bool dislayShipping = order.ShippingStatus != ShippingStatus.ShippingNotRequired;

						//payment method fee
						bool displayPaymentMethodFee = true;
						if(order.PaymentMethodAdditionalFeeExclTax == decimal.Zero)
						{
							displayPaymentMethodFee = false;
						}

						//tax
						bool displayTax = true;
						bool displayTaxRates = true;
						if(_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
						{
							displayTax = false;
							displayTaxRates = false;
						}
						else
						{
							if(order.OrderTax == 0 && _taxSettings.HideZeroTax)
							{
								displayTax = false;
								displayTaxRates = false;
							}
							else
							{
								taxRates = new SortedDictionary<decimal, decimal>();
								foreach(var tr in order.TaxRatesDictionary)
									taxRates.Add(tr.Key, _currencyService.ConvertCurrency(tr.Value, order.CurrencyRate));

								displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
								displayTax = !displayTaxRates;

								var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
								string taxStr = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, language);
								cusTaxTotal = taxStr;
							}
						}

						//discount
						bool dislayDiscount = false;
						if(order.OrderDiscount > decimal.Zero)
						{
							var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
							cusDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, language);
							dislayDiscount = true;
						}

						//total
						var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
						cusTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, language);




						//subtotal
						sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, _localizationService.GetResource("Messages.Order.SubTotal", languageId), cusSubTotal));

						//discount (applied to order subtotal)
						if(dislaySubTotalDiscount)
						{
							sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, _localizationService.GetResource("Messages.Order.SubTotalDiscount", languageId), cusSubTotalDiscount));
						}


						//shipping
						if(dislayShipping)
						{
							sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, _localizationService.GetResource("Messages.Order.Shipping", languageId), cusShipTotal));
						}

						//payment method fee
						if(displayPaymentMethodFee)
						{
							string paymentMethodFeeTitle = _localizationService.GetResource("Messages.Order.PaymentMethodAdditionalFee", languageId);
							sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, paymentMethodFeeTitle, cusPaymentMethodAdditionalFee));
						}

						//tax
						if(displayTax)
						{
							sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, _localizationService.GetResource("Messages.Order.Tax", languageId), cusTaxTotal));
						}
						if(displayTaxRates)
						{
							foreach(var item in taxRates)
							{
								string taxRate = String.Format(_localizationService.GetResource("Messages.Order.TaxRateLine"), _priceFormatter.FormatTaxRate(item.Key));
								string taxValue = _priceFormatter.FormatPrice(item.Value, true, order.CustomerCurrencyCode, false, language);
								sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, taxRate, taxValue));
							}
						}

						//discount
						if(dislayDiscount)
						{
							sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, _localizationService.GetResource("Messages.Order.TotalDiscount", languageId), cusDiscount));
						}

						//gift cards
						var gcuhC = order.GiftCardUsageHistory;
						foreach(var gcuh in gcuhC)
						{
							string giftCardText = String.Format(_localizationService.GetResource("Messages.Order.GiftCardInfo", languageId), HttpUtility.HtmlEncode(gcuh.GiftCard.GiftCardCouponCode));
							string giftCardAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, language);
							sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, giftCardText, giftCardAmount));
						}

						//reward points
						if(order.RedeemedRewardPointsEntry != null)
						{
							string rpTitle = string.Format(_localizationService.GetResource("Messages.Order.RewardPoints", languageId), -order.RedeemedRewardPointsEntry.Points);
							string rpAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, language);
							sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, rpTitle, rpAmount));
						}

						//total
						sb.AppendLine(string.Format("<tr style=\"text-align:right;\"><td>&nbsp;</td><td colspan=\"2\" style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{1}</strong></td> <td style=\"background-color: {0};padding:0.6em 0.4 em;\"><strong>{2}</strong></td></tr>", _templatesSettings.Color3, _localizationService.GetResource("Messages.Order.OrderTotal", languageId), cusTotal));
						#endregion

					}

					sb.AppendLine("</table>");
					result = sb.ToString();
					return result;
				}

                /// <summary>
                /// Convert a collection to a HTML table
                /// </summary>
                /// <param name="order">Order</param>
                /// <param name="languageId">Language identifier</param>
                /// <param name="vendorId">Vendor identifier (used to limit products by vendor</param>
                /// <returns>HTML table of products</returns>
                protected virtual string OrderNotesToHtml(Order order, int languageId)
                {
                    var result = "";

                    var language = _languageService.GetLanguageById(languageId);
                    var eventOrderNotes = order.OrderNotes.Where(on => on.DisplayToCustomer.Equals(true));
                    var sb = new StringBuilder();
                    sb.AppendLine("<p>");

                    sb.AppendLine(string.Format("<b>{0} :</b>", _localizationService.GetResource("Order.Notes", languageId)));

                        sb.AppendLine("<ol>");

                            foreach (var note in eventOrderNotes)
                            {
                                sb.AppendLine(string.Format("<li>{0}</li>", note.Note));
                            }
                    
                        sb.AppendLine("</ol>");
                    
                    sb.AppendLine("</p>");


                    result = sb.ToString();
                    return result;
                }

				protected virtual void AddOrderTokens(IList<Token> tokens, Order order, int languageId, Dictionary<int, string> secondaryStoreInfo = null, int vendorId = 0)
				{
					tokens.Add(new Token("Order.OrderNumber", order.Id.ToString()));

					tokens.Add(new Token("Order.CustomerFullName", string.Format("{0} {1}", order.BillingAddress.FirstName, order.BillingAddress.LastName)));
					tokens.Add(new Token("Order.CustomerEmail", order.BillingAddress.Email));

                    
					tokens.Add(new Token("Order.BillingFirstName", order.BillingAddress.FirstName));
					tokens.Add(new Token("Order.BillingLastName", order.BillingAddress.LastName));
					tokens.Add(new Token("Order.BillingPhoneNumber", order.BillingAddress.PhoneNumber));
					tokens.Add(new Token("Order.BillingEmail", order.BillingAddress.Email));
					tokens.Add(new Token("Order.BillingFaxNumber", order.BillingAddress.FaxNumber));
					tokens.Add(new Token("Order.BillingCompany", order.BillingAddress.Company));
					tokens.Add(new Token("Order.BillingAddress1", order.BillingAddress.Address1));
					tokens.Add(new Token("Order.BillingAddress2", order.BillingAddress.Address2));
					tokens.Add(new Token("Order.BillingCity", order.BillingAddress.City));
					tokens.Add(new Token("Order.BillingStateProvince", order.BillingAddress.StateProvince != null ? order.BillingAddress.StateProvince.GetLocalized(x => x.Name) : ""));
					tokens.Add(new Token("Order.BillingZipPostalCode", order.BillingAddress.ZipPostalCode));
					tokens.Add(new Token("Order.BillingCountry", order.BillingAddress.Country != null ? order.BillingAddress.Country.GetLocalized(x => x.Name) : ""));

					tokens.Add(new Token("Order.ShippingMethod", order.ShippingMethod));
					tokens.Add(new Token("Order.ShippingFirstName", order.ShippingAddress != null ? order.ShippingAddress.FirstName : ""));
					tokens.Add(new Token("Order.ShippingLastName", order.ShippingAddress != null ? order.ShippingAddress.LastName : ""));
					tokens.Add(new Token("Order.ShippingPhoneNumber", order.ShippingAddress != null ? order.ShippingAddress.PhoneNumber : ""));
					tokens.Add(new Token("Order.ShippingEmail", order.ShippingAddress != null ? order.ShippingAddress.Email : ""));
					tokens.Add(new Token("Order.ShippingFaxNumber", order.ShippingAddress != null ? order.ShippingAddress.FaxNumber : ""));
					tokens.Add(new Token("Order.ShippingCompany", order.ShippingAddress != null ? order.ShippingAddress.Company : ""));
					tokens.Add(new Token("Order.ShippingAddress1", order.ShippingAddress != null ? order.ShippingAddress.Address1 : ""));
					tokens.Add(new Token("Order.ShippingAddress2", order.ShippingAddress != null ? order.ShippingAddress.Address2 : ""));
					tokens.Add(new Token("Order.ShippingCity", order.ShippingAddress != null ? order.ShippingAddress.City : ""));
					tokens.Add(new Token("Order.ShippingStateProvince", order.ShippingAddress != null && order.ShippingAddress.StateProvince != null ? order.ShippingAddress.StateProvince.GetLocalized(x => x.Name) : ""));
					tokens.Add(new Token("Order.ShippingZipPostalCode", order.ShippingAddress != null ? order.ShippingAddress.ZipPostalCode : ""));
					tokens.Add(new Token("Order.ShippingCountry", order.ShippingAddress != null && order.ShippingAddress.Country != null ? order.ShippingAddress.Country.GetLocalized(x => x.Name) : ""));
					tokens.Add(new Token("Order.PickUpStore", _httpContext.Session["PickUpStore"] != null ? string.Format("Pickup Store : {0}", _httpContext.Session["PickUpStore"].ToString()) : ""));

					var cateringEvent = _cateringEventService.GetCateringEventByOrderId(order.Id);
					tokens.Add(new Token("Order.CateringDeliveryTime", string.Format("Delivery Time : {0}", cateringEvent.DeliveryTime.ToString("dddd dd MMMM h:mm tt yyyy"))));

					var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
					var paymentMethodName = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;
					tokens.Add(new Token("Order.PaymentMethod", paymentMethodName));
					tokens.Add(new Token("Order.VatNumber", order.VatNumber));

					tokens.Add(new Token("Order.Product(s)", this.ProductListToHtmlTable(order, languageId, secondaryStoreInfo, vendorId), true));

                    tokens.Add(new Token("Order.Note(s)", this.OrderNotesToHtml(order, languageId), true));

					var language = _languageService.GetLanguageById(languageId);
					if(language != null && !String.IsNullOrEmpty(language.LanguageCulture))
					{
						DateTime createdOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, TimeZoneInfo.Utc, _dateTimeHelper.GetCustomerTimeZone(order.Customer));
						tokens.Add(new Token("Order.CreatedOn", createdOn.ToString("D", new CultureInfo(language.LanguageCulture))));
					}
					else
					{
						tokens.Add(new Token("Order.CreatedOn", order.CreatedOnUtc.ToString("D")));
					}

					//TODO add a method for getting URL (use routing because it handles all SEO friendly URLs)
					tokens.Add(new Token("Order.OrderURLForCustomer", string.Format("{0}orderdetails/{1}", GetStoreUrl(order.StoreId), order.Id), true));

					//event notification
					_eventPublisher.EntityTokensAdded(order, tokens);
				}

				protected virtual int SendNotification(MessageTemplate messageTemplate,
						EmailAccount emailAccount, int languageId, IEnumerable<Token> tokens,
						string toEmailAddress, string toName,
						string attachmentFilePath = null, string attachmentFileName = null)
				{
					//retrieve localized message template data
					var bcc = messageTemplate.GetLocalized((mt) => mt.BccEmailAddresses, languageId);
					var subject = messageTemplate.GetLocalized((mt) => mt.Subject, languageId);
					var body = messageTemplate.GetLocalized((mt) => mt.Body, languageId);

					//Replace subject and body tokens 
					var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
					var bodyReplaced = _tokenizer.Replace(body, tokens, true);

					var email = new QueuedEmail()
					{
						Priority = 5,
						From = emailAccount.Email,
						FromName = emailAccount.DisplayName,
						To = toEmailAddress,
						ToName = toName,
						CC = string.Empty,
						Bcc = bcc,
						Subject = subjectReplaced,
						Body = bodyReplaced,
						AttachmentFilePath = attachmentFilePath,
						AttachmentFileName = attachmentFileName,
						CreatedOnUtc = DateTime.UtcNow,
						EmailAccountId = emailAccount.Id
					};

					_queuedEmailService.InsertQueuedEmail(email);
					return email.Id;
				}

				protected virtual MessageTemplate GetActiveMessageTemplate(string messageTemplateName, int storeId)
				{
					var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, storeId);

					//no template found
					if(messageTemplate == null)
						return null;

					//ensure it's active
					var isActive = messageTemplate.IsActive;
					if(!isActive)
						return null;

					return messageTemplate;
				}

				protected virtual EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
				{
					var emailAccounId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
					var emailAccount = _emailAccountService.GetEmailAccountById(emailAccounId);
					if(emailAccount == null)
						emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
					if(emailAccount == null)
						emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
					return emailAccount;

				}

				protected virtual int EnsureLanguageIsActive(int languageId, int storeId)
				{
					//load language by specified ID
					var language = _languageService.GetLanguageById(languageId);

					if(language == null || !language.Published)
					{
						//load any language from the specified store
						language = _languageService.GetAllLanguages(storeId: storeId).FirstOrDefault();
					}
					if(language == null || !language.Published)
					{
						//load any language
						language = _languageService.GetAllLanguages().FirstOrDefault();
					}

					if(language == null)
						throw new Exception("No active language could be loaded");
					return language.Id;
				}

				

				#endregion


				#region Methods
				/// <summary>
				/// Handles the event.
				/// </summary>
				/// <param name="eventMessage">The event message.</param>
				public void HandleEvent(OrderPlacedEvent eventMessage)
				{
					//is enabled?
					//if (!_verizonSettings.Enabled)
					//		return;

					var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName("Other.Catering");
					if(pluginDescriptor == null)
						return;
					if(!_pluginFinder.AuthenticateStore(pluginDescriptor, _storeContext.CurrentStore.Id))
						return;

					var plugin = pluginDescriptor.Instance() as CateringProvider;
					if(plugin == null)
						return;

					var order = eventMessage.Order;
					
					if(_workContext.OriginalCustomerIfImpersonated != null)
						_genericAttributeService.SaveAttribute<string>(order, "OrderImpersonatedBy", _workContext.OriginalCustomerIfImpersonated.GetFullName());

					if(_httpContext.Session["CateringEventId"] != null)
					{
						var currentCateringEventId = Int32.Parse(_httpContext.Session["CateringEventId"].ToString());
						var currentCateringEvent = _cateringEventService.GetCateringEventById(currentCateringEventId);

						currentCateringEvent.OrderId = order.Id;
						currentCateringEvent.CateringOrderStatus = (int)CateringOrderStatus.Placed;

						_cateringEventService.UpdateCateringEvent(currentCateringEvent);
						this.ProcessEmail(currentCateringEvent);
					}
					else
					{
						var currentCustomerId = _workContext.CurrentCustomer.Id;

						var currentCateringEvent = _cateringEventService.GetInProgressCateringEventByCustomerId(currentCustomerId);
						
						currentCateringEvent.OrderId = order.Id;
						currentCateringEvent.CateringOrderStatus = (int)CateringOrderStatus.Placed;

						_cateringEventService.UpdateCateringEvent(currentCateringEvent);
						this.ProcessEmail(currentCateringEvent);
					}

					_httpContext.Session["CateringEventId"] = null;
					//send SMS
					/*if (plugin.SendSms(String.Format("New order(#{0}) has been placed.", order.Id)))
					{
							order.OrderNotes.Add(new OrderNote()
							{
									Note = "\"Order placed\" SMS alert (to store owner) has been sent",
									DisplayToCustomer = false,
									CreatedOnUtc = DateTime.UtcNow
							});
							_orderService.UpdateOrder(order);
					}*/
				}



				protected void ProcessEmail(CateringEvent cateringEvent)
				{
					List<string> neededEmails = new List<string>();// = _cateringEmailService.GetCateringEmailByStoreId(cateringEvent.SupplierStoreId);

					var order = _orderService.GetOrderById(cateringEvent.OrderId);

					var secondaryStore = _cateringEventService.SecondaryStoreInfo(cateringEvent, order, out neededEmails);

					cateringEvent.PdfInvoicePath = this.ProcessPdf(secondaryStore, order, cateringEvent);

					_cateringEventService.UpdateCateringEvent(cateringEvent);

					foreach(var email in neededEmails)
					{
						this.SendOrderPlacedStoreOwnerNotification(order, 1, email, cateringEvent, secondaryStore);
					}
				}

				protected string ProcessPdf(Dictionary<int, string> secondaryStore, Order order, CateringEvent cateringEvent)
				{
					var pdfPath = _cateringPdfService.PrintOrderToPdf(order, _workContext.WorkingLanguage.Id, cateringEvent, secondaryStore);
					return pdfPath;
				}

				/// <summary>
				/// Sends an order placed notification to a store owner
				/// </summary>
				/// <param name="order">Order instance</param>
				/// <param name="languageId">Message language identifier</param>
				/// <returns>Queued email identifier</returns>
				protected virtual int SendOrderPlacedStoreOwnerNotification(Order order, int languageId, string emailAddress, CateringEvent cateringEvent, Dictionary<int, string> secondaryStoreInfo = null)
				{
					if(order == null)
						throw new ArgumentNullException("order");

					var store = _storeService.GetStoreById(order.StoreId) ?? _storeContext.CurrentStore;
					languageId = this.EnsureLanguageIsActive(languageId, store.Id);

					var messageTemplate = this.GetActiveMessageTemplate("OrderPlaced.StoreOwnerNotification", store.Id);
					if(messageTemplate == null)
						return 0;

					//email account
					var emailAccount = this.GetEmailAccountOfMessageTemplate(messageTemplate, languageId);

					//tokens
					var tokens = new List<Token>();
					_messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
					this.AddOrderTokens(tokens, order, languageId, secondaryStoreInfo);
					_messageTokenProvider.AddCustomerTokens(tokens, order.Customer);

					//event notification
					_eventPublisher.MessageTokensAdded(messageTemplate, tokens);

					var toEmail = emailAddress;
					var toName = emailAccount.DisplayName;
					var filePath = cateringEvent.PdfInvoicePath;
					var fileName = Path.GetFileName(filePath);
					return SendNotification(messageTemplate, emailAccount,
							languageId, tokens,
							toEmail, toName, filePath, fileName);
				} 
				#endregion
    }
}