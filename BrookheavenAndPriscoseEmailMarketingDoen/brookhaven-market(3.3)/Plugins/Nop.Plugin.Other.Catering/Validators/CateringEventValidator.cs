﻿using FluentValidation;
using Nop.Plugin.Other.Catering.Models;
using Nop.Plugin.Other.Catering.Services;
using Nop.Services.Localization;


namespace Nop.Plugin.Other.Catering.Validators
{
    public class CateringEventValidator : AbstractValidator<CateringEventModel>
    {
			public CateringEventValidator(ILocalizationService localizationService, ICateringEventService cateringEventService)
        {
					RuleFor(x => x.DeliveryTime)
						.NotNull()
						.WithMessage(localizationService.GetResource("Nop.Plugin.Other.Catering.Validation.DeliveryDate.Required"))
						.Must((x, deliveryTime) => cateringEventService.IsValidDeliveryTime(deliveryTime))
						.WithMessage(localizationService.GetResource("Nop.Plugin.Other.Catering.Validation.DeliveryDate.Invalid"));
        }
    }
}