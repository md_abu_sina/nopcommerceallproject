using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Nop.Core;
using FluentValidation.Attributes;
using Nop.Plugin.Other.Catering.Validators;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;

namespace Nop.Plugin.Other.Catering.Models
{
    [Validator(typeof(CateringEventValidator))]
    public class CateringEventModel : BaseNopModel
    {
        public CateringEventModel()
        {
            EventOrderItems = new List<EventOrderProductModel>();
        }
        public virtual int Id { get; set; }
        [DisplayName("# Of Guests:")]
        public virtual int NumberOfPeople { get; set; }
        public virtual bool CafePickup { get; set; }
        [DisplayName("Date & Time:")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yy H:mm:ss tt}",
                         ApplyFormatInEditMode = true)]
        public virtual DateTime DeliveryTime { get; set; }
        public virtual DateTime? OrderCreatedTime { get; set; }
        public virtual DateTime? OrderUpdatedTime { get; set; }
        public virtual int SupplierStoreId { get; set; }
        public virtual double SupplierStoreDistance { get; set; }
        public virtual String SupplierStoreName { get; set; }
        public virtual int AddressId { get; set; }
        [DisplayName("Address")]
        public virtual String ShippingAddress { get; set; }
        public virtual int CurrentCustomerId { get; set; }
        public virtual int OrderId { get; set; }
        public virtual int CateringOrderStatusId { get; set; }
        public virtual String CateringOrderStatus { get; set; }
        public virtual String OrderedBy { get; set; }
        public virtual List<OrderNote> OrderNotes { get; set; }
        public virtual bool EventInProgress { get; set; }

        public List<EventOrderProductModel> EventOrderItems { get; set; }
    }
}
