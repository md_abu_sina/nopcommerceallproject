﻿using Nop.Web.Models.Checkout;


namespace Nop.Plugin.Other.Catering.Models
{
    public partial class CateringCheckoutConfirmModel : CheckoutConfirmModel
    {
        public int CurrentEventId { get; set; }

    }
}