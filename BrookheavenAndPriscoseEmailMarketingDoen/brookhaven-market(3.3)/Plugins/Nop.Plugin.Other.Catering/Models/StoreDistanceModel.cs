﻿using System;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;

namespace Nop.Plugin.Other.Catering.Models
{
    //[Validator(typeof(EventItemValidator))]
		public class StoreDistanceModel : BaseNopModel
    {
			public class DistanceResponse
			{
					public string Status { get; set; }
					public string[] Origin_Addresses { get; set; }
					public string[] Destination_Addresses { get; set; }
					public Row[] Rows { get; set; }
			}

			public class Row
			{
					public Element[] Elements { get; set; }
			}

			public class Element
			{
					public string Status { get; set; }
					public Item Duration { get; set; }
					public Item Distance { get; set; }
			}

			public class Item
			{
					public int Value { get; set; }
					public string Text { get; set; }
			}

			/*public class NearestStore
			{
				private int _storeId;
				private string _storeName;
				private double _distance;

				public NearestStore(int storeId, string storeName, double distance)
				{
					// TODO: Complete member initialization
					this._storeId = storeId;
					this._storeName = storeName;
					this._distance = distance;
				}
				
			}*/
    }

}
