﻿using System;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Nop.Core.Infrastructure;
using Nop.Services.Localization;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Other.Catering
{
    public static class HtmlExtensions
    {

        //public static MvcHtmlString DeleteConfirmation<T>(this HtmlHelper<T> helper,string actionName,string controllerName, int model_Id, string buttonsSelector = null) where T : BaseNopEntityModel
        //{
        //    return DeleteConfirmation<T>(helper, actionName, controllerName, model_Id, buttonsSelector);
        //}

        //// Adds an action name parameter for using other delete action names
        public static MvcHtmlString DeleteConfirmation<T>(this HtmlHelper<T> helper, string actionName,string controllerName, int model_Id, string buttonsSelector = null) where T : BaseNopEntityModel
        {
            if (String.IsNullOrEmpty(actionName))
                actionName = "Delete";

            var modalId = MvcHtmlString.Create(helper.ViewData.ModelMetadata.ModelType.Name.ToLower() + "-delete-confirmation").ToHtmlString();

            
						var deleteConfirmationModel = new DeleteConfirmationModel
						{
							Id = model_Id,
							ControllerName = controllerName,
							ActionName = actionName,
							WindowId = modalId
						};

						var window = new StringBuilder();
						window.AppendLine(string.Format("<div id='{0}' style='display:none;'>", modalId));
						window.AppendLine(helper.Partial("Delete", deleteConfirmationModel).ToHtmlString());
						window.AppendLine("</div>");
						window.AppendLine("<script>");
						window.AppendLine("$(document).ready(function() {");
						window.AppendLine(string.Format("$('#{0}').click(function (e) ", buttonsSelector));
						window.AppendLine("{");
						window.AppendLine("e.preventDefault();");
						window.AppendLine(string.Format("var window = $('#{0}');", modalId));
						window.AppendLine("if (!window.data('kendoWindow')) {");
						window.AppendLine("window.kendoWindow({");
						window.AppendLine("modal: true,");
						window.AppendLine(string.Format("title: '{0}',", EngineContext.Current.Resolve<ILocalizationService>().GetResource("Admin.Common.AreYouSure")));
						window.AppendLine("actions: ['Close']");
						window.AppendLine("});");
						window.AppendLine("}");
						window.AppendLine("window.data('kendoWindow').center().open();");
						window.AppendLine("});");
						window.AppendLine("});");
						window.AppendLine("</script>");

						return MvcHtmlString.Create(window.ToString());
        }

    }
}

