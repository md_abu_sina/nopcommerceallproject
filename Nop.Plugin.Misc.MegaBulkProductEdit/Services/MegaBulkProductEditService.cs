﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using Nop.Services.Localization;

namespace Nop.Plugin.Misc.MegaBulkProductEdit.Services
{
    public partial class MegaBulkProductEditService : IMegaBulkProductEditService
    {
        #region Field

        private readonly IRepository<Product> _productRepository;
        private readonly IWorkContext _workContext;
        private readonly CatalogSettings _catalogSettings;
        private readonly IRepository<AclRecord> _aclRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;

        #endregion

        #region ctor

        public MegaBulkProductEditService(IRepository<Product> productRepository,
            IWorkContext workContext, CatalogSettings catalogSettings,
            IRepository<AclRecord> aclRepository, IRepository<StoreMapping> storeMappingRepository)
        {
            _productRepository = productRepository;
            _workContext = workContext;
            _catalogSettings = catalogSettings;
            _aclRepository = aclRepository;
            _storeMappingRepository = storeMappingRepository;
        }

        #endregion

        #region Methods

        public IPagedList<Product> SearchProductVariantsExt(bool? searchDescriptions, ProductType? productType, DateTime? createdDateFrom, 
            DateTime? createdDateTo, decimal? priceFrom, decimal? priceTo, int? stockQuantityFrom, int? stockQuantityTo, 
            int? stockManageInventoryMethod, bool? disableBuyButton, bool? productPublished, string merchantName = null, 
            int languageId = 0, int parentGroupedProductId = 0, bool visibleIndividuallyOnly = false, bool? featuredProducts = null, 
            decimal? priceMin = null, decimal? priceMax = null, int productTagId = 0, IList<int> filteredSpecs = null,
            ProductSortingEnum orderBy = ProductSortingEnum.Position, int pageIndex = 0, int pageSize = int.MaxValue, 
            bool showHidden = false, int warehouseId = 0, List<int> categoryIds = null, int manufacturerId = 0, int vendorId = 0, 
            int storeId = 0, string productName = null)
        {
            //validate "categoryIds" parameter
            if (categoryIds != null && categoryIds.Contains(0))
                categoryIds.Remove(0);

            #region Search products

            //products
            var query = _productRepository.Table;
            query = query.Where(p => !p.Deleted);

            //if (!showHidden)
            //{
            //    query = query.Where(p => p.Published);
            //}
            if (parentGroupedProductId > 0)
            {
                query = query.Where(p => p.ParentGroupedProductId == parentGroupedProductId);
            }
            if (visibleIndividuallyOnly)
            {
                query = query.Where(p => p.VisibleIndividually);
            }
            if (productType.HasValue)
            {
                int productTypeId = (int)productType.Value;
                query = query.Where(p => p.ProductTypeId == productTypeId);
            }

            //created date
            if (createdDateFrom.HasValue)
                query = query.Where(b => createdDateFrom.Value <= b.CreatedOnUtc);
            if (createdDateTo.HasValue)
                query = query.Where(b => createdDateTo.Value >= b.CreatedOnUtc);

            //price filter
            if (priceFrom.HasValue)
                query = priceTo.HasValue
                            ? query.Where(x => x.Price >= priceFrom.Value && x.Price <= priceTo.Value)
                            : query.Where(x => x.Price == priceFrom.Value);

            //stock quantity
            if (stockQuantityFrom.HasValue)
                query = stockQuantityTo.HasValue
                            ? query.Where(
                                x =>
                                x.StockQuantity >= stockQuantityFrom.Value && x.StockQuantity <= stockQuantityTo.Value)
                            : query.Where(x => x.StockQuantity == stockQuantityFrom.Value);

            //stock manage inventory
            if (stockManageInventoryMethod.HasValue)
                query = query.Where(x => x.ManageInventoryMethodId == stockManageInventoryMethod.Value);

            //disable buy
            if (disableBuyButton.HasValue)
                query = query.Where(x => x.DisableBuyButton == disableBuyButton.Value);

            //searching by keyword


            //merchant name
            //if (!string.IsNullOrWhiteSpace(merchantName))
            //{
            //    query = from p in query
            //            where p.MerchantName.Contains(merchantName)
            //            select p;
            //}

            if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
            {
                //Store mapping
                query = from p in query
                        join sm in _storeMappingRepository.Table
                        on new { c1 = p.Id, c2 = "Product" } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into p_sm
                        from sm in p_sm.DefaultIfEmpty()
                        where !p.LimitedToStores || storeId == sm.StoreId
                        select p;
            }

            //category filtering
            if (categoryIds != null && categoryIds.Count > 0)
            {
                var productIds = (from p in query
                                 from pc in p.ProductCategories.Where(pc => categoryIds.Contains(pc.CategoryId))
                                 select p.Id).ToArray();
                query = from p in query.Where(p => productIds.Contains(p.ParentGroupedProductId)) select p;
            }
            //manufacturer filtering
            if (manufacturerId > 0)
            {
                query = from p in query
                        from pm in p.ProductManufacturers.Where(pm => pm.ManufacturerId == manufacturerId)
                        where (!featuredProducts.HasValue || featuredProducts.Value == pm.IsFeaturedProduct)
                        select p;
            }

            //vendor filtering
            if (vendorId > 0)
            {
                query = query.Where(p => p.VendorId == vendorId);
            }

            //warehouse filtering
            if (warehouseId > 0)
            {
                query = query.Where(p => p.WarehouseId == warehouseId);
            }

            //tag filtering
            if (productTagId > 0)
            {
                query = from p in query
                        from pt in p.ProductTags.Where(pt => pt.Id == productTagId)
                        select p;
            }

            //only distinct products (group by ID)
            //if we use standard Distinct() method, then all fields will be compared (low performance)
            //it'll not work in SQL Server Compact when searching products by a keyword)
            query = from p in query
                    group p by p.Id
                        into pGroup
                        orderby pGroup.Key
                        select pGroup.FirstOrDefault();



            int simpleProductTypeId = (int)ProductType.SimpleProduct;

            query = query.Where(p => p.ProductTypeId == simpleProductTypeId);


            if (!String.IsNullOrWhiteSpace(productName))
            {
                query = from p in query
                        where p.Name.Contains(productName)
                        select p;
            }


            //sort products
            if (orderBy == ProductSortingEnum.Position && categoryIds != null && categoryIds.Count > 0)
            {
                //category position
                var firstCategoryId = categoryIds[0];
                query = query.OrderBy(p => p.ProductCategories.FirstOrDefault(pc => pc.CategoryId == firstCategoryId).DisplayOrder);
            }
            else if (orderBy == ProductSortingEnum.Position && manufacturerId > 0)
            {
                //manufacturer position
                query =
                    query.OrderBy(p => p.ProductManufacturers.FirstOrDefault(pm => pm.ManufacturerId == manufacturerId).DisplayOrder);
            }
            else if (orderBy == ProductSortingEnum.Position && parentGroupedProductId > 0)
            {
                //parent grouped product specified (sort associated products)
                query = query.OrderBy(p => p.DisplayOrder);
            }
            else if (orderBy == ProductSortingEnum.Position)
            {
                //otherwise sort by name
                query = query.OrderBy(p => p.Name);
            }
            else if (orderBy == ProductSortingEnum.NameAsc)
            {
                //Name: A to Z
                query = query.OrderBy(p => p.Name);
            }
            else if (orderBy == ProductSortingEnum.NameDesc)
            {
                //Name: Z to A
                query = query.OrderByDescending(p => p.Name);
            }
            else if (orderBy == ProductSortingEnum.PriceAsc)
            {
                //Price: Low to High
                query = query.OrderBy(p => p.Price);
            }
            else if (orderBy == ProductSortingEnum.PriceDesc)
            {
                //Price: High to Low
                query = query.OrderByDescending(p => p.Price);
            }
            else if (orderBy == ProductSortingEnum.CreatedOn)
            {
                //creation date
                query = query.OrderByDescending(p => p.CreatedOnUtc);
            }
            else
            {
                //actually this code is not reachable
                query = query.OrderBy(p => p.Name);
            }
            return new PagedList<Product>(query, pageIndex, pageSize);

            //var products = new PagedList<Product>(query, pageIndex, pageSize, int.MaxValue);
            //return products;
            #endregion
        }
    }
   #endregion
}