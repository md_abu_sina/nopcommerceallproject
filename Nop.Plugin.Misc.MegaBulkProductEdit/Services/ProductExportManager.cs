﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Catalog;
using Nop.Services.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Nop.Plugin.Misc.MegaBulkProductEdit.Services
{
    public class ProductExportManager : IProductExportManager
    {
        #region Field

        private readonly BulkProductEditExportSettings _exportSettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly StoreInformationSettings _storeInformationSettings;

        #endregion

        #region Ctr

        public ProductExportManager(BulkProductEditExportSettings exportSettings, IDateTimeHelper dateTimeHelper, StoreInformationSettings storeInformationSettings)
        {
            _exportSettings = exportSettings;
            _dateTimeHelper = dateTimeHelper;
            _storeInformationSettings = storeInformationSettings;

            _exportSettings.EnableProductId = _exportSettings.EnableProductVariantId = _exportSettings.EnableProductVariantName = _exportSettings.EnableProductDescription = true;
            _exportSettings.EnableProductCost = _exportSettings.EnableStockQuantity = _exportSettings.EnableCategory = true;
            _exportSettings.EnableManufacturer = _exportSettings.EnableCreatedOnUtc = true;

        }

        #endregion

        #region Methods

        public void ExportProductVariantsToXlsx(MemoryStream stream, IPagedList<Product> productVariants)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            // ok, we can run the real code of the sample now
            using (var xlPackage = new ExcelPackage(stream))
            {
                // get handle to the existing worksheet
                var worksheet = xlPackage.Workbook.Worksheets.Add("ProductVariants");
                //Create Headers and format them
                var properties = new List<string>();
                if (_exportSettings.EnableProductId)
                    properties.Add("ProductId");
                if (_exportSettings.EnableProductVariantId)
                    properties.Add("ProductVariantId");
                if (_exportSettings.EnableProductVariantName)
                    properties.Add("ProductName");
                if (_exportSettings.EnableProductDescription)
                    properties.Add("Description");
                if (_exportSettings.EnableProductCost)
                    properties.Add("ProductCost");
                if (_exportSettings.EnablePrice)
                    properties.Add("Price");
                if (_exportSettings.EnableStockQuantity)
                    properties.Add("StockQuantity");
                if (_exportSettings.EnableCategory)
                    properties.Add("Category");
                if (_exportSettings.EnableManufacturer)
                    properties.Add("Manufacturer");
                if (_exportSettings.EnableDisableBuyButton)
                    properties.Add("DisableBuyButton");
                if (_exportSettings.EnableCreatedOnUtc)
                    properties.Add("CreatedDate");
                
                for (int i = 0; i < properties.Count; i++)
                {
                    worksheet.Cells[1, i + 1].Value = properties[i];
                    worksheet.Cells[1, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[1, i + 1].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
                    worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                }
                
                int row = 2;
                foreach (var pv in productVariants)
                {
                    int col = 1;

                    //product variants properties
                    if (_exportSettings.EnableProductId)
                    {
                        worksheet.Cells[row, col].Value = pv.Id;
                        col++;
                    }
                    if (_exportSettings.EnableProductVariantId)
                    {
                        worksheet.Cells[row, col].Value = pv.Id;
                        col++;
                    }
                    if (_exportSettings.EnableProductVariantName)
                    {
                        worksheet.Cells[row, col].Value = pv.Name;
                        col++;
                    }
                    if (_exportSettings.EnableProductDescription)
                    {
                        worksheet.Cells[row, col].Value = pv.FullDescription;
                        col++;
                    }
                    if (_exportSettings.EnableProductCost)
                    {
                        worksheet.Cells[row, col].Value = pv.ProductCost;
                        col++;
                    }
                    if (_exportSettings.EnablePrice)
                    {
                        worksheet.Cells[row, col].Value = pv.Price;
                        col++;
                    }
                    if (_exportSettings.EnableStockQuantity)
                    {
                        worksheet.Cells[row, col].Value = pv.StockQuantity;
                        col++;
                    }
                    if (_exportSettings.EnableCategory)
                    {
                        var cats = string.Empty;
                        var productCategories = pv.ProductCategories as List<ProductCategory>;
                        if (productCategories != null)
                            productCategories.ForEach(x =>{ cats = x.Category.Name; });
                        worksheet.Cells[row, col].Value = cats.TrimEnd(',');
                        col++;
                    }
                    if (_exportSettings.EnableManufacturer)
                    {
                        var man = string.Empty;
                        var productManufacturers=pv.ProductManufacturers as List<ProductManufacturer>;
                        if (productManufacturers != null)
                            productManufacturers.ForEach(x => { man = x.Manufacturer.Name; });
                        worksheet.Cells[row, col].Value = man.TrimEnd(',');
                        col++;
                    }
                    if (_exportSettings.EnableDisableBuyButton)
                    {
                        worksheet.Cells[row, col].Value = pv.DisableBuyButton;
                        col++;
                    }
                    if (_exportSettings.EnableCreatedOnUtc)
                    {
                        worksheet.Cells[row, col].Value = string.Format(_dateTimeHelper.ConvertToUserTime(pv.CreatedOnUtc,DateTimeKind.Utc).ToString());
                        col++;
                    }

                    //next row
                    row++;
                }

                // set some core property values
                //xlPackage.Workbook.Properties.Title = string.Format("{0} product-variants", _storeInformationSettings.StoreName);
                //xlPackage.Workbook.Properties.Author = _storeInformationSettings.StoreName;
                //xlPackage.Workbook.Properties.Subject = string.Format("{0} product-variants", _storeInformationSettings.StoreName);
                //xlPackage.Workbook.Properties.Keywords = string.Format("{0} product-variants", _storeInformationSettings.StoreName);
                //xlPackage.Workbook.Properties.Category = "ProductVariants";
                //xlPackage.Workbook.Properties.Comments = string.Format("{0} product-variants", _storeInformationSettings.StoreName);

                //// set some extended property values
                //xlPackage.Workbook.Properties.Company = _storeInformationSettings.StoreName;
                //xlPackage.Workbook.Properties.HyperlinkBase = new Uri(_storeInformationSettings.StoreUrl);

                // save the new spreadsheet
                xlPackage.Save();
            }
        }

        #endregion
    }
}
