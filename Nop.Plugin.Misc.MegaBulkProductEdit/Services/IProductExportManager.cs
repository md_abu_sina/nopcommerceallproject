using System.IO;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Misc.MegaBulkProductEdit.Services
{
    public interface IProductExportManager
    {
        void ExportProductVariantsToXlsx(MemoryStream stream, Core.IPagedList<Product> productVariants);
    }
}