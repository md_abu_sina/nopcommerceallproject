﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Misc.MegaBulkProductEdit.Services
{
    public partial interface IMegaBulkProductEditService
	{
        IPagedList<Product> SearchProductVariantsExt(bool? searchDescriptions, ProductType? productType, DateTime? createdDateFrom, DateTime? createdDateTo, 
            decimal? priceFrom, decimal? priceTo, int? stockQuantityFrom, int? stockQuantityTo, int? stockManageInventoryMethod, bool? disableBuyButton,
            bool? productPublished=false, string merchantName = null, int languageId = 0, int parentGroupedProductId = 0, bool visibleIndividuallyOnly = false,
            bool? featuredProducts = null, decimal? priceMin = null, decimal? priceMax = null, int productTagId = 0, IList<int> filteredSpecs = null,
            ProductSortingEnum orderBy = ProductSortingEnum.Position, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, 
            int warehouseId = 0, List<int> categoryIds = null, int manufacturerId = 0, int vendorId = 0, int storeId = 0, string productName = null);
	}
}
