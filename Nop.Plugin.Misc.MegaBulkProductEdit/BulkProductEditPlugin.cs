﻿using System.Web.Routing;
using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Web.Framework.Menu;
using System.Linq;

namespace Nop.Plugin.Misc.MegaBulkProductEdit
{
	public class BulkProductEditPlugin : BasePlugin, IAdminMenuPlugin,IMiscPlugin
	{
	    private readonly ILocalizationService _localizationService;

	    public BulkProductEditPlugin(ILocalizationService localizationService)
	    {
	        _localizationService = localizationService;
	    }

	    public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "MiscMegaBulkProductEdit";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Misc.MegaBulkProductEdit.Controllers.Controllers" }, { "area", null } };
        }

        #region Menu Builder

        public bool Authenticate()
        {
            return true;
        }

        //public SiteMapNode BuildMenuItem()
        //{
        //    var node = new SiteMapNode()
        //    {
        //        Title = _localizationService.GetResource("Misc.Mega.BulkEdit.Menu.Text"),
        //        Visible = true,
        //        Url = "~/Plugin/Misc/MegaBulkProductEdit/Manage"
        //    };
        //    return node;
        //}

        #endregion

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var node = new SiteMapNode()
            {
                Title = _localizationService.GetResource("Misc.Mega.BulkEdit.Menu.Text"),
                Visible = true,
                Url = "~/Plugin/Misc/MegaBulkProductEdit/Manage"
            };
            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Third party plugins");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(node);
            else
                rootNode.ChildNodes.Add(node);
        }
    }
}