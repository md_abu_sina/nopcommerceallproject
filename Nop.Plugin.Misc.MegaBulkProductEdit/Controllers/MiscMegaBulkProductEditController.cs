﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Controllers;
using Nop.Admin.Models.Catalog;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Messages;
using Nop.Plugin.Misc.MegaBulkProductEdit.Model;
using Nop.Plugin.Misc.MegaBulkProductEdit.Services;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Security;

namespace Nop.Plugin.Misc.MegaBulkProductEdit.Controllers
{
    public partial class MiscMegaBulkProductEditController : BaseAdminController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly IPermissionService _permissionService;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly EmailAccountSettings _emailAccountSettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerService _customerService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IMegaBulkProductEditService _biponeeBulkProductVariantService;
        private readonly IProductExportManager _productVariantExportManager;
        private readonly BulkProductEditExportSettings _bulkProductVariantExportSettings;
        private readonly ISettingService _settingService;
        private readonly IShippingService _shippingService;
        private readonly IVendorService _vendorService;
        private readonly IStoreService _storeService;

        #endregion

        #region Ctor

        public MiscMegaBulkProductEditController(IWorkContext workContext,AdminAreaSettings adminAreaSettings, 
            ILanguageService languageService, ILocalizationService localizationService, IPictureService pictureService,
            IPermissionService permissionService, IEmailAccountService emailAccountService, 
            IQueuedEmailService queuedEmailService, EmailAccountSettings emailAccountSettings,
            IDateTimeHelper dateTimeHelper, ICustomerService customerSettings, ICategoryService categoryService, 
            IProductService productService, IManufacturerService manufacturerService, 
            IMegaBulkProductEditService biponeeBulkProductVariantService,
            IProductExportManager productVariantExportManager, 
            BulkProductEditExportSettings bulkProductVariantExportSettings, ISettingService settingService, IShippingService shippingService, IVendorService vendorService, IStoreService storeService)
        {
            _workContext = workContext;
            _adminAreaSettings = adminAreaSettings;
            _languageService = languageService;
            _localizationService = localizationService;
            _pictureService = pictureService;
            _permissionService = permissionService;
            _emailAccountService = emailAccountService;
            _queuedEmailService = queuedEmailService;
            _emailAccountSettings = emailAccountSettings;
            _dateTimeHelper = dateTimeHelper;
            _customerService = customerSettings;
            _categoryService = categoryService;
            _productService = productService;
            _manufacturerService = manufacturerService;
            _biponeeBulkProductVariantService = biponeeBulkProductVariantService;
            _productVariantExportManager = productVariantExportManager;
            _bulkProductVariantExportSettings = bulkProductVariantExportSettings;
            _settingService = settingService;
            _shippingService = shippingService;
            _vendorService = vendorService;
            _storeService = storeService;
        }

        #endregion

        #region Bulk editing
        public ActionResult BulkEdit()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new MegaBulkEditListModel();

            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //categories
            model.AvailableCategories.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem() { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem() { Text = m.Name, Value = m.Id.ToString() });

            //Inventory type
            model.AvailableInventoryType.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "All", Selected = true });
            model.AvailableInventoryType.Add(new SelectListItem() { Text = "Track inventory", Value = Convert.ToInt32(ManageInventoryMethod.ManageStock).ToString() });
            model.AvailableInventoryType.Add(new SelectListItem() { Text = "Don't track inventory", Value = Convert.ToInt32(ManageInventoryMethod.DontManageStock).ToString() });
            model.AvailableInventoryType.Add(new SelectListItem() { Text = "Track inventory by product attributes", Value = Convert.ToInt32(ManageInventoryMethod.ManageStockByAttributes).ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem() { Text = s.Name, Value = s.Id.ToString() });

            //warehouse
            model.AvailableWarehouses.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var wh in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem() { Text = wh.Name, Value = wh.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem() { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //set default value of stock -1
            model.StockQuantityFrom = -1;
            model.StockQuantityTo = -1;

            //Disable buy button
            model.AvailableDisableBuyButton.Add(new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "All" });
            model.AvailableDisableBuyButton.Add(new SelectListItem() { Text = "Enable", Value = "Enable" });
            model.AvailableDisableBuyButton.Add(new SelectListItem() { Text = "Disable", Value = "Disable" });

            //Merchant
            //model.AvailableMerchantName.Insert(0, new SelectListItem()
            //{
            //    Text = _localizationService.GetResource("Admin.Common.NotSelected"),
            //    Value = _localizationService.GetResource("Admin.Common.NotSelected"),
            //});

            return View("~/Plugins/Misc.MegaBulkProductEdit/Views/MiscMegaBulkProductEdit/BulkEdit.cshtml", model);
        }

        [HttpPost]
        [AdminAntiForgery(true)]
        public ActionResult BulkEditJson(DataSourceRequest command, MegaBulkEditListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            //created date
            DateTime? startDateValue = (model.StartDate == null) ? null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);
            DateTime? endDateValue = (model.EndDate == null) ? null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            //price range
            decimal? priceFrom = (model.PriceFrom == 0) ? (decimal?)null : model.PriceFrom;
            decimal? priceTo = (model.PriceTo == 0) ? (decimal?)null : model.PriceTo;

            //stock range
            int? stockQuantityFrom = (model.StockQuantityFrom == -1) ? (int?)null : Convert.ToInt16(model.StockQuantityFrom);
            int? stockQuantityTo = (model.StockQuantityTo == -1) ? (int?)null : Convert.ToInt16(model.StockQuantityTo);

            //stock manage
            int? stockManageInventoryMethod = model.ManageInventoryMethodValue == "All" ? (int?)null : Convert.ToInt32(model.ManageInventoryMethodValue);

            //disable buy button
            bool? disableBuyButton = (model.DisableBuyButton == "All") ? (bool?)null : (model.DisableBuyButton == "Enable");

            //published & unpublished (product)
            bool? productPublished = (model.ProductType == "All") ? (bool?)null : (model.ProductType == "Published");

            //prepare Merchant Name
            //if (string.Concat(model.MerchantName) == _localizationService.GetResource("Admin.Common.NotSelected"))
            //{
            //    model.MerchantName = string.Empty;
            //}

            var categoryIds = new List<int>() { model.SearchCategoryId };

            //include subcategories
            if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
                categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));

            var products = _biponeeBulkProductVariantService.SearchProductVariantsExt(searchDescriptions: false,
                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                createdDateFrom: startDateValue,
                createdDateTo: endDateValue,
                priceFrom: priceFrom,
                priceTo: priceTo,
                stockQuantityFrom: stockQuantityFrom,
                stockQuantityTo: stockQuantityTo,
                stockManageInventoryMethod: stockManageInventoryMethod,
                disableBuyButton: disableBuyButton,
                productPublished: productPublished,
                languageId: 0,
                parentGroupedProductId: 0,
                visibleIndividuallyOnly: false,
                featuredProducts: null,
                priceMin: null,
                priceMax: null,
                productTagId: 0,
                filteredSpecs: null,
                orderBy: ProductSortingEnum.Position,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                warehouseId: model.SearchWarehouseId,
                categoryIds: categoryIds,
                manufacturerId: model.SearchManufacturerId,
                vendorId: model.SearchVendorId,
                storeId: model.SearchStoreId,
                productName: model.SearchProductName);


            //var products = _productService.SearchProducts(categoryIds: new List<int>() { model.SearchCategoryId },
            //    manufacturerId: model.SearchManufacturerId,
            //    vendorId: model.SearchVendorId,
            //    productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
            //    keywords: model.SearchProductName,
            //    pageIndex: command.Page - 1,
            //    pageSize: command.PageSize,
            //    showHidden: true);

            var gridModel = new DataSourceResult();
            
            gridModel.Data = products.Select(x =>
            {
                return new MegaBulkEditProductModel()
                {
                    Id=x.Id,
                    Add = 0,
                    AdditionalShippingCharge = x.AdditionalShippingCharge,
                    DisableBuyButton = x.DisableBuyButton,
                    Delete = 0,
                    ManageInventoryMethod = x.ManageInventoryMethod.GetLocalizedEnum(_localizationService, _workContext.WorkingLanguage.Id),
                    Name = x.Name,
                    OldPrice = x.OldPrice,
                    Price = x.Price,
                    ProductCost = x.ProductCost,
                    Published = x.Published,
                    Sku = x.Sku,
                    StockQuantity = x.StockQuantity
                };
            });
            gridModel.Total = products.TotalCount;

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult BulkEditUpdate(IEnumerable<MegaBulkEditProductModel> products)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (products != null)
            {
                foreach (var pModel in products)
                {
                    //update
                    var product = _productService.GetProductById(pModel.Id);
                    if (product != null)
                    {
                        //a vendor should have access only to his products
                        if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                            continue;

                        product.Sku = pModel.Sku;
                        product.Price = pModel.Price;
                        product.OldPrice = pModel.OldPrice;
                        product.StockQuantity = pModel.StockQuantity+pModel.Add-pModel.Delete;
                        product.Published = pModel.Published;
                        product.DisableBuyButton = pModel.DisableBuyButton;
                        product.ProductCost=pModel.ProductCost;
                        product.AdditionalShippingCharge=pModel.AdditionalShippingCharge;

                        _productService.UpdateProduct(product);
                    }
                }
            }

            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult BulkEditDelete(IEnumerable<MegaBulkEditProductModel> products)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (products != null)
            {
                foreach (var pModel in products)
                {
                    //delete
                    var product = _productService.GetProductById(pModel.Id);
                    if (product != null)
                    {
                        //a vendor should have access only to his products
                        if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
                            continue;

                        _productService.DeleteProduct(product);
                    }
                }
            }
            return new NullJsonResult();
        }

        [NonAction]
        protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
        {
            var categoriesIds = new List<int>();
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, true);
            foreach (var category in categories)
            {
                categoriesIds.Add(category.Id);
                categoriesIds.AddRange(GetChildCategoryIds(category.Id));
            }
            return categoriesIds;
        }

        #endregion

        #region Export To Excel

        public ActionResult ExportExcelAllProduct()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            try
            {
                var productVariants = _biponeeBulkProductVariantService.SearchProductVariantsExt(null, null, null, null, null, null, null, null, null, null, null);

                byte[] bytes = null;
                using (var stream = new MemoryStream())
                {
                    _productVariantExportManager.ExportProductVariantsToXlsx(stream, productVariants);
                    bytes = stream.ToArray();
                }
                return File(bytes, "text/xls", "products(all).xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);

                return RedirectToRoute("Plugin.Misc.MegaBulkProductEdit.Manage");
            }
        }

        [HttpGet]
        public ActionResult ExportExcelSearchedProduct(DataSourceRequest command, MegaBulkEditListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            try
            {
                //created date
                DateTime? startDateValue = (model.StartDate == null) ? null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);
                DateTime? endDateValue = (model.EndDate == null) ? null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

                //price range
                decimal? priceFrom = (model.PriceFrom == 0) ? (decimal?)null : model.PriceFrom;
                decimal? priceTo = (model.PriceTo == 0) ? (decimal?)null : model.PriceTo;

                //stock range
                int? stockQuantityFrom = (model.StockQuantityFrom == -1) ? (int?)null : Convert.ToInt16(model.StockQuantityFrom);
                int? stockQuantityTo = (model.StockQuantityTo == -1) ? (int?)null : Convert.ToInt16(model.StockQuantityTo);

                //stock manage
                int? stockManageInventoryMethod = model.ManageInventoryMethodValue == "All" ? (int?)null : Convert.ToInt32(model.ManageInventoryMethodValue);

                //disable buy button
                bool? disableBuyButton = (model.DisableBuyButton == "All") ? (bool?)null : (model.DisableBuyButton == "Enable");

                //published & unpublished (product)
                bool? productPublished = (model.ProductType == "All") ? (bool?)null : (model.ProductType == "Published");

                bool? productVariantPublished = (model.ProductType == "All") ? (bool?)null : (model.ProductType == "Published");

                //prepare Merchant Name
                //if (string.Concat(model.MerchantName) == _localizationService.GetResource("Admin.Common.NotSelected"))
                //{
                //    model.MerchantName = string.Empty;
                //}

                var categoryIds = new List<int>() { model.SearchCategoryId };

                //include subcategories
                if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
                    categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));

                var products = _biponeeBulkProductVariantService.SearchProductVariantsExt(searchDescriptions: false,
                    productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                    createdDateFrom: startDateValue,
                    createdDateTo: endDateValue,
                    priceFrom: priceFrom,
                    priceTo: priceTo,
                    stockQuantityFrom: stockQuantityFrom,
                    stockQuantityTo: stockQuantityTo,
                    stockManageInventoryMethod: stockManageInventoryMethod,
                    disableBuyButton: disableBuyButton,
                    productPublished: productPublished,
                    languageId: 0,
                    parentGroupedProductId: 0,
                    visibleIndividuallyOnly: false,
                    featuredProducts: null,
                    priceMin: null,
                    priceMax: null,
                    productTagId: 0,
                    filteredSpecs: null,
                    orderBy: ProductSortingEnum.Position,
                    pageIndex: 0,
                    pageSize: int.MaxValue,
                    warehouseId: model.SearchWarehouseId,
                    categoryIds: categoryIds,
                    manufacturerId: model.SearchManufacturerId,
                    vendorId: model.SearchVendorId,
                    storeId: model.SearchStoreId,
                    productName: model.SearchProductName);

                byte[] bytes = null;
                using (var stream = new MemoryStream())
                {
                    _productVariantExportManager.ExportProductVariantsToXlsx(stream, products);
                    bytes = stream.ToArray();
                }
                return File(bytes, "text/xls", "products(search).xlsx");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToRoute("Plugin.Misc.MegaBulkProductEdit.Manage");
            }
        }

        #endregion

        #region Export Settings

        public ActionResult ExportSettings()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new ProductVariantExportSettingModel()
            {
                EnableCategory=  _bulkProductVariantExportSettings.EnableCategory,
                EnableCreatedOnUtc = _bulkProductVariantExportSettings.EnableCategory,
                EnableDisableBuyButton = _bulkProductVariantExportSettings.EnableDisableBuyButton,
                EnableManufacturer = _bulkProductVariantExportSettings.EnableManufacturer,
                EnablePrice = _bulkProductVariantExportSettings.EnablePrice,
                EnableProductCost=_bulkProductVariantExportSettings.EnableProductCost,
                EnableProductDescription = _bulkProductVariantExportSettings.EnableProductDescription,
                EnableProductId=_bulkProductVariantExportSettings.EnableProductId,
                EnableProductVariantId = _bulkProductVariantExportSettings.EnableProductVariantId,
                EnableProductVariantName = _bulkProductVariantExportSettings.EnableProductVariantName,
                EnableStockQuantity = _bulkProductVariantExportSettings.EnableStockQuantity,
            };

            return View("~/Plugins/Misc.MegaBulkProductEdit/Views/MiscMegaBulkProductEdit/Settings.cshtml", model);
        }

        [HttpPost]
        public ActionResult ExportSettings(ProductVariantExportSettingModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                _bulkProductVariantExportSettings.EnableCategory=  model.EnableCategory;
                _bulkProductVariantExportSettings.EnableCreatedOnUtc = model.EnableCategory;
                _bulkProductVariantExportSettings.EnableDisableBuyButton = model.EnableDisableBuyButton;
                _bulkProductVariantExportSettings.EnableManufacturer = model.EnableManufacturer;
                _bulkProductVariantExportSettings.EnablePrice = model.EnablePrice;
                _bulkProductVariantExportSettings.EnableProductCost=model.EnableProductCost;
                _bulkProductVariantExportSettings.EnableProductDescription = model.EnableProductDescription;
                _bulkProductVariantExportSettings.EnableProductId=model.EnableProductId;
                _bulkProductVariantExportSettings.EnableProductVariantId = model.EnableProductVariantId;
                _bulkProductVariantExportSettings.EnableProductVariantName = model.EnableProductVariantName;
                _bulkProductVariantExportSettings.EnableStockQuantity = model.EnableStockQuantity;            

                _settingService.SaveSetting(_bulkProductVariantExportSettings);

                SuccessNotification("Settings has been updated successfully.");

                return RedirectToRoute("Plugin.Misc.MegaBulkProductEdit.Manage");
            }
            return View("~/Plugins/Misc.MegaBulkProductEdit/Views/MiscMegaBulkProductEdit/Settings.cshtml", model);
        }

        #endregion
    }
}