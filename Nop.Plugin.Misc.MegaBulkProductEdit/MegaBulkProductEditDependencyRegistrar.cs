﻿using Autofac.Integration.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Core.Infrastructure;
using Nop.Core.Data;
using Nop.Data;
using Autofac.Core;
using Autofac;
using Nop.Plugin.Misc.MegaBulkProductEdit.Services;
using Nop.Core.Configuration;

namespace Nop.Plugin.Misc.MegaBulkProductEdit
{
    public class MegaBulkProductEditDependencyRegistrar : IDependencyRegistrar
	{
		private const string CONTEXT_NAME = "nop_object_context_mega_bulk_product_variant_edit";

		public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
		{
			//Register services
            builder.RegisterType<MegaBulkProductEditService>().As<IMegaBulkProductEditService>();
            builder.RegisterType<ProductExportManager>().As<IProductExportManager>();

			//Override the repository injection
			builder.RegisterType<EfRepository<Product>>().As<IRepository<Product>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME)).InstancePerHttpRequest();
		}

	    public int Order
		{
			get
			{
				return 0;
			}
		}
    }
}
