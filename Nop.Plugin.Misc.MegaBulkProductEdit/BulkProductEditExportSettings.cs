using Nop.Core.Configuration;

namespace Nop.Plugin.Misc.MegaBulkProductEdit
{
    public class BulkProductEditExportSettings:ISettings
    {
        public bool EnableProductId { get; set; }
        public bool EnableProductVariantId { get; set; }
        public bool EnableProductVariantName { get; set; }
        public bool EnableProductDescription { get; set; }
        public bool EnableStockQuantity { get; set;}
        public bool EnableDisableBuyButton { get; set; }
        public bool EnableProductCost { get; set; }
        public bool EnablePrice { get; set; }
        public bool EnableCategory { get; set; }
        public bool EnableManufacturer { get; set; }
        public bool EnableCreatedOnUtc { get; set; }
    }
}