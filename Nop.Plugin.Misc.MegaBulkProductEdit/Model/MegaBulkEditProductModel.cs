﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.MegaBulkProductEdit.Model
{
    public partial class MegaBulkEditProductModel : BaseNopEntityModel
    {
        [AllowHtml]
        public string Name { get; set; }

        [AllowHtml]
        public string Sku { get; set; }

        public decimal Price { get; set; }

        public decimal OldPrice { get; set; }

        [UIHint("ManageInventoryTypes")]
        public string ManageInventoryMethod { get; set; }

        public int StockQuantity { get; set; }

        public bool Published { get; set; }

        public virtual bool DisableBuyButton { get; set; }

        
        public virtual decimal ProductCost { get; set; }

        
        public virtual decimal AdditionalShippingCharge { get; set; }

        

        public int Add { get; set; }

        public int Delete { get; set; }
    }
}