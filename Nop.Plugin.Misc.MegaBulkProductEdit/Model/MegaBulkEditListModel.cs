﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.MegaBulkProductEdit.Model
{
    public class MegaBulkEditListModel : BaseNopModel
    {
        public MegaBulkEditListModel()
        {
            AvailableDisableBuyButton=  new List<SelectListItem>();
            AvailableInventoryType=new List<SelectListItem>();
            AvailableCategories=new List<SelectListItem>();
            AvailableManufacturers=new List<SelectListItem>();
            AvailableStores=new List<SelectListItem>();
            AvailableWarehouses=new List<SelectListItem>();
            AvailableVendors=new List<SelectListItem>();
            AvailableProductTypes=new List<SelectListItem>();
        }

        
        [AllowHtml]
        public virtual string SearchProductName { get; set; }

        
        public virtual int SearchCategoryId { get; set; }

        
        public virtual int SearchManufacturerId { get; set; }
        
        
        [UIHint("DateNullable")]
        public virtual DateTime? StartDate { get; set; }
        
        
        [UIHint("DateNullable")]
        public virtual DateTime? EndDate { get; set; }

        
        public virtual decimal PriceFrom { get; set; }

        
        public virtual decimal PriceTo { get; set; }

        //manage inventory type (managed, don't track
        
        public virtual string ManageInventoryMethodValue { get; set; }
        
        //stock quantity from/to
        
        public virtual int StockQuantityFrom { get; set; }

        
        public virtual int StockQuantityTo { get; set; }

        //product(Published N Unpublished)
        
        public virtual String ProductType { get; set; }

        //merchant name
       

        //disable buy button
        
        public virtual string DisableBuyButton { get; set; }
        
        
        public virtual int ProductTypeId { get; set; }

        
        public bool SearchIncludeSubCategories { get; set; }
        
        public int SearchStoreId { get; set; }
        
        public int SearchVendorId { get; set; }
        
        public int SearchWarehouseId { get; set; }
        
        public int SearchProductTypeId { get; set; }
        
        public IList<SelectListItem> AvailableStores { get; set; }
        public IList<SelectListItem> AvailableWarehouses { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }
        public IList<SelectListItem> AvailableProductTypes { get; set; }
        public virtual List<SelectListItem> AvailableDisableBuyButton { get; set; }
        public virtual List<SelectListItem> AvailableInventoryType { get; set; }
        public virtual IList<SelectListItem> AvailableCategories { get; set; }
        public virtual IList<SelectListItem> AvailableManufacturers { get; set; }
        
        public virtual bool IsLoggedInAsVendor { get; set; }
    }
}