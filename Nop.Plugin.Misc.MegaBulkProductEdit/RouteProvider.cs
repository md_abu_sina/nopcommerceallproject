﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Misc.MegaBulkProductEdit
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            #region Bulk Product Variant Edit

            routes.MapRoute("Plugin.Misc.MegaBulkProductEdit.Manage", "Plugin/Misc/MegaBulkProductEdit/Manage",
            new
            {
                controller = "MiscMegaBulkProductEdit",
                action = "BulkEdit"
            },
            new[] { "Nop.Plugin.Misc.MegaBulkProductEdit.Controllers" }).DataTokens.Add("area", "admin");

            routes.MapRoute("Plugin.Misc.MegaBulkProductEdit.ExportSettings", "Plugin/Misc/MegaBulkProductEdit/Settings",
            new
            {
                controller = "MiscMegaBulkProductEdit",
                action = "ExportSettings"
            },
            new[] { "Nop.Plugin.Misc.MegaBulkProductEdit.Controllers" }).DataTokens.Add("area", "admin");

            #endregion
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
