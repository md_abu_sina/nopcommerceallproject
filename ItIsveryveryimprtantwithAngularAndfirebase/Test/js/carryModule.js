var firebaseinaction = angular.module('firebaseinaction', ["firebase", "ngRoute", 'angular.filter']);


firebaseinaction.config(function ($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: '/AngularViews/CarrierPage.html'
        })
        .when('/addnewcarrier', {
            templateUrl: '/AngularViews/AddCarrierPage.html'
        });
});

//firebaseinaction.config(function ($routeProvider, $mdThemingProvider, $mdIconProvider) {

//    $routeProvider
//        .when('/user', {
//            templateUrl: '/AngularViews/pages/user.html',
//            //controller: 'userController'
//        })
//        .when('/user-role/:userId', {
//            templateUrl: '/AngularViews/pages/user-role.html',
//            //controller: 'userRoleController'
//        })
//        .when('/role-permission/:roleId', {
//            templateUrl: '/AngularViews/pages/permission.html',
//            //controller: 'permissionController'
//        })
//        // route for the home page
//        .when('/', {
//            templateUrl: '/AngularViews/pages/home.html',
//            //controller: 'DashboardController'
//        })
//        .when('/register', {
//            templateUrl: '/AngularViews/pages/register.html',
//            //controller: 'registerController'
//        })
//        .when('/login', {
//            templateUrl: '/AngularViews/pages/login.html',
//            //controller: 'loginController'
//        })
//         .when('/RegisterForExposeRecipe', {
//             templateUrl: '/AngularViews/pages/registrationforjs.html',
//             //controller: 'loginController'
//         })
//          .when('/RegisterForExposeRecipeDone', {
//              templateUrl: '/AngularViews/pages/registerforexposerecipeDone.html',
//              //controller: 'loginController'
//          })
//           .when('/companywhowanttoshaterecipe', {
//               templateUrl: '/AngularViews/pages/companywhowanttoshaterecipe.html',
//               //controller: 'loginController'
//           })
//         .when('/sharerecipeforouterworld', {
//             templateUrl: '/AngularViews/pages/RecipeShare.html',
//             //controller: 'loginController'
//         })
//        // route for the recipe page   
//        .when('/recipe', {
//            templateUrl: '/AngularViews/pages/recipe.html',

//        })
//        .when('/chains', {
//            templateUrl: '/AngularViews/pages/chains.html'

//        })
//         .when('/create-chain', {
//             templateUrl: '/AngularViews/pages/CreateChain.html'

//         })
//        .when('/recipe/:status/:ingredientId/:title?', {
//            templateUrl: '/AngularViews/pages/recipe.html',
//            //controller: 'recipeController'
//        })
//        .when('/recipe/:recipeId', {
//            templateUrl: '/AngularViews/pages/recipeDetail.html',
//            //controller: 'recipeDetailController'
//        })
//        .when('/edit/:recipeId', {
//            templateUrl: '/AngularViews/pages/edit.html',
//            //controller: 'editController'
//        })
//        .when('/recipeingredient/:ingredientId', {
//            templateUrl: '/AngularViews/pages/recipe-ingredient.html',
//            //controller: 'recipeIngredientController'
//        })

//        // route for the contact page
//        .when('/contact', {
//            templateUrl: '/AngularViews/pages/contact.html',
//            //controller: 'contactController'
//        })
//        .when('/food', {
//            templateUrl: '/AngularViews/pages/food.html',
//            //controller: 'foodController'
//        })
//        .when('/foodSearchByNdbNo/:ndbNo', {
//            templateUrl: '/AngularViews/pages/food.html',
//            //controller: 'foodController'
//        })
//        .when('/food/:foodId', {
//            templateUrl: '/AngularViews/pages/foodDetail.html',
//            //controller: 'foodDetailController'
//        })
//        .when('/special', {
//            templateUrl: '/AngularViews/pages/special.html',
//            //controller: 'specialController'
//        })
//        .when('/special/:chain/:createNew?', {
//            templateUrl: '/AngularViews/pages/special.html',
//            //controller: 'specialController'
//        })
//        .when('/addRecipe', {
//            templateUrl: '/AngularViews/pages/addRecipe.html',
//            //controller: 'addRecipeController'
//        })
//        .when('/recipestatistics', {
//            templateUrl: '/AngularViews/pages/recipeStatistics.html',
//            //controller: 'addRecipeController'
//        })
//        .when('/changepassword/:userId', {
//            templateUrl: '/AngularViews/pages/changepassword.html',
//            //controller: 'addRecipeController'
//        })
//        .when('/emailconfirmed', {
//            templateUrl: '/AngularViews/pages/emailconfirmed.html',
//            //controller: 'addRecipeController'
//        })
//        .when('/resetpasswordemail', {
//            templateUrl: '/AngularViews/pages/resetpasswordemail.html',
//            //controller: 'addRecipeController'
//        })
//        .when('/resetpassword/:userId', {
//            templateUrl: '/AngularViews/pages/resetpassword.html',
//            //controller: 'addRecipeController'
//        })
//         .when('/recipestatistics/:chain', {
//             templateUrl: '/AngularViews/pages/recipeStatistics.html',
//             //controller: 'specialController'
//         })
//        /*.when('/ingredient', {
//            templateUrl: '/AngularViews/pages/ingredient.html',
//            controller: 'ingredientController'
//        })
//        .when('/ingredient/:ingredientId', {
//            templateUrl: '/AngularViews/pages/ingredientDetail.html',
//            controller: 'ingredientDetailController'
//        })
//        .when('/ingredient/:description/:measure/:foodId', {
//            templateUrl: '/AngularViews/pages/ingredient.html',
//            controller: 'ingredientController'
//        })
//        .when('/recipe/:title', {
//            templateUrl: '/AngularViews/pages/recipe.html',
//            controller: 'recipeController'
//        })*/
//        .when('/addRecipe/:recipeId', {
//            templateUrl: '/AngularViews/pages/addRecipe.html',
//            //controller: 'addRecipeController'
//        })
//        .when('/ingredient', {
//            templateUrl: '/AngularViews/pages/ingredient.html',
//            //controller: 'ingredientController'
//        })
//        .when('/food-ingredient/:foodId', {
//            templateUrl: '/AngularViews/pages/ingredient.html',
//            //controller: 'ingredientController'
//        })
//        .when('/ingredient/:ingredientId', {
//            templateUrl: '/AngularViews/pages/ingredientDetail.html',
//            //controller: 'ingredientDetailController'
//        })
//        .when('/ingredient/:foodId/:measure?/:description?', {
//            templateUrl: '/AngularViews/pages/ingredient.html',
//            //controller: 'ingredientController'
//        })
//        .when('/addFood', {
//            templateUrl: '/AngularViews/pages/addFood.html',
//            //controller: 'addFoodController'
//        })
//        .when('/food/:description?/:measure?/:ndbNo?', {
//            templateUrl: '/AngularViews/pages/food.html',
//            //controller: 'foodController'
//        })
//        .when('/addFood/:foodId', {
//            templateUrl: '/AngularViews/pages/addFood.html',
//            //controller: 'addFoodController'
//        })
//        .when('/addIngredient', {
//            templateUrl: '/AngularViews/pages/addIngredient.html',
//            //controller: 'addIngredientController'
//        })
//        .when('/addIngredient/:ingredientId', {
//            templateUrl: '/AngularViews/pages/addIngredient.html',
//            //controller: 'addIngredientController'
//        })
//        .when('/addSpecialList/:id', {
//            templateUrl: '/AngularViews/pages/addSpecialList.html',
//            //controller: 'specialListAddController'
//        })
//         .when('/editSpecialList/:id/:specialgroupId', {
//             templateUrl: '/AngularViews/pages/editSpecialList.html',
//             //controller: 'specialListEditController'
//         })
//        .when('/SpecialListIndex/:specialGroupId', {
//            templateUrl: '/AngularViews/pages/SpecialListIndex.html',
//            //controller: 'specialListIndexController'
//        })
//        .when('/RecipeIngredient/edit/:recipeId/:ingredientId',
//        {
//            templateUrl: '/AngularViews/pages/RecipeIngredientEdit.html',
//            //controller: 'RecipeIngredientEditController'
//        })
//        .when('/aisles',
//        {
//            templateUrl: '/AngularViews/pages/aisles.html',
//            //controller: 'AislesController'
//        })
//        //.when('/aisles/:storeId?',
//        //{
//        //    templateUrl: '/AngularViews/pages/aisles.html',
//        //    controller: 'AislesSearchController'
//        //})
//        .when('/editDate/:id',
//        {
//            templateUrl: '/AngularViews/pages/specialEditDate.html',
//            //controller: 'specialEditDateController'
//        })
//        .when('/specialimportforspecialgroup/:specialgroupid',
//        {
//            templateUrl: '/AngularViews/pages/specialimport.html',
//            //controller: 'specialEditDateController'
//        })
//        .when('/category_aisles',
//        {
//            templateUrl: '/AngularViews/pages/category_aisles.html',
//            //controller: 'category_aislesController'
//        })
//        .when('/stores',
//        {
//            templateUrl: '/AngularViews/pages/stores.html',
//            controller: 'StoreController'
//        })
//        .when('/departments/:storeid?',
//        {
//            templateUrl: '/AngularViews/pages/departments.html',
//            controller: 'departmentsController'
//        })
//        .when('/createRole/:userId/:id?',
//        {
//            templateUrl: '/AngularViews/pages/createRole.html',
//            //controller: 'createRoleController'
//        })
//        .when('/edit-user/:userId',
//        {
//            templateUrl: '/AngularViews/pages/editUser.html',
//            //controller: 'editUserRoleController'
//        })
//        //====
//          .when('/create-user',
//        {
//            templateUrl: '/AngularViews/pages/CreateUser.html',
//            //controller: 'createUserRoleController'
//        })
//        .when('/detail-user/:userId',
//        {
//            templateUrl: '/AngularViews/pages/detailUser.html',
//            //controller: 'detailUserController'
//        })
//        .when('/createPermission/:id?',
//        {
//            templateUrl: '/AngularViews/pages/createPermission.html',
//            //controller: 'createPermissionController'
//        });
//});