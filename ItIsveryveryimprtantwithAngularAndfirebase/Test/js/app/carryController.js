firebaseinaction.controller('carrierController', function ($scope, $firebaseObject, $sce, $firebaseArray, $window) {
    $scope.carrierData = [];

    var ref = new Firebase("https://amber-fire-4900.firebaseio.com/");


    $scope.innerHtml = $scope.innerHtml || {};

    $scope.FirstObject = ref.child("FirstObject");
    $scope.SecondObject = ref.child("SecondObject");
    $scope.ThirdObject = ref.child("ThirdObject");

    $scope.buildHTMLS = [];

    addDataToFirebaseFirstObject();
    addDataToFirebaseSecondObject();
    addDataToFirebaseThirdObject();

    function addDataToFirebaseFirstObject() {
        //refMsg.remove();
        $scope.FirstObject.set({
            name: "Carrier 1",
            enclosed: false,
            open: true,
            serviceArea: {
                CA: {
                    notes: "Nor Cal (including Santa Rosa)"
                },
                CT: {
                    notes: "reasonable CT only"
                },
                MA: {
                    notes: "reasonable MA only"
                },
                NY: {
                    notes: "much better along the 90"
                }
            }
        });
        
    }
    function addDataToFirebaseSecondObject() {
        //refSecondMsg.remove();
        $scope.SecondObject.set({
            name: "Carrier 2",
            enclosed: false,
            open: true,
            serviceArea: {
                AZ: {
                    notes: "''"
                },
                CA: {
                    notes: "''"
                },
                GA: {
                    notes: "Only from CA and AZ"
                },
                IL: {
                    notes: "Only from CA and AZ.  CHI only."
                },
                MI: {
                    notes: "Only from CA and AZ"
                },
                TX: {
                    notes: "Only from CA and AZ"
                }
            }
        });
    }
    function addDataToFirebaseThirdObject() {
        //refThirdMsg.remove();
        $scope.ThirdObject.set({
            name: "Carrier 3",
            enclosed: false,
            open: true,
            exclusionArea: {
                MA: {
                    notes: "''"
                }
            }
        });
    }

 

    ref.on("value", function (snapshot) {
        $scope.carrierData.push(snapshot.val().FirstObject);
        $scope.carrierData.push(snapshot.val().SecondObject);
        $scope.carrierData.push(snapshot.val().ThirdObject);
        if (localStorage.getItem("firstTxtValue") && localStorage.getItem("secondTxtValue")) {
            $scope.buildHTMLS = [];
            var carrierList = findCarriers(localStorage.getItem("firstTxtValue"), localStorage.getItem("secondTxtValue"));
            displayAnswer(localStorage.getItem("firstTxtValue"), localStorage.getItem("secondTxtValue"), carrierList);
        }
        $scope.$apply();

    }, function (errorObject) {
        alert("The read failed: " + errorObject.code);
    });


    $scope.carryWrap = function () {
        localStorage.setItem("firstTxtValue", $scope.firstTxt);
        localStorage.setItem("secondTxtValue", $scope.secondTxt);
        $scope.buildHTMLS = [];
        var carrierList = findCarriers($scope.firstTxt, $scope.secondTxt);
        displayAnswer($scope.firstTxt, $scope.secondTxt, carrierList);
    };


    function findCarriers(z1, z2) {
        var viableCarriers = [];
        var carrierLen = $scope.carrierData.length;
        for (iC = 0; iC < carrierLen; iC++) {
            var carrier = $scope.carrierData[iC];
            var exclusionMap = carrier.exclusionArea;
            if (exclusionMap) {
                if (!exclusionMap[z1] && !exclusionMap[z2]) {
                    viableCarriers.push(carrier);
                }
            } else {
                var serviceMap = carrier.serviceArea
                if (serviceMap[z1] && serviceMap[z2]) {
                    viableCarriers.push(carrier);
                }
            }
        }
        return viableCarriers
    }

    function displayAnswer(z1, z2, carrierList) {
        var listLen = carrierList.length;
        $scope.firstHead = z1;
        $scope.secondHead = z2;
        var innerHt = innerHt || {};

        for (iC = 0; iC < listLen; iC++) {
            var noteArray = [];
            var carrier = carrierList[iC];
            var serviceMap = carrier.serviceArea;

            innerHt.name = carrier.name;
            if (serviceMap && serviceMap[z1] && serviceMap[z1].notes) {
                noteArray.push({
                    note: serviceMap[z1].notes
                });
            }
            if (serviceMap && serviceMap[z2] && serviceMap[z2].notes) {
                noteArray.push({
                    note: serviceMap[z2].notes
                });
            }
            $scope.buildHTMLS.push({
                name: carrier.name,
                note: noteArray
            });
        }
    }

    $scope.removePreviousSearch = function () {
        localStorage.removeItem("firstTxtValue");
        localStorage.removeItem("secondTxtValue");
        $window.location.reload();
    }
    $scope.addCarrier = function () {
        window.location.hash = "/addnewcarrier";
    }
});