firebaseinaction.controller('carrierAddController', function ($scope, $firebaseObject, $sce, $firebaseArray) {
    var ref = new Firebase("https://amber-fire-4900.firebaseio.com/");
    $scope.serviceAreaArray = $scope.serviceAreaArray || {};
    $scope.open = $scope.enclosed = false;

    var obj = $firebaseObject(ref);
    $scope.serverData = [];

    ShowDataFromFireBase();

   function ShowDataFromFireBase () {
        ref.once("value", function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var key = childSnapshot.key();
                var childData = childSnapshot.val();
                $scope.serverData.push(childData)
                $scope.$apply();
            });

        });
    }


    $scope.rows = [];

    $scope.rows.push({
        nameOfservice: '',
        serviceNote: ''
    });

    $scope.addField = function () {
        $scope.rows.push({
            nameOfservice: '',
            serviceNote: ''
        });
        console.log($scope.rows);
    }
    
    $scope.addCarrier = function () {
        var service = {};
        for (var i = 0; i < $scope.rows.length; i++) {
            service[$scope.rows[i].nameOfservice] = {
                "notes": $scope.rows[i].serviceNote
            };
        }
        var oncomplete = function () {
            ShowDataFromFireBase();
        };
        ref.push({
            name: $scope.carriername,
            enclosed: $scope.enclosed,
            open: $scope.open,
            serviceArea: service
        },oncomplete);
    }

    $scope.editField = function (itemNumber) {
        $scope.carriername = $scope.serverData[itemNumber].name;
        $scope.enclosed = $scope.serverData[itemNumber].enclosed;
        $scope.open = $scope.serverData[itemNumber].open;
        console.log($scope.serverData[itemNumber].serviceArea);
        $scope.rows = [];

        for (var prop in $scope.serverData[itemNumber].serviceArea) {
            $scope.rows.push({
                nameOfservice: prop,
                serviceNote: $scope.serverData[itemNumber].serviceArea[prop].notes
            });
        }
        console.log($scope.rows);
    }
});