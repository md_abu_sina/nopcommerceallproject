using System;
using Nop.Core;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.LiveNotification.Domain
{
    public class LiveNotificationDomain : BaseEntity
    {
        public int ProductId { get; set; }
        public string CreatedOnUtc { get; set; }
        public string UpdateOnUtc { get; set; }

    }
}