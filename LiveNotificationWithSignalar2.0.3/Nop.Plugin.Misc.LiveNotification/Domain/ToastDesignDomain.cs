using System;
using Nop.Core;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.LiveNotification.Domain
{
    public class ToastDesignDomain : BaseEntity
    {
        public string BackgroundColor { get; set; }
        public string ShowDuration { get; set; }
        public string HideDuration { get; set; }
        public string TimeOut { get; set; }
        public string ExtendedTimeOut { get; set; }

        public string CreatedOnUtc { get; set; }
        public string UpdateOnUtc { get; set; }

    }
}