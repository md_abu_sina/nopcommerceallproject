﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Seo;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using System.Collections.Generic;
using Nop.Services.Media;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;
using Nop.Web.Infrastructure.Cache;
using Nop.Core.Caching;
using System.Diagnostics;
using Nop.Services.Security;
using Nop.Services.Tax;
using Nop.Services.Directory;
using System.IO;
using Nop.Plugin.Misc.LiveNotification.Services;
using Nop.Plugin.Misc.LiveNotification.Models;
using Nop.Services.Orders;
using Nop.Plugin.Misc.LiveNotification.Domain;
using Nop.Plugin.LiveNotification.Models;
using Nop.Web.Models.Common;
using Nop.Admin.Models.Catalog;
using Nop.Services.Configuration;

namespace Nop.Plugin.Misc.LiveNotification.Controllers
{

    public class LiveNotificationController : Controller
    {
        #region Field

        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IWorkContext _workContext;
        private readonly CatalogSettings _catalogSettings;
        private readonly IProductService _productService;
        private readonly ILocalizationService _localizationService;
        private readonly ICategoryService _categoryService;
        private readonly IPictureService _pictureService;
        private readonly ILiveNotificationService _liveNotificationService;
        private readonly IToastDesignService _toastDesignService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IPermissionService _permissionService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly ICurrencyService _currencyService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderService _orderService;
        private readonly ISettingService _settingContext;


        #endregion

        #region Ctr

        public LiveNotificationController(IDateTimeHelper dateTimeHelper,
            IWorkContext workContext,
            CatalogSettings catalogSettings,
            IProductService productService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            ICategoryService categoryService,
            ILiveNotificationService liveNotificationService,
            IToastDesignService toastDesignService,
            IWebHelper webHelper,
            IStoreContext storeContext,
            ICacheManager cacheManager,
            IPermissionService permissionService,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            ICurrencyService currencyService,
            IPriceFormatter priceFormatter,
            IOrderService orderService,
            ISettingService settingContext)
        {
            _dateTimeHelper = dateTimeHelper;
            _workContext = workContext;
            _catalogSettings = catalogSettings;
            _productService = productService;
            _localizationService = localizationService;
            _categoryService = categoryService;
            _productService = productService;
            _pictureService = pictureService;
            _liveNotificationService = liveNotificationService;
            _toastDesignService = toastDesignService;
            _webHelper = webHelper;
            _storeContext = storeContext;
            _cacheManager = cacheManager;
            _permissionService = permissionService;
            _priceCalculationService = priceCalculationService;
            _taxService = taxService;
            _currencyService = currencyService;
            _priceFormatter = priceFormatter;
            _orderService = orderService;
            _settingContext = settingContext;
        }

        #endregion

        #region Methods


        public ActionResult PublicPage()
        {
            var viewBag = _liveNotificationService.CountTodayOrderedProductCountToday(DateTime.UtcNow);
            ViewBag.todaycount = viewBag;

            return View("~/Plugins/LiveNotification/Views/LiveNotificationView/PublicPage.cshtml");
        }

        public ActionResult TodayOrderFlyOut()
        {
            var todayOrders = _liveNotificationService.GetNotificationForTodayOrder(5);
            TodayOrderModelList model = new TodayOrderModelList();

            foreach (var todayOrder in todayOrders)
            {
                var pictures = _pictureService.GetPicturesByProductId(todayOrder.ProductId);
                var defaultPicture = pictures.FirstOrDefault();

                model.TodayOrder.Add(new TodayOrderModel
                {
                    ProductId = todayOrder.ProductId,
                    ProductName = _productService.GetProductById(todayOrder.ProductId).Name,
                    SeName = _productService.GetProductById(todayOrder.ProductId).GetSeName(),
                    Price = _productService.GetProductById(todayOrder.ProductId).Price,
                    ImgUrl = _pictureService.GetPictureUrl(defaultPicture, 75),
                });
            }

            return View("~/Plugins/LiveNotification/Views/LiveNotificationView/TodayOrderFlyout.cshtml", model);
        }

        public ActionResult GetAllOrderedProduct(int page = 1)
        {
            if (page >= 1)
                page = page - 1;

            var ids = new List<int>();
            List<Product> products = new List<Product>();
            var modelToday = new TodayOrderDetailListModel();
            var todayOrderDetailModel = new TodayOrderDetailModel();
            var todayOrders = _liveNotificationService.GetLiveNotification(page, 6);
            ids = _liveNotificationService.GetLiveNotification(page, 6).Select(x => x.ProductId).ToList();


            foreach (var product in _productService.GetProductsByIds(ids.ToArray()))
            {
                if (product.Published && !product.Deleted)
                {
                    if (product.StockQuantity > product.MinStockQuantity)
                    {
                        var productToView = _productService.GetProductById(product.Id);
                        products.Add(product);
                    }
                }
            }
            modelToday.Products.AddRange(PrepareProductOverviewModels(products));
            modelToday.PagerModel = new PagerModel
            {
                PageSize = todayOrders.PageSize,
                TotalRecords = todayOrders.TotalCount,
                PageIndex = todayOrders.PageIndex,
                RouteActionName = "Nop.Plugin.Misc.LiveNotification.TotalOrderedProduct",
                UseRouteLinks = true,
                RouteValues = new LiveNotificationRouteValues { page = page }
            };

            //return products;

            return View("~/Plugins/LiveNotification/Views/LiveNotificationView/AllOrderedProduct.cshtml", modelToday);
        }

        public ActionResult OrderedProductAtHomePage(int page = 1)
        {
            string showOrderedProductAtHomePage = _settingContext.GetSettingByKey<string>("ShowRecentlyOrderProduct");
            int numberOfProductToShow = Convert.ToInt16(_settingContext.GetSettingByKey<string>("NumberOfRecentlyOrderProduct"));


            if (page >= 1)
                page = page - 1;

            var ids = new List<int>();
            List<Product> products = new List<Product>();
            var modelToday = new TodayOrderDetailListHomePageModel();
            //var todayOrderDetailModel = new TodayOrderDetailModel();
            var todayOrders = _liveNotificationService.GetLiveNotification(page, numberOfProductToShow);
            ids = _liveNotificationService.GetLiveNotification(page, numberOfProductToShow).Select(x => x.ProductId).ToList();


            foreach (var product in _productService.GetProductsByIds(ids.ToArray()))
            {
                if (product.Published && !product.Deleted)
                {
                    if (product.StockQuantity > product.MinStockQuantity)
                    {
                        var productToView = _productService.GetProductById(product.Id);
                        products.Add(product);
                    }
                }
            }
            modelToday.Products.AddRange(PrepareProductOverviewModels(products));
            modelToday.PagerModel = new PagerModel
            {
                PageSize = todayOrders.PageSize,
                TotalRecords = todayOrders.TotalCount,
                PageIndex = todayOrders.PageIndex,
                RouteActionName = "Nop.Plugin.Misc.LiveNotification.TotalOrderedProduct",
                UseRouteLinks = true,
                RouteValues = new LiveNotificationRouteValues { page = page }
            };

            modelToday.ShowOrderedProductAtHomePage = Convert.ToBoolean(showOrderedProductAtHomePage);
            //return products;

            return View("~/Plugins/LiveNotification/Views/LiveNotificationView/OrderedProductAtHomePage.cshtml", modelToday);
        }

        public ActionResult OrderedProductAtHomePageAjax()
        {
            string showOrderedProductAtHomePage = _settingContext.GetSettingByKey<string>("ShowRecentlyOrderProduct");
            int numberOfProductToShow = Convert.ToInt16(_settingContext.GetSettingByKey<string>("NumberOfRecentlyOrderProduct"));

            var ids = new List<int>();
            List<Product> products = new List<Product>();
            var modelToday = new TodayOrderDetailListHomePageModel();
            var todayOrders = _liveNotificationService.GetLiveNotification(0, numberOfProductToShow);
            ids = _liveNotificationService.GetLiveNotification(0, numberOfProductToShow).Select(x => x.ProductId).ToList();


            foreach (var product in _productService.GetProductsByIds(ids.ToArray()))
            {
                if (product.Published && !product.Deleted)
                {
                    if (product.StockQuantity > product.MinStockQuantity)
                    {
                        var productToView = _productService.GetProductById(product.Id);
                        products.Add(product);
                    }
                }
            }
            modelToday.Products.AddRange(PrepareProductOverviewModels(products));
            //return products;
            modelToday.ShowOrderedProductAtHomePage = Convert.ToBoolean(showOrderedProductAtHomePage);
            string html = this.RenderPartialViewToString("~/Plugins/LiveNotification/Views/LiveNotificationView/OrderedProductAtHomePage.cshtml", modelToday);

            return Content(html);
        }

        public JsonResult TodayOrderFlyOutAjax()
        {
            var todayOrders = _liveNotificationService.GetNotificationForTodayOrder(5);
            TodayOrderModelList model = new TodayOrderModelList();

            foreach (var todayOrder in todayOrders)
            {
                var pictures = _pictureService.GetPicturesByProductId(todayOrder.ProductId);
                var defaultPicture = pictures.FirstOrDefault();

                model.TodayOrder.Add(new TodayOrderModel
                {
                    ProductId = todayOrder.ProductId,
                    ProductName = _productService.GetProductById(todayOrder.ProductId).Name,
                    SeName = _productService.GetProductById(todayOrder.ProductId).GetSeName(),
                    Price = _productService.GetProductById(todayOrder.ProductId).Price,
                    ImgUrl = _pictureService.GetPictureUrl(defaultPicture, 75),
                });
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult OrderedItem(string orderId)
        {
            var viewBag = _liveNotificationService.CountTodayOrderedProductCountToday(DateTime.UtcNow);
            ViewBag.todaycount = viewBag;
            LiveNotificationModelList objOfLiveNotificationModelList = new LiveNotificationModelList();
            objOfLiveNotificationModelList.TodayTotalCount = viewBag.ToString();
            if (Convert.ToInt16(viewBag) > 0)
            {
                var order = _orderService.GetOrderById(Convert.ToInt16(orderId));
                var products = order.OrderItems;

                foreach (var product in products)
                {
                    var pictures = _pictureService.GetPicturesByProductId(product.ProductId);
                    var defaultPicture = pictures.FirstOrDefault();
                    objOfLiveNotificationModelList.Products.Add(new LiveNotificationModel
                    {
                        ProductId = product.ProductId,
                        ProductName = product.Product.Name,
                        SeName = product.Product.GetSeName(),
                        ShortDescription = product.Product.ShortDescription,
                        Price = product.Product.Price,
                        ImgUrl = _pictureService.GetPictureUrl(defaultPicture, 75)
                    });
                }

                ToastDesignInsert();

                var toastDesign = _toastDesignService.GetToastDesignFirst();

                objOfLiveNotificationModelList.ToastDesign.Add(new ToastDesignModel
                {
                    BackgroundColor = "#" + toastDesign.FirstOrDefault().BackgroundColor,
                    ShowDuration = toastDesign.FirstOrDefault().ShowDuration,
                    HideDuration = toastDesign.FirstOrDefault().HideDuration,
                    TimeOut = toastDesign.FirstOrDefault().TimeOut,
                    ExtendedTimeOut = toastDesign.FirstOrDefault().ExtendedTimeOut
                });
            }
            return Json(objOfLiveNotificationModelList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ToastDesign()
        {
            ToastDesignInsert();
            var model = new ToastDesignModel();
            return View("~/Plugins/LiveNotification/Views/LiveNotificationView/ToastDesign.cshtml", model);
        }

        public void ToastDesignInsert()
        {
            var toastDesign = _toastDesignService.GetToastDesignFirst();
            if (toastDesign.Count() == 0)
            {
                ToastDesignDomain objOfToastDesignDomain = new ToastDesignDomain();
                objOfToastDesignDomain.BackgroundColor = "008080";
                objOfToastDesignDomain.ShowDuration = "10000";
                objOfToastDesignDomain.HideDuration = "1000";
                objOfToastDesignDomain.TimeOut = "10000";
                objOfToastDesignDomain.ExtendedTimeOut = "2000";
                objOfToastDesignDomain.CreatedOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");
                objOfToastDesignDomain.UpdateOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");
                _toastDesignService.Insert(objOfToastDesignDomain);
            }
        }
        [HttpPost]
        public ActionResult UpdateToastDesign(ToastDesignModel model)
        {
            ToastDesignDomain objOfToastDesignDomain = new ToastDesignDomain();

            _toastDesignService.Delete(model.Id);

            objOfToastDesignDomain.Id = model.Id;
            objOfToastDesignDomain.BackgroundColor = model.BackgroundColor;
            objOfToastDesignDomain.ShowDuration = model.ShowDuration;
            objOfToastDesignDomain.HideDuration = model.HideDuration;
            objOfToastDesignDomain.TimeOut = model.TimeOut;
            objOfToastDesignDomain.ExtendedTimeOut = model.ExtendedTimeOut;
            objOfToastDesignDomain.CreatedOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");
            objOfToastDesignDomain.UpdateOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");

            _toastDesignService.Insert(objOfToastDesignDomain);

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditeToastDesign(int Id)
        {
            var singleToastDesign = _toastDesignService.GetToastDesignById(Id);
            ViewBag.backcolor = singleToastDesign.BackgroundColor;
            return Json(new
            {
                Id = singleToastDesign.Id,
                BackgroundColor = singleToastDesign.BackgroundColor.Replace("#", ""),
                ShowDuration = singleToastDesign.ShowDuration,
                HideDuration = singleToastDesign.HideDuration,
                TimeOut = singleToastDesign.TimeOut,
                ExtendedTimeOut = singleToastDesign.ExtendedTimeOut
            });

        }


        [HttpPost]
        public ActionResult ToastDesignList(DataSourceRequest command, ToastDesignModel model)
        {

            var toastDesign = _toastDesignService.GetToastDesign(pageIndex: command.Page - 1, pageSize: command.PageSize);
            var gridModel = new DataSourceResult();
            gridModel.Data = toastDesign.Select(x =>
            {
                return new ToastDesignModel()
                {
                    Id = x.Id,
                    BackgroundColor = "#" + x.BackgroundColor,
                    ShowDuration = x.ShowDuration,
                    HideDuration = x.HideDuration,
                    TimeOut = x.TimeOut,
                    ExtendedTimeOut = x.ExtendedTimeOut,
                    CreatedOnUtc = x.CreatedOnUtc,
                    UpdateOnUtc = x.UpdateOnUtc
                };
            });
            gridModel.Total = toastDesign.TotalCount;
            return Json(gridModel);
        }
        #endregion

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            return View("~/Plugins/LiveNotification/Views/LiveNotificationView/Configure.cshtml");
        }

        protected IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
    bool preparePriceModel = true, bool preparePictureModel = true,
    int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
    bool forceRedirectionAfterAddingToCart = false)
        {
            if (products == null)
                throw new ArgumentNullException("products");

            var models = new List<ProductOverviewModel>();
            foreach (var product in products)
            {
                var model = new ProductOverviewModel
                {
                    Id = product.Id,
                    Name = product.GetLocalized(x => x.Name),
                    ShortDescription = product.GetLocalized(x => x.ShortDescription),
                    FullDescription = product.GetLocalized(x => x.FullDescription),
                    SeName = product.GetSeName(),
                };
                //price
                if (preparePriceModel)
                {
                    #region Prepare product price

                    var priceModel = new ProductOverviewModel.ProductPriceModel();

                    switch (product.ProductType)
                    {
                        case ProductType.GroupedProduct:
                            {
                                #region Grouped product

                                var associatedProducts = _productService.GetAssociatedProducts(product.Id, _storeContext.CurrentStore.Id);

                                switch (associatedProducts.Count)
                                {
                                    case 0:
                                        {
                                            //no associated products
                                            priceModel.OldPrice = null;
                                            priceModel.Price = null;
                                            priceModel.DisableBuyButton = true;
                                            priceModel.DisableWishlistButton = true;
                                            priceModel.AvailableForPreOrder = false;
                                        }
                                        break;
                                    default:
                                        {
                                            //we have at least one associated product
                                            priceModel.DisableBuyButton = true;
                                            priceModel.DisableWishlistButton = true;
                                            priceModel.AvailableForPreOrder = false;

                                            if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                                            {
                                                //find a minimum possible price
                                                decimal? minPossiblePrice = null;
                                                Product minPriceProduct = null;
                                                foreach (var associatedProduct in associatedProducts)
                                                {
                                                    //calculate for the maximum quantity (in case if we have tier prices)
                                                    var tmpPrice = _priceCalculationService.GetFinalPrice(associatedProduct,
                                                        _workContext.CurrentCustomer, decimal.Zero, true, int.MaxValue);
                                                    if (!minPossiblePrice.HasValue || tmpPrice < minPossiblePrice.Value)
                                                    {
                                                        minPriceProduct = associatedProduct;
                                                        minPossiblePrice = tmpPrice;
                                                    }
                                                }
                                                if (minPriceProduct != null && !minPriceProduct.CustomerEntersPrice)
                                                {
                                                    if (minPriceProduct.CallForPrice)
                                                    {
                                                        priceModel.OldPrice = null;
                                                        priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
                                                    }
                                                    else if (minPossiblePrice.HasValue)
                                                    {
                                                        //calculate prices
                                                        decimal taxRate;
                                                        decimal finalPriceBase = _taxService.GetProductPrice(minPriceProduct, minPossiblePrice.Value, out taxRate);
                                                        decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

                                                        priceModel.OldPrice = null;
                                                        priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));

                                                    }
                                                    else
                                                    {
                                                        //Actually it's not possible (we presume that minimalPrice always has a value)
                                                        //We never should get here
                                                        //Debug.WriteLine("Cannot calculate minPrice for product #{0}", product.Id);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //hide prices
                                                priceModel.OldPrice = null;
                                                priceModel.Price = null;
                                            }
                                        }
                                        break;
                                }

                                #endregion
                            }
                            break;
                        case ProductType.SimpleProduct:
                        default:
                            {
                                #region Simple product

                                //add to cart button
                                priceModel.DisableBuyButton = product.DisableBuyButton ||
                                    !_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart) ||
                                    !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

                                //add to wishlist button
                                priceModel.DisableWishlistButton = product.DisableWishlistButton ||
                                    !_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) ||
                                    !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

                                //rental
                                priceModel.IsRental = product.IsRental;

                                //pre-order
                                if (product.AvailableForPreOrder)
                                {
                                    priceModel.AvailableForPreOrder = !product.PreOrderAvailabilityStartDateTimeUtc.HasValue ||
                                        product.PreOrderAvailabilityStartDateTimeUtc.Value >= DateTime.UtcNow;
                                    priceModel.PreOrderAvailabilityStartDateTimeUtc = product.PreOrderAvailabilityStartDateTimeUtc;
                                }

                                //prices
                                if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
                                {
                                    if (!product.CustomerEntersPrice)
                                    {
                                        if (product.CallForPrice)
                                        {
                                            //call for price
                                            priceModel.OldPrice = null;
                                            priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
                                        }
                                        else
                                        {
                                            //prices

                                            //calculate for the maximum quantity (in case if we have tier prices)
                                            decimal minPossiblePrice = _priceCalculationService.GetFinalPrice(product,
                                                _workContext.CurrentCustomer, decimal.Zero, true, int.MaxValue);

                                            decimal taxRate;
                                            decimal oldPriceBase = _taxService.GetProductPrice(product, product.OldPrice, out taxRate);
                                            decimal finalPriceBase = _taxService.GetProductPrice(product, minPossiblePrice, out taxRate);

                                            decimal oldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(oldPriceBase, _workContext.WorkingCurrency);
                                            decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

                                            //do we have tier prices configured?
                                            var tierPrices = new List<TierPrice>();
                                            if (product.HasTierPrices)
                                            {
                                                tierPrices.AddRange(product.TierPrices
                                                    .OrderBy(tp => tp.Quantity)
                                                    .ToList()
                                                    .FilterByStore(_storeContext.CurrentStore.Id)
                                                    .FilterForCustomer(_workContext.CurrentCustomer)
                                                    .RemoveDuplicatedQuantities());
                                            }
                                            //When there is just one tier (with  qty 1), 
                                            //there are no actual savings in the list.
                                            bool displayFromMessage = tierPrices.Count > 0 &&
                                                !(tierPrices.Count == 1 && tierPrices[0].Quantity <= 1);
                                            if (displayFromMessage)
                                            {
                                                priceModel.OldPrice = null;
                                                priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));
                                            }
                                            else
                                            {
                                                if (finalPriceBase != oldPriceBase && oldPriceBase != decimal.Zero)
                                                {
                                                    priceModel.OldPrice = _priceFormatter.FormatPrice(oldPrice);
                                                    priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
                                                }
                                                else
                                                {
                                                    priceModel.OldPrice = null;
                                                    priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
                                                }
                                            }
                                            if (product.IsRental)
                                            {
                                                //rental product
                                                priceModel.OldPrice = _priceFormatter.FormatRentalProductPeriod(product, priceModel.OldPrice);
                                                priceModel.Price = _priceFormatter.FormatRentalProductPeriod(product, priceModel.Price);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //hide prices
                                    priceModel.OldPrice = null;
                                    priceModel.Price = null;
                                }

                                #endregion
                            }
                            break;
                    }

                    model.ProductPrice = priceModel;

                    #endregion
                }

                //picture
                if (preparePictureModel)
                {
                    #region Prepare product picture

                    //If a size has been set in the view, we use it in priority
                    int pictureSize = productThumbPictureSize.HasValue ? productThumbPictureSize.Value : 200;
                    //prepare picture model
                    var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
                    model.DefaultPictureModel = new PictureModel
                    {
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture)
                    };
                    //"title" attribute
                    model.DefaultPictureModel.Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute)) ?
                        picture.TitleAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), model.Name);
                    //"alt" attribute
                    model.DefaultPictureModel.AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute)) ?
                        picture.AltAttribute :
                        string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"), model.Name);

                    #endregion
                }

                models.Add(model);
            }
            return models;
        }
        public virtual string RenderPartialViewToString(string viewName, object model)
        {
            //Original source code: http://craftycodeblog.com/2010/05/15/asp-net-mvc-render-partial-view-to-string/
            if (string.IsNullOrEmpty(viewName))
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");

            this.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
                var viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}