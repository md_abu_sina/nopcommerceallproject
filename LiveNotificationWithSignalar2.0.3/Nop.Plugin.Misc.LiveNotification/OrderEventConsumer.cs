﻿using System;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using Nop.Services.Events;
using Nop.Services.Orders;
using Nop.Plugin.LiveNotification;
using Nop.Plugin.Misc.LiveNotification.Services;
using Nop.Plugin.Misc.LiveNotification.Domain;

namespace Nop.Plugin.Misc.LiveNotification
{
    public class OrderEventConsumer : IConsumer<OrderPlacedEvent>
    {
        private readonly IPluginFinder _pluginFinder;
        private readonly IOrderService _orderService;
        private readonly IStoreContext _storeContext;
        private readonly ILiveNotificationService _liveNotificationService;
        public OrderEventConsumer(
            IPluginFinder pluginFinder, 
            IOrderService orderService,
            IStoreContext storeContext,
            ILiveNotificationService liveNotificationService)
        {
            this._pluginFinder = pluginFinder;
            this._orderService = orderService;
            this._storeContext = storeContext;
            _liveNotificationService = liveNotificationService;
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="eventMessage">The event message.</param>
        ///    [HubMethodName("liveNotification")]
        ///    
        public void HandleEvent(OrderPlacedEvent eventMessage)
        {
            var order = eventMessage.Order;
            var products = order.OrderItems;
            foreach (var product in products)
            {
                LiveNotificationDomain objOfLiveNotificationDomain = new LiveNotificationDomain();
                objOfLiveNotificationDomain.ProductId = product.ProductId;
                objOfLiveNotificationDomain.CreatedOnUtc = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                objOfLiveNotificationDomain.UpdateOnUtc = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                _liveNotificationService.Insert(objOfLiveNotificationDomain);
            }
            LiveNotificationHub.liveNotification(order.Id.ToString());
        }
    }
}