﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.LiveNotification.Domain;
using System.Collections.Generic;
using Nop.Services.Events;

namespace Nop.Plugin.Misc.LiveNotification.Services
{
    public partial class ToastDesignService : IToastDesignService
    {
        #region Field
        private readonly IRepository<ToastDesignDomain> _toastDesignRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion

        #region Ctr

        public ToastDesignService(IRepository<ToastDesignDomain> toastDesignRepository, IEventPublisher eventPublisher)
        {
            _toastDesignRepository = toastDesignRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        public void Delete(int Id)
        {
            //item.Deleted = true;

            var query = from c in _toastDesignRepository.Table
                        where c.Id == Id
            select c;
            var toastDesignList = query.ToList();
            foreach (var toastDesign in toastDesignList)
            {
                _toastDesignRepository.Delete(toastDesign);
            }
        }


        public bool Update(ToastDesignDomain toastDesignDomain)
        {
            if (toastDesignDomain == null)
                throw new ArgumentNullException("customer");

            _toastDesignRepository.Update(toastDesignDomain);
            return true;
        }


        public void Insert(ToastDesignDomain item)
        {
            //default value
            item.CreatedOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd");
            item.UpdateOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd");

            _toastDesignRepository.Insert(item);
        }

        public IPagedList<ToastDesignDomain> GetToastDesign(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from c in _toastDesignRepository.Table
                        select c;


            query = query.OrderBy(b => b.CreatedOnUtc);

            var liveNotification = new PagedList<ToastDesignDomain>(query, pageIndex, pageSize);
            return liveNotification;
        }

        public IList<ToastDesignDomain> GetToastDesignFirst()
        {
            var query = from c in _toastDesignRepository.Table
                        select c;
            var listOfToast=query.ToList();
            return listOfToast;
        }



        public ToastDesignDomain GetToastDesignById(int Id)
        {
            var query = from c in _toastDesignRepository.Table
                        where c.Id == Id
                        select c;
            return new ToastDesignDomain
                {
                    Id = query.ToList().FirstOrDefault().Id,
                    BackgroundColor = query.ToList().FirstOrDefault().BackgroundColor,
                    ShowDuration = query.ToList().FirstOrDefault().ShowDuration,
                    HideDuration = query.ToList().FirstOrDefault().HideDuration,
                    TimeOut = query.ToList().FirstOrDefault().TimeOut,
                    ExtendedTimeOut = query.ToList().FirstOrDefault().ExtendedTimeOut,
                    CreatedOnUtc = query.ToList().FirstOrDefault().CreatedOnUtc,
                    UpdateOnUtc = query.ToList().FirstOrDefault().UpdateOnUtc

                };
        }
        #endregion    
    

   

    }
}