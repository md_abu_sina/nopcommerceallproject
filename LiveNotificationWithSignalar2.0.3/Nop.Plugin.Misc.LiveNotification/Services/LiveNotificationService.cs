﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.LiveNotification.Domain;
using System.Collections.Generic;
using Nop.Services.Events;
using System.Data.Entity;
using Nop.Data;
using Nop.Plugin.Misc.LiveNotification.Data;

namespace Nop.Plugin.Misc.LiveNotification.Services
{
    public partial class LiveNotificationService : ILiveNotificationService
    {
        #region Field
        private readonly IRepository<LiveNotificationDomain> _liveNotificationRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IDbContext _dbContext;
        #endregion

        #region Ctr

        public LiveNotificationService(IRepository<LiveNotificationDomain> liveNotificationRepository, IEventPublisher eventPublisher, IDbContext dbContext)
        {
            _liveNotificationRepository = liveNotificationRepository;
            this._eventPublisher = eventPublisher;
            _dbContext = dbContext;
        }

        #endregion

        #region Methods

        public void Delete(int ProductId)
        {
            //item.Deleted = true;

            var query = from c in _liveNotificationRepository.Table
                        where c.ProductId == ProductId
            select c;
            var homepageCategories = query.ToList();
            foreach (var homepageCategory in homepageCategories)
            {
                _liveNotificationRepository.Delete(homepageCategory);
            }
        }


        public bool Update(LiveNotificationDomain livenotification)
        {
            if (livenotification == null)
                throw new ArgumentNullException("customer");

            _liveNotificationRepository.Update(livenotification);
            _eventPublisher.EntityUpdated(livenotification);
            return true;
        }


        public void Insert(LiveNotificationDomain item)
        {
            //default value
            item.CreatedOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");
            item.UpdateOnUtc = DateTime.UtcNow.ToString("yyyy-MM-dd  HH:mm:ss");

            _liveNotificationRepository.Insert(item);
        }

        public IPagedList<LiveNotificationDomain> GetLiveNotification(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from c in _liveNotificationRepository.Table
                        group c by c.ProductId into groups
                        select groups.FirstOrDefault();


            query = query.OrderBy(b => b.ProductId);

            var liveNotification = new PagedList<LiveNotificationDomain>(query, pageIndex, pageSize);
            return liveNotification;
        }

        public int CountTodayOrderedProductCountToday(DateTime TodayDate)
        {
            var existingTableNames = new List<string>(_dbContext.SqlQuery<string>("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_type = 'BASE TABLE'"));
            var isTableExist= existingTableNames.FindAll(x=>x=="LiveNotification");
            if (isTableExist.Count>0)
            {
                string todayDateForSqlString = TodayDate.ToString("yyyy-MM-dd");
                var query = from p in _liveNotificationRepository.Table
                            where (p.CreatedOnUtc.Contains(todayDateForSqlString))
                            group p by p.ProductId into groups
                            select groups.FirstOrDefault();

                return query.ToList().Count();
            }
            else
            {
                return 0;
            }
        }

        public List<LiveNotificationDomain> GetNotificationForTodayOrder(int numberOfOrderToShow=5)
        {
            var existingTableNames = new List<string>(_dbContext.SqlQuery<string>("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_type = 'BASE TABLE'"));
            var isTableExist = existingTableNames.FindAll(x => x == "LiveNotification");

            string todayDateForSqlString = DateTime.UtcNow.ToString("yyyy-MM-dd");

            if (isTableExist.Count > 0)
            {
                var query = (from n in _liveNotificationRepository.Table
                             where (n.CreatedOnUtc.Contains(todayDateForSqlString) || n.UpdateOnUtc.Contains(todayDateForSqlString))
                             orderby n.UpdateOnUtc descending
                             select n).Take(numberOfOrderToShow);

                var todayOrder = query.ToList();
                return todayOrder;
            }
            else
            {
                var liveNotificationDomain = new List<LiveNotificationDomain>();
                return liveNotificationDomain;
            }
        }
        #endregion    
    


    }
}