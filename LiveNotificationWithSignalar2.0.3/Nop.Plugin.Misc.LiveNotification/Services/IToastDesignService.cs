﻿using System;
using Nop.Core;
using Nop.Plugin.Misc.LiveNotification.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.LiveNotification.Services
{
    public partial interface IToastDesignService
    {
        void Delete(int Id);
        void Insert(ToastDesignDomain item);
        bool Update(ToastDesignDomain toastDesignDomain);
        IPagedList<ToastDesignDomain> GetToastDesign(int pageIndex = 0, int pageSize = int.MaxValue);
        IList<ToastDesignDomain> GetToastDesignFirst();
        ToastDesignDomain GetToastDesignById(int Id);
    }
}