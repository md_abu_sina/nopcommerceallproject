﻿$(document).ready(function () {
    if ($(window).width() > 768) {
        $('.header').on('mouseenter', '#toptodayorderlink', function () {
            $('#flyout-todayorder').addClass('active');
        });
        $('.header').on('mouseleave', '#toptodayorderlink', function () {
            $('#flyout-todayorder').removeClass('active');
        });
        $('.header').on('mouseenter', '#flyout-todayorder', function () {
            $('#flyout-todayorder').addClass('active');
        });
        $('.header').on('mouseleave', '#flyout-todayorder', function () {
            $('#flyout-todayorder').removeClass('active');
        });
   } 
});
