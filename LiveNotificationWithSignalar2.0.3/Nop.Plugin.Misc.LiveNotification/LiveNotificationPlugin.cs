﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Nop.Core.Plugins;
using Nop.Plugin.Misc.LiveNotification.Data;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework.Menu;
using Owin;
using System;
using System.Collections.Generic;
using System.Web.Routing;
using System.Linq;

namespace Nop.Plugin.Misc.LiveNotification
{
    public class LiveNotificationPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        #region Fields

        private readonly LiveNotificationObjectContext _context;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingContext;

        private const string HomePageBeforeCategories = "home_page_before_categories";
        private const string HeaderLinkWidget = "header_links_after";

        #endregion


        #region Ctr

        public LiveNotificationPlugin(LiveNotificationObjectContext context, ILocalizationService localizationService, ISettingService settingContext)
        {
            _context = context;
            _localizationService = localizationService;
            _settingContext = settingContext;
        }

        #endregion

        #region Install / Uninstall


        public override void Install()
        {
            this.AddOrUpdatePluginLocaleResource("Misc.ToastDesign", "Toast Desing");
            this.AddOrUpdatePluginLocaleResource("LatestSellProducts", "Latest Sell Products");
            _settingContext.SetSetting("ShowRecentlyOrderProduct", "True");
            _settingContext.SetSetting("NumberOfRecentlyOrderProduct", 8);


            //resource
            //this.AddOrUpdatePluginLocaleResource("Plugins.Widgets.LivePersonChat.ButtonCode", "Button code(max 2000)");

            //install db
            _context.InstallSchema();

            //base install
            base.Install();
        }
        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            //_settingService.DeleteSetting<FroogleSettings>();

            //data
           _context.Uninstall();
           this.DeletePluginLocaleResource("Misc.ToastDesign");
           this.DeletePluginLocaleResource("LatestSellProducts");


           //_settingContext.DeleteSetting<string>("ShowRecentlyOrderProduct");
           //_settingContext.DeleteSetting<string>("NumberOfRecentlyOrderProduct");


            base.Uninstall();
        }

        #endregion

        #region Menu Builder

        public bool Authenticate()
        {
            return true;
        }

        #endregion


        //public void ManageSiteMap(SiteMapNode rootNode)
        //{
        //    var mainnode = new SiteMapNode()
        //    {
        //        Title = _localizationService.GetResource("Plugin"),
        //        Visible = true,
        //        RouteValues = new RouteValueDictionary() { { "area", "Admin" } }
        //    };
        //    rootNode.ChildNodes.Add(mainnode);
        //    var subnode = new SiteMapNode()
        //    {
        //        Title = _localizationService.GetResource("Misc.ToastDesign"),
        //        Visible = true,
        //        Url = "~/LiveNotification/ToastDesign",
        //        RouteValues = new RouteValueDictionary() { { "area", "Admin" } }
        //    };
        //    mainnode.ChildNodes.Add(subnode);
        //}


        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                SystemName = "LiveNotification",
                Title = _localizationService.GetResource("Misc.ToastDesign"),
                Visible = true,
                Url = "~/LiveNotification/ToastDesign",
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };
            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Third party plugins");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(menuItem);
            else
                rootNode.ChildNodes.Add(menuItem);
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string>
            { 
               // "body_end_html_tag_before"
               HomePageBeforeCategories,
               HeaderLinkWidget
            };
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "LiveNotification";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Misc.LiveNotification.Controllers" }, { "area", null } };
        }

        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            if (widgetZone.Equals(HeaderLinkWidget))
            {
                actionName = "PublicPage";
                controllerName = "LiveNotification";
                routeValues = new RouteValueDictionary
                {
                    {"Namespaces", "Nop.Plugin.Misc.LiveNotification.Controllers"},
                    {"area", null},
                    {"widgetZone", widgetZone}
                };
            }
            else
            {
                
                    actionName = "OrderedProductAtHomePage";
                    controllerName = "LiveNotification";
                    routeValues = new RouteValueDictionary
                {
                    {"Namespaces", "Nop.Plugin.Misc.LiveNotification.Controllers"},
                    {"area", null},
                    {"widgetZone", widgetZone}
                };
                
            }
        }
    }
}