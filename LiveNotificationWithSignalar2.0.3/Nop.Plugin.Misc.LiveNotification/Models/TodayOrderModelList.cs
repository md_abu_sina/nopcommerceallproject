﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Misc.LiveNotification.Models
{
    public class TodayOrderModelList : BaseNopModel
    {
        public TodayOrderModelList()
        {
            TodayOrder = new List<TodayOrderModel>();
        }

        public List<TodayOrderModel> TodayOrder { get; set; }
    }
}
