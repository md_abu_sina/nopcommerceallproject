﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.LiveNotification.Models;
using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Common;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Misc.LiveNotification.Models
{
    public class TodayOrderDetailListModel
    {
        public TodayOrderDetailListModel()
        {
            Products = new List<ProductOverviewModel>();
        }

        public PagerModel PagerModel { get; set; }

        public List<ProductOverviewModel> Products { get; set; }

        
    }
}