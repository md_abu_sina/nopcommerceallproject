﻿using Nop.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.LiveNotification.Models
{
    public class LiveNotificationRouteValues : IRouteValues
    {
        public int page { get; set; }
    }
}
