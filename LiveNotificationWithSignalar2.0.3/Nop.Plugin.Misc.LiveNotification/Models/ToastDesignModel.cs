﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.LiveNotification.Models
{
    public partial class ToastDesignModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Background Color")]
        public string BackgroundColor { get; set; }
        [NopResourceDisplayName("Show Duration")]
        public string ShowDuration { get; set; }
        [NopResourceDisplayName("Hide Duration")]
        public string HideDuration { get; set; }

        [NopResourceDisplayName("Time Out")]
        public string TimeOut { get; set; }

        [NopResourceDisplayName("Extended TimeOut")]
        public string ExtendedTimeOut { get; set; }
        public string CreatedOnUtc { get; set; }
        public string UpdateOnUtc { get; set; }
    }
}