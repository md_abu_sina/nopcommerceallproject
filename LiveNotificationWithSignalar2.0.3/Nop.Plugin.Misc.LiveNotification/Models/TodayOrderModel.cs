﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.LiveNotification.Models
{
    public partial class TodayOrderModel : BaseNopModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ShortDescription { get; set; }
        public string SeName { get; set; }
        public decimal Price { get; set; }
        public string ImgUrl { get; set; }
    }
}