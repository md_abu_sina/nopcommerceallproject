﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Microsoft.Owin.Builder;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Misc.LiveNotification.Data;
using Nop.Plugin.Misc.LiveNotification.Domain;
using Nop.Plugin.Misc.LiveNotification.Services;
using Owin;

namespace Nop.Plugin.Misc.LiveNotification
{
    public partial class DependencyRegister : IDependencyRegistrar
    {
        #region Field

        private const string ContextName = "nop_object_context_live_notification";

        #endregion

        #region Register

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            //Load custom data settings
            var dataSettingsManager = new DataSettingsManager();
            var dataSettings = dataSettingsManager.LoadSettings();

            //Register custom object context
            builder.Register<IDbContext>(c => RegisterIDbContext(c, dataSettings)).Named<IDbContext>(ContextName).InstancePerHttpRequest();
            builder.Register(c => RegisterIDbContext(c, dataSettings)).InstancePerHttpRequest();

            //Register services

            builder.RegisterType<LiveNotificationService>().As<ILiveNotificationService>();
            builder.RegisterType<ToastDesignService>().As<IToastDesignService>();
            //builder.RegisterType<AppBuilder>().As<IAppBuilder>();

            
            //Override the repository injection
            builder.RegisterType<EfRepository<LiveNotificationDomain>>().As<IRepository<LiveNotificationDomain>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(ContextName)).InstancePerHttpRequest();
            builder.RegisterType<EfRepository<ToastDesignDomain>>().As<IRepository<ToastDesignDomain>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>(ContextName)).InstancePerHttpRequest();


        }

        #endregion

        #region DB

        public int Order
        {
            get { return 0; }
        }

        private LiveNotificationObjectContext RegisterIDbContext(IComponentContext componentContext,
                                                                DataSettings dataSettings)
        {
            string dataConnectionStrings;

            if (dataSettings != null && dataSettings.IsValid())
            {
                dataConnectionStrings = dataSettings.DataConnectionString;
            }
            else
            {
                dataConnectionStrings = componentContext.Resolve<DataSettings>().DataConnectionString;
            }

            return new LiveNotificationObjectContext(dataConnectionStrings);
        }

        #endregion

       
    }
}