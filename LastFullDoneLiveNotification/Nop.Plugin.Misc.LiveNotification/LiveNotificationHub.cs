﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.LiveNotification
{
    [HubName("livenotificationHub")]
    public class LiveNotificationHub:Hub
    {
        [HubMethodName("liveNotification")]
        public static void liveNotification(string orderId)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<LiveNotificationHub>();
            context.Clients.All.liveNotification(orderId);
        }
    }
}
