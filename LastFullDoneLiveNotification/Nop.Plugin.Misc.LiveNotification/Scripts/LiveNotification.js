﻿var seName = "";
var todayOrder = "";
$(function () {
    // Declare a proxy to reference the hub.
    var notifications = $.connection.livenotificationHub;
    var orderIdForStart = "";
    //debugger;
    // Create a function that the hub can call to broadcast messages.
    notifications.client.liveNotification = function (orderId) {
        showMessage(orderId);
        todayOrderFly();
        latestSellProduct();
    };
    // Start the connection.
    $.connection.hub.start().done(function () {
    }).fail(function (e) {
        alert(e);
    });

});

function showMessage(orderId) {
    if (orderId > 0) {
    $.ajax({
        url: '/LiveNotification/OrderedItem',
        type: 'GET',
        data: { "orderId": orderId },
    }).success(function (result) {
        $(".todayQty").html("" + result.TodayTotalCount + "");
        var tostView = "";
        for (var i = 0; i < result.ToastDesign.length; i++) {
            localStorage.setItem("backGroudColor", result.ToastDesign[i].BackgroundColor);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "" + result.ToastDesign[i].ShowDuration + "",
                "hideDuration": "" + result.ToastDesign[i].HideDuration + "",
                "timeOut": "" + result.ToastDesign[i].TimeOut + "",
                "extendedTimeOut": "" + result.ToastDesign[i].ExtendedTimeOut + "",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
        }

        for (var i = 0; i < result.Products.length; i++) {
            tostView = '<div class="item" style="position: relative;padding-left: 80px;min-height: 75px;"><a href="' + result.Products[i].SeName + '" class="seNameToast"><div class="picture" style="margin-top: 3px;position: absolute;left: 0;top: 0;width: 75px;height: 75px;"><a href="' + result.Products[i].SeName + '"><img src="' + result.Products[i].ImgUrl + '"></a></div><div class="product"><div class="name" style="overflow: hidden;height: 24px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;"><a href="/' + result.Products[i].SeName + '"  style="font-weight:bold">' + result.Products[i].ProductName + '</a></div><div class="price"  style="font-weight:bold"><a href="' + result.Products[i].SeName + '">Price: <span>' + result.Products[i].Price + '</span></a></div><div style="height: 34px;overflow: hidden;"><a href="/' + result.Products[i].SeName + '"  style="font-weight:bold"><p style="margin-top: 4px;">Short Description: <span>' + result.Products[i].ShortDescription + '</span></p></a></div></div></a></div>'
            toastr["info"](tostView);
            seName = result.Products[i].SeName;
            var backColor=localStorage.getItem("backGroudColor");
            $('.toast-info').css("background-color", backColor)
            localStorage.setItem("seName", seName);
        }

        toastr.options.onclick = function () {
            seName = localStorage.getItem("seName");
            window.location.href = seName;
        }

        $(".toast").click(function () {
            window.location.href = $(this).find(".seNameToast").attr('href');
        });

    }).error(function () {
    });
    }
}

function todayOrderFly() {
    $.ajax({
        url: '/LiveNotification/TodayOrderFlyOutAjax',
        type: 'GET'
    }).success(function (result) {
        for (var i = 0; i < result.TodayOrder.length; i++) {
            var item = result.TodayOrder[i];
            if (i == 0) {
                todayOrder += "<div class='item first'>" +
                    "<div class='picture'><a href='@Url.RouteUrl('Product', new { SeName = " + item.SeName + " })'><img src='" + item.ImgUrl + "' /></a></div>" +
                    "<div class='product'>" +
                        "<div class='name' style='overflow: hidden;white-space: nowrap;text-overflow: ellipsis;'><a href='@Url.RouteUrl('Product', new { SeName = " + item.SeName + " })'>" + item.ProductName + "</a></div>" +
                                    "<div class='price' style='margin-top:-31px;'>Price: <span>" + item.Price + "</span></div></div></div>"
            }
            else {
                todayOrder +="<div class='item'>" +
                                    "<div class='picture'><a href='@Url.RouteUrl('Product', new { SeName = " + item.SeName + " })'><img src='" + item.ImgUrl + "' /></a></div>" +
                                    "<div class='product'>" +
                                        "<div class='name' style='overflow: hidden;white-space: nowrap;text-overflow: ellipsis;'><a href='@Url.RouteUrl('Product', new { SeName = " + item.SeName + " })'>" + item.ProductName + "</a></div>" +
                                                    "<div class='price' style='margin-top:-31px;'>Price: <span>" + item.Price + "</span></div></div></div>"
            }
        }
        $(".todayOrderFly").empty();
        $(".todayOrderFly").append(todayOrder);
    });
};


function latestSellProduct() {
    $.ajax({
        url: '/LiveNotification/OrderedProductAtHomePageAjax',
        type: 'GET'
    }).success(function (result) {
        $(".latestSell").empty();
        $(".latestSell").append(result);
    });
};
