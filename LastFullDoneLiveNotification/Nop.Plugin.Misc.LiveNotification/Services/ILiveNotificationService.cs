﻿using System;
using Nop.Core;
using Nop.Plugin.Misc.LiveNotification.Domain;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.LiveNotification.Services
{
    public partial interface ILiveNotificationService
    {
        void Delete(int ProductId);
        void Insert(LiveNotificationDomain item);
        bool Update(LiveNotificationDomain liveNotificationDomain);
        IPagedList<LiveNotificationDomain> GetLiveNotification(int pageIndex = 0, int pageSize = int.MaxValue);
        int CountTodayOrderedProductCountToday(DateTime TodayDate);

        List<LiveNotificationDomain> GetNotificationForTodayOrder(int numberOfOrderToShow);
    }
}