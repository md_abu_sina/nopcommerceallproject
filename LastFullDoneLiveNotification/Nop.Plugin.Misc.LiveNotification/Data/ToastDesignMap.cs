﻿using Nop.Plugin.Misc.LiveNotification.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Misc.LiveNotification.Data
{
    public class ToastDesignMap : EntityTypeConfiguration<ToastDesignDomain>
    {
        public ToastDesignMap()
        {
            ToTable("ToastDesign");

            HasKey(x => x.Id);
            Property(x => x.BackgroundColor);
            Property(x => x.ExtendedTimeOut);
            Property(x => x.HideDuration);
            Property(x => x.ShowDuration);
            Property(x => x.TimeOut);
        }
    }
}
