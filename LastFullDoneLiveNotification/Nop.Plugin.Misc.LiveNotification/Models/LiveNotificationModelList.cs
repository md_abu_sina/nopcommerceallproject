﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Misc.LiveNotification.Models
{
    public class LiveNotificationModelList : BaseNopModel
    {
        public LiveNotificationModelList()
        {
            Products = new List<LiveNotificationModel>();
            ToastDesign = new List<ToastDesignModel>();
        }

        public List<LiveNotificationModel> Products { get; set; }
        public List<ToastDesignModel> ToastDesign { get; set; }

        public string TodayTotalCount { get; set; }
    }
}
