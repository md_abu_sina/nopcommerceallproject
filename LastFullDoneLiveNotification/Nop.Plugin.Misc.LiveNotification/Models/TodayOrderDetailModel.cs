﻿using Nop.Web.Framework.UI.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.LiveNotification.Models
{
    public class TodayOrderDetailModel : BasePageableModel
    {


        public string ProductId { get; set; }

        public string ProductName { get; set; }
        public string ProductShortDescription { get; set; }
        public string ProductDescription { get; set; }
        public string ImgUrl { get; set; }
        public double Price { get; set; }
    }
}
